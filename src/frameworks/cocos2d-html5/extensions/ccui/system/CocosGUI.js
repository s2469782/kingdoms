/**
 * The namespace of Cocos UI
 * @namespace
 * @name ccui
 */
var ccui = ccui || {};

//These classes defines are use for jsDoc
/**
 * The same as cc.Class
 * @class
 */
ccui.Class = ccui.Class || cc.Class;
ccui.Class.extend = ccui.Class.extend || cc.Class.extend;

/**
 * that same as cc.Node
 * @class
 * @extends ccui.Class
 */
ccui.Node = ccui.Node || cc.Node;
ccui.Node.extend = ccui.Node.extend || cc.Node.extend;


/**
 * that same as cc.Node
 * @class
 * @extends ccui.Node
 */
ccui.ProtectedNode = ccui.ProtectedNode || cc.ProtectedNode;
ccui.ProtectedNode.extend = ccui.ProtectedNode.extend || cc.ProtectedNode.extend;

/**
 * Cocos UI version
 * @type {String}
 */
ccui.cocosGUIVersion = "CocosGUI v1.0.0.0";