var MessageConfig = MessageConfig || {};
MessageConfig.Message = {
    "101": {
        msgId: MessageCode.CH_LoginMsg,        
        name: "CH_LoginMsg",
        eventName: "event.CH_LoginMsg"
    },
    "102": {
        msgId: MessageCode.CH_LoginMsg_Res,        
        name: "CH_LoginMsg_Res",
        eventName: "event.CH_LoginMsg_Res"
    },
    "103": {
        msgId: MessageCode.CH_AskGateMsg,        
        name: "CH_AskGateMsg",
        eventName: "event.CH_AskGateMsg"
    },
    "104": {
        msgId: MessageCode.CH_AskGateMsg_Res,
        name: "CH_AskGateMsg_Res",
        eventName: "event.CH_AskGateMsg_Res"
    },
    "105": {
        msgId: MessageCode.CH_ModifyPlayerinfoMsg,
        name: "CH_ModifyPlayerInfo",
        eventName: "event.CH_ModifyPlayerinfoMsg"
    },
    "106": {
        msgId: MessageCode.CH_FortUpLevel,
        name: "CH_FortUpLevel",
        eventName: "event.CH_FortUpLevel"
    },
    "107": {
        msgId: MessageCode.CH_FortUpLevel_Res,
        name: "CH_FortUpLevel_Res",
        eventName: "event.CH_FortUpLevel_Res"
    },
    "108": {
        msgId: MessageCode.CH_LoginBonusMsg,
        name: "CH_LoginBonus",
        eventName: "event.CH_LoginBonus"
    },
    "109": {
        msgId: MessageCode.CH_LoginBonusMsg_Res,
        name: "CH_LoginBonus_Res",
        eventName: "event.CH_LoginBonus_Res"
    },
    "110":{
        msgId: MessageCode.CH_TaskRewardMsg,
        name: "CS_TaskReward",
        eventName: "event.CH_TaskRewardMsg"
    },
    "111":{
        msgId: MessageCode.CH_TaskRewardMsg_Res,
        name: "CS_TaskReward_Res",
        eventName: "event.CH_TaskRewardMsg_Res"
    },
    "112": {
        msgId: MessageCode.CH_AskAllTaskInfoMsg,
        name: "CH_AllTaskInfo",
        eventName: "event.CH_AllTaskInfo"
    },
    "113": {
        msgId: MessageCode.CH_AskAllTaskInfoMsg_Res,
        name: "CH_AllTaskInfo_Res",
        eventName: "event.CH_AllTaskInfo_Res"
    },
    "114":{
        msgId: MessageCode.CH_TaskActiveGiftMsg,
        name: "CH_TaskActiveGift",
        eventName: "event.CH_TaskActiveGift"
    },
    "115":{
        msgId: MessageCode.CH_TaskActiveGiftMsg_Res,
        name: "CH_TaskActiveGift_Res",
        eventName: "event.CH_TaskActiveGift_Res"
    },
    "116":{
        msgId: MessageCode.CH_TaskVIPGift,
        name: "CH_TaskVIPGift",
        eventName: "event.CH_TaskVIPGift"
    },
    "117":{
        msgId: MessageCode.CH_TaskVIPGift_Res,
        name: "CH_TaskVIPGift_Res",
        eventName: "event.CH_TaskVIPGift_Res"
    },
    "118":{
        msgId: MessageCode.CH_ModifyNickname,
        name: "CH_ModifyNickname",
        eventName: "event.CH_ModifyNickname"
    },
    "119": {
        msgId: MessageCode.CH_ModifyNickname_Res,
        name: "CH_ModifyPlayerInfo_Res",
        eventName: "event.CH_ModifyNickname_Res"
    },
    "120": {
        msgId: MessageCode.CH_ModifyPlayerinfoMsg_Res,
        name: "CH_ModifyPlayerInfo_Res",
        eventName: "event.CH_ModifyPlayerinfoMsg_Res"
    },
    
    "121":{
        msgId: MessageCode.HC_ScrollInfoMsg,
        name: "DG_ScrollInfo",
        eventName: "event.HC_ScrollInfoMsg"
    },

    "122":{
        msgId: MessageCode.HC_PicNoticeInfoMsg,
        name: "DG_PicNoticeInfo",
        eventName: "event.HC_PicNoticeInfoMsg"
    },

    "123":{
        msgId: MessageCode.CH_AskActivityMsg,
        name: "CH_AskActivity",
        eventName: "event.CH_AskActivityMsg"
    },
    "124":{
        msgId: MessageCode.CH_AskActivityMsg_Res,
        name: "CH_AskActivity_Res",
        eventName: "event.CH_AskActivityMsg_Res"
    },
    "125":{
        msgId: MessageCode.CH_ReceiveActAwardMsg,
        name: "CH_ReceiveActAward",
        eventName: "event.CH_ReceiveActAward"
    },
    "126":{
        msgId: MessageCode.CH_ReceiveActAwardMsg_Res,
        name: "CH_ReceiveActAward_Res",
        eventName: "event.CH_ReceiveActAward_Res"
    },
    "127": {
        msgId: MessageCode.CH_InHallReceiveMonthCardGift,
        name: "CH_InHallReceiveMonthCardGift",
        eventName: "event.CH_InHallReceiveMonthCardGift"
    },
    "128": {
        msgId: MessageCode.CH_InHallReceiveMonthCardGift_Res,
        name: "CH_InHallReceiveMonthCardGift_Res",
        eventName: "event.CH_InHallReceiveMonthCardGift_Res"
    },
    "129": {
        msgId: MessageCode.CH_SellItemMsg,
        name: "CH_SellItemMsg",
        eventName: "event.CH_SellItemMsg"
    },
    "130": {
        msgId: MessageCode.CH_SellItemMsg_Res,
        name: "CH_SellItemMsg_Res",
        eventName: "event.CH_SellItemMsg_Res"
    },
    "131": {
        msgId: MessageCode.HC_NewMainTask,
        name: "HC_NewMainTask",
        eventName: "event.HC_NewMainTask"
    },
    "132": {
        msgId: MessageCode.HC_UpdateActRule,
        name: "HC_UpdateActRule",
        eventName: "event.HC_UpdateActRule"
    },
    "133": {
        msgId: MessageCode.HC_ModifyActScoreMsg,
        name: "HC_ModifyActScoreMsg",
        eventName: "event.HC_ModifyActScoreMsg"
    },
    "134": {
        msgId: MessageCode.CS_BeginnerGuide,
        name: "CS_BeginnerGuide",
        eventName: "event.CS_BeginnerGuide"
    },
    "135": {
        msgId: MessageCode.CS_BeginnerGuide_Res,
        name: "CS_BeginnerGuide_Res",
        eventName: "event.CS_BeginnerGuide_Res"
    },


    "137": {
        msgId: MessageCode.CH_GrandPrixRankMsg,
        name: "CH_GrandPrixRankMsg",
        eventName: "event.CH_GrandPrixRankMsg"
    },
    "138": {
        msgId: MessageCode.CH_GrandPrixRankMsg_Res,
        name: "CH_GrandPrixRankMsg_Res",
        eventName: "event.CH_GrandPrixRankMsg_Res"
    },
    "139": {
        msgId: MessageCode.HC_GrandPrixRewardMsg,
        name: "HC_GrandPrixRewardMsg",
        eventName: "event.HC_GrandPrixRewardMsg"
    },
    "140": {
        msgId: MessageCode.HC_UpdateGrandRankMsg,
        name: "HC_UpdateGrandRankMsg",
        eventName: "event.HC_UpdateGrandRankMsg"
    },
    "141": {
        msgId: MessageCode.CH_AskExchangeInfoMsg,
        name: "CH_AskExchangeInfoMsg",
        eventName: "event.CH_AskExchangeInfoMsg"
    },
    "142": {
        msgId: MessageCode.CH_AskExchangeInfoMsg_Res,
        name: "CH_AskExchangeInfoMsg_Res",
        eventName: "event.CH_AskExchangeInfoMsg_Res"
    },

    "143": {
        msgId: MessageCode.CH_ExchangePrizesMsg,
        name: "CH_ExchangePrizesMsg",
        eventName: "event.CH_ExchangePrizesMsg"
    },
    "144": {
        msgId: MessageCode.CH_ExchangePrizesMsg_Res,
        name: "CH_ExchangePrizesMsg_Res",
        eventName: "event.CH_ExchangePrizesMsg_Res"
    },
    "145": {
        msgId: MessageCode.HC_UpdateExchangeMsg,
        name: "HC_UpdateExchangeMsg",
        eventName: "event.HC_UpdateExchangeMsg"
    },
    "146": {
        msgId: MessageCode.CH_GetFirstRewardMsg,
        name: "CH_GetFirstRewardMsg",
        eventName: "event.CH_GetFirstRewardMsg"
    },
    "147": {
        msgId: MessageCode.HC_GetFirstRewardMsg_Res,
        name: "HC_GetFirstRewardMsg_Res",
        eventName: "event.HC_GetFirstRewardMsg_Res"
    },
    "148": {
        msgId: MessageCode.HC_NewExchanInvenData,
        name: "HC_NewExchanInvenData",
        eventName: "event.HC_NewExchanInvenData"
    },
    "149": {
        msgId: MessageCode.CH_AskExchangeRecordMsg,
        name: "CH_AskExchangeRecordMsg",
        eventName: "event.CH_AskExchangeRecordMsg"
    },
    "150": {
        msgId: MessageCode.CH_AskExchangeRecordMsg_Res,
        name: "CH_AskExchangeRecordMsg_Res",
        eventName: "event.CH_AskExchangeRecordMsg_Res"
    },
    "151": {
        msgId: MessageCode.HC_RefreshLuckDraw,
        name: "HC_RefreshLuckDraw",
        eventName: "event.HC_RefreshLuckDraw"
    },
    "152": {
        msgId: MessageCode.CH_LuckDraw,
        name: "CH_LuckDraw",
        eventName: "event.CH_LuckDraw"
    },
    "153": {
        msgId: MessageCode.CH_LuckDraw_Res,
        name: "CH_LuckDraw_Res",
        eventName: "event.CH_LuckDraw_Res"
    },
    "154": {
        msgId: MessageCode.CH_RewardSignInMsg,
        name: "CH_RewardSignInMsg",
        eventName: "event.CH_RewardSignInMsg"
    },
    "155": {
        msgId: MessageCode.CH_RewardSignInMsg_Res,
        name: "CH_RewardSignInMsg_Res",
        eventName: "event.CH_RewardSignInMsg_Res"
    },
    "156": {
        msgId: MessageCode.CH_GetRewardCouponMsg,
        name: "CH_GetRewardCouponMsg",
        eventName: "event.CH_GetRewardCouponMsg"
    },
    "157": {
        msgId: MessageCode.CH_GetRewardCouponMsg_Res,
        name: "CH_GetRewardCouponMsg_Res",
        eventName: "event.CH_GetRewardCouponMsg_Res"
    },
    "158": {
        msgId: MessageCode.CH_IsNewPlayerMsg,
        name: "CH_IsNewPlayerMsg",
        eventName: "event.CH_IsNewPlayerMsg"
    },
    "159": {
        msgId: MessageCode.CH_ModifySignature,
        name: "CH_ModifySignature",
        eventName: "event.CH_ModifySignature"
    },
    "160": {
        msgId: MessageCode.CH_PlayerSignature_Res,
        name: "CH_PlayerSignature_Res",
        eventName: "event.CH_PlayerSignature_Res"
    },
    "161": {
        msgId: MessageCode.CH_RegisterMsg,
        name: "CH_RegisterMsg",
        eventName: "event.CH_RegisterMsg"
    },
    "162": {
        msgId: MessageCode.CH_RegisterMsg_Res,
        name: "CH_RegisterMsg_Res",
        eventName: "event.CH_RegisterMsg_Res"
    },
    "163": {
        msgId: MessageCode.CH_ModifyPassword,
        name: "CH_ModifyPassword",
        eventName: "event.CH_ModifyPassword"
    },
    "164": {
        msgId: MessageCode.CH_ModifyPassword_Res,
        name: "CH_ModifyPassword_Res",
        eventName: "event.CH_ModifyPassword_Res"
    },
    "165": {
        msgId: MessageCode.CH_UseItem,
        name: "CH_UseItem",
        eventName: "event.CH_UseItem"
    },
    "166": {
        msgId: MessageCode.CH_UseItem_Res,
        name: "CH_UseItem_Res",
        eventName: "event.CH_UseItem_Res"
    },
    "167": {
        msgId: MessageCode.JP_Total,
        name: "JP_Total",
        eventName: "event.JP_Total"
    },
    "168": {
        msgId: MessageCode.JP_Total_Res,
        name: "JP_Total_Res",
        eventName: "event.JP_Total_Res"
    },
    "501": {
        msgId: MessageCode.CS_JoinCommStageMsg,
        name: "CS_JoinCommStageMsg",
        eventName: "event.CS_JoinCommStageMsg"
    },
    "502": {
        msgId: MessageCode.CS_JoinCommStageMsg_Res,
        name: "CS_JoinCommStageMsg_Res",
        eventName: "event.CS_JoinCommStageMsg_Res"
    },
    "503": {
        msgId: MessageCode.SC_NewPlayerJoinRoomMsg,
        name: "SC_NewPlayerJoinRoomMsg",
        eventName: "event.SC_NewPlayerJoinRoomMsg"
    },
    "504": {
        msgId: MessageCode.SC_RemovePlayerFromRoomMsg,
        name: "SC_RemovePlayerFromRoomMsg",
        eventName: "event.SC_RemovePlayerFromRoomMsg"
    },
    "505": {
        msgId: MessageCode.CS_PlayerFireMsg,
        name: "CS_PlayerFireMsg",
        eventName: "event.CS_PlayerFireMsg"
    },
    "506": {
        msgId: MessageCode.CS_LeaveRoomMsg,
        name: "CS_LeaveRoomMsg",
        eventName: "event.CS_LeaveRoomMsg"
    },
    "507": {
        msgId: MessageCode.CS_LeaveRoomMsg_Res,
        name: "CS_LeaveRoomMsg_Res",
        eventName: "event.CS_LeaveRoomMsg_Res"
    },
    "508": {
        msgId: MessageCode.SC_PlayerFireInRoomMsg,
        name: "SC_PlayerFireInRoomMsg",
        eventName: "event.SC_PlayerFireInRoomMsg"
    },
    "509": {
        msgId: MessageCode.SC_NewFishJoinRoom,
        name: "SC_NewFishJoinRoom",
        eventName: "event.SC_NewFishJoinRoom"
    },
    "510": {
        msgId: MessageCode.CS_PlayerCatch,
        name: "CS_PlayerCatch",
        eventName: "event.CS_PlayerCatch"
    },
    "511": {
        msgId: MessageCode.CS_PlayerCatch_Res,
        name: "CS_PlayerCatch_Res",
        eventName: "event.CS_PlayerCatch_Res"
    },
    "512": {
        msgId: MessageCode.CS_ModifyFort,
        name: "CS_ModifyFort",
        eventName: "event.CS_ModifyFort"
    },
    "513": {
        msgId: MessageCode.CS_ModifyFort_Res,
        name: "CS_ModifyFort_Res",
        eventName: "event.CS_ModifyFort_Res"
    },
    "514": {
        msgId: MessageCode.SC_PlayerLevelUp,
        name: "SC_PlayerLevelUp",
        eventName: "event.SC_PlayerLevelUp"
    },
    "515": {
        msgId: MessageCode.CS_DoleCoolDownMsg,
        name: "CS_DoleCoolDown",
        eventName: "event.CS_DoleCoolDownMsg"
    },
    "516": {
        msgId: MessageCode.CS_DoleCoolDown_Res,
        name: "CS_DoleCoolDown_Res",
        eventName: "event.CS_DoleCoolDown_Res"
    },
    "517": {
        msgId: MessageCode.CS_DoleGetMsg,
        name: "CS_DoleGet",
        eventName: "event.CS_DoleGetMsg"
    },
    "518": {
        msgId: MessageCode.CS_DoleGet_Res,
        name: "CS_DoleGet_Res",
        eventName: "event.CS_DoleGet_Res"
    },
    "519": {
        msgId: MessageCode.CS_UnlockFort,
        name: "CS_UnlockFort",
        eventName: "event.CS_UnlockFort"
    },
    "520": {
        msgId: MessageCode.CS_UnlockFort_Res,
        name: "CS_UnlockFort_Res",
        eventName: "event.CS_UnlockFort_Res"
    },
    "521": {
        msgId: MessageCode.SC_TaskFinishMsg,
        name: "SC_TaskFinish",
        eventName: "event.SC_TaskFinish"
    },
    "522": {
        msgId: MessageCode.CS_TaskReward,
        name: "CS_TaskReward",
        eventName: "event.CS_TaskReward"
    },
    "523": {
        msgId: MessageCode.CS_TaskReward_Res,
        name: "CS_TaskReward_Res",
        eventName: "event.CS_TaskReward_Res"
    },
    "524": {
        msgId: MessageCode.CS_OnlineReward,
        name: "CS_OnlineReward",
        eventName: "event.CS_OnlineReward"
    },
    "525": {
        msgId: MessageCode.CS_OnlineReward_Res,
        name: "CS_OnlineReward_Res",
        eventName: "event.CS_OnlineReward_Res"
    },
    "526": {
        msgId: MessageCode.CS_JackpotDraw,
        name: "CS_JackpotDraw",
        eventName: "event.CS_JackpotDraw"
    },
    "527": {
        msgId: MessageCode.CS_JackpotDraw_Res,
        name: "CS_JackpotDraw_Res",
        eventName: "event.CS_JackpotDraw_Res"
    },
    "528": {
        msgId: MessageCode.CS_ModifyFortSkin,
        name: "CS_ModifyFortSkin",
        eventName: "event.CS_ModifyFortSkin"
    },
    "529": {
        msgId: MessageCode.CS_ModifyFortSkin_Res,
        name: "CS_ModifyFortSkin_Res",
        eventName: "event.CS_ModifyFortSkin_Res"
    },
    "530": {
        msgId: MessageCode.CS_UnlockFortSkin,
        name: "CS_ModifyFortSkin",
        eventName: "event.CS_UnlockFortSkin"
    },
    "531": {
        msgId: MessageCode.CS_UnlockFortSkin_Res,
        name: "CS_UnlockFortSkin_Res",
        eventName: "event.CS_UnlockFortSkin_Res"
    },
    "532": {
        msgId: MessageCode.SC_ModifyRoomInfoMsg,
        name: "SC_ModifyRoomInfoMsg",
        eventName: "event.SC_ModifyRoomInfoMsg"
    },
    "533": {
        msgId: MessageCode.CS_ChatInRoom,
        name: "CS_ChatInRoom",
        eventName: "event.CS_ChatInRoom"
    },
    "534": {
        msgId: MessageCode.CS_ChatInRoom_Res,
        name: "CS_ChatInRoom_Res",
        eventName: "event.CS_ChatInRoom_Res"
    },
    "535": {
        msgId: MessageCode.CS_InRoomReceiveMonthCardGift,
        name: "CS_InRoomReceiveMonthCardGift",
        eventName: "event.CS_InRoomReceiveMonthCardGift"
    },
    "536": {
        msgId: MessageCode.CS_InRoomReceiveMonthCardGift_Res,
        name: "CS_InRoomReceiveMonthCardGift_Res",
        eventName: "event.CS_InRoomReceiveMonthCardGift_Res"
    },
    "537": {
        msgId: MessageCode.CH_InRoomTaskVIPGift,
        name: "CH_InRoomTaskVIPGift",
        eventName: "event.CH_InRoomTaskVIPGift"
    },
    "538": {
        msgId: MessageCode.CH_InRoomTaskVIPGift_Res,
        name: "CH_InRoomTaskVIPGift_Res",
        eventName: "event.CH_InRoomTaskVIPGift_Res"
    },
    "547": {
        msgId: MessageCode.CS_OpenPokerFishMsg,
        name: "CS_OpenPokerFishMsg",
        eventName: "event.CS_OpenPokerFishMsg"
    },
    "548": {
        msgId: MessageCode.CS_OpenPokerFishMsg_Res,
        name: "CS_OpenPokerFishMsg_Res",
        eventName: "event.CS_OpenPokerFishMsg_Res"
    },
    "549": {
        msgId: MessageCode.SC_UpdateBonusPoolsDataMsg,
        name: "SC_UpdateBonusPoolsDataMsg",
        eventName: "event.SC_UpdateBonusPoolsDataMsg"
    },
    "550": {
        msgId: MessageCode.CS_ShowPlayerRank,
        name: "CS_ShowPlayerRank",
        eventName: "event.CS_ShowPlayerRank"
    },
    "551": {
        msgId: MessageCode.CS_ShowPlayerRank_Res,
        name: "CS_ShowPlayerRank_Res",
        eventName: "event.CS_ShowPlayerRank_Res"
    },

    "552": {
        msgId: MessageCode.CS_AgreeRank,
        name: "CS_AgreeRank",
        eventName: "event.CS_AgreeRank"
    },
    "553": {
        msgId: MessageCode.CS_AgreeRank_Res,
        name: "CS_AgreeRank_Res",
        eventName: "event.CS_AgreeRank_Res"
    },
    "554": {
        msgId: MessageCode.SC_LogMsg,
        name: "SC_LogMsg",
        eventName: "event.SC_LogMsg"
    },
    "555": {
        msgId: MessageCode.SC_BillingWelfare,
        name: "SC_BillingWelfare",
        eventName: "event.SC_BillingWelfare"
    },
    "556": {
        msgId: MessageCode.CS_ChangeFort,
        name: "CS_ChangeFort",
        eventName: "event.CS_ChangeFort"
    },
    "557": {
        msgId: MessageCode.CS_ChangeFort_Res,
        name: "CS_ChangeFort_Res",
        eventName: "event.CS_ChangeFort_Res"
    },
    "558": {
        msgId: MessageCode.CS_OutDateFort,
        name: "CS_OutDateFort",
        eventName: "event.CS_OutDateFort"
    },

    "559": {
        msgId: MessageCode.SC_FishTideMsg,
        name: "SC_FishTide",
        eventName: "event.SC_FishTideMsg"
    },
    "560": {
        msgId: MessageCode.CS_GrandPrixCatch_Res,
        name: "CS_GrandPrixCatch_Res",
        eventName: "event.CS_GrandPrixCatch_Res"
    },
    "561": {
        msgId: MessageCode.CS_UseDuiHuanCode,
        name: "CS_UseDuiHuanCode",
        eventName: "event.CS_UseDuiHuanCode"
    },
    "562": {
        msgId: MessageCode.CS_UseDuiHuanCode_Res,
        name: "CS_UseDuiHuanCode_Res",
        eventName: "event.CS_UseDuiHuanCode_Res"
    },
    "563": {
        msgId: MessageCode.SC_ActivityStateMsg,        
        name: "SC_ActivityStateMsg",
        eventName: "event.SC_ActivityStateMsg"
    },
    "564": {
        msgId: MessageCode.SC_UpdateHegemonyRankMsg,        
        name: "SC_UpdateHegemonyRankMsg",
        eventName: "event.SC_UpdateHegemonyRankMsg"
    },
    "566": {
        msgId: MessageCode.CS_ExchangePrizesMsg_Res,
        name: "CS_ExchangePrizesMsg_Res",
        eventName: "event.CS_ExchangePrizesMsg_Res"
    },
    "567": {
        msgId: MessageCode.CH_ReturnLoginMsg,
        name: "CH_ReturnLoginMsg",
        eventName: "event.CH_ReturnLoginMsg"
    },
    "569": {
        msgId: MessageCode.SC_JoinCommHegemonyInfoMsg,        
        name: "SC_JoinCommHegemonyInfoMsg",
        eventName: "event.SC_JoinCommHegemonyInfoMsg"
    },
    "570": {
        msgId: MessageCode.CS_GetFirstRewardMsg,
        name: "CH_GetFirstRewardMsg",
        eventName: "event.CH_GetFirstRewardMsg"
    },
    "571": {
        msgId: MessageCode.SC_GetFirstRewardMsg_Res,
        name: "HC_GetFirstRewardMsg_Res",
        eventName: "event.HC_GetFirstRewardMsg_Res"
    },
    "572": {
        msgId: MessageCode.CS_ShowBulletRank,
        name: "CS_ShowBulletRank",
        eventName: "event.CS_ShowBulletRank"
    },
    "573": {
        msgId: MessageCode.DG_PlayerRankInfo,
        name: "DG_PlayerRankInfo",
        eventName: "event.DG_PlayerRankInfo"
    },
    "574": {
        msgId: MessageCode.CH_AskEmailRecord,
        name: "CH_AskEmailRecord",
        eventName: "event.CH_AskEmailRecord"
    },
    "575": {
        msgId: MessageCode.CH_AskEmailRecordMsg_Res,
        name: "CH_AskEmailRecordMsg_Res",
        eventName: "event.CH_AskEmailRecordMsg_Res"
    },
    "576": {
        msgId: MessageCode.SC_OpenLzMsg,
        name: "SC_OpenLzMsg",
        eventName: "event.SC_OpenLzMsg"
    },

    "577": {
        msgId: MessageCode.CS_ShowLzRank,
        name: "CS_ShowLzRank",
        eventName: "event.CS_ShowLzRank"
    },
    "578": {
        msgId: MessageCode.CS_ShowLzRank_Res,
        name: "CS_ShowLzRank_Res",
        eventName: "event.CS_ShowLzRank_Res"
    },
    "579": {
        msgId: MessageCode.CS_AskShangXiaFen,
        name: "CS_AskShangXiaFen",
        eventName: "event.CS_AskShangXiaFen"
    },
    "580": {
        msgId: MessageCode.CS_AskShangXiaFen_Res,
        name: "CS_AskShangXiaFen_Res",
        eventName: "event.CS_AskShangXiaFen_Res"
    },
    "581": {
        msgId: MessageCode.CH_AskRoomLog,
        name: "CH_AskRoomLog",
        eventName: "event.CH_AskRoomLog"
    },
    "582": {
        msgId: MessageCode.CH_AskRoomLog_Res,
        name: "CH_AskRoomLog_Res",
        eventName: "event.CH_AskRoomLog_Res"
    },
    "583": {
        msgId: MessageCode.CH_AskCatchFishLog,
        name: "CH_AskCatchFishLog",
        eventName: "event.CH_AskCatchFishLog"
    },
    "584": {
        msgId: MessageCode.CH_AskCatchFishLog_Res,
        name: "CH_AskCatchFishLog_Res",
        eventName: "event.CH_AskCatchFishLog_Res"
    },
    "585": {
        msgId: MessageCode.CH_AskActivityAwardLog,
        name: "CH_AskActivityAwardLog",
        eventName: "event.CH_AskActivityAwardLog"
    },
    "586": {
        msgId: MessageCode.CH_AskActivityAwardLog_Res,
        name: "CH_AskActivityAwardLog_Res",
        eventName: "event.CH_AskActivityAwardLog_Res"
    },
    "587": {
        msgId: MessageCode.CH_JP_log,
        name: "CH_JP_log",
        eventName: "event.CH_JP_log"
    },
    "588": {
        msgId: MessageCode.CH_JP_log_Res,
        name: "CH_JP_log_Res",
        eventName: "event.CH_JP_log_Res"
    },
    "620": {
        msgId: MessageCode.SM_AllServerChat,
        name: "SM_AllServerChat",
        eventName: "event.SM_AllServerChat"
    },
    "623": {
        msgId: MessageCode.SM_EventNotice,
        name: "SM_EventNotice",
        eventName: "event.SM_EventNotice"
    },
    "701": {
        msgId: MessageCode.SC_ErrorMsg,
        name: "SC_ErrorMsg",
        eventName: "event.SC_ErrorMsg"
    },
    "901":{
        msgId: MessageCode.CS_AllEmailMsg,
        name: "CS_AllEmailMsg",
        eventName: "event.CS_AllEmailMsg"
    },
    "902":{
        msgId: MessageCode.CS_AllEmailMsg_Res,
        name: "CS_AllEmailMsg_Res",
        eventName: "event.CS_AllEmailMsg_Res"
    },
    "903":{
        msgId: MessageCode.CS_EmailReadMsg,
        name: "CS_EmailReadMsg",
        eventName: "event.CS_EmailReadMsg"
    },
    "904":{
        msgId: MessageCode.CS_EmailReadMsg_Res,
        name: "CS_EmailReadMsg_Res",
        eventName: "event.CS_EmailReadMsg_Res"
    },
    "905":{
        msgId: MessageCode.CS_EmailGetPropMsg,
        name: "CS_EmailGetPropMsg",
        eventName: "event.CS_EmailGetPropMsg"
    },
    "906":{
        msgId: MessageCode.CS_EmailGetPropMsg_Res,
        name: "CS_EmailGetPropMsg_Res",
        eventName: "event.CS_EmailGetPropMsg_Res"
    },
    "907":{
        msgId: MessageCode.CS_playerIsExistsMsg,
        name: "CS_playerIsExistsMsg",
        eventName: "event.CS_playerIsExistsMsg"
    },
    "908":{
        msgId: MessageCode.CS_playerIsExistsMsg_Res,
        name: "CS_playerIsExistsMsg_Res",
        eventName: "event.CS_playerIsExistsMsg_Res"
    },
    "909":{
        msgId: MessageCode.CS_FriendsGiftsMsg,
        name: "CS_FriendsGiftsMsg",
        eventName: "event.CS_FriendsGiftsMsg"
    },
    "910":{
        msgId: MessageCode.CS_FriendsGiftsMsg_Res,
        name: "CS_FriendsGiftsMsg_Res",
        eventName: "event.CS_FriendsGiftsMsg_Res"
    },
    "911":{
        msgId: MessageCode.SC_AskPlayerGetEmailMsg,
        name: "SC_AskPlayerGetEmailMsg",
        eventName: "event.SC_AskPlayerGetEmailMsg"
    },
    "1002": {
        msgId: MessageCode.CS_SkillUseMsg,
        name: "CS_SkillUseMsg",
        eventName: "event.CS_SkillUseMsg"
    },
    "1003": {
        msgId: MessageCode.CS_SkillUseMsg_Res,
        name: "SC_SkillUseMsg",
        eventName: "event.CS_SkillUseMsg_Res"
    },
    "1004": {
        msgId: MessageCode.CS_SkillLockMsg,
        name: "CS_SkillLockMsg",
        eventName: "event.CS_SkillLockMsg"
    },
    "1005": {
        msgId: MessageCode.CS_SkillLockMsg_Res,
        name: "FishInfo_Room",
        eventName: "event.CS_SkillLockMsg_Res"
    },
    "1006": {
        msgId: MessageCode.SC_IceEnd,
        name: "SC_SkillEndMsg",
        eventName: "event.SC_IceEnd"
    },
    "1201":{
        msgId: MessageCode.CS_AskPaymentMsg,
        name: "CS_AskPaymentMsg",
        eventName: "event.CS_AskPaymentMsg"
    },
    "1202": {
        msgId: MessageCode.CS_AskPaymentMsg_Res,
        name: "CS_AskPaymentMsg_Res",
        eventName: "event.CS_AskPaymentMsg_Res"
    },
    "1203": {
        msgId: MessageCode.CH_InHallReceiveOrderItemMsg,
        name: "CH_InHallReceiveOrderItemMsg",
        eventName: "event.CH_InHallReceiveOrderItemMsg"
    },
    "1204": {
        msgId: MessageCode.CH_InHallReceiveOrderItemMsg_Res,
        name: "CH_InHallReceiveOrderItemMsg_Res",
        eventName: "event.CH_InHallReceiveOrderItemMsg_Res"
    },
    "1205": {
        msgId: MessageCode.CS_InRoomReceiveOrderItemMsg,
        name: "CS_InRoomReceiveOrderItemMsg",
        eventName: "event.CS_InRoomReceiveOrderItemMsg"
    },
    "1206": {
        msgId: MessageCode.CS_InRoomReceiveOrderItemMsg_Res,
        name: "CS_InRoomReceiveOrderItemMsg_Res",
        eventName: "event.CS_InRoomReceiveOrderItemMsg_Res"
    },
    "1208": {
        msgId: MessageCode.CS_AskAllNotReceiveOrderMsg,
        name: "CS_AskAllNotReceiveOrderMsg",
        eventName: "event.CS_AskAllNotReceiveOrderMsg"
    },
    "1209": {
        msgId: MessageCode.CS_AskAllNotReceiveOrderMsg_Res,
        name: "CS_AskAllNotReceiveOrderMsg_Res",
        eventName: "event.CS_AskAllNotReceiveOrderMsg_Res"
    },
    "1210": {
        msgId: MessageCode.CH_InHallCheckAndReceiveOrderMsg,
        name: "CH_InHallCheckAndReceiveOrderMsg",
        eventName: "event.CH_InHallCheckAndReceiveOrderMsg"
    },
    "1211": {
        msgId: MessageCode.CH_InRoomCheckAndReceiveOrderMsg,
        name: "CH_InRoomCheckAndReceiveOrderMsg",
        eventName: "event.CH_InRoomCheckAndReceiveOrderMsg"
    },
    "1401": {
        msgId: MessageCode.CS_ShangZhuangMsg,
        name: "CS_ShangZhuangMsg",
        eventName: "event.CS_ShangZhuangMsg"
    },
    "1402": {
        msgId: MessageCode.CS_ShangZhuangMsg_Res,
        name: "CS_ShangZhuangMsg_Res",
        eventName: "event.CS_ShangZhuangMsg_Res"
    },
    "1403": {
        msgId: MessageCode.CS_XiaZhuangMsg,
        name: "CS_XiaZhuangMsg",
        eventName: "event.CS_XiaZhuangMsg"
    },
    "1404": {
        msgId: MessageCode.CS_XiaZhuangMsg_Res,
        name: "CS_XiaZhuangMsg_Res",
        eventName: "event.CS_XiaZhuangMsg_Res"
    },
    "1405": {
        msgId: MessageCode.CS_XiaZhuMsg,
        name: "CS_XiaZhuMsg",
        eventName: "event.CS_XiaZhuMsg"
    },
    "1406": {
        msgId: MessageCode.CS_XiaZhuMsg_Res,
        name: "CS_XiaZhuMsg_Res",
        eventName: "event.CS_XiaZhuMsg_Res"
    },
    "1407": {
        msgId: MessageCode.SC_PlayerXiaZhuMsg,
        name: "SC_PlayerXiaZhuMsg",
        eventName: "event.SC_PlayerXiaZhuMsg"
    },
    "1408": {
        msgId: MessageCode.SC_PlayerZuoZhuangMsg,
        name: "SC_PlayerZuoZhuangMsg",
        eventName: "event.SC_PlayerZuoZhuangMsg"
    },
    "1409": {
        msgId: MessageCode.SC_StateChangeMsg,
        name: "SC_StateChangeMsg",
        eventName: "event.SC_StateChangeMsg"
    },
    "1410": {
        msgId: MessageCode.SC_XiaZhuInfoMsg,
        name: "SC_XiaZhuInfoMsg",
        eventName: "event.SC_XiaZhuInfoMsg"
    },
    "1411": {
        msgId: MessageCode.SC_Result,
        name: "SC_Result",
        eventName: "event.SC_Result"
    },
    "1412": {
        msgId: MessageCode.CS_JoinDTStageMsg_Res,
        name: "CS_JoinDTStageMsg_Res",
        eventName: "event.CS_JoinDTStageMsg_Res"
    },
    "1413": {
        msgId: MessageCode.CS_AskShangZhuangListMsg,
        name: "CS_AskShangZhuangListMsg",
        eventName: "event.CS_AskShangZhuangListMsg"
    },
    "1414": {
        msgId: MessageCode.CS_AskShangZhuangListMsg_Res,
        name: "CS_AskShangZhuangListMsg_Res",
        eventName: "event.CS_AskShangZhuangListMsg_Res"
    },
    "1415": {
        msgId: MessageCode.CS_AskPlayerListMsg,
        name: "CS_AskPlayerListMsg",
        eventName: "event.CS_AskPlayerListMsg"
    },
    "1416": {
        msgId: MessageCode.CS_AskPlayerListMsg_Res,
        name: "CS_AskPlayerListMsg_Res",
        eventName: "event.CS_AskPlayerListMsg_Res"
    },
    "65499": {
        msgId: MessageCode.KeepAlive_Res,
        name: "KeepAlive_Res",
        eventName: "event.KeepAlive_Res"
    }
};