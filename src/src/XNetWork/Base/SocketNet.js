/**
 * Created by beiping on 2016/10/20.
 */
var WebSocket = WebSocket || window.WebSocket || window.MozWebSocket;

/**
 * Socket对象类
 */
var SocketNet = cc.Class.extend({
    _socket: null,           //< socket对象
    _url: null,              //< 服务器地址
    _handler: null,          //< 回调函数
    _state: null,            //< socket状态

    /**
     * 初始化
     * @param handler 回调函数
     */
    ctor: function (handler) {
        this._handler = handler;
        this._state = SocketNet.State.ON_CLOSE;

        if (handler && typeof handler != 'function') {
            cc.error("HttpNet:传入的handler不是function");
        }
    },


    /**
     * 连接
     * @param ip 服务器i地址
     * @param port 服务器端口号
     * @param callback 连接成功回调
     */
    connect: function (ip, port, callback) {
        var self = this;

        self.setUrl(ip, port);
        if (self._socket == null) {
            self._socket = new WebSocket(self._url);
        }
        self._socket.binaryType = "arraybuffer";

        //打开
        self._socket.onopen = function (event) {
            cc.log("socket连接成功");
            self._state = SocketNet.State.ON_OPEN;
            if (self._handler) {
                self._handler(self._state, event.data);
            }

            //连接成功回调
            if (callback && typeof callback == 'function') {
                callback();
            }
        };

        //收到数据
        self._socket.onmessage = function (event) {
            //传递消息给解析handler
            self._state = SocketNet.State.ON_MESSAGE;
            if (self._handler) {
                self._handler(self._state, event.data);
            }
        };

        //错误
        self._socket.onerror = function (event) {
            cc.error("socket错误");
            self._state = SocketNet.State.ON_ERROR;

            if (self._handler) {
                self._handler(self._state, "error");
            }
        };

        //关闭
        self._socket.onclose = function (event) {
            var reason = event.reason;
            var wasClean = event.wasClean;

            if(wasClean){
                cc.error("服务器主动关闭socket");
            }else{
                cc.error("其他原因关闭socket");
                reason = "false";
            }
            self._state = SocketNet.State.ON_CLOSE;
            self._socket = null;

            if (self._handler) {
                self._handler(self._state, reason);
            }
        };
    },

    /**
     * 关闭Socket
     */
    close: function () {
        var self = this;
        if (self._socket) {
            self._socket.close();
            self._socket = null;
        }
    },

    /**
     * 发送消息
     * @param msg 消息
     */
    send: function (msg) {
        var self = this;
        if (self._socket) {
            self._socket.send(msg);
        } else {
            cc.error("socket发送消息错误");
        }
    },

    /**
     * 重连
     */
    reConnect: function () {
        this.close();
        this.connect();
    },

    /**
     * 设置url
     * @param ip 服务器ip地址
     * @param port 服务器端口号
     */
    setUrl: function (ip, port) {
        if (ip && port) {
            //this._url = "ws://" + ip + ":" + port + "/websocket";
            //this._url = "wss://" + ip + ":" + port + "/websocket";
            switch (parseInt(GameConfig.serverIdx)){
                case 0://正式服
                    this._url = "wss://" + ip + "/websocket";
                    break;
                case 1://测试服
                    this._url = "wss://" + ip + "/websocket";
                    break;
                case 2://本地测试
                    this._url = "ws://" + ip +":"+port+ "/websocket";
                    break;
            }

        } else {
            cc.error("socket：IP和端口号错误");
        }
    }
});

/**
 * Socket状态
 */
SocketNet.State = {
    ON_OPEN: 0,             //< 打开状态
    ON_MESSAGE: 1,          //< 接收状态
    ON_ERROR: 2,            //< 错误状态
    ON_CLOSE: 3             //< 关闭状态  
};

/**
 * 创建Socket连接
 * @param handler
 */
SocketNet.create = function (handler) {
    return new SocketNet(handler);
};