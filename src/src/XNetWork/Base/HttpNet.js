var HttpNet = cc.Class.extend({
    _url: null,
    _http: null,
    _handler: null,

    /**
     * 初始化
     * @param url
     * @param handler 回调函数
     * @returns {boolean}
     */
    ctor: function (url, handler) {
        this._url = url;
        this._handler = handler;

        if (handler && typeof handler != 'function') {
            cc.error("HttpNet:传入的handler不是function");
        }
    },

    /**
     * 设置Url
     * @param url
     */
    setUrl: function (url) {
        this._url = url;
    },

    /**
     * 发送消息
     * @param msg
     * @param type
     */
    send: function (msg,type) {
        try {
            if (msg) {
                switch (type){
                    case HttpNet.Type.text:
                        this._get(msg);
                        break;
                    case HttpNet.Type.ArrayBuffer:
                        this._post(msg);
                        break;
                }

            } else {
                //
            }
        }catch (e){

        }
    },

    /**
     * POST消息
     * @param msg
     * @private
     */
    _post: function (msg) {
        var self = this;

        self._http = cc.loader.getXMLHttpRequest();
        self._http.open("POST", self._url, true);
        self._http.responseType = "arraybuffer";
        self._http.onreadystatechange = function () {
            self._readyCallBack(self._http);
        };
        self._http.send(msg);
    },

    /**
     *  GET消息
     * @private
     */
    _get: function (msg) {
        var self = this;

        /*self._http = cc.loader.getXMLHttpRequest();
        self._http.open("POST", self._url, true);
        self._http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        self._http.responseType = "arraybuffer";
        self._http.onreadystatechange = function () {
            self._readyCallBack(self._http);
        };
        self._http.send(msg);*/

        self._http = cc.loader.getXMLHttpRequest();
        self._http.open("GET", self._url+"?"+msg, true);
        self._http.setRequestHeader("Content-Type", "text/plain");
        self._http.responseType = "arraybuffer";
        self._http.onreadystatechange = function () {
            self._readyCallBack(self._http);
        };
        self._http.send();
    },

    /**
     * http响应回调函数
     * @param xhr
     * @private
     */
    _readyCallBack: function (xhr) {
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status <= 207) {
                if (xhr.response) {
                    this._handler(xhr.response);
                }
            } else {
                cc.error("网络错误");
            }
        }
    }
});

/**
 * 创建http链接
 * @param url
 * @param handler
 */
HttpNet.create = function (url, handler) {
    return new HttpNet(url, handler);
};

HttpNet.Type = {
    text:0, //字符串
    ArrayBuffer:1   //二级制数组
};