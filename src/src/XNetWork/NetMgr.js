cf.NetMgr = {

    _httpNet: null,         //< Http对象
    _socketNet: null,        //< Socket对象

    _bCloseSocket: false,
    _heartBeatTimer: null,
    _lastHeartTime: 0,
    _bSendMsg: false,

    /**
     * 初始化
     */
    init: function () {

        var self = this;
        //创建http链接
        self._httpNet = HttpNet.create(cf.HTTP_URL, this._parseHttpHandler.bind(this));

        //创建Socket
        self._socketNet = SocketNet.create(this._parseSocketHandler.bind(this));
    },

    /**
     * 发送Http消息
     * @param msgId
     * @param msg
     * @param type
     */
    sendHttpMsg: function (msgId, msg, type) {
        if(type == undefined)
            type = HttpNet.Type.ArrayBuffer;
        
        var self = this;
        if (self._httpNet && msg) {
            cc.log("sendHttpMsg msdId = " + msgId);
            switch (type){
                case HttpNet.Type.text:
                    self._httpNet.send(msg,type);
                    break;
                case HttpNet.Type.ArrayBuffer:
                    self._httpNet.send(self._writeMsg(msgId, msg),type);
                    break;
            }
        } else {
            cc.error("sendHttpMsg错误：msgId = " + msgId);
        }
    },

    /**
     * 发送Socket消息
     * @param msgId 消息id
     * @param msg 消息
     */
    sendSocketMsg: function (msgId, msg) {
        var self = this;
        if (self._socketNet) {
            cc.log("sendSocketMsg msgId = " + msgId);
            //除了心跳消息，剩余消息均重置移除时间
            if(msgId != MessageCode.KeepAlive && msgId != MessageCode.JP_Total){
                //重置移除时间
                cf.RemoveUserTime = 0;
            }
           
            self._socketNet.send(self._writeMsg(msgId, msg));

            self._bSendMsg = true;
        }
    },

    /**
     * 创建socket连接
     * @param ip 服务器ip地址
     * @param port 服务器端口号
     * @param callback 连接成功回调
     */
    connectSocket: function (ip, port, callback) {
        if (this._socketNet) {
            this._bCloseSocket = false;
            this._socketNet.connect(ip, port, callback);
        }
    },

    /**
     * 重连
     */
    reConnectSocket: function () {
        if (this._socketNet) {
            this._bCloseSocket = false;
            this._socketNet.reConnect();
        }
    },

    /**
     * 关闭scoket连接
     */
    closeSocket: function () {
        if (this._socketNet) {
            this._bCloseSocket = true;
            this._socketNet.close();
        }
    },

    isCloseSocket: function () {
        return this._bCloseSocket;
    },

    /**********************************************************
     *
     * 私有方法
     *
     **********************************************************/
    /**
     * 解析http消息
     * @param msg
     * @private
     */
    _parseHttpHandler: function (msg) {
        //解析消息
        this._readMsg(msg);
    },

    /**
     * 解析socket消息
     * @param state socket状态
     * @param msg 消息
     * @private
     */
    _parseSocketHandler: function (state, msg) {
        var self = this;

        switch (state) {
            case SocketNet.State.ON_OPEN:
                /*self._lastHeartTime = (new Date()).getTime();
                 self._heartBeatTimer = setInterval(function () {
                 if( self._bSendMsg ){
                 var timeout = (new Date()).getTime() - self._lastHeartTime;
                 cc.log("timeout = "+timeout);
                 if( timeout >= 20000 ){
                 self._bSendMsg = false;
                 //self._dispatchEvent(NotificationScene.eventName, NotificationScene.Error.ERROR_SOCKET_TIMEOUT);
                 }
                 }
                 }, 5000);*/
                break;
            case SocketNet.State.ON_MESSAGE:
                //self._lastHeartTime = (new Date()).getTime();
                //self._bSendMsg = false;

                //解析消息
                self._readMsg(msg);
                break;
            case SocketNet.State.ON_ERROR:
                //clearInterval(self._heartBeatTimer);
                self._dispatchEvent(NotificationScene.eventName, {
                    flag: NotificationScene.Error.ERROR_NET_CONNECT,
                    error: msg
                });
                break;
            case SocketNet.State.ON_CLOSE:
                //clearInterval(self._heartBeatTimer);
                self._dispatchEvent(NotificationScene.eventName, {
                    flag: NotificationScene.Error.ERROR_SOCKET_CLOSE,
                    error: msg
                });
                break;
        }
    },

    /**
     * 消息分发
     * @param eventName 事件名称
     * @param msg 消息
     * @private
     */
    _dispatchEvent: function (eventName, msg) {
        var event = new cc.EventCustom(eventName);
        event.setUserData(msg);
        cc.eventManager.dispatchEvent(event);
    },

    /**
     * 读取消息
     * @param msg
     * @returns {*}
     * @private
     */
    _readMsg: function (msg) {
        var self = this;
        //解析消息
        //创建二进制操作试图
        var dataView = new DataView(msg);
        var view = new Uint8Array(dataView.buffer, dataView.byteOffset, dataView.byteLength);
        //返回实际数据，前4字节为约定数据，所以从索引4开始返回
        var data = view.subarray(6, dataView.byteLength);

        //解析消息id
        var msgId = 0;
        for (var i = 0; i < 2; i++) {
            msgId |= view[i + 4] << (8 * (2 - i - 1));
        }
        var config = MessageConfig.Message[msgId];

        cc.log("解析消息 msgId = " + msgId);
        if (config) {
            cc.log("解析消息 name = " + config.name);
            cc.log("解析消息 eventName = " + config.eventName);

            //分发消息
            self._dispatchEvent(config.eventName, data);
        } else {
            cc.error("解析消息错误, msgId = " + msgId);
        }
        return data ? data : null;
    },

    _writeMsg: function (msgId, msg) {
        if (!msgId || !msg) {
            return null;
        }
        //实际包长，加4字节的包长和id
        var byteLength = msg.byteLength + 6;
        //创建新消息视图
        var newView = new Uint8Array(byteLength);
        //添加包长
        newView[0] = 0xff & (byteLength >> 24);
        newView[1] = 0xff & (byteLength >> 16);
        newView[2] = 0xff & (byteLength >> 8);
        newView[3] = 0xff & (byteLength >> 0);
        //添加消息id
        newView[4] = 0xff & (msgId >> 8);
        newView[5] = 0xff & (msgId >> 0);
        //添加原始消息数据，从索引4位置开始
        newView.set(msg, 6);
        
        return newView;
    }

};