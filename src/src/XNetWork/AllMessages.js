var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.CS_AllEmailMsg_Res = (function() {

    function CS_AllEmailMsg_Res(properties) {
        this.emailData = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_AllEmailMsg_Res.prototype.nIdLogin = 0;
    CS_AllEmailMsg_Res.prototype.emailData = $util.emptyArray;

    CS_AllEmailMsg_Res.create = function create(properties) {
        return new CS_AllEmailMsg_Res(properties);
    };

    CS_AllEmailMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        if (message.emailData != null && message.emailData.length)
            for (var i = 0; i < message.emailData.length; ++i)
                $root.SC_NewEmailMsg.encode(message.emailData[i], writer.uint32(18).fork()).ldelim();
        return writer;
    };

    CS_AllEmailMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_AllEmailMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                if (!(message.emailData && message.emailData.length))
                    message.emailData = [];
                message.emailData.push($root.SC_NewEmailMsg.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        return message;
    };

    return CS_AllEmailMsg_Res;
})();

$root.SC_NewEmailMsg = (function() {

    function SC_NewEmailMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_NewEmailMsg.prototype.nEmailId = 0;
    SC_NewEmailMsg.prototype.nSenderId = 0;
    SC_NewEmailMsg.prototype.nReceiverId = 0;
    SC_NewEmailMsg.prototype.strSenderName = "";
    SC_NewEmailMsg.prototype.nEmailType = 0;
    SC_NewEmailMsg.prototype.strSubject = "";
    SC_NewEmailMsg.prototype.strContent = "";
    SC_NewEmailMsg.prototype.strProp = "";
    SC_NewEmailMsg.prototype.strSendTime = "";
    SC_NewEmailMsg.prototype.strCloseTime = "";
    SC_NewEmailMsg.prototype.nflag = 0;

    SC_NewEmailMsg.create = function create(properties) {
        return new SC_NewEmailMsg(properties);
    };

    SC_NewEmailMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nEmailId);
        writer.uint32(16).sint32(message.nSenderId);
        writer.uint32(24).sint32(message.nReceiverId);
        writer.uint32(34).string(message.strSenderName);
        writer.uint32(40).sint32(message.nEmailType);
        if (message.strSubject != null && Object.hasOwnProperty.call(message, "strSubject"))
            writer.uint32(50).string(message.strSubject);
        if (message.strContent != null && Object.hasOwnProperty.call(message, "strContent"))
            writer.uint32(58).string(message.strContent);
        if (message.strProp != null && Object.hasOwnProperty.call(message, "strProp"))
            writer.uint32(66).string(message.strProp);
        writer.uint32(74).string(message.strSendTime);
        writer.uint32(82).string(message.strCloseTime);
        writer.uint32(88).sint32(message.nflag);
        return writer;
    };

    SC_NewEmailMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_NewEmailMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nEmailId = reader.sint32();
                break;
            case 2:
                message.nSenderId = reader.sint32();
                break;
            case 3:
                message.nReceiverId = reader.sint32();
                break;
            case 4:
                message.strSenderName = reader.string();
                break;
            case 5:
                message.nEmailType = reader.sint32();
                break;
            case 6:
                message.strSubject = reader.string();
                break;
            case 7:
                message.strContent = reader.string();
                break;
            case 8:
                message.strProp = reader.string();
                break;
            case 9:
                message.strSendTime = reader.string();
                break;
            case 10:
                message.strCloseTime = reader.string();
                break;
            case 11:
                message.nflag = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nEmailId"))
            throw $util.ProtocolError("missing required 'nEmailId'", { instance: message });
        if (!message.hasOwnProperty("nSenderId"))
            throw $util.ProtocolError("missing required 'nSenderId'", { instance: message });
        if (!message.hasOwnProperty("nReceiverId"))
            throw $util.ProtocolError("missing required 'nReceiverId'", { instance: message });
        if (!message.hasOwnProperty("strSenderName"))
            throw $util.ProtocolError("missing required 'strSenderName'", { instance: message });
        if (!message.hasOwnProperty("nEmailType"))
            throw $util.ProtocolError("missing required 'nEmailType'", { instance: message });
        if (!message.hasOwnProperty("strSendTime"))
            throw $util.ProtocolError("missing required 'strSendTime'", { instance: message });
        if (!message.hasOwnProperty("strCloseTime"))
            throw $util.ProtocolError("missing required 'strCloseTime'", { instance: message });
        if (!message.hasOwnProperty("nflag"))
            throw $util.ProtocolError("missing required 'nflag'", { instance: message });
        return message;
    };

    return SC_NewEmailMsg;
})();

$root.CS_AllEmailMsg = (function() {

    function CS_AllEmailMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_AllEmailMsg.prototype.nIdLogin = 0;
    CS_AllEmailMsg.prototype.strSafeCode = "";
    CS_AllEmailMsg.prototype.nStartId = 0;

    CS_AllEmailMsg.create = function create(properties) {
        return new CS_AllEmailMsg(properties);
    };

    CS_AllEmailMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nStartId);
        return writer;
    };

    CS_AllEmailMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_AllEmailMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nStartId = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nStartId"))
            throw $util.ProtocolError("missing required 'nStartId'", { instance: message });
        return message;
    };

    return CS_AllEmailMsg;
})();

$root.CS_EmailGetPropMsg_Res = (function() {

    function CS_EmailGetPropMsg_Res(properties) {
        this.oneEmailProp = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_EmailGetPropMsg_Res.prototype.nIdLogin = 0;
    CS_EmailGetPropMsg_Res.prototype.oneEmailProp = $util.emptyArray;

    CS_EmailGetPropMsg_Res.create = function create(properties) {
        return new CS_EmailGetPropMsg_Res(properties);
    };

    CS_EmailGetPropMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        if (message.oneEmailProp != null && message.oneEmailProp.length)
            for (var i = 0; i < message.oneEmailProp.length; ++i)
                $root.OneEmailProp.encode(message.oneEmailProp[i], writer.uint32(18).fork()).ldelim();
        return writer;
    };

    CS_EmailGetPropMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_EmailGetPropMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                if (!(message.oneEmailProp && message.oneEmailProp.length))
                    message.oneEmailProp = [];
                message.oneEmailProp.push($root.OneEmailProp.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        return message;
    };

    return CS_EmailGetPropMsg_Res;
})();

$root.OneEmailProp = (function() {

    function OneEmailProp(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    OneEmailProp.prototype.nEmailId = 0;
    OneEmailProp.prototype.nEmailType = 0;
    OneEmailProp.prototype.nGetRes = 0;
    OneEmailProp.prototype.itemReward = $util.emptyArray;

    OneEmailProp.create = function create(properties) {
        return new OneEmailProp(properties);
    };

    OneEmailProp.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nEmailId);
        writer.uint32(16).sint32(message.nEmailType);
        writer.uint32(24).sint32(message.nGetRes);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(34).fork()).ldelim();
        return writer;
    };

    OneEmailProp.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.OneEmailProp();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nEmailId = reader.sint32();
                break;
            case 2:
                message.nEmailType = reader.sint32();
                break;
            case 3:
                message.nGetRes = reader.sint32();
                break;
            case 4:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nEmailId"))
            throw $util.ProtocolError("missing required 'nEmailId'", { instance: message });
        if (!message.hasOwnProperty("nEmailType"))
            throw $util.ProtocolError("missing required 'nEmailType'", { instance: message });
        if (!message.hasOwnProperty("nGetRes"))
            throw $util.ProtocolError("missing required 'nGetRes'", { instance: message });
        return message;
    };

    return OneEmailProp;
})();

$root.CS_EmailGetPropMsg = (function() {

    function CS_EmailGetPropMsg(properties) {
        this.nEmailId = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_EmailGetPropMsg.prototype.nIdLogin = 0;
    CS_EmailGetPropMsg.prototype.strSafeCode = "";
    CS_EmailGetPropMsg.prototype.nEmailId = $util.emptyArray;

    CS_EmailGetPropMsg.create = function create(properties) {
        return new CS_EmailGetPropMsg(properties);
    };

    CS_EmailGetPropMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        if (message.nEmailId != null && message.nEmailId.length)
            for (var i = 0; i < message.nEmailId.length; ++i)
                writer.uint32(24).sint32(message.nEmailId[i]);
        return writer;
    };

    CS_EmailGetPropMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_EmailGetPropMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                if (!(message.nEmailId && message.nEmailId.length))
                    message.nEmailId = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.nEmailId.push(reader.sint32());
                } else
                    message.nEmailId.push(reader.sint32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CS_EmailGetPropMsg;
})();

$root.CS_EmailReadMsg_Res = (function() {

    function CS_EmailReadMsg_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_EmailReadMsg_Res.prototype.nIdLogin = 0;
    CS_EmailReadMsg_Res.prototype.nEmailId = 0;
    CS_EmailReadMsg_Res.prototype.nReadRes = 0;
    CS_EmailReadMsg_Res.prototype.strCloseTime = "";

    CS_EmailReadMsg_Res.create = function create(properties) {
        return new CS_EmailReadMsg_Res(properties);
    };

    CS_EmailReadMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(16).sint32(message.nEmailId);
        writer.uint32(24).sint32(message.nReadRes);
        if (message.strCloseTime != null && Object.hasOwnProperty.call(message, "strCloseTime"))
            writer.uint32(34).string(message.strCloseTime);
        return writer;
    };

    CS_EmailReadMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_EmailReadMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.nEmailId = reader.sint32();
                break;
            case 3:
                message.nReadRes = reader.sint32();
                break;
            case 4:
                message.strCloseTime = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("nEmailId"))
            throw $util.ProtocolError("missing required 'nEmailId'", { instance: message });
        if (!message.hasOwnProperty("nReadRes"))
            throw $util.ProtocolError("missing required 'nReadRes'", { instance: message });
        return message;
    };

    return CS_EmailReadMsg_Res;
})();

$root.CS_EmailReadMsg = (function() {

    function CS_EmailReadMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_EmailReadMsg.prototype.nIdLogin = 0;
    CS_EmailReadMsg.prototype.strSafeCode = "";
    CS_EmailReadMsg.prototype.nEmailId = 0;

    CS_EmailReadMsg.create = function create(properties) {
        return new CS_EmailReadMsg(properties);
    };

    CS_EmailReadMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nEmailId);
        return writer;
    };

    CS_EmailReadMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_EmailReadMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nEmailId = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nEmailId"))
            throw $util.ProtocolError("missing required 'nEmailId'", { instance: message });
        return message;
    };

    return CS_EmailReadMsg;
})();

$root.CS_FriendsGiftsMsg_Res = (function() {

    function CS_FriendsGiftsMsg_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_FriendsGiftsMsg_Res.prototype.nIdLogin = 0;
    CS_FriendsGiftsMsg_Res.prototype.itemReward = $util.emptyArray;

    CS_FriendsGiftsMsg_Res.create = function create(properties) {
        return new CS_FriendsGiftsMsg_Res(properties);
    };

    CS_FriendsGiftsMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(26).fork()).ldelim();
        return writer;
    };

    CS_FriendsGiftsMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_FriendsGiftsMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 3:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        return message;
    };

    return CS_FriendsGiftsMsg_Res;
})();

$root.CS_FriendsGiftsMsg = (function() {

    function CS_FriendsGiftsMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_FriendsGiftsMsg.prototype.nIdLogin = 0;
    CS_FriendsGiftsMsg.prototype.strSafeCode = "";
    CS_FriendsGiftsMsg.prototype.nReceiverId = 0;
    CS_FriendsGiftsMsg.prototype.strSubject = "";
    CS_FriendsGiftsMsg.prototype.strContent = "";
    CS_FriendsGiftsMsg.prototype.strProp = "";

    CS_FriendsGiftsMsg.create = function create(properties) {
        return new CS_FriendsGiftsMsg(properties);
    };

    CS_FriendsGiftsMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nReceiverId);
        if (message.strSubject != null && Object.hasOwnProperty.call(message, "strSubject"))
            writer.uint32(34).string(message.strSubject);
        if (message.strContent != null && Object.hasOwnProperty.call(message, "strContent"))
            writer.uint32(42).string(message.strContent);
        writer.uint32(50).string(message.strProp);
        return writer;
    };

    CS_FriendsGiftsMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_FriendsGiftsMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nReceiverId = reader.sint32();
                break;
            case 4:
                message.strSubject = reader.string();
                break;
            case 5:
                message.strContent = reader.string();
                break;
            case 6:
                message.strProp = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nReceiverId"))
            throw $util.ProtocolError("missing required 'nReceiverId'", { instance: message });
        if (!message.hasOwnProperty("strProp"))
            throw $util.ProtocolError("missing required 'strProp'", { instance: message });
        return message;
    };

    return CS_FriendsGiftsMsg;
})();

$root.CS_playerIsExistsMsg_Res = (function() {

    function CS_playerIsExistsMsg_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_playerIsExistsMsg_Res.prototype.nIdLogin = 0;
    CS_playerIsExistsMsg_Res.prototype.nSendRes = 0;

    CS_playerIsExistsMsg_Res.create = function create(properties) {
        return new CS_playerIsExistsMsg_Res(properties);
    };

    CS_playerIsExistsMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(16).sint32(message.nSendRes);
        return writer;
    };

    CS_playerIsExistsMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_playerIsExistsMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.nSendRes = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("nSendRes"))
            throw $util.ProtocolError("missing required 'nSendRes'", { instance: message });
        return message;
    };

    return CS_playerIsExistsMsg_Res;
})();

$root.CS_playerIsExistsMsg = (function() {

    function CS_playerIsExistsMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_playerIsExistsMsg.prototype.nIdLogin = 0;
    CS_playerIsExistsMsg.prototype.strSafeCode = "";
    CS_playerIsExistsMsg.prototype.nOtherPlayerId = 0;

    CS_playerIsExistsMsg.create = function create(properties) {
        return new CS_playerIsExistsMsg(properties);
    };

    CS_playerIsExistsMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nOtherPlayerId);
        return writer;
    };

    CS_playerIsExistsMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_playerIsExistsMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nOtherPlayerId = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nOtherPlayerId"))
            throw $util.ProtocolError("missing required 'nOtherPlayerId'", { instance: message });
        return message;
    };

    return CS_playerIsExistsMsg;
})();

$root.SC_ErrorMsg = (function() {

    function SC_ErrorMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_ErrorMsg.prototype.nError = 0;
    SC_ErrorMsg.prototype.strIdentity = "";

    SC_ErrorMsg.create = function create(properties) {
        return new SC_ErrorMsg(properties);
    };

    SC_ErrorMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nError);
        if (message.strIdentity != null && Object.hasOwnProperty.call(message, "strIdentity"))
            writer.uint32(18).string(message.strIdentity);
        return writer;
    };

    SC_ErrorMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_ErrorMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nError = reader.sint32();
                break;
            case 2:
                message.strIdentity = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nError"))
            throw $util.ProtocolError("missing required 'nError'", { instance: message });
        return message;
    };

    return SC_ErrorMsg;
})();

$root.ActVersions = (function() {

    function ActVersions(properties) {
        this.strRuleTime = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    ActVersions.prototype.nActID = 0;
    ActVersions.prototype.nVersion = 0;
    ActVersions.prototype.strRuleTime = $util.emptyArray;

    ActVersions.create = function create(properties) {
        return new ActVersions(properties);
    };

    ActVersions.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nActID);
        writer.uint32(16).sint32(message.nVersion);
        if (message.strRuleTime != null && message.strRuleTime.length)
            for (var i = 0; i < message.strRuleTime.length; ++i)
                writer.uint32(26).string(message.strRuleTime[i]);
        return writer;
    };

    ActVersions.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ActVersions();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nActID = reader.sint32();
                break;
            case 2:
                message.nVersion = reader.sint32();
                break;
            case 3:
                if (!(message.strRuleTime && message.strRuleTime.length))
                    message.strRuleTime = [];
                message.strRuleTime.push(reader.string());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nActID"))
            throw $util.ProtocolError("missing required 'nActID'", { instance: message });
        if (!message.hasOwnProperty("nVersion"))
            throw $util.ProtocolError("missing required 'nVersion'", { instance: message });
        return message;
    };

    return ActVersions;
})();

$root.BulletInfo_Room = (function() {

    function BulletInfo_Room(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    BulletInfo_Room.prototype.strBulletID = "";
    BulletInfo_Room.prototype.nBulletLevel = 0;
    BulletInfo_Room.prototype.nBulletValue = 0;
    BulletInfo_Room.prototype.fAngle = 0;
    BulletInfo_Room.prototype.nFortPosID = 0;
    BulletInfo_Room.prototype.strTime = "";

    BulletInfo_Room.create = function create(properties) {
        return new BulletInfo_Room(properties);
    };

    BulletInfo_Room.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strBulletID);
        writer.uint32(16).sint32(message.nBulletLevel);
        writer.uint32(24).sint32(message.nBulletValue);
        writer.uint32(37).float(message.fAngle);
        writer.uint32(40).sint32(message.nFortPosID);
        writer.uint32(50).string(message.strTime);
        return writer;
    };

    BulletInfo_Room.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.BulletInfo_Room();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strBulletID = reader.string();
                break;
            case 2:
                message.nBulletLevel = reader.sint32();
                break;
            case 3:
                message.nBulletValue = reader.sint32();
                break;
            case 4:
                message.fAngle = reader.float();
                break;
            case 5:
                message.nFortPosID = reader.sint32();
                break;
            case 6:
                message.strTime = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strBulletID"))
            throw $util.ProtocolError("missing required 'strBulletID'", { instance: message });
        if (!message.hasOwnProperty("nBulletLevel"))
            throw $util.ProtocolError("missing required 'nBulletLevel'", { instance: message });
        if (!message.hasOwnProperty("nBulletValue"))
            throw $util.ProtocolError("missing required 'nBulletValue'", { instance: message });
        if (!message.hasOwnProperty("fAngle"))
            throw $util.ProtocolError("missing required 'fAngle'", { instance: message });
        if (!message.hasOwnProperty("nFortPosID"))
            throw $util.ProtocolError("missing required 'nFortPosID'", { instance: message });
        if (!message.hasOwnProperty("strTime"))
            throw $util.ProtocolError("missing required 'strTime'", { instance: message });
        return message;
    };

    return BulletInfo_Room;
})();

$root.CatchFishRecord = (function() {

    function CatchFishRecord(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CatchFishRecord.prototype.strOrderID = "";
    CatchFishRecord.prototype.strTime = "";
    CatchFishRecord.prototype.nBulletValue = 0;
    CatchFishRecord.prototype.nGet = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CatchFishRecord.prototype.nFishID = 0;

    CatchFishRecord.create = function create(properties) {
        return new CatchFishRecord(properties);
    };

    CatchFishRecord.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strOrderID);
        writer.uint32(18).string(message.strTime);
        writer.uint32(24).sint32(message.nBulletValue);
        writer.uint32(32).sint64(message.nGet);
        writer.uint32(40).sint32(message.nFishID);
        return writer;
    };

    CatchFishRecord.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CatchFishRecord();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strOrderID = reader.string();
                break;
            case 2:
                message.strTime = reader.string();
                break;
            case 3:
                message.nBulletValue = reader.sint32();
                break;
            case 4:
                message.nGet = reader.sint64();
                break;
            case 5:
                message.nFishID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strOrderID"))
            throw $util.ProtocolError("missing required 'strOrderID'", { instance: message });
        if (!message.hasOwnProperty("strTime"))
            throw $util.ProtocolError("missing required 'strTime'", { instance: message });
        if (!message.hasOwnProperty("nBulletValue"))
            throw $util.ProtocolError("missing required 'nBulletValue'", { instance: message });
        if (!message.hasOwnProperty("nGet"))
            throw $util.ProtocolError("missing required 'nGet'", { instance: message });
        if (!message.hasOwnProperty("nFishID"))
            throw $util.ProtocolError("missing required 'nFishID'", { instance: message });
        return message;
    };

    return CatchFishRecord;
})();

$root.CH_AskActivity = (function() {

    function CH_AskActivity(properties) {
        this.strRuleVer = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskActivity.prototype.nidLogin = 0;
    CH_AskActivity.prototype.strSafeCode = "";
    CH_AskActivity.prototype.strRuleVer = $util.emptyArray;

    CH_AskActivity.create = function create(properties) {
        return new CH_AskActivity(properties);
    };

    CH_AskActivity.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        if (message.strRuleVer != null && message.strRuleVer.length)
            for (var i = 0; i < message.strRuleVer.length; ++i)
                $root.ActVersions.encode(message.strRuleVer[i], writer.uint32(26).fork()).ldelim();
        return writer;
    };

    CH_AskActivity.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskActivity();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                if (!(message.strRuleVer && message.strRuleVer.length))
                    message.strRuleVer = [];
                message.strRuleVer.push($root.ActVersions.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CH_AskActivity;
})();

$root.CH_AskCatchFishLog_Res = (function() {

    function CH_AskCatchFishLog_Res(properties) {
        this.catchFishRecordList = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskCatchFishLog_Res.prototype.catchFishRecordList = $util.emptyArray;

    CH_AskCatchFishLog_Res.create = function create(properties) {
        return new CH_AskCatchFishLog_Res(properties);
    };

    CH_AskCatchFishLog_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.catchFishRecordList != null && message.catchFishRecordList.length)
            for (var i = 0; i < message.catchFishRecordList.length; ++i)
                $root.CatchFishRecord.encode(message.catchFishRecordList[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    CH_AskCatchFishLog_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskCatchFishLog_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.catchFishRecordList && message.catchFishRecordList.length))
                    message.catchFishRecordList = [];
                message.catchFishRecordList.push($root.CatchFishRecord.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_AskCatchFishLog_Res;
})();

$root.CH_AskCatchFishLog = (function() {

    function CH_AskCatchFishLog(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskCatchFishLog.prototype.strStartTime = "";
    CH_AskCatchFishLog.prototype.strEndTime = "";
    CH_AskCatchFishLog.prototype.strRoomID = "";

    CH_AskCatchFishLog.create = function create(properties) {
        return new CH_AskCatchFishLog(properties);
    };

    CH_AskCatchFishLog.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strStartTime);
        writer.uint32(18).string(message.strEndTime);
        writer.uint32(26).string(message.strRoomID);
        return writer;
    };

    CH_AskCatchFishLog.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskCatchFishLog();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strStartTime = reader.string();
                break;
            case 2:
                message.strEndTime = reader.string();
                break;
            case 3:
                message.strRoomID = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strStartTime"))
            throw $util.ProtocolError("missing required 'strStartTime'", { instance: message });
        if (!message.hasOwnProperty("strEndTime"))
            throw $util.ProtocolError("missing required 'strEndTime'", { instance: message });
        if (!message.hasOwnProperty("strRoomID"))
            throw $util.ProtocolError("missing required 'strRoomID'", { instance: message });
        return message;
    };

    return CH_AskCatchFishLog;
})();

$root.CH_AskExchangeInfoMsg_Res = (function() {

    function CH_AskExchangeInfoMsg_Res(properties) {
        this.exchangeInfo = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskExchangeInfoMsg_Res.prototype.nRes = 0;
    CH_AskExchangeInfoMsg_Res.prototype.exchangeInfo = $util.emptyArray;
    CH_AskExchangeInfoMsg_Res.prototype.nUpdateTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CH_AskExchangeInfoMsg_Res.prototype.strSurplus = "";

    CH_AskExchangeInfoMsg_Res.create = function create(properties) {
        return new CH_AskExchangeInfoMsg_Res(properties);
    };

    CH_AskExchangeInfoMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        if (message.exchangeInfo != null && message.exchangeInfo.length)
            for (var i = 0; i < message.exchangeInfo.length; ++i)
                $root.ExchangeInfo.encode(message.exchangeInfo[i], writer.uint32(18).fork()).ldelim();
        if (message.nUpdateTime != null && Object.hasOwnProperty.call(message, "nUpdateTime"))
            writer.uint32(24).sint64(message.nUpdateTime);
        if (message.strSurplus != null && Object.hasOwnProperty.call(message, "strSurplus"))
            writer.uint32(34).string(message.strSurplus);
        return writer;
    };

    CH_AskExchangeInfoMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskExchangeInfoMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                if (!(message.exchangeInfo && message.exchangeInfo.length))
                    message.exchangeInfo = [];
                message.exchangeInfo.push($root.ExchangeInfo.decode(reader, reader.uint32()));
                break;
            case 3:
                message.nUpdateTime = reader.sint64();
                break;
            case 4:
                message.strSurplus = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        return message;
    };

    return CH_AskExchangeInfoMsg_Res;
})();

$root.ExchangeInfo = (function() {

    function ExchangeInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    ExchangeInfo.prototype.id = 0;
    ExchangeInfo.prototype.nShowID = 0;
    ExchangeInfo.prototype.nType = 0;
    ExchangeInfo.prototype.nChildType = 0;
    ExchangeInfo.prototype.nExchangeID = 0;
    ExchangeInfo.prototype.nExchangeNum = 0;
    ExchangeInfo.prototype.strCostNum = "";
    ExchangeInfo.prototype.nStockNum = 0;
    ExchangeInfo.prototype.nRemaNum = 0;
    ExchangeInfo.prototype.nPerExchangeLimitNum = 0;
    ExchangeInfo.prototype.nExamine = 0;
    ExchangeInfo.prototype.updateTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    ExchangeInfo.create = function create(properties) {
        return new ExchangeInfo(properties);
    };

    ExchangeInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.id);
        writer.uint32(16).sint32(message.nShowID);
        writer.uint32(24).sint32(message.nType);
        writer.uint32(32).sint32(message.nChildType);
        writer.uint32(40).sint32(message.nExchangeID);
        writer.uint32(48).sint32(message.nExchangeNum);
        writer.uint32(58).string(message.strCostNum);
        writer.uint32(64).sint32(message.nStockNum);
        writer.uint32(72).sint32(message.nRemaNum);
        writer.uint32(80).sint32(message.nPerExchangeLimitNum);
        writer.uint32(88).sint32(message.nExamine);
        writer.uint32(96).sint64(message.updateTime);
        return writer;
    };

    ExchangeInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ExchangeInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.id = reader.sint32();
                break;
            case 2:
                message.nShowID = reader.sint32();
                break;
            case 3:
                message.nType = reader.sint32();
                break;
            case 4:
                message.nChildType = reader.sint32();
                break;
            case 5:
                message.nExchangeID = reader.sint32();
                break;
            case 6:
                message.nExchangeNum = reader.sint32();
                break;
            case 7:
                message.strCostNum = reader.string();
                break;
            case 8:
                message.nStockNum = reader.sint32();
                break;
            case 9:
                message.nRemaNum = reader.sint32();
                break;
            case 10:
                message.nPerExchangeLimitNum = reader.sint32();
                break;
            case 11:
                message.nExamine = reader.sint32();
                break;
            case 12:
                message.updateTime = reader.sint64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("id"))
            throw $util.ProtocolError("missing required 'id'", { instance: message });
        if (!message.hasOwnProperty("nShowID"))
            throw $util.ProtocolError("missing required 'nShowID'", { instance: message });
        if (!message.hasOwnProperty("nType"))
            throw $util.ProtocolError("missing required 'nType'", { instance: message });
        if (!message.hasOwnProperty("nChildType"))
            throw $util.ProtocolError("missing required 'nChildType'", { instance: message });
        if (!message.hasOwnProperty("nExchangeID"))
            throw $util.ProtocolError("missing required 'nExchangeID'", { instance: message });
        if (!message.hasOwnProperty("nExchangeNum"))
            throw $util.ProtocolError("missing required 'nExchangeNum'", { instance: message });
        if (!message.hasOwnProperty("strCostNum"))
            throw $util.ProtocolError("missing required 'strCostNum'", { instance: message });
        if (!message.hasOwnProperty("nStockNum"))
            throw $util.ProtocolError("missing required 'nStockNum'", { instance: message });
        if (!message.hasOwnProperty("nRemaNum"))
            throw $util.ProtocolError("missing required 'nRemaNum'", { instance: message });
        if (!message.hasOwnProperty("nPerExchangeLimitNum"))
            throw $util.ProtocolError("missing required 'nPerExchangeLimitNum'", { instance: message });
        if (!message.hasOwnProperty("nExamine"))
            throw $util.ProtocolError("missing required 'nExamine'", { instance: message });
        if (!message.hasOwnProperty("updateTime"))
            throw $util.ProtocolError("missing required 'updateTime'", { instance: message });
        return message;
    };

    return ExchangeInfo;
})();

$root.CH_AskExchangeInfoMsg = (function() {

    function CH_AskExchangeInfoMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskExchangeInfoMsg.prototype.nUpdateTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    CH_AskExchangeInfoMsg.create = function create(properties) {
        return new CH_AskExchangeInfoMsg(properties);
    };

    CH_AskExchangeInfoMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint64(message.nUpdateTime);
        return writer;
    };

    CH_AskExchangeInfoMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskExchangeInfoMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nUpdateTime = reader.sint64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nUpdateTime"))
            throw $util.ProtocolError("missing required 'nUpdateTime'", { instance: message });
        return message;
    };

    return CH_AskExchangeInfoMsg;
})();

$root.CH_AskExchangeRecordMsg_Res = (function() {

    function CH_AskExchangeRecordMsg_Res(properties) {
        this.exchangeRecord = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskExchangeRecordMsg_Res.prototype.exchangeRecord = $util.emptyArray;

    CH_AskExchangeRecordMsg_Res.create = function create(properties) {
        return new CH_AskExchangeRecordMsg_Res(properties);
    };

    CH_AskExchangeRecordMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.exchangeRecord != null && message.exchangeRecord.length)
            for (var i = 0; i < message.exchangeRecord.length; ++i)
                $root.ExchangeRecord.encode(message.exchangeRecord[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    CH_AskExchangeRecordMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskExchangeRecordMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.exchangeRecord && message.exchangeRecord.length))
                    message.exchangeRecord = [];
                message.exchangeRecord.push($root.ExchangeRecord.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_AskExchangeRecordMsg_Res;
})();

$root.ExchangeRecord = (function() {

    function ExchangeRecord(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    ExchangeRecord.prototype.id = 0;
    ExchangeRecord.prototype.nExNum = 0;
    ExchangeRecord.prototype.strAppTime = "";
    ExchangeRecord.prototype.nState = 0;

    ExchangeRecord.create = function create(properties) {
        return new ExchangeRecord(properties);
    };

    ExchangeRecord.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.id);
        writer.uint32(16).sint32(message.nExNum);
        writer.uint32(26).string(message.strAppTime);
        writer.uint32(32).sint32(message.nState);
        return writer;
    };

    ExchangeRecord.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ExchangeRecord();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.id = reader.sint32();
                break;
            case 2:
                message.nExNum = reader.sint32();
                break;
            case 3:
                message.strAppTime = reader.string();
                break;
            case 4:
                message.nState = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("id"))
            throw $util.ProtocolError("missing required 'id'", { instance: message });
        if (!message.hasOwnProperty("nExNum"))
            throw $util.ProtocolError("missing required 'nExNum'", { instance: message });
        if (!message.hasOwnProperty("strAppTime"))
            throw $util.ProtocolError("missing required 'strAppTime'", { instance: message });
        if (!message.hasOwnProperty("nState"))
            throw $util.ProtocolError("missing required 'nState'", { instance: message });
        return message;
    };

    return ExchangeRecord;
})();

$root.CH_AskExchangeRecordMsg = (function() {

    function CH_AskExchangeRecordMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskExchangeRecordMsg.create = function create(properties) {
        return new CH_AskExchangeRecordMsg(properties);
    };

    CH_AskExchangeRecordMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        return writer;
    };

    CH_AskExchangeRecordMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskExchangeRecordMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_AskExchangeRecordMsg;
})();

$root.CH_AskRoomLog_Res = (function() {

    function CH_AskRoomLog_Res(properties) {
        this.roomLogList = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskRoomLog_Res.prototype.roomLogList = $util.emptyArray;

    CH_AskRoomLog_Res.create = function create(properties) {
        return new CH_AskRoomLog_Res(properties);
    };

    CH_AskRoomLog_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.roomLogList != null && message.roomLogList.length)
            for (var i = 0; i < message.roomLogList.length; ++i)
                $root.RoomLogInfo.encode(message.roomLogList[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    CH_AskRoomLog_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskRoomLog_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.roomLogList && message.roomLogList.length))
                    message.roomLogList = [];
                message.roomLogList.push($root.RoomLogInfo.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_AskRoomLog_Res;
})();

$root.RoomLogInfo = (function() {

    function RoomLogInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    RoomLogInfo.prototype.strOrderID = "";
    RoomLogInfo.prototype.strRoomID = "";
    RoomLogInfo.prototype.nConsume = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    RoomLogInfo.prototype.nGet = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    RoomLogInfo.prototype.strInOutTime = "";

    RoomLogInfo.create = function create(properties) {
        return new RoomLogInfo(properties);
    };

    RoomLogInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strOrderID);
        writer.uint32(18).string(message.strRoomID);
        writer.uint32(24).sint64(message.nConsume);
        writer.uint32(32).sint64(message.nGet);
        writer.uint32(42).string(message.strInOutTime);
        return writer;
    };

    RoomLogInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.RoomLogInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strOrderID = reader.string();
                break;
            case 2:
                message.strRoomID = reader.string();
                break;
            case 3:
                message.nConsume = reader.sint64();
                break;
            case 4:
                message.nGet = reader.sint64();
                break;
            case 5:
                message.strInOutTime = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strOrderID"))
            throw $util.ProtocolError("missing required 'strOrderID'", { instance: message });
        if (!message.hasOwnProperty("strRoomID"))
            throw $util.ProtocolError("missing required 'strRoomID'", { instance: message });
        if (!message.hasOwnProperty("nConsume"))
            throw $util.ProtocolError("missing required 'nConsume'", { instance: message });
        if (!message.hasOwnProperty("nGet"))
            throw $util.ProtocolError("missing required 'nGet'", { instance: message });
        if (!message.hasOwnProperty("strInOutTime"))
            throw $util.ProtocolError("missing required 'strInOutTime'", { instance: message });
        return message;
    };

    return RoomLogInfo;
})();

$root.CH_AskRoomLog = (function() {

    function CH_AskRoomLog(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskRoomLog.prototype.strStartTime = "";
    CH_AskRoomLog.prototype.strEndTime = "";

    CH_AskRoomLog.create = function create(properties) {
        return new CH_AskRoomLog(properties);
    };

    CH_AskRoomLog.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strStartTime);
        writer.uint32(18).string(message.strEndTime);
        return writer;
    };

    CH_AskRoomLog.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskRoomLog();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strStartTime = reader.string();
                break;
            case 2:
                message.strEndTime = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strStartTime"))
            throw $util.ProtocolError("missing required 'strStartTime'", { instance: message });
        if (!message.hasOwnProperty("strEndTime"))
            throw $util.ProtocolError("missing required 'strEndTime'", { instance: message });
        return message;
    };

    return CH_AskRoomLog;
})();

$root.CH_ExchangePrizesMsg_Res = (function() {

    function CH_ExchangePrizesMsg_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ExchangePrizesMsg_Res.prototype.nRes = 0;
    CH_ExchangePrizesMsg_Res.prototype.nExID = 0;
    CH_ExchangePrizesMsg_Res.prototype.itemReward = $util.emptyArray;
    CH_ExchangePrizesMsg_Res.prototype.strAppTime = "";
    CH_ExchangePrizesMsg_Res.prototype.nState = 0;
    CH_ExchangePrizesMsg_Res.prototype.strNewItem = "";

    CH_ExchangePrizesMsg_Res.create = function create(properties) {
        return new CH_ExchangePrizesMsg_Res(properties);
    };

    CH_ExchangePrizesMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        writer.uint32(16).sint32(message.nExID);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(26).fork()).ldelim();
        if (message.strAppTime != null && Object.hasOwnProperty.call(message, "strAppTime"))
            writer.uint32(34).string(message.strAppTime);
        if (message.nState != null && Object.hasOwnProperty.call(message, "nState"))
            writer.uint32(40).sint32(message.nState);
        if (message.strNewItem != null && Object.hasOwnProperty.call(message, "strNewItem"))
            writer.uint32(50).string(message.strNewItem);
        return writer;
    };

    CH_ExchangePrizesMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ExchangePrizesMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                message.nExID = reader.sint32();
                break;
            case 3:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            case 4:
                message.strAppTime = reader.string();
                break;
            case 5:
                message.nState = reader.sint32();
                break;
            case 6:
                message.strNewItem = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        if (!message.hasOwnProperty("nExID"))
            throw $util.ProtocolError("missing required 'nExID'", { instance: message });
        return message;
    };

    return CH_ExchangePrizesMsg_Res;
})();

$root.ItemReward = (function() {

    function ItemReward(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    ItemReward.prototype.nItemID = 0;
    ItemReward.prototype.nItemNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    ItemReward.prototype.nError = 0;
    ItemReward.prototype.nNum = 0;
    ItemReward.prototype.strEndTime = "";

    ItemReward.create = function create(properties) {
        return new ItemReward(properties);
    };

    ItemReward.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nItemID);
        writer.uint32(16).sint64(message.nItemNum);
        if (message.nError != null && Object.hasOwnProperty.call(message, "nError"))
            writer.uint32(24).sint32(message.nError);
        writer.uint32(32).sint32(message.nNum);
        if (message.strEndTime != null && Object.hasOwnProperty.call(message, "strEndTime"))
            writer.uint32(42).string(message.strEndTime);
        return writer;
    };

    ItemReward.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ItemReward();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nItemID = reader.sint32();
                break;
            case 2:
                message.nItemNum = reader.sint64();
                break;
            case 3:
                message.nError = reader.sint32();
                break;
            case 4:
                message.nNum = reader.sint32();
                break;
            case 5:
                message.strEndTime = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nItemID"))
            throw $util.ProtocolError("missing required 'nItemID'", { instance: message });
        if (!message.hasOwnProperty("nItemNum"))
            throw $util.ProtocolError("missing required 'nItemNum'", { instance: message });
        if (!message.hasOwnProperty("nNum"))
            throw $util.ProtocolError("missing required 'nNum'", { instance: message });
        return message;
    };

    return ItemReward;
})();

$root.CH_ExchangePrizesMsg = (function() {

    function CH_ExchangePrizesMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ExchangePrizesMsg.prototype.nID = 0;
    CH_ExchangePrizesMsg.prototype.nExNum = 0;

    CH_ExchangePrizesMsg.create = function create(properties) {
        return new CH_ExchangePrizesMsg(properties);
    };

    CH_ExchangePrizesMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nID);
        writer.uint32(16).sint32(message.nExNum);
        return writer;
    };

    CH_ExchangePrizesMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ExchangePrizesMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nID = reader.sint32();
                break;
            case 2:
                message.nExNum = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nID"))
            throw $util.ProtocolError("missing required 'nID'", { instance: message });
        if (!message.hasOwnProperty("nExNum"))
            throw $util.ProtocolError("missing required 'nExNum'", { instance: message });
        return message;
    };

    return CH_ExchangePrizesMsg;
})();

$root.CH_FortUpLevel_Res = (function() {

    function CH_FortUpLevel_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_FortUpLevel_Res.prototype.bSuccess = false;
    CH_FortUpLevel_Res.prototype.nMaxFort = 0;
    CH_FortUpLevel_Res.prototype.itemReward = $util.emptyArray;

    CH_FortUpLevel_Res.create = function create(properties) {
        return new CH_FortUpLevel_Res(properties);
    };

    CH_FortUpLevel_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).bool(message.bSuccess);
        if (message.nMaxFort != null && Object.hasOwnProperty.call(message, "nMaxFort"))
            writer.uint32(16).int32(message.nMaxFort);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(26).fork()).ldelim();
        return writer;
    };

    CH_FortUpLevel_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_FortUpLevel_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.bSuccess = reader.bool();
                break;
            case 2:
                message.nMaxFort = reader.int32();
                break;
            case 3:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("bSuccess"))
            throw $util.ProtocolError("missing required 'bSuccess'", { instance: message });
        return message;
    };

    return CH_FortUpLevel_Res;
})();

$root.CH_FortUpLevel = (function() {

    function CH_FortUpLevel(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_FortUpLevel.prototype.nidLogin = 0;
    CH_FortUpLevel.prototype.strSafeCode = "";
    CH_FortUpLevel.prototype.bUseStone = false;

    CH_FortUpLevel.create = function create(properties) {
        return new CH_FortUpLevel(properties);
    };

    CH_FortUpLevel.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).bool(message.bUseStone);
        return writer;
    };

    CH_FortUpLevel.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_FortUpLevel();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.bUseStone = reader.bool();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("bUseStone"))
            throw $util.ProtocolError("missing required 'bUseStone'", { instance: message });
        return message;
    };

    return CH_FortUpLevel;
})();

$root.CH_GetFirstRewardMsg = (function() {

    function CH_GetFirstRewardMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_GetFirstRewardMsg.create = function create(properties) {
        return new CH_GetFirstRewardMsg(properties);
    };

    CH_GetFirstRewardMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        return writer;
    };

    CH_GetFirstRewardMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_GetFirstRewardMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_GetFirstRewardMsg;
})();

$root.CH_GrandPrixRankMsg_Res = (function() {

    function CH_GrandPrixRankMsg_Res(properties) {
        this.weekRankInfo = [];
        this.dayRankInfo = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_GrandPrixRankMsg_Res.prototype.nMaxIntegral = 0;
    CH_GrandPrixRankMsg_Res.prototype.nTodayIntegral = 0;
    CH_GrandPrixRankMsg_Res.prototype.nIntegralNum = 0;
    CH_GrandPrixRankMsg_Res.prototype.nBulletNum = 0;
    CH_GrandPrixRankMsg_Res.prototype.nTodayRank = 0;
    CH_GrandPrixRankMsg_Res.prototype.nWeekRank = 0;
    CH_GrandPrixRankMsg_Res.prototype.weekRankInfo = $util.emptyArray;
    CH_GrandPrixRankMsg_Res.prototype.dayRankInfo = $util.emptyArray;
    CH_GrandPrixRankMsg_Res.prototype.nUpdateTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    CH_GrandPrixRankMsg_Res.create = function create(properties) {
        return new CH_GrandPrixRankMsg_Res(properties);
    };

    CH_GrandPrixRankMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nMaxIntegral);
        writer.uint32(16).sint32(message.nTodayIntegral);
        writer.uint32(29).float(message.nIntegralNum);
        writer.uint32(32).sint32(message.nBulletNum);
        writer.uint32(40).sint32(message.nTodayRank);
        writer.uint32(48).sint32(message.nWeekRank);
        if (message.weekRankInfo != null && message.weekRankInfo.length)
            for (var i = 0; i < message.weekRankInfo.length; ++i)
                $root.PlayerGrandInfo.encode(message.weekRankInfo[i], writer.uint32(58).fork()).ldelim();
        if (message.dayRankInfo != null && message.dayRankInfo.length)
            for (var i = 0; i < message.dayRankInfo.length; ++i)
                $root.PlayerGrandInfo.encode(message.dayRankInfo[i], writer.uint32(66).fork()).ldelim();
        writer.uint32(72).sint64(message.nUpdateTime);
        return writer;
    };

    CH_GrandPrixRankMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_GrandPrixRankMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nMaxIntegral = reader.sint32();
                break;
            case 2:
                message.nTodayIntegral = reader.sint32();
                break;
            case 3:
                message.nIntegralNum = reader.float();
                break;
            case 4:
                message.nBulletNum = reader.sint32();
                break;
            case 5:
                message.nTodayRank = reader.sint32();
                break;
            case 6:
                message.nWeekRank = reader.sint32();
                break;
            case 7:
                if (!(message.weekRankInfo && message.weekRankInfo.length))
                    message.weekRankInfo = [];
                message.weekRankInfo.push($root.PlayerGrandInfo.decode(reader, reader.uint32()));
                break;
            case 8:
                if (!(message.dayRankInfo && message.dayRankInfo.length))
                    message.dayRankInfo = [];
                message.dayRankInfo.push($root.PlayerGrandInfo.decode(reader, reader.uint32()));
                break;
            case 9:
                message.nUpdateTime = reader.sint64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nMaxIntegral"))
            throw $util.ProtocolError("missing required 'nMaxIntegral'", { instance: message });
        if (!message.hasOwnProperty("nTodayIntegral"))
            throw $util.ProtocolError("missing required 'nTodayIntegral'", { instance: message });
        if (!message.hasOwnProperty("nIntegralNum"))
            throw $util.ProtocolError("missing required 'nIntegralNum'", { instance: message });
        if (!message.hasOwnProperty("nBulletNum"))
            throw $util.ProtocolError("missing required 'nBulletNum'", { instance: message });
        if (!message.hasOwnProperty("nTodayRank"))
            throw $util.ProtocolError("missing required 'nTodayRank'", { instance: message });
        if (!message.hasOwnProperty("nWeekRank"))
            throw $util.ProtocolError("missing required 'nWeekRank'", { instance: message });
        if (!message.hasOwnProperty("nUpdateTime"))
            throw $util.ProtocolError("missing required 'nUpdateTime'", { instance: message });
        return message;
    };

    return CH_GrandPrixRankMsg_Res;
})();

$root.PlayerGrandInfo = (function() {

    function PlayerGrandInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    PlayerGrandInfo.prototype.nPrize = 0;
    PlayerGrandInfo.prototype.nRank = 0;
    PlayerGrandInfo.prototype.nIdLogin = 0;
    PlayerGrandInfo.prototype.strNickName = "";
    PlayerGrandInfo.prototype.fIntegral = 0;
    PlayerGrandInfo.prototype.nHeadType = 0;

    PlayerGrandInfo.create = function create(properties) {
        return new PlayerGrandInfo(properties);
    };

    PlayerGrandInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nPrize);
        writer.uint32(16).sint32(message.nRank);
        writer.uint32(24).sint32(message.nIdLogin);
        writer.uint32(34).string(message.strNickName);
        writer.uint32(45).float(message.fIntegral);
        writer.uint32(48).sint32(message.nHeadType);
        return writer;
    };

    PlayerGrandInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PlayerGrandInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nPrize = reader.sint32();
                break;
            case 2:
                message.nRank = reader.sint32();
                break;
            case 3:
                message.nIdLogin = reader.sint32();
                break;
            case 4:
                message.strNickName = reader.string();
                break;
            case 5:
                message.fIntegral = reader.float();
                break;
            case 6:
                message.nHeadType = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nPrize"))
            throw $util.ProtocolError("missing required 'nPrize'", { instance: message });
        if (!message.hasOwnProperty("nRank"))
            throw $util.ProtocolError("missing required 'nRank'", { instance: message });
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strNickName"))
            throw $util.ProtocolError("missing required 'strNickName'", { instance: message });
        if (!message.hasOwnProperty("fIntegral"))
            throw $util.ProtocolError("missing required 'fIntegral'", { instance: message });
        if (!message.hasOwnProperty("nHeadType"))
            throw $util.ProtocolError("missing required 'nHeadType'", { instance: message });
        return message;
    };

    return PlayerGrandInfo;
})();

$root.CH_GrandPrixRankMsg = (function() {

    function CH_GrandPrixRankMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_GrandPrixRankMsg.prototype.nUpdateTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    CH_GrandPrixRankMsg.create = function create(properties) {
        return new CH_GrandPrixRankMsg(properties);
    };

    CH_GrandPrixRankMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint64(message.nUpdateTime);
        return writer;
    };

    CH_GrandPrixRankMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_GrandPrixRankMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nUpdateTime = reader.sint64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nUpdateTime"))
            throw $util.ProtocolError("missing required 'nUpdateTime'", { instance: message });
        return message;
    };

    return CH_GrandPrixRankMsg;
})();

$root.CH_InHallReceiveMonthCardGift_Res = (function() {

    function CH_InHallReceiveMonthCardGift_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_InHallReceiveMonthCardGift_Res.prototype.nRes = 0;
    CH_InHallReceiveMonthCardGift_Res.prototype.itemReward = $util.emptyArray;

    CH_InHallReceiveMonthCardGift_Res.create = function create(properties) {
        return new CH_InHallReceiveMonthCardGift_Res(properties);
    };

    CH_InHallReceiveMonthCardGift_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(18).fork()).ldelim();
        return writer;
    };

    CH_InHallReceiveMonthCardGift_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_InHallReceiveMonthCardGift_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        return message;
    };

    return CH_InHallReceiveMonthCardGift_Res;
})();

$root.CH_InHallReceiveMonthCardGift = (function() {

    function CH_InHallReceiveMonthCardGift(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_InHallReceiveMonthCardGift.prototype.nidLogin = 0;
    CH_InHallReceiveMonthCardGift.prototype.strSafeCode = "";

    CH_InHallReceiveMonthCardGift.create = function create(properties) {
        return new CH_InHallReceiveMonthCardGift(properties);
    };

    CH_InHallReceiveMonthCardGift.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        return writer;
    };

    CH_InHallReceiveMonthCardGift.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_InHallReceiveMonthCardGift();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CH_InHallReceiveMonthCardGift;
})();

$root.CH_JP_log_Res = (function() {

    function CH_JP_log_Res(properties) {
        this.jplogs = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_JP_log_Res.prototype.jplogs = $util.emptyArray;

    CH_JP_log_Res.create = function create(properties) {
        return new CH_JP_log_Res(properties);
    };

    CH_JP_log_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.jplogs != null && message.jplogs.length)
            for (var i = 0; i < message.jplogs.length; ++i)
                $root.JPLog.encode(message.jplogs[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    CH_JP_log_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_JP_log_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.jplogs && message.jplogs.length))
                    message.jplogs = [];
                message.jplogs.push($root.JPLog.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_JP_log_Res;
})();

$root.JPLog = (function() {

    function JPLog(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    JPLog.prototype.idlogin = 0;
    JPLog.prototype.hitmoney = 0;
    JPLog.prototype.time = "";

    JPLog.create = function create(properties) {
        return new JPLog(properties);
    };

    JPLog.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.idlogin);
        writer.uint32(16).sint32(message.hitmoney);
        writer.uint32(26).string(message.time);
        return writer;
    };

    JPLog.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.JPLog();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.idlogin = reader.sint32();
                break;
            case 2:
                message.hitmoney = reader.sint32();
                break;
            case 3:
                message.time = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("idlogin"))
            throw $util.ProtocolError("missing required 'idlogin'", { instance: message });
        if (!message.hasOwnProperty("hitmoney"))
            throw $util.ProtocolError("missing required 'hitmoney'", { instance: message });
        if (!message.hasOwnProperty("time"))
            throw $util.ProtocolError("missing required 'time'", { instance: message });
        return message;
    };

    return JPLog;
})();

$root.CH_JP_log = (function() {

    function CH_JP_log(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_JP_log.prototype.roomType = 0;

    CH_JP_log.create = function create(properties) {
        return new CH_JP_log(properties);
    };

    CH_JP_log.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.roomType);
        return writer;
    };

    CH_JP_log.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_JP_log();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.roomType = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("roomType"))
            throw $util.ProtocolError("missing required 'roomType'", { instance: message });
        return message;
    };

    return CH_JP_log;
})();

$root.CH_LoginBonus_Res = (function() {

    function CH_LoginBonus_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_LoginBonus_Res.prototype.nType = 0;
    CH_LoginBonus_Res.prototype.nLoginWeekRecord = 0;
    CH_LoginBonus_Res.prototype.nLoginContinuity = 0;
    CH_LoginBonus_Res.prototype.itemReward = $util.emptyArray;

    CH_LoginBonus_Res.create = function create(properties) {
        return new CH_LoginBonus_Res(properties);
    };

    CH_LoginBonus_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nType);
        writer.uint32(16).sint32(message.nLoginWeekRecord);
        writer.uint32(24).sint32(message.nLoginContinuity);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(34).fork()).ldelim();
        return writer;
    };

    CH_LoginBonus_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_LoginBonus_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nType = reader.sint32();
                break;
            case 2:
                message.nLoginWeekRecord = reader.sint32();
                break;
            case 3:
                message.nLoginContinuity = reader.sint32();
                break;
            case 4:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nType"))
            throw $util.ProtocolError("missing required 'nType'", { instance: message });
        if (!message.hasOwnProperty("nLoginWeekRecord"))
            throw $util.ProtocolError("missing required 'nLoginWeekRecord'", { instance: message });
        if (!message.hasOwnProperty("nLoginContinuity"))
            throw $util.ProtocolError("missing required 'nLoginContinuity'", { instance: message });
        return message;
    };

    return CH_LoginBonus_Res;
})();

$root.CH_LoginBonus = (function() {

    function CH_LoginBonus(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_LoginBonus.prototype.nidLogin = 0;
    CH_LoginBonus.prototype.strSafeCode = "";
    CH_LoginBonus.prototype.nType = 0;
    CH_LoginBonus.prototype.nNum = 0;

    CH_LoginBonus.create = function create(properties) {
        return new CH_LoginBonus(properties);
    };

    CH_LoginBonus.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nType);
        writer.uint32(32).sint32(message.nNum);
        return writer;
    };

    CH_LoginBonus.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_LoginBonus();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nType = reader.sint32();
                break;
            case 4:
                message.nNum = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nType"))
            throw $util.ProtocolError("missing required 'nType'", { instance: message });
        if (!message.hasOwnProperty("nNum"))
            throw $util.ProtocolError("missing required 'nNum'", { instance: message });
        return message;
    };

    return CH_LoginBonus;
})();

$root.CH_LuckDraw_Res = (function() {

    function CH_LuckDraw_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_LuckDraw_Res.prototype.strLuckDrawData = "";
    CH_LuckDraw_Res.prototype.fAccPoints = 0;
    CH_LuckDraw_Res.prototype.itemReward = $util.emptyArray;

    CH_LuckDraw_Res.create = function create(properties) {
        return new CH_LuckDraw_Res(properties);
    };

    CH_LuckDraw_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strLuckDrawData);
        writer.uint32(21).float(message.fAccPoints);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(26).fork()).ldelim();
        return writer;
    };

    CH_LuckDraw_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_LuckDraw_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strLuckDrawData = reader.string();
                break;
            case 2:
                message.fAccPoints = reader.float();
                break;
            case 3:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strLuckDrawData"))
            throw $util.ProtocolError("missing required 'strLuckDrawData'", { instance: message });
        if (!message.hasOwnProperty("fAccPoints"))
            throw $util.ProtocolError("missing required 'fAccPoints'", { instance: message });
        return message;
    };

    return CH_LuckDraw_Res;
})();

$root.CH_PublicMsg = (function() {

    function CH_PublicMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_PublicMsg.prototype.strSafeCode = "";
    CH_PublicMsg.prototype.nidLogin = 0;

    CH_PublicMsg.create = function create(properties) {
        return new CH_PublicMsg(properties);
    };

    CH_PublicMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strSafeCode);
        if (message.nidLogin != null && Object.hasOwnProperty.call(message, "nidLogin"))
            writer.uint32(16).sint32(message.nidLogin);
        return writer;
    };

    CH_PublicMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_PublicMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strSafeCode = reader.string();
                break;
            case 2:
                message.nidLogin = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CH_PublicMsg;
})();

$root.CH_ReceiveActAward_Res = (function() {

    function CH_ReceiveActAward_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ReceiveActAward_Res.prototype.nActID = 0;
    CH_ReceiveActAward_Res.prototype.nIndex = 0;
    CH_ReceiveActAward_Res.prototype.strRecord = "";
    CH_ReceiveActAward_Res.prototype.itemReward = $util.emptyArray;

    CH_ReceiveActAward_Res.create = function create(properties) {
        return new CH_ReceiveActAward_Res(properties);
    };

    CH_ReceiveActAward_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nActID);
        writer.uint32(16).sint32(message.nIndex);
        writer.uint32(26).string(message.strRecord);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(34).fork()).ldelim();
        return writer;
    };

    CH_ReceiveActAward_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ReceiveActAward_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nActID = reader.sint32();
                break;
            case 2:
                message.nIndex = reader.sint32();
                break;
            case 3:
                message.strRecord = reader.string();
                break;
            case 4:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nActID"))
            throw $util.ProtocolError("missing required 'nActID'", { instance: message });
        if (!message.hasOwnProperty("nIndex"))
            throw $util.ProtocolError("missing required 'nIndex'", { instance: message });
        if (!message.hasOwnProperty("strRecord"))
            throw $util.ProtocolError("missing required 'strRecord'", { instance: message });
        return message;
    };

    return CH_ReceiveActAward_Res;
})();

$root.CH_ReceiveActAward = (function() {

    function CH_ReceiveActAward(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ReceiveActAward.prototype.nidLogin = 0;
    CH_ReceiveActAward.prototype.strSafeCode = "";
    CH_ReceiveActAward.prototype.nActID = 0;
    CH_ReceiveActAward.prototype.nAwardIndex = 0;

    CH_ReceiveActAward.create = function create(properties) {
        return new CH_ReceiveActAward(properties);
    };

    CH_ReceiveActAward.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nActID);
        writer.uint32(32).sint32(message.nAwardIndex);
        return writer;
    };

    CH_ReceiveActAward.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ReceiveActAward();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nActID = reader.sint32();
                break;
            case 4:
                message.nAwardIndex = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nActID"))
            throw $util.ProtocolError("missing required 'nActID'", { instance: message });
        if (!message.hasOwnProperty("nAwardIndex"))
            throw $util.ProtocolError("missing required 'nAwardIndex'", { instance: message });
        return message;
    };

    return CH_ReceiveActAward;
})();

$root.CH_SellItem_Res = (function() {

    function CH_SellItem_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_SellItem_Res.prototype.itemReward = $util.emptyArray;

    CH_SellItem_Res.create = function create(properties) {
        return new CH_SellItem_Res(properties);
    };

    CH_SellItem_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    CH_SellItem_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_SellItem_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_SellItem_Res;
})();

$root.CH_SellItem = (function() {

    function CH_SellItem(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_SellItem.prototype.strSafeCode = "";
    CH_SellItem.prototype.nItemID = 0;
    CH_SellItem.prototype.nItemNum = 0;

    CH_SellItem.create = function create(properties) {
        return new CH_SellItem(properties);
    };

    CH_SellItem.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strSafeCode);
        writer.uint32(16).sint32(message.nItemID);
        writer.uint32(24).sint32(message.nItemNum);
        return writer;
    };

    CH_SellItem.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_SellItem();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strSafeCode = reader.string();
                break;
            case 2:
                message.nItemID = reader.sint32();
                break;
            case 3:
                message.nItemNum = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nItemID"))
            throw $util.ProtocolError("missing required 'nItemID'", { instance: message });
        if (!message.hasOwnProperty("nItemNum"))
            throw $util.ProtocolError("missing required 'nItemNum'", { instance: message });
        return message;
    };

    return CH_SellItem;
})();

$root.CH_TaskActiveGift_Res = (function() {

    function CH_TaskActiveGift_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_TaskActiveGift_Res.prototype.nDailyActiveData = 0;
    CH_TaskActiveGift_Res.prototype.nWeekActiveData = 0;
    CH_TaskActiveGift_Res.prototype.itemReward = $util.emptyArray;

    CH_TaskActiveGift_Res.create = function create(properties) {
        return new CH_TaskActiveGift_Res(properties);
    };

    CH_TaskActiveGift_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nDailyActiveData);
        writer.uint32(16).sint32(message.nWeekActiveData);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(26).fork()).ldelim();
        return writer;
    };

    CH_TaskActiveGift_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_TaskActiveGift_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nDailyActiveData = reader.sint32();
                break;
            case 2:
                message.nWeekActiveData = reader.sint32();
                break;
            case 3:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nDailyActiveData"))
            throw $util.ProtocolError("missing required 'nDailyActiveData'", { instance: message });
        if (!message.hasOwnProperty("nWeekActiveData"))
            throw $util.ProtocolError("missing required 'nWeekActiveData'", { instance: message });
        return message;
    };

    return CH_TaskActiveGift_Res;
})();

$root.CH_TaskActiveGift = (function() {

    function CH_TaskActiveGift(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_TaskActiveGift.prototype.nidLogin = 0;
    CH_TaskActiveGift.prototype.strSafeCode = "";
    CH_TaskActiveGift.prototype.nID = 0;

    CH_TaskActiveGift.create = function create(properties) {
        return new CH_TaskActiveGift(properties);
    };

    CH_TaskActiveGift.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nID);
        return writer;
    };

    CH_TaskActiveGift.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_TaskActiveGift();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nID"))
            throw $util.ProtocolError("missing required 'nID'", { instance: message });
        return message;
    };

    return CH_TaskActiveGift;
})();

$root.CH_TaskVIPGift_Res = (function() {

    function CH_TaskVIPGift_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_TaskVIPGift_Res.prototype.nVIPLevel = 0;
    CH_TaskVIPGift_Res.prototype.itemReward = $util.emptyArray;

    CH_TaskVIPGift_Res.create = function create(properties) {
        return new CH_TaskVIPGift_Res(properties);
    };

    CH_TaskVIPGift_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nVIPLevel);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(18).fork()).ldelim();
        return writer;
    };

    CH_TaskVIPGift_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_TaskVIPGift_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nVIPLevel = reader.sint32();
                break;
            case 2:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nVIPLevel"))
            throw $util.ProtocolError("missing required 'nVIPLevel'", { instance: message });
        return message;
    };

    return CH_TaskVIPGift_Res;
})();

$root.CH_TaskVIPGift = (function() {

    function CH_TaskVIPGift(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_TaskVIPGift.prototype.nidLogin = 0;
    CH_TaskVIPGift.prototype.strSafeCode = "";
    CH_TaskVIPGift.prototype.nVIPLevel = 0;

    CH_TaskVIPGift.create = function create(properties) {
        return new CH_TaskVIPGift(properties);
    };

    CH_TaskVIPGift.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nVIPLevel);
        return writer;
    };

    CH_TaskVIPGift.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_TaskVIPGift();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nVIPLevel = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nVIPLevel"))
            throw $util.ProtocolError("missing required 'nVIPLevel'", { instance: message });
        return message;
    };

    return CH_TaskVIPGift;
})();

$root.CH_UseItem_Msg = (function() {

    function CH_UseItem_Msg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_UseItem_Msg.prototype.nItemID = 0;
    CH_UseItem_Msg.prototype.nItemNum = 0;

    CH_UseItem_Msg.create = function create(properties) {
        return new CH_UseItem_Msg(properties);
    };

    CH_UseItem_Msg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nItemID);
        writer.uint32(16).sint32(message.nItemNum);
        return writer;
    };

    CH_UseItem_Msg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_UseItem_Msg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nItemID = reader.sint32();
                break;
            case 2:
                message.nItemNum = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nItemID"))
            throw $util.ProtocolError("missing required 'nItemID'", { instance: message });
        if (!message.hasOwnProperty("nItemNum"))
            throw $util.ProtocolError("missing required 'nItemNum'", { instance: message });
        return message;
    };

    return CH_UseItem_Msg;
})();

$root.CH_UseItemRes_Msg = (function() {

    function CH_UseItemRes_Msg(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_UseItemRes_Msg.prototype.itemReward = $util.emptyArray;

    CH_UseItemRes_Msg.create = function create(properties) {
        return new CH_UseItemRes_Msg(properties);
    };

    CH_UseItemRes_Msg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    CH_UseItemRes_Msg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_UseItemRes_Msg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_UseItemRes_Msg;
})();

$root.CS_AgreeRank_Res = (function() {

    function CS_AgreeRank_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_AgreeRank_Res.prototype.nRankType = 0;
    CS_AgreeRank_Res.prototype.isAgreeRank = 0;

    CS_AgreeRank_Res.create = function create(properties) {
        return new CS_AgreeRank_Res(properties);
    };

    CS_AgreeRank_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRankType);
        writer.uint32(24).sint32(message.isAgreeRank);
        return writer;
    };

    CS_AgreeRank_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_AgreeRank_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRankType = reader.sint32();
                break;
            case 3:
                message.isAgreeRank = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRankType"))
            throw $util.ProtocolError("missing required 'nRankType'", { instance: message });
        if (!message.hasOwnProperty("isAgreeRank"))
            throw $util.ProtocolError("missing required 'isAgreeRank'", { instance: message });
        return message;
    };

    return CS_AgreeRank_Res;
})();

$root.CS_AgreeRank = (function() {

    function CS_AgreeRank(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_AgreeRank.prototype.nidLogin = 0;
    CS_AgreeRank.prototype.strSafeCode = "";
    CS_AgreeRank.prototype.nRankType = 0;
    CS_AgreeRank.prototype.nIsAgreeRank = 0;

    CS_AgreeRank.create = function create(properties) {
        return new CS_AgreeRank(properties);
    };

    CS_AgreeRank.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nRankType);
        writer.uint32(32).sint32(message.nIsAgreeRank);
        return writer;
    };

    CS_AgreeRank.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_AgreeRank();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nRankType = reader.sint32();
                break;
            case 4:
                message.nIsAgreeRank = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nRankType"))
            throw $util.ProtocolError("missing required 'nRankType'", { instance: message });
        if (!message.hasOwnProperty("nIsAgreeRank"))
            throw $util.ProtocolError("missing required 'nIsAgreeRank'", { instance: message });
        return message;
    };

    return CS_AgreeRank;
})();

$root.CS_ChangeFort_Res = (function() {

    function CS_ChangeFort_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ChangeFort_Res.prototype.nIdLogin = 0;
    CS_ChangeFort_Res.prototype.nSeatID = 0;
    CS_ChangeFort_Res.prototype.nFortId = 0;

    CS_ChangeFort_Res.create = function create(properties) {
        return new CS_ChangeFort_Res(properties);
    };

    CS_ChangeFort_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).int32(message.nIdLogin);
        writer.uint32(16).int32(message.nSeatID);
        writer.uint32(24).int32(message.nFortId);
        return writer;
    };

    CS_ChangeFort_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ChangeFort_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.int32();
                break;
            case 2:
                message.nSeatID = reader.int32();
                break;
            case 3:
                message.nFortId = reader.int32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("nFortId"))
            throw $util.ProtocolError("missing required 'nFortId'", { instance: message });
        return message;
    };

    return CS_ChangeFort_Res;
})();

$root.CS_ChangeFort = (function() {

    function CS_ChangeFort(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ChangeFort.create = function create(properties) {
        return new CS_ChangeFort(properties);
    };

    CS_ChangeFort.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        return writer;
    };

    CS_ChangeFort.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ChangeFort();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CS_ChangeFort;
})();

$root.CS_ChatInAllServer_Res = (function() {

    function CS_ChatInAllServer_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ChatInAllServer_Res.prototype.nSeatID = 0;
    CS_ChatInAllServer_Res.prototype.strNickname = "";
    CS_ChatInAllServer_Res.prototype.nVIPLevel = 0;
    CS_ChatInAllServer_Res.prototype.strChat = "";
    CS_ChatInAllServer_Res.prototype.strNowTime = "";
    CS_ChatInAllServer_Res.prototype.itemReward = $util.emptyArray;

    CS_ChatInAllServer_Res.create = function create(properties) {
        return new CS_ChatInAllServer_Res(properties);
    };

    CS_ChatInAllServer_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        writer.uint32(18).string(message.strNickname);
        writer.uint32(24).sint32(message.nVIPLevel);
        writer.uint32(34).string(message.strChat);
        writer.uint32(42).string(message.strNowTime);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(50).fork()).ldelim();
        return writer;
    };

    CS_ChatInAllServer_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ChatInAllServer_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            case 2:
                message.strNickname = reader.string();
                break;
            case 3:
                message.nVIPLevel = reader.sint32();
                break;
            case 4:
                message.strChat = reader.string();
                break;
            case 5:
                message.strNowTime = reader.string();
                break;
            case 6:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("strNickname"))
            throw $util.ProtocolError("missing required 'strNickname'", { instance: message });
        if (!message.hasOwnProperty("nVIPLevel"))
            throw $util.ProtocolError("missing required 'nVIPLevel'", { instance: message });
        if (!message.hasOwnProperty("strChat"))
            throw $util.ProtocolError("missing required 'strChat'", { instance: message });
        if (!message.hasOwnProperty("strNowTime"))
            throw $util.ProtocolError("missing required 'strNowTime'", { instance: message });
        return message;
    };

    return CS_ChatInAllServer_Res;
})();

$root.CS_ChatInRoom_Res = (function() {

    function CS_ChatInRoom_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ChatInRoom_Res.prototype.nSeatID = 0;
    CS_ChatInRoom_Res.prototype.strChat = "";
    CS_ChatInRoom_Res.prototype.strNowTime = "";

    CS_ChatInRoom_Res.create = function create(properties) {
        return new CS_ChatInRoom_Res(properties);
    };

    CS_ChatInRoom_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        writer.uint32(18).string(message.strChat);
        writer.uint32(26).string(message.strNowTime);
        return writer;
    };

    CS_ChatInRoom_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ChatInRoom_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            case 2:
                message.strChat = reader.string();
                break;
            case 3:
                message.strNowTime = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("strChat"))
            throw $util.ProtocolError("missing required 'strChat'", { instance: message });
        if (!message.hasOwnProperty("strNowTime"))
            throw $util.ProtocolError("missing required 'strNowTime'", { instance: message });
        return message;
    };

    return CS_ChatInRoom_Res;
})();

$root.CS_ChatInRoom = (function() {

    function CS_ChatInRoom(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ChatInRoom.prototype.nChatType = 0;
    CS_ChatInRoom.prototype.strChat = "";

    CS_ChatInRoom.create = function create(properties) {
        return new CS_ChatInRoom(properties);
    };

    CS_ChatInRoom.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nChatType);
        writer.uint32(18).string(message.strChat);
        return writer;
    };

    CS_ChatInRoom.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ChatInRoom();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nChatType = reader.sint32();
                break;
            case 2:
                message.strChat = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nChatType"))
            throw $util.ProtocolError("missing required 'nChatType'", { instance: message });
        if (!message.hasOwnProperty("strChat"))
            throw $util.ProtocolError("missing required 'strChat'", { instance: message });
        return message;
    };

    return CS_ChatInRoom;
})();

$root.CS_DoleCoolDown_Res = (function() {

    function CS_DoleCoolDown_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_DoleCoolDown_Res.prototype.nDoleID = 0;
    CS_DoleCoolDown_Res.prototype.strCoolDownTime = "";
    CS_DoleCoolDown_Res.prototype.nGold = 0;
    CS_DoleCoolDown_Res.prototype.nDoleNum = 0;
    CS_DoleCoolDown_Res.prototype.nSeatID = 0;

    CS_DoleCoolDown_Res.create = function create(properties) {
        return new CS_DoleCoolDown_Res(properties);
    };

    CS_DoleCoolDown_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nDoleID);
        writer.uint32(18).string(message.strCoolDownTime);
        writer.uint32(24).sint32(message.nGold);
        writer.uint32(32).sint32(message.nDoleNum);
        writer.uint32(40).sint32(message.nSeatID);
        return writer;
    };

    CS_DoleCoolDown_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_DoleCoolDown_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nDoleID = reader.sint32();
                break;
            case 2:
                message.strCoolDownTime = reader.string();
                break;
            case 3:
                message.nGold = reader.sint32();
                break;
            case 4:
                message.nDoleNum = reader.sint32();
                break;
            case 5:
                message.nSeatID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nDoleID"))
            throw $util.ProtocolError("missing required 'nDoleID'", { instance: message });
        if (!message.hasOwnProperty("strCoolDownTime"))
            throw $util.ProtocolError("missing required 'strCoolDownTime'", { instance: message });
        if (!message.hasOwnProperty("nGold"))
            throw $util.ProtocolError("missing required 'nGold'", { instance: message });
        if (!message.hasOwnProperty("nDoleNum"))
            throw $util.ProtocolError("missing required 'nDoleNum'", { instance: message });
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        return message;
    };

    return CS_DoleCoolDown_Res;
})();

$root.CS_DoleCoolDown = (function() {

    function CS_DoleCoolDown(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_DoleCoolDown.prototype.nSeatID = 0;

    CS_DoleCoolDown.create = function create(properties) {
        return new CS_DoleCoolDown(properties);
    };

    CS_DoleCoolDown.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        return writer;
    };

    CS_DoleCoolDown.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_DoleCoolDown();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        return message;
    };

    return CS_DoleCoolDown;
})();

$root.CS_DoleGet_Res = (function() {

    function CS_DoleGet_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_DoleGet_Res.prototype.nGold = 0;
    CS_DoleGet_Res.prototype.nLastTime = 0;
    CS_DoleGet_Res.prototype.nSeatID = 0;

    CS_DoleGet_Res.create = function create(properties) {
        return new CS_DoleGet_Res(properties);
    };

    CS_DoleGet_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nGold);
        writer.uint32(16).sint32(message.nLastTime);
        writer.uint32(24).sint32(message.nSeatID);
        return writer;
    };

    CS_DoleGet_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_DoleGet_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nGold = reader.sint32();
                break;
            case 2:
                message.nLastTime = reader.sint32();
                break;
            case 3:
                message.nSeatID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nGold"))
            throw $util.ProtocolError("missing required 'nGold'", { instance: message });
        if (!message.hasOwnProperty("nLastTime"))
            throw $util.ProtocolError("missing required 'nLastTime'", { instance: message });
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        return message;
    };

    return CS_DoleGet_Res;
})();

$root.CS_DoleGet = (function() {

    function CS_DoleGet(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_DoleGet.prototype.nSeatID = 0;

    CS_DoleGet.create = function create(properties) {
        return new CS_DoleGet(properties);
    };

    CS_DoleGet.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        return writer;
    };

    CS_DoleGet.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_DoleGet();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        return message;
    };

    return CS_DoleGet;
})();

$root.CS_GrandPrixCatch_Res = (function() {

    function CS_GrandPrixCatch_Res(properties) {
        this.listFish = [];
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_GrandPrixCatch_Res.prototype.strBulletID = "";
    CS_GrandPrixCatch_Res.prototype.nSeatID = 0;
    CS_GrandPrixCatch_Res.prototype.nGoldNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_GrandPrixCatch_Res.prototype.listFish = $util.emptyArray;
    CS_GrandPrixCatch_Res.prototype.nScore = 0;
    CS_GrandPrixCatch_Res.prototype.nExp = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_GrandPrixCatch_Res.prototype.nidLogin = 0;
    CS_GrandPrixCatch_Res.prototype.strRoomID = "";
    CS_GrandPrixCatch_Res.prototype.itemReward = $util.emptyArray;
    CS_GrandPrixCatch_Res.prototype.nJackpot = 0;
    CS_GrandPrixCatch_Res.prototype.nBulletLevel = 0;
    CS_GrandPrixCatch_Res.prototype.nBulletNum = 0;
    CS_GrandPrixCatch_Res.prototype.nIntegral = 0;

    CS_GrandPrixCatch_Res.create = function create(properties) {
        return new CS_GrandPrixCatch_Res(properties);
    };

    CS_GrandPrixCatch_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strBulletID);
        writer.uint32(16).sint32(message.nSeatID);
        writer.uint32(24).sint64(message.nGoldNum);
        if (message.listFish != null && message.listFish.length)
            for (var i = 0; i < message.listFish.length; ++i)
                $root.FishCatch_Room.encode(message.listFish[i], writer.uint32(34).fork()).ldelim();
        writer.uint32(40).sint32(message.nScore);
        writer.uint32(48).sint64(message.nExp);
        if (message.nidLogin != null && Object.hasOwnProperty.call(message, "nidLogin"))
            writer.uint32(56).sint32(message.nidLogin);
        if (message.strRoomID != null && Object.hasOwnProperty.call(message, "strRoomID"))
            writer.uint32(66).string(message.strRoomID);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(74).fork()).ldelim();
        if (message.nJackpot != null && Object.hasOwnProperty.call(message, "nJackpot"))
            writer.uint32(80).sint32(message.nJackpot);
        writer.uint32(88).sint32(message.nBulletLevel);
        writer.uint32(96).sint32(message.nBulletNum);
        writer.uint32(109).float(message.nIntegral);
        return writer;
    };

    CS_GrandPrixCatch_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_GrandPrixCatch_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strBulletID = reader.string();
                break;
            case 2:
                message.nSeatID = reader.sint32();
                break;
            case 3:
                message.nGoldNum = reader.sint64();
                break;
            case 4:
                if (!(message.listFish && message.listFish.length))
                    message.listFish = [];
                message.listFish.push($root.FishCatch_Room.decode(reader, reader.uint32()));
                break;
            case 5:
                message.nScore = reader.sint32();
                break;
            case 6:
                message.nExp = reader.sint64();
                break;
            case 7:
                message.nidLogin = reader.sint32();
                break;
            case 8:
                message.strRoomID = reader.string();
                break;
            case 9:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            case 10:
                message.nJackpot = reader.sint32();
                break;
            case 11:
                message.nBulletLevel = reader.sint32();
                break;
            case 12:
                message.nBulletNum = reader.sint32();
                break;
            case 13:
                message.nIntegral = reader.float();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strBulletID"))
            throw $util.ProtocolError("missing required 'strBulletID'", { instance: message });
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("nGoldNum"))
            throw $util.ProtocolError("missing required 'nGoldNum'", { instance: message });
        if (!message.hasOwnProperty("nScore"))
            throw $util.ProtocolError("missing required 'nScore'", { instance: message });
        if (!message.hasOwnProperty("nExp"))
            throw $util.ProtocolError("missing required 'nExp'", { instance: message });
        if (!message.hasOwnProperty("nBulletLevel"))
            throw $util.ProtocolError("missing required 'nBulletLevel'", { instance: message });
        if (!message.hasOwnProperty("nBulletNum"))
            throw $util.ProtocolError("missing required 'nBulletNum'", { instance: message });
        if (!message.hasOwnProperty("nIntegral"))
            throw $util.ProtocolError("missing required 'nIntegral'", { instance: message });
        return message;
    };

    return CS_GrandPrixCatch_Res;
})();

$root.FishCatch_Room = (function() {

    function FishCatch_Room(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    FishCatch_Room.prototype.strFishID = "";
    FishCatch_Room.prototype.nFishScore = 0;

    FishCatch_Room.create = function create(properties) {
        return new FishCatch_Room(properties);
    };

    FishCatch_Room.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strFishID);
        writer.uint32(16).sint32(message.nFishScore);
        return writer;
    };

    FishCatch_Room.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.FishCatch_Room();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strFishID = reader.string();
                break;
            case 2:
                message.nFishScore = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strFishID"))
            throw $util.ProtocolError("missing required 'strFishID'", { instance: message });
        if (!message.hasOwnProperty("nFishScore"))
            throw $util.ProtocolError("missing required 'nFishScore'", { instance: message });
        return message;
    };

    return FishCatch_Room;
})();

$root.CS_InRoomReceiveMonthCardGift_Res = (function() {

    function CS_InRoomReceiveMonthCardGift_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_InRoomReceiveMonthCardGift_Res.prototype.nRes = 0;
    CS_InRoomReceiveMonthCardGift_Res.prototype.itemReward = $util.emptyArray;

    CS_InRoomReceiveMonthCardGift_Res.create = function create(properties) {
        return new CS_InRoomReceiveMonthCardGift_Res(properties);
    };

    CS_InRoomReceiveMonthCardGift_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(18).fork()).ldelim();
        return writer;
    };

    CS_InRoomReceiveMonthCardGift_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_InRoomReceiveMonthCardGift_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        return message;
    };

    return CS_InRoomReceiveMonthCardGift_Res;
})();

$root.CS_InRoomReceiveMonthCardGift = (function() {

    function CS_InRoomReceiveMonthCardGift(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_InRoomReceiveMonthCardGift.prototype.nidLogin = 0;
    CS_InRoomReceiveMonthCardGift.prototype.strSafeCode = "";

    CS_InRoomReceiveMonthCardGift.create = function create(properties) {
        return new CS_InRoomReceiveMonthCardGift(properties);
    };

    CS_InRoomReceiveMonthCardGift.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        return writer;
    };

    CS_InRoomReceiveMonthCardGift.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_InRoomReceiveMonthCardGift();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CS_InRoomReceiveMonthCardGift;
})();

$root.CS_JackpotDraw_Res = (function() {

    function CS_JackpotDraw_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_JackpotDraw_Res.prototype.nDrawIndex = 0;
    CS_JackpotDraw_Res.prototype.itemReward = $util.emptyArray;

    CS_JackpotDraw_Res.create = function create(properties) {
        return new CS_JackpotDraw_Res(properties);
    };

    CS_JackpotDraw_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nDrawIndex);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(18).fork()).ldelim();
        return writer;
    };

    CS_JackpotDraw_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_JackpotDraw_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nDrawIndex = reader.sint32();
                break;
            case 2:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nDrawIndex"))
            throw $util.ProtocolError("missing required 'nDrawIndex'", { instance: message });
        return message;
    };

    return CS_JackpotDraw_Res;
})();

$root.CS_JackpotDraw = (function() {

    function CS_JackpotDraw(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_JackpotDraw.prototype.nDrawIndex = 0;

    CS_JackpotDraw.create = function create(properties) {
        return new CS_JackpotDraw(properties);
    };

    CS_JackpotDraw.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nDrawIndex);
        return writer;
    };

    CS_JackpotDraw.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_JackpotDraw();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nDrawIndex = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nDrawIndex"))
            throw $util.ProtocolError("missing required 'nDrawIndex'", { instance: message });
        return message;
    };

    return CS_JackpotDraw;
})();

$root.CS_JoinCommStageMsg_Res = (function() {

    function CS_JoinCommStageMsg_Res(properties) {
        this.playerInfoRoom = [];
        this.fishInfoRoom = [];
        this.skillCDTimeList = [];
        this.attrBullet = [];
        this.attrBall = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_JoinCommStageMsg_Res.prototype.nRes = 0;
    CS_JoinCommStageMsg_Res.prototype.strRoomID = "";
    CS_JoinCommStageMsg_Res.prototype.playerInfoRoom = $util.emptyArray;
    CS_JoinCommStageMsg_Res.prototype.nMaxNum = 0;
    CS_JoinCommStageMsg_Res.prototype.fishInfoRoom = $util.emptyArray;
    CS_JoinCommStageMsg_Res.prototype.strTime = "";
    CS_JoinCommStageMsg_Res.prototype.strSeed = "";
    CS_JoinCommStageMsg_Res.prototype.nidLogin = 0;
    CS_JoinCommStageMsg_Res.prototype.nOnlineRewardTime = 0;
    CS_JoinCommStageMsg_Res.prototype.nOnlineReward = 0;
    CS_JoinCommStageMsg_Res.prototype.strFortSkin = "";
    CS_JoinCommStageMsg_Res.prototype.fValue = 0;
    CS_JoinCommStageMsg_Res.prototype.nRebound = 0;
    CS_JoinCommStageMsg_Res.prototype.nBonusPools = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_JoinCommStageMsg_Res.prototype.nBillingWelfare = 0;
    CS_JoinCommStageMsg_Res.prototype.nSinglePairCannon = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_JoinCommStageMsg_Res.prototype.nPokerNum = 0;
    CS_JoinCommStageMsg_Res.prototype.skillCDTimeList = $util.emptyArray;
    CS_JoinCommStageMsg_Res.prototype.attrBullet = $util.emptyArray;
    CS_JoinCommStageMsg_Res.prototype.attrBall = $util.emptyArray;
    CS_JoinCommStageMsg_Res.prototype.collectionNum = 0;

    CS_JoinCommStageMsg_Res.create = function create(properties) {
        return new CS_JoinCommStageMsg_Res(properties);
    };

    CS_JoinCommStageMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        if (message.strRoomID != null && Object.hasOwnProperty.call(message, "strRoomID"))
            writer.uint32(18).string(message.strRoomID);
        if (message.playerInfoRoom != null && message.playerInfoRoom.length)
            for (var i = 0; i < message.playerInfoRoom.length; ++i)
                $root.PlayerInfo_Room.encode(message.playerInfoRoom[i], writer.uint32(26).fork()).ldelim();
        writer.uint32(32).sint32(message.nMaxNum);
        if (message.fishInfoRoom != null && message.fishInfoRoom.length)
            for (var i = 0; i < message.fishInfoRoom.length; ++i)
                $root.FishGroup_Room.encode(message.fishInfoRoom[i], writer.uint32(42).fork()).ldelim();
        writer.uint32(50).string(message.strTime);
        writer.uint32(58).string(message.strSeed);
        if (message.nidLogin != null && Object.hasOwnProperty.call(message, "nidLogin"))
            writer.uint32(64).sint32(message.nidLogin);
        writer.uint32(72).int32(message.nOnlineRewardTime);
        writer.uint32(80).int32(message.nOnlineReward);
        writer.uint32(90).string(message.strFortSkin);
        if (message.fValue != null && Object.hasOwnProperty.call(message, "fValue"))
            writer.uint32(101).float(message.fValue);
        writer.uint32(104).sint32(message.nRebound);
        writer.uint32(112).sint64(message.nBonusPools);
        writer.uint32(120).int32(message.nBillingWelfare);
        writer.uint32(128).sint64(message.nSinglePairCannon);
        writer.uint32(136).sint32(message.nPokerNum);
        if (message.skillCDTimeList != null && message.skillCDTimeList.length)
            for (var i = 0; i < message.skillCDTimeList.length; ++i)
                $root.SkillInfo_Room.encode(message.skillCDTimeList[i], writer.uint32(146).fork()).ldelim();
        if (message.attrBullet != null && message.attrBullet.length)
            for (var i = 0; i < message.attrBullet.length; ++i)
                writer.uint32(152).sint32(message.attrBullet[i]);
        if (message.attrBall != null && message.attrBall.length)
            for (var i = 0; i < message.attrBall.length; ++i)
                writer.uint32(160).sint32(message.attrBall[i]);
        if (message.collectionNum != null && Object.hasOwnProperty.call(message, "collectionNum"))
            writer.uint32(168).sint32(message.collectionNum);
        return writer;
    };

    CS_JoinCommStageMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_JoinCommStageMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                message.strRoomID = reader.string();
                break;
            case 3:
                if (!(message.playerInfoRoom && message.playerInfoRoom.length))
                    message.playerInfoRoom = [];
                message.playerInfoRoom.push($root.PlayerInfo_Room.decode(reader, reader.uint32()));
                break;
            case 4:
                message.nMaxNum = reader.sint32();
                break;
            case 5:
                if (!(message.fishInfoRoom && message.fishInfoRoom.length))
                    message.fishInfoRoom = [];
                message.fishInfoRoom.push($root.FishGroup_Room.decode(reader, reader.uint32()));
                break;
            case 6:
                message.strTime = reader.string();
                break;
            case 7:
                message.strSeed = reader.string();
                break;
            case 8:
                message.nidLogin = reader.sint32();
                break;
            case 9:
                message.nOnlineRewardTime = reader.int32();
                break;
            case 10:
                message.nOnlineReward = reader.int32();
                break;
            case 11:
                message.strFortSkin = reader.string();
                break;
            case 12:
                message.fValue = reader.float();
                break;
            case 13:
                message.nRebound = reader.sint32();
                break;
            case 14:
                message.nBonusPools = reader.sint64();
                break;
            case 15:
                message.nBillingWelfare = reader.int32();
                break;
            case 16:
                message.nSinglePairCannon = reader.sint64();
                break;
            case 17:
                message.nPokerNum = reader.sint32();
                break;
            case 18:
                if (!(message.skillCDTimeList && message.skillCDTimeList.length))
                    message.skillCDTimeList = [];
                message.skillCDTimeList.push($root.SkillInfo_Room.decode(reader, reader.uint32()));
                break;
            case 19:
                if (!(message.attrBullet && message.attrBullet.length))
                    message.attrBullet = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.attrBullet.push(reader.sint32());
                } else
                    message.attrBullet.push(reader.sint32());
                break;
            case 20:
                if (!(message.attrBall && message.attrBall.length))
                    message.attrBall = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.attrBall.push(reader.sint32());
                } else
                    message.attrBall.push(reader.sint32());
                break;
            case 21:
                message.collectionNum = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        if (!message.hasOwnProperty("nMaxNum"))
            throw $util.ProtocolError("missing required 'nMaxNum'", { instance: message });
        if (!message.hasOwnProperty("strTime"))
            throw $util.ProtocolError("missing required 'strTime'", { instance: message });
        if (!message.hasOwnProperty("strSeed"))
            throw $util.ProtocolError("missing required 'strSeed'", { instance: message });
        if (!message.hasOwnProperty("nOnlineRewardTime"))
            throw $util.ProtocolError("missing required 'nOnlineRewardTime'", { instance: message });
        if (!message.hasOwnProperty("nOnlineReward"))
            throw $util.ProtocolError("missing required 'nOnlineReward'", { instance: message });
        if (!message.hasOwnProperty("strFortSkin"))
            throw $util.ProtocolError("missing required 'strFortSkin'", { instance: message });
        if (!message.hasOwnProperty("nRebound"))
            throw $util.ProtocolError("missing required 'nRebound'", { instance: message });
        if (!message.hasOwnProperty("nBonusPools"))
            throw $util.ProtocolError("missing required 'nBonusPools'", { instance: message });
        if (!message.hasOwnProperty("nBillingWelfare"))
            throw $util.ProtocolError("missing required 'nBillingWelfare'", { instance: message });
        if (!message.hasOwnProperty("nSinglePairCannon"))
            throw $util.ProtocolError("missing required 'nSinglePairCannon'", { instance: message });
        if (!message.hasOwnProperty("nPokerNum"))
            throw $util.ProtocolError("missing required 'nPokerNum'", { instance: message });
        return message;
    };

    return CS_JoinCommStageMsg_Res;
})();

$root.PlayerInfo_Room = (function() {

    function PlayerInfo_Room(properties) {
        this.bulletInfoRoom = [];
        this.skillInfoRoom = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    PlayerInfo_Room.prototype.strGamePlayerID = "";
    PlayerInfo_Room.prototype.strNickname = "";
    PlayerInfo_Room.prototype.nLevel = 0;
    PlayerInfo_Room.prototype.nVIPLevel = 0;
    PlayerInfo_Room.prototype.nHeadID = 0;
    PlayerInfo_Room.prototype.nGoldNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    PlayerInfo_Room.prototype.nDiamondNum = 0;
    PlayerInfo_Room.prototype.nSeatID = 0;
    PlayerInfo_Room.prototype.nFortType = 0;
    PlayerInfo_Room.prototype.nFortMode = 0;
    PlayerInfo_Room.prototype.bulletInfoRoom = $util.emptyArray;
    PlayerInfo_Room.prototype.nFortSkin = 0;
    PlayerInfo_Room.prototype.nMaxFort = 0;
    PlayerInfo_Room.prototype.nDoleCoolDownTime = 0;
    PlayerInfo_Room.prototype.nRebound = 0;
    PlayerInfo_Room.prototype.skillInfoRoom = $util.emptyArray;

    PlayerInfo_Room.create = function create(properties) {
        return new PlayerInfo_Room(properties);
    };

    PlayerInfo_Room.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strGamePlayerID);
        writer.uint32(18).string(message.strNickname);
        writer.uint32(24).sint32(message.nLevel);
        writer.uint32(32).sint32(message.nVIPLevel);
        writer.uint32(40).sint32(message.nHeadID);
        writer.uint32(48).sint64(message.nGoldNum);
        writer.uint32(56).sint32(message.nDiamondNum);
        writer.uint32(64).sint32(message.nSeatID);
        writer.uint32(72).sint32(message.nFortType);
        writer.uint32(80).sint32(message.nFortMode);
        if (message.bulletInfoRoom != null && message.bulletInfoRoom.length)
            for (var i = 0; i < message.bulletInfoRoom.length; ++i)
                $root.BulletInfo_Room.encode(message.bulletInfoRoom[i], writer.uint32(90).fork()).ldelim();
        writer.uint32(96).sint32(message.nFortSkin);
        writer.uint32(104).sint32(message.nMaxFort);
        writer.uint32(112).sint32(message.nDoleCoolDownTime);
        writer.uint32(120).sint32(message.nRebound);
        if (message.skillInfoRoom != null && message.skillInfoRoom.length)
            for (var i = 0; i < message.skillInfoRoom.length; ++i)
                $root.SkillInfo_Room.encode(message.skillInfoRoom[i], writer.uint32(130).fork()).ldelim();
        return writer;
    };

    PlayerInfo_Room.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PlayerInfo_Room();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strGamePlayerID = reader.string();
                break;
            case 2:
                message.strNickname = reader.string();
                break;
            case 3:
                message.nLevel = reader.sint32();
                break;
            case 4:
                message.nVIPLevel = reader.sint32();
                break;
            case 5:
                message.nHeadID = reader.sint32();
                break;
            case 6:
                message.nGoldNum = reader.sint64();
                break;
            case 7:
                message.nDiamondNum = reader.sint32();
                break;
            case 8:
                message.nSeatID = reader.sint32();
                break;
            case 9:
                message.nFortType = reader.sint32();
                break;
            case 10:
                message.nFortMode = reader.sint32();
                break;
            case 11:
                if (!(message.bulletInfoRoom && message.bulletInfoRoom.length))
                    message.bulletInfoRoom = [];
                message.bulletInfoRoom.push($root.BulletInfo_Room.decode(reader, reader.uint32()));
                break;
            case 12:
                message.nFortSkin = reader.sint32();
                break;
            case 13:
                message.nMaxFort = reader.sint32();
                break;
            case 14:
                message.nDoleCoolDownTime = reader.sint32();
                break;
            case 15:
                message.nRebound = reader.sint32();
                break;
            case 16:
                if (!(message.skillInfoRoom && message.skillInfoRoom.length))
                    message.skillInfoRoom = [];
                message.skillInfoRoom.push($root.SkillInfo_Room.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strGamePlayerID"))
            throw $util.ProtocolError("missing required 'strGamePlayerID'", { instance: message });
        if (!message.hasOwnProperty("strNickname"))
            throw $util.ProtocolError("missing required 'strNickname'", { instance: message });
        if (!message.hasOwnProperty("nLevel"))
            throw $util.ProtocolError("missing required 'nLevel'", { instance: message });
        if (!message.hasOwnProperty("nVIPLevel"))
            throw $util.ProtocolError("missing required 'nVIPLevel'", { instance: message });
        if (!message.hasOwnProperty("nHeadID"))
            throw $util.ProtocolError("missing required 'nHeadID'", { instance: message });
        if (!message.hasOwnProperty("nGoldNum"))
            throw $util.ProtocolError("missing required 'nGoldNum'", { instance: message });
        if (!message.hasOwnProperty("nDiamondNum"))
            throw $util.ProtocolError("missing required 'nDiamondNum'", { instance: message });
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("nFortType"))
            throw $util.ProtocolError("missing required 'nFortType'", { instance: message });
        if (!message.hasOwnProperty("nFortMode"))
            throw $util.ProtocolError("missing required 'nFortMode'", { instance: message });
        if (!message.hasOwnProperty("nFortSkin"))
            throw $util.ProtocolError("missing required 'nFortSkin'", { instance: message });
        if (!message.hasOwnProperty("nMaxFort"))
            throw $util.ProtocolError("missing required 'nMaxFort'", { instance: message });
        if (!message.hasOwnProperty("nDoleCoolDownTime"))
            throw $util.ProtocolError("missing required 'nDoleCoolDownTime'", { instance: message });
        if (!message.hasOwnProperty("nRebound"))
            throw $util.ProtocolError("missing required 'nRebound'", { instance: message });
        return message;
    };

    return PlayerInfo_Room;
})();

$root.SkillInfo_Room = (function() {

    function SkillInfo_Room(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SkillInfo_Room.prototype.nSkillID = 0;
    SkillInfo_Room.prototype.strEndTime = "";

    SkillInfo_Room.create = function create(properties) {
        return new SkillInfo_Room(properties);
    };

    SkillInfo_Room.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSkillID);
        writer.uint32(18).string(message.strEndTime);
        return writer;
    };

    SkillInfo_Room.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SkillInfo_Room();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSkillID = reader.sint32();
                break;
            case 2:
                message.strEndTime = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSkillID"))
            throw $util.ProtocolError("missing required 'nSkillID'", { instance: message });
        if (!message.hasOwnProperty("strEndTime"))
            throw $util.ProtocolError("missing required 'strEndTime'", { instance: message });
        return message;
    };

    return SkillInfo_Room;
})();

$root.FishGroup_Room = (function() {

    function FishGroup_Room(properties) {
        this.vecFishInfo = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    FishGroup_Room.prototype.nFishGroupID = 0;
    FishGroup_Room.prototype.nPathID = 0;
    FishGroup_Room.prototype.nFormat = 0;
    FishGroup_Room.prototype.strShowTime = "";
    FishGroup_Room.prototype.strIndex = "";
    FishGroup_Room.prototype.strSkillIceEndTime = "";
    FishGroup_Room.prototype.vecFishInfo = $util.emptyArray;
    FishGroup_Room.prototype.bTide = false;
    FishGroup_Room.prototype.bFeatsFish = false;

    FishGroup_Room.create = function create(properties) {
        return new FishGroup_Room(properties);
    };

    FishGroup_Room.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nFishGroupID);
        writer.uint32(16).sint32(message.nPathID);
        writer.uint32(24).sint32(message.nFormat);
        writer.uint32(34).string(message.strShowTime);
        writer.uint32(42).string(message.strIndex);
        if (message.strSkillIceEndTime != null && Object.hasOwnProperty.call(message, "strSkillIceEndTime"))
            writer.uint32(50).string(message.strSkillIceEndTime);
        if (message.vecFishInfo != null && message.vecFishInfo.length)
            for (var i = 0; i < message.vecFishInfo.length; ++i)
                $root.FishInfo_Room.encode(message.vecFishInfo[i], writer.uint32(58).fork()).ldelim();
        if (message.bTide != null && Object.hasOwnProperty.call(message, "bTide"))
            writer.uint32(64).bool(message.bTide);
        if (message.bFeatsFish != null && Object.hasOwnProperty.call(message, "bFeatsFish"))
            writer.uint32(72).bool(message.bFeatsFish);
        return writer;
    };

    FishGroup_Room.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.FishGroup_Room();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nFishGroupID = reader.sint32();
                break;
            case 2:
                message.nPathID = reader.sint32();
                break;
            case 3:
                message.nFormat = reader.sint32();
                break;
            case 4:
                message.strShowTime = reader.string();
                break;
            case 5:
                message.strIndex = reader.string();
                break;
            case 6:
                message.strSkillIceEndTime = reader.string();
                break;
            case 7:
                if (!(message.vecFishInfo && message.vecFishInfo.length))
                    message.vecFishInfo = [];
                message.vecFishInfo.push($root.FishInfo_Room.decode(reader, reader.uint32()));
                break;
            case 8:
                message.bTide = reader.bool();
                break;
            case 9:
                message.bFeatsFish = reader.bool();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nFishGroupID"))
            throw $util.ProtocolError("missing required 'nFishGroupID'", { instance: message });
        if (!message.hasOwnProperty("nPathID"))
            throw $util.ProtocolError("missing required 'nPathID'", { instance: message });
        if (!message.hasOwnProperty("nFormat"))
            throw $util.ProtocolError("missing required 'nFormat'", { instance: message });
        if (!message.hasOwnProperty("strShowTime"))
            throw $util.ProtocolError("missing required 'strShowTime'", { instance: message });
        if (!message.hasOwnProperty("strIndex"))
            throw $util.ProtocolError("missing required 'strIndex'", { instance: message });
        return message;
    };

    return FishGroup_Room;
})();

$root.FishInfo_Room = (function() {

    function FishInfo_Room(properties) {
        this.lockInfo = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    FishInfo_Room.prototype.nFishIndex = 0;
    FishInfo_Room.prototype.strFishIndex = "";
    FishInfo_Room.prototype.strStartTime = "";
    FishInfo_Room.prototype.nNowSpeed = 0;
    FishInfo_Room.prototype.nNowSeatID = -1;
    FishInfo_Room.prototype.nLockLastTime = 0;
    FishInfo_Room.prototype.strLockStartTime = "";
    FishInfo_Room.prototype.lockInfo = $util.emptyArray;

    FishInfo_Room.create = function create(properties) {
        return new FishInfo_Room(properties);
    };

    FishInfo_Room.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nFishIndex);
        writer.uint32(18).string(message.strFishIndex);
        writer.uint32(26).string(message.strStartTime);
        writer.uint32(32).sint32(message.nNowSpeed);
        if (message.nNowSeatID != null && Object.hasOwnProperty.call(message, "nNowSeatID"))
            writer.uint32(40).sint32(message.nNowSeatID);
        if (message.nLockLastTime != null && Object.hasOwnProperty.call(message, "nLockLastTime"))
            writer.uint32(48).sint32(message.nLockLastTime);
        if (message.strLockStartTime != null && Object.hasOwnProperty.call(message, "strLockStartTime"))
            writer.uint32(58).string(message.strLockStartTime);
        if (message.lockInfo != null && message.lockInfo.length)
            for (var i = 0; i < message.lockInfo.length; ++i)
                $root.LockFishInfo.encode(message.lockInfo[i], writer.uint32(66).fork()).ldelim();
        return writer;
    };

    FishInfo_Room.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.FishInfo_Room();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nFishIndex = reader.sint32();
                break;
            case 2:
                message.strFishIndex = reader.string();
                break;
            case 3:
                message.strStartTime = reader.string();
                break;
            case 4:
                message.nNowSpeed = reader.sint32();
                break;
            case 5:
                message.nNowSeatID = reader.sint32();
                break;
            case 6:
                message.nLockLastTime = reader.sint32();
                break;
            case 7:
                message.strLockStartTime = reader.string();
                break;
            case 8:
                if (!(message.lockInfo && message.lockInfo.length))
                    message.lockInfo = [];
                message.lockInfo.push($root.LockFishInfo.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nFishIndex"))
            throw $util.ProtocolError("missing required 'nFishIndex'", { instance: message });
        if (!message.hasOwnProperty("strFishIndex"))
            throw $util.ProtocolError("missing required 'strFishIndex'", { instance: message });
        if (!message.hasOwnProperty("strStartTime"))
            throw $util.ProtocolError("missing required 'strStartTime'", { instance: message });
        if (!message.hasOwnProperty("nNowSpeed"))
            throw $util.ProtocolError("missing required 'nNowSpeed'", { instance: message });
        return message;
    };

    return FishInfo_Room;
})();

$root.LockFishInfo = (function() {

    function LockFishInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    LockFishInfo.prototype.nSeatID = 0;
    LockFishInfo.prototype.nSpeed = 0;
    LockFishInfo.prototype.nLastTime = 0;

    LockFishInfo.create = function create(properties) {
        return new LockFishInfo(properties);
    };

    LockFishInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        writer.uint32(16).sint32(message.nSpeed);
        writer.uint32(24).sint32(message.nLastTime);
        return writer;
    };

    LockFishInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.LockFishInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            case 2:
                message.nSpeed = reader.sint32();
                break;
            case 3:
                message.nLastTime = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("nSpeed"))
            throw $util.ProtocolError("missing required 'nSpeed'", { instance: message });
        if (!message.hasOwnProperty("nLastTime"))
            throw $util.ProtocolError("missing required 'nLastTime'", { instance: message });
        return message;
    };

    return LockFishInfo;
})();

$root.CS_JoinCommStageMsg = (function() {

    function CS_JoinCommStageMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_JoinCommStageMsg.prototype.nidLogin = 0;
    CS_JoinCommStageMsg.prototype.strSafeCode = "";
    CS_JoinCommStageMsg.prototype.nStageType = 0;
    CS_JoinCommStageMsg.prototype.nActivityID = 0;
    CS_JoinCommStageMsg.prototype.roomid = "";

    CS_JoinCommStageMsg.create = function create(properties) {
        return new CS_JoinCommStageMsg(properties);
    };

    CS_JoinCommStageMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nStageType);
        writer.uint32(32).sint32(message.nActivityID);
        if (message.roomid != null && Object.hasOwnProperty.call(message, "roomid"))
            writer.uint32(42).string(message.roomid);
        return writer;
    };

    CS_JoinCommStageMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_JoinCommStageMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nStageType = reader.sint32();
                break;
            case 4:
                message.nActivityID = reader.sint32();
                break;
            case 5:
                message.roomid = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nStageType"))
            throw $util.ProtocolError("missing required 'nStageType'", { instance: message });
        if (!message.hasOwnProperty("nActivityID"))
            throw $util.ProtocolError("missing required 'nActivityID'", { instance: message });
        return message;
    };

    return CS_JoinCommStageMsg;
})();

$root.CS_LeaveRoomMsg_Res = (function() {

    function CS_LeaveRoomMsg_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_LeaveRoomMsg_Res.prototype.nRes = 0;
    CS_LeaveRoomMsg_Res.prototype.strFortSkin = "";

    CS_LeaveRoomMsg_Res.create = function create(properties) {
        return new CS_LeaveRoomMsg_Res(properties);
    };

    CS_LeaveRoomMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        writer.uint32(18).string(message.strFortSkin);
        return writer;
    };

    CS_LeaveRoomMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_LeaveRoomMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                message.strFortSkin = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        if (!message.hasOwnProperty("strFortSkin"))
            throw $util.ProtocolError("missing required 'strFortSkin'", { instance: message });
        return message;
    };

    return CS_LeaveRoomMsg_Res;
})();

$root.CS_LeaveRoomMsg = (function() {

    function CS_LeaveRoomMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_LeaveRoomMsg.prototype.nidLogin = 0;
    CS_LeaveRoomMsg.prototype.nSeatID = 0;

    CS_LeaveRoomMsg.create = function create(properties) {
        return new CS_LeaveRoomMsg(properties);
    };

    CS_LeaveRoomMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.nSeatID);
        return writer;
    };

    CS_LeaveRoomMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_LeaveRoomMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nSeatID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        return message;
    };

    return CS_LeaveRoomMsg;
})();

$root.CS_ModifyFort_Res = (function() {

    function CS_ModifyFort_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ModifyFort_Res.prototype.nSeatID = 0;
    CS_ModifyFort_Res.prototype.nFortType = 0;

    CS_ModifyFort_Res.create = function create(properties) {
        return new CS_ModifyFort_Res(properties);
    };

    CS_ModifyFort_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        writer.uint32(16).sint32(message.nFortType);
        return writer;
    };

    CS_ModifyFort_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ModifyFort_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            case 2:
                message.nFortType = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("nFortType"))
            throw $util.ProtocolError("missing required 'nFortType'", { instance: message });
        return message;
    };

    return CS_ModifyFort_Res;
})();

$root.CS_ModifyFort = (function() {

    function CS_ModifyFort(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ModifyFort.prototype.bUpFort = false;

    CS_ModifyFort.create = function create(properties) {
        return new CS_ModifyFort(properties);
    };

    CS_ModifyFort.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).bool(message.bUpFort);
        return writer;
    };

    CS_ModifyFort.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ModifyFort();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.bUpFort = reader.bool();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("bUpFort"))
            throw $util.ProtocolError("missing required 'bUpFort'", { instance: message });
        return message;
    };

    return CS_ModifyFort;
})();

$root.CS_ModifyFortSkin_Res = (function() {

    function CS_ModifyFortSkin_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ModifyFortSkin_Res.prototype.nFortSkinID = 0;
    CS_ModifyFortSkin_Res.prototype.nSeatID = 0;

    CS_ModifyFortSkin_Res.create = function create(properties) {
        return new CS_ModifyFortSkin_Res(properties);
    };

    CS_ModifyFortSkin_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nFortSkinID);
        writer.uint32(16).sint32(message.nSeatID);
        return writer;
    };

    CS_ModifyFortSkin_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ModifyFortSkin_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nFortSkinID = reader.sint32();
                break;
            case 2:
                message.nSeatID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nFortSkinID"))
            throw $util.ProtocolError("missing required 'nFortSkinID'", { instance: message });
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        return message;
    };

    return CS_ModifyFortSkin_Res;
})();

$root.CS_ModifyFortSkin = (function() {

    function CS_ModifyFortSkin(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ModifyFortSkin.prototype.nFortSkinID = 0;

    CS_ModifyFortSkin.create = function create(properties) {
        return new CS_ModifyFortSkin(properties);
    };

    CS_ModifyFortSkin.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nFortSkinID);
        return writer;
    };

    CS_ModifyFortSkin.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ModifyFortSkin();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nFortSkinID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nFortSkinID"))
            throw $util.ProtocolError("missing required 'nFortSkinID'", { instance: message });
        return message;
    };

    return CS_ModifyFortSkin;
})();

$root.CS_OnlineReward_Res = (function() {

    function CS_OnlineReward_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_OnlineReward_Res.prototype.nGoldNum = 0;
    CS_OnlineReward_Res.prototype.nNextID = 0;
    CS_OnlineReward_Res.prototype.nNextTime = 0;

    CS_OnlineReward_Res.create = function create(properties) {
        return new CS_OnlineReward_Res(properties);
    };

    CS_OnlineReward_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nGoldNum);
        writer.uint32(16).sint32(message.nNextID);
        writer.uint32(24).sint32(message.nNextTime);
        return writer;
    };

    CS_OnlineReward_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_OnlineReward_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nGoldNum = reader.sint32();
                break;
            case 2:
                message.nNextID = reader.sint32();
                break;
            case 3:
                message.nNextTime = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nGoldNum"))
            throw $util.ProtocolError("missing required 'nGoldNum'", { instance: message });
        if (!message.hasOwnProperty("nNextID"))
            throw $util.ProtocolError("missing required 'nNextID'", { instance: message });
        if (!message.hasOwnProperty("nNextTime"))
            throw $util.ProtocolError("missing required 'nNextTime'", { instance: message });
        return message;
    };

    return CS_OnlineReward_Res;
})();

$root.CS_OnlineReward = (function() {

    function CS_OnlineReward(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_OnlineReward.prototype.nID = 0;

    CS_OnlineReward.create = function create(properties) {
        return new CS_OnlineReward(properties);
    };

    CS_OnlineReward.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nID);
        return writer;
    };

    CS_OnlineReward.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_OnlineReward();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nID"))
            throw $util.ProtocolError("missing required 'nID'", { instance: message });
        return message;
    };

    return CS_OnlineReward;
})();

$root.CS_OpenPokerFishMsg_Res = (function() {

    function CS_OpenPokerFishMsg_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_OpenPokerFishMsg_Res.prototype.nPokerIndex = 0;
    CS_OpenPokerFishMsg_Res.prototype.nRewardNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    CS_OpenPokerFishMsg_Res.create = function create(properties) {
        return new CS_OpenPokerFishMsg_Res(properties);
    };

    CS_OpenPokerFishMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nPokerIndex);
        writer.uint32(16).sint64(message.nRewardNum);
        return writer;
    };

    CS_OpenPokerFishMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_OpenPokerFishMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nPokerIndex = reader.sint32();
                break;
            case 2:
                message.nRewardNum = reader.sint64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nPokerIndex"))
            throw $util.ProtocolError("missing required 'nPokerIndex'", { instance: message });
        if (!message.hasOwnProperty("nRewardNum"))
            throw $util.ProtocolError("missing required 'nRewardNum'", { instance: message });
        return message;
    };

    return CS_OpenPokerFishMsg_Res;
})();

$root.CS_OpenPokerFishMsg = (function() {

    function CS_OpenPokerFishMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_OpenPokerFishMsg.create = function create(properties) {
        return new CS_OpenPokerFishMsg(properties);
    };

    CS_OpenPokerFishMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        return writer;
    };

    CS_OpenPokerFishMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_OpenPokerFishMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CS_OpenPokerFishMsg;
})();

$root.CS_OutDateFort = (function() {

    function CS_OutDateFort(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_OutDateFort.prototype.nIdLogin = 0;
    CS_OutDateFort.prototype.nSeatID = 0;
    CS_OutDateFort.prototype.nFortId = 0;
    CS_OutDateFort.prototype.nSinglePairCannon = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    CS_OutDateFort.create = function create(properties) {
        return new CS_OutDateFort(properties);
    };

    CS_OutDateFort.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).int32(message.nIdLogin);
        writer.uint32(16).int32(message.nSeatID);
        writer.uint32(24).int32(message.nFortId);
        writer.uint32(32).int64(message.nSinglePairCannon);
        return writer;
    };

    CS_OutDateFort.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_OutDateFort();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.int32();
                break;
            case 2:
                message.nSeatID = reader.int32();
                break;
            case 3:
                message.nFortId = reader.int32();
                break;
            case 4:
                message.nSinglePairCannon = reader.int64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("nFortId"))
            throw $util.ProtocolError("missing required 'nFortId'", { instance: message });
        if (!message.hasOwnProperty("nSinglePairCannon"))
            throw $util.ProtocolError("missing required 'nSinglePairCannon'", { instance: message });
        return message;
    };

    return CS_OutDateFort;
})();

$root.CS_PlayerCatch_Res = (function() {

    function CS_PlayerCatch_Res(properties) {
        this.listFish = [];
        this.itemReward = [];
        this.attrBall = [];
        this.attrBallHit = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_PlayerCatch_Res.prototype.strBulletID = "";
    CS_PlayerCatch_Res.prototype.nSeatID = 0;
    CS_PlayerCatch_Res.prototype.nGoldNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_PlayerCatch_Res.prototype.listFish = $util.emptyArray;
    CS_PlayerCatch_Res.prototype.nScore = 0;
    CS_PlayerCatch_Res.prototype.nidLogin = 0;
    CS_PlayerCatch_Res.prototype.strRoomID = "";
    CS_PlayerCatch_Res.prototype.itemReward = $util.emptyArray;
    CS_PlayerCatch_Res.prototype.nJackpot = 0;
    CS_PlayerCatch_Res.prototype.nBulletLevel = 0;
    CS_PlayerCatch_Res.prototype.nHegemony = 0;
    CS_PlayerCatch_Res.prototype.nLzNum = 0;
    CS_PlayerCatch_Res.prototype.nLzBeiShu = 0;
    CS_PlayerCatch_Res.prototype.nLzGold = 0;
    CS_PlayerCatch_Res.prototype.attrBall = $util.emptyArray;
    CS_PlayerCatch_Res.prototype.attrBallHit = $util.emptyArray;
    CS_PlayerCatch_Res.prototype.colletAttrBall = 0;
    CS_PlayerCatch_Res.prototype.mag = 0;

    CS_PlayerCatch_Res.create = function create(properties) {
        return new CS_PlayerCatch_Res(properties);
    };

    CS_PlayerCatch_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strBulletID);
        writer.uint32(16).sint32(message.nSeatID);
        writer.uint32(24).sint64(message.nGoldNum);
        if (message.listFish != null && message.listFish.length)
            for (var i = 0; i < message.listFish.length; ++i)
                $root.FishCatch_Room.encode(message.listFish[i], writer.uint32(34).fork()).ldelim();
        writer.uint32(40).sint32(message.nScore);
        if (message.nidLogin != null && Object.hasOwnProperty.call(message, "nidLogin"))
            writer.uint32(48).sint32(message.nidLogin);
        if (message.strRoomID != null && Object.hasOwnProperty.call(message, "strRoomID"))
            writer.uint32(58).string(message.strRoomID);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(66).fork()).ldelim();
        if (message.nJackpot != null && Object.hasOwnProperty.call(message, "nJackpot"))
            writer.uint32(72).sint32(message.nJackpot);
        writer.uint32(80).sint32(message.nBulletLevel);
        if (message.nHegemony != null && Object.hasOwnProperty.call(message, "nHegemony"))
            writer.uint32(88).sint32(message.nHegemony);
        if (message.nLzNum != null && Object.hasOwnProperty.call(message, "nLzNum"))
            writer.uint32(96).sint32(message.nLzNum);
        if (message.nLzBeiShu != null && Object.hasOwnProperty.call(message, "nLzBeiShu"))
            writer.uint32(104).sint32(message.nLzBeiShu);
        if (message.nLzGold != null && Object.hasOwnProperty.call(message, "nLzGold"))
            writer.uint32(112).sint32(message.nLzGold);
        if (message.attrBall != null && message.attrBall.length)
            for (var i = 0; i < message.attrBall.length; ++i)
                writer.uint32(120).sint32(message.attrBall[i]);
        if (message.attrBallHit != null && message.attrBallHit.length)
            for (var i = 0; i < message.attrBallHit.length; ++i)
                writer.uint32(128).sint32(message.attrBallHit[i]);
        if (message.colletAttrBall != null && Object.hasOwnProperty.call(message, "colletAttrBall"))
            writer.uint32(136).sint32(message.colletAttrBall);
        if (message.mag != null && Object.hasOwnProperty.call(message, "mag"))
            writer.uint32(144).sint32(message.mag);
        return writer;
    };

    CS_PlayerCatch_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_PlayerCatch_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strBulletID = reader.string();
                break;
            case 2:
                message.nSeatID = reader.sint32();
                break;
            case 3:
                message.nGoldNum = reader.sint64();
                break;
            case 4:
                if (!(message.listFish && message.listFish.length))
                    message.listFish = [];
                message.listFish.push($root.FishCatch_Room.decode(reader, reader.uint32()));
                break;
            case 5:
                message.nScore = reader.sint32();
                break;
            case 6:
                message.nidLogin = reader.sint32();
                break;
            case 7:
                message.strRoomID = reader.string();
                break;
            case 8:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            case 9:
                message.nJackpot = reader.sint32();
                break;
            case 10:
                message.nBulletLevel = reader.sint32();
                break;
            case 11:
                message.nHegemony = reader.sint32();
                break;
            case 12:
                message.nLzNum = reader.sint32();
                break;
            case 13:
                message.nLzBeiShu = reader.sint32();
                break;
            case 14:
                message.nLzGold = reader.sint32();
                break;
            case 15:
                if (!(message.attrBall && message.attrBall.length))
                    message.attrBall = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.attrBall.push(reader.sint32());
                } else
                    message.attrBall.push(reader.sint32());
                break;
            case 16:
                if (!(message.attrBallHit && message.attrBallHit.length))
                    message.attrBallHit = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.attrBallHit.push(reader.sint32());
                } else
                    message.attrBallHit.push(reader.sint32());
                break;
            case 17:
                message.colletAttrBall = reader.sint32();
                break;
            case 18:
                message.mag = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strBulletID"))
            throw $util.ProtocolError("missing required 'strBulletID'", { instance: message });
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("nGoldNum"))
            throw $util.ProtocolError("missing required 'nGoldNum'", { instance: message });
        if (!message.hasOwnProperty("nScore"))
            throw $util.ProtocolError("missing required 'nScore'", { instance: message });
        if (!message.hasOwnProperty("nBulletLevel"))
            throw $util.ProtocolError("missing required 'nBulletLevel'", { instance: message });
        return message;
    };

    return CS_PlayerCatch_Res;
})();

$root.CS_PlayerCatch = (function() {

    function CS_PlayerCatch(properties) {
        this.listFishID = [];
        this.listFishRandom = [];
        this.listFishBound = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_PlayerCatch.prototype.strBulletID = "";
    CS_PlayerCatch.prototype.listFishID = $util.emptyArray;
    CS_PlayerCatch.prototype.listFishRandom = $util.emptyArray;
    CS_PlayerCatch.prototype.listFishBound = $util.emptyArray;

    CS_PlayerCatch.create = function create(properties) {
        return new CS_PlayerCatch(properties);
    };

    CS_PlayerCatch.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strBulletID);
        if (message.listFishID != null && message.listFishID.length)
            for (var i = 0; i < message.listFishID.length; ++i)
                writer.uint32(18).string(message.listFishID[i]);
        if (message.listFishRandom != null && message.listFishRandom.length)
            for (var i = 0; i < message.listFishRandom.length; ++i)
                writer.uint32(24).int32(message.listFishRandom[i]);
        if (message.listFishBound != null && message.listFishBound.length)
            for (var i = 0; i < message.listFishBound.length; ++i)
                writer.uint32(32).int32(message.listFishBound[i]);
        return writer;
    };

    CS_PlayerCatch.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_PlayerCatch();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strBulletID = reader.string();
                break;
            case 2:
                if (!(message.listFishID && message.listFishID.length))
                    message.listFishID = [];
                message.listFishID.push(reader.string());
                break;
            case 3:
                if (!(message.listFishRandom && message.listFishRandom.length))
                    message.listFishRandom = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.listFishRandom.push(reader.int32());
                } else
                    message.listFishRandom.push(reader.int32());
                break;
            case 4:
                if (!(message.listFishBound && message.listFishBound.length))
                    message.listFishBound = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.listFishBound.push(reader.int32());
                } else
                    message.listFishBound.push(reader.int32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strBulletID"))
            throw $util.ProtocolError("missing required 'strBulletID'", { instance: message });
        return message;
    };

    return CS_PlayerCatch;
})();

$root.CS_PlayerFireMsg = (function() {

    function CS_PlayerFireMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_PlayerFireMsg.prototype.fAngleLeft = 0;
    CS_PlayerFireMsg.prototype.fAngleRight = 0;
    CS_PlayerFireMsg.prototype.strBulletIndexLeft = "";
    CS_PlayerFireMsg.prototype.strBulletIndexRight = "";
    CS_PlayerFireMsg.prototype.bulletType = 0;

    CS_PlayerFireMsg.create = function create(properties) {
        return new CS_PlayerFireMsg(properties);
    };

    CS_PlayerFireMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(13).float(message.fAngleLeft);
        writer.uint32(21).float(message.fAngleRight);
        writer.uint32(26).string(message.strBulletIndexLeft);
        writer.uint32(34).string(message.strBulletIndexRight);
        if (message.bulletType != null && Object.hasOwnProperty.call(message, "bulletType"))
            writer.uint32(40).sint32(message.bulletType);
        return writer;
    };

    CS_PlayerFireMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_PlayerFireMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.fAngleLeft = reader.float();
                break;
            case 2:
                message.fAngleRight = reader.float();
                break;
            case 3:
                message.strBulletIndexLeft = reader.string();
                break;
            case 4:
                message.strBulletIndexRight = reader.string();
                break;
            case 5:
                message.bulletType = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("fAngleLeft"))
            throw $util.ProtocolError("missing required 'fAngleLeft'", { instance: message });
        if (!message.hasOwnProperty("fAngleRight"))
            throw $util.ProtocolError("missing required 'fAngleRight'", { instance: message });
        if (!message.hasOwnProperty("strBulletIndexLeft"))
            throw $util.ProtocolError("missing required 'strBulletIndexLeft'", { instance: message });
        if (!message.hasOwnProperty("strBulletIndexRight"))
            throw $util.ProtocolError("missing required 'strBulletIndexRight'", { instance: message });
        return message;
    };

    return CS_PlayerFireMsg;
})();

$root.CS_ShowBulletRank = (function() {

    function CS_ShowBulletRank(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ShowBulletRank.prototype.nidLogin = 0;
    CS_ShowBulletRank.prototype.nRankType = 0;
    CS_ShowBulletRank.prototype.nUpdateTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_ShowBulletRank.prototype.nBulletPer = 0;

    CS_ShowBulletRank.create = function create(properties) {
        return new CS_ShowBulletRank(properties);
    };

    CS_ShowBulletRank.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.nRankType);
        writer.uint32(24).sint64(message.nUpdateTime);
        if (message.nBulletPer != null && Object.hasOwnProperty.call(message, "nBulletPer"))
            writer.uint32(32).sint32(message.nBulletPer);
        return writer;
    };

    CS_ShowBulletRank.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ShowBulletRank();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nRankType = reader.sint32();
                break;
            case 3:
                message.nUpdateTime = reader.sint64();
                break;
            case 4:
                message.nBulletPer = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nRankType"))
            throw $util.ProtocolError("missing required 'nRankType'", { instance: message });
        if (!message.hasOwnProperty("nUpdateTime"))
            throw $util.ProtocolError("missing required 'nUpdateTime'", { instance: message });
        return message;
    };

    return CS_ShowBulletRank;
})();

$root.CS_ShowPlayerRank_Res = (function() {

    function CS_ShowPlayerRank_Res(properties) {
        this.playerRankInfo = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ShowPlayerRank_Res.prototype.nRankType = 0;
    CS_ShowPlayerRank_Res.prototype.currRank = 0;
    CS_ShowPlayerRank_Res.prototype.isAgreeRank = 0;
    CS_ShowPlayerRank_Res.prototype.nUpdateTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_ShowPlayerRank_Res.prototype.playerRankInfo = $util.emptyArray;

    CS_ShowPlayerRank_Res.create = function create(properties) {
        return new CS_ShowPlayerRank_Res(properties);
    };

    CS_ShowPlayerRank_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRankType);
        writer.uint32(16).sint32(message.currRank);
        writer.uint32(24).sint32(message.isAgreeRank);
        writer.uint32(32).sint64(message.nUpdateTime);
        if (message.playerRankInfo != null && message.playerRankInfo.length)
            for (var i = 0; i < message.playerRankInfo.length; ++i)
                $root.PlayerRankInfo.encode(message.playerRankInfo[i], writer.uint32(42).fork()).ldelim();
        return writer;
    };

    CS_ShowPlayerRank_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ShowPlayerRank_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRankType = reader.sint32();
                break;
            case 2:
                message.currRank = reader.sint32();
                break;
            case 3:
                message.isAgreeRank = reader.sint32();
                break;
            case 4:
                message.nUpdateTime = reader.sint64();
                break;
            case 5:
                if (!(message.playerRankInfo && message.playerRankInfo.length))
                    message.playerRankInfo = [];
                message.playerRankInfo.push($root.PlayerRankInfo.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRankType"))
            throw $util.ProtocolError("missing required 'nRankType'", { instance: message });
        if (!message.hasOwnProperty("currRank"))
            throw $util.ProtocolError("missing required 'currRank'", { instance: message });
        if (!message.hasOwnProperty("isAgreeRank"))
            throw $util.ProtocolError("missing required 'isAgreeRank'", { instance: message });
        if (!message.hasOwnProperty("nUpdateTime"))
            throw $util.ProtocolError("missing required 'nUpdateTime'", { instance: message });
        return message;
    };

    return CS_ShowPlayerRank_Res;
})();

$root.PlayerRankInfo = (function() {

    function PlayerRankInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    PlayerRankInfo.prototype.nidLogin = 0;
    PlayerRankInfo.prototype.nHeadType = 0;
    PlayerRankInfo.prototype.strNickname = "";
    PlayerRankInfo.prototype.nPlayerLevel = 0;
    PlayerRankInfo.prototype.nVIPLevel = 0;
    PlayerRankInfo.prototype.nCurrRank = 0;
    PlayerRankInfo.prototype.nCurrNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    PlayerRankInfo.prototype.strOneTimeData = "";
    PlayerRankInfo.prototype.strPlayerSignature = "";

    PlayerRankInfo.create = function create(properties) {
        return new PlayerRankInfo(properties);
    };

    PlayerRankInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.nHeadType);
        writer.uint32(26).string(message.strNickname);
        writer.uint32(32).sint32(message.nPlayerLevel);
        writer.uint32(40).sint32(message.nVIPLevel);
        writer.uint32(48).sint32(message.nCurrRank);
        writer.uint32(56).sint64(message.nCurrNum);
        writer.uint32(66).string(message.strOneTimeData);
        writer.uint32(74).string(message.strPlayerSignature);
        return writer;
    };

    PlayerRankInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PlayerRankInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nHeadType = reader.sint32();
                break;
            case 3:
                message.strNickname = reader.string();
                break;
            case 4:
                message.nPlayerLevel = reader.sint32();
                break;
            case 5:
                message.nVIPLevel = reader.sint32();
                break;
            case 6:
                message.nCurrRank = reader.sint32();
                break;
            case 7:
                message.nCurrNum = reader.sint64();
                break;
            case 8:
                message.strOneTimeData = reader.string();
                break;
            case 9:
                message.strPlayerSignature = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nHeadType"))
            throw $util.ProtocolError("missing required 'nHeadType'", { instance: message });
        if (!message.hasOwnProperty("strNickname"))
            throw $util.ProtocolError("missing required 'strNickname'", { instance: message });
        if (!message.hasOwnProperty("nPlayerLevel"))
            throw $util.ProtocolError("missing required 'nPlayerLevel'", { instance: message });
        if (!message.hasOwnProperty("nVIPLevel"))
            throw $util.ProtocolError("missing required 'nVIPLevel'", { instance: message });
        if (!message.hasOwnProperty("nCurrRank"))
            throw $util.ProtocolError("missing required 'nCurrRank'", { instance: message });
        if (!message.hasOwnProperty("nCurrNum"))
            throw $util.ProtocolError("missing required 'nCurrNum'", { instance: message });
        if (!message.hasOwnProperty("strOneTimeData"))
            throw $util.ProtocolError("missing required 'strOneTimeData'", { instance: message });
        if (!message.hasOwnProperty("strPlayerSignature"))
            throw $util.ProtocolError("missing required 'strPlayerSignature'", { instance: message });
        return message;
    };

    return PlayerRankInfo;
})();

$root.CS_ShowPlayerRank = (function() {

    function CS_ShowPlayerRank(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ShowPlayerRank.prototype.nidLogin = 0;
    CS_ShowPlayerRank.prototype.nRankType = 0;
    CS_ShowPlayerRank.prototype.nUpdateTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_ShowPlayerRank.prototype.nBulletPer = 0;

    CS_ShowPlayerRank.create = function create(properties) {
        return new CS_ShowPlayerRank(properties);
    };

    CS_ShowPlayerRank.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.nRankType);
        writer.uint32(24).sint64(message.nUpdateTime);
        if (message.nBulletPer != null && Object.hasOwnProperty.call(message, "nBulletPer"))
            writer.uint32(32).sint32(message.nBulletPer);
        return writer;
    };

    CS_ShowPlayerRank.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ShowPlayerRank();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nRankType = reader.sint32();
                break;
            case 3:
                message.nUpdateTime = reader.sint64();
                break;
            case 4:
                message.nBulletPer = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nRankType"))
            throw $util.ProtocolError("missing required 'nRankType'", { instance: message });
        if (!message.hasOwnProperty("nUpdateTime"))
            throw $util.ProtocolError("missing required 'nUpdateTime'", { instance: message });
        return message;
    };

    return CS_ShowPlayerRank;
})();

$root.CS_TaskReward_Res = (function() {

    function CS_TaskReward_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_TaskReward_Res.prototype.nTaskID = 0;
    CS_TaskReward_Res.prototype.itemReward = $util.emptyArray;

    CS_TaskReward_Res.create = function create(properties) {
        return new CS_TaskReward_Res(properties);
    };

    CS_TaskReward_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nTaskID);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(26).fork()).ldelim();
        return writer;
    };

    CS_TaskReward_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_TaskReward_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nTaskID = reader.sint32();
                break;
            case 3:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nTaskID"))
            throw $util.ProtocolError("missing required 'nTaskID'", { instance: message });
        return message;
    };

    return CS_TaskReward_Res;
})();

$root.CS_TaskReward = (function() {

    function CS_TaskReward(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_TaskReward.prototype.nTaskID = 0;

    CS_TaskReward.create = function create(properties) {
        return new CS_TaskReward(properties);
    };

    CS_TaskReward.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nTaskID);
        return writer;
    };

    CS_TaskReward.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_TaskReward();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nTaskID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nTaskID"))
            throw $util.ProtocolError("missing required 'nTaskID'", { instance: message });
        return message;
    };

    return CS_TaskReward;
})();

$root.CS_UnlockFort_Res = (function() {

    function CS_UnlockFort_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_UnlockFort_Res.prototype.nMaxFort = 0;
    CS_UnlockFort_Res.prototype.nNowFort = 0;
    CS_UnlockFort_Res.prototype.nLastDiamond = 0;
    CS_UnlockFort_Res.prototype.nLastGold = 0;

    CS_UnlockFort_Res.create = function create(properties) {
        return new CS_UnlockFort_Res(properties);
    };

    CS_UnlockFort_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nMaxFort);
        writer.uint32(16).sint32(message.nNowFort);
        writer.uint32(24).sint32(message.nLastDiamond);
        writer.uint32(32).sint32(message.nLastGold);
        return writer;
    };

    CS_UnlockFort_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_UnlockFort_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nMaxFort = reader.sint32();
                break;
            case 2:
                message.nNowFort = reader.sint32();
                break;
            case 3:
                message.nLastDiamond = reader.sint32();
                break;
            case 4:
                message.nLastGold = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nMaxFort"))
            throw $util.ProtocolError("missing required 'nMaxFort'", { instance: message });
        if (!message.hasOwnProperty("nNowFort"))
            throw $util.ProtocolError("missing required 'nNowFort'", { instance: message });
        if (!message.hasOwnProperty("nLastDiamond"))
            throw $util.ProtocolError("missing required 'nLastDiamond'", { instance: message });
        if (!message.hasOwnProperty("nLastGold"))
            throw $util.ProtocolError("missing required 'nLastGold'", { instance: message });
        return message;
    };

    return CS_UnlockFort_Res;
})();

$root.CS_UnlockFort = (function() {

    function CS_UnlockFort(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_UnlockFort.prototype.nNextFort = 0;

    CS_UnlockFort.create = function create(properties) {
        return new CS_UnlockFort(properties);
    };

    CS_UnlockFort.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nNextFort);
        return writer;
    };

    CS_UnlockFort.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_UnlockFort();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nNextFort = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nNextFort"))
            throw $util.ProtocolError("missing required 'nNextFort'", { instance: message });
        return message;
    };

    return CS_UnlockFort;
})();

$root.CS_UnlockFortSkin_Res = (function() {

    function CS_UnlockFortSkin_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_UnlockFortSkin_Res.prototype.nSeatID = 0;
    CS_UnlockFortSkin_Res.prototype.nFortSkinID = 0;
    CS_UnlockFortSkin_Res.prototype.itemReward = $util.emptyArray;

    CS_UnlockFortSkin_Res.create = function create(properties) {
        return new CS_UnlockFortSkin_Res(properties);
    };

    CS_UnlockFortSkin_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        writer.uint32(16).sint32(message.nFortSkinID);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(26).fork()).ldelim();
        return writer;
    };

    CS_UnlockFortSkin_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_UnlockFortSkin_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            case 2:
                message.nFortSkinID = reader.sint32();
                break;
            case 3:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("nFortSkinID"))
            throw $util.ProtocolError("missing required 'nFortSkinID'", { instance: message });
        return message;
    };

    return CS_UnlockFortSkin_Res;
})();

$root.CS_UseDuiHuanCode_Res = (function() {

    function CS_UseDuiHuanCode_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_UseDuiHuanCode_Res.prototype.itemReward = $util.emptyArray;

    CS_UseDuiHuanCode_Res.create = function create(properties) {
        return new CS_UseDuiHuanCode_Res(properties);
    };

    CS_UseDuiHuanCode_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    CS_UseDuiHuanCode_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_UseDuiHuanCode_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CS_UseDuiHuanCode_Res;
})();

$root.CS_UseDuiHuanCode = (function() {

    function CS_UseDuiHuanCode(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_UseDuiHuanCode.prototype.strCode = "";
    CS_UseDuiHuanCode.prototype.nidLogin = 0;

    CS_UseDuiHuanCode.create = function create(properties) {
        return new CS_UseDuiHuanCode(properties);
    };

    CS_UseDuiHuanCode.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strCode);
        if (message.nidLogin != null && Object.hasOwnProperty.call(message, "nidLogin"))
            writer.uint32(16).sint32(message.nidLogin);
        return writer;
    };

    CS_UseDuiHuanCode.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_UseDuiHuanCode();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strCode = reader.string();
                break;
            case 2:
                message.nidLogin = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strCode"))
            throw $util.ProtocolError("missing required 'strCode'", { instance: message });
        return message;
    };

    return CS_UseDuiHuanCode;
})();

$root.HC_GetFirstRewardMsg_Res = (function() {

    function HC_GetFirstRewardMsg_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HC_GetFirstRewardMsg_Res.prototype.state = 0;
    HC_GetFirstRewardMsg_Res.prototype.itemReward = $util.emptyArray;

    HC_GetFirstRewardMsg_Res.create = function create(properties) {
        return new HC_GetFirstRewardMsg_Res(properties);
    };

    HC_GetFirstRewardMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).int32(message.state);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(18).fork()).ldelim();
        return writer;
    };

    HC_GetFirstRewardMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HC_GetFirstRewardMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.state = reader.int32();
                break;
            case 2:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("state"))
            throw $util.ProtocolError("missing required 'state'", { instance: message });
        return message;
    };

    return HC_GetFirstRewardMsg_Res;
})();

$root.HC_GrandPrixRewardMsg = (function() {

    function HC_GrandPrixRewardMsg(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HC_GrandPrixRewardMsg.prototype.itemReward = $util.emptyArray;

    HC_GrandPrixRewardMsg.create = function create(properties) {
        return new HC_GrandPrixRewardMsg(properties);
    };

    HC_GrandPrixRewardMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    HC_GrandPrixRewardMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HC_GrandPrixRewardMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return HC_GrandPrixRewardMsg;
})();

$root.HC_ModifyActScoreMsg = (function() {

    function HC_ModifyActScoreMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HC_ModifyActScoreMsg.prototype.nidLogin = 0;
    HC_ModifyActScoreMsg.prototype.nActID = 0;
    HC_ModifyActScoreMsg.prototype.nScore = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    HC_ModifyActScoreMsg.prototype.nRes = 0;

    HC_ModifyActScoreMsg.create = function create(properties) {
        return new HC_ModifyActScoreMsg(properties);
    };

    HC_ModifyActScoreMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.nActID);
        writer.uint32(24).sint64(message.nScore);
        if (message.nRes != null && Object.hasOwnProperty.call(message, "nRes"))
            writer.uint32(32).sint32(message.nRes);
        return writer;
    };

    HC_ModifyActScoreMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HC_ModifyActScoreMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nActID = reader.sint32();
                break;
            case 3:
                message.nScore = reader.sint64();
                break;
            case 4:
                message.nRes = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nActID"))
            throw $util.ProtocolError("missing required 'nActID'", { instance: message });
        if (!message.hasOwnProperty("nScore"))
            throw $util.ProtocolError("missing required 'nScore'", { instance: message });
        return message;
    };

    return HC_ModifyActScoreMsg;
})();

$root.HC_NewExchanInvenData = (function() {

    function HC_NewExchanInvenData(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HC_NewExchanInvenData.prototype.nID = 0;
    HC_NewExchanInvenData.prototype.nUpNum = 0;

    HC_NewExchanInvenData.create = function create(properties) {
        return new HC_NewExchanInvenData(properties);
    };

    HC_NewExchanInvenData.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nID);
        writer.uint32(16).sint32(message.nUpNum);
        return writer;
    };

    HC_NewExchanInvenData.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HC_NewExchanInvenData();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nID = reader.sint32();
                break;
            case 2:
                message.nUpNum = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nID"))
            throw $util.ProtocolError("missing required 'nID'", { instance: message });
        if (!message.hasOwnProperty("nUpNum"))
            throw $util.ProtocolError("missing required 'nUpNum'", { instance: message });
        return message;
    };

    return HC_NewExchanInvenData;
})();

$root.HC_NewMainTask = (function() {

    function HC_NewMainTask(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HC_NewMainTask.prototype.strTaskData = "";

    HC_NewMainTask.create = function create(properties) {
        return new HC_NewMainTask(properties);
    };

    HC_NewMainTask.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strTaskData);
        return writer;
    };

    HC_NewMainTask.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HC_NewMainTask();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strTaskData = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strTaskData"))
            throw $util.ProtocolError("missing required 'strTaskData'", { instance: message });
        return message;
    };

    return HC_NewMainTask;
})();

$root.HC_RefreshLuckDraw = (function() {

    function HC_RefreshLuckDraw(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HC_RefreshLuckDraw.prototype.strLuckDraw = "";

    HC_RefreshLuckDraw.create = function create(properties) {
        return new HC_RefreshLuckDraw(properties);
    };

    HC_RefreshLuckDraw.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strLuckDraw);
        return writer;
    };

    HC_RefreshLuckDraw.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HC_RefreshLuckDraw();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strLuckDraw = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strLuckDraw"))
            throw $util.ProtocolError("missing required 'strLuckDraw'", { instance: message });
        return message;
    };

    return HC_RefreshLuckDraw;
})();

$root.HC_UpdateActRule = (function() {

    function HC_UpdateActRule(properties) {
        this.actRuleInfo = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HC_UpdateActRule.prototype.actRuleInfo = $util.emptyArray;

    HC_UpdateActRule.create = function create(properties) {
        return new HC_UpdateActRule(properties);
    };

    HC_UpdateActRule.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.actRuleInfo != null && message.actRuleInfo.length)
            for (var i = 0; i < message.actRuleInfo.length; ++i)
                $root.ActRuleInfo.encode(message.actRuleInfo[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    HC_UpdateActRule.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HC_UpdateActRule();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.actRuleInfo && message.actRuleInfo.length))
                    message.actRuleInfo = [];
                message.actRuleInfo.push($root.ActRuleInfo.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return HC_UpdateActRule;
})();

$root.HC_UpdateExchangeMsg = (function() {

    function HC_UpdateExchangeMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HC_UpdateExchangeMsg.create = function create(properties) {
        return new HC_UpdateExchangeMsg(properties);
    };

    HC_UpdateExchangeMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        return writer;
    };

    HC_UpdateExchangeMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HC_UpdateExchangeMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return HC_UpdateExchangeMsg;
})();

$root.HegemonyRankInfo = (function() {

    function HegemonyRankInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HegemonyRankInfo.prototype.nidLogin = 0;
    HegemonyRankInfo.prototype.strNickName = "";
    HegemonyRankInfo.prototype.nCurrNum = 0;
    HegemonyRankInfo.prototype.nBonus = 0;

    HegemonyRankInfo.create = function create(properties) {
        return new HegemonyRankInfo(properties);
    };

    HegemonyRankInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strNickName);
        writer.uint32(24).sint32(message.nCurrNum);
        if (message.nBonus != null && Object.hasOwnProperty.call(message, "nBonus"))
            writer.uint32(32).sint32(message.nBonus);
        return writer;
    };

    HegemonyRankInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HegemonyRankInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strNickName = reader.string();
                break;
            case 3:
                message.nCurrNum = reader.sint32();
                break;
            case 4:
                message.nBonus = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strNickName"))
            throw $util.ProtocolError("missing required 'strNickName'", { instance: message });
        if (!message.hasOwnProperty("nCurrNum"))
            throw $util.ProtocolError("missing required 'nCurrNum'", { instance: message });
        return message;
    };

    return HegemonyRankInfo;
})();

$root.JP_Total_Res = (function() {

    function JP_Total_Res(properties) {
        this.JP = [];
        this.roomType = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    JP_Total_Res.prototype.JP = $util.emptyArray;
    JP_Total_Res.prototype.roomType = $util.emptyArray;

    JP_Total_Res.create = function create(properties) {
        return new JP_Total_Res(properties);
    };

    JP_Total_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.JP != null && message.JP.length)
            for (var i = 0; i < message.JP.length; ++i)
                writer.uint32(9).double(message.JP[i]);
        if (message.roomType != null && message.roomType.length)
            for (var i = 0; i < message.roomType.length; ++i)
                writer.uint32(16).sint32(message.roomType[i]);
        return writer;
    };

    JP_Total_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.JP_Total_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.JP && message.JP.length))
                    message.JP = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.JP.push(reader.double());
                } else
                    message.JP.push(reader.double());
                break;
            case 2:
                if (!(message.roomType && message.roomType.length))
                    message.roomType = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.roomType.push(reader.sint32());
                } else
                    message.roomType.push(reader.sint32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return JP_Total_Res;
})();

$root.JP_Total = (function() {

    function JP_Total(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    JP_Total.create = function create(properties) {
        return new JP_Total(properties);
    };

    JP_Total.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        return writer;
    };

    JP_Total.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.JP_Total();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return JP_Total;
})();

$root.SC_ActivityStateMsg = (function() {

    function SC_ActivityStateMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_ActivityStateMsg.prototype.nActID = 0;
    SC_ActivityStateMsg.prototype.nActType = 0;
    SC_ActivityStateMsg.prototype.nActState = 0;
    SC_ActivityStateMsg.prototype.nRemaTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    SC_ActivityStateMsg.prototype.nActStartTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    SC_ActivityStateMsg.prototype.nAddParam = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    SC_ActivityStateMsg.create = function create(properties) {
        return new SC_ActivityStateMsg(properties);
    };

    SC_ActivityStateMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nActID);
        writer.uint32(16).sint32(message.nActType);
        writer.uint32(24).sint32(message.nActState);
        writer.uint32(32).sint64(message.nRemaTime);
        if (message.nActStartTime != null && Object.hasOwnProperty.call(message, "nActStartTime"))
            writer.uint32(40).sint64(message.nActStartTime);
        if (message.nAddParam != null && Object.hasOwnProperty.call(message, "nAddParam"))
            writer.uint32(48).sint64(message.nAddParam);
        return writer;
    };

    SC_ActivityStateMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_ActivityStateMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nActID = reader.sint32();
                break;
            case 2:
                message.nActType = reader.sint32();
                break;
            case 3:
                message.nActState = reader.sint32();
                break;
            case 4:
                message.nRemaTime = reader.sint64();
                break;
            case 5:
                message.nActStartTime = reader.sint64();
                break;
            case 6:
                message.nAddParam = reader.sint64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nActID"))
            throw $util.ProtocolError("missing required 'nActID'", { instance: message });
        if (!message.hasOwnProperty("nActType"))
            throw $util.ProtocolError("missing required 'nActType'", { instance: message });
        if (!message.hasOwnProperty("nActState"))
            throw $util.ProtocolError("missing required 'nActState'", { instance: message });
        if (!message.hasOwnProperty("nRemaTime"))
            throw $util.ProtocolError("missing required 'nRemaTime'", { instance: message });
        return message;
    };

    return SC_ActivityStateMsg;
})();

$root.SC_BillingWelfare = (function() {

    function SC_BillingWelfare(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_BillingWelfare.prototype.nidLogin = 0;
    SC_BillingWelfare.prototype.nBillingWelfare = 0;

    SC_BillingWelfare.create = function create(properties) {
        return new SC_BillingWelfare(properties);
    };

    SC_BillingWelfare.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).int32(message.nBillingWelfare);
        return writer;
    };

    SC_BillingWelfare.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_BillingWelfare();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nBillingWelfare = reader.int32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nBillingWelfare"))
            throw $util.ProtocolError("missing required 'nBillingWelfare'", { instance: message });
        return message;
    };

    return SC_BillingWelfare;
})();

$root.SC_FishTide = (function() {

    function SC_FishTide(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_FishTide.prototype.strTime = "";
    SC_FishTide.prototype.nFishTideID = 0;

    SC_FishTide.create = function create(properties) {
        return new SC_FishTide(properties);
    };

    SC_FishTide.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strTime);
        writer.uint32(16).sint32(message.nFishTideID);
        return writer;
    };

    SC_FishTide.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_FishTide();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strTime = reader.string();
                break;
            case 2:
                message.nFishTideID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strTime"))
            throw $util.ProtocolError("missing required 'strTime'", { instance: message });
        if (!message.hasOwnProperty("nFishTideID"))
            throw $util.ProtocolError("missing required 'nFishTideID'", { instance: message });
        return message;
    };

    return SC_FishTide;
})();

$root.SC_JoinCommHegemonyInfoMsg = (function() {

    function SC_JoinCommHegemonyInfoMsg(properties) {
        this.hegemonyRankInfo = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_JoinCommHegemonyInfoMsg.prototype.nHegemonyEndTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    SC_JoinCommHegemonyInfoMsg.prototype.nCurrFeats = 0;
    SC_JoinCommHegemonyInfoMsg.prototype.nRank = 0;
    SC_JoinCommHegemonyInfoMsg.prototype.hegemonyRankInfo = $util.emptyArray;

    SC_JoinCommHegemonyInfoMsg.create = function create(properties) {
        return new SC_JoinCommHegemonyInfoMsg(properties);
    };

    SC_JoinCommHegemonyInfoMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint64(message.nHegemonyEndTime);
        writer.uint32(16).sint32(message.nCurrFeats);
        writer.uint32(24).sint32(message.nRank);
        if (message.hegemonyRankInfo != null && message.hegemonyRankInfo.length)
            for (var i = 0; i < message.hegemonyRankInfo.length; ++i)
                $root.HegemonyRankInfo.encode(message.hegemonyRankInfo[i], writer.uint32(34).fork()).ldelim();
        return writer;
    };

    SC_JoinCommHegemonyInfoMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_JoinCommHegemonyInfoMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nHegemonyEndTime = reader.sint64();
                break;
            case 2:
                message.nCurrFeats = reader.sint32();
                break;
            case 3:
                message.nRank = reader.sint32();
                break;
            case 4:
                if (!(message.hegemonyRankInfo && message.hegemonyRankInfo.length))
                    message.hegemonyRankInfo = [];
                message.hegemonyRankInfo.push($root.HegemonyRankInfo.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nHegemonyEndTime"))
            throw $util.ProtocolError("missing required 'nHegemonyEndTime'", { instance: message });
        if (!message.hasOwnProperty("nCurrFeats"))
            throw $util.ProtocolError("missing required 'nCurrFeats'", { instance: message });
        if (!message.hasOwnProperty("nRank"))
            throw $util.ProtocolError("missing required 'nRank'", { instance: message });
        return message;
    };

    return SC_JoinCommHegemonyInfoMsg;
})();

$root.SC_LogMsg = (function() {

    function SC_LogMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_LogMsg.prototype.logInfo = "";

    SC_LogMsg.create = function create(properties) {
        return new SC_LogMsg(properties);
    };

    SC_LogMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.logInfo);
        return writer;
    };

    SC_LogMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_LogMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.logInfo = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("logInfo"))
            throw $util.ProtocolError("missing required 'logInfo'", { instance: message });
        return message;
    };

    return SC_LogMsg;
})();

$root.SC_ModifyRoomInfo = (function() {

    function SC_ModifyRoomInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_ModifyRoomInfo.prototype.strTime = "";
    SC_ModifyRoomInfo.prototype.fValue = 0;

    SC_ModifyRoomInfo.create = function create(properties) {
        return new SC_ModifyRoomInfo(properties);
    };

    SC_ModifyRoomInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strTime);
        writer.uint32(21).float(message.fValue);
        return writer;
    };

    SC_ModifyRoomInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_ModifyRoomInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strTime = reader.string();
                break;
            case 2:
                message.fValue = reader.float();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strTime"))
            throw $util.ProtocolError("missing required 'strTime'", { instance: message });
        if (!message.hasOwnProperty("fValue"))
            throw $util.ProtocolError("missing required 'fValue'", { instance: message });
        return message;
    };

    return SC_ModifyRoomInfo;
})();

$root.SC_NewFishJoinRoom = (function() {

    function SC_NewFishJoinRoom(properties) {
        this.newFishGroupRoom = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_NewFishJoinRoom.prototype.strTime = "";
    SC_NewFishJoinRoom.prototype.newFishGroupRoom = $util.emptyArray;
    SC_NewFishJoinRoom.prototype.strRoomID = "";

    SC_NewFishJoinRoom.create = function create(properties) {
        return new SC_NewFishJoinRoom(properties);
    };

    SC_NewFishJoinRoom.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strTime);
        if (message.newFishGroupRoom != null && message.newFishGroupRoom.length)
            for (var i = 0; i < message.newFishGroupRoom.length; ++i)
                $root.FishGroup_Room.encode(message.newFishGroupRoom[i], writer.uint32(18).fork()).ldelim();
        if (message.strRoomID != null && Object.hasOwnProperty.call(message, "strRoomID"))
            writer.uint32(26).string(message.strRoomID);
        return writer;
    };

    SC_NewFishJoinRoom.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_NewFishJoinRoom();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strTime = reader.string();
                break;
            case 2:
                if (!(message.newFishGroupRoom && message.newFishGroupRoom.length))
                    message.newFishGroupRoom = [];
                message.newFishGroupRoom.push($root.FishGroup_Room.decode(reader, reader.uint32()));
                break;
            case 3:
                message.strRoomID = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strTime"))
            throw $util.ProtocolError("missing required 'strTime'", { instance: message });
        return message;
    };

    return SC_NewFishJoinRoom;
})();

$root.SC_NewPlayerJoinRoomMsg = (function() {

    function SC_NewPlayerJoinRoomMsg(properties) {
        this.playerInfoRoom = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_NewPlayerJoinRoomMsg.prototype.nSeatID = 0;
    SC_NewPlayerJoinRoomMsg.prototype.playerInfoRoom = $util.emptyArray;
    SC_NewPlayerJoinRoomMsg.prototype.strTime = "";
    SC_NewPlayerJoinRoomMsg.prototype.nidLogin = 0;
    SC_NewPlayerJoinRoomMsg.prototype.strRoomID = "";
    SC_NewPlayerJoinRoomMsg.prototype.strFishID = "";

    SC_NewPlayerJoinRoomMsg.create = function create(properties) {
        return new SC_NewPlayerJoinRoomMsg(properties);
    };

    SC_NewPlayerJoinRoomMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        if (message.playerInfoRoom != null && message.playerInfoRoom.length)
            for (var i = 0; i < message.playerInfoRoom.length; ++i)
                $root.PlayerInfo_Room.encode(message.playerInfoRoom[i], writer.uint32(18).fork()).ldelim();
        writer.uint32(26).string(message.strTime);
        if (message.nidLogin != null && Object.hasOwnProperty.call(message, "nidLogin"))
            writer.uint32(32).sint32(message.nidLogin);
        if (message.strRoomID != null && Object.hasOwnProperty.call(message, "strRoomID"))
            writer.uint32(42).string(message.strRoomID);
        if (message.strFishID != null && Object.hasOwnProperty.call(message, "strFishID"))
            writer.uint32(50).string(message.strFishID);
        return writer;
    };

    SC_NewPlayerJoinRoomMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_NewPlayerJoinRoomMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            case 2:
                if (!(message.playerInfoRoom && message.playerInfoRoom.length))
                    message.playerInfoRoom = [];
                message.playerInfoRoom.push($root.PlayerInfo_Room.decode(reader, reader.uint32()));
                break;
            case 3:
                message.strTime = reader.string();
                break;
            case 4:
                message.nidLogin = reader.sint32();
                break;
            case 5:
                message.strRoomID = reader.string();
                break;
            case 6:
                message.strFishID = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("strTime"))
            throw $util.ProtocolError("missing required 'strTime'", { instance: message });
        return message;
    };

    return SC_NewPlayerJoinRoomMsg;
})();

$root.SC_PlayerFireInRoomMsg = (function() {

    function SC_PlayerFireInRoomMsg(properties) {
        this.fAngle = [];
        this.vecBulletIndex = [];
        this.bulletNum = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_PlayerFireInRoomMsg.prototype.nSeatID = 0;
    SC_PlayerFireInRoomMsg.prototype.fAngle = $util.emptyArray;
    SC_PlayerFireInRoomMsg.prototype.nBulletLevel = 0;
    SC_PlayerFireInRoomMsg.prototype.strBulletTime = "";
    SC_PlayerFireInRoomMsg.prototype.vecBulletIndex = $util.emptyArray;
    SC_PlayerFireInRoomMsg.prototype.nGoldNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    SC_PlayerFireInRoomMsg.prototype.nidLogin = 0;
    SC_PlayerFireInRoomMsg.prototype.strRoomID = "";
    SC_PlayerFireInRoomMsg.prototype.fAccPoints = 0;
    SC_PlayerFireInRoomMsg.prototype.nExp = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    SC_PlayerFireInRoomMsg.prototype.bulletType = -1;
    SC_PlayerFireInRoomMsg.prototype.bulletNum = $util.emptyArray;

    SC_PlayerFireInRoomMsg.create = function create(properties) {
        return new SC_PlayerFireInRoomMsg(properties);
    };

    SC_PlayerFireInRoomMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        if (message.fAngle != null && message.fAngle.length)
            for (var i = 0; i < message.fAngle.length; ++i)
                writer.uint32(21).float(message.fAngle[i]);
        if (message.nBulletLevel != null && Object.hasOwnProperty.call(message, "nBulletLevel"))
            writer.uint32(32).sint32(message.nBulletLevel);
        if (message.strBulletTime != null && Object.hasOwnProperty.call(message, "strBulletTime"))
            writer.uint32(42).string(message.strBulletTime);
        if (message.vecBulletIndex != null && message.vecBulletIndex.length)
            for (var i = 0; i < message.vecBulletIndex.length; ++i)
                writer.uint32(50).string(message.vecBulletIndex[i]);
        writer.uint32(56).sint64(message.nGoldNum);
        if (message.nidLogin != null && Object.hasOwnProperty.call(message, "nidLogin"))
            writer.uint32(64).sint32(message.nidLogin);
        if (message.strRoomID != null && Object.hasOwnProperty.call(message, "strRoomID"))
            writer.uint32(74).string(message.strRoomID);
        writer.uint32(85).float(message.fAccPoints);
        writer.uint32(88).sint64(message.nExp);
        if (message.bulletType != null && Object.hasOwnProperty.call(message, "bulletType"))
            writer.uint32(96).sint32(message.bulletType);
        if (message.bulletNum != null && message.bulletNum.length)
            for (var i = 0; i < message.bulletNum.length; ++i)
                writer.uint32(104).sint32(message.bulletNum[i]);
        return writer;
    };

    SC_PlayerFireInRoomMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_PlayerFireInRoomMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            case 2:
                if (!(message.fAngle && message.fAngle.length))
                    message.fAngle = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.fAngle.push(reader.float());
                } else
                    message.fAngle.push(reader.float());
                break;
            case 4:
                message.nBulletLevel = reader.sint32();
                break;
            case 5:
                message.strBulletTime = reader.string();
                break;
            case 6:
                if (!(message.vecBulletIndex && message.vecBulletIndex.length))
                    message.vecBulletIndex = [];
                message.vecBulletIndex.push(reader.string());
                break;
            case 7:
                message.nGoldNum = reader.sint64();
                break;
            case 8:
                message.nidLogin = reader.sint32();
                break;
            case 9:
                message.strRoomID = reader.string();
                break;
            case 10:
                message.fAccPoints = reader.float();
                break;
            case 11:
                message.nExp = reader.sint64();
                break;
            case 12:
                message.bulletType = reader.sint32();
                break;
            case 13:
                if (!(message.bulletNum && message.bulletNum.length))
                    message.bulletNum = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.bulletNum.push(reader.sint32());
                } else
                    message.bulletNum.push(reader.sint32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("nGoldNum"))
            throw $util.ProtocolError("missing required 'nGoldNum'", { instance: message });
        if (!message.hasOwnProperty("fAccPoints"))
            throw $util.ProtocolError("missing required 'fAccPoints'", { instance: message });
        if (!message.hasOwnProperty("nExp"))
            throw $util.ProtocolError("missing required 'nExp'", { instance: message });
        return message;
    };

    return SC_PlayerFireInRoomMsg;
})();

$root.SC_PlayerHegemonyRank = (function() {

    function SC_PlayerHegemonyRank(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_PlayerHegemonyRank.prototype.nRank = 0;

    SC_PlayerHegemonyRank.create = function create(properties) {
        return new SC_PlayerHegemonyRank(properties);
    };

    SC_PlayerHegemonyRank.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRank);
        return writer;
    };

    SC_PlayerHegemonyRank.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_PlayerHegemonyRank();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRank = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRank"))
            throw $util.ProtocolError("missing required 'nRank'", { instance: message });
        return message;
    };

    return SC_PlayerHegemonyRank;
})();

$root.SC_PlayerLevelUp = (function() {

    function SC_PlayerLevelUp(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_PlayerLevelUp.prototype.nSeatID = 0;
    SC_PlayerLevelUp.prototype.nidLogin = 0;
    SC_PlayerLevelUp.prototype.nLevel = 0;
    SC_PlayerLevelUp.prototype.nExp = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    SC_PlayerLevelUp.prototype.itemReward = $util.emptyArray;

    SC_PlayerLevelUp.create = function create(properties) {
        return new SC_PlayerLevelUp(properties);
    };

    SC_PlayerLevelUp.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        writer.uint32(16).sint32(message.nidLogin);
        writer.uint32(24).sint32(message.nLevel);
        writer.uint32(32).sint64(message.nExp);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(42).fork()).ldelim();
        return writer;
    };

    SC_PlayerLevelUp.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_PlayerLevelUp();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            case 2:
                message.nidLogin = reader.sint32();
                break;
            case 3:
                message.nLevel = reader.sint32();
                break;
            case 4:
                message.nExp = reader.sint64();
                break;
            case 5:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nLevel"))
            throw $util.ProtocolError("missing required 'nLevel'", { instance: message });
        if (!message.hasOwnProperty("nExp"))
            throw $util.ProtocolError("missing required 'nExp'", { instance: message });
        return message;
    };

    return SC_PlayerLevelUp;
})();

$root.SC_RemovePlayerFromRoomMsg = (function() {

    function SC_RemovePlayerFromRoomMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_RemovePlayerFromRoomMsg.prototype.nSeatID = 0;

    SC_RemovePlayerFromRoomMsg.create = function create(properties) {
        return new SC_RemovePlayerFromRoomMsg(properties);
    };

    SC_RemovePlayerFromRoomMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatID);
        return writer;
    };

    SC_RemovePlayerFromRoomMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_RemovePlayerFromRoomMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatID"))
            throw $util.ProtocolError("missing required 'nSeatID'", { instance: message });
        return message;
    };

    return SC_RemovePlayerFromRoomMsg;
})();

$root.SC_SkillEndMsg = (function() {

    function SC_SkillEndMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_SkillEndMsg.prototype.nSeatId = 0;
    SC_SkillEndMsg.prototype.nSkillId = 0;
    SC_SkillEndMsg.prototype.strNowTime = "";

    SC_SkillEndMsg.create = function create(properties) {
        return new SC_SkillEndMsg(properties);
    };

    SC_SkillEndMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatId);
        writer.uint32(16).sint32(message.nSkillId);
        writer.uint32(26).string(message.strNowTime);
        return writer;
    };

    SC_SkillEndMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_SkillEndMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatId = reader.sint32();
                break;
            case 2:
                message.nSkillId = reader.sint32();
                break;
            case 3:
                message.strNowTime = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatId"))
            throw $util.ProtocolError("missing required 'nSeatId'", { instance: message });
        if (!message.hasOwnProperty("nSkillId"))
            throw $util.ProtocolError("missing required 'nSkillId'", { instance: message });
        if (!message.hasOwnProperty("strNowTime"))
            throw $util.ProtocolError("missing required 'strNowTime'", { instance: message });
        return message;
    };

    return SC_SkillEndMsg;
})();

$root.SC_SkillUseMsg = (function() {

    function SC_SkillUseMsg(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_SkillUseMsg.prototype.nSeatId = 0;
    SC_SkillUseMsg.prototype.nSkillId = 0;
    SC_SkillUseMsg.prototype.itemReward = $util.emptyArray;
    SC_SkillUseMsg.prototype.strEndTime = "";
    SC_SkillUseMsg.prototype.strNowTime = "";

    SC_SkillUseMsg.create = function create(properties) {
        return new SC_SkillUseMsg(properties);
    };

    SC_SkillUseMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nSeatId);
        writer.uint32(16).sint32(message.nSkillId);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(26).fork()).ldelim();
        writer.uint32(34).string(message.strEndTime);
        writer.uint32(42).string(message.strNowTime);
        return writer;
    };

    SC_SkillUseMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_SkillUseMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nSeatId = reader.sint32();
                break;
            case 2:
                message.nSkillId = reader.sint32();
                break;
            case 3:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            case 4:
                message.strEndTime = reader.string();
                break;
            case 5:
                message.strNowTime = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nSeatId"))
            throw $util.ProtocolError("missing required 'nSeatId'", { instance: message });
        if (!message.hasOwnProperty("nSkillId"))
            throw $util.ProtocolError("missing required 'nSkillId'", { instance: message });
        if (!message.hasOwnProperty("strEndTime"))
            throw $util.ProtocolError("missing required 'strEndTime'", { instance: message });
        if (!message.hasOwnProperty("strNowTime"))
            throw $util.ProtocolError("missing required 'strNowTime'", { instance: message });
        return message;
    };

    return SC_SkillUseMsg;
})();

$root.SC_TaskFinish = (function() {

    function SC_TaskFinish(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_TaskFinish.prototype.nTaskID = 0;

    SC_TaskFinish.create = function create(properties) {
        return new SC_TaskFinish(properties);
    };

    SC_TaskFinish.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nTaskID);
        return writer;
    };

    SC_TaskFinish.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_TaskFinish();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nTaskID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nTaskID"))
            throw $util.ProtocolError("missing required 'nTaskID'", { instance: message });
        return message;
    };

    return SC_TaskFinish;
})();

$root.SC_UpdateBonusPoolsDataMsg = (function() {

    function SC_UpdateBonusPoolsDataMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_UpdateBonusPoolsDataMsg.prototype.nNewBonusPools = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    SC_UpdateBonusPoolsDataMsg.create = function create(properties) {
        return new SC_UpdateBonusPoolsDataMsg(properties);
    };

    SC_UpdateBonusPoolsDataMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint64(message.nNewBonusPools);
        return writer;
    };

    SC_UpdateBonusPoolsDataMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_UpdateBonusPoolsDataMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nNewBonusPools = reader.sint64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nNewBonusPools"))
            throw $util.ProtocolError("missing required 'nNewBonusPools'", { instance: message });
        return message;
    };

    return SC_UpdateBonusPoolsDataMsg;
})();

$root.SC_UpdateHegemonyRankMsg = (function() {

    function SC_UpdateHegemonyRankMsg(properties) {
        this.hegemonyRankInfo = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_UpdateHegemonyRankMsg.prototype.hegemonyRankInfo = $util.emptyArray;
    SC_UpdateHegemonyRankMsg.prototype.bSett = false;

    SC_UpdateHegemonyRankMsg.create = function create(properties) {
        return new SC_UpdateHegemonyRankMsg(properties);
    };

    SC_UpdateHegemonyRankMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.hegemonyRankInfo != null && message.hegemonyRankInfo.length)
            for (var i = 0; i < message.hegemonyRankInfo.length; ++i)
                $root.HegemonyRankInfo.encode(message.hegemonyRankInfo[i], writer.uint32(10).fork()).ldelim();
        writer.uint32(16).bool(message.bSett);
        return writer;
    };

    SC_UpdateHegemonyRankMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_UpdateHegemonyRankMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.hegemonyRankInfo && message.hegemonyRankInfo.length))
                    message.hegemonyRankInfo = [];
                message.hegemonyRankInfo.push($root.HegemonyRankInfo.decode(reader, reader.uint32()));
                break;
            case 2:
                message.bSett = reader.bool();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("bSett"))
            throw $util.ProtocolError("missing required 'bSett'", { instance: message });
        return message;
    };

    return SC_UpdateHegemonyRankMsg;
})();

$root.SM_EventNotice = (function() {

    function SM_EventNotice(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SM_EventNotice.prototype.playerName = "";
    SM_EventNotice.prototype.nVipLv = 0;
    SM_EventNotice.prototype.nEventType = 0;
    SM_EventNotice.prototype.nItemID = 0;
    SM_EventNotice.prototype.nCondition1 = 0;
    SM_EventNotice.prototype.nCondition2 = 0;
    SM_EventNotice.prototype.nCondition3 = 0;

    SM_EventNotice.create = function create(properties) {
        return new SM_EventNotice(properties);
    };

    SM_EventNotice.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.playerName);
        writer.uint32(16).sint32(message.nVipLv);
        writer.uint32(24).sint32(message.nEventType);
        if (message.nItemID != null && Object.hasOwnProperty.call(message, "nItemID"))
            writer.uint32(32).sint32(message.nItemID);
        writer.uint32(40).sint32(message.nCondition1);
        writer.uint32(48).sint32(message.nCondition2);
        writer.uint32(56).sint32(message.nCondition3);
        return writer;
    };

    SM_EventNotice.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SM_EventNotice();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.playerName = reader.string();
                break;
            case 2:
                message.nVipLv = reader.sint32();
                break;
            case 3:
                message.nEventType = reader.sint32();
                break;
            case 4:
                message.nItemID = reader.sint32();
                break;
            case 5:
                message.nCondition1 = reader.sint32();
                break;
            case 6:
                message.nCondition2 = reader.sint32();
                break;
            case 7:
                message.nCondition3 = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("playerName"))
            throw $util.ProtocolError("missing required 'playerName'", { instance: message });
        if (!message.hasOwnProperty("nVipLv"))
            throw $util.ProtocolError("missing required 'nVipLv'", { instance: message });
        if (!message.hasOwnProperty("nEventType"))
            throw $util.ProtocolError("missing required 'nEventType'", { instance: message });
        if (!message.hasOwnProperty("nCondition1"))
            throw $util.ProtocolError("missing required 'nCondition1'", { instance: message });
        if (!message.hasOwnProperty("nCondition2"))
            throw $util.ProtocolError("missing required 'nCondition2'", { instance: message });
        if (!message.hasOwnProperty("nCondition3"))
            throw $util.ProtocolError("missing required 'nCondition3'", { instance: message });
        return message;
    };

    return SM_EventNotice;
})();

$root.CS_AskShangZhuangList_Res = (function() {

    function CS_AskShangZhuangList_Res(properties) {
        this.listZhuangJia = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_AskShangZhuangList_Res.prototype.listZhuangJia = $util.emptyArray;

    CS_AskShangZhuangList_Res.create = function create(properties) {
        return new CS_AskShangZhuangList_Res(properties);
    };

    CS_AskShangZhuangList_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.listZhuangJia != null && message.listZhuangJia.length)
            for (var i = 0; i < message.listZhuangJia.length; ++i)
                $root.PlayerInfo_DT.encode(message.listZhuangJia[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    CS_AskShangZhuangList_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_AskShangZhuangList_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.listZhuangJia && message.listZhuangJia.length))
                    message.listZhuangJia = [];
                message.listZhuangJia.push($root.PlayerInfo_DT.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CS_AskShangZhuangList_Res;
})();

$root.PlayerInfo_DT = (function() {

    function PlayerInfo_DT(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    PlayerInfo_DT.prototype.strNickname = "";
    PlayerInfo_DT.prototype.nHeadID = 0;
    PlayerInfo_DT.prototype.dChipNum = 0;

    PlayerInfo_DT.create = function create(properties) {
        return new PlayerInfo_DT(properties);
    };

    PlayerInfo_DT.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strNickname);
        writer.uint32(16).sint32(message.nHeadID);
        writer.uint32(25).double(message.dChipNum);
        return writer;
    };

    PlayerInfo_DT.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PlayerInfo_DT();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strNickname = reader.string();
                break;
            case 2:
                message.nHeadID = reader.sint32();
                break;
            case 3:
                message.dChipNum = reader.double();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strNickname"))
            throw $util.ProtocolError("missing required 'strNickname'", { instance: message });
        if (!message.hasOwnProperty("nHeadID"))
            throw $util.ProtocolError("missing required 'nHeadID'", { instance: message });
        if (!message.hasOwnProperty("dChipNum"))
            throw $util.ProtocolError("missing required 'dChipNum'", { instance: message });
        return message;
    };

    return PlayerInfo_DT;
})();

$root.CS_JoinDTStageMsg_Res = (function() {

    function CS_JoinDTStageMsg_Res(properties) {
        this.listRoadShow = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_JoinDTStageMsg_Res.prototype.nRes = 0;
    CS_JoinDTStageMsg_Res.prototype.strRoomID = "";
    CS_JoinDTStageMsg_Res.prototype.listRoadShow = $util.emptyArray;
    CS_JoinDTStageMsg_Res.prototype.nState = 0;
    CS_JoinDTStageMsg_Res.prototype.strEndTime = "";
    CS_JoinDTStageMsg_Res.prototype.strZJNickname = "";
    CS_JoinDTStageMsg_Res.prototype.dZJChipNum = 0;
    CS_JoinDTStageMsg_Res.prototype.nZJHeadID = 0;
    CS_JoinDTStageMsg_Res.prototype.nDragonNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_JoinDTStageMsg_Res.prototype.nTigerNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_JoinDTStageMsg_Res.prototype.nAndNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CS_JoinDTStageMsg_Res.prototype.nPersonDragonNum = 0;
    CS_JoinDTStageMsg_Res.prototype.nPersonTigerNum = 0;
    CS_JoinDTStageMsg_Res.prototype.nPersonAndNum = 0;
    CS_JoinDTStageMsg_Res.prototype.nDragonPoker = 0;
    CS_JoinDTStageMsg_Res.prototype.nDragonPokerHua = 0;
    CS_JoinDTStageMsg_Res.prototype.nTigerPoker = 0;
    CS_JoinDTStageMsg_Res.prototype.nTigerPokerHua = 0;
    CS_JoinDTStageMsg_Res.prototype.nZuoZhuangNum = 0;
    CS_JoinDTStageMsg_Res.prototype.nShangZhuangMin = 0;
    CS_JoinDTStageMsg_Res.prototype.dChipNum = 0;

    CS_JoinDTStageMsg_Res.create = function create(properties) {
        return new CS_JoinDTStageMsg_Res(properties);
    };

    CS_JoinDTStageMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        if (message.strRoomID != null && Object.hasOwnProperty.call(message, "strRoomID"))
            writer.uint32(18).string(message.strRoomID);
        if (message.listRoadShow != null && message.listRoadShow.length)
            for (var i = 0; i < message.listRoadShow.length; ++i)
                writer.uint32(24).sint32(message.listRoadShow[i]);
        writer.uint32(32).int32(message.nState);
        writer.uint32(42).string(message.strEndTime);
        writer.uint32(50).string(message.strZJNickname);
        writer.uint32(57).double(message.dZJChipNum);
        writer.uint32(64).sint32(message.nZJHeadID);
        writer.uint32(72).sint64(message.nDragonNum);
        writer.uint32(80).sint64(message.nTigerNum);
        writer.uint32(88).sint64(message.nAndNum);
        writer.uint32(96).sint32(message.nPersonDragonNum);
        writer.uint32(104).sint32(message.nPersonTigerNum);
        writer.uint32(112).sint32(message.nPersonAndNum);
        writer.uint32(120).sint32(message.nDragonPoker);
        writer.uint32(128).sint32(message.nDragonPokerHua);
        writer.uint32(136).sint32(message.nTigerPoker);
        writer.uint32(144).sint32(message.nTigerPokerHua);
        writer.uint32(152).sint32(message.nZuoZhuangNum);
        writer.uint32(160).sint32(message.nShangZhuangMin);
        writer.uint32(169).double(message.dChipNum);
        return writer;
    };

    CS_JoinDTStageMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_JoinDTStageMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                message.strRoomID = reader.string();
                break;
            case 3:
                if (!(message.listRoadShow && message.listRoadShow.length))
                    message.listRoadShow = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.listRoadShow.push(reader.sint32());
                } else
                    message.listRoadShow.push(reader.sint32());
                break;
            case 4:
                message.nState = reader.int32();
                break;
            case 5:
                message.strEndTime = reader.string();
                break;
            case 6:
                message.strZJNickname = reader.string();
                break;
            case 7:
                message.dZJChipNum = reader.double();
                break;
            case 8:
                message.nZJHeadID = reader.sint32();
                break;
            case 9:
                message.nDragonNum = reader.sint64();
                break;
            case 10:
                message.nTigerNum = reader.sint64();
                break;
            case 11:
                message.nAndNum = reader.sint64();
                break;
            case 12:
                message.nPersonDragonNum = reader.sint32();
                break;
            case 13:
                message.nPersonTigerNum = reader.sint32();
                break;
            case 14:
                message.nPersonAndNum = reader.sint32();
                break;
            case 15:
                message.nDragonPoker = reader.sint32();
                break;
            case 16:
                message.nDragonPokerHua = reader.sint32();
                break;
            case 17:
                message.nTigerPoker = reader.sint32();
                break;
            case 18:
                message.nTigerPokerHua = reader.sint32();
                break;
            case 19:
                message.nZuoZhuangNum = reader.sint32();
                break;
            case 20:
                message.nShangZhuangMin = reader.sint32();
                break;
            case 21:
                message.dChipNum = reader.double();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        if (!message.hasOwnProperty("nState"))
            throw $util.ProtocolError("missing required 'nState'", { instance: message });
        if (!message.hasOwnProperty("strEndTime"))
            throw $util.ProtocolError("missing required 'strEndTime'", { instance: message });
        if (!message.hasOwnProperty("strZJNickname"))
            throw $util.ProtocolError("missing required 'strZJNickname'", { instance: message });
        if (!message.hasOwnProperty("dZJChipNum"))
            throw $util.ProtocolError("missing required 'dZJChipNum'", { instance: message });
        if (!message.hasOwnProperty("nZJHeadID"))
            throw $util.ProtocolError("missing required 'nZJHeadID'", { instance: message });
        if (!message.hasOwnProperty("nDragonNum"))
            throw $util.ProtocolError("missing required 'nDragonNum'", { instance: message });
        if (!message.hasOwnProperty("nTigerNum"))
            throw $util.ProtocolError("missing required 'nTigerNum'", { instance: message });
        if (!message.hasOwnProperty("nAndNum"))
            throw $util.ProtocolError("missing required 'nAndNum'", { instance: message });
        if (!message.hasOwnProperty("nPersonDragonNum"))
            throw $util.ProtocolError("missing required 'nPersonDragonNum'", { instance: message });
        if (!message.hasOwnProperty("nPersonTigerNum"))
            throw $util.ProtocolError("missing required 'nPersonTigerNum'", { instance: message });
        if (!message.hasOwnProperty("nPersonAndNum"))
            throw $util.ProtocolError("missing required 'nPersonAndNum'", { instance: message });
        if (!message.hasOwnProperty("nDragonPoker"))
            throw $util.ProtocolError("missing required 'nDragonPoker'", { instance: message });
        if (!message.hasOwnProperty("nDragonPokerHua"))
            throw $util.ProtocolError("missing required 'nDragonPokerHua'", { instance: message });
        if (!message.hasOwnProperty("nTigerPoker"))
            throw $util.ProtocolError("missing required 'nTigerPoker'", { instance: message });
        if (!message.hasOwnProperty("nTigerPokerHua"))
            throw $util.ProtocolError("missing required 'nTigerPokerHua'", { instance: message });
        if (!message.hasOwnProperty("nZuoZhuangNum"))
            throw $util.ProtocolError("missing required 'nZuoZhuangNum'", { instance: message });
        if (!message.hasOwnProperty("nShangZhuangMin"))
            throw $util.ProtocolError("missing required 'nShangZhuangMin'", { instance: message });
        if (!message.hasOwnProperty("dChipNum"))
            throw $util.ProtocolError("missing required 'dChipNum'", { instance: message });
        return message;
    };

    return CS_JoinDTStageMsg_Res;
})();

$root.CS_PublicMsg = (function() {

    function CS_PublicMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_PublicMsg.prototype.nidLogin = 0;

    CS_PublicMsg.create = function create(properties) {
        return new CS_PublicMsg(properties);
    };

    CS_PublicMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).int32(message.nidLogin);
        return writer;
    };

    CS_PublicMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_PublicMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.int32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        return message;
    };

    return CS_PublicMsg;
})();

$root.CS_ShangZhuang_Res = (function() {

    function CS_ShangZhuang_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ShangZhuang_Res.prototype.nRes = 0;

    CS_ShangZhuang_Res.create = function create(properties) {
        return new CS_ShangZhuang_Res(properties);
    };

    CS_ShangZhuang_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).int32(message.nRes);
        return writer;
    };

    CS_ShangZhuang_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ShangZhuang_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.int32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        return message;
    };

    return CS_ShangZhuang_Res;
})();

$root.CS_XiaZhu_Res = (function() {

    function CS_XiaZhu_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_XiaZhu_Res.prototype.nXiaZhuNum = 0;
    CS_XiaZhu_Res.prototype.dChipNum = 0;
    CS_XiaZhu_Res.prototype.nPersonDragonNum = 0;
    CS_XiaZhu_Res.prototype.nPersonTigerNum = 0;
    CS_XiaZhu_Res.prototype.nPersonAndNum = 0;

    CS_XiaZhu_Res.create = function create(properties) {
        return new CS_XiaZhu_Res(properties);
    };

    CS_XiaZhu_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).int32(message.nXiaZhuNum);
        writer.uint32(17).double(message.dChipNum);
        writer.uint32(24).sint32(message.nPersonDragonNum);
        writer.uint32(32).sint32(message.nPersonTigerNum);
        writer.uint32(40).sint32(message.nPersonAndNum);
        return writer;
    };

    CS_XiaZhu_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_XiaZhu_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nXiaZhuNum = reader.int32();
                break;
            case 2:
                message.dChipNum = reader.double();
                break;
            case 3:
                message.nPersonDragonNum = reader.sint32();
                break;
            case 4:
                message.nPersonTigerNum = reader.sint32();
                break;
            case 5:
                message.nPersonAndNum = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nXiaZhuNum"))
            throw $util.ProtocolError("missing required 'nXiaZhuNum'", { instance: message });
        if (!message.hasOwnProperty("dChipNum"))
            throw $util.ProtocolError("missing required 'dChipNum'", { instance: message });
        if (!message.hasOwnProperty("nPersonDragonNum"))
            throw $util.ProtocolError("missing required 'nPersonDragonNum'", { instance: message });
        if (!message.hasOwnProperty("nPersonTigerNum"))
            throw $util.ProtocolError("missing required 'nPersonTigerNum'", { instance: message });
        if (!message.hasOwnProperty("nPersonAndNum"))
            throw $util.ProtocolError("missing required 'nPersonAndNum'", { instance: message });
        return message;
    };

    return CS_XiaZhu_Res;
})();

$root.CS_XiaZhu = (function() {

    function CS_XiaZhu(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_XiaZhu.prototype.nXiaZhuNum = 0;
    CS_XiaZhu.prototype.nXiaZhuType = 0;

    CS_XiaZhu.create = function create(properties) {
        return new CS_XiaZhu(properties);
    };

    CS_XiaZhu.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).int32(message.nXiaZhuNum);
        writer.uint32(16).int32(message.nXiaZhuType);
        return writer;
    };

    CS_XiaZhu.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_XiaZhu();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nXiaZhuNum = reader.int32();
                break;
            case 2:
                message.nXiaZhuType = reader.int32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nXiaZhuNum"))
            throw $util.ProtocolError("missing required 'nXiaZhuNum'", { instance: message });
        if (!message.hasOwnProperty("nXiaZhuType"))
            throw $util.ProtocolError("missing required 'nXiaZhuType'", { instance: message });
        return message;
    };

    return CS_XiaZhu;
})();

$root.SC_PlayerXiaZhu = (function() {

    function SC_PlayerXiaZhu(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_PlayerXiaZhu.prototype.nXiaZhuNum = 0;
    SC_PlayerXiaZhu.prototype.nXiaZhuType = 0;
    SC_PlayerXiaZhu.prototype.nAllXiaZhuNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    SC_PlayerXiaZhu.create = function create(properties) {
        return new SC_PlayerXiaZhu(properties);
    };

    SC_PlayerXiaZhu.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nXiaZhuNum);
        writer.uint32(16).sint32(message.nXiaZhuType);
        writer.uint32(24).sint64(message.nAllXiaZhuNum);
        return writer;
    };

    SC_PlayerXiaZhu.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_PlayerXiaZhu();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nXiaZhuNum = reader.sint32();
                break;
            case 2:
                message.nXiaZhuType = reader.sint32();
                break;
            case 3:
                message.nAllXiaZhuNum = reader.sint64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nXiaZhuNum"))
            throw $util.ProtocolError("missing required 'nXiaZhuNum'", { instance: message });
        if (!message.hasOwnProperty("nXiaZhuType"))
            throw $util.ProtocolError("missing required 'nXiaZhuType'", { instance: message });
        if (!message.hasOwnProperty("nAllXiaZhuNum"))
            throw $util.ProtocolError("missing required 'nAllXiaZhuNum'", { instance: message });
        return message;
    };

    return SC_PlayerXiaZhu;
})();

$root.SC_PlayerZuoZhuang = (function() {

    function SC_PlayerZuoZhuang(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_PlayerZuoZhuang.prototype.strNickname = "";
    SC_PlayerZuoZhuang.prototype.dChipNum = 0;
    SC_PlayerZuoZhuang.prototype.nHeadID = 0;

    SC_PlayerZuoZhuang.create = function create(properties) {
        return new SC_PlayerZuoZhuang(properties);
    };

    SC_PlayerZuoZhuang.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strNickname);
        writer.uint32(17).double(message.dChipNum);
        writer.uint32(24).sint32(message.nHeadID);
        return writer;
    };

    SC_PlayerZuoZhuang.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_PlayerZuoZhuang();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strNickname = reader.string();
                break;
            case 2:
                message.dChipNum = reader.double();
                break;
            case 3:
                message.nHeadID = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strNickname"))
            throw $util.ProtocolError("missing required 'strNickname'", { instance: message });
        if (!message.hasOwnProperty("dChipNum"))
            throw $util.ProtocolError("missing required 'dChipNum'", { instance: message });
        if (!message.hasOwnProperty("nHeadID"))
            throw $util.ProtocolError("missing required 'nHeadID'", { instance: message });
        return message;
    };

    return SC_PlayerZuoZhuang;
})();

$root.SC_Result = (function() {

    function SC_Result(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_Result.prototype.nWin = 0;
    SC_Result.prototype.nPokerDragon = 0;
    SC_Result.prototype.nPokerDragonHua = 0;
    SC_Result.prototype.nPokerTiger = 0;
    SC_Result.prototype.nPokerTigerHua = 0;
    SC_Result.prototype.dBankerGetChip = 0;
    SC_Result.prototype.dBankerChip = 0;
    SC_Result.prototype.dPlayerGetChip = 0;
    SC_Result.prototype.dPlayerChip = 0;

    SC_Result.create = function create(properties) {
        return new SC_Result(properties);
    };

    SC_Result.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).int32(message.nWin);
        writer.uint32(16).int32(message.nPokerDragon);
        writer.uint32(24).int32(message.nPokerDragonHua);
        writer.uint32(32).int32(message.nPokerTiger);
        writer.uint32(40).int32(message.nPokerTigerHua);
        writer.uint32(49).double(message.dBankerGetChip);
        writer.uint32(57).double(message.dBankerChip);
        writer.uint32(65).double(message.dPlayerGetChip);
        writer.uint32(73).double(message.dPlayerChip);
        return writer;
    };

    SC_Result.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_Result();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nWin = reader.int32();
                break;
            case 2:
                message.nPokerDragon = reader.int32();
                break;
            case 3:
                message.nPokerDragonHua = reader.int32();
                break;
            case 4:
                message.nPokerTiger = reader.int32();
                break;
            case 5:
                message.nPokerTigerHua = reader.int32();
                break;
            case 6:
                message.dBankerGetChip = reader.double();
                break;
            case 7:
                message.dBankerChip = reader.double();
                break;
            case 8:
                message.dPlayerGetChip = reader.double();
                break;
            case 9:
                message.dPlayerChip = reader.double();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nWin"))
            throw $util.ProtocolError("missing required 'nWin'", { instance: message });
        if (!message.hasOwnProperty("nPokerDragon"))
            throw $util.ProtocolError("missing required 'nPokerDragon'", { instance: message });
        if (!message.hasOwnProperty("nPokerDragonHua"))
            throw $util.ProtocolError("missing required 'nPokerDragonHua'", { instance: message });
        if (!message.hasOwnProperty("nPokerTiger"))
            throw $util.ProtocolError("missing required 'nPokerTiger'", { instance: message });
        if (!message.hasOwnProperty("nPokerTigerHua"))
            throw $util.ProtocolError("missing required 'nPokerTigerHua'", { instance: message });
        if (!message.hasOwnProperty("dBankerGetChip"))
            throw $util.ProtocolError("missing required 'dBankerGetChip'", { instance: message });
        if (!message.hasOwnProperty("dBankerChip"))
            throw $util.ProtocolError("missing required 'dBankerChip'", { instance: message });
        if (!message.hasOwnProperty("dPlayerGetChip"))
            throw $util.ProtocolError("missing required 'dPlayerGetChip'", { instance: message });
        if (!message.hasOwnProperty("dPlayerChip"))
            throw $util.ProtocolError("missing required 'dPlayerChip'", { instance: message });
        return message;
    };

    return SC_Result;
})();

$root.SC_StateChange = (function() {

    function SC_StateChange(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_StateChange.prototype.nState = 0;
    SC_StateChange.prototype.nTime = 0;
    SC_StateChange.prototype.nZuoZhuangNum = 0;

    SC_StateChange.create = function create(properties) {
        return new SC_StateChange(properties);
    };

    SC_StateChange.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).int32(message.nState);
        writer.uint32(16).int32(message.nTime);
        writer.uint32(24).sint32(message.nZuoZhuangNum);
        return writer;
    };

    SC_StateChange.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_StateChange();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nState = reader.int32();
                break;
            case 2:
                message.nTime = reader.int32();
                break;
            case 3:
                message.nZuoZhuangNum = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nState"))
            throw $util.ProtocolError("missing required 'nState'", { instance: message });
        if (!message.hasOwnProperty("nTime"))
            throw $util.ProtocolError("missing required 'nTime'", { instance: message });
        if (!message.hasOwnProperty("nZuoZhuangNum"))
            throw $util.ProtocolError("missing required 'nZuoZhuangNum'", { instance: message });
        return message;
    };

    return SC_StateChange;
})();

$root.SC_XiaZhuInfo = (function() {

    function SC_XiaZhuInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    SC_XiaZhuInfo.prototype.nXiaZhuDragon = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    SC_XiaZhuInfo.prototype.nXiaZhuTiger = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    SC_XiaZhuInfo.prototype.nXiaZhuAnd = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    SC_XiaZhuInfo.prototype.dZhuangJia = 0;

    SC_XiaZhuInfo.create = function create(properties) {
        return new SC_XiaZhuInfo(properties);
    };

    SC_XiaZhuInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint64(message.nXiaZhuDragon);
        writer.uint32(16).sint64(message.nXiaZhuTiger);
        writer.uint32(24).sint64(message.nXiaZhuAnd);
        writer.uint32(33).double(message.dZhuangJia);
        return writer;
    };

    SC_XiaZhuInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SC_XiaZhuInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nXiaZhuDragon = reader.sint64();
                break;
            case 2:
                message.nXiaZhuTiger = reader.sint64();
                break;
            case 3:
                message.nXiaZhuAnd = reader.sint64();
                break;
            case 4:
                message.dZhuangJia = reader.double();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nXiaZhuDragon"))
            throw $util.ProtocolError("missing required 'nXiaZhuDragon'", { instance: message });
        if (!message.hasOwnProperty("nXiaZhuTiger"))
            throw $util.ProtocolError("missing required 'nXiaZhuTiger'", { instance: message });
        if (!message.hasOwnProperty("nXiaZhuAnd"))
            throw $util.ProtocolError("missing required 'nXiaZhuAnd'", { instance: message });
        if (!message.hasOwnProperty("dZhuangJia"))
            throw $util.ProtocolError("missing required 'dZhuangJia'", { instance: message });
        return message;
    };

    return SC_XiaZhuInfo;
})();

$root.CH_AllTaskInfo_Res = (function() {

    function CH_AllTaskInfo_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AllTaskInfo_Res.prototype.strDayMission = "";
    CH_AllTaskInfo_Res.prototype.strWeekMission = "";
    CH_AllTaskInfo_Res.prototype.strSeriesRecord = "";

    CH_AllTaskInfo_Res.create = function create(properties) {
        return new CH_AllTaskInfo_Res(properties);
    };

    CH_AllTaskInfo_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strDayMission);
        writer.uint32(18).string(message.strWeekMission);
        writer.uint32(26).string(message.strSeriesRecord);
        return writer;
    };

    CH_AllTaskInfo_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AllTaskInfo_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strDayMission = reader.string();
                break;
            case 2:
                message.strWeekMission = reader.string();
                break;
            case 3:
                message.strSeriesRecord = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strDayMission"))
            throw $util.ProtocolError("missing required 'strDayMission'", { instance: message });
        if (!message.hasOwnProperty("strWeekMission"))
            throw $util.ProtocolError("missing required 'strWeekMission'", { instance: message });
        if (!message.hasOwnProperty("strSeriesRecord"))
            throw $util.ProtocolError("missing required 'strSeriesRecord'", { instance: message });
        return message;
    };

    return CH_AllTaskInfo_Res;
})();

$root.CH_AllTaskInfo = (function() {

    function CH_AllTaskInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AllTaskInfo.prototype.nidLogin = 0;
    CH_AllTaskInfo.prototype.strSafeCode = "";

    CH_AllTaskInfo.create = function create(properties) {
        return new CH_AllTaskInfo(properties);
    };

    CH_AllTaskInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        return writer;
    };

    CH_AllTaskInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AllTaskInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CH_AllTaskInfo;
})();

$root.CH_AskGateMsg_Res = (function() {

    function CH_AskGateMsg_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskGateMsg_Res.prototype.nGateID = 0;
    CH_AskGateMsg_Res.prototype.strIP = "";
    CH_AskGateMsg_Res.prototype.nPort = 0;

    CH_AskGateMsg_Res.create = function create(properties) {
        return new CH_AskGateMsg_Res(properties);
    };

    CH_AskGateMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nGateID);
        writer.uint32(18).string(message.strIP);
        writer.uint32(24).sint32(message.nPort);
        return writer;
    };

    CH_AskGateMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskGateMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nGateID = reader.sint32();
                break;
            case 2:
                message.strIP = reader.string();
                break;
            case 3:
                message.nPort = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nGateID"))
            throw $util.ProtocolError("missing required 'nGateID'", { instance: message });
        if (!message.hasOwnProperty("strIP"))
            throw $util.ProtocolError("missing required 'strIP'", { instance: message });
        if (!message.hasOwnProperty("nPort"))
            throw $util.ProtocolError("missing required 'nPort'", { instance: message });
        return message;
    };

    return CH_AskGateMsg_Res;
})();

$root.CH_AskGateMsg = (function() {

    function CH_AskGateMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskGateMsg.prototype.nidLogin = 0;
    CH_AskGateMsg.prototype.strSafeCode = "";
    CH_AskGateMsg.prototype.strVer = "";

    CH_AskGateMsg.create = function create(properties) {
        return new CH_AskGateMsg(properties);
    };

    CH_AskGateMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(26).string(message.strVer);
        return writer;
    };

    CH_AskGateMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskGateMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.strVer = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("strVer"))
            throw $util.ProtocolError("missing required 'strVer'", { instance: message });
        return message;
    };

    return CH_AskGateMsg;
})();

$root.CH_GetRewardCouponMsg_Res = (function() {

    function CH_GetRewardCouponMsg_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_GetRewardCouponMsg_Res.prototype.nCouponsNum = 0;
    CH_GetRewardCouponMsg_Res.prototype.nIsNewPlayer = 0;

    CH_GetRewardCouponMsg_Res.create = function create(properties) {
        return new CH_GetRewardCouponMsg_Res(properties);
    };

    CH_GetRewardCouponMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nCouponsNum);
        writer.uint32(16).sint32(message.nIsNewPlayer);
        return writer;
    };

    CH_GetRewardCouponMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_GetRewardCouponMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nCouponsNum = reader.sint32();
                break;
            case 2:
                message.nIsNewPlayer = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nCouponsNum"))
            throw $util.ProtocolError("missing required 'nCouponsNum'", { instance: message });
        if (!message.hasOwnProperty("nIsNewPlayer"))
            throw $util.ProtocolError("missing required 'nIsNewPlayer'", { instance: message });
        return message;
    };

    return CH_GetRewardCouponMsg_Res;
})();

$root.CH_GetRewardCouponMsg = (function() {

    function CH_GetRewardCouponMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_GetRewardCouponMsg.create = function create(properties) {
        return new CH_GetRewardCouponMsg(properties);
    };

    CH_GetRewardCouponMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        return writer;
    };

    CH_GetRewardCouponMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_GetRewardCouponMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_GetRewardCouponMsg;
})();

$root.CH_IsNewPlayerMsg = (function() {

    function CH_IsNewPlayerMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_IsNewPlayerMsg.prototype.nLotteryBase = "";
    CH_IsNewPlayerMsg.prototype.nIsNewPlayer = 0;
    CH_IsNewPlayerMsg.prototype.nSignTimes = 0;

    CH_IsNewPlayerMsg.create = function create(properties) {
        return new CH_IsNewPlayerMsg(properties);
    };

    CH_IsNewPlayerMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.nLotteryBase);
        writer.uint32(16).sint32(message.nIsNewPlayer);
        writer.uint32(24).sint32(message.nSignTimes);
        return writer;
    };

    CH_IsNewPlayerMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_IsNewPlayerMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nLotteryBase = reader.string();
                break;
            case 2:
                message.nIsNewPlayer = reader.sint32();
                break;
            case 3:
                message.nSignTimes = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nLotteryBase"))
            throw $util.ProtocolError("missing required 'nLotteryBase'", { instance: message });
        if (!message.hasOwnProperty("nIsNewPlayer"))
            throw $util.ProtocolError("missing required 'nIsNewPlayer'", { instance: message });
        if (!message.hasOwnProperty("nSignTimes"))
            throw $util.ProtocolError("missing required 'nSignTimes'", { instance: message });
        return message;
    };

    return CH_IsNewPlayerMsg;
})();

$root.CH_LoginMsg_Res = (function() {

    function CH_LoginMsg_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_LoginMsg_Res.prototype.nRes = 0;
    CH_LoginMsg_Res.prototype.nidLogin = 0;
    CH_LoginMsg_Res.prototype.strNickname = "";
    CH_LoginMsg_Res.prototype.nPlayerLevel = 0;
    CH_LoginMsg_Res.prototype.nVIPLevel = 0;
    CH_LoginMsg_Res.prototype.strPhone = "";
    CH_LoginMsg_Res.prototype.nPermission = 0;
    CH_LoginMsg_Res.prototype.strGamePlayerID = "";
    CH_LoginMsg_Res.prototype.strAutoPassword = "";
    CH_LoginMsg_Res.prototype.strDailyData = "";
    CH_LoginMsg_Res.prototype.strOneTimeData = "";
    CH_LoginMsg_Res.prototype.strBlockadeTime = "";
    CH_LoginMsg_Res.prototype.nGoldNum = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CH_LoginMsg_Res.prototype.nDiamondNum = 0;
    CH_LoginMsg_Res.prototype.strIdentity = "";
    CH_LoginMsg_Res.prototype.strSafeCode = "";
    CH_LoginMsg_Res.prototype.strGateName = "";
    CH_LoginMsg_Res.prototype.nFortData = 0;
    CH_LoginMsg_Res.prototype.nJackpot = 0;
    CH_LoginMsg_Res.prototype.nExp = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CH_LoginMsg_Res.prototype.nSex = 0;
    CH_LoginMsg_Res.prototype.nHeadType = 0;
    CH_LoginMsg_Res.prototype.strItemList = "";
    CH_LoginMsg_Res.prototype.strDayMission = "";
    CH_LoginMsg_Res.prototype.strWeekMission = "";
    CH_LoginMsg_Res.prototype.nSeriesForeverRecord = 0;
    CH_LoginMsg_Res.prototype.strSeriesRecord = "";
    CH_LoginMsg_Res.prototype.nFortSkinUse = 0;
    CH_LoginMsg_Res.prototype.nVIPExp = 0;
    CH_LoginMsg_Res.prototype.strWeekData = "";
    CH_LoginMsg_Res.prototype.nCoupons = 0;
    CH_LoginMsg_Res.prototype.nLottery = 0;
    CH_LoginMsg_Res.prototype.strVIPEndTime = "";
    CH_LoginMsg_Res.prototype.strLoginData = "";
    CH_LoginMsg_Res.prototype.strFortSkin = "";
    CH_LoginMsg_Res.prototype.strNowTime = "";
    CH_LoginMsg_Res.prototype.strEmailData = "";
    CH_LoginMsg_Res.prototype.nChannelID = 0;
    CH_LoginMsg_Res.prototype.strCtxID = "";
    CH_LoginMsg_Res.prototype.nUpdateRankTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    CH_LoginMsg_Res.prototype.fAccPoints = 0;
    CH_LoginMsg_Res.prototype.strUID = "";
    CH_LoginMsg_Res.prototype.bReturn = false;
    CH_LoginMsg_Res.prototype.nRoomType = 0;
    CH_LoginMsg_Res.prototype.playerSignature = "";
    CH_LoginMsg_Res.prototype.nLotteryBase = "";
    CH_LoginMsg_Res.prototype.nIsNewPlayer = 0;
    CH_LoginMsg_Res.prototype.nSignTimes = 0;
    CH_LoginMsg_Res.prototype.nGuideMarkRes = 0;
    CH_LoginMsg_Res.prototype.dChipNum = 0;

    CH_LoginMsg_Res.create = function create(properties) {
        return new CH_LoginMsg_Res(properties);
    };

    CH_LoginMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        writer.uint32(16).sint32(message.nidLogin);
        writer.uint32(26).string(message.strNickname);
        writer.uint32(32).sint32(message.nPlayerLevel);
        writer.uint32(40).sint32(message.nVIPLevel);
        writer.uint32(50).string(message.strPhone);
        writer.uint32(56).sint32(message.nPermission);
        writer.uint32(66).string(message.strGamePlayerID);
        writer.uint32(74).string(message.strAutoPassword);
        writer.uint32(82).string(message.strDailyData);
        writer.uint32(90).string(message.strOneTimeData);
        writer.uint32(98).string(message.strBlockadeTime);
        writer.uint32(104).sint64(message.nGoldNum);
        writer.uint32(112).sint32(message.nDiamondNum);
        writer.uint32(122).string(message.strIdentity);
        writer.uint32(130).string(message.strSafeCode);
        if (message.strGateName != null && Object.hasOwnProperty.call(message, "strGateName"))
            writer.uint32(138).string(message.strGateName);
        writer.uint32(144).sint32(message.nFortData);
        writer.uint32(152).sint32(message.nJackpot);
        writer.uint32(160).sint64(message.nExp);
        writer.uint32(168).sint32(message.nSex);
        writer.uint32(176).sint32(message.nHeadType);
        writer.uint32(186).string(message.strItemList);
        if (message.strDayMission != null && Object.hasOwnProperty.call(message, "strDayMission"))
            writer.uint32(194).string(message.strDayMission);
        if (message.strWeekMission != null && Object.hasOwnProperty.call(message, "strWeekMission"))
            writer.uint32(202).string(message.strWeekMission);
        if (message.nSeriesForeverRecord != null && Object.hasOwnProperty.call(message, "nSeriesForeverRecord"))
            writer.uint32(208).sint32(message.nSeriesForeverRecord);
        if (message.strSeriesRecord != null && Object.hasOwnProperty.call(message, "strSeriesRecord"))
            writer.uint32(218).string(message.strSeriesRecord);
        writer.uint32(224).sint32(message.nFortSkinUse);
        writer.uint32(232).sint32(message.nVIPExp);
        writer.uint32(242).string(message.strWeekData);
        writer.uint32(248).sint32(message.nCoupons);
        writer.uint32(256).sint32(message.nLottery);
        writer.uint32(266).string(message.strVIPEndTime);
        writer.uint32(274).string(message.strLoginData);
        writer.uint32(282).string(message.strFortSkin);
        if (message.strNowTime != null && Object.hasOwnProperty.call(message, "strNowTime"))
            writer.uint32(290).string(message.strNowTime);
        writer.uint32(298).string(message.strEmailData);
        if (message.nChannelID != null && Object.hasOwnProperty.call(message, "nChannelID"))
            writer.uint32(304).sint32(message.nChannelID);
        if (message.strCtxID != null && Object.hasOwnProperty.call(message, "strCtxID"))
            writer.uint32(314).string(message.strCtxID);
        writer.uint32(320).sint64(message.nUpdateRankTime);
        writer.uint32(333).float(message.fAccPoints);
        if (message.strUID != null && Object.hasOwnProperty.call(message, "strUID"))
            writer.uint32(338).string(message.strUID);
        if (message.bReturn != null && Object.hasOwnProperty.call(message, "bReturn"))
            writer.uint32(344).bool(message.bReturn);
        if (message.nRoomType != null && Object.hasOwnProperty.call(message, "nRoomType"))
            writer.uint32(352).sint32(message.nRoomType);
        writer.uint32(362).string(message.playerSignature);
        writer.uint32(370).string(message.nLotteryBase);
        writer.uint32(376).sint32(message.nIsNewPlayer);
        writer.uint32(384).sint32(message.nSignTimes);
        writer.uint32(392).sint32(message.nGuideMarkRes);
        writer.uint32(401).double(message.dChipNum);
        return writer;
    };

    CH_LoginMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_LoginMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                message.nidLogin = reader.sint32();
                break;
            case 3:
                message.strNickname = reader.string();
                break;
            case 4:
                message.nPlayerLevel = reader.sint32();
                break;
            case 5:
                message.nVIPLevel = reader.sint32();
                break;
            case 6:
                message.strPhone = reader.string();
                break;
            case 7:
                message.nPermission = reader.sint32();
                break;
            case 8:
                message.strGamePlayerID = reader.string();
                break;
            case 9:
                message.strAutoPassword = reader.string();
                break;
            case 10:
                message.strDailyData = reader.string();
                break;
            case 11:
                message.strOneTimeData = reader.string();
                break;
            case 12:
                message.strBlockadeTime = reader.string();
                break;
            case 13:
                message.nGoldNum = reader.sint64();
                break;
            case 14:
                message.nDiamondNum = reader.sint32();
                break;
            case 15:
                message.strIdentity = reader.string();
                break;
            case 16:
                message.strSafeCode = reader.string();
                break;
            case 17:
                message.strGateName = reader.string();
                break;
            case 18:
                message.nFortData = reader.sint32();
                break;
            case 19:
                message.nJackpot = reader.sint32();
                break;
            case 20:
                message.nExp = reader.sint64();
                break;
            case 21:
                message.nSex = reader.sint32();
                break;
            case 22:
                message.nHeadType = reader.sint32();
                break;
            case 23:
                message.strItemList = reader.string();
                break;
            case 24:
                message.strDayMission = reader.string();
                break;
            case 25:
                message.strWeekMission = reader.string();
                break;
            case 26:
                message.nSeriesForeverRecord = reader.sint32();
                break;
            case 27:
                message.strSeriesRecord = reader.string();
                break;
            case 28:
                message.nFortSkinUse = reader.sint32();
                break;
            case 29:
                message.nVIPExp = reader.sint32();
                break;
            case 30:
                message.strWeekData = reader.string();
                break;
            case 31:
                message.nCoupons = reader.sint32();
                break;
            case 32:
                message.nLottery = reader.sint32();
                break;
            case 33:
                message.strVIPEndTime = reader.string();
                break;
            case 34:
                message.strLoginData = reader.string();
                break;
            case 35:
                message.strFortSkin = reader.string();
                break;
            case 36:
                message.strNowTime = reader.string();
                break;
            case 37:
                message.strEmailData = reader.string();
                break;
            case 38:
                message.nChannelID = reader.sint32();
                break;
            case 39:
                message.strCtxID = reader.string();
                break;
            case 40:
                message.nUpdateRankTime = reader.sint64();
                break;
            case 41:
                message.fAccPoints = reader.float();
                break;
            case 42:
                message.strUID = reader.string();
                break;
            case 43:
                message.bReturn = reader.bool();
                break;
            case 44:
                message.nRoomType = reader.sint32();
                break;
            case 45:
                message.playerSignature = reader.string();
                break;
            case 46:
                message.nLotteryBase = reader.string();
                break;
            case 47:
                message.nIsNewPlayer = reader.sint32();
                break;
            case 48:
                message.nSignTimes = reader.sint32();
                break;
            case 49:
                message.nGuideMarkRes = reader.sint32();
                break;
            case 50:
                message.dChipNum = reader.double();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strNickname"))
            throw $util.ProtocolError("missing required 'strNickname'", { instance: message });
        if (!message.hasOwnProperty("nPlayerLevel"))
            throw $util.ProtocolError("missing required 'nPlayerLevel'", { instance: message });
        if (!message.hasOwnProperty("nVIPLevel"))
            throw $util.ProtocolError("missing required 'nVIPLevel'", { instance: message });
        if (!message.hasOwnProperty("strPhone"))
            throw $util.ProtocolError("missing required 'strPhone'", { instance: message });
        if (!message.hasOwnProperty("nPermission"))
            throw $util.ProtocolError("missing required 'nPermission'", { instance: message });
        if (!message.hasOwnProperty("strGamePlayerID"))
            throw $util.ProtocolError("missing required 'strGamePlayerID'", { instance: message });
        if (!message.hasOwnProperty("strAutoPassword"))
            throw $util.ProtocolError("missing required 'strAutoPassword'", { instance: message });
        if (!message.hasOwnProperty("strDailyData"))
            throw $util.ProtocolError("missing required 'strDailyData'", { instance: message });
        if (!message.hasOwnProperty("strOneTimeData"))
            throw $util.ProtocolError("missing required 'strOneTimeData'", { instance: message });
        if (!message.hasOwnProperty("strBlockadeTime"))
            throw $util.ProtocolError("missing required 'strBlockadeTime'", { instance: message });
        if (!message.hasOwnProperty("nGoldNum"))
            throw $util.ProtocolError("missing required 'nGoldNum'", { instance: message });
        if (!message.hasOwnProperty("nDiamondNum"))
            throw $util.ProtocolError("missing required 'nDiamondNum'", { instance: message });
        if (!message.hasOwnProperty("strIdentity"))
            throw $util.ProtocolError("missing required 'strIdentity'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nFortData"))
            throw $util.ProtocolError("missing required 'nFortData'", { instance: message });
        if (!message.hasOwnProperty("nJackpot"))
            throw $util.ProtocolError("missing required 'nJackpot'", { instance: message });
        if (!message.hasOwnProperty("nExp"))
            throw $util.ProtocolError("missing required 'nExp'", { instance: message });
        if (!message.hasOwnProperty("nSex"))
            throw $util.ProtocolError("missing required 'nSex'", { instance: message });
        if (!message.hasOwnProperty("nHeadType"))
            throw $util.ProtocolError("missing required 'nHeadType'", { instance: message });
        if (!message.hasOwnProperty("strItemList"))
            throw $util.ProtocolError("missing required 'strItemList'", { instance: message });
        if (!message.hasOwnProperty("nFortSkinUse"))
            throw $util.ProtocolError("missing required 'nFortSkinUse'", { instance: message });
        if (!message.hasOwnProperty("nVIPExp"))
            throw $util.ProtocolError("missing required 'nVIPExp'", { instance: message });
        if (!message.hasOwnProperty("strWeekData"))
            throw $util.ProtocolError("missing required 'strWeekData'", { instance: message });
        if (!message.hasOwnProperty("nCoupons"))
            throw $util.ProtocolError("missing required 'nCoupons'", { instance: message });
        if (!message.hasOwnProperty("nLottery"))
            throw $util.ProtocolError("missing required 'nLottery'", { instance: message });
        if (!message.hasOwnProperty("strVIPEndTime"))
            throw $util.ProtocolError("missing required 'strVIPEndTime'", { instance: message });
        if (!message.hasOwnProperty("strLoginData"))
            throw $util.ProtocolError("missing required 'strLoginData'", { instance: message });
        if (!message.hasOwnProperty("strFortSkin"))
            throw $util.ProtocolError("missing required 'strFortSkin'", { instance: message });
        if (!message.hasOwnProperty("strEmailData"))
            throw $util.ProtocolError("missing required 'strEmailData'", { instance: message });
        if (!message.hasOwnProperty("nUpdateRankTime"))
            throw $util.ProtocolError("missing required 'nUpdateRankTime'", { instance: message });
        if (!message.hasOwnProperty("fAccPoints"))
            throw $util.ProtocolError("missing required 'fAccPoints'", { instance: message });
        if (!message.hasOwnProperty("playerSignature"))
            throw $util.ProtocolError("missing required 'playerSignature'", { instance: message });
        if (!message.hasOwnProperty("nLotteryBase"))
            throw $util.ProtocolError("missing required 'nLotteryBase'", { instance: message });
        if (!message.hasOwnProperty("nIsNewPlayer"))
            throw $util.ProtocolError("missing required 'nIsNewPlayer'", { instance: message });
        if (!message.hasOwnProperty("nSignTimes"))
            throw $util.ProtocolError("missing required 'nSignTimes'", { instance: message });
        if (!message.hasOwnProperty("nGuideMarkRes"))
            throw $util.ProtocolError("missing required 'nGuideMarkRes'", { instance: message });
        if (!message.hasOwnProperty("dChipNum"))
            throw $util.ProtocolError("missing required 'dChipNum'", { instance: message });
        return message;
    };

    return CH_LoginMsg_Res;
})();

$root.CH_LoginMsg = (function() {

    function CH_LoginMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_LoginMsg.prototype.nLoginType = 0;
    CH_LoginMsg.prototype.strTreeUID = "";
    CH_LoginMsg.prototype.strSession = "";
    CH_LoginMsg.prototype.strIdentity = "";
    CH_LoginMsg.prototype.strMac = "";
    CH_LoginMsg.prototype.nBigChannelID = 0;
    CH_LoginMsg.prototype.strChannelName = "";
    CH_LoginMsg.prototype.nLoginChannelID = 0;
    CH_LoginMsg.prototype.nidLogin = 0;
    CH_LoginMsg.prototype.strAccount = "";
    CH_LoginMsg.prototype.strPassword = "";
    CH_LoginMsg.prototype.strCtxID = "";
    CH_LoginMsg.prototype.strPayID = "";
    CH_LoginMsg.prototype.strSafeCode = "";

    CH_LoginMsg.create = function create(properties) {
        return new CH_LoginMsg(properties);
    };

    CH_LoginMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nLoginType);
        writer.uint32(18).string(message.strTreeUID);
        writer.uint32(26).string(message.strSession);
        writer.uint32(34).string(message.strIdentity);
        writer.uint32(42).string(message.strMac);
        writer.uint32(48).sint32(message.nBigChannelID);
        writer.uint32(58).string(message.strChannelName);
        writer.uint32(64).sint32(message.nLoginChannelID);
        writer.uint32(72).sint32(message.nidLogin);
        writer.uint32(82).string(message.strAccount);
        writer.uint32(90).string(message.strPassword);
        if (message.strCtxID != null && Object.hasOwnProperty.call(message, "strCtxID"))
            writer.uint32(98).string(message.strCtxID);
        if (message.strPayID != null && Object.hasOwnProperty.call(message, "strPayID"))
            writer.uint32(106).string(message.strPayID);
        if (message.strSafeCode != null && Object.hasOwnProperty.call(message, "strSafeCode"))
            writer.uint32(114).string(message.strSafeCode);
        return writer;
    };

    CH_LoginMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_LoginMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nLoginType = reader.sint32();
                break;
            case 2:
                message.strTreeUID = reader.string();
                break;
            case 3:
                message.strSession = reader.string();
                break;
            case 4:
                message.strIdentity = reader.string();
                break;
            case 5:
                message.strMac = reader.string();
                break;
            case 6:
                message.nBigChannelID = reader.sint32();
                break;
            case 7:
                message.strChannelName = reader.string();
                break;
            case 8:
                message.nLoginChannelID = reader.sint32();
                break;
            case 9:
                message.nidLogin = reader.sint32();
                break;
            case 10:
                message.strAccount = reader.string();
                break;
            case 11:
                message.strPassword = reader.string();
                break;
            case 12:
                message.strCtxID = reader.string();
                break;
            case 13:
                message.strPayID = reader.string();
                break;
            case 14:
                message.strSafeCode = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nLoginType"))
            throw $util.ProtocolError("missing required 'nLoginType'", { instance: message });
        if (!message.hasOwnProperty("strTreeUID"))
            throw $util.ProtocolError("missing required 'strTreeUID'", { instance: message });
        if (!message.hasOwnProperty("strSession"))
            throw $util.ProtocolError("missing required 'strSession'", { instance: message });
        if (!message.hasOwnProperty("strIdentity"))
            throw $util.ProtocolError("missing required 'strIdentity'", { instance: message });
        if (!message.hasOwnProperty("strMac"))
            throw $util.ProtocolError("missing required 'strMac'", { instance: message });
        if (!message.hasOwnProperty("nBigChannelID"))
            throw $util.ProtocolError("missing required 'nBigChannelID'", { instance: message });
        if (!message.hasOwnProperty("strChannelName"))
            throw $util.ProtocolError("missing required 'strChannelName'", { instance: message });
        if (!message.hasOwnProperty("nLoginChannelID"))
            throw $util.ProtocolError("missing required 'nLoginChannelID'", { instance: message });
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strAccount"))
            throw $util.ProtocolError("missing required 'strAccount'", { instance: message });
        if (!message.hasOwnProperty("strPassword"))
            throw $util.ProtocolError("missing required 'strPassword'", { instance: message });
        return message;
    };

    return CH_LoginMsg;
})();

$root.CH_ModifyNickname = (function() {

    function CH_ModifyNickname(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ModifyNickname.prototype.nidLogin = 0;
    CH_ModifyNickname.prototype.strNickname = "";
    CH_ModifyNickname.prototype.strSafeCode = "";

    CH_ModifyNickname.create = function create(properties) {
        return new CH_ModifyNickname(properties);
    };

    CH_ModifyNickname.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strNickname);
        writer.uint32(26).string(message.strSafeCode);
        return writer;
    };

    CH_ModifyNickname.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ModifyNickname();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strNickname = reader.string();
                break;
            case 3:
                message.strSafeCode = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strNickname"))
            throw $util.ProtocolError("missing required 'strNickname'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CH_ModifyNickname;
})();

$root.CH_ModifyPassword_Res = (function() {

    function CH_ModifyPassword_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ModifyPassword_Res.prototype.nidLogin = 0;
    CH_ModifyPassword_Res.prototype.nRes = 0;

    CH_ModifyPassword_Res.create = function create(properties) {
        return new CH_ModifyPassword_Res(properties);
    };

    CH_ModifyPassword_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.nRes);
        return writer;
    };

    CH_ModifyPassword_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ModifyPassword_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nRes = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        return message;
    };

    return CH_ModifyPassword_Res;
})();

$root.CH_ModifyPassword = (function() {

    function CH_ModifyPassword(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ModifyPassword.prototype.strOldPassword = "";
    CH_ModifyPassword.prototype.strNewPassword = "";
    CH_ModifyPassword.prototype.strSafeCode = "";
    CH_ModifyPassword.prototype.nidLogin = 0;

    CH_ModifyPassword.create = function create(properties) {
        return new CH_ModifyPassword(properties);
    };

    CH_ModifyPassword.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strOldPassword);
        writer.uint32(18).string(message.strNewPassword);
        writer.uint32(26).string(message.strSafeCode);
        if (message.nidLogin != null && Object.hasOwnProperty.call(message, "nidLogin"))
            writer.uint32(32).sint32(message.nidLogin);
        return writer;
    };

    CH_ModifyPassword.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ModifyPassword();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strOldPassword = reader.string();
                break;
            case 2:
                message.strNewPassword = reader.string();
                break;
            case 3:
                message.strSafeCode = reader.string();
                break;
            case 4:
                message.nidLogin = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strOldPassword"))
            throw $util.ProtocolError("missing required 'strOldPassword'", { instance: message });
        if (!message.hasOwnProperty("strNewPassword"))
            throw $util.ProtocolError("missing required 'strNewPassword'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CH_ModifyPassword;
})();

$root.CH_ModifyPlayerInfo_Res = (function() {

    function CH_ModifyPlayerInfo_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ModifyPlayerInfo_Res.prototype.nRes = 0;
    CH_ModifyPlayerInfo_Res.prototype.nidLogin = 0;

    CH_ModifyPlayerInfo_Res.create = function create(properties) {
        return new CH_ModifyPlayerInfo_Res(properties);
    };

    CH_ModifyPlayerInfo_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        writer.uint32(16).sint32(message.nidLogin);
        return writer;
    };

    CH_ModifyPlayerInfo_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ModifyPlayerInfo_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                message.nidLogin = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        return message;
    };

    return CH_ModifyPlayerInfo_Res;
})();

$root.CH_ModifyPlayerInfo = (function() {

    function CH_ModifyPlayerInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ModifyPlayerInfo.prototype.nidLogin = 0;
    CH_ModifyPlayerInfo.prototype.nSex = 0;
    CH_ModifyPlayerInfo.prototype.nHeadType = 0;
    CH_ModifyPlayerInfo.prototype.strSafeCode = "";
    CH_ModifyPlayerInfo.prototype.strPhone = "";

    CH_ModifyPlayerInfo.create = function create(properties) {
        return new CH_ModifyPlayerInfo(properties);
    };

    CH_ModifyPlayerInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.nSex);
        writer.uint32(24).sint32(message.nHeadType);
        writer.uint32(34).string(message.strSafeCode);
        writer.uint32(42).string(message.strPhone);
        return writer;
    };

    CH_ModifyPlayerInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ModifyPlayerInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nSex = reader.sint32();
                break;
            case 3:
                message.nHeadType = reader.sint32();
                break;
            case 4:
                message.strSafeCode = reader.string();
                break;
            case 5:
                message.strPhone = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nSex"))
            throw $util.ProtocolError("missing required 'nSex'", { instance: message });
        if (!message.hasOwnProperty("nHeadType"))
            throw $util.ProtocolError("missing required 'nHeadType'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("strPhone"))
            throw $util.ProtocolError("missing required 'strPhone'", { instance: message });
        return message;
    };

    return CH_ModifyPlayerInfo;
})();

$root.CH_PlayerSignature_Res = (function() {

    function CH_PlayerSignature_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_PlayerSignature_Res.prototype.nRes = 0;

    CH_PlayerSignature_Res.create = function create(properties) {
        return new CH_PlayerSignature_Res(properties);
    };

    CH_PlayerSignature_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        return writer;
    };

    CH_PlayerSignature_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_PlayerSignature_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        return message;
    };

    return CH_PlayerSignature_Res;
})();

$root.CH_PlayerSignature = (function() {

    function CH_PlayerSignature(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_PlayerSignature.prototype.strSignature = "";

    CH_PlayerSignature.create = function create(properties) {
        return new CH_PlayerSignature(properties);
    };

    CH_PlayerSignature.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strSignature);
        return writer;
    };

    CH_PlayerSignature.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_PlayerSignature();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strSignature = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strSignature"))
            throw $util.ProtocolError("missing required 'strSignature'", { instance: message });
        return message;
    };

    return CH_PlayerSignature;
})();

$root.CH_RegisterMsg_Res = (function() {

    function CH_RegisterMsg_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_RegisterMsg_Res.prototype.strAccount = "";
    CH_RegisterMsg_Res.prototype.nidLogin = 0;
    CH_RegisterMsg_Res.prototype.strCtxID = "";

    CH_RegisterMsg_Res.create = function create(properties) {
        return new CH_RegisterMsg_Res(properties);
    };

    CH_RegisterMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strAccount);
        writer.uint32(16).sint32(message.nidLogin);
        if (message.strCtxID != null && Object.hasOwnProperty.call(message, "strCtxID"))
            writer.uint32(26).string(message.strCtxID);
        return writer;
    };

    CH_RegisterMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_RegisterMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strAccount = reader.string();
                break;
            case 2:
                message.nidLogin = reader.sint32();
                break;
            case 3:
                message.strCtxID = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strAccount"))
            throw $util.ProtocolError("missing required 'strAccount'", { instance: message });
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        return message;
    };

    return CH_RegisterMsg_Res;
})();

$root.CH_RegisterMsg = (function() {

    function CH_RegisterMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_RegisterMsg.prototype.strAccount = "";
    CH_RegisterMsg.prototype.strPassword = "";
    CH_RegisterMsg.prototype.strInvCode = "";
    CH_RegisterMsg.prototype.nBigChannelID = 0;
    CH_RegisterMsg.prototype.strChannelName = "";
    CH_RegisterMsg.prototype.strPhoneMac = "";
    CH_RegisterMsg.prototype.strCtxID = "";

    CH_RegisterMsg.create = function create(properties) {
        return new CH_RegisterMsg(properties);
    };

    CH_RegisterMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strAccount);
        writer.uint32(18).string(message.strPassword);
        if (message.strInvCode != null && Object.hasOwnProperty.call(message, "strInvCode"))
            writer.uint32(26).string(message.strInvCode);
        if (message.nBigChannelID != null && Object.hasOwnProperty.call(message, "nBigChannelID"))
            writer.uint32(32).sint32(message.nBigChannelID);
        if (message.strChannelName != null && Object.hasOwnProperty.call(message, "strChannelName"))
            writer.uint32(42).string(message.strChannelName);
        if (message.strPhoneMac != null && Object.hasOwnProperty.call(message, "strPhoneMac"))
            writer.uint32(50).string(message.strPhoneMac);
        if (message.strCtxID != null && Object.hasOwnProperty.call(message, "strCtxID"))
            writer.uint32(58).string(message.strCtxID);
        return writer;
    };

    CH_RegisterMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_RegisterMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strAccount = reader.string();
                break;
            case 2:
                message.strPassword = reader.string();
                break;
            case 3:
                message.strInvCode = reader.string();
                break;
            case 4:
                message.nBigChannelID = reader.sint32();
                break;
            case 5:
                message.strChannelName = reader.string();
                break;
            case 6:
                message.strPhoneMac = reader.string();
                break;
            case 7:
                message.strCtxID = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strAccount"))
            throw $util.ProtocolError("missing required 'strAccount'", { instance: message });
        if (!message.hasOwnProperty("strPassword"))
            throw $util.ProtocolError("missing required 'strPassword'", { instance: message });
        return message;
    };

    return CH_RegisterMsg;
})();

$root.CH_ReturnLoginMsg = (function() {

    function CH_ReturnLoginMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ReturnLoginMsg.prototype.nidLogin = 0;

    CH_ReturnLoginMsg.create = function create(properties) {
        return new CH_ReturnLoginMsg(properties);
    };

    CH_ReturnLoginMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.nidLogin != null && Object.hasOwnProperty.call(message, "nidLogin"))
            writer.uint32(8).sint32(message.nidLogin);
        return writer;
    };

    CH_ReturnLoginMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ReturnLoginMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_ReturnLoginMsg;
})();

$root.CH_RewardSignInMsg_Res = (function() {

    function CH_RewardSignInMsg_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_RewardSignInMsg_Res.prototype.nLotteryTicket = "";

    CH_RewardSignInMsg_Res.create = function create(properties) {
        return new CH_RewardSignInMsg_Res(properties);
    };

    CH_RewardSignInMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.nLotteryTicket);
        return writer;
    };

    CH_RewardSignInMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_RewardSignInMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nLotteryTicket = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nLotteryTicket"))
            throw $util.ProtocolError("missing required 'nLotteryTicket'", { instance: message });
        return message;
    };

    return CH_RewardSignInMsg_Res;
})();

$root.CH_RewardSignInMsg = (function() {

    function CH_RewardSignInMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_RewardSignInMsg.create = function create(properties) {
        return new CH_RewardSignInMsg(properties);
    };

    CH_RewardSignInMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        return writer;
    };

    CH_RewardSignInMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_RewardSignInMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_RewardSignInMsg;
})();

$root.CS_BeginnerGuide_Res = (function() {

    function CS_BeginnerGuide_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_BeginnerGuide_Res.prototype.nIdLogin = 0;
    CS_BeginnerGuide_Res.prototype.nGuideMarkRes = 0;

    CS_BeginnerGuide_Res.create = function create(properties) {
        return new CS_BeginnerGuide_Res(properties);
    };

    CS_BeginnerGuide_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(16).sint32(message.nGuideMarkRes);
        return writer;
    };

    CS_BeginnerGuide_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_BeginnerGuide_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.nGuideMarkRes = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("nGuideMarkRes"))
            throw $util.ProtocolError("missing required 'nGuideMarkRes'", { instance: message });
        return message;
    };

    return CS_BeginnerGuide_Res;
})();

$root.CS_BeginnerGuide = (function() {

    function CS_BeginnerGuide(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_BeginnerGuide.prototype.nIdLogin = 0;
    CS_BeginnerGuide.prototype.nGuideMark = 0;

    CS_BeginnerGuide.create = function create(properties) {
        return new CS_BeginnerGuide(properties);
    };

    CS_BeginnerGuide.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(16).sint32(message.nGuideMark);
        return writer;
    };

    CS_BeginnerGuide.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_BeginnerGuide();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.nGuideMark = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("nGuideMark"))
            throw $util.ProtocolError("missing required 'nGuideMark'", { instance: message });
        return message;
    };

    return CS_BeginnerGuide;
})();

$root.HC_UpdateGrandRankMsg = (function() {

    function HC_UpdateGrandRankMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HC_UpdateGrandRankMsg.create = function create(properties) {
        return new HC_UpdateGrandRankMsg(properties);
    };

    HC_UpdateGrandRankMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        return writer;
    };

    HC_UpdateGrandRankMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HC_UpdateGrandRankMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return HC_UpdateGrandRankMsg;
})();

$root.CH_InHallCheckAndReceiveOrderMsg = (function() {

    function CH_InHallCheckAndReceiveOrderMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_InHallCheckAndReceiveOrderMsg.prototype.nIdLogin = 0;
    CH_InHallCheckAndReceiveOrderMsg.prototype.strSafeCode = "";
    CH_InHallCheckAndReceiveOrderMsg.prototype.strOtherPar = "";

    CH_InHallCheckAndReceiveOrderMsg.create = function create(properties) {
        return new CH_InHallCheckAndReceiveOrderMsg(properties);
    };

    CH_InHallCheckAndReceiveOrderMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        if (message.strOtherPar != null && Object.hasOwnProperty.call(message, "strOtherPar"))
            writer.uint32(26).string(message.strOtherPar);
        return writer;
    };

    CH_InHallCheckAndReceiveOrderMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_InHallCheckAndReceiveOrderMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.strOtherPar = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CH_InHallCheckAndReceiveOrderMsg;
})();

$root.CH_InHallReceiveOrderItemMsg_Res = (function() {

    function CH_InHallReceiveOrderItemMsg_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_InHallReceiveOrderItemMsg_Res.prototype.nPayCode = 0;
    CH_InHallReceiveOrderItemMsg_Res.prototype.itemReward = $util.emptyArray;
    CH_InHallReceiveOrderItemMsg_Res.prototype.strVIPEndTime = "";
    CH_InHallReceiveOrderItemMsg_Res.prototype.nDoubleBilling = 0;
    CH_InHallReceiveOrderItemMsg_Res.prototype.nDayFirstBilling = 0;

    CH_InHallReceiveOrderItemMsg_Res.create = function create(properties) {
        return new CH_InHallReceiveOrderItemMsg_Res(properties);
    };

    CH_InHallReceiveOrderItemMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nPayCode);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(18).fork()).ldelim();
        writer.uint32(26).string(message.strVIPEndTime);
        writer.uint32(32).sint32(message.nDoubleBilling);
        writer.uint32(40).sint32(message.nDayFirstBilling);
        return writer;
    };

    CH_InHallReceiveOrderItemMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_InHallReceiveOrderItemMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nPayCode = reader.sint32();
                break;
            case 2:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            case 3:
                message.strVIPEndTime = reader.string();
                break;
            case 4:
                message.nDoubleBilling = reader.sint32();
                break;
            case 5:
                message.nDayFirstBilling = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nPayCode"))
            throw $util.ProtocolError("missing required 'nPayCode'", { instance: message });
        if (!message.hasOwnProperty("strVIPEndTime"))
            throw $util.ProtocolError("missing required 'strVIPEndTime'", { instance: message });
        if (!message.hasOwnProperty("nDoubleBilling"))
            throw $util.ProtocolError("missing required 'nDoubleBilling'", { instance: message });
        if (!message.hasOwnProperty("nDayFirstBilling"))
            throw $util.ProtocolError("missing required 'nDayFirstBilling'", { instance: message });
        return message;
    };

    return CH_InHallReceiveOrderItemMsg_Res;
})();

$root.CH_InHallReceiveOrderItemMsg = (function() {

    function CH_InHallReceiveOrderItemMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_InHallReceiveOrderItemMsg.prototype.nIdLogin = 0;
    CH_InHallReceiveOrderItemMsg.prototype.strSafeCode = "";
    CH_InHallReceiveOrderItemMsg.prototype.strOrderID = "";
    CH_InHallReceiveOrderItemMsg.prototype.nActID = 0;
    CH_InHallReceiveOrderItemMsg.prototype.nIndex = 0;

    CH_InHallReceiveOrderItemMsg.create = function create(properties) {
        return new CH_InHallReceiveOrderItemMsg(properties);
    };

    CH_InHallReceiveOrderItemMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(26).string(message.strOrderID);
        if (message.nActID != null && Object.hasOwnProperty.call(message, "nActID"))
            writer.uint32(32).sint32(message.nActID);
        if (message.nIndex != null && Object.hasOwnProperty.call(message, "nIndex"))
            writer.uint32(40).sint32(message.nIndex);
        return writer;
    };

    CH_InHallReceiveOrderItemMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_InHallReceiveOrderItemMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.strOrderID = reader.string();
                break;
            case 4:
                message.nActID = reader.sint32();
                break;
            case 5:
                message.nIndex = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("strOrderID"))
            throw $util.ProtocolError("missing required 'strOrderID'", { instance: message });
        return message;
    };

    return CH_InHallReceiveOrderItemMsg;
})();

$root.CH_InRoomCheckAndReceiveOrderMsg = (function() {

    function CH_InRoomCheckAndReceiveOrderMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_InRoomCheckAndReceiveOrderMsg.prototype.nIdLogin = 0;
    CH_InRoomCheckAndReceiveOrderMsg.prototype.strSafeCode = "";
    CH_InRoomCheckAndReceiveOrderMsg.prototype.strOtherPar = "";

    CH_InRoomCheckAndReceiveOrderMsg.create = function create(properties) {
        return new CH_InRoomCheckAndReceiveOrderMsg(properties);
    };

    CH_InRoomCheckAndReceiveOrderMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        if (message.strOtherPar != null && Object.hasOwnProperty.call(message, "strOtherPar"))
            writer.uint32(26).string(message.strOtherPar);
        return writer;
    };

    CH_InRoomCheckAndReceiveOrderMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_InRoomCheckAndReceiveOrderMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.strOtherPar = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CH_InRoomCheckAndReceiveOrderMsg;
})();

$root.CH_ReceiveMonthCardGift_Res = (function() {

    function CH_ReceiveMonthCardGift_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ReceiveMonthCardGift_Res.prototype.nRes = 0;
    CH_ReceiveMonthCardGift_Res.prototype.itemReward = $util.emptyArray;

    CH_ReceiveMonthCardGift_Res.create = function create(properties) {
        return new CH_ReceiveMonthCardGift_Res(properties);
    };

    CH_ReceiveMonthCardGift_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(18).fork()).ldelim();
        return writer;
    };

    CH_ReceiveMonthCardGift_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ReceiveMonthCardGift_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        return message;
    };

    return CH_ReceiveMonthCardGift_Res;
})();

$root.CH_ReceiveMonthCardGift = (function() {

    function CH_ReceiveMonthCardGift(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_ReceiveMonthCardGift.prototype.nidLogin = 0;
    CH_ReceiveMonthCardGift.prototype.strSafeCode = "";

    CH_ReceiveMonthCardGift.create = function create(properties) {
        return new CH_ReceiveMonthCardGift(properties);
    };

    CH_ReceiveMonthCardGift.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(18).string(message.strSafeCode);
        return writer;
    };

    CH_ReceiveMonthCardGift.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_ReceiveMonthCardGift();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CH_ReceiveMonthCardGift;
})();

$root.CS_AskAllNotReceiveOrderMsg_Res = (function() {

    function CS_AskAllNotReceiveOrderMsg_Res(properties) {
        this.orderInfor = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_AskAllNotReceiveOrderMsg_Res.prototype.orderInfor = $util.emptyArray;

    CS_AskAllNotReceiveOrderMsg_Res.create = function create(properties) {
        return new CS_AskAllNotReceiveOrderMsg_Res(properties);
    };

    CS_AskAllNotReceiveOrderMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.orderInfor != null && message.orderInfor.length)
            for (var i = 0; i < message.orderInfor.length; ++i)
                $root.OrderInfor.encode(message.orderInfor[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    CS_AskAllNotReceiveOrderMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_AskAllNotReceiveOrderMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.orderInfor && message.orderInfor.length))
                    message.orderInfor = [];
                message.orderInfor.push($root.OrderInfor.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CS_AskAllNotReceiveOrderMsg_Res;
})();

$root.OrderInfor = (function() {

    function OrderInfor(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    OrderInfor.prototype.strOrderId = "";
    OrderInfor.prototype.nPayCode = 0;
    OrderInfor.prototype.strAskTime = "";
    OrderInfor.prototype.nRealAmount = 0;

    OrderInfor.create = function create(properties) {
        return new OrderInfor(properties);
    };

    OrderInfor.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strOrderId);
        writer.uint32(16).sint32(message.nPayCode);
        writer.uint32(26).string(message.strAskTime);
        writer.uint32(32).sint32(message.nRealAmount);
        return writer;
    };

    OrderInfor.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.OrderInfor();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strOrderId = reader.string();
                break;
            case 2:
                message.nPayCode = reader.sint32();
                break;
            case 3:
                message.strAskTime = reader.string();
                break;
            case 4:
                message.nRealAmount = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strOrderId"))
            throw $util.ProtocolError("missing required 'strOrderId'", { instance: message });
        if (!message.hasOwnProperty("nPayCode"))
            throw $util.ProtocolError("missing required 'nPayCode'", { instance: message });
        if (!message.hasOwnProperty("strAskTime"))
            throw $util.ProtocolError("missing required 'strAskTime'", { instance: message });
        if (!message.hasOwnProperty("nRealAmount"))
            throw $util.ProtocolError("missing required 'nRealAmount'", { instance: message });
        return message;
    };

    return OrderInfor;
})();

$root.CS_AskAllNotReceiveOrderMsg = (function() {

    function CS_AskAllNotReceiveOrderMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_AskAllNotReceiveOrderMsg.prototype.nIdLogin = 0;
    CS_AskAllNotReceiveOrderMsg.prototype.strSafeCode = "";

    CS_AskAllNotReceiveOrderMsg.create = function create(properties) {
        return new CS_AskAllNotReceiveOrderMsg(properties);
    };

    CS_AskAllNotReceiveOrderMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        return writer;
    };

    CS_AskAllNotReceiveOrderMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_AskAllNotReceiveOrderMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        return message;
    };

    return CS_AskAllNotReceiveOrderMsg;
})();

$root.CS_AskPaymentMsg_Res = (function() {

    function CS_AskPaymentMsg_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_AskPaymentMsg_Res.prototype.nGoodsId = 0;
    CS_AskPaymentMsg_Res.prototype.strOrderID = "";
    CS_AskPaymentMsg_Res.prototype.strSignPar = "";
    CS_AskPaymentMsg_Res.prototype.strOtherPar = "";

    CS_AskPaymentMsg_Res.create = function create(properties) {
        return new CS_AskPaymentMsg_Res(properties);
    };

    CS_AskPaymentMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nGoodsId);
        if (message.strOrderID != null && Object.hasOwnProperty.call(message, "strOrderID"))
            writer.uint32(18).string(message.strOrderID);
        if (message.strSignPar != null && Object.hasOwnProperty.call(message, "strSignPar"))
            writer.uint32(26).string(message.strSignPar);
        if (message.strOtherPar != null && Object.hasOwnProperty.call(message, "strOtherPar"))
            writer.uint32(34).string(message.strOtherPar);
        return writer;
    };

    CS_AskPaymentMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_AskPaymentMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nGoodsId = reader.sint32();
                break;
            case 2:
                message.strOrderID = reader.string();
                break;
            case 3:
                message.strSignPar = reader.string();
                break;
            case 4:
                message.strOtherPar = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nGoodsId"))
            throw $util.ProtocolError("missing required 'nGoodsId'", { instance: message });
        return message;
    };

    return CS_AskPaymentMsg_Res;
})();

$root.CS_AskPaymentMsg = (function() {

    function CS_AskPaymentMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_AskPaymentMsg.prototype.nIdLogin = 0;
    CS_AskPaymentMsg.prototype.strSafeCode = "";
    CS_AskPaymentMsg.prototype.nGoodsId = 0;
    CS_AskPaymentMsg.prototype.strOtherPar = "";

    CS_AskPaymentMsg.create = function create(properties) {
        return new CS_AskPaymentMsg(properties);
    };

    CS_AskPaymentMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(24).sint32(message.nGoodsId);
        if (message.strOtherPar != null && Object.hasOwnProperty.call(message, "strOtherPar"))
            writer.uint32(34).string(message.strOtherPar);
        return writer;
    };

    CS_AskPaymentMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_AskPaymentMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.nGoodsId = reader.sint32();
                break;
            case 4:
                message.strOtherPar = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("nGoodsId"))
            throw $util.ProtocolError("missing required 'nGoodsId'", { instance: message });
        return message;
    };

    return CS_AskPaymentMsg;
})();

$root.CS_InRoomReceiveOrderItemMsg_Res = (function() {

    function CS_InRoomReceiveOrderItemMsg_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_InRoomReceiveOrderItemMsg_Res.prototype.nPayCode = 0;
    CS_InRoomReceiveOrderItemMsg_Res.prototype.itemReward = $util.emptyArray;
    CS_InRoomReceiveOrderItemMsg_Res.prototype.strVIPEndTime = "";
    CS_InRoomReceiveOrderItemMsg_Res.prototype.nDoubleBilling = 0;
    CS_InRoomReceiveOrderItemMsg_Res.prototype.nDayFirstBilling = 0;

    CS_InRoomReceiveOrderItemMsg_Res.create = function create(properties) {
        return new CS_InRoomReceiveOrderItemMsg_Res(properties);
    };

    CS_InRoomReceiveOrderItemMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nPayCode);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(18).fork()).ldelim();
        writer.uint32(26).string(message.strVIPEndTime);
        writer.uint32(32).sint32(message.nDoubleBilling);
        writer.uint32(40).sint32(message.nDayFirstBilling);
        return writer;
    };

    CS_InRoomReceiveOrderItemMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_InRoomReceiveOrderItemMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nPayCode = reader.sint32();
                break;
            case 2:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            case 3:
                message.strVIPEndTime = reader.string();
                break;
            case 4:
                message.nDoubleBilling = reader.sint32();
                break;
            case 5:
                message.nDayFirstBilling = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nPayCode"))
            throw $util.ProtocolError("missing required 'nPayCode'", { instance: message });
        if (!message.hasOwnProperty("strVIPEndTime"))
            throw $util.ProtocolError("missing required 'strVIPEndTime'", { instance: message });
        if (!message.hasOwnProperty("nDoubleBilling"))
            throw $util.ProtocolError("missing required 'nDoubleBilling'", { instance: message });
        if (!message.hasOwnProperty("nDayFirstBilling"))
            throw $util.ProtocolError("missing required 'nDayFirstBilling'", { instance: message });
        return message;
    };

    return CS_InRoomReceiveOrderItemMsg_Res;
})();

$root.CS_InRoomReceiveOrderItemMsg = (function() {

    function CS_InRoomReceiveOrderItemMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_InRoomReceiveOrderItemMsg.prototype.nIdLogin = 0;
    CS_InRoomReceiveOrderItemMsg.prototype.strSafeCode = "";
    CS_InRoomReceiveOrderItemMsg.prototype.strOrderID = "";

    CS_InRoomReceiveOrderItemMsg.create = function create(properties) {
        return new CS_InRoomReceiveOrderItemMsg(properties);
    };

    CS_InRoomReceiveOrderItemMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(26).string(message.strOrderID);
        return writer;
    };

    CS_InRoomReceiveOrderItemMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_InRoomReceiveOrderItemMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.strOrderID = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("strOrderID"))
            throw $util.ProtocolError("missing required 'strOrderID'", { instance: message });
        return message;
    };

    return CS_InRoomReceiveOrderItemMsg;
})();

$root.CS_ReceiveOrderItemMsg_Res = (function() {

    function CS_ReceiveOrderItemMsg_Res(properties) {
        this.itemReward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ReceiveOrderItemMsg_Res.prototype.nRes = 0;
    CS_ReceiveOrderItemMsg_Res.prototype.itemReward = $util.emptyArray;
    CS_ReceiveOrderItemMsg_Res.prototype.strVIPEndTime = "";
    CS_ReceiveOrderItemMsg_Res.prototype.nDoubleBilling = 0;

    CS_ReceiveOrderItemMsg_Res.create = function create(properties) {
        return new CS_ReceiveOrderItemMsg_Res(properties);
    };

    CS_ReceiveOrderItemMsg_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nRes);
        if (message.itemReward != null && message.itemReward.length)
            for (var i = 0; i < message.itemReward.length; ++i)
                $root.ItemReward.encode(message.itemReward[i], writer.uint32(18).fork()).ldelim();
        if (message.strVIPEndTime != null && Object.hasOwnProperty.call(message, "strVIPEndTime"))
            writer.uint32(26).string(message.strVIPEndTime);
        if (message.nDoubleBilling != null && Object.hasOwnProperty.call(message, "nDoubleBilling"))
            writer.uint32(32).sint32(message.nDoubleBilling);
        return writer;
    };

    CS_ReceiveOrderItemMsg_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ReceiveOrderItemMsg_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nRes = reader.sint32();
                break;
            case 2:
                if (!(message.itemReward && message.itemReward.length))
                    message.itemReward = [];
                message.itemReward.push($root.ItemReward.decode(reader, reader.uint32()));
                break;
            case 3:
                message.strVIPEndTime = reader.string();
                break;
            case 4:
                message.nDoubleBilling = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nRes"))
            throw $util.ProtocolError("missing required 'nRes'", { instance: message });
        return message;
    };

    return CS_ReceiveOrderItemMsg_Res;
})();

$root.CS_ReceiveOrderItemMsg = (function() {

    function CS_ReceiveOrderItemMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_ReceiveOrderItemMsg.prototype.nIdLogin = 0;
    CS_ReceiveOrderItemMsg.prototype.strSafeCode = "";
    CS_ReceiveOrderItemMsg.prototype.strOrderID = "";

    CS_ReceiveOrderItemMsg.create = function create(properties) {
        return new CS_ReceiveOrderItemMsg(properties);
    };

    CS_ReceiveOrderItemMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(18).string(message.strSafeCode);
        writer.uint32(26).string(message.strOrderID);
        return writer;
    };

    CS_ReceiveOrderItemMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_ReceiveOrderItemMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.strSafeCode = reader.string();
                break;
            case 3:
                message.strOrderID = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("strSafeCode"))
            throw $util.ProtocolError("missing required 'strSafeCode'", { instance: message });
        if (!message.hasOwnProperty("strOrderID"))
            throw $util.ProtocolError("missing required 'strOrderID'", { instance: message });
        return message;
    };

    return CS_ReceiveOrderItemMsg;
})();

$root.ActInfo = (function() {

    function ActInfo(properties) {
        this.actRuleInfoList = [];
        this.channelList = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    ActInfo.prototype.nActID = 0;
    ActInfo.prototype.nType = 0;
    ActInfo.prototype.strName = "";
    ActInfo.prototype.actRuleInfoList = $util.emptyArray;
    ActInfo.prototype.nStartTimeType = 0;
    ActInfo.prototype.strWeekStartDay = "";
    ActInfo.prototype.strDayStartEndTime = "";
    ActInfo.prototype.strStartShowTime = "";
    ActInfo.prototype.strEndShowTime = "";
    ActInfo.prototype.strStartTime = "";
    ActInfo.prototype.strEndTime = "";
    ActInfo.prototype.strContent = "";
    ActInfo.prototype.nVersion = 0;
    ActInfo.prototype.bRank = false;
    ActInfo.prototype.nSort = 0;
    ActInfo.prototype.strBtnBKUrl = "";
    ActInfo.prototype.strContentBKUrl = "";
    ActInfo.prototype.nState = 0;
    ActInfo.prototype.channelList = $util.emptyArray;

    ActInfo.create = function create(properties) {
        return new ActInfo(properties);
    };

    ActInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nActID);
        writer.uint32(16).sint32(message.nType);
        writer.uint32(26).string(message.strName);
        if (message.actRuleInfoList != null && message.actRuleInfoList.length)
            for (var i = 0; i < message.actRuleInfoList.length; ++i)
                $root.ActRuleInfo.encode(message.actRuleInfoList[i], writer.uint32(34).fork()).ldelim();
        writer.uint32(40).sint32(message.nStartTimeType);
        writer.uint32(50).string(message.strWeekStartDay);
        writer.uint32(58).string(message.strDayStartEndTime);
        writer.uint32(66).string(message.strStartShowTime);
        writer.uint32(74).string(message.strEndShowTime);
        writer.uint32(82).string(message.strStartTime);
        writer.uint32(90).string(message.strEndTime);
        writer.uint32(98).string(message.strContent);
        writer.uint32(104).sint32(message.nVersion);
        writer.uint32(112).bool(message.bRank);
        writer.uint32(120).sint32(message.nSort);
        writer.uint32(130).string(message.strBtnBKUrl);
        writer.uint32(138).string(message.strContentBKUrl);
        writer.uint32(144).sint32(message.nState);
        if (message.channelList != null && message.channelList.length)
            for (var i = 0; i < message.channelList.length; ++i)
                writer.uint32(154).string(message.channelList[i]);
        return writer;
    };

    ActInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ActInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nActID = reader.sint32();
                break;
            case 2:
                message.nType = reader.sint32();
                break;
            case 3:
                message.strName = reader.string();
                break;
            case 4:
                if (!(message.actRuleInfoList && message.actRuleInfoList.length))
                    message.actRuleInfoList = [];
                message.actRuleInfoList.push($root.ActRuleInfo.decode(reader, reader.uint32()));
                break;
            case 5:
                message.nStartTimeType = reader.sint32();
                break;
            case 6:
                message.strWeekStartDay = reader.string();
                break;
            case 7:
                message.strDayStartEndTime = reader.string();
                break;
            case 8:
                message.strStartShowTime = reader.string();
                break;
            case 9:
                message.strEndShowTime = reader.string();
                break;
            case 10:
                message.strStartTime = reader.string();
                break;
            case 11:
                message.strEndTime = reader.string();
                break;
            case 12:
                message.strContent = reader.string();
                break;
            case 13:
                message.nVersion = reader.sint32();
                break;
            case 14:
                message.bRank = reader.bool();
                break;
            case 15:
                message.nSort = reader.sint32();
                break;
            case 16:
                message.strBtnBKUrl = reader.string();
                break;
            case 17:
                message.strContentBKUrl = reader.string();
                break;
            case 18:
                message.nState = reader.sint32();
                break;
            case 19:
                if (!(message.channelList && message.channelList.length))
                    message.channelList = [];
                message.channelList.push(reader.string());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nActID"))
            throw $util.ProtocolError("missing required 'nActID'", { instance: message });
        if (!message.hasOwnProperty("nType"))
            throw $util.ProtocolError("missing required 'nType'", { instance: message });
        if (!message.hasOwnProperty("strName"))
            throw $util.ProtocolError("missing required 'strName'", { instance: message });
        if (!message.hasOwnProperty("nStartTimeType"))
            throw $util.ProtocolError("missing required 'nStartTimeType'", { instance: message });
        if (!message.hasOwnProperty("strWeekStartDay"))
            throw $util.ProtocolError("missing required 'strWeekStartDay'", { instance: message });
        if (!message.hasOwnProperty("strDayStartEndTime"))
            throw $util.ProtocolError("missing required 'strDayStartEndTime'", { instance: message });
        if (!message.hasOwnProperty("strStartShowTime"))
            throw $util.ProtocolError("missing required 'strStartShowTime'", { instance: message });
        if (!message.hasOwnProperty("strEndShowTime"))
            throw $util.ProtocolError("missing required 'strEndShowTime'", { instance: message });
        if (!message.hasOwnProperty("strStartTime"))
            throw $util.ProtocolError("missing required 'strStartTime'", { instance: message });
        if (!message.hasOwnProperty("strEndTime"))
            throw $util.ProtocolError("missing required 'strEndTime'", { instance: message });
        if (!message.hasOwnProperty("strContent"))
            throw $util.ProtocolError("missing required 'strContent'", { instance: message });
        if (!message.hasOwnProperty("nVersion"))
            throw $util.ProtocolError("missing required 'nVersion'", { instance: message });
        if (!message.hasOwnProperty("bRank"))
            throw $util.ProtocolError("missing required 'bRank'", { instance: message });
        if (!message.hasOwnProperty("nSort"))
            throw $util.ProtocolError("missing required 'nSort'", { instance: message });
        if (!message.hasOwnProperty("strBtnBKUrl"))
            throw $util.ProtocolError("missing required 'strBtnBKUrl'", { instance: message });
        if (!message.hasOwnProperty("strContentBKUrl"))
            throw $util.ProtocolError("missing required 'strContentBKUrl'", { instance: message });
        if (!message.hasOwnProperty("nState"))
            throw $util.ProtocolError("missing required 'nState'", { instance: message });
        return message;
    };

    return ActInfo;
})();

$root.ActRuleInfo = (function() {

    function ActRuleInfo(properties) {
        this.nValue = [];
        this.strAward = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    ActRuleInfo.prototype.idActivityRule = 0;
    ActRuleInfo.prototype.idActivity = 0;
    ActRuleInfo.prototype.nLevel = 0;
    ActRuleInfo.prototype.nIndex = 0;
    ActRuleInfo.prototype.nConditionType = 0;
    ActRuleInfo.prototype.nValue = $util.emptyArray;
    ActRuleInfo.prototype.strAward = $util.emptyArray;
    ActRuleInfo.prototype.nSurplusNum = 0;
    ActRuleInfo.prototype.nUpdateType = 0;
    ActRuleInfo.prototype.nUpdateNum = 0;
    ActRuleInfo.prototype.updateTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    ActRuleInfo.prototype.strOtherParam = "";

    ActRuleInfo.create = function create(properties) {
        return new ActRuleInfo(properties);
    };

    ActRuleInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.idActivityRule);
        writer.uint32(16).sint32(message.idActivity);
        writer.uint32(24).sint32(message.nLevel);
        writer.uint32(32).sint32(message.nIndex);
        writer.uint32(40).sint32(message.nConditionType);
        if (message.nValue != null && message.nValue.length)
            for (var i = 0; i < message.nValue.length; ++i)
                writer.uint32(48).sint32(message.nValue[i]);
        if (message.strAward != null && message.strAward.length)
            for (var i = 0; i < message.strAward.length; ++i)
                writer.uint32(58).string(message.strAward[i]);
        writer.uint32(64).sint32(message.nSurplusNum);
        writer.uint32(72).sint32(message.nUpdateType);
        writer.uint32(80).sint32(message.nUpdateNum);
        writer.uint32(88).sint64(message.updateTime);
        writer.uint32(98).string(message.strOtherParam);
        return writer;
    };

    ActRuleInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ActRuleInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.idActivityRule = reader.sint32();
                break;
            case 2:
                message.idActivity = reader.sint32();
                break;
            case 3:
                message.nLevel = reader.sint32();
                break;
            case 4:
                message.nIndex = reader.sint32();
                break;
            case 5:
                message.nConditionType = reader.sint32();
                break;
            case 6:
                if (!(message.nValue && message.nValue.length))
                    message.nValue = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.nValue.push(reader.sint32());
                } else
                    message.nValue.push(reader.sint32());
                break;
            case 7:
                if (!(message.strAward && message.strAward.length))
                    message.strAward = [];
                message.strAward.push(reader.string());
                break;
            case 8:
                message.nSurplusNum = reader.sint32();
                break;
            case 9:
                message.nUpdateType = reader.sint32();
                break;
            case 10:
                message.nUpdateNum = reader.sint32();
                break;
            case 11:
                message.updateTime = reader.sint64();
                break;
            case 12:
                message.strOtherParam = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("idActivityRule"))
            throw $util.ProtocolError("missing required 'idActivityRule'", { instance: message });
        if (!message.hasOwnProperty("idActivity"))
            throw $util.ProtocolError("missing required 'idActivity'", { instance: message });
        if (!message.hasOwnProperty("nLevel"))
            throw $util.ProtocolError("missing required 'nLevel'", { instance: message });
        if (!message.hasOwnProperty("nIndex"))
            throw $util.ProtocolError("missing required 'nIndex'", { instance: message });
        if (!message.hasOwnProperty("nConditionType"))
            throw $util.ProtocolError("missing required 'nConditionType'", { instance: message });
        if (!message.hasOwnProperty("nSurplusNum"))
            throw $util.ProtocolError("missing required 'nSurplusNum'", { instance: message });
        if (!message.hasOwnProperty("nUpdateType"))
            throw $util.ProtocolError("missing required 'nUpdateType'", { instance: message });
        if (!message.hasOwnProperty("nUpdateNum"))
            throw $util.ProtocolError("missing required 'nUpdateNum'", { instance: message });
        if (!message.hasOwnProperty("updateTime"))
            throw $util.ProtocolError("missing required 'updateTime'", { instance: message });
        if (!message.hasOwnProperty("strOtherParam"))
            throw $util.ProtocolError("missing required 'strOtherParam'", { instance: message });
        return message;
    };

    return ActRuleInfo;
})();

$root.ActRecord = (function() {

    function ActRecord(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    ActRecord.prototype.nActID = 0;
    ActRecord.prototype.nScore = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    ActRecord.prototype.strOneTimeData = "";
    ActRecord.prototype.nRecordTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    ActRecord.create = function create(properties) {
        return new ActRecord(properties);
    };

    ActRecord.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nActID);
        writer.uint32(16).sint64(message.nScore);
        writer.uint32(26).string(message.strOneTimeData);
        writer.uint32(32).sint64(message.nRecordTime);
        return writer;
    };

    ActRecord.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ActRecord();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nActID = reader.sint32();
                break;
            case 2:
                message.nScore = reader.sint64();
                break;
            case 3:
                message.strOneTimeData = reader.string();
                break;
            case 4:
                message.nRecordTime = reader.sint64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nActID"))
            throw $util.ProtocolError("missing required 'nActID'", { instance: message });
        if (!message.hasOwnProperty("nScore"))
            throw $util.ProtocolError("missing required 'nScore'", { instance: message });
        if (!message.hasOwnProperty("strOneTimeData"))
            throw $util.ProtocolError("missing required 'strOneTimeData'", { instance: message });
        if (!message.hasOwnProperty("nRecordTime"))
            throw $util.ProtocolError("missing required 'nRecordTime'", { instance: message });
        return message;
    };

    return ActRecord;
})();

$root.CH_AskActivityAwardLog_Res = (function() {

    function CH_AskActivityAwardLog_Res(properties) {
        this.actAwardList = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskActivityAwardLog_Res.prototype.actAwardList = $util.emptyArray;

    CH_AskActivityAwardLog_Res.create = function create(properties) {
        return new CH_AskActivityAwardLog_Res(properties);
    };

    CH_AskActivityAwardLog_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.actAwardList != null && message.actAwardList.length)
            for (var i = 0; i < message.actAwardList.length; ++i)
                $root.ActAwarList.encode(message.actAwardList[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    CH_AskActivityAwardLog_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskActivityAwardLog_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.actAwardList && message.actAwardList.length))
                    message.actAwardList = [];
                message.actAwardList.push($root.ActAwarList.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_AskActivityAwardLog_Res;
})();

$root.ActAwarList = (function() {

    function ActAwarList(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    ActAwarList.prototype.name = "";
    ActAwarList.prototype.recivedGold = 0;
    ActAwarList.prototype.time = "";
    ActAwarList.prototype.mygold = 0;

    ActAwarList.create = function create(properties) {
        return new ActAwarList(properties);
    };

    ActAwarList.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.name);
        writer.uint32(16).sint32(message.recivedGold);
        writer.uint32(26).string(message.time);
        writer.uint32(32).sint32(message.mygold);
        return writer;
    };

    ActAwarList.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ActAwarList();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.name = reader.string();
                break;
            case 2:
                message.recivedGold = reader.sint32();
                break;
            case 3:
                message.time = reader.string();
                break;
            case 4:
                message.mygold = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("name"))
            throw $util.ProtocolError("missing required 'name'", { instance: message });
        if (!message.hasOwnProperty("recivedGold"))
            throw $util.ProtocolError("missing required 'recivedGold'", { instance: message });
        if (!message.hasOwnProperty("time"))
            throw $util.ProtocolError("missing required 'time'", { instance: message });
        if (!message.hasOwnProperty("mygold"))
            throw $util.ProtocolError("missing required 'mygold'", { instance: message });
        return message;
    };

    return ActAwarList;
})();

$root.CH_AskActivityAwardLog = (function() {

    function CH_AskActivityAwardLog(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskActivityAwardLog.prototype.nidLogin = 0;
    CH_AskActivityAwardLog.prototype.actId = 0;
    CH_AskActivityAwardLog.prototype.safecode = "";

    CH_AskActivityAwardLog.create = function create(properties) {
        return new CH_AskActivityAwardLog(properties);
    };

    CH_AskActivityAwardLog.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.actId);
        writer.uint32(26).string(message.safecode);
        return writer;
    };

    CH_AskActivityAwardLog.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskActivityAwardLog();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.actId = reader.sint32();
                break;
            case 3:
                message.safecode = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("actId"))
            throw $util.ProtocolError("missing required 'actId'", { instance: message });
        if (!message.hasOwnProperty("safecode"))
            throw $util.ProtocolError("missing required 'safecode'", { instance: message });
        return message;
    };

    return CH_AskActivityAwardLog;
})();

$root.CH_AskActivity_Res = (function() {

    function CH_AskActivity_Res(properties) {
        this.actInfoList = [];
        this.actRecordList = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CH_AskActivity_Res.prototype.actInfoList = $util.emptyArray;
    CH_AskActivity_Res.prototype.actRecordList = $util.emptyArray;

    CH_AskActivity_Res.create = function create(properties) {
        return new CH_AskActivity_Res(properties);
    };

    CH_AskActivity_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.actInfoList != null && message.actInfoList.length)
            for (var i = 0; i < message.actInfoList.length; ++i)
                $root.ActInfo.encode(message.actInfoList[i], writer.uint32(10).fork()).ldelim();
        if (message.actRecordList != null && message.actRecordList.length)
            for (var i = 0; i < message.actRecordList.length; ++i)
                $root.ActRecord.encode(message.actRecordList[i], writer.uint32(18).fork()).ldelim();
        return writer;
    };

    CH_AskActivity_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CH_AskActivity_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.actInfoList && message.actInfoList.length))
                    message.actInfoList = [];
                message.actInfoList.push($root.ActInfo.decode(reader, reader.uint32()));
                break;
            case 2:
                if (!(message.actRecordList && message.actRecordList.length))
                    message.actRecordList = [];
                message.actRecordList.push($root.ActRecord.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return CH_AskActivity_Res;
})();

$root.DG_PicNoticeInfo = (function() {

    function DG_PicNoticeInfo(properties) {
        this.picNoticeInfoList = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    DG_PicNoticeInfo.prototype.picNoticeInfoList = $util.emptyArray;

    DG_PicNoticeInfo.create = function create(properties) {
        return new DG_PicNoticeInfo(properties);
    };

    DG_PicNoticeInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.picNoticeInfoList != null && message.picNoticeInfoList.length)
            for (var i = 0; i < message.picNoticeInfoList.length; ++i)
                $root.PicNoticeInfo.encode(message.picNoticeInfoList[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    DG_PicNoticeInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.DG_PicNoticeInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.picNoticeInfoList && message.picNoticeInfoList.length))
                    message.picNoticeInfoList = [];
                message.picNoticeInfoList.push($root.PicNoticeInfo.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return DG_PicNoticeInfo;
})();

$root.PicNoticeInfo = (function() {

    function PicNoticeInfo(properties) {
        this.channelList = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    PicNoticeInfo.prototype.nPicNoticeID = 0;
    PicNoticeInfo.prototype.strTitle = "";
    PicNoticeInfo.prototype.nSort = 0;
    PicNoticeInfo.prototype.nFunUrl = 0;
    PicNoticeInfo.prototype.strStartTime = "";
    PicNoticeInfo.prototype.strEndTime = "";
    PicNoticeInfo.prototype.strPicUrl = "";
    PicNoticeInfo.prototype.bAllChannel = false;
    PicNoticeInfo.prototype.nVersion = 0;
    PicNoticeInfo.prototype.nState = 0;
    PicNoticeInfo.prototype.strBtnName = "";
    PicNoticeInfo.prototype.strWebName = "";
    PicNoticeInfo.prototype.channelList = $util.emptyArray;

    PicNoticeInfo.create = function create(properties) {
        return new PicNoticeInfo(properties);
    };

    PicNoticeInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nPicNoticeID);
        writer.uint32(18).string(message.strTitle);
        writer.uint32(24).sint32(message.nSort);
        writer.uint32(32).sint32(message.nFunUrl);
        writer.uint32(42).string(message.strStartTime);
        writer.uint32(50).string(message.strEndTime);
        writer.uint32(58).string(message.strPicUrl);
        writer.uint32(64).bool(message.bAllChannel);
        writer.uint32(72).sint32(message.nVersion);
        writer.uint32(80).sint32(message.nState);
        writer.uint32(90).string(message.strBtnName);
        writer.uint32(98).string(message.strWebName);
        if (message.channelList != null && message.channelList.length)
            for (var i = 0; i < message.channelList.length; ++i)
                writer.uint32(104).sint32(message.channelList[i]);
        return writer;
    };

    PicNoticeInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PicNoticeInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nPicNoticeID = reader.sint32();
                break;
            case 2:
                message.strTitle = reader.string();
                break;
            case 3:
                message.nSort = reader.sint32();
                break;
            case 4:
                message.nFunUrl = reader.sint32();
                break;
            case 5:
                message.strStartTime = reader.string();
                break;
            case 6:
                message.strEndTime = reader.string();
                break;
            case 7:
                message.strPicUrl = reader.string();
                break;
            case 8:
                message.bAllChannel = reader.bool();
                break;
            case 9:
                message.nVersion = reader.sint32();
                break;
            case 10:
                message.nState = reader.sint32();
                break;
            case 11:
                message.strBtnName = reader.string();
                break;
            case 12:
                message.strWebName = reader.string();
                break;
            case 13:
                if (!(message.channelList && message.channelList.length))
                    message.channelList = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.channelList.push(reader.sint32());
                } else
                    message.channelList.push(reader.sint32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nPicNoticeID"))
            throw $util.ProtocolError("missing required 'nPicNoticeID'", { instance: message });
        if (!message.hasOwnProperty("strTitle"))
            throw $util.ProtocolError("missing required 'strTitle'", { instance: message });
        if (!message.hasOwnProperty("nSort"))
            throw $util.ProtocolError("missing required 'nSort'", { instance: message });
        if (!message.hasOwnProperty("nFunUrl"))
            throw $util.ProtocolError("missing required 'nFunUrl'", { instance: message });
        if (!message.hasOwnProperty("strStartTime"))
            throw $util.ProtocolError("missing required 'strStartTime'", { instance: message });
        if (!message.hasOwnProperty("strEndTime"))
            throw $util.ProtocolError("missing required 'strEndTime'", { instance: message });
        if (!message.hasOwnProperty("strPicUrl"))
            throw $util.ProtocolError("missing required 'strPicUrl'", { instance: message });
        if (!message.hasOwnProperty("bAllChannel"))
            throw $util.ProtocolError("missing required 'bAllChannel'", { instance: message });
        if (!message.hasOwnProperty("nVersion"))
            throw $util.ProtocolError("missing required 'nVersion'", { instance: message });
        if (!message.hasOwnProperty("nState"))
            throw $util.ProtocolError("missing required 'nState'", { instance: message });
        if (!message.hasOwnProperty("strBtnName"))
            throw $util.ProtocolError("missing required 'strBtnName'", { instance: message });
        if (!message.hasOwnProperty("strWebName"))
            throw $util.ProtocolError("missing required 'strWebName'", { instance: message });
        return message;
    };

    return PicNoticeInfo;
})();

$root.DG_PlayerRankInfo = (function() {

    function DG_PlayerRankInfo(properties) {
        this.treasureMapRankInfo = [];
        this.zbsDayRank = [];
        this.zhsWeekRank = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    DG_PlayerRankInfo.prototype.strUpdateTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    DG_PlayerRankInfo.prototype.treasureMapRankInfo = $util.emptyArray;
    DG_PlayerRankInfo.prototype.zbsDayRank = $util.emptyArray;
    DG_PlayerRankInfo.prototype.zhsWeekRank = $util.emptyArray;

    DG_PlayerRankInfo.create = function create(properties) {
        return new DG_PlayerRankInfo(properties);
    };

    DG_PlayerRankInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint64(message.strUpdateTime);
        if (message.treasureMapRankInfo != null && message.treasureMapRankInfo.length)
            for (var i = 0; i < message.treasureMapRankInfo.length; ++i)
                $root.PlayerRankInfo.encode(message.treasureMapRankInfo[i], writer.uint32(18).fork()).ldelim();
        if (message.zbsDayRank != null && message.zbsDayRank.length)
            for (var i = 0; i < message.zbsDayRank.length; ++i)
                $root.PlayerRankInfo.encode(message.zbsDayRank[i], writer.uint32(26).fork()).ldelim();
        if (message.zhsWeekRank != null && message.zhsWeekRank.length)
            for (var i = 0; i < message.zhsWeekRank.length; ++i)
                $root.PlayerRankInfo.encode(message.zhsWeekRank[i], writer.uint32(34).fork()).ldelim();
        return writer;
    };

    DG_PlayerRankInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.DG_PlayerRankInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strUpdateTime = reader.sint64();
                break;
            case 2:
                if (!(message.treasureMapRankInfo && message.treasureMapRankInfo.length))
                    message.treasureMapRankInfo = [];
                message.treasureMapRankInfo.push($root.PlayerRankInfo.decode(reader, reader.uint32()));
                break;
            case 3:
                if (!(message.zbsDayRank && message.zbsDayRank.length))
                    message.zbsDayRank = [];
                message.zbsDayRank.push($root.PlayerRankInfo.decode(reader, reader.uint32()));
                break;
            case 4:
                if (!(message.zhsWeekRank && message.zhsWeekRank.length))
                    message.zhsWeekRank = [];
                message.zhsWeekRank.push($root.PlayerRankInfo.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strUpdateTime"))
            throw $util.ProtocolError("missing required 'strUpdateTime'", { instance: message });
        return message;
    };

    return DG_PlayerRankInfo;
})();

$root.DG_ScrollInfo = (function() {

    function DG_ScrollInfo(properties) {
        this.scrollInfoList = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    DG_ScrollInfo.prototype.scrollInfoList = $util.emptyArray;

    DG_ScrollInfo.create = function create(properties) {
        return new DG_ScrollInfo(properties);
    };

    DG_ScrollInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.scrollInfoList != null && message.scrollInfoList.length)
            for (var i = 0; i < message.scrollInfoList.length; ++i)
                $root.ScrollInfo.encode(message.scrollInfoList[i], writer.uint32(10).fork()).ldelim();
        return writer;
    };

    DG_ScrollInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.DG_ScrollInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                if (!(message.scrollInfoList && message.scrollInfoList.length))
                    message.scrollInfoList = [];
                message.scrollInfoList.push($root.ScrollInfo.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    return DG_ScrollInfo;
})();

$root.ScrollInfo = (function() {

    function ScrollInfo(properties) {
        this.channelList = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    ScrollInfo.prototype.nScrollID = 0;
    ScrollInfo.prototype.strTitle = "";
    ScrollInfo.prototype.strContent = "";
    ScrollInfo.prototype.strStartTime = "";
    ScrollInfo.prototype.strEndTime = "";
    ScrollInfo.prototype.nIntercycle = 0;
    ScrollInfo.prototype.bAllChannel = false;
    ScrollInfo.prototype.nVersion = 0;
    ScrollInfo.prototype.channelList = $util.emptyArray;
    ScrollInfo.prototype.nState = 0;

    ScrollInfo.create = function create(properties) {
        return new ScrollInfo(properties);
    };

    ScrollInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nScrollID);
        writer.uint32(18).string(message.strTitle);
        writer.uint32(26).string(message.strContent);
        writer.uint32(34).string(message.strStartTime);
        writer.uint32(42).string(message.strEndTime);
        writer.uint32(48).sint32(message.nIntercycle);
        writer.uint32(56).bool(message.bAllChannel);
        writer.uint32(64).sint32(message.nVersion);
        if (message.channelList != null && message.channelList.length)
            for (var i = 0; i < message.channelList.length; ++i)
                writer.uint32(72).sint32(message.channelList[i]);
        writer.uint32(80).sint32(message.nState);
        return writer;
    };

    ScrollInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ScrollInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nScrollID = reader.sint32();
                break;
            case 2:
                message.strTitle = reader.string();
                break;
            case 3:
                message.strContent = reader.string();
                break;
            case 4:
                message.strStartTime = reader.string();
                break;
            case 5:
                message.strEndTime = reader.string();
                break;
            case 6:
                message.nIntercycle = reader.sint32();
                break;
            case 7:
                message.bAllChannel = reader.bool();
                break;
            case 8:
                message.nVersion = reader.sint32();
                break;
            case 9:
                if (!(message.channelList && message.channelList.length))
                    message.channelList = [];
                if ((tag & 7) === 2) {
                    var end2 = reader.uint32() + reader.pos;
                    while (reader.pos < end2)
                        message.channelList.push(reader.sint32());
                } else
                    message.channelList.push(reader.sint32());
                break;
            case 10:
                message.nState = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nScrollID"))
            throw $util.ProtocolError("missing required 'nScrollID'", { instance: message });
        if (!message.hasOwnProperty("strTitle"))
            throw $util.ProtocolError("missing required 'strTitle'", { instance: message });
        if (!message.hasOwnProperty("strContent"))
            throw $util.ProtocolError("missing required 'strContent'", { instance: message });
        if (!message.hasOwnProperty("strStartTime"))
            throw $util.ProtocolError("missing required 'strStartTime'", { instance: message });
        if (!message.hasOwnProperty("strEndTime"))
            throw $util.ProtocolError("missing required 'strEndTime'", { instance: message });
        if (!message.hasOwnProperty("nIntercycle"))
            throw $util.ProtocolError("missing required 'nIntercycle'", { instance: message });
        if (!message.hasOwnProperty("bAllChannel"))
            throw $util.ProtocolError("missing required 'bAllChannel'", { instance: message });
        if (!message.hasOwnProperty("nVersion"))
            throw $util.ProtocolError("missing required 'nVersion'", { instance: message });
        if (!message.hasOwnProperty("nState"))
            throw $util.ProtocolError("missing required 'nState'", { instance: message });
        return message;
    };

    return ScrollInfo;
})();

$root.GD_ModifyActScore_Res = (function() {

    function GD_ModifyActScore_Res(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    GD_ModifyActScore_Res.prototype.nidLogin = 0;
    GD_ModifyActScore_Res.prototype.nActID = 0;
    GD_ModifyActScore_Res.prototype.nScore = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    GD_ModifyActScore_Res.prototype.nRes = 0;

    GD_ModifyActScore_Res.create = function create(properties) {
        return new GD_ModifyActScore_Res(properties);
    };

    GD_ModifyActScore_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.nActID);
        writer.uint32(24).sint64(message.nScore);
        if (message.nRes != null && Object.hasOwnProperty.call(message, "nRes"))
            writer.uint32(32).sint32(message.nRes);
        return writer;
    };

    GD_ModifyActScore_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.GD_ModifyActScore_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nActID = reader.sint32();
                break;
            case 3:
                message.nScore = reader.sint64();
                break;
            case 4:
                message.nRes = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nActID"))
            throw $util.ProtocolError("missing required 'nActID'", { instance: message });
        if (!message.hasOwnProperty("nScore"))
            throw $util.ProtocolError("missing required 'nScore'", { instance: message });
        return message;
    };

    return GD_ModifyActScore_Res;
})();

$root.GD_ModifyActScore = (function() {

    function GD_ModifyActScore(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    GD_ModifyActScore.prototype.nidLogin = 0;
    GD_ModifyActScore.prototype.nChannelID = 0;
    GD_ModifyActScore.prototype.nActID = 0;
    GD_ModifyActScore.prototype.nRecordType = 0;
    GD_ModifyActScore.prototype.nAddScore = 0;
    GD_ModifyActScore.prototype.nNowScore = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

    GD_ModifyActScore.create = function create(properties) {
        return new GD_ModifyActScore(properties);
    };

    GD_ModifyActScore.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.nChannelID);
        writer.uint32(24).sint32(message.nActID);
        writer.uint32(32).sint32(message.nRecordType);
        writer.uint32(40).sint32(message.nAddScore);
        if (message.nNowScore != null && Object.hasOwnProperty.call(message, "nNowScore"))
            writer.uint32(48).sint64(message.nNowScore);
        return writer;
    };

    GD_ModifyActScore.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.GD_ModifyActScore();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nChannelID = reader.sint32();
                break;
            case 3:
                message.nActID = reader.sint32();
                break;
            case 4:
                message.nRecordType = reader.sint32();
                break;
            case 5:
                message.nAddScore = reader.sint32();
                break;
            case 6:
                message.nNowScore = reader.sint64();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nChannelID"))
            throw $util.ProtocolError("missing required 'nChannelID'", { instance: message });
        if (!message.hasOwnProperty("nActID"))
            throw $util.ProtocolError("missing required 'nActID'", { instance: message });
        if (!message.hasOwnProperty("nRecordType"))
            throw $util.ProtocolError("missing required 'nRecordType'", { instance: message });
        if (!message.hasOwnProperty("nAddScore"))
            throw $util.ProtocolError("missing required 'nAddScore'", { instance: message });
        return message;
    };

    return GD_ModifyActScore;
})();

$root.GD_UpdateActScore = (function() {

    function GD_UpdateActScore(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    GD_UpdateActScore.prototype.nidLogin = 0;
    GD_UpdateActScore.prototype.nActID = 0;
    GD_UpdateActScore.prototype.nScore = 0;

    GD_UpdateActScore.create = function create(properties) {
        return new GD_UpdateActScore(properties);
    };

    GD_UpdateActScore.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        writer.uint32(16).sint32(message.nActID);
        writer.uint32(24).sint32(message.nScore);
        return writer;
    };

    GD_UpdateActScore.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.GD_UpdateActScore();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                message.nActID = reader.sint32();
                break;
            case 3:
                message.nScore = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        if (!message.hasOwnProperty("nActID"))
            throw $util.ProtocolError("missing required 'nActID'", { instance: message });
        if (!message.hasOwnProperty("nScore"))
            throw $util.ProtocolError("missing required 'nScore'", { instance: message });
        return message;
    };

    return GD_UpdateActScore;
})();

$root.HM_AskShangXiaFenRecord_Res = (function() {

    function HM_AskShangXiaFenRecord_Res(properties) {
        this.sxFenRecordList = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HM_AskShangXiaFenRecord_Res.prototype.nidLogin = 0;
    HM_AskShangXiaFenRecord_Res.prototype.sxFenRecordList = $util.emptyArray;

    HM_AskShangXiaFenRecord_Res.create = function create(properties) {
        return new HM_AskShangXiaFenRecord_Res(properties);
    };

    HM_AskShangXiaFenRecord_Res.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nidLogin);
        if (message.sxFenRecordList != null && message.sxFenRecordList.length)
            for (var i = 0; i < message.sxFenRecordList.length; ++i)
                $root.ShangXiaFenRecordInfo.encode(message.sxFenRecordList[i], writer.uint32(18).fork()).ldelim();
        return writer;
    };

    HM_AskShangXiaFenRecord_Res.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HM_AskShangXiaFenRecord_Res();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nidLogin = reader.sint32();
                break;
            case 2:
                if (!(message.sxFenRecordList && message.sxFenRecordList.length))
                    message.sxFenRecordList = [];
                message.sxFenRecordList.push($root.ShangXiaFenRecordInfo.decode(reader, reader.uint32()));
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nidLogin"))
            throw $util.ProtocolError("missing required 'nidLogin'", { instance: message });
        return message;
    };

    return HM_AskShangXiaFenRecord_Res;
})();

$root.ShangXiaFenRecordInfo = (function() {

    function ShangXiaFenRecordInfo(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    ShangXiaFenRecordInfo.prototype.strOrderID = "";
    ShangXiaFenRecordInfo.prototype.dMondy = 0;
    ShangXiaFenRecordInfo.prototype.strTime = "";
    ShangXiaFenRecordInfo.prototype.nPayType = 0;

    ShangXiaFenRecordInfo.create = function create(properties) {
        return new ShangXiaFenRecordInfo(properties);
    };

    ShangXiaFenRecordInfo.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strOrderID);
        writer.uint32(17).double(message.dMondy);
        writer.uint32(26).string(message.strTime);
        writer.uint32(32).sint32(message.nPayType);
        return writer;
    };

    ShangXiaFenRecordInfo.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ShangXiaFenRecordInfo();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strOrderID = reader.string();
                break;
            case 2:
                message.dMondy = reader.double();
                break;
            case 3:
                message.strTime = reader.string();
                break;
            case 4:
                message.nPayType = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strOrderID"))
            throw $util.ProtocolError("missing required 'strOrderID'", { instance: message });
        if (!message.hasOwnProperty("dMondy"))
            throw $util.ProtocolError("missing required 'dMondy'", { instance: message });
        if (!message.hasOwnProperty("strTime"))
            throw $util.ProtocolError("missing required 'strTime'", { instance: message });
        if (!message.hasOwnProperty("nPayType"))
            throw $util.ProtocolError("missing required 'nPayType'", { instance: message });
        return message;
    };

    return ShangXiaFenRecordInfo;
})();

$root.HM_AskShangXiaFenRecord = (function() {

    function HM_AskShangXiaFenRecord(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    HM_AskShangXiaFenRecord.prototype.strStartTime = "";
    HM_AskShangXiaFenRecord.prototype.strEndTime = "";
    HM_AskShangXiaFenRecord.prototype.nidLogin = 0;

    HM_AskShangXiaFenRecord.create = function create(properties) {
        return new HM_AskShangXiaFenRecord(properties);
    };

    HM_AskShangXiaFenRecord.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(10).string(message.strStartTime);
        writer.uint32(18).string(message.strEndTime);
        if (message.nidLogin != null && Object.hasOwnProperty.call(message, "nidLogin"))
            writer.uint32(24).sint32(message.nidLogin);
        return writer;
    };

    HM_AskShangXiaFenRecord.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.HM_AskShangXiaFenRecord();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.strStartTime = reader.string();
                break;
            case 2:
                message.strEndTime = reader.string();
                break;
            case 3:
                message.nidLogin = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("strStartTime"))
            throw $util.ProtocolError("missing required 'strStartTime'", { instance: message });
        if (!message.hasOwnProperty("strEndTime"))
            throw $util.ProtocolError("missing required 'strEndTime'", { instance: message });
        return message;
    };

    return HM_AskShangXiaFenRecord;
})();

$root.CS_SkillLockMsg = (function() {

    function CS_SkillLockMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_SkillLockMsg.prototype.nIdLogin = 0;
    CS_SkillLockMsg.prototype.nSkillId = 0;
    CS_SkillLockMsg.prototype.strFishIndex = "";

    CS_SkillLockMsg.create = function create(properties) {
        return new CS_SkillLockMsg(properties);
    };

    CS_SkillLockMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(16).sint32(message.nSkillId);
        writer.uint32(26).string(message.strFishIndex);
        return writer;
    };

    CS_SkillLockMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_SkillLockMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.nSkillId = reader.sint32();
                break;
            case 3:
                message.strFishIndex = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("nSkillId"))
            throw $util.ProtocolError("missing required 'nSkillId'", { instance: message });
        if (!message.hasOwnProperty("strFishIndex"))
            throw $util.ProtocolError("missing required 'strFishIndex'", { instance: message });
        return message;
    };

    return CS_SkillLockMsg;
})();

$root.CS_SkillOpenMsg = (function() {

    function CS_SkillOpenMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_SkillOpenMsg.prototype.nIdLogin = 0;
    CS_SkillOpenMsg.prototype.nSkillId = 0;

    CS_SkillOpenMsg.create = function create(properties) {
        return new CS_SkillOpenMsg(properties);
    };

    CS_SkillOpenMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(16).sint32(message.nSkillId);
        return writer;
    };

    CS_SkillOpenMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_SkillOpenMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.nSkillId = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("nSkillId"))
            throw $util.ProtocolError("missing required 'nSkillId'", { instance: message });
        return message;
    };

    return CS_SkillOpenMsg;
})();

$root.CS_SkillUseMsg = (function() {

    function CS_SkillUseMsg(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    CS_SkillUseMsg.prototype.nIdLogin = 0;
    CS_SkillUseMsg.prototype.nSkillId = 0;

    CS_SkillUseMsg.create = function create(properties) {
        return new CS_SkillUseMsg(properties);
    };

    CS_SkillUseMsg.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(8).sint32(message.nIdLogin);
        writer.uint32(16).sint32(message.nSkillId);
        return writer;
    };

    CS_SkillUseMsg.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.CS_SkillUseMsg();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nIdLogin = reader.sint32();
                break;
            case 2:
                message.nSkillId = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nIdLogin"))
            throw $util.ProtocolError("missing required 'nIdLogin'", { instance: message });
        if (!message.hasOwnProperty("nSkillId"))
            throw $util.ProtocolError("missing required 'nSkillId'", { instance: message });
        return message;
    };

    return CS_SkillUseMsg;
})();