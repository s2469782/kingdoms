var MessageCode = MessageCode || {};
/**********************************************************
 *协议id：1
 *协议号：100------200
 *协议名称：login
 **********************************************************/
// CH_LoginMsg 客户端登录游戏http服务器
MessageCode.CH_LoginMsg = 101;
// CH_LoginMsg_Res http服务器返回结果
MessageCode.CH_LoginMsg_Res = 102;
// CH_AskGateMsg 客户端向http服务器索要可以连接的gate服务器
MessageCode.CH_AskGateMsg = 103;
// CH_AskGateMsg_Res http服务器返回结果
MessageCode.CH_AskGateMsg_Res = 104;
// CH_ModifyPlayerInfo 修改玩家基本信息
MessageCode.CH_ModifyPlayerinfoMsg = 105;
// CH_FortUpLevel 炮台升级
MessageCode.CH_FortUpLevel = 106;
// CH_FortUpLevel_Res 炮台升级结果
MessageCode.CH_FortUpLevel_Res = 107;
// CH_LoginBonus 领取登录奖励
MessageCode.CH_LoginBonusMsg = 108;
// CH_LoginBonus_Res 领取登录奖励结果
MessageCode.CH_LoginBonusMsg_Res = 109;
// CH_TaskReward 大厅中领取任务奖励
MessageCode.CH_TaskRewardMsg = 110;
// CH_TaskReward_Res 返回领取任务奖励结果
MessageCode.CH_TaskRewardMsg_Res = 111;
// CH_AllTaskInfo 所有任务信息
MessageCode.CH_AskAllTaskInfoMsg = 112;
// CH_AllTaskInfo_Res
MessageCode.CH_AskAllTaskInfoMsg_Res = 113;
// CH_TaskActiveGift 玩家领取活跃奖励
MessageCode.CH_TaskActiveGiftMsg = 114;
// CH_TaskActiveGift_Res 领奖结果
MessageCode.CH_TaskActiveGiftMsg_Res = 115;
// 玩家领取vip等级奖励
MessageCode.CH_TaskVIPGift = 116;
// 玩家领取vip等级奖励结果
MessageCode.CH_TaskVIPGift_Res = 117;
// CH_ModifyNickname 修改玩家昵称
MessageCode.CH_ModifyNickname = 118;
// CH_ModifyNickname_Res 修改玩家昵称结果
MessageCode.CH_ModifyNickname_Res = 119;
// CH_ModifyPlayerInfo_Res 修改玩家基本信息结果
MessageCode.CH_ModifyPlayerinfoMsg_Res = 120;
//跑马灯消息
MessageCode.HC_ScrollInfoMsg = 121;
//图片公告的信息
MessageCode.HC_PicNoticeInfoMsg = 122;
// CH_AskActivityInfo 客户端索要活动
MessageCode.CH_AskActivityMsg = 123;
// CH_AskActivityInfo_Res 活动结果
MessageCode.CH_AskActivityMsg_Res = 124;
// CH_ReceiveActAward 玩家领取活动奖励
MessageCode.CH_ReceiveActAwardMsg = 125;
// CH_ReceiveActAward_Res 玩家领取活动奖励结果
MessageCode.CH_ReceiveActAwardMsg_Res = 126;
// CH_InHallReceiveMonthCardGift 大厅领取每日月卡奖励
MessageCode.CH_InHallReceiveMonthCardGift = 127;
// CH_InHallReceiveMonthCardGift_Res 大厅领取每日月卡奖励结果
MessageCode.CH_InHallReceiveMonthCardGift_Res = 128;
// CH_SellItem 出售道具
MessageCode.CH_SellItemMsg = 129;
// CH_SellItem_Res 出售道具结果
MessageCode.CH_SellItemMsg_Res = 130;
// HC_NewMainTask 新的主线任务
MessageCode.HC_NewMainTask = 131;
//  HC_UpdateActRule 通知更新规则奖励剩余数量
MessageCode. HC_UpdateActRule = 132;
// GD_ModifyActScore_Res 活动成绩
MessageCode.HC_ModifyActScoreMsg = 133;
// CS_BeginnerGuide 新手引导
MessageCode.CS_BeginnerGuide = 134;
// CS_BeginnerGuide 新手引导返回
MessageCode.CS_BeginnerGuide_Res = 135;
// CH_GrandPrixRankMsg 客户端请求大奖赛排行
MessageCode.CH_GrandPrixRankMsg = 137;
// CH_GrandPrixRankMsg_Res 返回大奖赛排行结果
MessageCode.CH_GrandPrixRankMsg_Res = 138;
// HC_GrandPrixRewardMsg 通知客户端大奖赛奖励信息
MessageCode.HC_GrandPrixRewardMsg = 139;
// HC_UpdateGrandRankMsg Gate通知客户端更新大奖赛数据
MessageCode.HC_UpdateGrandRankMsg = 140;

// CH_AskExchangeInfoMsg 客户端请求兑换奖品数据
MessageCode.CH_AskExchangeInfoMsg = 141;
// CH_AskExchangeInfoMsg_Res 客户端请求兑换奖品数据结果
MessageCode.CH_AskExchangeInfoMsg_Res = 142;
// CH_ExchangePrizesMsg 客户端请求兑换奖品
MessageCode.CH_ExchangePrizesMsg = 143;
// CH_ExchangePrizesMsg_Res 客户端兑换奖品结果
MessageCode.CH_ExchangePrizesMsg_Res = 144;
// HC_UpdateExchangeMsg 通知客户端更新兑换奖品数据
MessageCode.HC_UpdateExchangeMsg = 145;
// CH_GetFirstRewardMsg 客户端领取首充奖励
MessageCode.CH_GetFirstRewardMsg = 146;
//HC_GetFirstRewardMsg_Res 服务器返回首充奖励
MessageCode.HC_GetFirstRewardMsg_Res = 147;
// HC_NewExchanInvenData 通知客户端新的兑换库存数据
MessageCode.HC_NewExchanInvenData = 148;
// CH_AskExchangeRecordMsg 客户端请求兑换奖励记录
MessageCode.CH_AskExchangeRecordMsg = 149;
// CH_AskExchangeRecordMsg_Res 客户端请求兑换奖励记录结果
MessageCode.CH_AskExchangeRecordMsg_Res = 150;
// CH_RewardSignInMsg   签到摇奖
MessageCode.CH_RewardSignInMsg = 154;
// CH_RewardSignInMsg   签到摇奖返回
MessageCode.CH_RewardSignInMsg_Res = 155;
// CH_RewardSignInMsg   领取摇奖点券
MessageCode.CH_GetRewardCouponMsg = 156;
// CH_RewardSignInMsg   领取摇奖点券
MessageCode.CH_GetRewardCouponMsg_Res = 157;
// CH_IsNewPlayerMsg   是否为新玩家
MessageCode.CH_IsNewPlayerMsg = 158;

// HC_RefreshLuckDraw 刷新客户端抽奖数据
MessageCode.HC_RefreshLuckDraw = 151;
// CH_PublicMsg 抽奖
MessageCode.CH_LuckDraw = 152;
// CH_LuckDraw_Res  抽奖结果
MessageCode.CH_LuckDraw_Res = 153;
// CH_ModifySignature   修改签名
MessageCode.CH_ModifySignature = 159;
// CH_PlayerSignature_Res  签名回复
MessageCode.CH_PlayerSignature_Res = 160;
// CH_RegisterMsg 客户端注册
MessageCode.CH_RegisterMsg = 161;
// CH_RegisterMsg_Res 注册结果
MessageCode.CH_RegisterMsg_Res = 162;
// CH_ModifyPassword 修改密码
MessageCode.CH_ModifyPassword = 163;
// CH_ModifyPassword_Res 修改密码结果
MessageCode.CH_ModifyPassword_Res = 164;
// CH_UseItem  使用道具
MessageCode.CH_UseItem = 165;
// CH_ModifyPassword_Res 使用道具结果
MessageCode.CH_UseItem_Res = 166;
//JP_Total 病毒活動取JP
MessageCode.JP_Total = 167;
//JP_Total_Res 病毒活動取JP結果
MessageCode.JP_Total_Res = 168;
/**********************************************************
 *协议id：2
 *协议号：200------300
 *协议名称：activity
 **********************************************************/
// SC_ActivityMsg_CODE---102---服务端活动协议
MessageCode.SC_ActivityMsg_CODE = 201;

/**********************************************************
 *协议id：3
 *协议号：300------400
 *协议名称：gate服务器 与 http服务器间通信协议
 **********************************************************/
// GH_RegisterMsg gate服务器向http服务器注册
MessageCode.GH_RegisterMsg = 301;
// GH_PlayerCount gate服务器向http服务器上传在线人数
MessageCode.GH_PlayerCountMsg = 302;

/**********************************************************
 *协议id：4
 *协议号：400------500
 *协议名称：gate服务器 与 game服务器间通信协议
 **********************************************************/
// GG_RegisterMsg game服务器向gate服务器注册
MessageCode.GG_ResgisterMsg = 401;



/**********************************************************
 *协议id：5
 *协议号：500------600
 *协议名称：room
 **********************************************************/
// CS_JoinCommStageMsg 客户端进入房间消息
MessageCode.CS_JoinCommStageMsg = 501;
// CS_JoinCommStageMsg_Res 服务端回复客户端进入房间消息
MessageCode.CS_JoinCommStageMsg_Res = 502;
// SC_NewPlayerJoinRoomMsg 新玩家进入房间，通知所有房间内的其他玩家
MessageCode.SC_NewPlayerJoinRoomMsg = 503;
// SC_RemovePlayerFromRoomMsg 房间移除玩家通知
MessageCode.SC_RemovePlayerFromRoomMsg = 504;
// CS_PlayerFireMsg 玩家发炮
MessageCode.CS_PlayerFireMsg = 505;
// CS_LeaveRoomMsg 客户端发送给gameServer离开房间
MessageCode.CS_LeaveRoomMsg = 506;
// CS_LeaveRoomMsg_Res gameServer返回客户端离开房间结果
MessageCode.CS_LeaveRoomMsg_Res = 507;
// SC_PlayerFireInRoomMsg gameServer通知所有房间内玩家，有玩家开炮
MessageCode.SC_PlayerFireInRoomMsg = 508;
// SC_NewFishJoinRoom gameServer通知所有房间内玩家，有新鱼进场
MessageCode.SC_NewFishJoinRoom = 509;
// CS_PlayerCatch 玩家捕捉到鱼
MessageCode.CS_PlayerCatch = 510;
// CS_PlayerCatch_Res 玩家捕捉结果
MessageCode.CS_PlayerCatch_Res = 511;
// CS_ModifyFort 玩家修改炮类型
MessageCode.CS_ModifyFort = 512;
// CS_ModifyFort_Res 玩家修改炮类型
MessageCode.CS_ModifyFort_Res = 513;
// SC_PlayerLevelUp 玩家升级
MessageCode.SC_PlayerLevelUp = 514;
// CS_DoleCoolDown 开始救济金倒计时
MessageCode.CS_DoleCoolDownMsg = 515;
// CS_DoleCoolDown_Res 救济金倒计时开始
MessageCode.CS_DoleCoolDown_Res = 516;
// CS_DoleGet 领取救济金
MessageCode.CS_DoleGetMsg = 517;
// CS_DoleGet_Res 领取救济金结果
MessageCode.CS_DoleGet_Res = 518;
// CS_UnlockFort 玩家解锁炮
MessageCode.CS_UnlockFort = 519;
// CS_UnlockFort_Res 玩家解锁炮结果
MessageCode.CS_UnlockFort_Res = 520;
// 玩家完成某个任务，服务器主动通知
MessageCode.SC_TaskFinishMsg = 521;
// 玩家领取任务奖励
MessageCode.CS_TaskReward = 522;
// 玩家领取任务奖励结果
MessageCode.CS_TaskReward_Res = 523;
// CS_OnlineReward 领取在线奖励
MessageCode.CS_OnlineReward = 524;
// CS_OnlineReward_Res 玩家领取在线奖励结果
MessageCode.CS_OnlineReward_Res = 525;
// 玩家个人彩金池抽奖
MessageCode.CS_JackpotDraw = 526;
// 玩家个人彩金池抽奖结果
MessageCode.CS_JackpotDraw_Res = 527;
// 玩家换炮台皮肤
MessageCode.CS_ModifyFortSkin = 528;
// 换炮台皮肤结果
MessageCode.CS_ModifyFortSkin_Res = 529;
// 解锁炮台皮肤
MessageCode.CS_UnlockFortSkin = 530;
// 解锁炮台皮肤结果
MessageCode.CS_UnlockFortSkin_Res = 531;
// SC_ModifyRoomInfo 服务器发送客户端 概率修正
MessageCode.SC_ModifyRoomInfoMsg = 532;
// CS_ChatInRoom 玩家在房间中聊天
MessageCode.CS_ChatInRoom = 533;
// CS_ChatInRoom_Res 服务器转发，有玩家在房间内聊天
MessageCode.CS_ChatInRoom_Res = 534;
// CS_InRoomReceiveMonthCardGift 房间领取每日月卡奖励
MessageCode.CS_InRoomReceiveMonthCardGift = 535;
// CS_InRoomReceiveMonthCardGift_Res 房间领取每日月卡奖励结果
MessageCode.CS_InRoomReceiveMonthCardGift_Res = 536;
// CH_TaskVIPGift 房间中玩家领取vip等级奖励
MessageCode.CH_InRoomTaskVIPGift = 537;
// CH_TaskVIPGift_Res 房间中玩家领取vip等级奖励结果
MessageCode.CH_InRoomTaskVIPGift_Res = 538;
// CS_OpenPokerFishMsg 玩家翻扑克鱼
MessageCode.CS_OpenPokerFishMsg = 547;
// CS_OpenPokerFishMsg_Res 玩家翻扑克鱼结果
MessageCode.CS_OpenPokerFishMsg_Res = 548;
// SC_UpdateBonusPoolsDataMsg 通知玩家最新奖金池数据
MessageCode.SC_UpdateBonusPoolsDataMsg = 549;

// CS_ShowPlayerRank 客户端请求排行榜信息
MessageCode.CS_ShowPlayerRank = 550;
// CS_ShowPlayerRank_Res 返回客户端信息
MessageCode.CS_ShowPlayerRank_Res = 551;

// CS_AgreeRank 是否同意排行
MessageCode.CS_AgreeRank = 552;
// CS_AgreeRank_Res 返回信息
MessageCode.CS_AgreeRank_Res = 553;
// SC_LogMsg 日志消息
MessageCode.SC_LogMsg = 554;
// SC_BillingWelfare 福利金
MessageCode.SC_BillingWelfare = 555;
// CS_ChangeFort 请求切换单双炮
MessageCode.CS_ChangeFort = 556;
// CS_ChangeFort_Res 切换单双炮返回
MessageCode.CS_ChangeFort_Res = 557;
// CS_OutDateFort 单双炮过期通知
MessageCode.CS_OutDateFort = 558;
// SC_FishTide 鱼潮来临
MessageCode.SC_FishTideMsg = 559;
// CS_GrandPrixCatch_Res 大奖赛玩家捕捉结果
MessageCode.CS_GrandPrixCatch_Res = 560;
// CS_UseDuiHuanCode 玩家使用兑换码
MessageCode.CS_UseDuiHuanCode = 561;
// CS_UseDuiHuanCode_Res DG_UseDuiHuanCode(DBServer向GateServer返回结果) 玩家使用兑换码结果
MessageCode.CS_UseDuiHuanCode_Res = 562;
// SC_ActivityStateMsg 通知玩家活动状态
MessageCode.SC_ActivityStateMsg = 563;
// SC_UpdateHegemonyRankMsg 通知玩家争霸赛最新排行数据
MessageCode.SC_UpdateHegemonyRankMsg = 564;
// CS_ExchangePrizesMsg 客户端请求兑换奖品（房间中）
MessageCode.CS_ExchangePrizesMsg = 565;
// CS_ExchangePrizesMsg_Res 客户端兑换奖品结果（房间中）
MessageCode.CS_ExchangePrizesMsg_Res = 566;
// CH_ReturnLoginMsg 断线重连
MessageCode.CH_ReturnLoginMsg = 567;
// SC_JoinCommHegemonyInfoMsg 争霸赛已开启则进入房间发送排行信息
MessageCode.SC_JoinCommHegemonyInfoMsg = 569;
// CS_GetFirstRewardMsg 玩家在房间中领取首冲奖励
MessageCode.CS_GetFirstRewardMsg = 570;
// CS_GetFirstRewardMsg_Res 玩家在房间中领取首冲奖励结果
MessageCode.SC_GetFirstRewardMsg_Res = 571;
// CS_ShowBulletRank 玩家请求炸弹头排行;
MessageCode.CS_ShowBulletRank = 572;
// CS_ShowBulletRank 玩家请求炸弹头排行;
MessageCode.DG_PlayerRankInfo = 573;
// 获取玩家赠送记录 CH_PublicMsg
MessageCode.CH_AskEmailRecord = 574;
// CH_AskEmailRecordMsg_Res 索要邮件历史记录结果
MessageCode.CH_AskEmailRecordMsg_Res = 575;
// SC_OpenLzMsg 开龙珠
MessageCode.SC_OpenLzMsg = 576;
// CS_ShowPlayerRank 玩家要开龙珠排行榜
MessageCode.CS_ShowLzRank = 577;
// DG_PlayerRankInfo 返回结果
MessageCode.CS_ShowLzRank_Res = 578;

// HM_AskShangXiaFenRecord 索要3天内上下分记录
MessageCode.CS_AskShangXiaFen = 579;
// HM_AskShangXiaFenRecord_Res 返回上下分记录
MessageCode.CS_AskShangXiaFen_Res = 580;
// CH_AskRoomLog 玩家索要进出房间log
MessageCode.CH_AskRoomLog = 581;
// CH_AskRoomLog_Res 进出房间记录
MessageCode.CH_AskRoomLog_Res = 582;
// CH_AskCatchFishLog 玩家要详情
MessageCode.CH_AskCatchFishLog = 583;
// CH_AskCatchFishLog_Res 返回结果
MessageCode.CH_AskCatchFishLog_Res = 584;
//CH_AskActivityAwardLog 玩家活動紀錄
MessageCode.CH_AskActivityAwardLog = 585;
//CH_AskActivityAwardLog_Res 玩家活動紀錄返回結果
MessageCode.CH_AskActivityAwardLog_Res = 586;
MessageCode.CH_JP_log = 587;
MessageCode.CH_JP_log_Res = 588;
/****************************************************
 * ******
 *协议id：6
 *协议号：600------700
 *协议名称：session
 **********************************************************/
// SS_RegisterMsg gateServer向ServerManager服务器注册
MessageCode.SM_ResgisterMsg = 601;
// SM_DisconnectOnePlayer 玩家在其他地方登录
MessageCode.SM_RemoteLogin = 602;
// ServerManager 通知 DB从新获取跑马灯数据
MessageCode.MD_UpdateScroll = 603;
// ServerManager通知DB新的系统邮件
MessageCode.MD_NewSystemEmailMsg = 604;
// ServerManager通知DB新的系统邮件结果
MessageCode.MD_NewSystemEmailMsg_Res = 605;
// ServerManager通知DB重新获取福利数据
MessageCode.MD_UpdateWelfareMsg = 606;
// ServerManager 通知 DB重新获取图片公告数据
MessageCode.MD_UpdatePicNotice = 607;
// ServerManager 通知 DB重新获取活动
MessageCode.MD_UpdateActivity = 608;
// 全服消息
MessageCode.SM_AllServerChat = 620;
// 事件公告
MessageCode.SM_EventNotice = 623;



/**********************************************************
 *协议id 7
 *协议号：700------800
 *协议名称：error
 **********************************************************/
MessageCode.SC_ErrorMsg = 701;
MessageCode.ErrorCode_PasswordError = 701; // 密码错误
MessageCode.ErrorCode_AccountError = 702;  // 账号错误
MessageCode.ErrorCode_Blockade = 703;      // 封号状态
MessageCode.ErrorCode_SafeCodeError = 704; // 安全码比对错误
MessageCode.ErrorCode_NoFreeGS = 705;      // 没有可用的gameServer
MessageCode.ErrorCode_RoomError = 706;     // 房间分配错误，属于服务器错误，联系客服或重新尝试
MessageCode.ErrorCode_RemoteLogin = 707;   // 异地登录
MessageCode.ErrorCode_NoGold = 708;        // 金币不足
MessageCode.ErrorCode_LevelLess = 709;     // 玩家等级不足
MessageCode.ErrorCode_FortLess = 710;      // 玩家炮等级不足
MessageCode.ErrorCode_WaitJoin = 711;      // 已经在游戏中，请等待离开
MessageCode.ErrorCode_LackData = 712;       // 缺少数据，玩家idlogin为0，缺少安全码等
MessageCode.ErrorCode_NoPlayer = 713;       // 没有找到玩家数据，重新登录
MessageCode.ErrorCode_NoDiamond = 714;      // 钻石不足
MessageCode.ErrorCode_NoTaskFinish = 715;   // 任务没有完成
MessageCode.ErrorCode_PackageFull = 716;    // 背包已满
MessageCode.ErrorCode_NoStack = 717;        // 不能继续堆叠
MessageCode.ErrorCode_TaskAwardReceive = 718;   // 任务奖励已领取
MessageCode.ErrorCode_ReSignIn = 719;       // 没有玩家数据请重新登录
MessageCode.ErrorCode_NoMoreOnlineReward = 720;// 没有更多的在线奖励
MessageCode.ErrorCode_AlreadyReceive = 721;     // 已经领取，不能重复领取
MessageCode.ErrorCode_RoomID = 722;         // 房间id错误
MessageCode.ErrorCode_NoEnoughDay = 723;    // 不满足天数要求
MessageCode.ErrorCode_Range = 724;          // 范围错误
MessageCode.ErrorCode_NoItem = 725;         // 道具不足
MessageCode.ErrorCode_NoTime = 726;         // 在线奖励的时间不满足条件
MessageCode.ErrorCode_NoActValue = 727;     // 活跃值不足
MessageCode.ErrorCode_NoJackpotDraw = 728;  // 个人奖金不足
MessageCode.ErrorCode_NoBonusFish = 729;    // 没打够彩金鱼
MessageCode.ErrorCode_DataError = 730;      // 数据错误，涵盖所有找不到数据的情况
MessageCode.ErrorCode_NoVIP = 731;          // vip等级不够
MessageCode.ErrorCode_SkillNoTime = 732;    // 技能时间已结束
MessageCode.ErrorCode_NoFish = 733;         // 没找到鱼
MessageCode.ErrorCode_NoFortSkin = 734;     // 没找到皮肤
MessageCode.ErrorCode_Expired = 735;        // 已过期
MessageCode.ErrorCode_NoGift = 736;         // 没有奖励
MessageCode.ErrorCode_AlreadyOwned = 737;   // 已经拥有
MessageCode.ErrorCode_LimitChar = 738;      // 限制字符
MessageCode.ErrorCode_NicknameLong = 739;   // 昵称太长
MessageCode.ErrorCode_NicknameRepeat = 740; // 昵称重复
MessageCode.ErrorCode_NotFindAct = 741;     // 没有找到活动
MessageCode.ErrorCode_NoCondition = 742;    // 没有满足领取条件
MessageCode.ErrorCode_TimeOut = 743;        // 读写数据响应超时
MessageCode.ErrorCode_NoSell = 744;         // 禁止出售
MessageCode.ErrorCode_NoOrder = 745;        // 订单不存在
MessageCode.ErrorCode_NoPay = 746;          // 订单未支付
MessageCode.ErrorCode_NoPayCode = 747;      // 商品不存在
MessageCode.ErrorCode_NoCardTime = 748;     // 月卡过期
MessageCode.ErrorCode_RepeatReceive = 749;  // 重复领取
MessageCode.ErrorCode_BeyondLimit = 750;    // 超出限额
MessageCode.ErrorCode_CreateLinkFail = 751; // 建立链接失败，检查配置以及服务器状态
MessageCode.ErrorCode_FishFull = 752;       // 召唤的鱼数量超出
MessageCode.ErrorCode_TreasureHuntOver = 753;// 浅湾寻宝玩法已过期

MessageCode.ErrorCode_FortPastDue = 754;// 单双炮时间过期或不能使用
MessageCode.ErrorCode_NoBullet = 755;        // 子弹数量不足

MessageCode.ErrorCode_DuiHuanCodeError = 756;// 兑换码错误
MessageCode.ErrorCode_DuiHuanCodeUsered = 757;// 兑换码已使用
MessageCode.ErrorCode_DuiHuanCodeRepeat = 758;// 兑换码不能重复领取奖励
MessageCode.ErrorCode_DuiHuanCodeCondition = 759;// 兑换码领取条件不足
MessageCode.ErrorCode_Version = 760;            // 版本错误
MessageCode.ErrorCode_NoLottery = 761;         // 奖券不足
MessageCode.ErrorCode_ReturnFail = 762;         // 断线重连失败
MessageCode.ErrorCode_NoLuckDrawNum = 763;  // 抽奖次数不足
MessageCode.ErrorCode_NoAccPoints = 764;    // 积分不足
MessageCode.ErrorCode_RewardSign = 765;     // 摇奖签到活动已关闭
MessageCode.ErrorCode_NotNewPlayer = 766;     //不是新玩家
MessageCode.ErrorCode_SignInNumNotEnough = 767;     //签到次数不够
MessageCode.ErrorCode_TodayAlreadySignIn = 768;     //今天已经签到
MessageCode.ErrorCode_NoDoleNum = 769;          // 救济金次数不足
MessageCode.ErrorCode_SignatureLong = 770;   // 签名修改失败
MessageCode.ErrorCode_RegisterError = 771;   // 注册账号错误
MessageCode.ErrorCode_AccountUsed = 772;    // 账号已使用
MessageCode.ErrorCode_CostEnough = 773;    // 货币不足
MessageCode.ErrorCode_NoChat = 774;         // 禁言
MessageCode.ErrorCode_Inv = 775;            // 邀请码错误
MessageCode.ErrorCode_NoAuthorityEmail = 776;    // 无权限发送邮件
MessageCode.ErrorCode_ChipsIsNotEnough = 777;    // 筹码不足
MessageCode.ErrorCode_CanNotBets = 778;          // 下注达到上限无法下注
MessageCode.ErrorCode_IntervalLimit = 779;  // 现金网使用，2次操作间隔时间不能少于3秒
MessageCode.ErrorCode_WaitforLeave = 780;   // 等待离开完成


MessageCode.ErrorCode_CheckError = 600;         // 验证登录签名错误
MessageCode.ErrorCode_LackIdentity = 601;       // 非第三方登录需要玩家手机识别码
MessageCode.ErrorCode_AppIDError = 602;         // appID错误，不是我们游戏的登录
MessageCode.ErrorCode_ScriptError = 603;        // 脚本执行错误
MessageCode.ErrorCode_CodeError = 604;          // code无效（微信登录）
MessageCode.ErrorCode_UidError = 605;           // 玩家第三方登录uid错误
MessageCode.ErrorCode_UnknowError = 606;        // 未知错误
MessageCode.ErrorCode_TokenError = 607;         // 无效的token
MessageCode.ErrorCode_LinkTimeout = 608;        // 会话超时
MessageCode.ErrorCode_ChannelError = 611;       // 渠道名称和渠道ID不匹配
MessageCode.ErrorCode_AccountLength = 612;      // 账号长度错误
MessageCode.ErrorCode_PasswordNotNull = 613;    // 密码不能空
/**********************************************************
 *协议id：9
 *协议号：900-----1000
 *协议名称：email
 ***********************************************************/
// 客户端向服务端请求未读取邮件
MessageCode.CS_AllEmailMsg = 901;
// 服务端发送未读取邮件给客户端
MessageCode.CS_AllEmailMsg_Res = 902;
// 客户端读取邮件
MessageCode.CS_EmailReadMsg = 903;
// 服务器响应客户端读取邮件
MessageCode.CS_EmailReadMsg_Res = 904;
// 客户端领取邮件道具
MessageCode.CS_EmailGetPropMsg = 905;
// 服务端响应领取邮件道具
MessageCode.CS_EmailGetPropMsg_Res = 906;
// 客户端赠送道具验证玩家是否存在
MessageCode.CS_playerIsExistsMsg = 907;
// 客户端赠送道具验证玩家是否存在结果
MessageCode.CS_playerIsExistsMsg_Res = 908;
// 客户端赠送好友道具
MessageCode.CS_FriendsGiftsMsg = 909;
// 客户端赠送好友道具结果
MessageCode.CS_FriendsGiftsMsg_Res = 910;
// 服务器通知客户端有新邮件
MessageCode.SC_AskPlayerGetEmailMsg = 911;

/**********************************************************
 *协议id：10
 *协议号：1000-----1100
 *协议名称：skill
 ***********************************************************/

// 客户端请求服务端技能使用消息
MessageCode.CS_SkillUseMsg = 1002;
// 服务端响应客户端技能使用消息
MessageCode.CS_SkillUseMsg_Res = 1003;
// 客户端请求服务端技能锁定消息
MessageCode.CS_SkillLockMsg = 1004;
// 服务端响应客户端技能锁定消息
MessageCode.CS_SkillLockMsg_Res = 1005;
// 冰冻时间结束
MessageCode.SC_IceEnd = 1006;

/**********************************************************
 *协议id：12
 *协议号：1200-----1300
 *协议名称：pay
 ***********************************************************/
// CS_AskOrderIdMsg 客户端请求支付订单号
MessageCode.CS_AskPaymentMsg = 1201;
// CS_AskOrderIdMsg_Res 客户端请求支付订单号结果
MessageCode.CS_AskPaymentMsg_Res = 1202;
// CH_InHallReceiveOrderItemMsg 在大厅领取订单道具
MessageCode.CH_InHallReceiveOrderItemMsg = 1203;
// CH_InHallReceiveOrderItemMsg_Res 在大厅领取订单道具结果
MessageCode.CH_InHallReceiveOrderItemMsg_Res = 1204;
// CS_InRoomReceiveOrderItemMsg 在房间领取订单道具
MessageCode.CS_InRoomReceiveOrderItemMsg = 1205;
// CS_InRoomReceiveOrderItemMsg_Res 在房间领取订单道具结果
MessageCode.CS_InRoomReceiveOrderItemMsg_Res = 1206;
// CS_ReceiveSuccessMsg 通知PhpServer领取道具成功
MessageCode.CS_ReceiveSuccessMsg = 1207;
// CS_AskAllNotReceiveOrderMsg 客户端请求所有未领取的订单
MessageCode.CS_AskAllNotReceiveOrderMsg = 1208;
// CS_AskAllNotReceiveOrderMsg_Res 客户端请求未领取的订单结果
MessageCode.CS_AskAllNotReceiveOrderMsg_Res = 1209;
// CH_InHallCheckAndReceiveOrderMsg 在大厅请求校验订单并领取订单
MessageCode.CH_InHallCheckAndReceiveOrderMsg = 1210;
// CH_InRoomCheckAndReceiveOrderMsg 在房间请求校验订单并领取订单
MessageCode.CH_InRoomCheckAndReceiveOrderMsg = 1211;

/**********************************************************
 *协议id：14
 *协议号：1401-----1500
 *协议名称：龙虎的消息
 ***********************************************************/
// CS_PublicMsg 玩家上庄
MessageCode.CS_ShangZhuangMsg = 1401;
// CS_ShangZhuang_Res 上庄结果
MessageCode.CS_ShangZhuangMsg_Res = 1402;
// CS_PublicMsg 玩家下庄
MessageCode.CS_XiaZhuangMsg = 1403;
// CS_ShangZhuang_Res 下庄结果
MessageCode.CS_XiaZhuangMsg_Res = 1404;
// CS_XiaZhuMsg 玩家下注
MessageCode.CS_XiaZhuMsg = 1405;
// CS_XiaZhuMsg_Res 玩家下注结果
MessageCode.CS_XiaZhuMsg_Res = 1406;
// SC_PlayerXiaZhu 通知同房间玩家，有人下注
MessageCode.SC_PlayerXiaZhuMsg = 1407;
// SC_PlayerZuoZhuang 通知同房间玩家，新庄家坐庄
MessageCode.SC_PlayerZuoZhuangMsg = 1408;
// SC_StateChange 状态转变
MessageCode.SC_StateChangeMsg = 1409;
// SC_XiaZhuInfo 下注最终数据567
MessageCode.SC_XiaZhuInfoMsg = 1410;
// SC_Result 结算
MessageCode.SC_Result = 1411;
// CS_JoinDTStageMsg_Res 服务端回复客户端进入龙虎房间消息
MessageCode.CS_JoinDTStageMsg_Res = 1412;
// CS_PublicMsg 玩家要上庄列表
MessageCode.CS_AskShangZhuangListMsg = 1413;
// CS_AskShangZhuangList_Res 上庄列表
MessageCode.CS_AskShangZhuangListMsg_Res = 1414;
// CS_PublicMsg 索要玩家列表
MessageCode.CS_AskPlayerListMsg = 1415;
// CS_AskShangZhuangList_Res 玩家列表
MessageCode.CS_AskPlayerListMsg_Res = 1416;

//心跳
MessageCode.KeepAlive = 65500;
//心跳回复
MessageCode.KeepAlive_Res = 65499;


