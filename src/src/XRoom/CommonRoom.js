var CommonRoom = BaseRoom.extend({
    _superGoldPoolNum: 0,                    //< 超级彩金池金币数目
    _lastGoldPoolNum: 0,                     //< 保存前一次的彩金池数目
    _goldNumDif: 0,                          //< 超级彩金池金币差值
    _listView: null,
    _fishData: null,
    
    /**
     * 构造方法
     * @param delegate 委托对象
     * @param roomId 房间id
     * @param msg 服务器消息 CS_JoinCommStageMsg_Res
     */
    ctor: function (delegate, roomId, msg) {
        this._super(delegate, roomId,msg);
    },

    /**
     * 释放资源
     */
    onClear: function () {
        this._super();
        //清空缓冲池
        cc.pool.drainAllPools();
    },

    /**
     * 逻辑更新
     * @param dt
     */
    updateLogic: function (dt) {
        this._super(dt);
        cf.FishMgr.updateLogic(dt);
    },

    /**
     * 初始化UI
     * @param msg
     */
    initUI: function (msg) {
        this._super(msg);
        
        var self = this;
        
        //龙珠UI 
        self._uiDragonBall = new UIDragonBall(self._roomId);
        this._delegate.addChild(self._uiDragonBall, cf.Zoder.PLAYER);
        
        
        //新手任务和新手引导
        var player = this._locPlayer;
        
        cf.EffectMgr.playLocalTip(player._curPos);
        
       
    
        

        // var spineJson1, spineAtlas1;
        // spineJson1 = Res.GAME.status_Json;
        // spineAtlas1 = Res.GAME.status_Atlas;

        // var status =  ActionMgr.createSpine(spineJson1,spineAtlas1,1);
        // status.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        // status.setAnimation(0, "dice4", true);
        // this._delegate.addChild(status, cf.Zoder.BK);
        
       
        
     
        

        
    },

    //提示卡牌结果获取失败
    promptOfCardNumError: function () {
        cf.UITools.showHintToast(cf.Language.getText("text_1272"));
    },

    

   
    /**
     * 显示被攻击特效
     */
    showBeAttacked: function (fish, num) {
        var self = this;
        if (fish._sprite) {
            // if (!self._bShowAttack) {
            //     self._bShowAttack = true;
                var tintTo = cc.tintTo(0.4, 255, 0, 0);
                var tintToBack = cc.tintTo(0.4, fish._curColor);
                var seq = cc.sequence(
                    tintTo, 
                    tintToBack,
                );

                fish._sprite.runAction(cc.sequence(
                    cc.repeat(seq, num),
                    cc.callFunc(function () {
                        fish._sprite.setColor(fish._curColor);
                        //fish._bShowAttack = false;
                    })
                ));
           // }
        }
    },

    /**
     * 添加监听
     * @private
     */
    addListener: function () {
        this._super();
        var self = this;
        //创建网络消息
        var msgDataArray = [
            //福利金LOG（测试用）
            //{msgId: MessageCode.SC_LogMsg, callFunc: self._resLog},
            //玩家捕鱼消息
            {msgId: MessageCode.CS_PlayerCatch_Res, callFunc: self._resPlayerCatch},
            //更新彩金池数据
            {msgId: MessageCode.SC_UpdateBonusPoolsDataMsg, callFunc: self._resUpdateBonusPool},
            //争霸赛活动状态
            {msgId: MessageCode.SC_ActivityStateMsg, callFunc: self._resRankActivityState},
            //获取对应活动的玩家排名
            {msgId: MessageCode.SC_UpdateHegemonyRankMsg, callFunc: self._resPlayerRankList},
            //玩家首次进入房间时需要传递的消息
            {msgId: MessageCode.SC_JoinCommHegemonyInfoMsg, callFunc: self._resPlayJoinRoom}
        ];

        //创建网络监听       
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];          
            self._onNetMsgListener.push(cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this)));
        }
    },

    /**
     * 移除监听
     * @private
     */
    removeListener: function () {
        this._super();
    },   

    /**
     * 玩家捕鱼消息处理
     * @param msg
     * @private
     */
    _resPlayerCatch: function (msg) {
        var resMsg = $root.CS_PlayerCatch_Res.decode(msg);
        
        if (resMsg) {
            
            if(resMsg.listFish.length > 1){
                console.log(resMsg);
                console.log("this._fishData",this._fishData);
                if (this._fishData != resMsg){
                    
                    this._fishData = resMsg;
                     //cc.log("玩家座位：" + resMsg.nSeatID + ",捕鱼!!!!!!!!!!!");
                //刷新玩家金币信息，奖金信息
                var player = this._roomPlayer[resMsg.nSeatID];
                if(player) {
                    player.updatePlayerData(Player.DataType.GOLD, resMsg.nGoldNum);
                    player.refreshUI();
                }

                var localPlayer = cf.PlayerMgr.getLocalPlayer();

                //移除子弹
                if (localPlayer.getSeatID() != resMsg.nSeatID) {
                    //子弹张网
                    cf.BulletMgr.removeBullet(resMsg.nSeatID, resMsg.strBulletID);
                } else {

                //本地玩家添加彩金鱼
                if (resMsg.nJackpot != 0) {
                    localPlayer.setJackpot(resMsg.nJackpot);
                    var temp = localPlayer.getBonusFishNum();
                    localPlayer.setBonusFishNum(++temp);
                }
            }
                 //大于1则说明是技能鱼，进行技能鱼逻辑判定
                 var fishLen = resMsg.listFish.length;
                 if (fishLen > 1) {
                   
                   // var time = setTimeout(()=>{
                        this.checkSkillFish(resMsg);
                        //clearTimeout(time);
                   // },3000);
                    
                 }

                var fishLength = resMsg.listFish.length;
                if (fishLength < 0) {
                    return;
                }

                if(resMsg.nSeatID == - 1) {
                    cc.log("---------------------------------");
                }

                //如果有鱼被捕捉，执行鱼死亡动画
                //道具奖励在鱼死亡中实现了
                cf.FishMgr.removeFish(resMsg);

                }
                

            }else{
                 //cc.log("玩家座位：" + resMsg.nSeatID + ",捕鱼!!!!!!!!!!!");
            //刷新玩家金币信息，奖金信息
            var player = this._roomPlayer[resMsg.nSeatID];
            // console.log("player",player)
            if(player) {
                player.updatePlayerData(Player.DataType.GOLD, resMsg.nGoldNum);
                player.refreshUI();
            }

            var localPlayer = cf.PlayerMgr.getLocalPlayer();

            //移除子弹
            if (localPlayer.getSeatID() != resMsg.nSeatID) {
                //子弹张网
                cf.BulletMgr.removeBullet(resMsg.nSeatID, resMsg.strBulletID);
            } else {

                //本地玩家添加彩金鱼
                if (resMsg.nJackpot != 0) {
                    localPlayer.setJackpot(resMsg.nJackpot);
                    var temp = localPlayer.getBonusFishNum();
                    localPlayer.setBonusFishNum(++temp);
                }
            }

            var fishLength = resMsg.listFish.length;
            if (fishLength < 0) {
                return;
            }

            if(resMsg.nSeatID == - 1) {
                cc.log("---------------------------------");
            }

            //如果有鱼被捕捉，执行鱼死亡动画
            //道具奖励在鱼死亡中实现了
            cf.FishMgr.removeFish(resMsg);


            }
           
            
        } else {
            cc.error("_resPlayerCatch错误:CS_PlayerCatch_Res");
        }
    
    },

    
    /**
     * 添加技能鱼检测逻辑 需要确定特殊鱼类型
     */
     checkSkillFish: function (resMsg) {
        var fishList = resMsg.listFish;
        resMsg.isSkillFish = true;
        resMsg.ndeadTime = 0.2;
        var skillType = -1;
        var skillFishArray = [];
        for (var i in fishList) {
            var fish = cf.FishMgr.getFishMap().get(fishList[i].strFishID);
            if (fish && cf.checkCanLock(fish)) {
                switch (fish.getFishModel()._fishId) {
                    case 27:
                        skillFishArray.splice(0, 0, fish);
                        skillType = 0;
                        cf.SkillfishBoolean = false;
                        break;
                    case 28://闪电鱼
                        skillFishArray.splice(0, 0, fish);
                        skillType = 1;
                        cf.SkillfishBoolean = false;
                        break;
                    default:
                        skillFishArray.push(fish);
                        
                        break;
                }
            }
        }

        //执行技能效果
       // var time = setTimeout(()=>{
            switch (skillType) {
                case 0://炸弹鱼特效
                     
                        cf.EffectMgr.doAimBoomEffect(skillFishArray, resMsg);
                        
                    break;
                case 1://闪电鱼特效
                    
                var rand = cf.randomBool();
                if (rand) {
                    cf.EffectMgr.addRandomLineFishOrder(skillFishArray, resMsg);
                    resMsg.ndeadTime = 0.15 * skillFishArray.length + 0.2;
                } else {
                    cf.EffectMgr.addRedioActivityLineFishOrder(skillFishArray, resMsg);
                }
                        
                    
                    break;
            }
            clearTimeout(time);
       // }, 1500)
        
    },

    /**
     * 更新超级彩金池数据
     */
    _resUpdateBonusPool: function (msg) {
        var resMsg = $root.SC_UpdateBonusPoolsDataMsg.decode(msg);
        if (resMsg) {
            this._superGoldPoolNum = resMsg.nNewBonusPools;
            var dif = this._superGoldPoolNum - this._lastGoldPoolNum;
            if (dif < 0) {
                this._goldNumDif = Math.abs((this._superGoldPoolNum - this._lastGoldPoolNum) / 100);
                this._lastGoldPoolNum = this._superGoldPoolNum - 0.1 * Math.abs(dif);
                return;
            }
            this._goldNumDif = (this._superGoldPoolNum - this._lastGoldPoolNum) / 10;
        }
    },

    /**
     * 服务器发送的争霸赛的活动状态
     */
    _resRankActivityState: function (msg) {
        var resMsg = $root.SC_ActivityStateMsg.decode(msg);
        if(resMsg) {
            // 活动ID
            var actId = resMsg.nActID;
            // 活动类型
            var actType = resMsg.nActType;
            // 活动状态：1-30秒后开启 2-开启 3-结束
            var actState = resMsg.nActState;
            //活动开始时间
            var nActStartTime = resMsg.nActStartTime;
            // 活动剩余时间
            this._rankActivityRemainTime = resMsg.nRemaTime / 1000;

            var presentTime = (new Date()).getTime();
            var rewardNum = resMsg.nAddParam;

            cf.SaveDataMgr.setNativeData("rankRewardNum", resMsg.nAddParam);

            cf.SaveDataMgr.setNativeData("rankStartTime", nActStartTime);
            switch (actState) {
                case 1:
                    //活动三十秒后开始
                    cf.SaveDataMgr.setNativeData("rankIsReady", true);
                    this._rankListInRoom = new RoomRankActivityScore();
                    this._rankListInRoom.setCountTime((nActStartTime - presentTime) / 1000);
                    this._rankListInRoom.setActivityState(0);
                    this._rankListInRoom.setRewardGold(rewardNum);
                    this.setSuperGoldPoolState(false);
                    this._delegate.addChild(this._rankListInRoom,cf.Zoder.UI);
                    break;
                case 2:
                    //活动已经开始
                    cf.SaveDataMgr.setNativeData("rankIsReady", false);
                    if(this._rankListInRoom) {
                        this._rankListInRoom.setCountTime(this._rankActivityRemainTime);
                        this._rankListInRoom.setHegemony(0);
                        this._rankActivityRemainTime = 0;
                        this._rankListInRoom.setActivityState(1);
                        this.setSuperGoldPoolState(false);
                    }
                    else {
                        this._rankListInRoom = new RoomRankActivityScore();
                        this._rankListInRoom.setCountTime(this._rankActivityRemainTime);
                        this._rankListInRoom.setHegemony(0);
                        this._rankListInRoom.setActivityState(1);
                        this._rankActivityRemainTime = 0;
                        this.setSuperGoldPoolState(false);
                        this._delegate.addChild(this._rankListInRoom,cf.Zoder.UI);
                    }

                    break;
                case 3:
                    //活动已经结束
                    this._rankListInRoom.showWaitLabel();
                    break;
            }
        }
    },

    /**
     * 玩家的排行信息
     */
    _resPlayerRankList: function (msg) {
        var resMsg = $root.SC_UpdateHegemonyRankMsg.decode(msg);
        if(resMsg) {
            var bSett = resMsg.bSett;
            var playerNameList = [];
            var playerScoreList = [];
            var concludeLocalPlayer = false;

            for(var i in resMsg.hegemonyRankInfo) {
                // 当前排名
                var strNickName = resMsg.hegemonyRankInfo[i].strNickName;
                // 玩家id
                var playerId = resMsg.hegemonyRankInfo[i].nidLogin;
                // 当前分数
                var currNum = resMsg.hegemonyRankInfo[i].nCurrNum;

                if(strNickName == cf.PlayerMgr.getLocalPlayer().getNickName()) {
                    this._rankListInRoom.setCompetitionRank(parseInt(i) + 1);
                    //结算时使用的参数 玩家获取的奖金
                    var nBonus = resMsg.hegemonyRankInfo[i].nBonus;
                    concludeLocalPlayer = true;
                    this._rankListInRoom.setlocalPlayerRewardInfo(nBonus);
                }
                playerNameList.push(strNickName);
                playerScoreList.push(currNum);
            }
            if(!concludeLocalPlayer) {
                this._rankListInRoom.setCompetitionRank(parseInt(-1));
            }
            if(this._rankListInRoom) {
                this._rankListInRoom.addRankItem(playerNameList,playerScoreList);
            }
            if(bSett) {
                this._rankListInRoom.showWaitLabel();
                this._rankListInRoom.runAction(cc.sequence(cc.delayTime(0.5),cc.callFunc(function () {
                    this.addClearUI();
                },this._rankListInRoom)));
            }
        }
    },

    _resPlayJoinRoom: function(msg) {
        var resMsg = $root.SC_JoinCommHegemonyInfoMsg.decode(msg);
        if(resMsg) {
            // 争霸赛结束时间
            var activityEndTime =  resMsg.nHegemonyEndTime;
            // 玩家当前争霸赛功绩值
            var momentScore = resMsg.nCurrFeats;
            // 当前排名（未上榜为0）
            var momentRank = resMsg.nRank;
            // 排行数据
            var playerNameList = [];
            var playerScoreList = [];
            for(var i in resMsg.hegemonyRankInfo) {
                // 当前排名
                var strNickName = resMsg.hegemonyRankInfo[i].strNickName;
                // 玩家id
                var playerId = resMsg.hegemonyRankInfo[i].nidLogin;
                // 当前分数
                var currNum = resMsg.hegemonyRankInfo[i].nCurrNum;
                playerNameList.push(strNickName);
                playerScoreList.push(currNum);
            }
            this._rankActivityRemainTime = (activityEndTime - this._serverTime) / 1000;
            this._rankListInRoom = new RoomRankActivityScore();
            this._rankListInRoom.setCountTime(this._rankActivityRemainTime);
            this._rankActivityRemainTime = 0;
            this._rankListInRoom.setActivityState(1);
            this.setSuperGoldPoolState(false);
            this._delegate.addChild(this._rankListInRoom,cf.Zoder.UI);
            this._rankListInRoom.setHegemony(momentScore);
            this._rankListInRoom.setCompetitionRank(momentRank);
            this._rankListInRoom.addRankItem(playerNameList,playerScoreList);
        }
    },

    /**
     * 房间事件解析
     * @param msg
     * @private
     */
    _doEvent: function (msg) {
        this._super(msg);
        if (msg == null) {
            return;
        }
        //判断是否为结构
        var flag = msg;
        if (typeof msg == "object") {
            flag = msg.flag;
        }
        //显示商城，皮肤解锁，
        switch (flag) {            
            case BaseRoom.EventFlag.ERROR_TREASUREHUNT_OUTOFTIME:                
                break;           
            case  BaseRoom.EventFlag.ERROR_CARDNUM_NOTENOUGH:
                this._locPlayer.cardLayer.satisfyCardOpen = false;
                this._locPlayer.cardLayer.removeAllCard();
                break;           
        }
    },
    
    /**
     * 福利金LOG（测试用）
     * @param msg
     * @private
     */
    _resLog:function(msg){
        var resMsg = $root.SC_LogMsg.decode(msg);

        var logBtn = this._delegate.getChildByTag(100000);
        if(!logBtn.isVisible()){
            logBtn.setVisible(true);
        }
        cc.log(resMsg.logInfo);
        if(this._listView == null){
            this._listView = new ccui.ListView();
            this._listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
            this._listView.setGravity(ccui.ListView.GRAVITY_CENTER_VERTICAL);
            this._listView.setTouchEnabled(false);
            this._listView.setContentSize(cc.size(1200, 400));
            this._listView.setBounceEnabled(true);
            this._listView.setPosition(100, 100);
            this._listView.jumpToBottom();//设置回到底部
            this._delegate.addChild(this._listView);

            //创建默认背景
            var itemLayout = new ccui.Layout();
            var _label = new cc.LabelTTF( resMsg.logInfo, cf.Language.FontName, 20,cc.size(600,0));
            itemLayout.setContentSize(_label.getContentSize());

            _label.setPosition(itemLayout.width / 2,itemLayout.height / 2 );

            _label.setColor(cc.color.RED);

            itemLayout.addChild(_label);

            this._listView.pushBackCustomItem(itemLayout);

            this._listView.setItemsMargin(8);
        }
        else{
            if(this._listView.getItems().length > 1){
                this._listView.removeItem(0);
            }
            //创建默认背景
            var itemLayout = new ccui.Layout();
            var _label = new cc.LabelTTF(resMsg.logInfo, cf.Language.FontName, 20,cc.size(600,0));
            itemLayout.setContentSize(_label.getContentSize());

            _label.setPosition(itemLayout.width / 2,itemLayout.height / 2 );

            _label.setColor(cc.color.RED);

            itemLayout.addChild(_label);
            this._listView.pushBackCustomItem(itemLayout);

            this._listView.setItemsMargin(8);
        }
    }
});