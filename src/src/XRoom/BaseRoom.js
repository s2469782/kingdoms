var BaseRoom = cc.Class.extend({
    _delegate: null,                //< 委托对象
    _randSeed: null,                //< 服务器随机种子
    _mainNode: null,
    _roomId: 0,                     //< 房间id
    _serverTime: null,              //< 服务器时间
    //地图相关
    _bLoad: false,                  //< 是否加载完成
    _resBK: null,
    _mapNode: null,                 //< 地图节点
    //网络监听
    _onNetMsgListener: null,        //< 网络消息监听
    _doEventListener: null,         //< 事件监听
    //龍珠JP
     _labelDragonJP: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],    //數字滾動
    _run: false,    //龍珠JP跳動
    _nowNum: 0,
    _nowNumStr: null,
    _setUpNum: 0,
    _IncreaseGap: 0.3,
    _previousNum: null,
    //玩家数据
    _roomPlayer: [],                //<玩家数组
    _locPlayer: null,
    csTimeDif: null,                        //< 客户端与服务器差值
    //触摸相关
    _touchPointLeft: null,
    _touchPointRight: null,
    _touchNum: 0,
    _touchTime: -1,
    _mapTouchPoint: null,
    _rankActivityRemainTime: 0,             //< 争霸赛活动的剩余时间
    _hasToldGoldOutRoom: false,                 //< 是否已经进行过新手的金币超出提示
    _lvReward: 0,
    _uiDragonBall: null,        //龙珠UI
    _uiGameSetLayer: null,
    _labelNetDelay: null,
    _dragonJP: null,  //龍珠JP
    _bulletIndex: null,
    

    ctor: function (delegate, roomId, msg) {
        this._delegate = delegate;
        //添加网络监听
        this.addListener();
        //初始化场景
        this._initScene(roomId);
        //初始化房间数据
        this._initRoomData(msg);
        //初始化UI
        this.initUI(msg);
       
        //支付逻辑判断
        if (SDKHelper.isInBack()) {
            SDKHelper.setInBack(false);

            //判断支付状态
            if (SDKHelper._resultState == SDKHelper.CODE.SUCCESS) {
                cf.UITools.showLoadingToast(cf.Language.getText("text_1227"), 20, true, delegate);
                setTimeout(function () {
                    SDKHelper.setTarget(delegate);
                    SDKHelper.setPayCallBack(null);
                    SDKHelper.payNativeCallBack(SDKHelper._resultState);
                }, 2000);
            } else {
                SDKHelper.clearPayState();
            }
        } else {
            //请求是否有未到账订单，游戏生命周期只调用一次
            /*if (cf.AskOrder) {
             cf.AskOrder = false;
             SDKHelper.askOrderList(delegate);
             }*/
        }
       
       
    //    cf.EffectMgr.larbar(29,3060,65);

        
                

    },

    /**
     * 病毒JP
     */
    getJP: function(){
        var _msg = new $root.JP_Total();
        var wr = $root.JP_Total.encode(_msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.JP_Total, wr);

       
        var t2 = setInterval(function(){
            var _msg = new $root.JP_Total();
            var wr = $root.JP_Total.encode(_msg).finish();
            cf.NetMgr.sendSocketMsg(MessageCode.JP_Total, wr);

        },10000);

        
    },

    
    

    /**
     * 释放资源
     */
    onClear: function () {
        this._mainNode.removeAllChildren();
        this._delegate.removeAllChildren();
        this._mainNode = null;
        this._delegate = null;
        this._serverTime = null;
        cf.removeTextureForKey(this._resBK);
        this._roomPlayer = null;
        this.removeListener();
        
    },

    //病毒設定金額
    setWinMoney(num) {
        
        this._nowNum = 0;

        this._setUpNum = num;

        this._IncreaseGap = num / (10 * 3);

        if (this._IncreaseGap < 0.00001) {
            this._IncreaseGap = 0.00001;
        }
        // for (var a = 0, len = this.spriteNumber.length; a < len; a++) {
           
        //     this.spriteNumber[a].spriteFrame = this.frameNumber[0];
        // }

    },
    //病毒讓錢開始滾動
    runMoney(){
        for (var a = 0, b = 0, len = 10; a < len; a++) {
            this._labelDragonJP[a].string = "0";
        }
        this._run = true;


    },

    //錢亂跳
    setRunMoneySpritFrame() {
         
         
        // let str = this.nowNumStr.split("").reverse().join("");
         let str = this._nowNumStr.split("").reverse().join("");
        

         for (var a = 0, b = 0, len = str.length; a < len; a++) {
             if (str[a] !== '.' ) {
                var pos = str.length-(1+a);
                this._labelDragonJP[pos].string = Math.floor(parseInt(str[a]));
                 b++;
             }
         }
 
     },
     //最終結果
    setMoneySpritFrame() {

        // let str = this.nowNumStr.split("").reverse().join("");
         
         let str = ""+Math.floor(parseInt(this._nowNumStr.split("").join("")));
      
        
         for (var a = 0, b = 0, len = str.length; a < len; a++) {
             if (str[a] !== '.' ) {
                var pos = str.length-(1+a);
                this._labelDragonJP[pos].string = Math.floor(parseInt(str[a]));
                
             }else {
                
                
             }
             b++;
         }

         this._previousNum = Math.floor(parseInt(this._nowNumStr));
     },

  
    
    /**
     * 逻辑更新
     * @param dt
     */
    updateLogic: function (dt) {
        if (this._bLoad) {
            if (this._touchTime > -1) {
                this._touchTime -= dt;
            }
        }

        //龍珠JP
        if(this._run){
            this._nowNum = Math.floor(parseInt((this._nowNum + this._IncreaseGap)));
            
            if(this._nowNum >= this._setUpNum){
                var te = (Math.floor( this._setUpNum *10000000000+0.000000001  ) / 10000000000 ) + 0.000000001;
                
                var tearr = (te+"").split('');
                this._nowNumStr ="";
                // var spotIndex = tearr.indexOf('.');
                
                for(var a=0,b=0; a<=tearr.length;a++){
                 
                    // if(a > spotIndex ){
                //         this._nowNumStr += tearr[a];
                //         b++;
                //         if(b>=4){
                //             break;
                //         }
                //     }
                //    else{
                       this._nowNumStr += tearr[a];
                        
                  //}
                    
                }

                this.setMoneySpritFrame();
                this._run = false;
               
            }else{
                
                this._nowNumStr = Math.floor(this._nowNum)+"";
                this.setRunMoneySpritFrame();
            }

        }
    },

    /**
     * 初始化场景
     * @param roomId
     * @private
     */
    _initScene: function (roomId) {
        var self = this;

        cf.isPlayGameVoice = false;
        cf.nGameVoiceCount = 0;
        if (self._delegate) {
            self._roomId = roomId;

            self._mapNode = new cc.Node();
            self._delegate.addChild(self._mapNode, cf.Zoder.BK);

            //创建背景
            //设置纹理格式
            cf.setDefaultTextureFormat(cc.Texture2D.PIXEL_FORMAT_RGB565);

            self._bLoad = false;

           
           
            
            //加载layer
            var background = cf.Data.ROOM_DATA[roomId].background;
            
            //龍珠JP background
            var dragonJp = new cc.Sprite("#dragon_jp.png");
            dragonJp.setPosition(cc.winSize.width/2, cc.winSize.height-93);
            dragonJp.setVisible(false);
            self._delegate.addChild(dragonJp,cf.Zoder.BULLET);        

            var heroBtn = new ccui.Button();
            heroBtn.loadTextureNormal("dragon_icon_game.png", ccui.Widget.PLIST_TEXTURE);
            heroBtn.setPosition(dragonJp.width /2 , dragonJp.height/2 -40);
            heroBtn.setPressedActionEnabled(true);
            heroBtn.pressIndex = this._roomId;
            heroBtn.setVisible(false);
            heroBtn.addClickEventListener((sender, type)=>{
                var heroLayer = new UIDragonHeroRecord(sender.pressIndex);
                this._delegate.addChild(heroLayer, cf.Zoder.BULLET);
            });
            dragonJp.addChild(heroBtn);

        // //龍珠JP 金額
    
        for(var a = 0; a < this._labelDragonJP.length; a++){
            this._labelDragonJP[a] = new cc.LabelBMFont(this._labelDragonJP[a], Res.LOBBY.font_resource_090);
            this._labelDragonJP[a].setPosition((dragonJp.width / 2)+112-(a*25), dragonJp.height/2-3);
            this._labelDragonJP[a].setScaleX(0.7);
            this._labelDragonJP[a].setScaleY(0.6);
            dragonJp.addChild(this._labelDragonJP[a]);

        }
        
            //龍珠JP數字顯示
            if(roomId == 1002){
                dragonJp.setVisible(false);
                heroBtn.setVisible(false);
            }else{
                this.getJP();
                heroBtn.setVisible(true);
                dragonJp.setVisible(true);
            }
           
         //   var path = self._resBK = createVerPath("picture/" + background + "_01" + GameConfig.suffix_jpg);
         var path = self._resBK = createVerPath("picture/" + background + "_01" + GameConfig.suffix_jpg);
            cc.loader.load(path, function () {
                var spMap = new cc.Sprite(self._resBK);
                spMap.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
                spMap.setTag("spMap");
                self._mapNode.addChild(spMap);

                

                self._bLoad = true;
            });

            

            cf.setDefaultTextureFormat(cc.Texture2D.PIXEL_FORMAT_RGBA4444);

            //生成场景物件
        } else {
            cc.error("父节点为null");
        }


    },

    

    /**
     * 初始化房间信息
     * @param msg CS_JoinCommStageMsg_Res
     * @private
     */
    _initRoomData: function (msg) {
        var self = this;
        cf.SkillMgr.init();
        //记录服务器随机种子
        self._randSeed = msg.strSeed;

        //记录服务器时间
        self._serverTime = msg.strTime;
        self._mainNode = new cc.Node();
        self._delegate.addChild(self._mainNode, cf.Zoder.FISH);
        
        
        //初始化特效
        cf.EffectMgr.init(self._delegate);

        //初始化子弹
        cf.BulletMgr.init(self._mainNode); 

        //初始化玩家信息
        self._createPlayer(msg.playerInfoRoom);

        /*cf.debugNode = new cc.Node();
         self._mainNode.addChild(cf.debugNode,cf.Zoder.FISH+1);*/
        //初始化鱼群信息
        cf.FishMgr.init(self._mainNode);
        cf.FishMgr.setLocalTime(new Date().getTime());
        cf.FishMgr.createFishData(msg.fishInfoRoom, msg.strTime);


        //初始化触摸信息
        this._touchPointLeft = cc.p(0, 0);
        this._touchPointRight = cc.p(0, 0);
        this._mapTouchPoint = cf.Map.create();

        var sJson, sAtlas;
        sJson = Res.GAME.wave_Json;
        sAtlas = Res.GAME.wave_Atlas;
        
        var efJson, efAtlas;
        efJson = Res.GAME.ef_Json;
        efAtlas =Res.GAME.ef_Atlas;
        

       if(self._mainNode.getChildByTag("sp")){
        self._mainNode.getChildByTag("sp").removeFromParent();
       }

       if(self._delegate.getChildByTag("effect")){
        self._delegate.getChildByTag("effect").removeFromParent();
       }
       var wave =  ActionMgr.createSpine(sJson,sAtlas,1);
       wave.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
       wave.setTag("sp");

       var ef = ActionMgr.createSpine(efJson, efAtlas, 1);
       ef.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
       ef.setTag("effect");
       if(self._roomId == 1002){
           wave.setAnimation(0, "bg_1", true);
           ef.setAnimation(1, "bg_01", true);
       }else if(self._roomId  == 1003){
           wave.setAnimation(0, "bg_2", true);
           ef.setAnimation(1, "bg_02", true);
       }else if(self._roomId  == 1004){
           wave.setAnimation(0, "bg_3", true);
           ef.setAnimation(1, "bg_03", true);
       }
       self._delegate.addChild(ef, cf.Zoder.BK+1);
       self._mainNode.addChild(wave, cf.Zoder.FISH+1); 


    },

    /**
     * 创建玩家
     * @param playerInfo
     * @private
     */
    _createPlayer: function (playerInfo) {
        var winSize = cc.winSize;
        
        //创建座位
        for (var i = 0; i < 4; ++i) {
            var player = new Player();
            this._roomPlayer[i] = player;
            this._mainNode.addChild(player, cf.Zoder.PLAYER);
            
            player.setAnchorPoint(0.5, 0);
            switch (i) {
                case 0://左下:逆时针旋转对应座位2
                    player.setPosition(winSize.width / 2 - 280, 0);
                    player.changeAutoEffectPosition(860, 350);
                    player.changeAutoNoticePosition(65, 250);
                    
                    break;
                case 1://右下：逆时针旋转对应座位3
                    player.setPosition(winSize.width / 2 + 280, 0);
                    player.changeAutoEffectPosition(300, 350);
                    player.changeAutoNoticePosition(-495, 250);
                    
                    break;
                case 2://右上
                    player.setRotation(180);
                    player.setPosition(winSize.width / 2 + 280, winSize.height);
                    player.changeAutoEffectPosition(860, 350);
                    player.changeAutoNoticePosition(65, 250);
                   
                    break;
                case 3://左上
                    player.setRotation(180);
                    player.setPosition(winSize.width / 2 - 280, winSize.height);
                    player.changeAutoEffectPosition(300, 350);
                    player.changeAutoNoticePosition(-495, 250);
                    
                    break;
            }
        }

        //获取玩家数据
        for (var i = 0; i < playerInfo.length; ++i) {
            //创建玩家信息结构体
            var playerData = playerInfo[i];

            //判断是否是本地玩家
            if (parseInt(playerData.strGamePlayerID) == cf.PlayerMgr.getLocalPlayer().getIdLogin()) {
                this._roomPlayer[playerData.nSeatID].setPlayerModel(cf.PlayerMgr.getLocalPlayer());
                this._roomPlayer[playerData.nSeatID].setLocalPlayer(true);
                this._roomPlayer[playerData.nSeatID].setRandom(this._randSeed);
                cc.log("本地玩家座位id = " + playerData.nSeatID);
            }
            this._roomPlayer[playerData.nSeatID].setPlayerModelData(playerData);
            this._roomPlayer[playerData.nSeatID].setAlive(true);
            this._roomPlayer[playerData.nSeatID].setRoomId(this._roomId);

            /*var skillInfo = playerData.skillInfoRoom;
             var locallength = skillInfo.length;
             for (var index = 0; index < locallength; index++) {
             if (skillInfo[index].nSkillID == cf.SkillId.rageSkill) {
             var skillLastTime = (skillInfo[index].strEndTime - this._serverTime);
             cf.SkillMgr.setRageSkillOpen(playerData.nSeatID);
             cf.SkillMgr.setRageSkillLastTime(playerData.nSeatID, skillLastTime / 1000);
             this._roomPlayer[playerData.nSeatID].runAction(cc.sequence(cc.delayTime(0.5), cc.callFunc(function () {
             this.addPromptOfRage();
             }, this._roomPlayer[playerData.nSeatID])));
             }
             }*/
        }

        //本地玩家座位id
        var localSeatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
        //如果本地玩家座位id大于2，则旋转画布
        var isRotate = false;
        if (localSeatId >= 2) {
            this._mainNode.setRotation(-180);
            this._mainNode.setPosition(cc.winSize.width, cc.winSize.height);
            isRotate = true;
        }

        for (var i = 0; i < 4; ++i) {
            if (this._roomPlayer[i].isAlive()) {
                this._roomPlayer[i].initData(this._serverTime);
                //暂时取消进房间同步子弹
               // this._roomPlayer[i].synchronousBullet();
            }
            this._roomPlayer[i].rotationUI(i, isRotate);
        }
        this._locPlayer = this._roomPlayer[localSeatId];
    },

    /**
     * 初始化UI
     * @param msg
     */
    initUI: function (msg) {
        var self = this;

        //技能按钮
        //锁定
        var lockSkillBtn = new SkillIcon(cf.SkillId.lockSkill, cc.p(cc.winSize.width-55, 430));
        self._delegate.addChild(lockSkillBtn, cf.Zoder.BULLET);

        //狂暴
        //var rageSkillBtn = new SkillIcon(cf.SkillId.rageSkill, cc.p(cc.winSize.width / 2 + 10, 2));
        // self._delegate.addChild(rageSkillBtn, cf.Zoder.UI);
        //双炮
        var gunSkillBtn = new SkillIcon(cf.SkillId.gunSkill, cc.p(cc.winSize.width-8, 210));
        self._delegate.addChild(gunSkillBtn, cf.Zoder.BULLET);

        //自动开炮
        var autoBtn = new SkillIcon(cf.SkillId.auto, cc.p(cc.winSize.width - 8, 100));
        self._delegate.addChild(autoBtn, cf.Zoder.BULLET);

        this.recomeResetSkill(msg);

        //设置
        self._uiGameSetLayer = new UIGameSetLayer();
        self._uiGameSetLayer.setPosition(cc.winSize.width, cc.winSize.height - 95);
        self._delegate.addChild(self._uiGameSetLayer, cf.Zoder.BULLET);

        //延迟
        var spNetDelay = new cc.Sprite("#resource_024_10.png");
        spNetDelay.setAnchorPoint(1, 1);
        spNetDelay.setPosition(cc.winSize.width, cc.winSize.height);
        spNetDelay.setColor(cc.color.BLACK);
        spNetDelay.setOpacity(150);
        spNetDelay.setScaleX(0.8);
        self._delegate.addChild(spNetDelay,cf.Zoder.UI);        

        var labelNetDelay = self._labelNetDelay = new cc.LabelBMFont(cf.NetDelayTime+"ms", Res.LOBBY.font_resource_093);
        labelNetDelay.setColor(cc.color.GREEN);
        labelNetDelay.setPosition(spNetDelay.width / 2, spNetDelay.height / 2);
        labelNetDelay.setScaleX(1 / 0.8 * 0.5);
        labelNetDelay.setScaleY(0.5);
        spNetDelay.addChild(labelNetDelay);

        //自動篩選
        var autolockBtn = new SkillIcon(cf.SkillId.autolock, cc.p(cc.winSize.width-55, 310));
        self._delegate.addChild(autolockBtn, cf.Zoder.BULLET);
        cf.layerHasShowInGame = false;

        

         
       
       
            
            
          
    
       
    },

    //断线重连后初始化技能
    recomeResetSkill: function (msg) {
        var skillCDTimeList = msg.skillCDTimeList;
        var localSeatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
        for (var i in skillCDTimeList) {
            var skillLastTime = (skillCDTimeList[i].strEndTime - this._serverTime) / 1000;
            if (!(skillLastTime > 0)) {
                continue;
            }
            switch (skillCDTimeList[i].nSkillID) {
                case cf.SkillId.rageSkill:
                    cf.SkillMgr.setRageSkillOpen(localSeatId);
                    cf.SkillMgr.setRageSkillLastTime(localSeatId, skillLastTime);
                    var skillIcon = this._delegate.getChildByTag(10004);
                    var hasRunTime = cf.Data.SKILLDATA[cf.SkillId.lockSkill].skill_cd - skillLastTime;
                    this._roomPlayer[localSeatId].runAction(cc.sequence(cc.delayTime(0.3), cc.callFunc(function () {
                        skillIcon.doCoolAction(cf.SkillId.rageSkill, Math.round(hasRunTime * 100 / cf.Data.SKILLDATA[cf.SkillId.rageSkill].skill_cd));
                    }, this._roomPlayer[localSeatId])));
                    break;
                case cf.SkillId.lockSkill:
                    cf.SkillMgr.setLockSkillOpen(localSeatId);
                    cf.SkillMgr.setLockSkillLastTime(localSeatId, skillLastTime);
                    var skillIcon = this._delegate.getChildByTag(10001);
                    var hasRunTime = cf.Data.SKILLDATA[cf.SkillId.lockSkill].skill_cd - skillLastTime;
                    this._roomPlayer[localSeatId].runAction(cc.sequence(cc.delayTime(0.3), cc.callFunc(function () {
                        skillIcon.doCoolAction(cf.SkillId.lockSkill, Math.round(hasRunTime * 100 / cf.Data.SKILLDATA[cf.SkillId.lockSkill].skill_cd));
                    }, this._roomPlayer[localSeatId])));
                    break;
                case cf.SkillId.iceSkill:
                    var skillIcon = this._delegate.getChildByTag(10002);
                    var hasRunTime = cf.Data.SKILLDATA[cf.SkillId.iceSkill].skill_cd - skillLastTime;
                    this._roomPlayer[localSeatId].runAction(cc.sequence(cc.delayTime(0.3), cc.callFunc(function () {
                        skillIcon.doCoolAction(cf.SkillId.iceSkill, Math.round(hasRunTime * 100 / cf.Data.SKILLDATA[cf.SkillId.iceSkill].skill_cd));
                    }, this._roomPlayer[localSeatId])));
                    break;
            }
        }
    },

    _fortAngleLogic: function (touches, bDelete) {
        //获取本地玩家
        var localPlayer = this._locPlayer;
        if (localPlayer.isAlive() == false)
            return;

        var point0 = null, point1 = null;
        var touchNum = touches.length;
        if (!localPlayer.isDoubleFort()) {
            if (!bDelete && touchNum >= 1) {
                point0 = touches[touchNum - 1].getLocation();
                cf.convertPosWithSeatId(point0);
                localPlayer.setFortAngle(point0);
                localPlayer.onFireBefore();
            }
            this._touchNum = 0;
        } else {
            for (var i = 0; i < touchNum; i++) {
                var touch = touches[i];
                var pos = touch.getLocation();
                var id = touch.getID();

                if (bDelete) {
                    this._mapTouchPoint.remove(id);
                } else {
                    this._mapTouchPoint.put(id, pos);
                }

            }
            var size = this._mapTouchPoint.size();
            if (size <= 0) {
                this._touchNum = size;
                return;
            }
            if (size >= 2) {
                point0 = this._mapTouchPoint.getValueByIndex(0);
                point1 = this._mapTouchPoint.getValueByIndex(1);
                if (point0.x < point1.x) {
                    this._touchPointLeft.x = point0.x;
                    this._touchPointLeft.y = point0.y;
                    this._touchPointRight.x = point1.x;
                    this._touchPointRight.y = point1.y;
                } else {
                    this._touchPointLeft.x = point1.x;
                    this._touchPointLeft.y = point1.y;
                    this._touchPointRight.x = point0.x;
                    this._touchPointRight.y = point0.y;
                }
                cf.convertPosWithSeatId(this._touchPointLeft);
                cf.convertPosWithSeatId(this._touchPointRight);
                localPlayer.setFortAngle(this._touchPointLeft, this._touchPointRight);
            } else {
                if (this._touchNum > 1) {
                    this._touchTime = 0.5;
                }

                point0 = this._mapTouchPoint.getValueByIndex(0);
                this._touchPointLeft.x = point0.x;
                this._touchPointLeft.y = point0.y;

                if (localPlayer.getAutoFireState()) {
                    if (this._touchTime < 0) {
                        cf.convertPosWithSeatId(this._touchPointLeft);
                        localPlayer.setFortAngle(this._touchPointLeft);
                    }
                } else {
                    cf.convertPosWithSeatId(this._touchPointLeft);
                    localPlayer.setFortAngle(this._touchPointLeft);
                }
            }
            localPlayer.onFireBefore();
            this._touchNum = size;
        }
    },

    /**********************************************************
     * 触摸相关方法
     **********************************************************/
    onTouchesBegan: function (touches, event) {
        var player = this._locPlayer;
        if (!player._touch) {
            player.setTouch(touches);
        }
        //隐藏快捷键
        player.hideFastBtn();

        if (player.isUnlockUI()) {
            return;
        }
       

        if (this._uiGameSetLayer) {
            this._uiGameSetLayer.hide();
        }

        if (this._uiDragonBall) {
            this._uiDragonBall.hideRule();
        }


        if (player._bankrupt || !player._bFortCanFire) {
            var str = "", strLeft = null, strRight = null;
            if (GameConfig.buy == 1) {
                str = cf.Language.getText("text_1438");
                strLeft = cf.Language.getText("text_1116");
                strRight = cf.Language.getText("text_1125");
            } else {
                str = "您的弹药不足,请即刻前往存款补充,重回渔场大杀四方!";
                strRight = "确定";
            }
            var dlg = UIDialog.createCustom(str, strRight, strLeft, false, function (type) {
                if (type == UIDialog.Btn.RIGHT) {
                }
            }, this);
            this._delegate.addChild(dlg, 100);
            return;
        }
        if (cf.SkillMgr.checkPlayerCanFire(player.getSeatID())) {
            this._fortAngleLogic(touches, false);
        }
    },

    onTouchesMoved: function (touches, event) {
        var player = this._locPlayer;
        if (player._bankrupt || !player._bFortCanFire) {
            //金币不足提示
            //cf.dispatchEvent(BaseRoom.DoEvent,BaseRoom.EventFlag.UI_SHOP);
            return;
        }
        if (cf.SkillMgr.checkPlayerCanFire(player.getSeatID())) {
            this._fortAngleLogic(touches, false);
        }
    },

    onTouchesEnded: function (touches, event) {
        //获取本地玩家
        var player = this._locPlayer;
        if (cf.SkillMgr.checkPlayerCanFire(player.getSeatID())) {
            this._fortAngleLogic(touches, true);
            if (this._touchNum <= 0)
                this._playerShotFire();
        }
    },

    onTouchesCancelled: function (touches, event) {
        //获取本地玩家
        var player = this._locPlayer;
        if ((!player._LOCKSKILLISON && !player._RAGESKILLISON)) {
            this._fortAngleLogic(touches, true);
            if (this._touchNum <= 0)
                this._playerShotFire();
        }
    },

    _playerShotFire: function () {
        var player = this._locPlayer;

        if (player.isAlive()) {
            //停止开炮
            player.onFireEnd();
        }
    },

    /**
     * 添加监听
     * @private
     */
    addListener: function () {
        var self = this;

        //创建网络消息
        var msgDataArray = [
            //玩家进入房间消息
            {msgId: MessageCode.SC_NewPlayerJoinRoomMsg, callFunc: self._resPlayerEnter},
            //玩家离开房间消息
            {msgId: MessageCode.SC_RemovePlayerFromRoomMsg, callFunc: self._resPlayerLeave},
            //玩家开炮消息
            {msgId: MessageCode.SC_PlayerFireInRoomMsg, callFunc: self._resPlayerOnFire},
            //玩家切换炮倍消息
            {msgId: MessageCode.CS_ModifyFort_Res, callFunc: self._resPlayerModifyFort},
            //玩家升级
            //{msgId: MessageCode.SC_PlayerLevelUp, callFunc: self._resPlayerLvUp},
            //玩家离开房间返回大厅
            {msgId: MessageCode.CS_LeaveRoomMsg_Res, callFunc: self._resLeaveRoom},
            //玩家更换炮台皮肤
            {msgId: MessageCode.CS_ModifyFortSkin_Res, callFunc: self._resModifyFortSkin},
            //玩家解锁皮肤
            {msgId: MessageCode.CS_UnlockFortSkin_Res, callFunc: self._resUnlockFortSkin},
            //服务器修正概率
            {msgId: MessageCode.SC_ModifyRoomInfoMsg, callFunc: self._resModifyRoomInfo},
            //鱼潮到来
            {msgId: MessageCode.SC_FishTideMsg, callFunc: self._resCreateFishTide},
            //切换炮倍的返回结果,
            {msgId: MessageCode.CS_ChangeFort_Res, callFunc: self._resModifyFortMod},
            //单双炮过期通知
            {msgId: MessageCode.CS_OutDateFort, callFunc: self._resOutDateFort},
            //玩家救济金倒计时
            {msgId: MessageCode.CS_DoleCoolDown_Res, callFunc: self._resDoleCoolDown},
            //玩家领取救济金
            {msgId: MessageCode.CS_DoleGet_Res, callFunc: self._resDoleGet},
            //龍珠JP更新
            {msgId: MessageCode.JP_Total_Res, callFunc: self._resUpdateDragonJP}
        ];

        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener.push(cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this)));
        }
        //创建事件监听
        self._doEventListener = cf.addListener(BaseRoom.DoEvent, self._doEvent.bind(this));
        
    },


    CheckKill: function(n){
        var self = this;

       
        
        var commonRoom = cf.RoomMgr.getRoom();
        var player = commonRoom._locPlayer;     
        var _catchFishArray = [];
        var fishMgr = cf.FishMgr;
        var fishMap = fishMgr.getFishMap();
        var length = fishMap.size();
        var screen = [];
        var fishkill = [];
        var randArr = [];
      //  if (this._model.getSeatID() == this._localPlayerModel.getSeatID()) {
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        
            for(var a = 0; a < length; a ++){

                if(fishMap._elements[a].value.checkInScreen()){
                    screen.push(fishMap._elements[a].value.checkInScreen());
                }
            }
            if(length < n){
                n = length;
            }
                for(var b = 0; b < n; b++){
                    var num = Math.floor(Math.random()*screen.length);
                    _catchFishArray.push(fishMap._elements[num].value.getModel().getServerIndex());
                    var fishKillRate = fishMap._elements[num].value.getModel().getKillRate();
                    fishkill.push(fishKillRate);
                    var rand =  playerModel.getRandom();
                    var randRate = rand.nextInt(0, 100000);
                    randArr.push(randRate);

                }
               
            //概率 = 鱼概率*（1+房间修正+个人修正）
            
            //cc.log("张网：鱼击杀原始概率 = " + fishKillRate + ", 加成概率 = " + fishKillRate * 2 + ", 当前概率 = " + randRate + ", 当前次数 = " + rand.getCount());

            //判断是否捕捉,这里将击杀概率放大2倍
           // if (randRate <= fishKillRate * 2000 && !(this._localPlayerModel.getStopSendMsg())) {
                
                //发捕鱼消息给服务器
                //cc.log("张网：发送子弹消息，索引 = " + this.getServerIndex());
                var msg = new $root.CS_PlayerCatch();
                msg.strBulletID = this._bulletIndex;
                msg.listFishID = _catchFishArray;
                // msg.listFishRandom = fishkill;
                // msg.listFishBound = randArr;
                 msg.listFishRandom = fishkill;
                msg.listFishBound = randArr;
                var wr = $root.CS_PlayerCatch.encode(msg).finish();
                cf.NetMgr.sendSocketMsg(MessageCode.CS_PlayerCatch, wr);

               
            
           // }
       // }


    },

    

     //龍屬性技能
     skill_shu: function(msg){
         var self = this;
        var bgJson, bgAtlas;
        bgJson = Res.GAME.skill_bg_Json;
        bgAtlas = Res.GAME.skill_bg_Atlas;

        var ZhangFeiJson, ZhangFeiAtlas;
        ZhangFeiJson = Res.GAME.ZhangFei_Json;
        ZhangFeiAtlas = Res.GAME.ZhangFei_Atlas; 

        var ShuJson, ShuAtlas;
        ShuJson = Res.GAME.Shu_Json;
        ShuAtlas = Res.GAME.Shu_Atlas;

        var LiuBeiJson, LiuBeiAtlas;
        LiuBeiJson = Res.GAME.LiuBei_Json;
        LiuBeiAtlas = Res.GAME.LiuBei_Atlas;

        var GuanYuJson, GuanYuAtlas;
        GuanYuJson = Res.GAME.GuanYu_Json;
        GuanYuAtlas = Res.GAME.GuanYu_Atlas;

        var skillShuJson, skillShuAtlas;
        skillShuJson = Res.GAME.skill_Shu_Json;
        skillShuAtlas = Res.GAME.skill_Shu_Atlas;

        var skillmoney1Json, skillmoney1Atlas;
        skillmoney1Json = Res.GAME.skill_money1_Json;
        skillmoney1Atlas = Res.GAME.skill_money1_Atlas;

       

        // var commonRoom = cf.RoomMgr.getRoom();                  
        // var player = commonRoom._locPlayer;
        //根据座位id设置位置
     //   var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
      //  var convert = cf.convertSeatId(msg.nSeatID);
        cf.SoundMgr.playEffect(69);

        var bg =  ActionMgr.createSpine(bgJson,bgAtlas,1);
        bg.setAnimation(0, "Shu", false);
        bg.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
        var convert = cf.convertSeatId(msg.nSeatID);


        var money1 = ActionMgr.createSpine(skillmoney1Json,skillmoney1Atlas,1);
        money1.setAnimation(0, "Shu", false);


        var skill_shu = ActionMgr.createSpine(skillShuJson,skillShuAtlas,1);
        skill_shu.setAnimation(2, "Shu", false);

        var zhang = ActionMgr.createSpine(ZhangFeiJson,ZhangFeiAtlas,1);
        zhang.setAnimation(1, "Shu", false);
        bg.addChild(zhang);
        
        var guan = ActionMgr.createSpine(GuanYuJson,GuanYuAtlas,1);
        guan.setAnimation(2, "Shu", false);
        guan.setCompleteListener(()=>{
            guan.removeFromParent();
        });
        bg.addChild(guan);

        

        var Liu = ActionMgr.createSpine(LiuBeiJson,LiuBeiAtlas,1);
        Liu.setAnimation(3, "Shu", false);
        Liu.setCompleteListener(()=>{
            Liu.removeFromParent();
        });
        bg.addChild(Liu);

        var shu = ActionMgr.createSpine(ShuJson,ShuAtlas,1);
        shu.setAnimation(4, "Shu", false);
      
        if(self._locPlayer.getSeatID() == msg.nSeatID){
        shu.setCompleteListener(()=>{
            bg.runAction(cc.sequence(
                cc.delayTime(0.1),
                cc.callFunc(()=>{
                    bg.addChild(money1);
                    bg.addChild(skill_shu);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);
                }),
                cc.delayTime(1),
                cc.callFunc(()=>{
                var fishMgr = cf.FishMgr;
                var fishMap = fishMgr.getFishMap();
               

        // for(var a = 0; a < fishMap._elements.length; a ++){

        //     fishMap._elements[a].value.setFrozen(true);
        // }

                   // 动画结束事调用-- loopCount 循环的次数  traceIndex动画的索引  
                this.CheckKill(cf.Data.ATTRBALLSKILL_DATA[5].hitRange);
                        // for(var a = 0; a < fishMap._elements.length; a ++){
                        //     this.showBeAttacked(fishMap._elements[a].value, 2);
                        //     fishMap._elements[a].value.setFrozen(false);
    
                        // }
                cf.dispatchEvent(BaseRoom.DoEvent,{
                    flag: BaseRoom.EventFlag.UPDATE_DRAGONSKILL_USED,
                    sender: 5,
                    seatId: msg.nSeatID
        
                });

            })
    
            ))

        });
            bg.addChild(shu);
       
            self._delegate.addChild(bg, cf.Zoder.UI);
        }else{
            money1.setPosition(cc.winSize.width/2, cc.winSize.height/2);
            skill_shu.setPosition(cc.winSize.width/2, cc.winSize.height/2);

            self._delegate.runAction(cc.sequence(
                cc.delayTime(3.5),
                cc.callFunc(()=>{
                    self._delegate.addChild(money1);
                    self._delegate.addChild(skill_shu);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);
                }),
                cc.delayTime(1),
                cc.callFunc(()=>{
                money1.removeFromParent();
                skill_shu.removeFromParent();
                // var fishMgr = cf.FishMgr;
                // var fishMap = fishMgr.getFishMap();

                // this.CheckKill(10);
                      
                
                })
    
            ))



        }

    },

    //土屬性技能
    skill_MaChao: function(msg){
        
        var self = this;
        var bgJson, bgAtlas;
        bgJson = Res.GAME.skill_bg_Json;
        bgAtlas = Res.GAME.skill_bg_Atlas;

        var MaChaoJson, MaChaoAtlas;
        MaChaoJson = Res.GAME.MaChao_Json;
        MaChaoAtlas = Res.GAME.MaChao_Atlas;

        var skillMaChaoJson, skillMaChaoAtlas;
        skillMaChaoJson = Res.GAME.skill_MaChao_Json;
        skillMaChaoAtlas = Res.GAME.skill_MaChao_Atlas;

        var skillmoney1Json, skillmoney1Atlas;
        skillmoney1Json = Res.GAME.skill_money1_Json;
        skillmoney1Atlas = Res.GAME.skill_money1_Atlas;

        // var commonRoom = cf.RoomMgr.getRoom();                  
        // var player = commonRoom._locPlayer;
        //根据座位id设置位置
       var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
       var convert = cf.convertSeatId(msg.nSeatID);
        cf.SoundMgr.playEffect(68);

        var money1 = ActionMgr.createSpine(skillmoney1Json,skillmoney1Atlas,1);
        money1.setAnimation(4, "MaChao", false);


        var skillMaChao = ActionMgr.createSpine(skillMaChaoJson,skillMaChaoAtlas,1);
        skillMaChao.setAnimation(2, "animation", false);
        skillMaChao.addAnimation(2, "animation2", false);

        var MaChao = ActionMgr.createSpine(MaChaoJson,MaChaoAtlas,1);
        MaChao.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        MaChao.setAnimation(1, "MaChao1", false);
        MaChao.addAnimation(1, "MaChao2", false);
        

        var bg =  ActionMgr.createSpine(bgJson,bgAtlas,1);
        bg.setAnimation(0, "MaChao1", false);
       // bg.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        bg.addAnimation(0, "MaChao2", false);
        
        
       // guan.setPosition(bg.width/2-800, bg.height/2-300);
       var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
        var convert = cf.convertSeatId(msg.nSeatID);

        
     
        if(self._locPlayer.getSeatID() == msg.nSeatID){
            self._delegate.addChild(MaChao, cf.Zoder.UI);

            MaChao.addChild(bg, cf.Zoder.BULLET);

            MaChao.runAction(cc.sequence(
                cc.delayTime(2),
                cc.callFunc(()=>{

                    MaChao.addChild(skillMaChao, cf.Zoder.BULLET);
                    MaChao.addChild(money1, cf.Zoder.BULLET);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);
                   
                }),
                cc.delayTime(1.5),
                cc.callFunc(()=>{
                   
                    var fishMgr = cf.FishMgr;
                    var fishMap = fishMgr.getFishMap();
                   
                       // 动画结束事调用-- loopCount 循环的次数  traceIndex动画的索引  
                    this.CheckKill(cf.Data.ATTRBALLSKILL_DATA[3].hitRange);
                            

                    cf.dispatchEvent(BaseRoom.DoEvent,{
                        flag: BaseRoom.EventFlag.UPDATE_DRAGONSKILL_USED,
                        sender: 3,
                        seatId: msg.nSeatID
            
                    });
                    
                }),
                cc.delayTime(3),
                cc.callFunc(()=>{
                    MaChao.removeFromParent();
                })
            ))

        }else{
            money1.setPosition(cc.winSize.width/2, cc.winSize.height/2);
            money2.setPosition(cc.winSize.width/2, cc.winSize.height/2);
            skillMaChao.setPosition(cc.winSize.width/2, cc.winSize.height/2);


            self._delegate.runAction(cc.sequence(
                cc.delayTime(1.5),
                cc.callFunc(()=>{
                    self._delegate.addChild(money1, cf.Zoder.BULLET);
                    self._delegate.addChild(skillMaChao, cf.Zoder.BULLET);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);
                    //this.CheckKill(10);
                }),
                cc.delayTime(4),
                cc.callFunc(()=>{
                    money1.removeFromParent();
                    skillMaChao.removeFromParent();
                     
                
                            

    
                })
            ))


        }

    },
    
    //龍珠冰屬性
    skill_sunJian: function(msg){

        var self = this;
        var bgJson, bgAtlas;
        bgJson = Res.GAME.skill_bg_Json;
        bgAtlas = Res.GAME.skill_bg_Atlas;

        var sunJianJson, sunJianAtlas;
        sunJianJson = Res.GAME.sunJian_Json;
        sunJianAtlas = Res.GAME.sunJian_Atlas;

        var skillsunJianJson, skillsunJianAtlas;
        skillsunJianJson = Res.GAME.skill_sunJian_Json;
        skillsunJianAtlas = Res.GAME.skill_sunJian_Atlas;

        var skillmoney1Json, skillmoney1Atlas;
        skillmoney1Json = Res.GAME.skill_money1_Json;
        skillmoney1Atlas = Res.GAME.skill_money1_Atlas;

        // var commonRoom = cf.RoomMgr.getRoom();                  
        // var player = commonRoom._locPlayer;
        //根据座位id设置位置
     //   var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
      //  var convert = cf.convertSeatId(msg.nSeatID);
        cf.SoundMgr.playEffect(64);

        var money1 = ActionMgr.createSpine(skillmoney1Json,skillmoney1Atlas,1);
        money1.setAnimation(4, "SunJian", false);

       

        var skillsunJian = ActionMgr.createSpine(skillsunJianJson,skillsunJianAtlas,1);
        skillsunJian.setAnimation(2, "SunJian", false);

        var sunJian = ActionMgr.createSpine(sunJianJson,sunJianAtlas,1);
        sunJian.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        sunJian.setAnimation(1, "SunJian1", false);
        sunJian.addAnimation(1, "SunJian2", false);
        

        var bg =  ActionMgr.createSpine(bgJson,bgAtlas,1);
        bg.setAnimation(0, "SunJian1", false);
       // bg.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        bg.addAnimation(0, "SunJian2", false);
        

       // guan.setPosition(bg.width/2-800, bg.height/2-300);
       var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
        var convert = cf.convertSeatId(msg.nSeatID);

        
        
        if(self._locPlayer.getSeatID() == msg.nSeatID){
            self._delegate.addChild(sunJian, cf.Zoder.UI);

            sunJian.addChild(bg, cf.Zoder.BULLET);

            sunJian.runAction(cc.sequence(
                cc.delayTime(1.5),
                cc.callFunc(()=>{
                    sunJian.addChild(money1, cf.Zoder.BULLET);
                    sunJian.addChild(skillsunJian, cf.Zoder.BULLET);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);

                    
                }),
                cc.delayTime(1.5),
                cc.callFunc(()=>{
                    var fishMgr = cf.FishMgr;
                    var fishMap = fishMgr.getFishMap();
                   
            // for(var a = 0; a < fishMap._elements.length; a ++){
    
            //     fishMap._elements[a].value.setFrozen(true);
            // }
    
                       // 动画结束事调用-- loopCount 循环的次数  traceIndex动画的索引  
                    this.CheckKill(cf.Data.ATTRBALLSKILL_DATA[0].hitRange);
                            

                    cf.dispatchEvent(BaseRoom.DoEvent,{
                        flag: BaseRoom.EventFlag.UPDATE_DRAGONSKILL_USED,
                        sender: 0,
                        seatId: msg.nSeatID
            
                    });
                    
                }),
                cc.delayTime(4),
                cc.callFunc(()=>{
                    sunJian.removeFromParent();
                   
    
                })
            ))

        }else{
            money1.setPosition(cc.winSize.width/2, cc.winSize.height/2);
           
            skillsunJian.setPosition(cc.winSize.width/2, cc.winSize.height/2);


            self._delegate.runAction(cc.sequence(
                cc.delayTime(1.5),
                cc.callFunc(()=>{
                    self._delegate.addChild(money1, cf.Zoder.BULLET);
                   
                    self._delegate.addChild(skillsunJian, cf.Zoder.BULLET);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);
                   // this.CheckKill(10);
                }),
                cc.delayTime(4),
                cc.callFunc(()=>{
                    money1.removeFromParent();
                   
                    skillsunJian.removeFromParent();
                     
                   
                            

    
                })
            ))


        }
        

        
    },

    //龍珠鳳凰
    skill_GuanYu: function(msg){
        var self = this;
        var bgJson, bgAtlas;
        bgJson = Res.GAME.skill_bg_Json;
        bgAtlas = Res.GAME.skill_bg_Atlas;

        var GuanYuJson, GuanYuAtlas;
        GuanYuJson = Res.GAME.GuanYu_Json;
        GuanYuAtlas = Res.GAME.GuanYu_Atlas;

        var skillGuanYuJson, skillGuanYuAtlas;
        skillGuanYuJson = Res.GAME.skill_GuanYu_Json;
        skillGuanYuAtlas = Res.GAME.skill_GuanYu_Atlas;

        var skillmoney1Json, skillmoney1Atlas;
        skillmoney1Json = Res.GAME.skill_money1_Json;
        skillmoney1Atlas = Res.GAME.skill_money1_Atlas;

        // var commonRoom = cf.RoomMgr.getRoom();                  
        // var player = commonRoom._locPlayer;
        //根据座位id设置位置
     //   var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
      //  var convert = cf.convertSeatId(msg.nSeatID);
        cf.SoundMgr.playEffect(67);

        var money1 = ActionMgr.createSpine(skillmoney1Json,skillmoney1Atlas,1);
        money1.setAnimation(4, "GuanYu", false);


        var skillGuanYu = ActionMgr.createSpine(skillGuanYuJson,skillGuanYuAtlas,1);
        skillGuanYu.setAnimation(2, "GuanYu", false);

        var guan = ActionMgr.createSpine(GuanYuJson,GuanYuAtlas,1);
        guan.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        guan.setAnimation(1, "GuanYu1", false);
        guan.addAnimation(1, "GuanYu2", false);
        

        var bg =  ActionMgr.createSpine(bgJson,bgAtlas,1);
        bg.setAnimation(0, "GuanYu1", false);
       // bg.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        bg.addAnimation(0, "GuanYu2", false);
        

       // guan.setPosition(bg.width/2-800, bg.height/2-300);
       var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
        var convert = cf.convertSeatId(msg.nSeatID);

        
        
        if(self._locPlayer.getSeatID() == msg.nSeatID){
            self._delegate.addChild(guan, cf.Zoder.UI);

            guan.addChild(bg, cf.Zoder.BULLET);

            guan.runAction(cc.sequence(
                cc.delayTime(1.5),
                cc.callFunc(()=>{
                    guan.addChild(money1, cf.Zoder.BULLET);
                    guan.addChild(skillGuanYu, cf.Zoder.BULLET);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);

                   
                }),
                cc.delayTime(1),
                cc.callFunc(()=>{
                    var fishMgr = cf.FishMgr;
                    var fishMap = fishMgr.getFishMap();
                   
            // for(var a = 0; a < fishMap._elements.length; a ++){
    
            //     fishMap._elements[a].value.setFrozen(true);
            // }
    
                       // 动画结束事调用-- loopCount 循环的次数  traceIndex动画的索引  
                    this.CheckKill(cf.Data.ATTRBALLSKILL_DATA[4].hitRange);
                            

                    cf.dispatchEvent(BaseRoom.DoEvent,{
                        flag: BaseRoom.EventFlag.UPDATE_DRAGONSKILL_USED,
                        sender: 4,
                        seatId: msg.nSeatID
            
                    });
                }),
                cc.delayTime(4),
                cc.callFunc(()=>{
                    guan.removeFromParent();
                    
    
                })
            ))

        }else{
            money1.setPosition(cc.winSize.width/2, cc.winSize.height/2);
            skillGuanYu.setPosition(cc.winSize.width/2, cc.winSize.height/2);


            self._delegate.runAction(cc.sequence(
                cc.delayTime(1.5),
                cc.callFunc(()=>{
                    self._delegate.addChild(money1, cf.Zoder.BULLET);
                    self._delegate.addChild(skillGuanYu, cf.Zoder.BULLET);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);

                    //this.CheckKill(10);
                }),
                cc.delayTime(4),
                cc.callFunc(()=>{
                    money1.removeFromParent();
                    skillGuanYu.removeFromParent();
                     
                    
                })
            ))


        }
        
    },
    skill_XiahouDun: function(msg){
        var self = this;
        var bgJson, bgAtlas;
        bgJson = Res.GAME.skill_bg_Json;
        bgAtlas = Res.GAME.skill_bg_Atlas;

        var XiahouDunJson, XiahouDunAtlas;
        XiahouDunJson = Res.GAME.XiahouDun_Json;
        XiahouDunAtlas = Res.GAME.XiahouDun_Atlas;

        var skill_XiahouDunJson, skill_XiahouDunAtlas;
        skill_XiahouDunJson = Res.GAME.skill_XiahouDun_Json;
        skill_XiahouDunAtlas = Res.GAME.skill_XiahouDun_Atlas;

        var skillmoney1Json, skillmoney1Atlas;
        skillmoney1Json = Res.GAME.skill_money1_Json;
        skillmoney1Atlas = Res.GAME.skill_money1_Atlas;


        var textJson, textAtlas;
        textJson = Res.GAME.skill_txt_Json;
        textAtlas = Res.GAME.skill_txt_Atlas;

        var textlightJson, textlightAtlas;
        textlightJson = Res.GAME.skill_txt_light_Json;
        textlightAtlas = Res.GAME.skill_txt_light_Atlas;
        // var commonRoom = cf.RoomMgr.getRoom();                  
        // var player = commonRoom._locPlayer;
        //根据座位id设置位置
    //    var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
    //    var convert = cf.convertSeatId(msg.nSeatID);
        cf.SoundMgr.playEffect(65);

        var money1 = ActionMgr.createSpine(skillmoney1Json,skillmoney1Atlas,1);
        money1.setAnimation(4, "XiahouDun", false);

        

        var skillXiahouDun = ActionMgr.createSpine(skill_XiahouDunJson,skill_XiahouDunAtlas,1);
        skillXiahouDun.setAnimation(2, "XiahouDun", false);

    
        var XiahouDun = ActionMgr.createSpine(XiahouDunJson,XiahouDunAtlas,1);
        XiahouDun.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        XiahouDun.setAnimation(1, "01_XiahouDun", false);
        XiahouDun.addAnimation(1, "02_XiahouDun", false);

        var skillTextLight = ActionMgr.createSpine(textlightJson,textlightAtlas,1);
        skillTextLight.setAnimation(1, "02_XiahouDun", false);
       
        var skillText = ActionMgr.createSpine(textJson,textAtlas,1);
        skillText.setAnimation(1, "01_XiahouDun", false);
        skillText.addAnimation(1, "02_XiahouDun", false);

        var bg =  ActionMgr.createSpine(bgJson,bgAtlas,1);
        bg.setAnimation(0, "01_XiahouDun", false);
        bg.addAnimation(0, "02_XiahouDun", false);
        

       // guan.setPosition(bg.width/2-800, bg.height/2-300);
       var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
        var convert = cf.convertSeatId(msg.nSeatID);

        
        
        if(self._locPlayer.getSeatID() == msg.nSeatID){
            self._delegate.addChild(XiahouDun, cf.Zoder.UI);

            XiahouDun.addChild(bg, cf.Zoder.BULLET);
            XiahouDun.addChild(skillText);
            XiahouDun.runAction(cc.sequence(
                cc.delayTime(0.5),
                cc.callFunc(()=>{
                    XiahouDun.addChild(skillTextLight);
                }),
                cc.delayTime(1),
                cc.callFunc(()=>{
                    bg.removeFromParent();
                    XiahouDun.addChild(money1, cf.Zoder.BULLET);
                    XiahouDun.addChild(skillXiahouDun, cf.Zoder.BULLET);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);

                  
                }),
                cc.delayTime(1.5),
                cc.callFunc(()=>{
                    XiahouDun.removeFromParent();
                    var fishMgr = cf.FishMgr;
                    var fishMap = fishMgr.getFishMap();
                   
            // for(var a = 0; a < fishMap._elements.length; a ++){
    
            //     fishMap._elements[a].value.setFrozen(true);
            // }
                       // 动画结束事调用-- loopCount 循环的次数  traceIndex动画的索引  
                    this.CheckKill(cf.Data.ATTRBALLSKILL_DATA[1].hitRange);
                            

                    cf.dispatchEvent(BaseRoom.DoEvent,{
                        flag: BaseRoom.EventFlag.UPDATE_DRAGONSKILL_USED,
                        sender: 1,
                        seatId: msg.nSeatID
            
                    });
                })
            ))

       }else{
            money1.setPosition(cc.winSize.width/2, cc.winSize.height/2);
           
            skillXiahouDun.setPosition(cc.winSize.width/2, cc.winSize.height/2);


            self._delegate.runAction(cc.sequence(
                cc.delayTime(1.5),
                cc.callFunc(()=>{
                    self._delegate.addChild(money1, cf.Zoder.BULLET);
                    self._delegate.addChild(skillXiahouDun, cf.Zoder.BULLET);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);

                   // this.CheckKill(10);
                }),
                cc.delayTime(4),
                cc.callFunc(()=>{
                    money1.removeFromParent();
                   // money2.removeFromParent();
                    skillXiahouDun.removeFromParent();
                     
                    
                            

    
                })
            ))


       }

            
        
    },

    skill_ZhaoYun: function(msg){
        var self = this;
        var bgJson, bgAtlas;
        bgJson = Res.GAME.skill_bg_Json;
        bgAtlas = Res.GAME.skill_bg_Atlas;

        var ZhaoYunJson, ZhaoYunAtlas;
        ZhaoYunJson = Res.GAME.ZhaoYun_Json;
        ZhaoYunAtlas = Res.GAME.ZhaoYun_Atlas;

        var skillZhaoYunJson, skillZhaoYunAtlas;
        skillZhaoYunJson = Res.GAME.skill_ZhaoYun_Json;
        skillZhaoYunAtlas = Res.GAME.skill_ZhaoYun_Atlas;

        var skillmoney1Json, skillmoney1Atlas;
        skillmoney1Json = Res.GAME.skill_money1_Json;
        skillmoney1Atlas = Res.GAME.skill_money1_Atlas;

        // var commonRoom = cf.RoomMgr.getRoom();                  
        // var player = commonRoom._locPlayer;
        //根据座位id设置位置
       var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
       var convert = cf.convertSeatId(msg.nSeatID);
        cf.SoundMgr.playEffect(66);

        var money1 = ActionMgr.createSpine(skillmoney1Json,skillmoney1Atlas,1);
        money1.setAnimation(4, "ZhaoYun", false);


        var skillZhaoYun = ActionMgr.createSpine(skillZhaoYunJson,skillZhaoYunAtlas,1);
        skillZhaoYun.setAnimation(2, "ZhaoYun", false);

        var ZhaoYun = ActionMgr.createSpine(ZhaoYunJson,ZhaoYunAtlas,1);
        ZhaoYun.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        ZhaoYun.setAnimation(1, "ZhaoYun1", false);
        ZhaoYun.addAnimation(1, "ZhaoYun2", false);
        

        var bg =  ActionMgr.createSpine(bgJson,bgAtlas,1);
        bg.setAnimation(0, "ZhaoYun1", false);
       // bg.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        bg.addAnimation(0, "ZhaoYun2", false);
        
        
       // guan.setPosition(bg.width/2-800, bg.height/2-300);
       var seatId = cf.convertSeatId(self._locPlayer.getSeatID());
        var convert = cf.convertSeatId(msg.nSeatID);

        
     
        if(self._locPlayer.getSeatID() == msg.nSeatID){
            self._delegate.addChild(ZhaoYun, cf.Zoder.UI);

            ZhaoYun.addChild(bg, cf.Zoder.BULLET);

            ZhaoYun.runAction(cc.sequence(
                cc.delayTime(2.5),
                cc.callFunc(()=>{

                    ZhaoYun.addChild(skillZhaoYun, cf.Zoder.BULLET);
                    ZhaoYun.addChild(money1, cf.Zoder.BULLET);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);

                   
                }),
                cc.delayTime(0.8),
                cc.callFunc(()=>{
                    var fishMgr = cf.FishMgr;
                    var fishMap = fishMgr.getFishMap();
                   
           
    
                    // 动画结束事调用-- loopCount 循环的次数  traceIndex动画的索引  
                    this.CheckKill(cf.Data.ATTRBALLSKILL_DATA[2].hitRange);
                            

                    cf.dispatchEvent(BaseRoom.DoEvent,{
                        flag: BaseRoom.EventFlag.UPDATE_DRAGONSKILL_USED,
                        sender: 2,
                        seatId: msg.nSeatID
            
                    });

                }),
                cc.delayTime(3),
                cc.callFunc(()=>{
                    ZhaoYun.removeFromParent();
                 
    
                })
            ))

        }else{
            money1.setPosition(cc.winSize.width/2, cc.winSize.height/2);
            skillZhaoYun.setPosition(cc.winSize.width/2, cc.winSize.height/2);


            self._delegate.runAction(cc.sequence(
                cc.delayTime(1.5),
                cc.callFunc(()=>{
                    self._delegate.addChild(money1, cf.Zoder.BULLET);
                    self._delegate.addChild(skillZhaoYun, cf.Zoder.BULLET);
                    cf.EffectMgr.shakeRoom(4, 20, 0.05);

                   // this.CheckKill(10);
                }),
                cc.delayTime(3),
                cc.callFunc(()=>{
                    money1.removeFromParent();
                    skillZhaoYun.removeFromParent();
                     
                    
                            

    
                })
            ))


        }
            
        
    },
    /**
     * 確認技能炮
     */
    
    checkCanSkillFire: function(msg){
        var self = this;

        switch(msg.bulletType){
            case 0: //金
                //self.skill_ice();
                self.skill_sunJian(msg);
                cf.SkillfishBoolean = true;
            break;

            case 1://木
            self.skill_XiahouDun(msg);
            cf.SkillfishBoolean = true;
               // self.skill_ice();
            break;

            case 2://土
            self.skill_ZhaoYun(msg);
            cf.SkillfishBoolean = true;
            break;

            case 3://關羽
            self.skill_MaChao(msg);
            cf.SkillfishBoolean = true;
            break;

            case 4://關羽
            self.skill_GuanYu(msg);
            cf.SkillfishBoolean = true;
                
            break;

            case 5://集氣
                self.skill_shu(msg);
                cf.SkillfishBoolean = true;
            break;



        }
        
        
    },

    /**
     * 移除监听
     * @private
     */
    removeListener: function () {
        //移除网络监听
        var len = this._onNetMsgListener.length;
        for (var i = 0; i < len; ++i) {
            cf.removeListener(this._onNetMsgListener[i]);
        }
        cf.removeListener(this._doEventListener);
    },
    /**
     * 玩家进入消息处理
     * @param msg
     * @private
     */
    _resPlayerEnter: function (msg) {
        var resMsg = $root.SC_NewPlayerJoinRoomMsg.decode(msg);
        if (resMsg) {
            //cc.log("新玩家进场，座位id = " + resMsg.nSeatID);
            //同步服务器时间
            cf.PlayerMgr.getLocalPlayer().setGameTime(resMsg.strTime);
            cf.CanContinueAutoFire = true;
            var playerData = resMsg.playerInfoRoom[0];
            var player = this._roomPlayer[resMsg.nSeatID];
            if (player) {
                
                player.setPlayerModelData(playerData);
                player.setAlive(true);
                player.setRoomId(this._roomId);
                player.initData();
            }
        } else {
            cc.error("_resNewPlayerEnter错误:SC_NewPlayerJoinRoomMsg");
        }
    },

    /**
     * 解析破产并开启救济金的消息 只有本地玩家会接到救济金的消息
     * @param msg
     * @private
     */
    _resDoleCoolDown: function (msg) {
        var resMsg = $root.CS_DoleCoolDown_Res.decode(msg);
        if (resMsg) {
            //cc.log(resMsg._nDoleID, resMsg._nGold, resMsg._strCoolDownTime, resMsg._nDoleNum);
            var player = this._roomPlayer[resMsg.nSeatID];
            if (resMsg.nSeatID == cf.PlayerMgr.getLocalPlayer().getSeatID()) {
                //player.initDoleUi(resMsg);
            }
        } else {
            cc.error("_resDoleCoolDown错误:CS_DoleCoolDown_Res");
        }
    },

    /**
     * 解析领取救济金后服务器返回的消息
     * @param msg
     * @private
     */
    _resDoleGet: function (msg) {
        cf.UITools.hideLoadingToast();
        var resMsg = $root.CS_DoleGet_Res.decode(msg);
        if (resMsg) {
            var localSeatID = cf.PlayerMgr.getLocalPlayer().getSeatID();
            if (resMsg.nGold == 0) {
                //cc.log("冷却时间未到");
                this._roomPlayer[localSeatID].setCountTimeAndCountDownL(resMsg);
            }
            else {
                var player = this._roomPlayer[resMsg.nSeatID];
                if (localSeatID == resMsg.nSeatID) {
                    //本地玩家
                    player.setDoleUiState(false);
                    player.setStateWaitForGet(false);
                    //播放救济金特效
                    cf.EffectMgr.playDoleGold(player._curPos);
                }
                player.updatePlayerData(Player.DataType.GOLD, resMsg.nGold);
                player.refreshUI();
            }
        } else {
            cc.error("_resDoleGet错误:CS_DoleGet_Res");
        }
    },
    /**
     * 玩家离开消息处理
     * @param msg
     * @private
     */
    _resPlayerLeave: function (msg) {
        var resMsg = $root.SC_RemovePlayerFromRoomMsg.decode(msg);
        if (resMsg) {
            //cc.log("玩家退出，座位id = " + resMsg.nSeatID);

            //清空退出玩家子弹
            cf.BulletMgr.removeAllBullet(resMsg.nSeatID);
            //

            //清空座位数据
            this._roomPlayer[resMsg.nSeatID].clearData();
        }
    },

    /**
     * 玩家开火消息处理
     * @param msg SC_PlayerFireInRoomMsg
     * @private
     */
    _resPlayerOnFire: function (msg) {
        var resMsg = $root.SC_PlayerFireInRoomMsg.decode(msg);
        if (resMsg) {
            //cc.log("玩家开炮，座位id = " + resMsg.nSeatID);
            //this._bulletIndex = resMsg.vecBulletIndex[0];
            var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
            var player = this._roomPlayer[resMsg.nSeatID];

             if (localPlayerModel.getSeatID() != resMsg.nSeatID) {
                //其他玩家开炮
                player.onFireAfter(resMsg);
                
            }
            
           if(resMsg.bulletType >= 0){
               this._bulletIndex = resMsg.vecBulletIndex[0];  
                this.checkCanSkillFire(resMsg);

           }else{
            
            
            //排除本地玩家
            player.updatePlayerData(Player.DataType.GOLD, resMsg.nGoldNum);
            player.updatePlayerData(Player.DataType.FACC_Points, resMsg.fAccPoints);
            player.updatePlayerData(Player.DataType.EXP, resMsg.nExp);
            player.refreshUI();
            }
        
        } else {
            cc.error("_resPlayerOnFire错误:SC_PlayerFireInRoomMsg");
        }
    },


    /**
     * 玩家切换炮倍消息处理
     * @param msg
     * @private
     */
    _resPlayerModifyFort: function (msg) {
        var resMsg = $root.CS_ModifyFort_Res.decode(msg);
        if (resMsg) {
            //cc.log("玩家座位 id = " + resMsg.nSeatID + "，切换炮倍 = " + resMsg.nFortType);
            
            this._roomPlayer[resMsg.nSeatID].modifyFort(true, resMsg.nFortType);
        } else {
            cc.error("_resPlayerModifyFort错误:CS_ModifyFort_Res");
        }
    },

    /**
     * 解析玩家升级消息
     * @param msg
     * @private
     */
    _resPlayerLvUp: function (msg) {

        var resMsg = $root.SC_PlayerLevelUp.decode(msg);
        if (resMsg) {
            //cc.log("座位id：" + resMsg.nSeatID + ", 升级 = " + resMsg.nLevel);

            //更新玩家等级数据
            this._roomPlayer[resMsg.nSeatID].updatePlayerData(Player.DataType.LEVEL, resMsg.nLevel);
            this._roomPlayer[resMsg.nSeatID].updatePlayerData(Player.DataType.EXP, resMsg.nExp);

            //判断是否是本地玩家
            var seatID = cf.PlayerMgr.getLocalPlayer().getSeatID();
            if (resMsg.nSeatID == seatID) {

                if (resMsg.nLevel > cf.PlayerMgr.getLocalPlayer().getFortData()) {
                    if (resMsg.nLevel > 14) {
                        cf.PlayerMgr.getLocalPlayer().setFortData(20);
                    }
                    else {
                        cf.PlayerMgr.getLocalPlayer().setFortData(resMsg.nLevel);
                    }
                }
                //播放升级特效，加奖励
                cf.EffectMgr.playLevelUpEffect(resMsg);
            } else {
                this._roomPlayer[resMsg.nSeatID].updateRoleInfo();
            }

        } else {
            cc.error("_resPlayerLvUp错误:SC_PlayerLevelUp");
        }
    },
    /**
     * 玩家离开房间消息逻辑
     * @param msg
     * @private
     */
    _resLeaveRoom: function (msg) {
        var resMsg = $root.CS_LeaveRoomMsg_Res.decode(msg);
        MsgPool.getInstance().resetChatRecordRoom();
        if (resMsg) {
            if (resMsg.nRes == 0) {
                //判断下玩家的去向
                var leaveRoomInfo = cf.PlayerMgr.getLocalPlayer().getLeaveRoomInfo();
                switch (leaveRoomInfo.flag) {
                    case cf.SceneFlag.LOBBY:
                        cf.SceneMgr.runScene(cf.SceneFlag.LOBBY);
                        break;
                    case cf.SceneFlag.GAME_LOADING:
                        cf.dispatchEvent(GameScene.Reload, leaveRoomInfo.data);
                        break;
                }

            } else {
                cc.error("resLeaveRoom错误：CS_LeaveRoomMsg_Res");
                cf.SceneMgr.runScene(cf.SceneFlag.LOBBY);
            }
        }
    },

    /**
     * 玩家更换炮台皮肤
     * @param msg
     * @private
     */
    _resModifyFortSkin: function (msg) {
        cf.UITools.hideLoadingToast();
        var resMsg = $root.CS_ModifyFortSkin_Res.decode(msg);
        if (resMsg) {
            cc.log("更换炮台皮肤, 皮肤id = " + resMsg.nFortSkinID);

            //本地玩家
            var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
            if (localPlayerModel.getSeatID() == resMsg.nSeatID) {
            }

            var player = this._roomPlayer[resMsg.nSeatID];
            //更新炮台皮肤数据
            player.updatePlayerData(Player.DataType.SKIN, resMsg.nFortSkinID);
            player.modifyFortSkin();
        }
    },

    /**
     * 玩家解锁炮台皮肤
     * @param msg
     * @private
     */
    _resUnlockFortSkin: function (msg) {
        cf.UITools.hideLoadingToast();
        var resMsg = $root.CS_UnlockFortSkin_Res.decode(msg);
        if (resMsg) {
            var player = this._roomPlayer[resMsg.nSeatID];

            //本地玩家
            var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
            if (localPlayerModel.getSeatID() == resMsg.nSeatID) {
            }
            //更新炮台皮肤数据
            player.updatePlayerData(Player.DataType.SKIN, resMsg.nFortSkinID);
            player.updatePlayerData(Player.DataType.ADD_SKIN, resMsg.nFortSkinID);
            player.modifyFortSkin();

            //更新消耗
            var len = resMsg.itemReward.length;
            for (var i = 0; i < len; ++i) {
                var item = resMsg.itemReward[i];
                switch (item.nItemID) {
                    case cf.ItemID.GOLD://金币
                        player.updatePlayerData(Player.DataType.GOLD, item.nItemNum);
                        break;
                    case cf.ItemID.DIAMOND://钻石
                        player.updatePlayerData(Player.DataType.DIAMOND, item.nItemNum);
                        break
                }
            }
            //刷新UI
            player.refreshUI();
        }
    },

    _resModifyRoomInfo: function (msg) {
        //var resMsg = $root.SC_ModifyRoomInfo.decode(msg);
        //cc.error("服务器修正概率 fValue = " + resMsg.fValue);
    },

    /**
     * 生成鱼潮
     */
    _resCreateFishTide: function (msg) {
        var resMsg = $root.SC_FishTide.decode(msg);
        if (resMsg) {
            cf.FishMgr.createFishArrayByArrayId(resMsg);
        }
    },
    /**
     * 隐藏角色信息
     */
    hideRoleInfo: function () {
        for (var i = 0; i < 4; ++i) {
            var player = this._roomPlayer[i];
            if (player && !player.isLocalPlayer()) {
                player.showRoleInfo(false);
            }
        }
    },
    /**
     * 切换单双炮消息结果
     * @param msg
     * @private
     */
    _resModifyFortMod: function (msg) {
        var resMsg = $root.CS_ChangeFort_Res.decode(msg);
        var fortMod = resMsg.nFortId;
        var seatId = resMsg.nSeatID;
        if (fortMod == Player.FortMode.SINGLE) {
            this._roomPlayer[seatId].setFortModeSingle();
            if (seatId == this._locPlayer.getSeatID()) {
                this._delegate.getChildByTag(10005)._gunSkill.setSpriteFrame("fishery_032.png");
                this._delegate.getChildByTag(10005)._spEffect.setVisible(false);
            }
        }
        else {
            this._roomPlayer[seatId].setFortModeDouble();
            if (seatId == this._locPlayer.getSeatID()) {
                this._delegate.getChildByTag(10005)._gunSkill.setSpriteFrame("fishery_031.png");
                this._delegate.getChildByTag(10005)._spEffect.setVisible(true);
            }
        }
    },
    _resOutDateFort: function (msg) {

        var resMsg = $root.CS_OutDateFort.decode(msg);
        var fortMod = resMsg.nFortId;
        var seatId = resMsg.nSeatID;
        var nSinglePairCannon = resMsg.nSinglePairCannon;
        if (seatId == this._locPlayer.getSeatID()) {
            cf.PlayerMgr.getLocalPlayer().setSinglePairCannon(nSinglePairCannon);
            if (fortMod == Player.FortMode.SINGLE) {
                this._roomPlayer[seatId].setFortModeSingle();
                this._delegate.getChildByTag(10005)._gunSkill.setSpriteFrame("fishery_032.png");
                this._delegate.getChildByTag(10005)._spEffect.setVisible(false);
            }
            else {
                this._roomPlayer[seatId].setFortModeDouble();
                this._delegate.getChildByTag(10005)._gunSkill.setSpriteFrame("fishery_031.png");
                this._delegate.getChildByTag(10005)._spEffect.setVisible(true);
            }
            cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UPDATE_ROOM_UI);
        }
    },
    /**
     * 病毒JP接收
     * @param msg
     * @private
     */
    _resUpdateDragonJP: function(msg){
        
        var resMsg = $root.JP_Total_Res.decode(msg);
        var room = resMsg.roomType.indexOf(this._roomId);
       
        if(room != -1){
        var jpMoney = Math.floor(resMsg.JP[room]);
        
        if(this._previousNum != jpMoney){
            if(jpMoney >= 9999999999){
                this.setWinMoney(9999999999);
                this.runMoney();
            }else{
                this.setWinMoney(jpMoney);
                this.runMoney();
            }
            
        }

    }
        
    },
    /**
     * 房间事件解析
     * @param msg
     * @private
     */
    _doEvent: function (msg) {
        if (msg == null) {
            return;
        }
        //判断是否为结构
        var flag = msg;
        if (typeof msg == "object") {
            flag = msg.flag;
        }
        //显示商城，皮肤解锁，
        switch (flag) {
            case BaseRoom.EventFlag.UI_LOBBY://返回大厅
                this.stopSendMsg();
                cf.PlayerMgr.getLocalPlayer().leaveRoom({flag: cf.SceneFlag.LOBBY});
                break;
            case BaseRoom.EventFlag.UI_CONTINUEGAME:
                this._locPlayer.continueAutoFire();
                this._locPlayer.addPromptOfAutoFire();
                break;

            case BaseRoom.EventFlag.ERROR_LOCK_NOTIME:
                cf.SkillMgr.setLockSkillLastTime(this._locPlayer.getSeatID(), 0);
                break;

            case BaseRoom.EventFlag.ERROR_LOCK_NOFISH://锁定的鱼id不存在
                this._solveSkillProblemLockNoFish();
                break;

            case BaseRoom.EventFlag.UPDATE_ROOM_UI://刷新房间UI
                for (var i = 1; i < 6; i++) {
                    var skillIcon = this._delegate.getChildByTag(10000 + parseInt(i));
                    if (skillIcon) {
                        skillIcon._updateItemShowUi(i);
                    }
                }
                this._locPlayer.refreshUI();
                break;
            case BaseRoom.EventFlag.STOPAUTOFIRE:
                this._locPlayer.setBAutoFireState(false);
                break;
            case BaseRoom.EventFlag.CONTINUEAUTOFIRE:
                this._locPlayer.continueAutoFire();
                break;
            case BaseRoom.EventFlag.ERROR_FISHNUM_OUTOFLIMIT:
                this.promptOfOutOfLimit();
                break;
            case BaseRoom.EventFlag.UI_HELP:
                var helpLayer = this._delegate.getChildByTag(BaseRoom.EventFlag.UI_HELP);
                cf.layerHasShowInGame = true;
                if (helpLayer == null) {
                    helpLayer = new UIHelpLayer();
                    helpLayer.setTag(BaseRoom.EventFlag.UI_HELP);
                    this._delegate.addChild(helpLayer, cf.Zoder.UI);
                } else {
                    helpLayer.show();
                }
                if (this._locPlayer.getAutoFireState()) {
                    this._locPlayer.setBAutoFireState(false);
                    cf.CanContinueAutoFire = false;
                }
                break;
            case BaseRoom.EventFlag.UI_OPTION:
                var optionLayer = this._delegate.getChildByTag(BaseRoom.EventFlag.UI_OPTION);
                cf.layerHasShowInGame = true;
                if (!optionLayer) {
                    optionLayer = new UIOptionLayer();
                    optionLayer.setTag(BaseRoom.EventFlag.UI_OPTION);
                    this._delegate.addChild(optionLayer, cf.Zoder.BULLET);
                } else {
                    optionLayer.show();
                }
                if (this._locPlayer.getAutoFireState()) {
                    this._locPlayer.setBAutoFireState(false);
                    cf.CanContinueAutoFire = false;
                }
                break;
            //全服消息发送成功的提示
            case BaseRoom.EventFlag.SENDSUCCESSOFALLSERVERCHAT:
                cf.UITools.showHintToast(cf.Language.getText("text_1298"));
                break;
            case BaseRoom.EventFlag.SHOWSKILLUSEICON:
                var seatId = msg.seatId;
                var skillId = msg.skillId;
                this._roomPlayer[seatId].addSkillConsumeIcon(skillId);
                break;
            case BaseRoom.EventFlag.UPDATE_AUTO_UI:
                var autoBtn = this._delegate.getChildByTag(10006);
                if (autoBtn) {
                    var spFrame = autoBtn.getChildByTag(10001);
                    if (spFrame)
                        spFrame.setSpriteFrame("fishery_015.png");
                }
                break;
            case BaseRoom.EventFlag.EFFECT_GET_GOLD:
                //粒子特效
                var goldParticle = new cc.ParticleSystem(Res.LOBBY.get_gold_plist);
                goldParticle.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
                goldParticle.setAutoRemoveOnFinish(true);
                this._delegate.addChild(goldParticle, 100);
                cf.SoundMgr.playEffect(15);
                break;
            case BaseRoom.EventFlag.LOCKCOOL:
                var skillIcon = this._delegate.getChildByTag(10001);
                skillIcon.doCoolAction(cf.SkillId.lockSkill, 0);
                break;
            case BaseRoom.EventFlag.ICECOOL:
                var skillIcon = this._delegate.getChildByTag(10002);
                skillIcon.doCoolAction(cf.SkillId.iceSkill, 0);
                break;
            case BaseRoom.EventFlag.SUMMONCOOL:
                var skillIcon = this._delegate.getChildByTag(10003);
                skillIcon.doCoolAction(cf.SkillId.summonSkill, 0);
                break;
            case BaseRoom.EventFlag.RAGECOOL:
                var skillIcon = this._delegate.getChildByTag(10004);
                skillIcon.doCoolAction(cf.SkillId.rageSkill, 0);
                break;
            case BaseRoom.EventFlag.DOLEHASGETALLTIMES:
                if (this._locPlayer) {
                    this._locPlayer.doleHasGetAllTimes();
                }
                break;
            case BaseRoom.EventFlag.UPDATE_DRAGON_BALL_UI:
                if (this._uiDragonBall) {
                    this._uiDragonBall.updateUI(msg);
                }
                break;
            case BaseRoom.EventFlag.UPDATE_NET_DELAY:
                if(this._labelNetDelay){
                    if(cf.NetDelayTime > 500){
                        this._labelNetDelay.setColor(cc.color.RED);
                    }else if(cf.NetDelayTime > 200){
                        this._labelNetDelay.setColor(cc.color.YELLOW);
                    }else{
                        this._labelNetDelay.setColor(cc.color.GREEN);
                    }
                    this._labelNetDelay.setString(cf.NetDelayTime+"ms");
                }
                break;
               case BaseRoom.EventFlag.ACTIVITY_TIMEOUT_NOTICE:
                    var ruleNode = new cc.Scale9Sprite("resource_036_9.png");
                    ruleNode.setContentSize(600, 50);
                    ruleNode.setAnchorPoint(0.5,1);
                    ruleNode.setPosition(cc.winSize.width/2,cc.winSize.height/2+250);
                    this._delegate.addChild(ruleNode);

                    var label = new cc.LabelTTF( "每日活动即将结算，若有未领取的奖励请尽快领取喔", "YaHei", 23);
                    label.setAnchorPoint(0.5,0.5);
                    label.setPosition(ruleNode.width / 2,ruleNode.height / 2 );
                    ruleNode.addChild(label);

                    ruleNode.runAction(cc.sequence(
                        cc.repeat(cc.sequence(
                            cc.delayTime(2), 
                            cc.rotateTo(0.5,3), 
                            cc.rotateTo(0.5,-3),
                            cc.rotateTo(0.5,0),
                        ),4),

                            cc.callFunc( function() {
                                
                                this.removeFromParent();
                                //fish._bShowAttack = false;
                            },ruleNode)
                            
                    ));

               break;


                case BaseRoom.EventFlag.UPDATE_DRAGONSKILL_USED:
                    if (this._uiDragonBall){
                        
                        this._uiDragonBall.SkillUsed(msg);
                    }
                break;

                case BaseRoom.EventFlag.UPDATE_DRAGONENERGY:
                    if (this._uiDragonBall){
                        this._uiDragonBall.eneryShow(msg);
                    }
                break;
                
                case BaseRoom.EventFlag.RETRACK_DRAGONSKILL:
                    if (this._uiDragonBall){
                        
                        this._uiDragonBall._recheckDragonSkill();
                    }
                break;
                case BaseRoom.EventFlag.BOSS_COMING_BACKGROUND:

                   var map = this._mapNode.getChildByTag("spMap");
                //    if(spMap){
                //        spMap.removeFromParent();
                //    }else{
                       
                       var spMap = new cc.Sprite(Res.BossImage);
                       spMap.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
                       spMap.setScaleX(1.5);
                       spMap.setScaleY(1.5);
                       spMap.setOpacity(0);
                       this._mapNode.addChild(spMap);
                      // spMap.setTag("spMap");
                       var fadeOut = cc.fadeIn(2);
                       spMap.runAction(fadeOut);
                       

                 //  }
                   
                   

        }
    },
    
    /**
     * 获取房间内的服务器时间
     */
    getRoomServerTime: function () {
        return this._serverTime;
    },

    setRoomServerTime: function (serverTime) {
        this._serverTime = serverTime;
    },

    /**
     * 获取客户端服务器差值
     */
    getRoomCSTimeDif: function () {
        return this.csTimeDif;
    },

    setRoomCSTimeDif: function (timeDif) {
        this.csTimeDif = timeDif;
    },

    _skillNumUpdate: function (skillId, itemReward) {
        if ((!skillId && !itemReward) || itemReward.nError) {
            cc.error("道具数据获取出错");
        }
        var pNode = this._delegate.getChildByTag(10000 + skillId);
        var skillIcon = pNode.getChildByTag(10001);
        var itemId = itemReward.nItemID;
        var itemNum = itemReward.nItemNum;
        var itemEndTime = itemReward.strEndTime;
        //itemNum 只用来更新消耗的道具数量
        var player = this._locPlayer;
        //cc.log(itemNum + "使用");
        if (itemId != cf.ItemID.DIAMOND) {
            player.getPlayerModel().setPlayerItem(itemId, itemNum, itemEndTime, itemReward.nError);
        }
        var costLabel = skillIcon.getChildByTag(10001);
        //根据id进行判定当前技能消耗的道具是否为钻石 首先在技能卡道具数量为0时 自动将道具的消耗类型替换为钻石消耗
        if (itemId != cf.ItemID.DIAMOND) {
            if (cf.Data.ITEM_DATA[itemId] != undefined && cf.Data.ITEM_DATA[itemId + 10] != undefined) {
                //使用的是非绑定的道具
                //cc.log("使用的为非绑定道具");
                costLabel.setString("x" + itemNum);
            }
            else if (cf.Data.ITEM_DATA[itemId] != undefined && cf.Data.ITEM_DATA[itemId - 10] != undefined) {
                //使用的是绑定的道具 需要判定是否存在数量大于1的非绑定道具
                //cc.log("使用的为绑定道具");
                var cardNumUnBind = player.getPlayerModel().getPlayerItemNum(itemId - 10);
                if (itemNum == 0) {
                    costLabel.setString("x" + cardNumUnBind);
                }
                else {
                    costLabel.setString("x" + (cardNumUnBind + itemNum));
                }
            }
        }
    },
    promptOfCanNotUseLock: function () {
        cf.UITools.showHintToast(cf.Language.getText("text_1203"));
    },

    promptOfFortDataIsNotEnough: function () {
        cf.UITools.showHintToast(cf.Language.getText("text_1204"));
    },

    promptOfOutOfLimit: function () {
        cf.UITools.showHintToast(cf.Language.getText("text_1228"));
        cf.SkillMgr._summonSkillCall(this._locPlayer.getSeatID());
    },

    promptOfGoldOutRoom: function () {
        var self = this;
        self._hasToldGoldOutRoom = true;
        cf.layerHasShowInGame = true;
        var jumpRoomId = this._roomId + 1;
        var prompt = UIDialog.create("text_1463", "text_1098", "text_1099", false, function (type) {
            if (type == UIDialog.Btn.RIGHT) {
                if (cf.PlayerMgr.getLocalPlayer().getGoldNum() > cf.Data.ROOM_DATA[jumpRoomId].gold_limit[0]) {
                    if (!(cf.PlayerMgr.getLocalPlayer().getGoldNum() < cf.Data.ROOM_DATA[jumpRoomId].gold_limit[1])) {
                        jumpRoomId = parseInt(jumpRoomId) + 1;
                    }
                    var _roomData = {
                        roomId: jumpRoomId,
                        roomType: cf.Data.ROOM_DATA[jumpRoomId].room_type,
                        bReturn: false
                    };
                    self.stopSendMsg();
                    cf.PlayerMgr.getLocalPlayer().leaveRoom({flag: cf.SceneFlag.GAME_LOADING, data: _roomData});
                }
                else {
                    cf.UITools.showHintToast(cf.Language.getText("text_1558"));
                    self._hasToldGoldOutRoom = false;
                }
            }
            else if (type == UIDialog.Btn.LEFT) {
                cf.layerHasShowInGame = false;
            }
        }, this);
        this._delegate.addChild(prompt, 10);
    },

    /**
     * use skill error 733 fishServerIndexIsNotExist
     */
    _solveSkillProblemLockNoFish: function () {
        //自动创建一个本地逻辑 拿到玩家的vip等级 根据vip等级给出 模拟的锁定信息
        var player = this._locPlayer;
        var fish = player._lockObject;
        if (!player._lockObject) {
            return;
        }
        var normalSpeed = fish._originalDuration;
        var vipLevel = player.getPlayerModel().getVipLevel();
        var slowFactor = cf.Data.VIP_DATA[vipLevel].slowDown;
        var slowSpeed = normalSpeed / slowFactor;
        var lastTime = cf.Data.SKILLDATA[cf.SkillId.lockSkill].skill_last;
        if (lastTime >= 0) {
            cf.SkillMgr.setLockSkillLastTime(player.getSeatID(), lastTime);
        }
        player._countTimeForLockEff = 0;
        cc.log("733错误处理的技能持续时间" + lastTime + "当前的精灵速度" + slowSpeed);
        //移除精灵的锁定效果图标
        player.setLockElement(fish);
        cc.log("锁定技能生效");
        if (!fish.getIsFrozen()) {
            cc.log("非冰冻的情况下设置速度");
            fish.setActionSpeedChange(false, slowSpeed);
        }
        else {
            fish.setSlowFactor(slowFactor);
        }
    },
    /**
     * 在服务器停止恢复后暂停发送消息的操作
     */
    stopSendMsg: function () {
        var localPlayer = cf.PlayerMgr.getLocalPlayer();
        localPlayer.setStopSendMsg(true);
        if (cf.SkillMgr.getLockSkillState(localPlayer.getSeatID())) {
            cf.SkillMgr.stopLockSkill(localPlayer.getSeatID());
            //cc.log("停止锁定技能");
        }
        if (cf.SkillMgr.getRageSkillState(localPlayer.getSeatID())) {
            cf.SkillMgr.stopRageSkill(localPlayer.getSeatID());
            //cc.log("停止狂暴技能");
        }
        this._locPlayer.setBAutoFireState(false);
        this._locPlayer._bAutoFire = false;
        this._locPlayer._stopSendMsg = true;
    },

    /**
     * 关闭RoomUI(跳转)
     */
    closeRoomUI: function () {
    },

    getRoomPlayer: function (seatID) {
        if (this._roomPlayer)
            return this._roomPlayer[seatID];
        return null;
    },

    getRoomId: function () {
        return this._roomId;
    },

    /**
     * 振动
     * @param times 振动次数
     * @param moveOffset 振动偏移
     * @param amplitude 振动频率
     */
    shakeRoom: function (times, moveOffset, amplitude) {
       
        var action = this._mapNode.getActionByTag(88);
        if (action != null && !action.isDone()) {
            return;
        }

        var pos = this._mapNode.getPosition();
        var _shake = cc.sequence(
            cc.repeat(cc.sequence(
                cc.moveTo(amplitude, pos.x + moveOffset, pos.y),
                cc.moveTo(amplitude, pos.x - moveOffset, pos.y),
                cc.moveTo(amplitude, pos.x, pos.y + moveOffset),
                cc.moveTo(amplitude, pos.x, pos.y - moveOffset)
            ), times),
            cc.callFunc(function () {
                //这里的this是mapArray
                this.stopAllActions();
                this.setPosition(0, 0);
            }, this._mapNode)
        );
        _shake.setTag(88);
        this._mapNode.runAction(_shake);
    }
});

BaseRoom.DoEvent = "CommonRoom.DoEvent";
BaseRoom.EventFlag = {

    
    UI_HELP: 102,                       //< 显示帮助
    UI_OPTION: 103,                    //< 显示设置
    UI_LOBBY: 104,                     //< 显示大厅
    UI_CONTINUEGAME: 106,              //< 继续游戏
    

    ERROR_LOCK_NOTIME: 201,            //<技能时间超出
    ERROR_LOCK_NOFISH: 202,            //< 无法获取锁定技能的id
    ERROR_DIAMOND_NOTENOUGH: 203,      //< 钻石不足错误
    ERROR_SUMMON_OUTNUM: 204,          //< 召唤次数超出
    ERROR_TREASUREHUNT_OUTOFTIME: 205, //< 寻宝时间超时
    ERROR_FISHNUM_OUTOFLIMIT: 206,     //< 召唤鱼数量超出房间限制
    ERROR_CARDNUM_NOTENOUGH: 207,      //< 卡牌不满足条件


    UPDATE_ROOM_UI: 301,                 //< 刷新房间UI
    STOPAUTOFIRE: 302,                   //< 停止自动开炮
    CONTINUEAUTOFIRE: 303,               //< 继续自动开炮
    SENDSUCCESSOFALLSERVERCHAT: 304,     //< 发送全服消息成功
    SHOWSKILLUSEICON: 305,
    UPDATE_AUTO_UI: 306,                 //< 刷新自动开炮按钮
    EFFECT_GET_GOLD: 307,                //< 获得金币特效
    LOCKCOOL: 308,                    //  锁定执行冷却
    ICECOOL: 309,                     //  冰冻技能冷却
    SUMMONCOOL: 310,                  // 召唤技能冷却
    RAGECOOL: 311,                    // 锁定技能冷却
    DOLEHASGETALLTIMES: 312,          // 救济金次数已经全部完成 （玩家不在提升Vip的前提下，如果玩家提升Vip则重置）
    UPDATE_DRAGON_BALL_UI: 313,      //刷新龙珠UI
    UPDATE_NET_DELAY: 314,           //网络延迟
    ACTIVITY_TIMEOUT_NOTICE: 315,  //活動提示
    UPDATE_DRAGONSKILL_USED: 317, //龍珠技能使用
    UPDATE_DRAGONENERGY: 318, //集氣數量
    RETRACK_DRAGONSKILL: 319, //重新確認技能數量
    BOSS_COMING_BACKGROUND: 320,
    MAX: 999
};
