var RoomMgr = cc.Class.extend({
    _delegate: null,            //< 委托对象
    _bInit: false,              //<
    _room: null,                //< 房间对象
    _maxRoomNum: 0,             //< 房间大小

    /**
     * 初始化
     * @param delegate
     */
    init: function (delegate) {
        this._delegate = delegate;
    },

    /**
     * 资源释放
     */
    onClear: function () {
        if (this._room) {
            this._room.onClear();
            this._room = null;
        }
        this._bInit = false;
        this._delegate = null;
        this._maxRoomNum = 0;
    },

    /**
     * 创建房间
     * @param roomId 房间id
     * @param msg 房间信息 CS_JoinCommStageMsg_Res
     */
    createRoom: function (roomId, msg) {
        var self = this;

        //设置房间大小
        self.setMaxRoomNum(msg.nMaxNum);

        cf.CanContinueAutoFire = true;
        if (self._delegate) {
            //初始化房间
            //根据房间id，创建房间           
            if(roomId != 1005){
                self._room = new CommonRoom(self._delegate, roomId, msg);
                self._bInit = true;
            }
        }
    },

    /**
     * 逻辑更新
     * @param dt
     */
    updateLogic: function (dt) {
        if (this._bInit) {
            if (this._room) {
                this._room.updateLogic(dt);
            }
        }
    },

    onTouchesBegan: function (touches, event) {
        if (this._bInit) {
            if (this._room) {
                this._room.onTouchesBegan(touches, event);
            }
        }
    },

    onTouchesMoved: function (touches, event) {
        if (this._bInit) {
            if (this._room) {
                this._room.onTouchesMoved(touches, event);
            }
        }
    },

    onTouchesEnded: function (touches, event) {
        if (this._bInit) {
            if (this._room) {
                this._room.onTouchesEnded(touches, event);
            }
        }
    },

    onTouchesCancelled: function (touches, event) {
        if (this._bInit) {
            if (this._room) {
                this._room.onTouchesCancelled(touches, event);
            }
        }
    },

    /**
     * 得到房间大小
     * @returns {number}
     */
    getMaxRoomNum: function () {
        return this._maxRoomNum;
    },

    /**
     * 设置房间大小
     * @param val
     */
    setMaxRoomNum: function (val) {
        this._maxRoomNum = val;
    },

    getRoomPlayer: function (seatID) {
        if (this._room) {
            return this._room.getRoomPlayer(seatID);
        }
        return null;
    },

    getRoom: function () {
        if (this._room) {
            return this._room;
        }
        return null;
    },

    /**
     * 获取房间id
     * @returns {*}
     */
    getRoomId: function () {
        if (this._room) {
            return this._room.getRoomId();
        }
        return -1;
    },

    /**
     * 振动
     * @param times 振动次数
     * @param moveOffset 振动偏移
     * @param amplitude 振幅
     */
    shakeRoom:function (times,moveOffset,amplitude) {
        if(this._room){
            this._room.shakeRoom(times,moveOffset,amplitude);
        }
    }
});

/**
 * 房间单例对象
 * @type {undefined}
 * @private
 */
RoomMgr._sInstance = undefined;
RoomMgr.getInstance = function () {
    if (!RoomMgr._sInstance) {
        RoomMgr._sInstance = new RoomMgr();
    }
    return RoomMgr._sInstance;
};

/**
 * 移除房间单例
 */
RoomMgr.purgeRelease = function () {
    if (RoomMgr._sInstance) {
        RoomMgr._sInstance.onClear();
        RoomMgr._sInstance = null;
    }
};