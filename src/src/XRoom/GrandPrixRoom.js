var GrandPrixRoom = BaseRoom.extend({
    _integralLabel: null,
    _bulletLabel: null,

    /**
     * 构造方法
     * @param delegate 委托对象
     * @param roomId 房间id
     * @param msg 服务器消息 CS_JoinCommStageMsg_Res
     */
    ctor: function (delegate, roomId, msg) {
        this._super(delegate, roomId, msg);
    },

    /**
     * 释放资源
     */
    onClear: function () {
        this._super();
        //清空缓冲池
        cc.pool.drainAllPools();
    },
    /**
     * 逻辑更新
     * @param dt
     */
    updateLogic: function (dt) {
        this._super(dt);
        cf.FishMgr.updateLogic(dt);
    },

    /**
     * 初始化UI
     * @param msg
     */
    initUI: function (msg) {
        this._super(msg);
        //积分
        var integral = new cc.Sprite("#fishery_017.png");
        integral.setPosition(104, cc.winSize.height / 2);
        this._delegate.addChild(integral, cf.Zoder.UI);

        var jsonInfo = cf.SaveDataMgr.getNativeData("GrandPrixRank");
        var strIntegral = "0";
        var strBullet = "3000";
        if (jsonInfo) {
            //将字符串转化为json
            var jsonData = JSON.parse(jsonInfo);
            strIntegral = parseInt(jsonData.nIntegralNum) + "";
            strBullet = jsonData.nBulletNum + "";
        }
        var integralLabel = this._integralLabel = cc.LabelTTF.create(cf.Language.getText("text_1339") + strIntegral, cf.Language.FontName, 25);
        integralLabel.setFontFillColor(cc.color.YELLOW);
        integralLabel.setAnchorPoint(0, 0.5);
        integralLabel.enableStroke(cc.color.BLUE, 1.5)
        integralLabel.setPosition(cc.p(80, integral.height / 4 * 3));
        integral.addChild(integralLabel);

        //子弹数量
        /*var bullet = new cc.Sprite("#fishery_017.png");
         bullet.setPosition(104, cc.winSize.height / 2 - 50);
         this._delegate.addChild(bullet, cf.Zoder.UI);*/

        var bulletLabel = this._bulletLabel = cc.LabelTTF.create(cf.Language.getText("text_1340") + strBullet, cf.Language.FontName, 25);
        bulletLabel.setAnchorPoint(0, 0.5);
        bulletLabel.setFontFillColor(cc.color.YELLOW);
        bulletLabel.enableStroke(cc.color.BLUE, 1.5)
        bulletLabel.setPosition(cc.p(80, integral.height / 4));
        integral.addChild(bulletLabel);
    },

    /**
     * 添加监听
     * @private
     */
    addListener: function () {
        var self = this;
        this._super();

        //创建网络消息
        var msgDataArray = [
            //玩家捕鱼消息
            {msgId: MessageCode.CS_GrandPrixCatch_Res, callFunc: self._resPlayerCatch}
        ];

        //创建网络监听
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener.push(cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this)));
        }
    },

    /**
     * 移除监听
     * @private
     */
    removeListener: function () {
        this._super();
    },

    /**
     * 玩家捕鱼消息处理
     * @param msg
     * @private
     */
    _resPlayerCatch: function (msg) {
        var resMsg = $root.CS_GrandPrixCatch_Res.decode(msg);
        if (resMsg) {
            //cc.log("玩家座位：" + resMsg.nSeatID + ",捕鱼!!!!!!!!!!!");

            //刷新玩家金币信息，奖金信息
            var player = this._roomPlayer[resMsg.nSeatID];
            player.updatePlayerData(Player.DataType.GOLD, resMsg.nGoldNum);
            player.updatePlayerData(Player.DataType.EXP, resMsg.nExp);
            player.refreshUI();

            var localPlayer = cf.PlayerMgr.getLocalPlayer();

            //移除子弹
            if (localPlayer.getSeatID() != resMsg.nSeatID) {
                //子弹张网
                cf.BulletMgr.removeBullet(resMsg.nSeatID, resMsg.strBulletID);
            } else {
                this._integralLabel.setString(cf.Language.getText("text_1339") + parseInt(resMsg.nIntegral));
                this._bulletLabel.setString(cf.Language.getText("text_1340") + resMsg.nBulletNum);

                var jsonInfo = cf.SaveDataMgr.getNativeData("GrandPrixRank");
                if (jsonInfo) {
                    //将字符串转化为json
                    var jsonData = JSON.parse(jsonInfo);
                    jsonData.nIntegralNum = resMsg.nIntegral;
                    jsonData.nBulletNum = resMsg.nBulletNum;
                    //存入本地
                    cf.SaveDataMgr.setNativeData("GrandPrixRank", JSON.stringify(jsonData));
                }
            }

            var fishLength = resMsg.listFish.length;
            if (fishLength < 0) {
                return;
            }

            //如果有鱼被捕捉，执行鱼死亡动画
            //道具奖励在鱼死亡中实现了
            cf.FishMgr.removeFish(resMsg);
        } else {
            cc.error("_resPlayerCatch错误:CS_PlayerCatch_Res");
        }
    },

    /**
     * 房间事件解析
     * @param msg
     * @private
     */
    _doEvent: function (msg) {
        this._super(msg);
    }
});