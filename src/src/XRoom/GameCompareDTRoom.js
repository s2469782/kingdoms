//等待状态清零
var GameCompareDTRoom = cc.Class.extend({
    _delegate: null,                //< 委托对象
    _roomMsg: null,                     //< 进入房间的消息
    _historyNum: 0,                  //< 历史记录 超过10则重置为0
    _ownerState: 0,                  //< 玩家上庄状态 0未上庄 1等待上庄 2上庄中需要在广播的上庄中进行判定
    _chipState: 0,                   //< 押注状态 0：初始状态 1：龙 2：和 3：虎
    _gameState: 0,                   //< 游戏状态 0 等待 1下注阶段 2等待开牌 3开牌结算
    _shangzhuanglist: null,         //< 玩家上庄列表
    _countTime: 0,                   //< 倒计时时间
    _locTime: 0,
    _listIsOn: false,               //< 列表是否开启
    _showChipsArray: [],
    _showChipsArrayOther: [],
    _chipsCount: 0,                  //< 下注计时
    _prompt:null,                   //<  玩家奖励提示 为了仅显示最新结果
    _localPlayerIsBanker: false,    //< 本地玩家正处于上庄状态
    _timeDelayCount:0,               //< 添加音效延时

    ctor: function (delegate, roomId, msg) {
        this._delegate = delegate;
        this._roomMsg = msg;
        this._gameState = this._roomMsg.nState;
        this._showChipsArray = [];
        this._showChipsArrayOther =[];
        this._minBankChipsNum = this._roomMsg.nShangZhuangMin;
        var presentTime = (new Date()).getTime();
        this._countTime = Math.round((this._roomMsg.strEndTime - presentTime) / 1000);
        cf.PlayerMgr.getLocalPlayer().setPlayerChipNum(this._roomMsg.dChipNum);
        cf.PlayerMgr.getLocalPlayer().setGoldNum(this._roomMsg.dChipNum);

        //添加网络监听
        this.addListener();
        //初始化UI
        this.initUI(roomId,msg);
    },

    updateLogic: function(dt) {
        if(this._countTime > 0) {
            this._locTime += dt;
            if(this._locTime > 1) {
                this._locTime = 0;
                this._countTime -= 1;
                this._countTimePrompt.setString(this._countTime);
            }
        }
        else {
            this._countTime = 0;
            this._countTimePrompt.setString(this._countTime);
        }
    },

    initUI: function(roomId,msg) {
        //创建背景
        var self = this;
        //设置纹理格式

        var spMap = self._spBk = new cc.Sprite("#gameDt_006.png");
        spMap.setPosition(cc.winSize.width/2,cc.winSize.height/2);
        self._delegate.addChild(spMap,100);

        var dragon = new cc.Sprite("#gameDt_018.png");
        dragon.setPosition(340,spMap.height - 100);
        spMap.addChild(dragon);

        var tiger = new cc.Sprite("#gameDt_016.png");
        tiger.setPosition(spMap.width - 360,spMap.height - 100);
        spMap.addChild(tiger);

        var table = new cc.Sprite("#gameDt_007.png");
        table.setPosition(spMap.width / 2,360);
        spMap.addChild(table);

        if(self._gameState == 3) {
            self._card1 = new BaseCard(self._roomMsg.nDragonPokerHua + 1,self._roomMsg.nDragonPoker);
            self._card1.setPosition(spMap.width / 2 - 130,spMap.height - 100);
            self._card1.turnCard(0);
            spMap.addChild(self._card1);

            self._card2 = new BaseCard(self._roomMsg.nTigerPokerHua + 1,self._roomMsg.nTigerPoker);
            self._card2.setPosition(spMap.width / 2 + 130,spMap.height - 100);
            self._card2.turnCard(0);
            spMap.addChild(self._card2);
        }
        else {
            self._card1 = new BaseCard(1,13);
            self._card1.setPosition(spMap.width / 2 - 130,spMap.height - 100);
            spMap.addChild(self._card1);

            self._card2 = new BaseCard(1,13);
            self._card2.setPosition(spMap.width / 2 + 130,spMap.height - 100);
            spMap.addChild(self._card2);
        }

        var statePrompt = self._statePrompt = new ccui.Text("等待中", cf.Language.FontName, 18);
        statePrompt.setPosition(spMap.width / 2 - 50,spMap.height - 160);
        statePrompt.setAnchorPoint(0,0.5);
        statePrompt.setTextColor(cf.TitleColor.FONT_2);
        statePrompt.enableOutline(cf.TitleColor.OUT_2, 1);
        spMap.addChild(statePrompt);

        var countTimePrompt = self._countTimePrompt = new ccui.Text(this._countTime, cf.Language.FontName, 18);
        countTimePrompt.setPosition(spMap.width / 2 + 20,spMap.height - 160);
        countTimePrompt.setAnchorPoint(0,0.5);
        countTimePrompt.setTextColor(cf.TitleColor.FONT_2);
        countTimePrompt.enableOutline(cf.TitleColor.OUT_2, 1);
        spMap.addChild(countTimePrompt);

        self.initBtn();
        self.initRoleInfo();
        self.initChipLabel();
        self.initRoleInfoBanker();
        self.initHistoryIcon();

        //游戏状态 0 等待 1下注阶段 2等待开牌 3开牌结算
        switch (this._gameState) {
            case 0:
                this._statePrompt.setString("等待中");
                break;
            case 1:
                if(this._ownerState != 2) {
                    this.setFakeBtnState(true,false);
                }
                this._statePrompt.setString("请下注");
                break;
            case 2:
                this._statePrompt.setString("待开牌");
                break;
            case 3:
                this._statePrompt.setString("结算中");
                break;
        }
    },

    initRoleInfoBanker: function () {
        var self = this;
        var headData = cf.Data.HEADPORTRAIT_DATA;
        var goldNum = this._roomMsg.dZJChipNum;
        var nickName = this._roomMsg.strZJNickname;

        //默认头像为0
        var nHeadID = this._roomMsg.nZJHeadID;

        var tempSp = self._roleHead = new cc.Scale9Sprite(ActionMgr.getFrame("resource_103.png"));
        tempSp.setPosition(self._spBk.width / 2 - 10,self._spBk.height - 60);
        tempSp.setScale(0.4);
        self._spBk.addChild(tempSp);

        var roleIcon = self._roleIcon = new cc.Sprite("#" + headData[nHeadID].headIcon);
        roleIcon.setPosition(tempSp.width / 2,tempSp.height / 2);
        tempSp.addChild(roleIcon);

        var tempSpNameBg = new cc.Sprite("#gameDt_011.png");
        tempSpNameBg.setPosition(tempSp.width / 2 + 30,-60);
        tempSp.addChild(tempSpNameBg,-1);

        self._bankerGoldLabel = new cc.LabelBMFont(goldNum.toString(), Res.LOBBY.font_lh);
        self._bankerGoldLabel.setAnchorPoint(0,0.5);
        self._bankerGoldLabel.setPosition(80, 20);
        tempSpNameBg.addChild(self._bankerGoldLabel);

        self._nickName = new ccui.Text(nickName, cf.Language.FontName, 30);
        self._nickName.setPosition(80,60);
        self._nickName.setAnchorPoint(0,0.5);
        self._nickName.setTextColor(cf.TitleColor.FONT_2);
        self._nickName.enableOutline(cf.TitleColor.OUT_2, 2);
        tempSpNameBg.addChild(self._nickName);

        self._bossTime = new ccui.Text(this._roomMsg.nZuoZhuangNum + " / 10", cf.Language.FontName, 40);
        self._bossTime.setPosition(120,-30);
        self._bossTime.setAnchorPoint(0,0.5);
        self._bossTime.setTextColor(cf.TitleColor.FONT_2);
        self._bossTime.enableOutline(cf.TitleColor.OUT_2, 2);
        tempSpNameBg.addChild(self._bossTime);
    },

    initRoleInfo: function () {
        var self = this;
        var headData = cf.Data.HEADPORTRAIT_DATA;
        var locationPlayer = cf.PlayerMgr.getLocalPlayer();
        var goldNum = locationPlayer.getPlayerChipNum();
        var nickName = locationPlayer.getNickName();

        //默认头像为0
        var nHeadID = locationPlayer.getHeadId();

        var tempSp = new cc.Scale9Sprite(ActionMgr.getFrame("resource_103.png"));
        tempSp.setPosition(340,60);
        tempSp.setScale(0.8);
        self._spBk.addChild(tempSp);

        var roleIcon = new cc.Sprite("#" + headData[nHeadID].headIcon);
        roleIcon.setPosition(tempSp.width / 2,tempSp.height / 2);
        tempSp.addChild(roleIcon);

        var tempSpNameBg = new cc.Sprite("#gameDt_011.png");
        tempSpNameBg.setPosition(330,30);
        tempSp.addChild(tempSpNameBg,-1);

        self._goldLabel = new cc.LabelBMFont(goldNum.toString(), Res.LOBBY.font_lh);
        self._goldLabel.setAnchorPoint(0,0.5);
        self._goldLabel.setPosition(80, 20);
        tempSpNameBg.addChild(self._goldLabel);

        var nickName = new ccui.Text(nickName, cf.Language.FontName, 30);
        nickName.setPosition(100,60);
        nickName.setAnchorPoint(0,0.5);
        nickName.setTextColor(cf.TitleColor.FONT_2);
        nickName.enableOutline(cf.TitleColor.OUT_2, 2);
        tempSpNameBg.addChild(nickName);
    },

    //初始化历史记录
    initHistoryIcon: function () {
        this._historyIconArray = [];
        var length = this._roomMsg.listRoadShow.length;
        for(var i = 0;i < length;i++) {
            // 0和，1龙，2虎
            var iconName = "#gameDt_015.png";
            if(this._roomMsg.listRoadShow[i]) {
                switch (this._roomMsg.listRoadShow[i]) {
                    case 0:
                        iconName = "#gameDt_020.png";
                        break;
                    case 1:
                        iconName = "#gameDt_017.png";
                        break;
                    case 2:
                        iconName = "#gameDt_015.png";
                        break;
                }
            }
            else {
                iconName = "#gameDt_015.png";
            }
            var localIcon = new cc.Sprite(iconName);
            localIcon.setPosition(150 + 70*i,190);
            this._spBk.addChild(localIcon);
            if(i == length) {
                localIcon.setOpacity(0);
            }
            this._historyIconArray.push(localIcon);
        }
    },

    //执行记录变动动画
    doHistoryMoveAciton:function (result) {
        if(this._historyIconArray.length > 15) {
            var iconName;
            switch (result) {
                case 0:
                    iconName = "gameDt_020.png";
                    break;
                case 1:
                    iconName = "gameDt_017.png";
                    break;
                case 2:
                    iconName = "gameDt_015.png";
                    break;
            }
            this._historyIconArray[this._historyIconArray.length - 1].setSpriteFrame(iconName);
            for(var i in this._historyIconArray) {
                if(i == 0) {
                    this._historyIconArray[i].runAction(cc.sequence(cc.moveBy(0.5,cc.p(-70,0)),cc.fadeOut(1),cc.callFunc(function () {
                        this._historyIconArray[this._historyIconArray.length - 1].setPosition(150 + (this._historyIconArray.length - 1)*70,190);
                    },this)));
                }
                else if(i == this._historyIconArray.length - 1){
                    this._historyIconArray[i].runAction(cc.sequence(cc.moveBy(0.5,cc.p(-70,0)),cc.fadeIn(1)));
                }
                else {
                    this._historyIconArray[i].runAction(cc.moveBy(0.5,cc.p(-70,0)));
                }
            }
            var temp = this._historyIconArray[0];
            this._historyIconArray.splice(0,1);
            this._historyIconArray.push(temp);
        }
        else {
            if(this._historyIconArray.length == 15) {
                var localIcon = new cc.Sprite("#gameDt_015.png");
                localIcon.setPosition(150 + 70*(this._historyIconArray.length),190);
                this._spBk.addChild(localIcon);
                localIcon.setOpacity(0);
                this._historyIconArray.push(localIcon);
                this.doHistoryMoveAciton(result);
            }
            else {
                var iconName;
                switch (result) {
                    case 0:
                        iconName = "#gameDt_020.png";
                        break;
                    case 1:
                        iconName = "#gameDt_017.png";
                        break;
                    case 2:
                        iconName = "#gameDt_015.png";
                        break;
                }
                var localIcon = new cc.Sprite(iconName);
                this._historyIconArray.push(localIcon);
                localIcon.setPosition(150 + 70*(this._historyIconArray.length - 1),190);
                this._spBk.addChild(localIcon);

                if(this._historyIconArray.length == 15) {
                    var localIcon = new cc.Sprite("#gameDt_015.png");
                    localIcon.setPosition(150 + 70*(this._historyIconArray.length),190);
                    this._spBk.addChild(localIcon);
                    localIcon.setOpacity(0);
                    this._historyIconArray.push(localIcon);
                }
            }
        }
    },

    initBtn: function () {
        var self = this;
        var backBtn = new ccui.Button("gameDt_013.png",null,null,1);
        backBtn.setPosition(80,self._spBk.height - 60);
        self._spBk.addChild(backBtn);
        backBtn.addClickEventListener(function () {
            //退出房间
            if(self._ownerState == 2) {
                var prompt = UIDialog.createCustom("您当前为庄家\n下庄后才可退出游戏。", cf.Language.getText("text_1098"), null, false, function (type) {
                    if (type == UIDialog.Btn.RIGHT) {

                    }
                }, self);
                self._delegate.addChild(prompt, 101);
            }
            else if(self._setChipNum1.getString() > 0 || self._setChipNum2.getString() > 0 || self._setChipNum3.getString() > 0) {
                self.backGamePrompt();
            }
            else {
                cf.PlayerMgr.getLocalPlayer().leaveRoom({flag: cf.SceneFlag.LOBBY});
            }
        });

        var detaileBtn = new ccui.Button("gameDt_014.png",null,null,1);
        detaileBtn.setPosition(self._spBk.width - 80,self._spBk.height - 50);
        self._spBk.addChild(detaileBtn);
        detaileBtn.addClickEventListener(function () {
           //显示规则
            self.initGameDetailPrompt();
        });

        var exchangeBtn = new ccui.Button("gameDt_012.png",null,null,1);
        exchangeBtn.setPosition(self._spBk.width - 80,self._spBk.height - 140);
        self._spBk.addChild(exchangeBtn);
        exchangeBtn.addClickEventListener(function () {
            //获取兑换数据
            if (cf.PlayerMgr.getLocalPlayer().getExchangeInfo() == null) {
                var playerModel = cf.PlayerMgr.getLocalPlayer();
                var updateTime = playerModel.getUpdateExchangeTime();
                var msg = new $root.CH_AskExchangeInfoMsg();
                msg.nUpdateTime = updateTime;
                var wr = $root.CH_AskExchangeInfoMsg.encode(msg).finish();
                cf.NetMgr.sendSocketMsg(MessageCode.CH_AskExchangeInfoMsg, wr);
            }
            //延时0.1秒获取兑换数据时间
            setTimeout(function () {
                if (cf.PlayerMgr.getLocalPlayer().getExchangeInfo() != null) {
                    cf.layerHasShowInGame = true;
                    var Layer = self._delegate.getChildByTag(10001);
                    if (Layer == null) {
                        Layer = new UIExchangeLayer(0);
                        self._delegate.addChild(Layer, 100, 10001);
                    } else {
                        Layer.show();
                    }
                } else {
                    cf.UITools.showHintToast(cf.Language.getText("text_1119"));
                }
            }, 100);
        });

        //上庄按钮
        var becomeOwnerBtn = self._becomeOwnerBtn = new ccui.Button("gameDt_019.png",null,null,1);
        becomeOwnerBtn.setPosition(150,50);
        becomeOwnerBtn.setTitleOffset(0,5);
        becomeOwnerBtn.setTitleText("上  庄");
        becomeOwnerBtn.setTitleFontName(cf.Language.FontName);
        becomeOwnerBtn.setTitleFontSize(40);
        self._spBk.addChild(becomeOwnerBtn);
        becomeOwnerBtn.addClickEventListener(function () {
            if(cf.PlayerMgr.getLocalPlayer().getPlayerChipNum() >= self._minBankChipsNum) {
                //上庄按钮
                switch (self._ownerState) {
                    case 0:
                        if(!self._localPlayerIsBanker) {
                            self.playerShangZhuang();
                        }
                        break;
                    case 1:
                        //展示上庄列表
                        if(!self._listIsOn) {
                            self.askPlayerShangZhuangList();
                        }
                        break;
                    case 2:
                        self.playerBets();
                        break;
                }
            }
            else {
                //提示您的筹码不足无法上庄
                var prompt = UIDialog.createCustom("上庄所需筹码数："+ self._minBankChipsNum + "\n您当前筹码数不足,请您兑换。", cf.Language.getText("text_1098"), null, false, function (type) {
                    if (type == UIDialog.Btn.RIGHT) {

                    }
                }, self);
                self._delegate.addChild(prompt, 101);
            }
        });

        var chipNumArr = [1,5,20,50,100,300];
        for(var i = 0;i < 6;i++) {
            var chipBtn = new ccui.Button("gameDt_00"+ (i) +".png",null,null,1);
            chipBtn.setPosition(self._spBk.width / 2 + 90 + 100 * i,50);
            self._spBk.addChild(chipBtn);
            chipBtn.setTag(i);
            chipBtn.setScale(0.75 + 0.05*i);
            chipBtn.addTouchEventListener(self.menuItemCallback, self);

            var chipNum =  new cc.LabelBMFont(chipNumArr[i]+"万", Res.LOBBY.font_lh);
            chipNum.setPosition(chipBtn.width / 2, chipBtn.height / 2);
            chipNum.setScale(0.8);
            chipBtn.addChild(chipNum);
        }

        //龙虎和按钮
        //var particlePos = [[cc.p(420, 285),cc.p(190, 235)],[cc.p(760, 285),cc.p(530, 235)],[cc.p(1100, 285),cc.p(870, 235)]];
        var frameIcon = ["#gameDt_022.png","#gameDt_023.png","#gameDt_024.png"];
        var framePos = [cc.p(300,370),cc.p(640,370),cc.p(980,370)];
        for(var index = 1;index < 4;index++) {
            var frame = new cc.Sprite(frameIcon[index - 1]);
            frame.setPosition(framePos[index - 1]);
            frame.setTag(20000 + index);
            frame.setVisible(false);
            self._spBk.addChild(frame);

            var fakeBtn = new ccui.Button("resource_036_9.png",null,null,1);
            fakeBtn.setScale9Enabled(true);
            fakeBtn.setContentSize(320,230);
            fakeBtn.setPosition(300+340 * (index- 1),self._spBk.height / 2 - 20);
            self._spBk.addChild(fakeBtn);
            fakeBtn.setOpacity(0);
            fakeBtn.setTouchEnabled(false);
            fakeBtn.setTag(10000 + index);
            fakeBtn.addClickEventListener(function () {
                cc.log("_chipState");
                self._chipState = this.getTag() - 10000;
                self.setFakeBtnState(true,false);
                var localFrame = self._spBk.getChildByTag(this.getTag() + 10000);
                localFrame.setVisible(true);
                // self._particleEffect[0].stopAllActions();
                // self._particleEffect[1].stopAllActions();
                // self._particleEffect[0].setPosition(particlePos[self._chipState - 1][0]);
                // self._particleEffect[1].setPosition(particlePos[self._chipState - 1][1]);
                // self._particleEffect[0].runAction(cc.repeatForever(ActionMgr.createSquare(2, 1, 235, 50)));
                // self._particleEffect[1].runAction(cc.repeatForever(ActionMgr.createSquare(2, 1, -230, -50)));
                // self._particleEffect[0].resetSystem();
                // self._particleEffect[1].resetSystem();
            });
        }

        //LOG关闭按钮
        // var logBtn = new ccui.Button("button_006.png", null, null, 1);
        // this._delegate.addChild(logBtn, 101);
        // logBtn.setScale(0.8);
        // logBtn.setAnchorPoint(0, 0.5);
        // logBtn.setPosition(0, cc.winSize.height / 2 - 200);
        // logBtn.setTitleText("关闭LOG");
        // logBtn.setTitleFontSize(30);
        // logBtn.setTag(100000);
        // logBtn.setVisible(false);
        // logBtn.setTitleFontName(cf.Language.FontName);
        // logBtn.addClickEventListener(function () {
        //     if(self._listViewLog){
        //         if(self._listViewLog.isVisible()){
        //             self._listViewLog.setVisible(false);
        //             logBtn.setTitleText("打开LOG");
        //         }
        //         else{
        //             self._listViewLog.setVisible(true);
        //             logBtn.setTitleText("关闭LOG");
        //         }
        //     }
        // });

        //玩家列表
        var playerBtn = new ccui.Button();
        playerBtn.loadTextureNormal("playerListIcon.png", ccui.Widget.PLIST_TEXTURE);
        playerBtn.setPosition(self._spBk.width - 60, 200);
        playerBtn.setPressedActionEnabled(true);
        playerBtn.addClickEventListener(function () {
            if(!self._listIsOn) {
                self.askPlayerList();
            }
        });
        self._spBk.addChild(playerBtn,1);
    },

    setFakeBtnState: function (state1,state2) {
        for(var i = 1;i < 4;i++) {
            var fakeBtn = this._spBk.getChildByTag(10000 + i);
            if(fakeBtn) {
                fakeBtn.setTouchEnabled(state1);
                var localFrame = this._spBk.getChildByTag(20000 + i);
                localFrame.setVisible(state2);
            }
        }
    },

    initGameDetailPrompt: function () {
        var self = this;
        if(self._detaileBg) {
            return;
        }
        var detaileStr = "游戏规则:\n1.除大小王外共52张牌,每局只派两张,即龙、虎每门各派一张牌\n2.双方比大小,最大为K,最小为A  \nK > Q > J > 10 > 9 > 8 > 7 > 6 > 5 > 4 > 3 > 2 > A\n3.每局开牌千玩家可投注龙、虎、和三门\n\n\n投注规则:\n1.龙局：1赔1(和局全退)\n2.虎局：1赔1(和局全退)\n3.和局：1赔5";
        //添加背景
        var detaileBg = self._detaileBg = new cc.Scale9Sprite("resource_036_9.png");
        detaileBg.setContentSize(580, 420);
        detaileBg.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        this._spBk.addChild(detaileBg,101);

        var titleLabel = new ccui.Text(detaileStr, cf.Language.FontName, 20);
        titleLabel.setPosition(detaileBg.width / 2, detaileBg.height / 2);
        titleLabel.setTextColor(cf.TitleColor.FONT_2);
        titleLabel.enableOutline(cf.TitleColor.OUT_2, cf.TitleColor.SIZE - 2);
        detaileBg.addChild(titleLabel);

        //关闭按钮
        var closeBtn = new ccui.Button();
        closeBtn.loadTextureNormal("resource_021.png", ccui.Widget.PLIST_TEXTURE);
        closeBtn.setPosition(detaileBg.width - 20, detaileBg.height - 20);
        closeBtn.setPressedActionEnabled(true);
        closeBtn.setScale(0.6);
        closeBtn.addClickEventListener(function () {
            detaileBg.removeFromParent();
            self._detaileBg = null;
        });
        detaileBg.addChild(closeBtn);
    },

    showListOfShangZhuang: function (listZhuangJia,type) {
        var self = this;
        var caseBg = new cc.Scale9Sprite("resource_007_9.png");
        caseBg.setPosition(cc.winSize.width / 2,cc.winSize.height / 2);
        caseBg.setContentSize(620,450);
        this._spBk.addChild(caseBg , 2);

        self._listIsOn = true;

        var listView = this._listView = new ccui.ListView();
        listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        listView.setTouchEnabled(true);
        listView.setBounceEnabled(true);
        listView.setContentSize(cc.size(600, 430));
        listView.setPosition(10, 5);
        listView.setGravity(ccui.ListView.GRAVITY_CENTER_VERTICAL);

        //根据服务器数据填充列表，根据领取标记排序
        listView.setItemsMargin(4);
        caseBg.addChild(listView);

        //关闭按钮
        var closeBtn = new ccui.Button();
        closeBtn.loadTextureNormal("resource_021.png", ccui.Widget.PLIST_TEXTURE);
        closeBtn.setPosition(caseBg.width - 20, caseBg.height - 20);
        closeBtn.setPressedActionEnabled(true);
        closeBtn.setScale(0.6);
        closeBtn.addClickEventListener(function () {
            caseBg.removeFromParent();
            self._listIsOn = false;
        });
        caseBg.addChild(closeBtn);

        for(var i in listZhuangJia) {
            var item = this.createShangZhuangItem(listZhuangJia[i]);
            if (item) {
                listView.pushBackCustomItem(item);
            }
        }

        if(type == 0) {
            var cancelBtn = new ccui.Button();
            cancelBtn.loadTextureNormal("gameDt_019.png", ccui.Widget.PLIST_TEXTURE);
            cancelBtn.setPosition(caseBg.width / 2,  - 20);
            cancelBtn.setPressedActionEnabled(true);
            cancelBtn.setTitleOffset(0,5);
            cancelBtn.setTitleText("取消上庄");
            cancelBtn.setTitleFontName(cf.Language.FontName);
            cancelBtn.setTitleFontSize(40);
            cancelBtn.setScale(0.6);
            cancelBtn.addClickEventListener(function () {
                self.playerBets("申请成功\n  已帮您取消上庄。");
                caseBg.removeFromParent();
                self._listIsOn = false;
            });
            caseBg.addChild(cancelBtn);
        }
    },

    createShangZhuangItem: function (listMsg) {
        var itemBk = new ccui.ImageView("fishery_017.png", ccui.Widget.PLIST_TEXTURE);
        itemBk.setScale9Enabled(true);
        itemBk.setContentSize(600,70);
        var itemLayout = new ccui.Layout();
        itemLayout.setContentSize(itemBk.width, itemBk.height);
        itemLayout.width = 600;
        //初始化宽高
        var w = 600;
        var h = itemLayout.height;
        //背景
        itemBk.setPosition(w / 2, h / 2);
        itemLayout.addChild(itemBk);

        var headData = cf.Data.HEADPORTRAIT_DATA;

        //默认头像为0
        var nHeadID = listMsg.nHeadID;
        //任务图标
        var itemIcon = new ccui.ImageView(headData[nHeadID].headIcon, ccui.Widget.PLIST_TEXTURE);
        itemIcon.setPosition(80, h / 2);
        itemIcon.setScale(0.4);
        itemLayout.addChild(itemIcon);

        var nameLabel = new ccui.Text(listMsg.strNickname, cf.Language.FontName, 20);
        nameLabel.setPosition(160,h / 2);
        nameLabel.setAnchorPoint(0,0.5);
        nameLabel.setTextColor(cf.TitleColor.FONT_2);
        nameLabel.enableOutline(cf.TitleColor.OUT_2, 2);
        itemLayout.addChild(nameLabel);

        var chipsNumLabel = new ccui.Text(listMsg.dChipNum, cf.Language.FontName, 20);
        chipsNumLabel.setPosition(460,h / 2);
        chipsNumLabel.setAnchorPoint(0,0.5);
        chipsNumLabel.setTextColor(cf.TitleColor.FONT_2);
        chipsNumLabel.enableOutline(cf.TitleColor.OUT_2, 2);
        itemLayout.addChild(chipsNumLabel);

        return itemLayout;
    },

    initChipLabel: function () {
        var self = this;
        for(var i = 0;i < 3;i++) {
            var prompt = new ccui.Text("总注:", cf.Language.FontName, 20);
            prompt.setPosition(240 + i * 280,480);
            prompt.setAnchorPoint(0,0.5);
            prompt.setTextColor(cf.TitleColor.FONT_2);
            prompt.enableOutline(cf.TitleColor.OUT_2, 2);
            self._spBk.addChild(prompt);
        }

        self._allChipNum1 = new ccui.Text(self._roomMsg.nDragonNum, cf.Language.FontName, 20);
        self._allChipNum1.setPosition(320,480);
        self._allChipNum1.setAnchorPoint(0,0.5);
        self._allChipNum1.setTextColor(cf.TitleColor.FONT_2);
        self._allChipNum1.enableOutline(cf.TitleColor.OUT_2, 2);
        self._spBk.addChild(self._allChipNum1);

        self._allChipNum2 = new ccui.Text(self._roomMsg.nAndNum, cf.Language.FontName, 20);
        self._allChipNum2.setPosition(600,480);
        self._allChipNum2.setAnchorPoint(0,0.5);
        self._allChipNum2.setTextColor(cf.TitleColor.FONT_2);
        self._allChipNum2.enableOutline(cf.TitleColor.OUT_2, 2);
        self._spBk.addChild(self._allChipNum2);

        self._allChipNum3 = new ccui.Text(self._roomMsg.nTigerNum, cf.Language.FontName, 20);
        self._allChipNum3.setPosition(880,480);
        self._allChipNum3.setAnchorPoint(0,0.5);
        self._allChipNum3.setTextColor(cf.TitleColor.FONT_2);
        self._allChipNum3.enableOutline(cf.TitleColor.OUT_2, 2);
        self._spBk.addChild(self._allChipNum3);

        self._setChipNum1 = new ccui.Text("未下注", cf.Language.FontName, 20);
        self._setChipNum1.setPosition(270,255);
        self._setChipNum1.setAnchorPoint(0,0.5);
        self._setChipNum1.setTextColor(cf.TitleColor.FONT_2);
        self._setChipNum1.enableOutline(cf.TitleColor.OUT_2, 2);
        self._spBk.addChild(self._setChipNum1);

        self._setChipNum2 = new ccui.Text("未下注", cf.Language.FontName, 20);
        self._setChipNum2.setPosition(620,255);
        self._setChipNum2.setAnchorPoint(0,0.5);
        self._setChipNum2.setTextColor(cf.TitleColor.FONT_2);
        self._setChipNum2.enableOutline(cf.TitleColor.OUT_2, 2);
        self._spBk.addChild(self._setChipNum2);

        self._setChipNum3 = new ccui.Text("未下注", cf.Language.FontName, 20);
        self._setChipNum3.setPosition(960,255);
        self._setChipNum3.setAnchorPoint(0,0.5);
        self._setChipNum3.setTextColor(cf.TitleColor.FONT_2);
        self._setChipNum3.enableOutline(cf.TitleColor.OUT_2, 2);
        self._spBk.addChild(self._setChipNum3);

        // this._particleEffect = [];
        // var _particleNode = new cc.Node();
        // _particleNode.setPosition(0, 0);
        // self._spBk.addChild(_particleNode);
        //
        // this._particleEffect[0] = new cc.ParticleSystem(Res.LOBBY.particle_003_plist);
        // this._particleEffect[0].setPosition(-500, 0);
        // this._particleEffect[0].runAction(cc.repeatForever(ActionMgr.createSquare(2, 1, 235, 50)));
        // _particleNode.addChild(this._particleEffect[0]);
        //
        // this._particleEffect[1] = new cc.ParticleSystem(Res.LOBBY.particle_003_plist);
        // this._particleEffect[1].setPosition(-500, 0);
        // this._particleEffect[1].runAction(cc.repeatForever(ActionMgr.createSquare(2, 1, -230, -50)));
        // _particleNode.addChild(this._particleEffect[1]);

        if(this._roomMsg.nPersonDragonNum != 0) {
            self._setChipNum1.setPosition(250,255);
            self._setChipNum1.setString(this._roomMsg.nPersonDragonNum);
        }
        else if(this._roomMsg.nPersonAndNum != 0) {
            self._setChipNum2.setPosition(600,255);
            self._setChipNum2.setString(this._roomMsg.nPersonAndNum);
        }
        else if(this._roomMsg.nPersonTigerNum != 0) {
            self._setChipNum3.setPosition(940,255);
            self._setChipNum3.setString(this._roomMsg.nPersonTigerNum);
        }
    },

    showDragonCard: function (suit,num) {
        var self = this;
        self._card1.removeFromParent();
        self._card1 = new BaseCard(suit,num);
        self._card1.setPosition(self._spBk.width / 2 - 130,self._spBk.height - 100);
        self._card1.turnCard(0);
        self._spBk.addChild(self._card1);
    },

    showTigerCard: function (suit,num) {
        var self = this;
        self._card2.removeFromParent();
        self._card2 = new BaseCard(suit,num);
        self._card2.setPosition(self._spBk.width / 2 + 130,self._spBk.height - 100);
        self._card2.turnCard(0);
        self._spBk.addChild(self._card2);
    },

    //更新庄家名称以及庄家筹码数
    updateOwnerInfo: function (nickName,bankerChipnum) {
        var self = this;
        self._bankerGoldLabel.setString(bankerChipnum.toString());

        self._nickName.setString(nickName);
    },

    updateChipsNum: function () {
        var goldNum = cf.PlayerMgr.getLocalPlayer().getPlayerChipNum();
        this._goldLabel.setString(goldNum.toString());
    },

    //重置下注金额
    resetChipNum: function () {
        var self = this;
        self._setChipNum1.setString("未下注");
        self._setChipNum2.setString("未下注");
        self._setChipNum3.setString("未下注");

        self._setChipNum1.setPosition(270,255);
        self._setChipNum2.setPosition(620,255);
        self._setChipNum3.setPosition(960,255);

        self._allChipNum1.setString("0");
        self._allChipNum2.setString("0");
        self._allChipNum3.setString("0");
    },

    menuItemCallback: function (sender,type) {
        var self = this;
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;
            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                if(self._gameState != 1) {
                    var prompt = UIDialog.createCustom("请您等待开始下注。", cf.Language.getText("text_1098"), null, false, function (type) {
                        if (type == UIDialog.Btn.RIGHT) {

                        }
                    }, self);
                    self._delegate.addChild(prompt, 101);
                    return;
                }
                if( self._chipState == 0) {
                    var prompt = UIDialog.createCustom("请您点击桌面区域选择下注类型。", cf.Language.getText("text_1098"), null, false, function (type) {
                        if (type == UIDialog.Btn.RIGHT) {

                        }
                    }, self);
                    self._delegate.addChild(prompt, 101);
                    return
                }
                switch (sender.tag) {
                    case 0:
                        //1倍筹码
                        self.checkChipNumEnough(10000);
                        break;
                    case 1:
                        //5倍筹码
                        self.checkChipNumEnough(50000);
                        break;
                    case 2:
                        //20倍筹码
                        self.checkChipNumEnough(200000);
                        break;
                    case 3:
                        //50倍筹码
                        self.checkChipNumEnough(500000);
                        break;
                    case 4:
                        //100倍筹码
                        self.checkChipNumEnough(1000000);
                        break;
                    case 5:
                        //300倍筹码
                        self.checkChipNumEnough(3000000);
                        break;
                }
                break;
        }
    },

    checkChipNumEnough: function (chipsNum) {
        var self = this;
        if(cf.PlayerMgr.getLocalPlayer().getPlayerChipNum() >= chipsNum) {
            self.setChip(chipsNum);
        }
        else {
            var prompt = UIDialog.createCustom("抱歉您的筹码数量不足无法下注\n请您兑换筹码。", cf.Language.getText("text_1098"), null, false, function (type) {
                if (type == UIDialog.Btn.RIGHT) {

                }
            }, this);
            self._delegate.addChild(prompt, 101);
        }
    },

    //上庄
    playerShangZhuang: function () {
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var msg = new $root.CS_PublicMsg();
        msg.nidLogin = playerModel.getIdLogin();
        var wr = $root.CS_PublicMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_ShangZhuangMsg, wr);

        this._ownerState = 1;
        this._becomeOwnerBtn.setTitleText("上庄列表");
    },

    playerBets: function (str) {
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var msg = new $root.CS_PublicMsg();
        msg.nidLogin = playerModel.getIdLogin();
        var wr = $root.CS_PublicMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_XiaZhuangMsg, wr);

        this._ownerState = 0;
        this._becomeOwnerBtn.setTitleText("上  庄");
        if(this._localPlayerIsBanker) {
            this._becomeOwnerBtn.setColor(cc.color.GRAY);
            this._becomeOwnerBtn.setTouchEnabled(false);
        }

        var promptStr = "申请成功\n本局结束后自动下庄。";
        if(str) {
            promptStr = str;
        }
        var prompt = UIDialog.createCustom(promptStr, cf.Language.getText("text_1098"), null, false, function (type) {
            if (type == UIDialog.Btn.RIGHT) {

            }
        }, this);
        this._delegate.addChild(prompt, 101);
    },

    //退出房间提示
    backGamePrompt: function () {
        //玩家下注后退出游戏提示
        //展示上庄列表
        var prompt = UIDialog.createCustom("您已经下注,\n如果退出则会造成损失。", cf.Language.getText("text_1559"), cf.Language.getText("text_1099"), false, function (type) {
            if (type == UIDialog.Btn.RIGHT) {
                cf.PlayerMgr.getLocalPlayer().leaveRoom({flag: cf.SceneFlag.LOBBY});
            }
            else if (type == UIDialog.Btn.LEFT) {

            }
        }, this);
        this._delegate.addChild(prompt, 101);
    },

    //无法押注错误提示
    cannotBetsPrompt: function () {
        var prompt = UIDialog.createCustom("抱歉当前押注数量已经达到上限，\n请您更换押注对象", cf.Language.getText("text_1098"),null, false, function (type) {
            if (type == UIDialog.Btn.RIGHT) {

            }
            else if (type == UIDialog.Btn.LEFT) {

            }
        }, this);
        this._delegate.addChild(prompt, 101);
    },

    //下注
    setChip: function (chipNum) {

        var msg = new $root.CS_XiaZhu();
        // 押注数量
        msg.nXiaZhuNum = chipNum;
        // 0押和，1押龙，2押虎
        switch (this._chipState - 1) {
            case 0:
                msg.nXiaZhuType = 1;
                break;
            case 1:
                msg.nXiaZhuType = 0;
                break;
            case 2:
                msg.nXiaZhuType = 2;
                break;
        }

        var wr = $root.CS_XiaZhu.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_XiaZhuMsg, wr);
    },

    //索要玩家上庄列表
    askPlayerShangZhuangList: function () {
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var msg = new $root.CS_PublicMsg();
        msg.nidLogin = playerModel.getIdLogin();
        var wr = $root.CS_PublicMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_AskShangZhuangListMsg, wr);
    },

    //玩家列表
    askPlayerList: function () {
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var msg = new $root.CS_PublicMsg();
        msg.nidLogin = playerModel.getIdLogin();
        var wr = $root.CS_PublicMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_AskPlayerListMsg, wr);
    },

    doChipsFly: function (tag,others,type) {
        var self = this;
        var baseSpeed = 1500;
        if(tag != undefined) {
            var chipNumArr = [1,5,20,50,100,300];
            var scaleArray = [0.75,0.8,0.85,0.9,0.95,1];
            switch (tag){
                case 10000:
                    tag = 0;
                    break;
                case 50000:
                    tag = 1;
                    break;
                case 200000:
                    tag = 2;
                    break;
                case 500000:
                    tag = 3;
                    break;
                case 1000000:
                    tag = 4;
                    break;
                case 3000000:
                    tag = 5;
                    break;
            }
            if(others) {
                var chips = new cc.Sprite("#gameDt_00"+ (tag) +".png",null,null,1);
                chips.setPosition(self._spBk.width - 60, 200);
                chips.setScale(scaleArray[tag]);
                self._spBk.addChild(chips);

                var chipNum =  new cc.LabelBMFont(chipNumArr[tag]+"万", Res.LOBBY.font_lh);
                chipNum.setPosition(chips.width / 2, chips.height / 2);
                chipNum.setScale(0.8);
                chips.addChild(chipNum);

                self._showChipsArrayOther.push(chips);

                var localPos = cc.p(0,0);
                switch (type){
                    case 0:
                        localPos = cc.p(560 + Math.round(Math.random() * 160),320 + Math.round(Math.random() * 90));
                        break;
                    case 1:
                        localPos = cc.p(220 + Math.round(Math.random() * 160),320 + Math.round(Math.random() * 90));
                        break;
                    case 2:
                        localPos = cc.p(880 + Math.round(Math.random() * 160),320 + Math.round(Math.random() * 90));
                        break;
                }
                var dis = cc.pDistance(chips.getPosition(),localPos);
                chips.runAction(cc.sequence(cc.delayTime(Math.random() * 0.8),cc.callFunc(function () {
                    cf.SoundMgr.playEffect(44,false,0.2);
                }),cc.EaseExponentialOut.create(cc.moveTo(dis / baseSpeed,localPos))));
            }
            else {
                var chips = new cc.Sprite("#gameDt_00"+ (tag) +".png",null,null,1);
                chips.setPosition(self._spBk.width / 2 + 90 + 100 * tag,50);
                chips.setScale(scaleArray[tag]);
                self._spBk.addChild(chips);

                var chipNum =  new cc.LabelBMFont(chipNumArr[tag]+"万", Res.LOBBY.font_lh);
                chipNum.setPosition(chips.width / 2, chips.height / 2);
                chipNum.setScale(0.8);
                chips.addChild(chipNum);

                self._showChipsArray.push(chips);

                var localPos = cc.p(0,0);
                switch (this._chipState - 1){
                    case 0:
                        localPos = cc.p(220 + Math.round(Math.random() * 160),320 + Math.round(Math.random() * 90));
                        break;
                    case 1:
                        localPos = cc.p(560 + Math.round(Math.random() * 160),320 + Math.round(Math.random() * 90));
                        break;
                    case 2:
                        localPos = cc.p(880 + Math.round(Math.random() * 160),320 + Math.round(Math.random() * 90));
                        break;
                }
                var dis = cc.pDistance(chips.getPosition(),localPos);
                chips.runAction(cc.sequence(cc.delayTime(Math.random() * 0.8),cc.callFunc(function () {
                    cf.SoundMgr.playEffect(44,false,0.2);
                }),cc.EaseExponentialOut.create(cc.moveTo(dis / baseSpeed,localPos))));
            }
        }
    },

    removeAllShowChips: function (aimPos) {
        var self = this;
        var pos = cc.p(400, -100);
        if(aimPos) {
            pos = aimPos;
        }
        var length = self._showChipsArray.length;
        var factorNum = 0.005;
        // if(length > 20) {
        //     factorNum = 0.03;
        // }
        // else if(length > 50) {
        //     factorNum = 0.01;
        // }
        for(var i = 0;i < length;i++) {
            self._showChipsArray[i].runAction(cc.sequence(cc.EaseExponentialOut.create(cc.moveTo(0.3 + factorNum * i,pos)),cc.callFunc(function() {
                this.removeFromParent();
            },self._showChipsArray[i])));
        }
        self._showChipsArray.splice(0,length);
        self._showChipsArray = [];
    },

    //庄家或者其他玩家的获利筹码特效
    removeAllShowBankerChips: function (isBanker) {
        var self = this;
        var length = self._showChipsArrayOther.length;
        var factorNum = 0.005;
        var aimpos = cc.p(0,0);
        if(isBanker) {
            aimpos = cc.p(cc.winSize.width / 2, cc.winSize.height + 100);
            if( self._nickName.getString() ==  cf.PlayerMgr.getLocalPlayer().getNickName()) {
                aimpos = cc.p(400, -100);
            }
            if(length > 50) {
                for(var i = 0;i < length - 50;i++) {
                    self._showChipsArrayOther[i].runAction(cc.sequence(cc.EaseExponentialOut.create(cc.moveTo(0.3 + factorNum * i,aimpos)),cc.callFunc(function() {
                        this.removeFromParent();
                    },self._showChipsArrayOther[i])));
                }
                self.removeAllShowChips(aimpos);
                aimpos = cc.p(self._spBk.width - 60, 200);
                for(var index = length - 50;index < length;index++) {
                    self._showChipsArrayOther[index].runAction(cc.sequence(cc.EaseExponentialOut.create(cc.moveTo(0.3 + factorNum * index,aimpos)),cc.callFunc(function() {
                        this.removeFromParent();
                    },self._showChipsArrayOther[index])));
                }
                self._showChipsArrayOther.splice(0,length);
                self._showChipsArrayOther = [];
            }
            else {
                for(var i = 0;i < length;i++) {
                    self._showChipsArrayOther[i].runAction(cc.sequence(cc.EaseExponentialOut.create(cc.moveTo(0.3 + factorNum * i,aimpos)),cc.callFunc(function() {
                        this.removeFromParent();
                    },self._showChipsArrayOther[i])));
                }
                self.removeAllShowChips(aimpos);
            }
        }
        else {
            aimpos = cc.p(self._spBk.width - 60, 200);
            for(var i = 0;i < length;i++) {
                self._showChipsArrayOther[i].runAction(cc.sequence(cc.EaseExponentialOut.create(cc.moveTo(0.3 + factorNum * i,aimpos)),cc.callFunc(function() {
                    this.removeFromParent();
                },self._showChipsArrayOther[i])));
            }
            self.removeAllShowChips(aimpos);
            self._showChipsArrayOther.splice(0,length);
            self._showChipsArrayOther = [];
        }

    },

    //展示状态
    showState: function () {

        switch (this._gameState) {
            case 0:
                break;
            case 1:
                this.stateShowAction(0);
                cf.SoundMgr.playEffect(45);
                break;
            case 2:
                this.stateShowAction(1);
                cf.SoundMgr.playEffect(49);
                break;
            case 3:
                break;
        }
    },

    stateShowAction: function (type) {
        var iconName = "#gameDt_026.png";
        if(type == 1){
            iconName = "#gameDt_025.png";
        }
        var promptBg = new cc.Sprite("#gameDt_027.png");
        promptBg.setPosition(cc.winSize.width / 2,cc.winSize.height / 2);
        promptBg.setScale(0.5);
        this._spBk.addChild(promptBg,1);

        var promptSp = new cc.Sprite(iconName);
        promptSp.setPosition(promptBg.width / 2,promptBg.height / 2 + 26);
        promptBg.addChild(promptSp);

        promptBg.runAction(cc.sequence(cc.scaleTo(0.5,1.4),cc.delayTime(0.8),cc.callFunc(function () {
            promptBg.removeFromParent();
        })));
    },


    /**
     * 添加监听
     * @private
     */
    addListener: function () {
        var self = this;

        //创建网络消息
        var msgDataArray = [
            //玩家进入房间消息
            {msgId: MessageCode.SC_NewPlayerJoinRoomMsg, callFunc: self._resPlayerEnter},
            //玩家离开房间消息
            {msgId: MessageCode.SC_RemovePlayerFromRoomMsg, callFunc: self._resPlayerLeave},
            //玩家离开房间返回大厅
            {msgId: MessageCode.CS_LeaveRoomMsg_Res, callFunc: self._resLeaveRoom},

            //{msgId: MessageCode.SC_LogMsg, callFunc: self._resLog},
            //上庄结果
            {msgId: MessageCode.CS_ShangZhuangMsg_Res, callFunc: self._resShangZhuang},
            //玩家下注结果
            {msgId: MessageCode.CS_XiaZhuMsg_Res, callFunc: self._resBets},
            //通知同房间玩家，新庄家坐庄
            {msgId: MessageCode.SC_PlayerZuoZhuangMsg, callFunc: self._resPlayerZuoZhuang},
            //通知同房间玩家，有人下注
            {msgId: MessageCode.SC_PlayerXiaZhuMsg, callFunc: self._resPlayerBets},
            //状态转变
            {msgId: MessageCode.SC_StateChangeMsg, callFunc: self._resStateChange},
            //下注最终数据
            {msgId: MessageCode.SC_XiaZhuInfoMsg, callFunc: self._resXiaZhuInfo},
            //结算
            {msgId: MessageCode.SC_Result, callFunc: self._resResult},
            //上庄列表
            {msgId: MessageCode.CS_AskShangZhuangListMsg_Res, callFunc: self._resShangZhuangList},
            //玩家列表
            {msgId: MessageCode.CS_AskPlayerListMsg_Res, callFunc: self._resPlayerList},
        ];

        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener.push(cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this)));
        }
        //创建事件监听
        self._doEventListener = cf.addListener(GameCompareDTRoom.DoEvent, self._doEvent.bind(this));
    },

    _doEvent: function(msg) {
        if (msg == null) {
            return;
        }
        //判断是否为结构
        var flag = msg;
        if (typeof msg == "object") {
            flag = msg.flag;
        }
        //显示商城，皮肤解锁，
        switch (flag) {
            case GameCompareDTRoom.EventFlag.UPDATECHIPSNUM://刷新筹码数量
                this.updateChipsNum();
                break;
        }
    },

    /**
     * 玩家进入消息处理
     * @param msg
     * @private
     */
    _resPlayerEnter: function (msg) {
        var resMsg = $root.SC_NewPlayerJoinRoomMsg.decode(msg);
        if (resMsg) {
            //cc.log("新玩家进场，座位id = " + resMsg.nSeatID);
            //同步服务器时间
            cf.PlayerMgr.getLocalPlayer().setGameTime(resMsg.strTime);
            var playerData = resMsg.playerInfoRoom[0];
        } else {
            cc.error("_resNewPlayerEnter错误:SC_NewPlayerJoinRoomMsg");
        }
    },

    /**
     * 玩家离开消息处理
     * @param msg
     * @private
     */
    _resPlayerLeave: function (msg) {
        var resMsg = $root.SC_RemovePlayerFromRoomMsg.decode(msg);
        if (resMsg) {
            //cc.log("玩家退出，座位id = " + resMsg.nSeatID);

        }
    },

    /**
     * 玩家离开房间消息逻辑
     * @param msg
     * @private
     */
    _resLeaveRoom: function (msg) {
        var resMsg = $root.CS_LeaveRoomMsg_Res.decode(msg);
        MsgPool.getInstance().resetChatRecordRoom();
        if (resMsg) {
            if (resMsg.nRes == 0) {
                //判断下玩家的去向
                var leaveRoomInfo = cf.PlayerMgr.getLocalPlayer().getLeaveRoomInfo();
                switch (leaveRoomInfo.flag) {
                    case cf.SceneFlag.LOBBY:
                        cf.SceneMgr.runScene(cf.SceneFlag.LOBBY);
                        break;
                    case cf.SceneFlag.GAME_LOADING:
                        cf.dispatchEvent(GameScene.Reload, leaveRoomInfo.data);
                        break;
                }

            } else {
                cc.error("resLeaveRoom错误：CS_LeaveRoomMsg_Res");
                cf.SceneMgr.runScene(cf.SceneFlag.LOBBY);
            }
        }
    },

    _resShangZhuang:function (msg) {
        var resMsg = $root.CS_ShangZhuang_Res.decode(msg);
        if (resMsg) {
            //消息暂未使用
            // var result = resMsg.nRes;
            // if(result == 0) {
            //     this._ownerState == 1;
            // }
            // else {
            //     this._ownerState == 0;
            // }
        }
    },

    _resBets:function (msg) {
        var resMsg = $root.CS_XiaZhu_Res.decode(msg);
        if (resMsg) {
            // 下注金额
            var nXiaZhuNum = resMsg.nXiaZhuNum;
            // 玩家剩余金额
            var dChipNum = resMsg.dChipNum;

            var nPersonTigerNum = resMsg.nPersonTigerNum;
            var nPersonDragonNum = resMsg.nPersonDragonNum;
            var nPersonAndNum = resMsg.nPersonAndNum;
            //更新玩家金币
            this._goldLabel.setString(dChipNum.toString());
            cf.PlayerMgr.getLocalPlayer().setPlayerChipNum(dChipNum);
            cf.PlayerMgr.getLocalPlayer().setGoldNum(dChipNum);
            switch (this._chipState - 1){
                case 0:
                    this._setChipNum1.setString(nPersonDragonNum);
                    this._setChipNum1.setPosition(250,255);
                    break;
                case 1:
                    this._setChipNum2.setString(nPersonAndNum);
                    this._setChipNum2.setPosition(600,255);
                    break;
                case 2:
                    this._setChipNum3.setString(nPersonTigerNum);
                    this._setChipNum3.setPosition(940,255);
                    break;
            }
            //执行筹码飞行动画
            this.doChipsFly(nXiaZhuNum);
            cf.SoundMgr.playEffect(44);
        }
    },

    _resPlayerZuoZhuang:function (msg) {
        var resMsg = $root.SC_PlayerZuoZhuang.decode(msg);
        if (resMsg) {
            //上庄消息 需要改变状态以及滚动提示
            var headData = cf.Data.HEADPORTRAIT_DATA;

            //默认头像为0
            var nHeadID = resMsg.nHeadID;
            this._roleIcon.setSpriteFrame(headData[nHeadID].headIcon);
            // 庄家昵称
            var strNickname = resMsg.strNickname;
            // 庄家金额
            var dChipNum = resMsg.dChipNum;

            var localPlayer = cf.PlayerMgr.getLocalPlayer();
            if(strNickname == localPlayer.getNickName()) {
                this._becomeOwnerBtn.setTitleText("下  庄");
                this._ownerState = 2;
                this._localPlayerIsBanker = true;
            }
            else {
                if(this._localPlayerIsBanker) {
                    this._becomeOwnerBtn.setTitleText("上  庄");
                    this._ownerState = 0;
                    this._localPlayerIsBanker = false;
                    this._becomeOwnerBtn.setColor(cc.color.WHITE);
                    this._becomeOwnerBtn.setTouchEnabled(true);
                }
            }

            //刷新庄家信息
            this.updateOwnerInfo(strNickname,dChipNum);
            //执行滚动提示
            var promptBg = new cc.Sprite("#gameDt_027.png");
            promptBg.setPosition(cc.winSize.width / 2,cc.winSize.height / 2);
            promptBg.setScale(0.5);
            this._spBk.addChild(promptBg,1);

            var promptInfo = new ccui.Text(strNickname + "上庄", cf.Language.FontName, 34);
            promptInfo.setPosition(promptBg.width / 2,promptBg.height / 2);
            promptInfo.setTextColor(cc.color.YELLOW);
            promptBg.addChild(promptInfo);

            promptBg.runAction(cc.sequence(cc.scaleTo(0.5,1.6),cc.delayTime(0.8),cc.callFunc(function () {
                promptBg.removeFromParent();
            })));
        }
    },

    _resPlayerBets:function (msg) {
        var resMsg = $root.SC_PlayerXiaZhu.decode(msg);
        if (resMsg) {
            var self = this;
            // 下注金额
            var nXiaZhuNum = resMsg.nXiaZhuNum;
            // 玩家剩余金额
            var nXiaZhuType = resMsg.nXiaZhuType; // 0押和，1押龙，2押虎

            self._chipsCount++;
            if((self._chipsCount > 6) || (nXiaZhuType == 0 && self._chipsCount > 3)) {
                //执行筹码飞行动画
                self.doChipsFly(nXiaZhuNum,true,nXiaZhuType);
                self._chipsCount = 0;
            }
            // 该类型的下注总额
            var nAllXiaZhuNum = resMsg.nAllXiaZhuNum;
            switch (nXiaZhuType) {
                case 0:
                    //更新下注总额
                    this._allChipNum2.setString(nAllXiaZhuNum);
                    break;
                case 1:
                    this._allChipNum1.setString(nAllXiaZhuNum);
                    break;
                case 2:
                    this._allChipNum3.setString(nAllXiaZhuNum);
                    break;
            }
        }
    },

    _resStateChange:function (msg) {
        var resMsg = $root.SC_StateChange.decode(msg);
        if (resMsg) {
            var self = this;
            // 状态id
            var nState = resMsg.nState;  // 状态，0局间等待，1下注，2等待开牌，3开牌
            // 持续时间
            var nTime = resMsg.nTime;   // 持续时间（秒）

            var nZuoZhuangNum = resMsg.nZuoZhuangNum;

            self._bossTime.setString(nZuoZhuangNum + " / 10");

            this._gameState = nState;
            //游戏状态 0 等待 1下注阶段 2等待开牌 3开牌结算
            switch (this._gameState) {
                case 0:
                    self._card1.removeFromParent();
                    self._card1 = new BaseCard(1,13);
                    self._card1.setPosition(self._spBk.width / 2 - 130,self._spBk.height - 100);
                    self._spBk.addChild(self._card1);

                    self._card2.removeFromParent();
                    self._card2 = new BaseCard(1,13);
                    self._card2.setPosition(self._spBk.width / 2 + 130,self._spBk.height - 100);
                    self._spBk.addChild(self._card2);
                    this._statePrompt.setString("等待中");
                    break;
                case 1:
                    if(this._ownerState != 2) {
                        this.setFakeBtnState(true,false);
                    }
                    this._statePrompt.setString("请下注");
                    break;
                case 2:
                    this._statePrompt.setString("待开牌");
                    break;
                case 3:
                    this._statePrompt.setString("结算中");
                    break;
            }

            if(this._gameState != 1) {
                cc.log("_gameState");
                // self._particleEffect[0].stopAllActions();
                // self._particleEffect[1].stopAllActions();
                // self._particleEffect[0].stopSystem();
                // self._particleEffect[0].setPosition(-500,0);
                // self._particleEffect[1].stopSystem();
                // self._particleEffect[1].setPosition(-500,0);
                this.setFakeBtnState(false,false);
            }
            this._countTime = nTime;
            this.showState();
        }
    },

    _resXiaZhuInfo:function (msg) {
        var resMsg = $root.SC_XiaZhuInfo.decode(msg);
        if (resMsg) {
            // 下龙
            var nXiaZhuDragon = resMsg.nXiaZhuDragon;
            // 下虎
            var nXiaZhuTiger = resMsg.nXiaZhuTiger;
            // 下和
            var nXiaZhuAnd = resMsg.nXiaZhuAnd;
            // 庄家
            var dZhuangJia = resMsg.dZhuangJia;

            this._allChipNum1.setString(nXiaZhuDragon);
            this._allChipNum2.setString(nXiaZhuAnd);
            this._allChipNum3.setString(nXiaZhuTiger);
        }
    },

    _resResult:function (msg) {
        var resMsg = $root.SC_Result.decode(msg);
        if (resMsg) {
            var self = this;
            // 获胜方
            var nWin = resMsg.nWin;    // 0和，1龙，2虎
            this.doHistoryMoveAciton(nWin);
            switch (nWin) {
                case 0:
                    cf.SoundMgr.playEffect(47);
                    break;
                case 1:
                    cf.SoundMgr.playEffect(46);
                    break;
                case 2:
                    cf.SoundMgr.playEffect(48);
                    break;
            }
            // 龙牌
            var nPokerDragon = resMsg.nPokerDragon;
            // 龙花色
            var nPokerDragonHua = resMsg.nPokerDragonHua;  // 0黑桃，1红桃，2草花，3方片
            // 虎牌
            var nPokerTiger = resMsg.nPokerTiger;
            // 虎花色
            var nPokerTigerHua = resMsg.nPokerTigerHua;  // 0黑桃，1红桃，2草花，3方片
            // 庄家获利
            var dBankerGetChip = resMsg.dBankerGetChip;
            // 庄家当前筹码
            var dBankerChip = resMsg.dBankerChip;
            // 玩家获利
            var dPlayerGetChip = resMsg.dPlayerGetChip;
            // 玩家当前筹码
            var dPlayerChip = resMsg.dPlayerChip;

            //展示牌
            this.showDragonCard(nPokerDragonHua + 1,nPokerDragon.toString());
            this.showTigerCard(nPokerTigerHua + 1,nPokerTiger.toString());

            if((dPlayerGetChip != undefined && dPlayerGetChip != 0) || ((self._setChipNum1.getString() > 0) ||(self._setChipNum2.getString() > 0) ||(self._setChipNum3.getString() > 0))) {
                //展示获利结果 并更新筹码
                var localRewardStruct = {};
                localRewardStruct.nItemID = 15;
                localRewardStruct.nItemNum = dPlayerChip;
                localRewardStruct.nError = 0;
                localRewardStruct.nNum = dPlayerGetChip;
                // if(dPlayerGetChip < 0) {
                //     localRewardStruct.nNum = 0;
                // }
                localRewardStruct.strEndTime = "";
                if(self._prompt) {
                    self._prompt.removeFromParent();
                }
                self._delegate.runAction(cc.sequence(cc.delayTime(1),cc.callFunc(function () {
                    if(dPlayerGetChip > 0) {
                        var prompt = self._prompt = UIAwardToast.create([localRewardStruct], function () {
                            self.updateChipsNum();
                            self._prompt = null;
                        }, self, false, cf.DCType.DC_SYSTEM_LOGIN,1);
                        self._delegate.addChild(prompt, 101);
                    }
                    else {
                        var prompt = self._prompt = UIAwardToast.create([localRewardStruct], function () {
                            self.updateChipsNum();
                            self._prompt = null;
                        }, self, false, cf.DCType.DC_SYSTEM_LOGIN,0);
                        self._delegate.addChild(prompt, 101);
                    }

                    //庄家获利展示
                    var bankerPrompt = new ccui.Text("庄家得分：" + dBankerGetChip, cf.Language.FontName, 26);
                    bankerPrompt.setPosition(cc.winSize.width / 2 - bankerPrompt.width / 2,190);
                    if(dPlayerGetChip > 0) {
                        bankerPrompt.setPosition(cc.winSize.width / 2 - bankerPrompt.width / 2,150);
                    }
                    bankerPrompt.setAnchorPoint(0,0.5);
                    bankerPrompt.setTextColor(cf.TitleColor.FONT_2);
                    bankerPrompt.enableOutline(cf.TitleColor.OUT_2, 2);
                    prompt.addChild(bankerPrompt);

                    //个人获利展示
                    var playerPrompt = new ccui.Text("个人得分：" + dPlayerGetChip, cf.Language.FontName, 26);
                    playerPrompt.setPosition(cc.winSize.width / 2 - playerPrompt.width / 2,260);
                    if(dPlayerGetChip > 0) {
                        playerPrompt.setPosition(cc.winSize.width / 2 - playerPrompt.width / 2,190);
                    }
                    playerPrompt.setAnchorPoint(0,0.5);
                    playerPrompt.setTextColor(cf.TitleColor.FONT_2);
                    playerPrompt.enableOutline(cf.TitleColor.OUT_2, 2);
                    prompt.addChild(playerPrompt);

                    prompt._closeLabel.setPosition(0,-260);
                })));

                if(dPlayerGetChip > 0) {
                    this.removeAllShowChips();
                }
            }
            if(dBankerGetChip > 0) {
                //执行庄家获利效果
                this.removeAllShowBankerChips(true);
            }
            else {
                //执行其他玩家获利效果
                this.removeAllShowBankerChips(false);
            }
            this._bankerGoldLabel.setString(dBankerChip);
            cf.PlayerMgr.getLocalPlayer().setPlayerChipNum(dPlayerChip);
            cf.PlayerMgr.getLocalPlayer().setGoldNum(dPlayerChip);
            this._goldLabel.setString(dPlayerChip);
            //重置押注金额
            this.resetChipNum();
            this._chipState = 0;
        }
    },

    _resShangZhuangList:function (msg) {
        var resMsg = $root.CS_AskShangZhuangList_Res.decode(msg);
        if (resMsg) {
            var listZhuangJia = resMsg.listZhuangJia;
            this.showListOfShangZhuang(listZhuangJia,0);
        }
    },

    _resPlayerList:function (msg) {
        var resMsg = $root.CS_AskShangZhuangList_Res.decode(msg);
        if (resMsg) {
            var listZhuangJia = resMsg.listZhuangJia;
            this.showListOfShangZhuang(listZhuangJia,1);
        }
    },

    /**
     * 福利金LOG（测试用）
     * @param msg
     * @private
     */
    _resLog:function(msg){
        var resMsg = $root.SC_LogMsg.decode(msg);

        var logBtn = this._delegate.getChildByTag(100000);
        if(!logBtn.isVisible()){
            logBtn.setVisible(true);
        }
        cc.log(resMsg.logInfo);
        if(this._listViewLog == null){
            this._listViewLog = new ccui.ListView();
            this._listViewLog.setDirection(ccui.ScrollView.DIR_VERTICAL);
            this._listViewLog.setGravity(ccui.ListView.GRAVITY_CENTER_VERTICAL);
            this._listViewLog.setTouchEnabled(false);
            this._listViewLog.setContentSize(cc.size(1200, 400));
            this._listViewLog.setBounceEnabled(true);
            this._listViewLog.setPosition(100, 100);
            this._listViewLog.jumpToBottom();//设置回到底部
            this._delegate.addChild(this._listViewLog,101);

            //创建默认背景
            var itemLayout = new ccui.Layout();
            var _label = new cc.LabelTTF( resMsg.logInfo, cf.Language.FontName, 20,cc.size(600,0));
            itemLayout.setContentSize(_label.getContentSize());

            _label.setPosition(itemLayout.width / 2 + 50,itemLayout.height / 2 );

            _label.setColor(cc.color.RED);

            itemLayout.addChild(_label);

            this._listViewLog.pushBackCustomItem(itemLayout);

            this._listViewLog.setItemsMargin(8);
        }
        else{
            if(this._listViewLog.getItems().length > 1){
                this._listViewLog.removeItem(0);
            }
            //创建默认背景
            var itemLayout = new ccui.Layout();
            var _label = new cc.LabelTTF(resMsg.logInfo, cf.Language.FontName, 20,cc.size(600,0));
            itemLayout.setContentSize(_label.getContentSize());

            _label.setPosition(itemLayout.width / 2 + 50,itemLayout.height / 2 );

            _label.setColor(cc.color.RED);

            itemLayout.addChild(_label);
            this._listViewLog.pushBackCustomItem(itemLayout);

            this._listViewLog.setItemsMargin(8);
        }
    },

    /**
     * 移除监听
     * @private
     */
    removeListener: function () {
        //移除网络监听
        var len = this._onNetMsgListener.length;
        for (var i = 0; i < len; ++i) {
            cf.removeListener(this._onNetMsgListener[i]);
        }
        cf.removeListener(this._doEventListener);
    },

    onClear:function () {
        this._delegate = null;
        this.removeListener();
    },

    /**********************************************************
     * 触摸相关方法
     **********************************************************/
    onTouchesBegan: function (touches, event) {
        return false;
    },

    onTouchesMoved: function (touches, event) {

    },

    onTouchesEnded: function (touches, event) {

    },

    onTouchesCancelled: function (touches, event) {

    },
});

GameCompareDTRoom.DoEvent = "GameCompareDTRoom.DoEvent";
GameCompareDTRoom.EventFlag = {
    UPDATECHIPSNUM:1001,
};
