var BaseFish = BaseNode.extend({

    _sprite: null,                  //< 精灵
    _spriteShadow: null,

    _fishModel: null,               //< 鱼信息
    _startPoint: null,               //< 初始点
    _lastPoint: null,               //< 上一点
    _curAngle: 0,                    //< 当前角度
    _pathAction: null,              //< 路径动作
    _speedAction: null,              //< 控制路径动作速度的动作
    _fishState: null,               //< 鱼状态
    _seatID: -1,                     //< 座位ID
    _catchMsg: null,                   //< 玩家捕鱼消息
    _originalDuration: null,         //< 路径的原始运行时间

    _lockedInfo: null,                //< 锁定信息

    _unFrozenTime: null,              //< 精灵的解冻时间

    _isFrozen: null,                  //< 精灵是否处于冰冻状态

    comStageLock: false,              //< 在第一次进入房间时如果房间内存在被锁定的精灵则激活该变量执行相应的锁定逻辑

    comStageIce: false,               //< 用于进场同步冰冻特效

    _slowFactor: 1,                     //< 被减速的因数
    _bShowAttack: false,               //< 是否显示攻击特效
    _curColor: null,                   //< 当前颜色值
    _checkAngle: false,                //< 角度调整完成
    _countTime: false,                 //< 计时变量
    //_debugSp: null,

    onEnter: function () {
        this._super();
    },

    onExit: function () {
        this._super();
    },

    /**
     * 初始化
     * @param id 鱼种类id
     * @param serverIndex 服务器索引
     */
    initData: function (id, serverIndex, bFeatsFish) {
        this._fishModel = FishModel.create(id, serverIndex);
        if (bFeatsFish != undefined) {
            this._fishModel._bFeatsFish = bFeatsFish;
        }
        this._lastPoint = cc.p(0, 0);
        this._curAngle = 0;
        if (this._fishState == null) {
            this._fishState = BaseFish.State.STANDBY;
        }
        this._seatID = -1;
        this._catchMsg = null;
        this._bShowAttack = false;
        this._checkAngle = false;
        this._curColor = cc.color.WHITE;
        this.initCollisionData();
        this.scheduleUpdate();
        this.setGroupIndex(0);
        this.setScale(this._fishModel.getZoom());
    },

    /**
     * 重置精灵数据
     * @param id 鱼种类id
     * @param serverIndex 服务器索引
     */
    resetData: function (id, serverIndex, bFeatsFish) {
        this._fishModel.initData(id, serverIndex);
        if (bFeatsFish != undefined) {
            this._fishModel._bFeatsFish = bFeatsFish;
        }
        this._lastPoint.x = 0;
        this._lastPoint.y = 0;
        this._curAngle = 0;
        this._fishState = BaseFish.State.STANDBY;
        this._seatID = -1;
        this._catchMsg = null;
        this._bShowAttack = false;
        this._curColor = cc.color.WHITE;
        this._isFrozen = false;
        this._checkAngle = false;
        this.initCollisionData();
        this.scheduleUpdate();
        this.setScale(this._fishModel.getZoom());
        this._sprite.setFlippedX(false);
        this.setFishColor(cc.color.WHITE);
        this.setOpacity(255);
        this._sprite.setRotation(0);
    },

    /**
     * 初始化碰撞数据
     */
    initCollisionData: function () {
        var self = this;
        //包围盒测试
        //self._debugSp = null;
        //self._debugSp = [];
        var collisionId = self._fishModel._collisionId;
        var scale = self._fishModel.getZoom();
        //本地版暂时使用c++逻辑
        if (cc.sys.isNative) {
            //获取碰撞数据
            var collisionData = cf.Data.FISH_COLLISION_DATA[collisionId];
            self._collisionLen = collisionData.circleArray.length;

            //初始化碰撞模型
            self._collisionData = new CollisionData;
            self._collisionData.clearData();
            for (var i = 0; i < self._collisionLen; ++i) {
                var circle = collisionData.circleArray[i];
                if (i == 0) {
                    self._collisionData.addCheckCircle(circle.pt, circle.r * scale);
                } else {
                    self._collisionData.addData(circle.pt, circle.r * scale);
                }
            }
        } else {
            self._collisionData = cf.clone(cf.Data.FISH_COLLISION_DATA[collisionId]);
            self._collisionLen = self._collisionData.circleArray.length;
            for (var j = 0; j < self._collisionLen; ++j) {
                self._collisionData.circleArray[j].r *= scale;

                //包围盒测试
                /*
                self._debugSp[j] = new cc.Sprite("#mask.png");
                self._debugSp[j].setPosition(self._collisionData.circleArray[j].pt);
                self._debugSp[j].setScale(self._collisionData.circleArray[j].r * 2 / 100);
                self._debugSp[j].setOpacity(120);
                self._debugSp[j].setColor(cc.color.BLACK);
                cf.debugNode.addChild(self._debugSp[j]);
                */
            }
        }
    },

    /**
     * 更新计算碰撞数据
     */
    updateCollisionData: function () {
        var self = this;

        //本地版暂时使用c++逻辑
        if (cc.sys.isNative) {
            self._collisionData.updateData(this, self._sprite.getRotation());
        } else {
            var collisionId = self._fishModel._collisionId;
            var collisionData = cf.Data.FISH_COLLISION_DATA[collisionId];
            for (var i = 0; i < self._collisionLen; ++i) {
                var circle = self._collisionData.circleArray[i];
                //计算圆心相对位置            
                circle.pt.x = self.x + collisionData.circleArray[i].pt.x;
                circle.pt.y = self.y + collisionData.circleArray[i].pt.y;
                //根据角度转换圆心位置
                var localRotation = 0;
                if (self._sprite) {
                    localRotation = self._sprite.getRotation();
                }
                cf.pointRotateByAngle(circle.pt, self.x, self.y, cc.degreesToRadians(localRotation));

                //包围盒测试
                //self._debugSp[i].setPosition(circle.pt);
            }
        }
    },

    checkNodeInRect: function () {
        return this._collisionData.checkNodeInRect(this, 1280, 720);
    },

    /**
     * 更新计算方向
     */
    updateDir: function (dt) {
        var self = this;

        /*
         //native版getPosition会反复的创建对象，这里引擎的垃圾回收机制写的不好，会
         //导致卡顿，web版本没问题，所以使用下面的逻辑来完成计算
         var curPos = this.getPosition();
         if( !cc.pointEqualToPoint(curPos,self._lastPoint) ){
         //计算方向向量
         var dir = cc.pSub(curPos,self._lastPoint);
         //计算方向夹角
         var angle = cc.radiansToDegrees( cc.pToAngle(dir) );
         //这里向下为正方向，做个处理转换下角度
         self._curAngle = -90-angle;
         self.setRotation(self._curAngle);
         self._lastPoint = curPos;
         }
         */
        if (self._isFrozen && self._checkAngle) {
            return;
        }
        if (this.x != self._lastPoint.x || this.y != self._lastPoint.y) {
            //计算方向分量
            var dirX = this.x - self._lastPoint.x;
            var dirY = this.y - self._lastPoint.y;
            
            
            //计算方向夹角
            var atanVal = Math.atan2(dirY, dirX);
            var angle = cc.radiansToDegrees(atanVal);

            
            //这里向下为正方向，做个处理转换下角度
            switch (self._fishModel._fishId) {
                
               //小豬
                //調整影子的位置
                case 1:
               
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-10);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+10);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-10);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+10);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;
                    //鳥用負值(不然看起來太接近地面)
                //戰狗
                case 2:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-30);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+30);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-30);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+30);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;
                case 3:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            //self._spriteShadow.setPosition(0, self._sprite.height/2-10);
                            self._spriteShadow.setPosition(0, self._sprite.height/2+140);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                               // self._spriteShadow.setPosition(0, -self._sprite.height/2+10);
                               self._spriteShadow.setPosition(0, self._sprite.height/2-140);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                           // self._spriteShadow.setPosition(0, self._sprite.height/2-10);
                           self._spriteShadow.setPosition(0, self._sprite.height/2+140);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                           // self._spriteShadow.setPosition(0, -self._sprite.height/2+10);
                           self._spriteShadow.setPosition(0, self._sprite.height/2-140);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;

                case 4:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-35);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+35);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-35);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+35);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                break;
                case 5:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-35);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+35);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-35);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+35);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;
                    case 6:
                        if (dirX < 0) {
                            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                                
                               if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, self._sprite.height/2-40);
                               }
                               self._sprite.setScaleX(1);
                            }
                            else {
                                if(self._spriteShadow){
                                    self._spriteShadow.setPosition(0, -self._sprite.height/2+40);
                                   }
                                self._sprite.setScaleX(-1);
                           
                            }
                           
                       }
                       else {
                           if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                               
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, self._sprite.height/2-40);
                               }
                            self._sprite.setScaleX(-1);
                           }
                           else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+40);
                               }
                            self._sprite.setScaleX(1);
                              
                           }
                       }
                        break;
                    
                case 9:
                case 10:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-38);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+38);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-38);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+38);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;
                case 11:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-45);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+45);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-45);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+45);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;
                case 12:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-45);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+45);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-45);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+45);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;
                case 13:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-53);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+53);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-53);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+53);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;
                case 14:
                case 15:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-50);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+50);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-50);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+50);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;
                    //夫人的影子
                case 16:
                case 17:
                case 18:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-70);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+70);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-70);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+70);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;
                //郭泛影子
                case 19:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-150);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {

                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+150);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-150);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+150);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                break;
                 //李催影子
                case 20:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-105);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {

                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+105);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-105);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+105);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                break;
                case 21:
                case 22:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-105);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {

                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+105);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-150);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+150);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                break;
                case 27:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-60);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+60);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-60);
                           }
                        self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, -self._sprite.height/2+60);
                           }
                        self._sprite.setScaleX(1);
                          
                       }
                   }
                   break;
                case 28:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-60);
                           }
                           self._sprite.setScaleX(1);
                        }
                        else {
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+60);
                               }
                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                           

                        self._sprite.setScaleX(-1);
                       }
                       else {

                        self._sprite.setScaleX(1);
                          
                       }
                   }
                break;

                case 29:
                case 30:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                           self._sprite.setScaleX(1);
                        }
                        else {

                            self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                        self._sprite.setScaleX(-1);
                       }
                       else {

                        self._sprite.setScaleX(1);
                          
                       }
                   }
                   // self._curAngle = 180-angle;
                   
                    // console.log("29,30 SeatID",cf.PlayerMgr.getLocalPlayer().getSeatID(),"DIRX",dirX,self._spriteShadow);
                    self._curAngle = -180-angle
                    break;
                
                    
                    // self._spriteShadow.setRotation(cf.convertAngleWithSeatId(cf.PlayerMgr.getLocalPlayer().getSeatID(), 0))
                default:
                    if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, self._sprite.height/2-30);
                            }
                           
                           self._sprite.setScaleX(1);
                        }
                        else {
                           
                            if(self._spriteShadow){
                                self._spriteShadow.setPosition(0, -self._sprite.height/2+30);
                            }
                           self._sprite.setScaleX(-1);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                        if(self._spriteShadow){
                            self._spriteShadow.setPosition(0, self._sprite.height/2-50);
                        }
                           self._sprite.setScaleX(-1);
                       }
                       else {
                        if(self._spriteShadow){

                            self._spriteShadow.setPosition(0, -self._sprite.height/2+50);
                        }
                           self._sprite.setScaleX(1);
                          
                       }
                   }
                    break;
                    // case 29:
                    // case 30:
                    //     if (dirX < 0) {
                    //         if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                                
                    //            if(self._spriteShadow){
                    //             //self._spriteShadow.setPosition(0, self._sprite.height/2-10);
                    //             self._spriteShadow.setPosition(0, self._sprite.height/2+140);
                    //            }
                    //            self._sprite.setScaleX(-1);
                    //         }
                    //         else {
                    //             if(self._spriteShadow){
                    //                // self._spriteShadow.setPosition(0, -self._sprite.height/2+10);
                    //                self._spriteShadow.setPosition(0, self._sprite.height/2-140);
                    //                }
                    //             self._sprite.setScaleX(1);
                           
                    //         }
                           
                    //    }
                    //    else {
                    //        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                               
                    //         if(self._spriteShadow){
                    //            // self._spriteShadow.setPosition(0, self._sprite.height/2-10);
                    //            self._spriteShadow.setPosition(0, self._sprite.height/2+140);
                    //            }
                    //         self._sprite.setScaleX(1);
                    //        }
                    //        else {
                    //         if(self._spriteShadow){
                    //            // self._spriteShadow.setPosition(0, -self._sprite.height/2+10);
                    //            self._spriteShadow.setPosition(0, self._sprite.height/2-140);
                    //            }
                    //         self._sprite.setScaleX(-1);
                              
                    //        }
                    //    }
                    //     break;
            }

            if (self._sprite) {
                switch (self._fishModel._fishType) {
                    case cf.FishType.LOTTERY:
                        break;
                    // case cf.FishType.BOSS:
                    //    if(self._fishModel._fishId == 25){
                    //         // if (dirX < 0) {
                    //         //      if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                                     
                    //         //     //     self._sprite.setFlippedX(true);
                    //         //         self._sprite.setScaleY(-1);
                    //         //      }
                    //         //      else {
                    //         //         self._sprite.setScaleY(1);
                    //         //     //     self._sprite.setFlippedX(false);
                    //         //      }
                                
                    //         // }
                    //         // else {
                    //         //     if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                    //         //         //self._sprite.setFlippedX(false);
                    //         //         self._sprite.setScaleY(1);
                    //         //     }
                    //         //     else {
                    //         //         self._sprite.setScaleY(-1);
                    //         //        // self._sprite.setFlippedX(true);
                    //         //     }
                    //         // }
                    //         if(angle <50 && angle > 0 || angle > -50 && angle < 0){
                    //             self._sprite.setScaleY(1);
                    //             self._sprite.setRotation(90);
                    //         }else{
                    //             //self._sprite.setScaleY(-1);
                    //            // self._sprite.setScaleX(-1);
                    //             self._sprite.setRotation(90);
                    //         }
                    //     }
                    //      break;
                    default:
                       
                        if (dirX < 0) {
                            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                                
                                self._sprite.setScaleY(-1);
                                self._sprite.setScaleX(-1);
                            }
                            else {
                                
                                self._sprite.setScaleX(-1);
                                self._sprite.setScaleY(1);
                           
                            }
                           
                       }
                       else {
                           if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                            self._sprite.setScaleY(-1);
                            self._sprite.setScaleX(1);
                            
                            
                            
                           }
                           else {
                            
                            self._sprite.setScaleX(1);
                            self._sprite.setScaleY(1);
                              
                           }
                       }
                        break
                      // self._sprite.setRotation(180);
                        break;
                }
            }
            if (self._spriteShadow) {
                switch (self._fishModel._fishType) {
                    case cf.FishType.LOTTERY:
                        break;
                    case cf.FishType.BOSS:
                    case cf.FishType.MATCH:
                       
                    default:
                        
                       // self._spriteShadow.setRotation(self._curAngle);
                       if (dirX < 0) {
                        if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                            
                            self._spriteShadow.setFlippedY(true);
                            
                           self._spriteShadow.setFlippedX(true);

                        }
                        else {
                            
                            
                            
                            self._spriteShadow.setFlippedY(false);
                            self._spriteShadow.setFlippedX(true);
                       
                        }
                       
                   }
                   else {
                       if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2) {
                        
                        
                        
                        self._spriteShadow.setFlippedY(true);
                        self._spriteShadow.setFlippedX(false);
                       }
                       else {
                        
                        self._spriteShadow.setFlippedY(false);
                        self._spriteShadow.setFlippedX(false);
                          
                       }
                   }
                    break
                        
                }
            }
            //self.setRotation(self._curAngle);
            self._lastPoint.x = this.x;
            self._lastPoint.y = this.y;
            this._checkAngle = true;
        }
    },

    /**
     * 逻辑更新
     * @param dt
     */
    update: function (dt) {
        switch (this._fishState) {
            case BaseFish.State.MOVE:
                this.updateCollisionData();
                this.updateDir(dt);
                this.firstLockCameLogic();
                this.countDownStateFrozen(dt);
                this.updatezIndex(dt);
                break;
        }
    },
    //調整疊圖zindex
   updatezIndex: function(dt){
       var self = this;
       
       switch (self._fishModel._fishId) {
        //小豬
        case 1:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
            
                self.getParent().reorderChild(self, self.getPosition().y+60);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y+20);
               }
        break;
        //民兵
        case 2:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
            
                self.getParent().reorderChild(self, self.getPosition().y+115);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y+75);
               }
        break;
        case 6:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
            
                self.getParent().reorderChild(self, self.getPosition().y+110);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y+90);
               }
        break;
               //力士
         case 10:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 120);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 120);
               }

            break;
         case 11:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 120);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 100);
               }

            break;
               //蠻兵
         case 12:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 140);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 120);
               }

            break;
            //運輸隊,馬
         case 13:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 130);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 110);
               }

            break;

            //弩砲車
         case 14:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 100);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 60);
               }

            break;
        //运输队
         case 15:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 90);
                   
               }else{
                

                self.getParent().reorderChild(self, -self.getPosition().y + 90);
               }

         
            break;
        //蔡文姬, 夫人
         case 16:
         case 17:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 170);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 150);
               }
             break;
        //張濟, 郭汜
         case 18:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 170);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 170);
               }  
            break;
         case 19:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 180);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 170);
               }  
            break;
         case 20:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 170);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 160);
               }
             break;
         //李儒, 董白
         case 21:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 170);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 160);
               }

         break;
         case 22:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 180);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 150);
               }

         break;
         //貂蟬, 呂布, 董卓
         case 23:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 230);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 230);
               }
         break;
         case 24:
         case 25:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
                
                self.getParent().reorderChild(self, self.getPosition().y + 270);
                   
               }else{
                
                self.getParent().reorderChild(self, -self.getPosition().y + 230);
               }
         break;
         default:
            if (cf.PlayerMgr.getLocalPlayer().getSeatID() >= 2){
               
                self.getParent().reorderChild(self, self.getPosition().y+100);
                   
               }else{
                

                self.getParent().reorderChild(self, -self.getPosition().y+80);
               }
         break;


       }
      
     

   },
    jxTransFormAnimate: function (dt) {
        var self = this;
        self._attactcount += dt;
        if (self._attactcount > 4) {
            self._attactcount = 0;
            switch (self._fishModel._fishId) {
                case 25:
                    self._sprite.stopActionByTag(10001);
                    self._spriteShadow.stopActionByTag(10001);
                    //self.doAnimate("Fish_Lv26_Move_Attack");
                    //创建动画
                    var action1 = ActionMgr.getAnimate("Fish_Lv26_Move_Attack");
                    action1.setTag(10002);
                    var action2 = ActionMgr.getAnimate("Fish_Lv26_Move_Attack");
                    action2.setTag(10002);
                    this._sprite.runAction(cc.sequence(action1, cc.callFunc(function () {
                        self._attactcount = 0;
                        self._sprite.stopActionByTag(10002);
                        self._spriteShadow.stopActionByTag(10002);
                        self.doAnimate(self._fishModel.getMoveAction());
                    })));
                    if (this._spriteShadow) {
                        this._spriteShadow.runAction(action2);
                    }
                    break;
                default:
                    break
            }
        }
    },

    /**
     * 第一次进行房间时使用的更新逻辑
     */
    firstLockCameLogic: function () {
        var self = this;
        if (self.comStageLock) {
            if (self._lockedInfo && self._lockedInfo.length > 0) {
                for (var i in self._lockedInfo) {
                    if (self._lockedInfo[i]) {
                        var seatId = self._lockedInfo[i].nSeatID;
                        var lastTime = self._lockedInfo[i].nLastTime;
                        var player = cf.RoomMgr.getRoomPlayer(seatId);
                        if (player) {
                            player.setLockElement(this);
                            cf.SkillMgr.setLockSkillOpen(seatId);
                            cf.SkillMgr.setLockSkillLastTime(seatId, lastTime / 1000);
                            self.comStageLock = false;
                        }
                    }
                    else {
                        cc.error(i + "index" + self._lockedInfo[i] + "------------------------------------" + self._lockedInfo.length);
                    }
                }
                if (this.getIsFrozen()) {
                    self.setSlowFactor(self._originalDuration / self._lockedInfo[0].nSpeed);
                } else {
                    self.setActionSpeedChange(false, self._lockedInfo[0].nSpeed);
                }
            }
            else {
                self.comStageLock = false;
            }
        }
    },

    /**
     * 获得模型数据
     * @returns {null}
     */
    getModel: function () {
        return this._fishModel;
    },

    /**
     * 执行路径运动
     * @param pathData 路径信息
     * @param trackTime 时间戳
     */
    doPath: function (pathData, trackTime, offsetPoint, maxOffsetX) {
        var self = this;

        self.setVisible(true);
        //设置鱼状态为移动
        self._fishState = BaseFish.State.MOVE;

        self.stopAllActions();
        //根据id获取时间
        self._originalDuration = pathData.duration;
        var duration = self._originalDuration;

        var moveAction = null;
        if (offsetPoint && offsetPoint.x != undefined && offsetPoint.y != undefined) {
            if (pathData.pathType == cf.PathType.LINE) {
                //设置路径位置
                var startPoint, endPoint;
                var width = 200;
                if (pathData.startPoint.x < 0) {
                    startPoint = cc.p(-width + offsetPoint.x - maxOffsetX, pathData.startPoint.y + offsetPoint.y);
                    endPoint = cc.p(1280 + width + offsetPoint.x + maxOffsetX, pathData.endPoint.y + offsetPoint.y);
                }
                else {
                    startPoint = cc.p(1280 + width + offsetPoint.x + maxOffsetX, pathData.startPoint.y + offsetPoint.y);
                    endPoint = cc.p(-width + offsetPoint.x - maxOffsetX, pathData.endPoint.y + offsetPoint.y);
                }
                //分辨率不同需要需要转换基础运动路径
                startPoint = cf.convertCoordinates(startPoint.x, startPoint.y);
                endPoint = cf.convertCoordinates(endPoint.x, endPoint.y);
                self._lastPoint = cc.p(startPoint.x, startPoint.y);
                self._startPoint = cc.p(startPoint.x, startPoint.y);
                self.setPosition(self._lastPoint);


                moveAction = cc.moveTo(duration, endPoint);
            }
            else {
                moveAction = cc.cardinalSplineTo(duration, pathData.wayPoint, 0);
            }
        }
        else {
            //设置路径位置
            self._lastPoint = pathData.startPoint;
            self._startPoint = pathData.startPoint;
            self.setPosition(self._lastPoint);

            switch (pathData.pathType) {
                case cf.PathType.LINE://直线路径
                    moveAction = cc.moveTo(duration, pathData.endPoint);
                    break;
                case cf.PathType.BEZIER://贝塞尔路径
                    moveAction = cc.bezierTo(duration, pathData.wayPoint);
                    break;
                case cf.PathType.CURVE://曲线路径
                    moveAction = cc.cardinalSplineTo(duration, pathData.wayPoint, 0);
                    break;
            }
        }
        //判断时间戳
        if (trackTime < 0) {
            //小于0：鱼还未游出，设置一个延迟时间等待
            self._pathAction = cc.sequence(
                cc.callFunc(function () {
                    self.setVisible(false);
                }, this),
                cc.delayTime(Math.abs(trackTime / 1000)),
                cc.callFunc(function () {
                    self.setVisible(true);
                    var type = this._fishModel.getType();
                    if (type != undefined) {
                        if (type == cf.FishType.BOSS) {
                            cf.EffectMgr.playBossComing(this._fishModel,type);
                        }
                    }
                }, this),
                moveAction,
                cc.callFunc(function () {
                    var type = this._fishModel.getType();
                    if (type != undefined) {
                        if (type == cf.FishType.BOSS) {
                            this.setMusicByRoomId();
                        }
                    }
                    cc.pool.putInPool(this);
                }, this)
            );
            self._speedAction = cc.speed(self._pathAction, 1);
            self.runAction(self._speedAction);
        } else {
            //大于0：鱼已经游出，直接设置时间
            self._pathAction = cc.sequence(
                moveAction,
                cc.callFunc(function () {
                    cc.pool.putInPool(this);
                }, this)
            );
            self._speedAction = cc.speed(self._pathAction, 1);
            self.runAction(self._speedAction);
            //设置运动时间
            self._pathAction.setElapsed(trackTime / 1000, false);
        }
    },

    /**
     * 死亡动画
     * @param msg 玩家捕鱼消息
     */
    doDied: function (msg) {
        var self = this;
        this._catchMsg = msg;
        this._seatID = msg.nSeatID;
        
        
        //统一转换坐标
        //cf.convertPosWithSeatId(this._lastPoint);
        
        this.stopAllActions();
        this._sprite.stopAllActions();
        this._sprite.removeAllChildren();
        this._fishState = BaseFish.State.DIED;
        var player = cf.RoomMgr.getRoomPlayer(cf.PlayerMgr.getLocalPlayer().getSeatID());
        if (player && this == player.getLockElement()) {
            player.setLockElement(null);
            player.setLastObject(null);
        }

        //执行死亡动画，具体由继承类实现
        //FrameFish,BoneFish

        //临时用程序实现，具体看之后美术素材
        var seq = cc.sequence(
            cc.scaleTo(0.1, 0.8),
           cc.scaleTo(0.1, 1.0)
           
           
        );
        //彩金鱼
        if (this._fishModel._randomScore) {
            //计算随机分数后的鱼倍数
            cc.log(this._catchMsg.nScore + "被打死的鱼的分值(扣除彩金)");
            var fortType = this._catchMsg.nBulletLevel;
            var fortNum = cf.Data.CANNON_LEVEL_DATA[fortType].cannonlevel;
            var score = this._catchMsg.nScore / fortNum;
            this._fishModel.setScore(score);
        }

        var type = this._fishModel.getType();
        //技能鱼特殊处理
        if (msg.isSkillFish) {
            //显示争霸分,这里显示的是总分
             if (type == cf.FishType.SKILL) {
                //  switch(this._fishModel._fishId){
                //      case 27:
                //          this.setPosition(cc.winSize.width/2, cc.winSize.height/2);
                //          this.getParent().reorderChild(this, 100);
                //         this._sprite.setAnimation(0, "attack30", false);
                //         this._sprite.setTimeScale(0.7);
                        
                //      break; 
                //      case 28:
                         
                //             this.setPosition(cc.winSize.width/2, cc.winSize.height/2);
                //             this.getParent().reorderChild(this, 100);
                //             this._sprite.setAnimation(0, "attack31", false);
                            
                        
                        
                //        // cf.EffectMgr.doSkillFishEffect(this._catchMsg);
                       
                //      break;
                //  }
                 this.setOpacity = 0;
                 this.runAction(cc.sequence(
                     cc.delayTime(1.5),
                     cc.callFunc(()=>{
                      
                            //骨骼动画则移除
                            this.removeFromParent();
                        // cf.EffectMgr.addHammerAnimation(this._catchMsg, this._fishModel._fishId);
                     })
                 ))
                 
            //     cf.EffectMgr.showMatchScore(this._lastPoint, this._catchMsg);
             }else{
                
                    cf.EffectMgr.play(this._lastPoint, this._fishModel, this._catchMsg);
                
            this.runAction(cc.sequence(
                cc.delayTime(msg.ndeadTime),
                cc.repeat(seq, 5),
                cc.callFunc(function () {
                    if (this._fishModel._animateType == cf.AnimateType.BONES) {
                        //骨骼动画则移除
                        this.removeFromParent();
                    } else {
                        cc.pool.putInPool(this);
                    }
                }, this)
            ));
             }
        } else {
            //cf.RoomMgr.getRoomId()
            
            switch(this._fishModel._fishId){
                case 16:
                    cf.SoundMgr.playEffect(81, false);
                    break;
                case 17:
                    cf.SoundMgr.playEffect(82, false);
                    break;
                case 18:
                    cf.SoundMgr.playEffect(83, false);
                    break;
                case 19:
                    cf.SoundMgr.playEffect(84, false);
                    break;
                case 20:
                    cf.SoundMgr.playEffect(85, false);
                    break;
                case 21:
                    cf.SoundMgr.playEffect(86, false);
                    break;
                case 22:
                    cf.SoundMgr.playEffect(87, false);
                    break;
                case 23:
                    cf.SoundMgr.playEffect(78, false);
                    this.setMusicByRoomId();
                    break;
                case 24:
                    cf.SoundMgr.playEffect(79, false);
                    this.setMusicByRoomId();
                    break;
                case 25:
                    cf.SoundMgr.playEffect(80, false);
                    this.setMusicByRoomId();
                break;
            }

            switch(this._fishModel._fishId){
                case 29:
                     cf.EffectMgr.BonusEffect(this._lastPoint, this._fishModel, this._catchMsg);
                    //  cf.EffectMgr.gamingTable(this._lastPoint, this._fishModel, this._catchMsg);

                break;
                case 30:
                    cf.EffectMgr.BonusEffect(this._lastPoint, this._fishModel, this._catchMsg);
                    // cf.EffectMgr.larbar(this._lastPoint, this._fishModel, this._catchMsg);

                break;
                default:
                    //播放特效
                cf.EffectMgr.play(this._lastPoint, this._fishModel, this._catchMsg);
                break;

            }

            
            
           
                
                
            

            this.runAction(cc.sequence(
                cc.repeat(seq, 5),
                cc.callFunc(function () {
                    if (this._fishModel._animateType == cf.AnimateType.BONES) {
                        //骨骼动画则移除
                        this.removeFromParent();
                    } else {
                        cc.pool.putInPool(this);
                    }
                }, this)
            ));
        }
    },

    /**
     * 
     * boss死掉音樂換回
     */
    setMusicByRoomId: function(){
        var roomId = cf.RoomMgr.getRoomId();
        cf.SoundMgr.stopMusic();
        cf.SoundMgr.playMusic(cf.Data.ROOM_DATA[roomId].bgm, true);
    },
    /**
     * 设置精灵的减速因数
     */
    setSlowFactor: function (factor) {
        this._slowFactor = factor;
    },

    /**
     * 根据服务器给出的效果进行鱼的减速设置
     * @param elapsed 已经执行的时间设置精灵的位置
     * @param reset 是否需要返回原始速度
     */
    setActionSpeedChange: function (reset, elapsed) {
        var self = this;
        if (reset) {
            //cc.log("将速度重置为1");
            self._slowFactor = 1;
            self._speedAction.setSpeed(self._slowFactor);
            return;
        }
        if (elapsed != undefined) {
            self._slowFactor = self._originalDuration / elapsed;
            self._speedAction.setSpeed(self._slowFactor);
            //cc.log("设置精灵的速度");
        }
    },

    /**
     * 检测精灵是否可锁定
     */
    checkCanLock: function () {
        return this.isVisible() && this._fishState == BaseFish.State.MOVE && cf.checkCanLock(this);
    },

    /**
     * 检测精灵在屏幕内
     */
    checkInScreen: function () {
        return this.isVisible() && this._fishState == BaseFish.State.MOVE && cf.checkInScreen(this);
    },

    doSummonSkillAction: function () {
        var self = this;
        this.setCascadeOpacityEnabled(true);
        this.setOpacity(0);
        this.runAction(cc.spawn(
            cc.fadeIn(1.5),
            cc.callFunc(function () {
                cf.EffectMgr.playSummonEffect(self.getPosition());
            })
        ));
    },

    /**
     * 解除锁定
     */
    unLockElement: function () {
        if (this.getIsFrozen()) {
            this.setSlowFactor(1);
        }
        else {
            this.setActionSpeedChange(true, null);
        }
    },

    /**
     * 获取精灵的冰冻状态
     */
    getIsFrozen: function () {
        return this._isFrozen;
    },

    /**
     * 设置冰冻结束时间
     */
    setFrozenEndTime: function (frozenEndTime) {
        var self = this;
        if (frozenEndTime) {
            self._unFrozenTime = frozenEndTime / 1000;
            self._isFrozen = true;
            //cc.log("冰冻技能的剩余时间" + self._unFrozenTime);
        }
    },

    /**
     *  精灵本身对冰冻时间进行计时，在冰冻结束后如果存在减速效果则将冰冻效果切换为减速效果
     */
    countDownStateFrozen: function () {
        var self = this;
        if (self.comStageIce) {
            if (cf.PlayerMgr.getLocalPlayer()) {
                var player = cf.RoomMgr.getRoomPlayer(cf.PlayerMgr.getLocalPlayer().getSeatID());
                if (player) {
                    if (!cf.SkillMgr.getIceSkillState()) {
                        cf.SkillMgr.setIceSkillOpen(true);
                        cf.SkillMgr.setIceSkillLastTime(self._unFrozenTime);
                        cf.EffectMgr.playIceEffect();
                        self.setFrozen(true);
                        self.comStageIce = false;
                    }
                    else {
                        this.setFrozen(true);
                        self.comStageIce = false;
                    }
                }
            }
        }
    },

    /**
     * 鱼的冰冻设置
     */
    setFrozen: function (isFrozen) {
        var self = this;
        if (isFrozen) {
            self._isFrozen = isFrozen;
            if (self._speedAction) {
                self._speedAction.setSpeed(0);
                self.showFrozen(true);
                cc.log("设置冰冻" + this._fishModel._serverIndex);
            }
        }
        else {
            self._isFrozen = isFrozen;
            if (self._speedAction) {
                self._speedAction.setSpeed(self._slowFactor);
                self.showFrozen(false);
                cc.log("解除冰冻" + this._fishModel._serverIndex);
            }
        }
    },

    /**
     * 获取鱼模型
     */
    getFishModel: function () {
        return this._fishModel;
    },

    doActionOver: function () {
        //重置数据
        this.stopAllActions();
        this._fishState = BaseFish.State.STANDBY;
        this._lastPoint.x = 0;
        this._lastPoint.y = 0;
        this._curAngle = 0;

        //this.setVisible(false);
        this.setPosition(9999, 9999);
    },

    setFishState: function (state) {
        this._fishState = state;
    },

    getFishState: function () {
        return this._fishState;
    },

    /**
     * 设置精灵的被锁定次数
     */
    setAimedTimes: function (time) {
        this._aimedTimes = time;
    },

    /**
     * 获得精灵的被锁定次数
     */
    getAimedTimes: function () {
        return this._aimedTimes;
    },

    /**
     * 设置精灵的锁定信息列表
     */
    setLockInfo: function (lockInfo) {
        var self = this;
        //先处理lockInfo
        //暂时使用冒泡排序的方式对原始的锁定信息进行相应的数据排列
        var length = lockInfo.length;
        if (!(length > 0)) {
            return;
        }
        self.setAimedTimes(length);
        for (var i = 0; i < length - 1; i++) {
            for (var j = 0; j < length - 1 - i; j++) {
                var localInfo1 = lockInfo[j];
                var localInfo2 = lockInfo[j + 1];
                if (localInfo1.nSpeed > localInfo2.nSpeed) {
                    var tmp = lockInfo[j];
                    lockInfo[j] = lockInfo[j + 1];
                    lockInfo[j + 1] = tmp;
                }
            }
        }
        self._lockedInfo = [];
        self._lockedInfo.push(lockInfo[0]);
        var element;
        for (var index = 1; index < length; index++) {
            element = self._lockedInfo[self._lockedInfo.length - 1];
            if (lockInfo[index].nLastTime > element.nLastTime) {
                self._lockedInfo.push(lockInfo[index]);
            }
        }
    },

    getLockPriority: function () {
        return cf.Data.FISH_DATA[this.getModel()._fishId].lockPriority;
    },

    /**
     * 设置鱼体颜色
     * @param color
     */
    setFishColor: function (color) {
        if (this._sprite) {
            this._curColor = color;
            this._sprite.setColor(color);
        }
    },

    /**
     * 显示被攻击特效
     */
    showBeAttacked: function () {
        var self = this;
        if (self._sprite) {
            if (!self._bShowAttack) {
                self._bShowAttack = true;
                var tintTo = cc.tintTo(0.4, 255, 0, 0);
                var tintToBack = cc.tintTo(0.4, self._curColor);
                self._sprite.runAction(cc.sequence(
                    tintTo,
                    tintToBack,
                    cc.callFunc(function () {
                        self._sprite.setColor(self._curColor);
                        self._bShowAttack = false;
                    })
                ));
            }
        }
    },

    /**
     * 是否显示冰冻效果
     * @param bShow
     */
    showFrozen: function (bShow) {

    },

    /**
     * 重置精灵
     * @remark 复用则从缓冲池取出
     * @param id
     * @param serverIndex
     */
    reuse: function (id, serverIndex, bFeatsFish) {
        this.resetData(id, serverIndex, bFeatsFish);
    },

    /**
     * 剔除精灵
     * @remark 不使用则压入缓冲池
     */
    unuse: function () {
        cf.FishMgr.removeFishFromMap(this._fishModel.getServerIndex());
        this.doActionOver();
        this.removeFromParent(true);
        this.unscheduleUpdate();
    }

});

/**
 * 鱼状态
 */
BaseFish.State = {
    MOVE: 0,        //< 移动状态
    DIED: 1,        //< 死亡状态
    STANDBY: 2,     //< 待机状态
    COLLISION: 3,    //< 碰撞状态
    ATTACK: 4
};