var Fort = cc.Node.extend({
    _spFort: null,      //< 炮精灵   
    _nFortNum: 1,        //< 炮倍
    _nFortPosID: 0,      //< 炮位，这里指左右
    _nSeatID: null,

    _animatePos: null,
    _animateTargetPos: null,
    _bulletPos: null,
    _playerModel: null,
    _hitTotalCount: null,
    _roomId:null,

    /**
     构造函数
     * @param playerModel 玩家数据
     * @param fortPosID 炮台位置类型
     * @returns {boolean}
     */
    ctor: function (playerModel, fortPosID, hitCount, roomId) {
        this._super();

        var self = this;

        self._roomId = roomId;

        //根据炮倍索引选择对应皮肤
        var cannonLevelData = cf.Data.CANNON_LEVEL_DATA[playerModel.getFortType()];
        var skinInfo = cannonLevelData.skinIndex.get(self._roomId);
        //读取炮台数据
        var cannonData = cf.Data.CANNON_DATA[skinInfo[0]];
        var frameName = "#" + cannonData.cannon_icon + ".png";
        //关键帧动画
        self._spFort = new cc.Sprite(frameName);
        self._spFort.setAnchorPoint(0.5, 0.25);
        //骨骼动画
        /*self._spFort = ActionMgr.createSpine(Res.GAME.cannon_02Json,Res.GAME.cannon_02Atlas);
         this._spFort.setMix('cannon_01_stop', 'cannon_01_attack', 0.5);
         this._spFort.setMix('cannon_01_attack', 'cannon_01_stop', 0.5);
         self._spFort.setAnimation(0, "cannon_02_stop", true);*/

        self.addChild(self._spFort);

        self._playerModel = playerModel;
        self._nSeatID = playerModel.getSeatID();
        self._nFortPosID = fortPosID;

        //动画位置
        self._animatePos = cc.p(0, 0);
        self._animateTargetPos = cc.p(0, 0);
        self._bulletPos = cc.p(0, 0);

        //反弹次数
        self._hitTotalCount = hitCount;
        return true;
    },

    /**
     * 开炮
     * * @param node 父节点
     * @param msg 服务器消息:SC_PlayerFireInRoomMsg
     */
    onFire: function (node, msg) {
        //开炮动画
        var rotation = this._spFort.getRotation();
        //说明：这里使用了局部变量来存储位置信息(避免临时生成{}导致垃圾回收，有待验证！！！)
        this._animateTargetPos.x = Math.sin(cc.degreesToRadians(rotation)) * (-10);
        this._animateTargetPos.y = Math.cos(cc.degreesToRadians(rotation)) * (-10);

        this._spFort.stopAllActions();
        var act = cc.sequence(
            cc.moveTo(0.05, this._animateTargetPos),
            cc.moveTo(0.05, this._animatePos)
        );
        this._spFort.runAction(act);

        //骨骼动画
        //this._spFort.clearTracks(0);
        //this._spFort.setAnimation(1, "cannon_02_attack", false);

        this._addBullet(node, msg);
    },

    /**
     * 添加子弹
     * @param node 父节点
     * @param msg 服务器消息:SC_PlayerFireInRoomMsg
     * @private
     */
    _addBullet: function (node, msg) {
        //根据炮台角度计算子弹位置便宜
        //判断炮位类型
        var angle = 0, bulletIndex = "";
        switch (this._nFortPosID) {
            case Fort.FortPosID.LEFT:
                angle = cf.convertAngleWithSeatId(msg.nSeatID, msg.fAngle[0]);
                bulletIndex = msg.vecBulletIndex[0];
                break;
            case Fort.FortPosID.RIGHT:
                angle = cf.convertAngleWithSeatId(msg.nSeatID, msg.fAngle[1]);
                bulletIndex = msg.vecBulletIndex[1];
                break;
        }

        //转换坐标为世界坐标
        //说明：这里使用了局部变量来存储位置信息(避免临时生成{}导致垃圾回收，有待验证！！！)  
        this._bulletPos.x = this.x + Math.sin(cc.degreesToRadians(angle)) * (Fort.BULLET_OFFSET);
        this._bulletPos.y = this.y + Math.cos(cc.degreesToRadians(angle)) * (Fort.BULLET_OFFSET);
        this._bulletPos = node.convertToWorldSpace(this._bulletPos);

        //根据座位id转换坐标
        cf.convertPosWithSeatId(this._bulletPos);

        //创建子弹数据        
        //添加子弹
        var bullet = Bullet.create();
        bullet.setName(bulletIndex);
        //初始化或者重置model数据
        var bulletModel = bullet.getModel();
        bulletModel.setSeatID(msg.nSeatID);
        bulletModel.setServerIndex(bulletIndex);
        bulletModel.setBulletType(this._playerModel.getFortType());
        bulletModel.setAngle(angle);
        bulletModel.setBulletPos(this._bulletPos);
        bulletModel.setBulletHitCount(this._hitTotalCount);
        bulletModel.setBulletSpeed(cf.BulletSpeed.COMMON);
        var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
        if(player){
            var lockElement = player.getLockElement();
            if (lockElement && lockElement.checkCanLock()) {
                bulletModel.setLockedElement(lockElement);
            }
            if(cf.SkillMgr.getRageSkillState(player.getSeatID())){
                bulletModel.setBulletSpeed(cf.BulletSpeed.RAGE);
            }
        }
        bullet.initData(this._roomId, msg.nSeatID);
        cf.BulletMgr.addBullet(bullet,msg.nSeatID,bulletIndex);
    },

    /**
     * 同步子弹位置
     * @param node 父节点
     * @param serverTime 服务器时间
     * @param msg 服务器消息
     */
    synchronousBullet: function (node, serverTime, msg) {
        /*cc.log("同步子弹座位 seatID = " + this._nSeatID + ", 本地玩家座位id = " + cf.PlayerMgr.getLocalPlayer().getSeatID());
        var angle = cf.convertAngleWithSeatId(this._nSeatID, msg._fAngle);
        //转换坐标为世界坐标
        //说明：这里使用了局部变量来存储位置信息(避免临时生成{}导致垃圾回收，有待验证！！！)
        this._bulletPos.x = this.x + Math.sin(cc.degreesToRadians(angle)) * (Fort.BULLET_OFFSET);
        this._bulletPos.y = this.y + Math.cos(cc.degreesToRadians(angle)) * (Fort.BULLET_OFFSET);
        this._bulletPos = node.convertToWorldSpace(this._bulletPos);

        //根据座位id转换坐标
        cf.convertPosWithSeatId(this._bulletPos);

        //创建子弹数据
        var bulletModel = BulletModel.create();
        bulletModel.setSeatID(this._nSeatID);
        bulletModel.setServerIndex(msg._strBulletID);
        bulletModel.setBulletLevel(msg._nBulletLevel);
        bulletModel.setBulletType(this._playerModel.getFortSkinUse());
        bulletModel.setAngle(angle);
        bulletModel.setBulletTime(msg._strTime);
        bulletModel.setServerTime(serverTime);
        bulletModel.setBulletPos(this._bulletPos);
        cf.BulletMgr.addBullet(bulletModel);*/
    },

    /**
     * 设置炮台旋转角度
     * @param angle
     */
    setFortAngle: function (angle) {
        this._spFort.setRotation(angle);
    },

    getFortAngle: function () {
        return this._spFort.getRotation();
    },

    /**
     * 炮台换肤
     * @param fortIndex
     * @param skinIndex
     */
    modifyFortSkin: function (fortIndex, skinIndex, roomId) {
        this._roomId = roomId;
        //根据炮倍选择对应皮肤
        var cannonLevelData = cf.Data.CANNON_LEVEL_DATA[fortIndex];
        var skinInfo = cannonLevelData.skinIndex.get(roomId);

        //更换炮台皮肤
        var cannonData = cf.Data.CANNON_DATA[skinInfo[0]];
        this._spFort.setSpriteFrame(cannonData.cannon_icon + ".png");
    },

    modifySkillFortSkin: function(roomId, index){
        this._roomId = roomId;
        this._spFort.setSpriteFrame( "cannon_11_01.png");

    },


});

/**
 * 炮位类型
 */
Fort.FortPosID = {
    LEFT: 1,
    RIGHT: 2
};

Fort.BULLET_OFFSET = 0;