var Player = cc.Node.extend({
    _playerModel: null,             //< 玩家数据模型
    _leftFort: null,                //< 主炮
    _rightFort: null,               //< 副炮
    _autoFireInterval: 0.33,         //< 自动开炮间隔

    _labelFortNum: null,            //< 炮倍Label
    _labelGold: null,               //< 金币Label

    _bLocalPlayer: null,            //< 是否是本机玩家
    _bAlive: false,                 //< 是否被激活

    _bAutoFire: false,               //< 子弹开炮
    _bTouchFire: false,              //< 触摸发炮
    _lastAutoOnFire: false,          //< 记录自动开炮未改变时的值
    _autoFireLabel: null,            //< 自动开炮提示图标
    _serverTime: null,               //< 服务器时间

    _spFortBk: null,                //< 炮座节点
    _spWaitText: null,              //< 等待文字
    _spGoBankrupt: null,             //< 破产图标

    _fortNode: null,                //< 炮座节点
    _curTime: 0,                     //< 当前时间
    _lastTime: 0,                    //< 上一次时间
    _curFortType: 1,                 //< 当前炮倍

    _curPos: null,                  //< 炮座位置
    _roomId: null,                  //< 房间id
    _cannonLimit: null,             //< 房间炮倍限制(索引值)
    _bFortCanFire: false,

    _waitForGet: false,               //< 等待领取
    _countDownTime: null,             //< 倒计时时间
    _isCountDown: false,              //< 是否开启倒计时
    _countTime: null,                 //< 计时
    _doleBtn: null,                   //< 救济金按钮
    _countDownLabel: null,            //< 倒计时显示label
    _doleId: null,                    //< 救济金档位

    _fastBtnArray: null,              //< 快捷键按钮数组
    _bFastBtn: false,                //< 快捷显示标记
    _bClickFast: true,
    _bankrupt: false,                  //< 破产

    _minCannonLevel: 0,                 //< 最小房间炮倍

    _lockObject: null,                   //< 用来记录本地被锁定的精灵(防止本地出现733)的情况

    _roleInfoBD: null,                  //< 玩家信息

    _autoLockUI: null,                  //<  智能捕魚

    _lockElement: null,
    _goldPos: null,                      //< 金币位置
    _diamondPos: null,                   //< 钻石位置
    _goldIcon: null,                     //< 金币icon
    _diamondIcon: null,                  //< 钻石icon
    _bGoldAction: true,
    _bUnlock: false,                    //< 是否已经解锁
    _spUnlockIcon: null,                //< 解锁图标

    _skillCountTime: null,
    _lockSprite: null,                       //锁定效果显示的精灵
    _hasLock: null,                          //已锁定
    _firstLock: false,                       //第一次锁定
    _touch: null,
    _lastBonusScore: 0,                     //< 彩金特效分数
    _bFastBtnSubClick: [],               //< 快捷键子按钮点击开关
    _ragePromptLabel: null,
    _skillUseIcon: null,
    _leftFortAngle: 0,
    _rightFortAngle: 0,
    _ragePromptEffect: null,
    _rageParticle: null,
    _doleHasAllGetPrompt: null,         //< 救济金次数已经领取完成
    _hasSendLockObject:false,           //< 已经发送过锁定消息

    _autoEffect: null,                  //<智能捕魚開啟特效
    _spEffect: null,

    _autoNotice: null,
         //<智能捕魚啟動時關閉自動
    /**
     * 构造方法
     */
    ctor: function () {
        this._super();

        this._playerModel = PlayerModel.create();
        //默认都不是本地玩家
        this.setLocalPlayer(false);

        //创建炮座节点
        this._fortNode = new cc.Node();
        this.addChild(this._fortNode, 0);
        this._fortNode.setPosition(0, 0);

        //空位UI
        this._spFortBk = new cc.Sprite("#fishery_010.png");
        this._spFortBk.setAnchorPoint(0.5, 0);
        this._spFortBk.setPosition(0, 0);
        this.addChild(this._spFortBk, -1);

        //等待提示
        this._spWaitText = new cc.Sprite("#fishery_027.png");
        this._spWaitText.setPosition(this._spFortBk.width / 2, this._spFortBk.height / 2 - 20);
        this._spFortBk.addChild(this._spWaitText);
        this._spWaitText.runAction(cc.repeatForever(cc.blink(2, 2)));

        this._curPos = cc.p(0, 0);
        this._goldPos = cc.p(0, 0);
        this._diamondPos = cc.p(0, 0);

        this._playerModel.setBulletCount(0);
        this.setFireIntervalByRage(false);

        this._autoLockUI = new UIAutoPick();
       
        this._autoEffect = new cc.Sprite("#fishery_022.png");
        this._autoEffect.setPosition(0, 0);
        this._autoEffect.runAction(cc.repeatForever(cc.rotateBy(3, 360)));
        this._autoEffect.setVisible(false);
        this._autoEffect.setScale(1.2);
        this.addChild(this._autoEffect, -100);

        this._spEffect = new cc.Sprite("#fishery_022.png");
        this._spEffect.setPosition(0, 0);
        this._spEffect.runAction(cc.repeatForever(cc.rotateBy(3, 360)));
        this._spEffect.setVisible(false);
        this._spEffect.setScale(1.2);
        this.addChild(this._spEffect, -100);

         //智能捕魚提示框
         this._autoNotice = new cc.Sprite("#noticeBack.png");
         this._autoNotice.setPosition(0, 0);
        // this._autoNotice.runAction(cc.repeatForever(cc.rotateBy(3, 360)));
        this._autoNotice.setVisible(false);
       // this._autoNotice.setScale(1.2);
        this.addChild(this._autoNotice, -100);
 
         

        
    },
    /**
     * 改變智能捕魚特效位置
     */
    changeAutoEffectPosition: function(x, y){
        this._autoEffect.setPosition(x, y+10);
        this._spEffect.setPosition(x, y+125);
    },
    /**
     * 改變技能捕魚提示位置
     */
    changeAutoNoticePosition: function(x, y){
        this._autoNotice.setPosition(x, y);  
    },
    /**
     * 初始化数据
     * @param serverTime 服务器时间
     */
    initData: function (serverTime) {
        this._initUI();
        this._serverTime = serverTime;
        this._curTime = 0;
        this._lastTime = 0;
        this._countTime = 0;
        this._bFastBtnSubClick = [];
        for (var i = 0; i < 2; ++i)
            this._bFastBtnSubClick[i] = false;
        this._playerModel.setBulletCount(0);
        //破产检测
        this.checkPlayerIsGoBankrupt();
    },

    /**
     * 清除数据
     */
    clearData: function () {
        
        this._fortNode.removeAllChildren(true);
        this._roleInfoBD = null;
        this._spGoBankrupt = null;
        this._treasureHuntIcon = null;
        this._goldIcon = null;
        this._ragePromptLabel = null;
        this._skillUseIcon = null;
        this._spWaitText.setOpacity(255);
        this.setLocalPlayer(false);
        this.setAlive(false);
        this.unscheduleUpdate();
        this._ragePromptEffect = null;
        this._rageParticle = null;
    },

    /**
     * 设置玩家信息
     * @param model
     */
    setPlayerModel: function (model) {
        this._playerModel = model;
    },

    /**
     * 获取玩家信息
     * @returns {null}
     */
    getPlayerModel: function () {
        return this._playerModel;
    },

    /**
     * 设置玩家模型数据
     * @param data
     */
    setPlayerModelData: function (data) {
        
        this._playerModel.setGamePlayerId(data.strGamePlayerID);
        this._playerModel.setNickName(data.strNickname);
        this._playerModel.setPlayerLevel(data.nLevel);
        this._playerModel.setVipLevel(data.nVIPLevel);
        this._playerModel.setHeadId(data.nHeadID);
        this._playerModel.setGoldNum(data.nGoldNum);
        this._playerModel.setDiamondNum(data.nDiamondNum);
        this._playerModel.setSeatID(data.nSeatID);
        this._playerModel.setBulletList(data.bulletInfoRoom);
        this._playerModel.setFortType(data.nFortType);
        this._playerModel.setFortMode(data.nFortMode);
        this._playerModel.setFortData(data.nMaxFort);
        this._playerModel.setFortSkinUse(data.nFortSkin);
        this._playerModel.setDoleCoolDownTime(data.nDoleCoolDownTime);
        this._playerModel.setBulletHitCount(data.nRebound);        
    },

    /**
     * 设置是否是本地玩家
     * @param val bool值
     */
    setLocalPlayer: function (val) {
        this._bLocalPlayer = val;
    },

    /**
     * 获取是否是本地玩家
     * @returns {null}
     */
    isLocalPlayer: function () {
        return this._bLocalPlayer;
    },

    /**
     * 设置玩家被激活
     * @param val
     */
    setAlive: function (val) {
        this._bAlive = val;
    },

    /**
     * 获取玩家是否被激活
     * @returns {boolean}
     */
    isAlive: function () {
        return this._bAlive;
    },

    /**
     * 设置随机数对象
     * @param seed
     */
    setRandom: function (seed) {
        this._playerModel.setRandom(seed);
    },

    /**
     * 获取本地座位Id
     */
    getSeatID: function () {
        return this._playerModel.getSeatID();
    },

    /**
     * 
     * 關閉智能捕魚旋轉特效
     */
    openAutoLockEffect: function(){
        var self = this;
        if(!self._autoEffect.isVisible()){
            self._autoEffect.setVisible(true);
            self._spEffect.setVisible(true);
        }
    },
    closeAutoLockEffect: function(){
        var self = this;
        if(self._autoEffect.isVisible()){
            self._autoEffect.setVisible(false);
            self._spEffect.setVisible(false);
        }
    },
    /**
     * 获取锁定目标的触摸点
     */
    setTouch: function (touch) {
        var self = this;
        self._touch = touch;
        if(cf.autolock){
            cf.autolock = false;
            
            if(self._autoEffect.isVisible()){
                 self._autoEffect.setVisible(false);
                // this.closeAutoLockEffect();
                if(!self._autoNotice.isVisible()){
                    var _autoN = new cc.Sprite("#notice.png");
                    _autoN.setPosition(this._autoNotice.width / 2, this._autoNotice.height/2+6);
                    // this._autoNotice.runAction(cc.repeatForever(cc.rotateBy(3, 360)));
                
                    self._autoNotice.addChild(_autoN);
                    self._autoNotice.setVisible(true);
                    var obj = setTimeout(() =>{
                        self._autoNotice.setVisible(false);
                        _autoN.removeFromParent();
                        clearTimeout(obj);
                    }, 5000);
                }
            }
            
        }
        
        
        if (cf.SkillMgr.getLockSkillState(this.getSeatID()) || cf.SkillMgr.getRageSkillState(this.getSeatID())) {
            if (self._touch) {
                var fishMgr = cf.FishMgr;
                var fishMap = fishMgr.getFishMap();
                var length = fishMap.size();
                var localSprite;
                if (length > 0) {
                    var tmpPoint = cc.p(this._touch[0].getLocation().x, this._touch[0].getLocation().y);
                    cf.convertPosWithSeatId(tmpPoint);
                    for (var i = 0; i < length; ++i) {
                        var fish = fishMap._elements[i].value;
                        //屏幕内并且鱼可以检测碰撞
                        if (fish.checkInScreen()) {
                            //先检测外围圆
                            var fishPoint = cc.p(fish.getPosition().x, fish.getPosition().y);
                            fishPoint = cf.convertPosWithSeatId(fishPoint);
                            if (cc.pDistance(tmpPoint, fish.getPosition()) < 100) {
                                if (!localSprite) {
                                    localSprite = fish;
                                }
                                if (localSprite.getLockPriority() <= fish.getLockPriority()) {
                                    localSprite = fish;
                                }
                            }
                        }
                    }
                    if (localSprite && (localSprite != this.getLockElement())) {
                        self._playerLock(localSprite.getFishModel().getServerIndex());
                    }
                }
            }
        }
        self._touch = null;
    },

    //智能捕魚鎖定特效關掉
    AutoLockEffectClose: function(){
        if(this._spEffect.isVisible()){
            this._spEffect.setVisible(false);

        }
    },
    //智能捕魚
    setAutoLock: function(fishId){
        var self = this;
        //var seatId = cf.convertSeatId(self._playerModel.getSeatID());
        
        if (cf.SkillMgr.getLockSkillState(self.getSeatID()) || cf.SkillMgr.getRageSkillState(self.getSeatID())) {
            

                var fishMgr = cf.FishMgr;
                var fishMap = fishMgr.getFishMap();
                var length = fishMap.size();
                var localSprite;
                if (length > 0) {
                    var tmpPoint = cc.p(self._fortNode.getPosition().x, self._fortNode.getPosition().y);
                   
                   // var tmpPoint = cc.p(fishId.getLocation().x, fishId.getLocation().y);
                   // cf.convertPosWithSeatId(tmpPoint);
                   // for (var i = 0; i < length; ++i) {
                        //var fish = fishMap._elements[i].value;
                        //屏幕内并且鱼可以检测碰撞
                        // console.log(cc.pDistance(tmpPoint, fishId.getPosition()));
                        if (fishId.checkInScreen()) {

                            this.setLockElement(fishId);
                            //先检测外围圆
                            var fishPoint = cc.p(fishId.getPosition().x, fishId.getPosition().y);
                            fishPoint = cf.convertPosWithSeatId(fishPoint);
                            if (cc.pDistance(tmpPoint, fishId.getPosition()) < 100) {
                                
                                if (!localSprite) {
                                    localSprite = fishId;
                                }
                                if (localSprite.getLockPriority() <= fishId.getLockPriority()) {
                                    localSprite = fishId;
                                }
                            }
                        }
                   // }
                    if (localSprite && (localSprite != this.getLockElement())) {
                        //self._playerLock(localSprite.getFishModel().getServerIndex());
                        if(this._autoLockUI != null){
                            this._autoLockUI.checkBeiShuLV();
                        }else{
                            this.closeAutoLockEffect();
                            cf.autolock = false;
                            cf.autofishData = [];
                            this._playerShotFire();
                            cf.EffectMgr.stopLockEffect();
                            this._lockSkillEffEnd();
                        }
                    }
                }
            }
        

    },

    /**
     * 设置房间id
     * @param id
     */
    setRoomId: function (id) {
        this._roomId = id;
        this._cannonLimit = cf.Data.ROOM_DATA[id].cannonlevel_limit;
        if (this._roomId == 1005) {
            this._cannonLimit[0] = 36;
        }
        this._minCannonLevel = cf.Data.CANNON_LEVEL_DATA[this._cannonLimit[0]].cannonlevel;
    },

    getRoomId: function () {
        return this._roomId;
    },

    /**
     * 旋转UI
     * @remark 无座位id时用此方法旋转
     * @param index 实际座位id
     * @param isRotate 画布是否旋转
     */
    rotationUI: function (index, isRotate) {
        switch (index) {
            case 0://左下:逆时针旋转对应座位2
            case 1://右下：逆时针旋转对应座位3

                if (isRotate) {
                    
                    this._spWaitText.setRotation(180);
                }
                break;
            case 2://右上
            case 3://左上

                if (!isRotate) {
                    this._spWaitText.setRotation(180);
                }
                break;
        }
    },

    /**
     * 初始化UI
     * @private
     */
    _initUI: function () {
        var self = this;

        self._curPos.x = self.x;
        self._curPos.y = self.y;
        cf.convertPosWithSeatId(self._curPos);

        //隐藏等待炮座
        self._spWaitText.setOpacity(0);

        var fortBkWidth = self._spFortBk.width;
        var fortBkHeight = self._spFortBk.height;

        //获取反弹次数
        var hitCount = self._playerModel.getBulletHitCount();
        //创建主炮
        self._leftFort = new Fort(self._playerModel, Fort.FortPosID.LEFT, hitCount, self._roomId);
        self._leftFort.setPosition(-45, fortBkHeight / 2);
        self._fortNode.addChild(self._leftFort, 1);

        //创建副炮
        self._rightFort = new Fort(self._playerModel, Fort.FortPosID.RIGHT, hitCount,self._roomId);
        self._rightFort.setPosition(45, fortBkHeight / 2);
        self._fortNode.addChild(self._rightFort, 1);

        self._ragePromptLabel = new cc.LabelTTF(cf.Language.getText("text_1188"), cf.Language.FontName, 25);
        self._ragePromptLabel.setPosition(0, 140);
        self._ragePromptLabel.setColor(cc.color.YELLOW);
        self._ragePromptLabel.enableStroke(cc.color.RED, 1.5);
        //this._ragePromptLabel.setFontFillColor(cc.color(0, 254, 236));
        self._ragePromptLabel.setAnchorPoint(0.5, 0);
        self._ragePromptLabel.setVisible(false);
        self._fortNode.addChild(this._ragePromptLabel);

        //狂暴特效
        var ragePromptEffect = self._ragePromptEffect = new cc.Sprite("#skill_003.png");
        ragePromptEffect.setPosition(0, 45);
        ragePromptEffect.setVisible(false);
        self._fortNode.addChild(ragePromptEffect, -1);

        /*self._rageParticle = new cc.ParticleSystem(Res.GAME.particle_rage_plist);
        self._rageParticle.setPosition(0, 0);
        self._rageParticle.stopSystem();
        this._fortNode.addChild(self._rageParticle, -2);*/


        self._skillUseIcon = new cc.Sprite(ActionMgr.getFrame("fishery_004.png"));
        self._skillUseIcon.setAnchorPoint(0.5, 0.5);
        self._skillUseIcon.setPosition(0, 180);
        self._skillUseIcon.setScale(1.7);
        self._skillUseIcon.setOpacity(0);
        self._fortNode.addChild(this._skillUseIcon);

        //判断单双炮模式
        if (self._playerModel.getFortMode() == Player.FortMode.SINGLE) {
            self._rightFort.setVisible(false);
            self._leftFort.setPosition(0, fortBkHeight / 2);
        }

        //炮倍背景
        var fortNumBk = new cc.Sprite("#fishery_011.png");
        fortNumBk.setAnchorPoint(0.5, 0);
        fortNumBk.setPosition(0, 0);
       
        self._fortNode.addChild(fortNumBk, 2);

        //炮倍数字
        var fortNum = cf.Data.CANNON_LEVEL_DATA[self._playerModel.getFortType()].cannonlevel;
        self._labelFortNum = new ccui.LabelBMFont(fortNum, Res.LOBBY.font_resource_093);
        self._labelFortNum.setPosition(fortNumBk.width / 2, fortNumBk.height / 2 - 30);
        //self._labelFortNum.setColor(cc.color(18, 28, 97));
        fortNumBk.addChild(self._labelFortNum, 0);

        //创建分数面板
        var fortDataBk = new cc.Sprite("#fishery_030.png");
        fortDataBk.setOpacity(255);
        self._fortNode.addChild(fortDataBk, -1);

        //金币Label
        self._labelGold = new ccui.LabelBMFont(cf.numberToY(self._playerModel.getGoldNum(),2) + "", Res.LOBBY.font_resource_093);
        fortDataBk.addChild(self._labelGold, 0);
        self._labelGold.setAnchorPoint(0, 0.5);
        self._labelGold.setPosition(55, fortDataBk.height / 2+3);

        //金币icon
        var iconGold = self._goldIcon = new cc.Sprite("#resource_069.png");
        fortDataBk.addChild(iconGold, 0);
        iconGold.setScale(0.55);
        iconGold.setPosition(39, fortDataBk.height / 2+5);


        //玩家信息框
        self._roleInfoBD = new UIRoomRoleInfoBD(self._playerModel, self.isLocalPlayer());
        self._roleInfoBD.setPosition(0, 200);
        if (self._bLocalPlayer) {
            //self._roleInfoBD.setPosition(0, 310);
        }
        self._roleInfoBD.showRoleInfo(false);
        self._fortNode.addChild(self._roleInfoBD);

        var nickName = new cc.LabelBMFont(self._playerModel.getNickName(), Res.LOBBY.font_resource_093);
        nickName.setPosition(fortDataBk.width / 2, fortDataBk.height + 10);
        //nickName.setFontFillColor(cc.color(0, 255, 228));
        nickName.setScale(0.8);
        nickName.setVisible(false);
        fortDataBk.addChild(nickName, 0);

        //破产图标
        self._spGoBankrupt = new cc.Sprite("#fishery_009.png");
        self._spGoBankrupt.setPosition(0, 40);
        self._spGoBankrupt.setOpacity(0);
        self._fortNode.addChild(self._spGoBankrupt, 10);

        //根据座位id设置位置
        var seatId = cf.convertSeatId(self._playerModel.getSeatID());
        
        switch (seatId) {
            case 0://左下
                fortDataBk.setAnchorPoint(1, 0);
                fortDataBk.setPosition(-fortBkWidth / 2 + 10, 0);

                self._labelGold.setAnchorPoint(1,0.5);
                self._labelGold.setPosition(fortDataBk.width -55, fortDataBk.height / 2);
                iconGold.setPosition(17, fortDataBk.height / 2);
                self._goldPos.x = self._curPos.x - 320;
                self._goldPos.y = self._curPos.y + 60;
                break;
            case 1://右下
                fortDataBk.setAnchorPoint(0, 0);
                fortDataBk.setPosition(fortBkWidth / 2 - 15, 0);

                self._labelGold.setAnchorPoint(1,0.5);
                self._labelGold.setPosition(fortDataBk.width - 55, fortDataBk.height / 2);
                iconGold.setPosition(fortDataBk.width - 185, fortDataBk.height / 2);

                self._goldPos.x = self._curPos.x + 320;
                self._goldPos.y = self._curPos.y + 60;
                break;
            case 2://右上
                self._labelFortNum.setRotation(180);
                self._labelFortNum.setPositionY(fortNumBk.height / 2 - 30);

                fortDataBk.setFlippedY(true);
                fortDataBk.setFlippedX(true);
                fortDataBk.setAnchorPoint(1, 0);
                fortDataBk.setPosition(-fortBkWidth / 2 + 15, 0);

                self._labelGold.setAnchorPoint(1,0.5);
                self._labelGold.setPosition(55, fortDataBk.height / 2);
                self._labelGold.setRotation(180);
                iconGold.setPosition(185, fortDataBk.height / 2);
                iconGold.setRotation(180);

                self._roleInfoBD.setRotation(180);
                self._spGoBankrupt.setRotation(180);
                self._skillUseIcon.setRotation(180);
                self._ragePromptLabel.setRotation(180);

                self._goldPos.x = self._curPos.x + 320;
                self._goldPos.y = self._curPos.y - 60;

                nickName.setRotation(180);
                nickName.setPosition(fortDataBk.width / 2, fortDataBk.height -5 );
                break;
            case 3://左上
                self._labelFortNum.setRotation(180);
                self._labelFortNum.setPositionY(fortNumBk.height / 2 - 30);

                fortDataBk.setFlippedY(true);
                fortDataBk.setFlippedX(true);
                fortDataBk.setAnchorPoint(0, 0);
                fortDataBk.setPosition(fortBkWidth / 2 -10, 0);

                self._labelGold.setPosition(fortDataBk.width - 55, fortDataBk.height / 2);
                self._labelGold.setRotation(180);
                iconGold.setPosition(fortDataBk.width - 17, fortDataBk.height / 2);
                iconGold.setRotation(180);

                self._roleInfoBD.setRotation(180);
                self._spGoBankrupt.setRotation(180);
                self._skillUseIcon.setRotation(180);
                self._ragePromptLabel.setRotation(180);

                self._goldPos.x = self._curPos.x - 320;
                self._goldPos.y = self._curPos.y - 60;

                nickName.setRotation(180);
                nickName.setPosition(fortDataBk.width / 2, fortDataBk.height -5 );
                break;
        }

        //转换坐标系为旋转节点坐标
        cf.convertPosWithSeatId(self._goldPos);
        self.scheduleUpdate();

        //创建加减炮按钮
        //如果是本地玩家才创建
        if (self.isLocalPlayer()) {
            var btnAdd = new ccui.Button();
            btnAdd.loadTextureNormal("button_001.png", ccui.Widget.PLIST_TEXTURE);
            btnAdd.setPressedActionEnabled(true);
            btnAdd.setAnchorPoint(0.5, 0);
            btnAdd.setPosition(fortBkWidth / 2 - 55, -10);
            btnAdd.addClickEventListener(function () {
                if(!cf.btnDisable){
                    self._changeFort(true);
                }
            });
            self._fortNode.addChild(btnAdd, 3);

            var btnCut = new ccui.Button();
            btnCut.loadTextureNormal("button_002.png", ccui.Widget.PLIST_TEXTURE);
            btnCut.setPressedActionEnabled(true);
            btnCut.setAnchorPoint(0.5, 0);
            btnCut.setPosition(-fortBkWidth / 2 + 55, -10);
            btnCut.addClickEventListener(function () {    
                if(!cf.btnDisable){
                    self._changeFort(false);
                }
            });
            self._fortNode.addChild(btnCut, 3);

            //初始化炮倍
            self._curFortType = self._playerModel.getFortType();

            //解锁图标
            self._spUnlockIcon = new cc.Sprite("#resource_082.png");
            self._spUnlockIcon.setAnchorPoint(0.5, 0);
            self._spUnlockIcon.setPosition(0, fortBkHeight / 2);
            self._spUnlockIcon.setScale(0.5);
            self._spUnlockIcon.setVisible(false);
            self._fortNode.addChild(self._spUnlockIcon, 3);

            self._unlockPrompt = new ccui.Text("",cf.Language.FontName,23);
            self._unlockPrompt.setPosition(0,200);
            self._unlockPrompt.setVisible(false);
            self._fortNode.addChild(self._unlockPrompt,10);

            //创建快捷键
            //換炮(目前隱藏中)
            var btnData = [
                "fishery_016.png",
                "fishery_016.png",
                "fishery_016.png"
                //"fishery_015.png"
            ];
            self._fastBtnArray = [];
            for (var i = 0; i < 2; ++i) {
                var btn = self._fastBtnArray[i] = new ccui.Button();
                btn.loadTextureNormal(btnData[i], ccui.Widget.PLIST_TEXTURE);
                btn.setPosition(0, fortBkHeight / 2);
                if (i == 0) {
                    btn.setPosition(0, fortBkHeight / 2 - 10);
                    btn.setOpacity(0);
                } else {
                    //btn.setTouchEnabled(false);
                    btn.setVisible(false);
                }
                btn.setTag(Player.FastTag.FAST + i);
                btn.setSwallowTouches(true);
                btn.addTouchEventListener(self._fastTouchEvent, this);
                self._fortNode.addChild(self._fastBtnArray[i], 10);
            }
            self._bFastBtn = false;
            nickName.setVisible(true);
            nickName.setColor(cc.color.GREEN);

            var tipBK = new cc.Sprite("#playerTitle.png");
            tipBK.setScale(1);
            // tipBK.setScaleX(0.25);
            // tipBK.setScaleY(0.6);
            tipBK.setPosition(fortDataBk.width/2,45);
            fortDataBk.addChild(tipBK,-1);
        } else {
            var btnRoleInfo = new ccui.Button();
            btnRoleInfo.loadTextureNormal("fishery_010.png", ccui.Widget.PLIST_TEXTURE);
            btnRoleInfo.setPosition(0, fortBkHeight / 2);
            btnRoleInfo.setScaleX(0.65);
            btnRoleInfo.setOpacity(0);
            btnRoleInfo.addClickEventListener(function () {
                cf.SoundMgr.playEffect(8);
                self.showRoleInfo(!self._roleInfoBD.isVisible());
            });
            self._fortNode.addChild(btnRoleInfo, 0);
        }
    },

    
    
    /**
     * 设置炮台角度
     * @param leftPoint 左炮台位置
     * @param rightPoint 右炮台位置
     */
    setFortAngle: function (leftPoint, rightPoint) {
        if (leftPoint) {
            this._setLeftFortAngle(leftPoint);
            if (rightPoint === undefined || rightPoint == null) {
                rightPoint = leftPoint;
            }
        }
        if (this._playerModel.getFortMode() == Player.FortMode.DOUBLE) {
            this._setRightFortAngle(rightPoint);
        }
    },

    /**
     * 计算炮台角度
     * @param startPos
     * @param endPos
     * @returns {number}
     */
    _calculateFortAngle: function (startPos, endPos) {
        //计算方向分量
        var dirX = endPos.x - startPos.x;
        var dirY = endPos.y - startPos.y;
        //计算方向夹角
        var atanVal = Math.atan2(dirY, dirX);
        var angle = cc.radiansToDegrees(atanVal);
        //炮台向下为正方向
        var curAngle = 90 - angle;
        //根据座位转换角度
        return cf.convertAngleWithSeatId(this._playerModel.getSeatID(), curAngle);
    },

    /**
     * 设置左炮角度
     * @param touchPoint
     * @private
     */
    _setLeftFortAngle: function (touchPoint) {
        var pos = this.convertToWorldSpace(this._leftFort.getPosition());
        //根据座位转换坐标
        cf.convertPosWithSeatId(pos);
        this._leftFortAngle = this._calculateFortAngle(pos, touchPoint);
        this._leftFort.setFortAngle(this._leftFortAngle);
    },

    /**
     * 设置右炮角度
     * @param touchPoint
     * @private
     */
    _setRightFortAngle: function (touchPoint) {
        var pos = this.convertToWorldSpace(this._rightFort.getPosition());
        //根据座位转换坐标
        cf.convertPosWithSeatId(pos);
        this._rightFortAngle = this._calculateFortAngle(pos, touchPoint);
        this._rightFort.setFortAngle(this._rightFortAngle);
    },

    /**
     * 设置开炮间隔
     * @param state  { boolean }  间隔时间
     */
    setFireIntervalByRage: function (state) {
        var self = this;
        if (state) {
            self._autoFireInterval = cf.fireInterval.rageFireInterval;
        }
        else {
            self._autoFireInterval = cf.fireInterval.initialFireInterval;
        }
    },

    /**
     * 逻辑更新
     * @param dt
     */
    update: function (dt) {
        this._curTime += dt;
        //救济金倒计时
        this.coutingDown(dt);
        //开炮
        if (this.checkCanFire()) {
            if (this._bLocalPlayer) {
                this._onAutoFire();
            }
            
        }
        this.checkPlayerIsGoBankrupt();
        if (this._bLocalPlayer) {
            this._clearLockTarget(dt);
            //检测本地的技能冷却结束
            this._iceEffCountDown(dt);
            this._summonSkillCoolDown(dt);
        }
        //为了解除玩家对精灵的减速效果
        this._lockEffCountDown(dt);
        this._rageEffCountDown(dt);
    },

    /**
     * 检测是否可以开炮
     * @returns {boolean}
     */
    checkCanFire: function () {
        var fireCost = this._minCannonLevel * this._playerModel.getFortMode();
        if (cf.SkillMgr.getRageSkillState(this.getSeatID())) {
            fireCost *= 2;
        }
        this._bFortCanFire = (this._playerModel.getGoldNum() >= fireCost);
        return this._bFortCanFire;
    },

    /**
     * set _bAutoFire
     */
    setBAutoFireState: function (state) {
        if (this._bAutoFire != state) {
            this._lastAutoOnFire = this._bAutoFire;
            this._bAutoFire = state;
        }
    },

    getAutoFireState: function () {
        return this._bAutoFire;
    },

    /**
     * 继续游戏前如果自动发炮为开启状态 则继续开炮状态
     */
    continueAutoFire: function () {
        if (this._lastAutoOnFire == true) {
            this._lastAutoOnFire = this._bAutoFire;
            this.setBAutoFireState(true);
            cf.CanContinueAutoFire = true;
        }
    },

    /**
     * 自动开炮
     */
    _onAutoFire: function () {
        if(this._bUnlock){
            return;
        }
        if (this._bAutoFire || this._bTouchFire) {
            this.fire();
        }
    },

    /**
     * 龍珠技能開炮
     */
    dragonSkillFire:function(sender){
        
        var curTime = (new Date()).getTime();
        //创建子弹信息
        var bulletMsg = new $root.SC_PlayerFireInRoomMsg();
        bulletMsg.nSeatID = this._playerModel.getSeatID();
        bulletMsg.fAngle = [];
        bulletMsg.fAngle[0] = this._leftFortAngle;//this._leftFort.getFortAngle();
        bulletMsg.fAngle[1] = 0;
        bulletMsg.vecBulletIndex = [];
        bulletMsg.vecBulletIndex[0] = this._playerModel.getIdLogin() + "-" + curTime + "-0";
        bulletMsg.vecBulletIndex[1] = "";
        //子弹类型根据炮倍读表获取
        bulletMsg.nBulletLevel = 1;
        //子弹发射时间
        bulletMsg.strBulletTime = curTime;

        //判断是否是双炮模式
        if (this._playerModel.getFortMode() == Player.FortMode.DOUBLE) {
                this.tansFormFortMode();
                this.setFortModeSingle();
            
        }
        if (this.isLocalPlayer()) {
            //开炮音效
            cf.SoundMgr.playEffect(7);
        }
              //发送开炮消息
            var msg = new $root.CS_PlayerFireMsg();
            msg.fAngleLeft = bulletMsg.fAngle[0];
            msg.fAngleRight = bulletMsg.fAngle[1];
            msg.strBulletIndexLeft = bulletMsg.vecBulletIndex[0];
            msg.strBulletIndexRight = bulletMsg.vecBulletIndex[1];
            //msg.bulletType = -1;
            msg.bulletType = sender;
            var wr = $root.CS_PlayerFireMsg.encode(msg).finish();
            cf.NetMgr.sendSocketMsg(MessageCode.CS_PlayerFireMsg, wr);

        
       
      

       // modifySkillFortSkin

    },
    /**
     * 开炮逻辑
     */
    fire: function () {

        if (this._playerModel.getBulletCount() > cf.BULLET_COUNT) {
            return;
        }

        if (this._curTime - this._lastTime > this._autoFireInterval) {
            this._lastTime = this._curTime;
            //当前时间
            var curTime = (new Date()).getTime();

            //创建子弹信息
            var bulletMsg = new $root.SC_PlayerFireInRoomMsg();
            bulletMsg.nSeatID = this._playerModel.getSeatID();
            bulletMsg.fAngle = [];
            bulletMsg.fAngle[0] = this._leftFortAngle;//this._leftFort.getFortAngle();
            bulletMsg.fAngle[1] = 0;
            bulletMsg.vecBulletIndex = [];
            bulletMsg.vecBulletIndex[0] = this._playerModel.getIdLogin() + "-" + curTime + "-0";
            bulletMsg.vecBulletIndex[1] = "";
            
            //子弹类型根据炮倍读表获取
            bulletMsg.nBulletLevel = 1;
            //子弹发射时间
            bulletMsg.strBulletTime = curTime;
            
            
            //发送开炮消息
            var msg = new $root.CS_PlayerFireMsg();
            msg.fAngleLeft = bulletMsg.fAngle[0];
            msg.fAngleRight = bulletMsg.fAngle[1];
            msg.strBulletIndexLeft = bulletMsg.vecBulletIndex[0];
            msg.strBulletIndexRight = bulletMsg.vecBulletIndex[1];
            

            // if(cf.dragon_gold){
            //     msg.bulletType = 0;
            //     if (this._playerModel.getFortMode() == Player.FortMode.DOUBLE) {
            //         this.tansFormFortMode();
            //         this.setFortModeSingle();
                    
            //     }
            //     cf.goldBullet --;
                
            //     cf.dispatchEvent(BaseRoom.DoEvent, {
            //     flag: BaseRoom.EventFlag.UPDATE_DRAGONSKILL_UI,
            //     type: 0,
            //     bulletNum: cf.goldBullet,
            //     seatId: this._playerModel.getSeatID()
                     
            //     });
            // }else if (cf.dragon_wood){

            //     msg.bulletType = 1;
                
            //     if (this._playerModel.getFortMode() == Player.FortMode.DOUBLE) {
            //         this.tansFormFortMode();
            //         this.setFortModeSingle();
                    
            //     }
            //     cf.woodBullet --;
                 
               
            //     cf.dispatchEvent(BaseRoom.DoEvent, {
            //     flag: BaseRoom.EventFlag.UPDATE_DRAGONSKILL_UI,
            //     type: 1,
            //     bulletNum: cf.woodBullet,
            //     seatId: this._playerModel.getSeatID()
            //     });
                

            // }else{
                
                msg.bulletType = -1;
                
            //}

            //判断是否是双炮模式
            if (this._playerModel.getFortMode() == Player.FortMode.DOUBLE) {
                bulletMsg.fAngle[1] = this._rightFortAngle;
                bulletMsg.vecBulletIndex[1] = this._playerModel.getIdLogin() + "-" + curTime + "-1";
            }

            var wr = $root.CS_PlayerFireMsg.encode(msg).finish();
            cf.NetMgr.sendSocketMsg(MessageCode.CS_PlayerFireMsg, wr);

            //读表获取对应的炮倍
            if (this._playerModel.getFortMode() == Player.FortMode.DOUBLE) {
                this._playerModel.setBulletCountOffset(2);
            } else {
                this._playerModel.setBulletCountOffset(1);
            }
            this.onFireAfter(bulletMsg);

            if (this.isLocalPlayer()) {
                
                      //开炮音效
                    cf.SoundMgr.playEffect(7);
                
              
            }

            // if (cf.autolock){
            //     if (cf.autolockBulletNum > 0){
            //         cf.autolockBulletNum -= 1;
                    
            //         if(cf.autolockBulletNum <= 0){
            //             this.closeAutoLockEffect();
            //             cf.autolock = false;
            //             cf.autofishData = [];
            //             this._playerShotFire();
            //             cf.EffectMgr.stopLockEffect();
            //             this._lockSkillEffEnd();
            //             if(!this._autoNotice.isVisible()){
            //                 var _autoN = new cc.Sprite("#notice2.png");
            //                 _autoN.setPosition(this._autoNotice.width / 2, this._autoNotice.height/2+6);
            //                 // this._autoNotice.runAction(cc.repeatForever(cc.rotateBy(3, 360)));
                        
            //                 this._autoNotice.addChild(_autoN);
            //                 this._autoNotice.setVisible(true);
            //                 var obj = setTimeout(() =>{
            //                     this._autoNotice.setVisible(false);
            //                     _autoN.removeFromParent();
            //                     clearTimeout(obj);
            //                 }, 5000);
            //             }
                        
            //         }
            //     }
            // }
        }
    },

    /**
     * 开炮前
     */
    onFireBefore: function () {
        if(cf.btnDisable){
            if(cf.goldwoodDisable){
                if (!this._bAutoFire && !this._bTouchFire) {
                    this._bTouchFire = true;
                }
            }

        }else{
            
            if (!this._bAutoFire && !this._bTouchFire) {
                this._bTouchFire = true;
            }

        }
        
    },

    /**
     * 开炮后
     * @param msg 子弹消息 SC_PlayerFireInRoomMsg
     */
    onFireAfter: function (msg) {
        //判断是否是双炮模式
        if (cf.SkillMgr.getLockSkillState(this.getSeatID()) && !this._bLocalPlayer && this._lockElement && this._lockElement.checkCanLock()) {
            //cc.log("使用本地的发射角度调整");
            this.setFortAngle(this.getLockElement().getPosition());
            msg.fAngle[0] = this._leftFortAngle;//this._leftFort.getFortAngle();
            msg.fAngle[1] = this._rightFortAngle;//this._rightFort.getFortAngle();            
        }
        if (this._playerModel.getFortMode() == Player.FortMode.DOUBLE) {
            this._rightFort.onFire(this, msg);
            this._rightFort.setFortAngle(msg.fAngle[1]);
        }
        this._leftFort.onFire(this, msg);
        this._leftFort.setFortAngle(msg.fAngle[0]);
    },

    /**
     * 开炮结束
     */
    onFireEnd: function () {
        //cc.log("===玩家停止开炮===");
        this._bTouchFire = false;
    },

    /**
     * 同步子弹
     */
    synchronousBullet: function () {
        if (this._playerModel) {
            var len = this._playerModel._bulletList.length;
            for (var i = 0; i < len; ++i) {
                //获取子弹信息
                var bulletInfo = this._playerModel._bulletList[i];
                switch (bulletInfo._nFortPosID) {
                    case Fort.FortPosID.LEFT:
                        this._leftFort.synchronousBullet(this, this._serverTime, bulletInfo);
                        break;
                    case Fort.FortPosID.RIGHT:
                        this._rightFort.synchronousBullet(this, this._serverTime, bulletInfo);
                        break;
                }
            }
        }
    },

    /**
     * 更新玩家数据
     * @param dataType 数据类型
     * @param val 数值
     */
    updatePlayerData: function (dataType, val) {
        if (val == null)
            return;
        switch (dataType) {
            case Player.DataType.GOLD://玩家金币
                this._playerModel.setGoldNum(val);
                break;
            case Player.DataType.DIAMOND://玩家钻石
                this._playerModel.setDiamondNum(val);
                break;
            case Player.DataType.EXP://玩家经验
                this._playerModel.setExp(val);
                break;
            case Player.DataType.CUR_FORT://当前使用炮倍
                this._curFortType = val;
                this._playerModel.setFortType(val);
                break;
            case Player.DataType.MAX_FORT://最大解锁炮倍
                this._playerModel.setFortData(val);
                break;
            case Player.DataType.LEVEL://玩家等级
                this._playerModel.setPlayerLevel(val);
                break;
            case Player.DataType.SKIN://炮台皮肤
                this._playerModel.setFortSkinUse(val);
                break;
            case Player.DataType.ADD_SKIN://解锁增加炮台
                this._playerModel.addFortSkin(null, val, 0);
                break;
            case Player.DataType.FACC_Points://增加积分
                this._playerModel.setFAccPoints(val);
                break;
            case Player.DataType.LOTTERY:
                this._playerModel.setLottery(val);
                break;
        }
    },

    /**
     * 刷新UI
     */
    refreshUI: function () {
        this._labelGold.setString(cf.numberToY(this._playerModel.getGoldNum(),2) + "");
        //每次金币改变检测破产状态
        this.checkPlayerIsGoBankrupt();
    },

    /**
     * 切换炮倍
     * @param bFortUp true增加，false减少
     * @private
     */
    _changeFort: function (bFortUp) {
        cf.SoundMgr.playEffect(9);
        if(bFortUp) {
            this._curFortType++;
            if(this._curFortType > this._cannonLimit[1]) {
                this._curFortType = this._cannonLimit[1];
            }
            if(this._bUnlock) {
                this._bUnlock = false;
                if(this._playerModel.getFortType() == this._cannonLimit[0] && !cf.playerHasPay) {
                    this.modifyFort(true, this._cannonLimit[0]);
                    return
                }
            }
            else {
                if(this._playerModel.getFortData() < parseInt(this._cannonLimit[1])) {
                    if(this._playerModel.getPlayerLevel() < this._curFortType && this._playerModel.getPlayerLevel() < 15 && !cf.playerHasPay) {
                        this.modifyFort(false, this._curFortType);
                        return;
                    }
                    else if(this._curFortType >= cf.Data.SEVERCONFIGDATA[0].unlockFortLv && !cf.playerHasPay) {
                        this.modifyFort(false, this._curFortType);
                        return;
                    }
                }
            }
        }
        else {
            this._curFortType--;
            //初始逻辑
            // if(this._curFortType < this._cannonLimit[0]) {
            //     this._curFortType = this._cannonLimit[0];
            // }
            // if(this._bUnlock) {
            //     this._bUnlock = false;
            //     this.modifyFort(true, this._curFortType);
            //     return
            // }
            if(this._curFortType < this._cannonLimit[0]) {
                if(this._playerModel.getFortData() + 1 <= this._cannonLimit[1]) {
                    this._curFortType = this._playerModel.getFortData() + 1;
                }
                else {
                    this._curFortType = this._cannonLimit[1];
                }
            }
            if(this._playerModel.getFortData() < parseInt(this._cannonLimit[1])) {
                if(this._playerModel.getPlayerLevel() < this._curFortType && this._playerModel.getPlayerLevel() < 15 && !cf.playerHasPay) {
                    this.modifyFort(false, this._curFortType);
                    return;
                }
                else if(this._curFortType >= cf.Data.SEVERCONFIGDATA[0].unlockFortLv && !cf.playerHasPay) {
                    this.modifyFort(false, this._curFortType);
                    return;
                }
            }
            if(this._bUnlock) {
                this._bUnlock = false;
                if(this._playerModel.getFortType() != parseInt(this._cannonLimit[0])) {
                    this.modifyFort(true, this._curFortType);
                    return;
                }
                this.modifyFort(true, this._curFortType);
            }
        }
        var msg = new $root.CS_ModifyFort();
        msg.bUpFort = bFortUp;
        var wr = $root.CS_ModifyFort.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_ModifyFort, wr);
    },

    /**
     * 更新炮倍
     * @param type 炮倍索引
     */
    modifyFort: function (bUnlock,type) {
        var fortNum = cf.Data.CANNON_LEVEL_DATA[type].cannonlevel;
        if (bUnlock) {
            //已解锁
            this._bUnlock = false;
            //更新数据
            if (this._spUnlockIcon)
                this._spUnlockIcon.setVisible(false);
            if(this._unlockPrompt) {
                this._unlockPrompt.setVisible(false);
                this._unlockPrompt.removeAllChildren();
            }
            this.updatePlayerData(Player.DataType.CUR_FORT, type);

            //更换皮肤
            this.modifyFortSkin();
        } else {
            //未解锁
            this._bUnlock = true;
            //cc.log("炮台未解锁，炮倍 = " + type);
            this._spUnlockIcon.setVisible(true);
            this._unlockPrompt.setVisible(true);
            var cutLevel = this._playerModel.getFortData() - this._playerModel.getPlayerLevel() + 1;

            this._unlockPrompt.setString(cf.Language.getText("text_1550")+ cutLevel +cf.Language.getText("text_1551") + fortNum + cf.Language.getText("text_1020"));
            var unlockOfPay = new ccui.Text(cf.Language.getText("text_1547"),cf.Language.FontName,23);
            unlockOfPay.setPosition(this._unlockPrompt.width / 2,-30);
            unlockOfPay.setColor(cc.color.YELLOW);
            this._unlockPrompt.addChild(unlockOfPay,10);

            var baseBoard = new cc.Scale9Sprite("resource_121.png");
            baseBoard.setPosition(this._unlockPrompt.width / 2, -10);
            baseBoard.setContentSize(this._unlockPrompt.width + 40,100);
            this._unlockPrompt.addChild(baseBoard,-1);            
        }
        this._labelFortNum.setString(fortNum + "");
    },

    /**
     * 检测玩家是否破产(本地玩家的)
     */
    checkPlayerIsGoBankrupt: function () {
        var self = this;
        var fireCost = this._minCannonLevel * this._playerModel.getFortMode();
        var goldNum = self._playerModel.getGoldNum();

        if (goldNum >= fireCost) {
            //设置破产状态
            if (self._bankrupt) {


                //解除破产
                self.setPlayerBankrupt(false);

                //救济金
                if (this.isLocalPlayer()) {
                    if (self._doleBtn) {
                        self._doleBtn.setVisible(false);
                    }
                    if(self._doleHasAllGetPrompt) {
                        self._doleHasAllGetPrompt.setVisible(false);
                    }
                }
            }
        } else {
            //设置破产状态
            if (!self._bankrupt) {
                
                var bulletIsEmpty = cf.BulletMgr.bulletIsEmpty(self._playerModel.getSeatID());
                if (bulletIsEmpty) {
                    //破产
                    self.setPlayerBankrupt(true);

                    //救济金
                    if (this.isLocalPlayer()) {
                        this.checkPlayerGetDole();
                    }
                }
            }
        }
    },

    /**
     * 设置玩家破产状态
     * @param bBankrupt
     */
    setPlayerBankrupt: function (bBankrupt) {
        var self = this;
        self._bankrupt = bBankrupt;
        if (bBankrupt) {
            //破产
            if (this._spGoBankrupt) {
                this._spGoBankrupt.setOpacity(255);
            }

            cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UPDATE_AUTO_UI);

            this.onFireEnd();
            this._bAutoFire = false;
            this._bTouchFire = false;
            this._lastAutoOnFire = false;
            if (cf.autolock){
                        this.closeAutoLockEffect();
                        cf.autolock = false;
                        cf.autofishData = [];
                        this._playerShotFire();
                        cf.EffectMgr.stopLockEffect();
                        this._lockSkillEffEnd();
                        cf.SkillMgr.resetLockSkill(this.getSeatID());
                        

            }
            
            this.addPromptOfAutoFire();
        } else {
            if (this._spGoBankrupt) {
                this._spGoBankrupt.setOpacity(0);
            }
        }
    },

    /**
     * 检测玩家能否领取救济金
     */
    checkPlayerGetDole: function () {
        var self = this;

        if (!self._waitForGet) {
            if (self._playerModel) {
                var msg = new $root.CS_DoleCoolDown();
                msg.nSeatID = self._playerModel.getSeatID();
                var wr = $root.CS_DoleCoolDown.encode(msg).finish();
                cf.NetMgr.sendSocketMsg(MessageCode.CS_DoleCoolDownMsg, wr);
            } else {
                cc.error("倒计时消息错误:CS_DoleCoolDownMsg");

                //弹出错误提示
            }
        }
        else {
            self.setDoleUiState(true);
        }
    },

    /**
     * 设置布尔变量waitForGet的值
     * @param state { boolean }
     */
    setStateWaitForGet: function (state) {
        var self = this;
        self._waitForGet = state;
    },

    /**
     * 初始化救济金ui显示
     * @param parsedMsg { object } 消息解析后的信息
     */
    initDoleUi: function (parsedMsg) {
        var self = this;
        //self._waitForGet = true;
        if (!self._doleBtn && !self._countDownLabel) {
            //cc.log("初始化救济金图标");
            self._doleBtn = new ccui.Button("fishery_026.png", null, null, 1);
            self._doleBtn.setAnchorPoint(cc.p(0.5, 0.5));
            self._doleBtn.setTag(10001);
            self._doleBtn.setPosition(cc.p(0, 200));
            self._doleBtn.setPressedActionEnabled(true);
            self._fortNode.addChild(self._doleBtn);
            self._doleBtn.addClickEventListener(function (sender) {
                cf.SoundMgr.playEffect(8);
                if (self._isCountDown) {
                    //不满足条件，显示商店
                    cf.UITools.showHintToast("不满足救济金领取条件");
                }
                else {
                    if (self._playerModel) {
                        var msg = new $root.CS_DoleGet();
                        msg.nSeatID = self._playerModel.getSeatID();
                        var wr = $root.CS_DoleGet.encode(msg).finish();
                        cf.NetMgr.sendSocketMsg(MessageCode.CS_DoleGetMsg, wr);
                        cf.UITools.showLoadingToast(cf.Language.getText("text_1012"), 10, false, cf.RoomMgr.getRoom()._delegate);
                    } else {
                        cc.error("救济金获取消息错误:CS_DoleGetMsg");
                    }
                }
            });
            self._effect = new cc.Sprite(ActionMgr.getFrame("fishery_022.png"));
            self._effect.setAnchorPoint(cc.p(0.5, 0.5));
            self._effect.setTag(10001);
            self._effect.setScale(1.5);
            self._effect.setPosition(cc.p(self._doleBtn.width / 2, self._doleBtn.height / 2));
            self._effect.setVisible(false);
            self._doleBtn.addChild(self._effect, -1);
            self._effect.runAction(cc.repeatForever(cc.rotateBy(1, 60)));

            var doleGetNum = 0;
            if(parsedMsg.nDoleID && parsedMsg.nDoleID > 0) {
                doleGetNum = parseInt(parsedMsg.nDoleID) - 1;
            }
            var doleLabel = new cc.LabelBMFont(cf.Data.DOLE_DATA[this._playerModel.getVipLevel()].doleInfo[doleGetNum][1], Res.LOBBY.font_resource_083);
            doleLabel.setAnchorPoint(0.5, 0.5);
            doleLabel.setScale(0.7);
            doleLabel.setPosition(self._doleBtn.width / 2, 30);

            self._doleBtn.addChild(doleLabel);
            self._countDownTime = parsedMsg.strCoolDownTime;
            if (self._countDownTime > 0) {
                self._isCountDown = true;
                var str = cf.transFormTime(self._countDownTime);
                self._countDownLabel = new cc.LabelTTF(str, cf.Language.FontName, 25);
                self._countDownLabel.setAnchorPoint(cc.p(0.5, 0.5));
                self._countDownLabel.enableStroke(cc.color.BLUE, 2);
                self._countDownLabel.setFontFillColor(cc.color(0, 254, 236));
                self._countDownLabel.setPosition(cc.p(self._doleBtn.width / 2, -2));
                self._doleBtn.addChild(self._countDownLabel);
                self._effect.setVisible(false);
            }
            else if (self._countDownTime < 0) {
                self._countDownLabel = new cc.LabelTTF(cf.Language.getText("text_1018"), cf.Language.FontName, 25);
                self._countDownLabel.setAnchorPoint(cc.p(0.5, 0.5));
                self._countDownLabel.enableStroke(cc.color.BLUE, 2);
                self._countDownLabel.setFontFillColor(cc.color(0, 254, 236));
                self._countDownLabel.setPosition(cc.p(self._doleBtn.width / 2, -2));
                self._doleBtn.addChild(self._countDownLabel);
                self._effect.setVisible(true);
            }
            if(self._doleHasAllGetPrompt) {
                self._doleHasAllGetPrompt.setVisible(false);
            }
        }
        else {
            self._countDownTime = parsedMsg.strCoolDownTime;
            self.setDoleUiState(true);
            if(self._doleHasAllGetPrompt) {
                self._doleHasAllGetPrompt.setVisible(false);
            }
            if (self._countDownTime > 0) {
                self._isCountDown = true;
                self._countDownLabel.setString(cf.transFormTime(self._countDownTime));
                self._effect.setVisible(false);
                self.setStateWaitForGet(false);
            }
            else if (self._countDownTime < 0) {
                self._countDownLabel.setString(cf.Language.getText("text_1018"));
                self._effect.setVisible(true);
                self.setStateWaitForGet(true);
            }
        }
    },

    /**
     * 救济金已经完成领取
     */
    doleHasGetAllTimes: function () {
        var self = this;
        if(!self._doleHasAllGetPrompt) {
            self._doleHasAllGetPrompt = new ccui.Text(cf.Language.getText("text_1535"),cf.Language.FontName,23);
            self._doleHasAllGetPrompt.setPosition(0,200);
            self._fortNode.addChild(self._doleHasAllGetPrompt);

            var childAllGetPrompt = new ccui.Text(cf.Language.getText("text_1536"),cf.Language.FontName,23);
            childAllGetPrompt.setPosition(self._doleHasAllGetPrompt.width / 2,-26);
            childAllGetPrompt.setColor(cc.color(255, 255, 0));
            self._doleHasAllGetPrompt.addChild(childAllGetPrompt);

            var baseBoard = new cc.Scale9Sprite("resource_121.png");
            baseBoard.setPosition(self._doleHasAllGetPrompt.width / 2 , -5);
            baseBoard.setContentSize(childAllGetPrompt.width + 10,90);
            self._doleHasAllGetPrompt.addChild(baseBoard,-1);
        }
        else {
            self._doleHasAllGetPrompt.setVisible(true);
        }
    },

    /**
     * 设置救济金ui显隐状态
     * @param state { boolean }
     */
    setDoleUiState: function (state) {
        this._doleBtn.setVisible(state);
        this._countDownLabel.setVisible(state);
    },

    /**
     * 冷却时间未到重新设置计时时间并开始计时
     */
    setCountTimeAndCountDownL: function (parseGetMsg) {
        var self = this;
        self._countDownTime = parseGetMsg.nLastTime;
        self._isCountDown = true;
        self._countDownLabel.setString(cf.transFormTime(self._countDownTime));
    },
    /**
     * 计时函数
     */
    coutingDown: function (dt) {
        var self = this;
        if (self._isCountDown) {
            self._countTime += dt;
            if (self._countTime >= 1) {
                self._countDownTime -= 1;
                self._countTime = 0;
                self._countDownLabel.setString(cf.transFormTime(self._countDownTime));
                if (self._countDownTime <= 0) {
                    self._countDownLabel.setString(cf.Language.getText("text_1018"));
                    self._isCountDown = false;
                    self._effect.setVisible(true);
                    self.setStateWaitForGet(true);
                }
            }
        }
    },

    /**
     * 显示快捷键
     * @param bShow
     */
    showFastBtn: function (bShow) {
        this._bFastBtn = bShow;
        var self = this;
        for (var i = 1; i < 2; ++i) {
            var moveY = 110, fade = 255;
            //var moveAction = cc.moveBy(0.6, -60 + (i - 1) * 120, moveY);
            var moveAction = cc.moveBy(0.6, 0, moveY);
            self._fastBtnArray[i].stopAllActions();
            if (!bShow) {
                self._fastBtnArray[i].setTouchEnabled(false);
                moveAction = moveAction.reverse();
                fade = 0;
            } else {
                fade = 255;
                self._fastBtnArray[i].setVisible(true);
                self._fastBtnArray[i].setTouchEnabled(true);
            }
            var spawn = cc.spawn(
                cc.fadeTo(0.4, fade),
                moveAction.easing(cc.easeBackOut())
            );
            var seq = cc.sequence(
                spawn,
                cc.callFunc(function (node, idx) {
                    if (!bShow) {
                        node.setVisible(false);
                        self._bFastBtnSubClick[idx] = false;
                    } else {
                        self._bFastBtnSubClick[idx] = true;
                    }
                    self._bClickFast = true;
                }, self._fastBtnArray[i], i)
            );
            self._fastBtnArray[i].runAction(seq);
        }
    },

    /**
     * 快捷键触摸事件
     * @param sender
     * @param type
     * @private
     */
    _fastTouchEvent: function (sender, type) {
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;
            case ccui.Widget.TOUCH_ENDED:
                this.showRoleInfo(!this._roleInfoBD.isVisible());
                /*if (this._bClickFast) {
                    cf.SoundMgr.playEffect(8);
                    this._bClickFast = false;
                    this.showFastBtn(!this._bFastBtn);
                    this.showRoleInfo(!this._roleInfoBD.isVisible());
                    switch (sender.getTag()) {
                        case Player.FastTag.FAST:
                            break;
                        case Player.FastTag.SKIN://打开炮台皮肤面板                            
                            break;
                        case Player.FastTag.CHAT://聊天
                            break;
                    }
                }*/
                break;
        }
    },

    addPromptOfAutoFire: function () {
        if (this._bAutoFire) {
            var aniFunc = function (nodeExecutingAction, data) {
                nodeExecutingAction.setString(cf.Language.getText("text_1185") + data);
            };
            var delay = cc.delayTime(0.5);
            var sequence = cc.sequence(
                cc.callFunc(aniFunc, this._autoFireLabel, ""),
                delay,
                cc.callFunc(aniFunc, this._autoFireLabel, "."),
                delay.clone(),
                cc.callFunc(aniFunc, this._autoFireLabel, ".."),
                delay.clone(),
                cc.callFunc(aniFunc, this._autoFireLabel, "..."),
                delay.clone()
            );

            if (this._autoFireLabel == null) {
                this._autoFireLabel = new cc.LabelTTF(cf.Language.getText("text_1185"), cf.Language.FontName, 25);
                this._autoFireLabel.setPosition(-70, 140);
                this._autoFireLabel.setAnchorPoint(0, 0);
                this.addChild(this._autoFireLabel);
            }
            if (!cf.SkillMgr.getRageSkillState(this.getSeatID())) {
                this._autoFireLabel.setVisible(true);
                this._autoFireLabel.runAction(cc.repeatForever(sequence));
            }
        } else {
            if (this._autoFireLabel) {
                this._autoFireLabel.setVisible(false);
                this._autoFireLabel.stopAllActions();
            }
        }
    },

    addPromptOfRage: function () {
        var self = this;
        if (self._ragePromptLabel && self._ragePromptEffect /*&& self._rageParticle*/) {
            var bRage = cf.SkillMgr.getRageSkillState(self.getSeatID());

            //特效
            self._ragePromptLabel.setVisible(bRage);
            self._ragePromptEffect.setVisible(bRage);
            //闪光特效
            if (self._bLocalPlayer) {
                cf.EffectMgr.playBlink(bRage, cc.color.YELLOW);
            }

            if (bRage) {
                var aniFunc = function (nodeExecutingAction, data) {
                    nodeExecutingAction.setString(cf.Language.getText("text_1188") + data);
                };
                var delay = cc.delayTime(1);
                var sequence = cc.sequence(
                    cc.callFunc(aniFunc, self._ragePromptLabel, ""),
                    delay,
                    cc.callFunc(aniFunc, self._ragePromptLabel, "."),
                    delay.clone(),
                    cc.callFunc(aniFunc, self._ragePromptLabel, ".."),
                    delay.clone(),
                    cc.callFunc(aniFunc, self._ragePromptLabel, "..."),
                    delay.clone()
                );
                self._ragePromptLabel.runAction(cc.repeatForever(sequence));
                self._ragePromptEffect.runAction(cc.repeatForever(cc.rotateBy(0.5, 270)));

                //self._rageParticle.resetSystem();
            } else {
                self._ragePromptLabel.stopAllActions();
                self._ragePromptEffect.stopAllActions();
                //self._rageParticle.stopSystem();
            }
        }
    },

    /**
     * 添加技能使用icon
     */
    addSkillConsumeIcon: function (skillId) {
        var fadeIn = cc.fadeIn(0.5);
        var delay = cc.delayTime(1.5);
        var fadeOut = cc.fadeOut(0.5);
        var sequence = cc.sequence(fadeIn, delay, fadeOut);
        switch (skillId) {
            case cf.SkillId.lockSkill:
                if (this._skillUseIcon) {
                    this._skillUseIcon.setSpriteFrame(ActionMgr.getFrame("fishery_004.png"));
                    this._skillUseIcon.runAction(sequence);
                }
                break;
            case cf.SkillId.iceSkill:
                if (this._skillUseIcon) {
                    this._skillUseIcon.setSpriteFrame(ActionMgr.getFrame("fishery_006.png"));
                    this._skillUseIcon.runAction(sequence);
                }
                break;
            case cf.SkillId.rageSkill:
                if (this._skillUseIcon) {
                    this._skillUseIcon.setSpriteFrame(ActionMgr.getFrame("fishery_005.png"));
                    this._skillUseIcon.runAction(sequence);
                }
                break;
        }
    },

    _clearLockTarget: function (dt) {
        var self = this;
        //在锁定技能结束的5秒内重新锁定上一个目标
        if (self._lockObject != null) {
            self._skillCountTime += dt;
            if (!self._lockObject.checkCanLock()) {
                self._lockObject = null;
            }
            if (self._skillCountTime > 5) {
                //cc.log("5秒后置空");
                self._lockObject = null;
                self._skillCountTime = 0;
            }
        }
    },


    checkLockElementCanUse: function () {
        var lockedElement = this.getLockElement();
        if ((!cf.checkCanLock(lockedElement) && (lockedElement._pathAction.getElapsed() > lockedElement._originalDuration / 2))) {
            //lockedElement._sprite.removeAllChildren();
            this.setLockElement(null);
            this._lockObject = null;
            this._playerShotFire();
            //cc.log("锁定目标置空");
        }
    },

    doLockLogic: function () {
        var self = this;
        var lockElement = self.getLockElement();
        //cc.log("执行选择锁定对象逻辑");
        if (!lockElement) {
            //cc.log("选择精灵");
            self._lockSkillLogic();
            return;
        }
        self.checkLockElementCanUse();
        if (lockElement) {
            //cc.log("锁定逻辑执行" + self.getSeatID());
            if (this._bLocalPlayer) {
                if (lockElement.checkCanLock()) {
                    if (self._bFortCanFire) {
                        self._fortAngleAfterSkill(lockElement.getPosition());
                        //cc.log("调整角度发炮" + lockElement.getPosition().x + "--------------------------" + lockElement._fishState + this.getSeatID());
                    }
                }
                cf.EffectMgr.playLockEffect(lockElement.getPosition());
            }
        }
    },

    doAutoLogic: function(){
        var self = this;
        var lockElement = self.getLockElement();
        var commonRoom = cf.RoomMgr.getRoom();
        
        
        //cc.log("执行选择锁定对象逻辑");
        if (!lockElement) {
                //cf.autolock = false;
                self._AutolockSkillLogic();
                return;
        }
            //cc.log("选择精灵");
           
        
            self.checkLockElementCanUse();
            if (lockElement) {
               
                //cc.log("锁定逻辑执行" + self.getSeatID());
                if (this._bLocalPlayer) {
                    if (lockElement.checkCanLock()) {
                        if (self._bFortCanFire) {
                            self._fortAngleAfterSkill(lockElement.getPosition());
                            //cc.log("调整角度发炮" + lockElement.getPosition().x + "--------------------------" + lockElement._fishState + this.getSeatID());
                        }
                    }
                    cf.EffectMgr.playLockEffect(lockElement.getPosition());
                }
            }
        
    
    },

    /**
     * 锁定计时逻辑
     */
    _lockEffCountDown: function (dt) {
        var self = this;
        if (cf.SkillMgr.getLockSkillState(self.getSeatID())) {
            //cc.log("获取技能过后的状态" + cf.SkillMgr.getLockSkillState(self.getSeatID()));
            cf.SkillMgr.checkLockSkillTime(self.getSeatID(), dt);
            if (!cf.SkillMgr.getLockSkillState(this.getSeatID())) {
                this._lockSkillEffEnd();
                return;
            }
            if(cf.autolock == true){
                self.doAutoLogic();
            }else{
                self.doLockLogic();
            }
        }
    },

    /**
     * 狂暴技能逻辑
     */
    _rageEffCountDown: function (dt) {
        var self = this;
        if (cf.SkillMgr.getRageSkillState(self.getSeatID())) {
            //cc.log("获取技能过后的状态" + cf.SkillMgr.getRageSkillState(self.getSeatID()));
            this.onFireBefore();
            cf.SkillMgr.checkRageSkillTime(self.getSeatID(), dt);
            if (!cf.SkillMgr.getRageSkillState(this.getSeatID())) {
                self._escapeRage();
                self.addPromptOfRage();
                self._bTouchFire = false;
                self.continueAutoFire();
                self.addPromptOfAutoFire();
                return;
            }
            if (!self._bLocalPlayer) {
                return;
            }
        }
    },

    /**
     * 使用锁定技能后炮台角度
     * @param point 触摸点坐标
     * @private
     */
    _fortAngleAfterSkill: function (point) {

        //获取本地玩家
        if (!this._bLocalPlayer) {
            return;
        }
        if (this.isAlive() == false)
            return;
        this.setFortAngle(point);
        this.onFireBefore();
    },

    /**
     * 锁定效果终止逻辑
     */
    _lockSkillEffEnd: function () {
        var self = this;
        this._playerShotFire();
        self.setLockElement(null);
        self.setLastObject(null);
        var nowTime = (new Date()).getTime();
        cc.log(nowTime + "锁定效果到期。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。" +
            "。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。");
        if (cf.CanContinueAutoFire) {
            self.continueAutoFire();
            self.addPromptOfAutoFire();
        }
        if (self._bLocalPlayer) {
            cf.EffectMgr.stopLockEffect();
        }
        self._hasSendLockObject = false;
        //cc.log("重新回到初始条件 重新锁定或者变更为未锁定)");
    },

    _iceEffCountDown: function (dt) {
        var self = this;
        //检测技能状态的结束
        if (cf.SkillMgr.getIceSkillState()) {
            cf.SkillMgr.checkIceSkillTime(dt);
            if (!cf.SkillMgr.getIceSkillState()) {
                self._frozenSkillEnd();
            }
        }
    },

    _summonSkillCoolDown: function (dt) {
        //检测技能状态的结束
        if (cf.SkillMgr.getSummonSkillCooling()) {
            cf.SkillMgr.checkSummonSkillTime(dt);
        }
    },

    _frozenSkillEnd: function () {
        var self = this;
        cf.EffectMgr.stopIceEffect();
        this._escapeFreeze();
    },
    _AutolockSkillLogic: function () {
        var lockElement = this.getLockElement();
                cc.log("选择当前场景中锁定优先级最高的精灵");
            if(this._lockObject && !lockElement && this._lockObject.checkInScreen()){
                this._playerShotFire();
                cf.EffectMgr.stopLockEffect();
                this._lockSkillEffEnd();
                
                if(this._autoLockUI != null){
                    this._autoLockUI.checkBeiShuLV();
                }else{
                    this.closeAutoLockEffect();
                    cf.autolock = false;
                    cf.autofishData = [];
                    this._playerShotFire();
                    cf.EffectMgr.stopLockEffect();
                    this._lockSkillEffEnd();
                }
                return;
                
                
            }else{
                this._playerShotFire();
                cf.EffectMgr.stopLockEffect();
                this._lockSkillEffEnd();
                
                if(this._autoLockUI != null){
                    this._autoLockUI.checkBeiShuLV();
                }else{
                    this.closeAutoLockEffect();
                    cf.autolock = false;
                    cf.autofishData = [];
                    this._playerShotFire();
                    cf.EffectMgr.stopLockEffect();
                    this._lockSkillEffEnd();
                }
                //this._playerShotFire();
                
            }
 

    },
    _lockSkillLogic: function () {
        var self = this;
        var lockedElement = self.getLockElement();
        var commonRoom = cf.RoomMgr.getRoom();
        if (self._lockObject) {
            cc.log(self._lockObject.getFishModel().getServerIndex());
        }
        if (self._lockObject && !lockedElement && self._lockObject.checkInScreen()) {
            cc.log("继续锁定上一个目标");
            cc.log(self._lockObject.getFishModel().getServerIndex());
            self._playerLock(self._lockObject.getFishModel().getServerIndex());
            return;
        }
        var localSprite = null;
        if (!commonRoom._touch && !lockedElement && !self._hasSendLockObject) {
            cc.log("自动选择大鱼");
            //localSprite = cf.FishMgr.getThePriorityOfAllElement();
            //cc.log("选择当前场景中锁定优先级最高的精灵");
            if (localSprite) {
                cc.log("选择当前场景中锁定优先级最高的精灵");
                self._hasSendLockObject = true;
                self._playerLock(localSprite.getFishModel().getServerIndex());
            }
            else {
                cf.EffectMgr.stopLockEffect();
                self._hasSendLockObject = false;
                this._playerShotFire();
            }
        }
    },

    /**
     * 执行冰冻
     */
    iceSkillLogic: function () {
        var fishMgr = cf.FishMgr;
        var fishMap = fishMgr.getFishMap();
        var length = fishMap.size();
        if (length > 0) {
            for (var i = 0; i < length; ++i) {
                var fish = fishMap._elements[i].value;
                fish.setFrozen(true);
            }
        }
    },

    _escapeFreeze: function () {
        cc.log("解冻所有鱼");
        var fishMgr = cf.FishMgr;
        var fishMap = fishMgr.getFishMap();
        var length = fishMap.size();
        if (length > 0) {
            for (var i = 0; i < length; ++i) {
                var fish = fishMap._elements[i].value;
                //屏幕内并且鱼可以检测碰撞
                if (fish.getIsFrozen()) {
                    fish.setFrozen(false);
                }
            }
        }
    },

    //召唤技能的逻辑
    _summonSkillLogic: function () {

    },

    //狂暴技能的逻辑
    rageSkillLogic: function () {
        this.setFireIntervalByRage(true);
    },

    _escapeRage: function () {
        this.setFireIntervalByRage(false);
    },

    /**
     * 停止开炮
     */
    _playerShotFire: function () {
        if (this.isAlive()) {
            //停止开炮
            this.onFireEnd();
        }
    },

    /**
     * 锁定和狂暴技能锁定信息
     */
    _playerLock: function (fishIndex) {
        cc.log("发送被锁定的鱼的消息" + fishIndex);
        var skillId = cf.SkillId.lockSkill;
        var localSprite = cf.FishMgr.getFishMap().get(fishIndex);
        this._lockObject = localSprite;
        this.setLockElement(null);
        //发送消息
        var msg = new $root.CS_SkillLockMsg();
        msg.nIdLogin = cf.PlayerMgr.getLocalPlayer().getIdLogin();
        msg.nSkillId = skillId;
        msg.strFishIndex = fishIndex;
        var wr = $root.CS_SkillLockMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_SkillLockMsg, wr);
    },

    /**
     * 更换炮台皮肤
     */
    modifyFortSkin: function () {
        this._leftFort.modifyFortSkin(this._playerModel.getFortType(), this._playerModel.getFortSkinUse(), this._roomId);
        this._rightFort.modifyFortSkin(this._playerModel.getFortType(), this._playerModel.getFortSkinUse(), this._roomId);
    },

    /**
     * 
     * @param 更換bonus炮皮膚
     */

     modifySkillFortSkin: function(){
        this._leftFort.modifySkillFortSkin(this._roomId);
        this._rightFort.modifySkillFortSkin(this._roomId);


     },

    /**
     * 显示角色信息面板
     * @param bShow
     */
    showRoleInfo: function (bShow) {
        if (this._roleInfoBD) {
            this._roleInfoBD.updateRoleInfo(this._playerModel);
            this._roleInfoBD.showRoleInfo(bShow);
        }
    },

    /**
     * 刷新角色信息
     */
    updateRoleInfo: function () {
        if (this._roleInfoBD) {
            this._roleInfoBD.updateRoleInfo(this._playerModel);
        }
    },

    /**
     * 玩家属性lockElement的设置方法
     */
    setLockElement: function (lockElement) {
        this._lockElement = lockElement;
    },

    /**
     * 玩家属性lockElement的获取方法
     */
    getLockElement: function () {
        return this._lockElement;
    },

    /**
     * 设置上一锁定目标对象是否为空
     */
    setLastObject: function (lockElement) {
        this._lockObject = lockElement;
    },

    /**
     * 设置是否发送锁定对象的状态
     */
    setHasSendLockObject: function (state) {
        this._hasSendLockObject = state;
    },

    doGoldAction: function (callback) {
        if (this._bGoldAction && this._goldIcon) {
            this._bGoldAction = false;
            this._goldIcon.runAction(cc.sequence(
                cc.scaleTo(0.1, 0.4),
                cc.scaleTo(0.1, 0.5),
                cc.callFunc(function () {
                   // cf.SoundMgr.playEffect(76,false);
                    this._bGoldAction = true;
                    if (callback)
                        callback();
                }, this)
            ));
        }
    },

    /**
     * 记录上一次彩金特效分数
     * @param val
     */
    setLastBonusScore: function (val) {
        this._lastBonusScore = val;
    },

    /**
     * 获取上一次彩金特效分数
     * @returns {number}
     */
    getLastBonusScore: function () {
        return this._lastBonusScore;
    },

    hideFastBtn: function () {
        this.showRoleInfo(false);
        /*if (this._bFastBtn) {
            this._bFastBtn = false;
            this.showFastBtn(false);
            this.showRoleInfo(false);
        }*/
    },
    /**
     * 检测炮台为双炮还是单炮
     * @returns {boolean}
     */
    isDoubleFort: function () {
        return (this._playerModel.getFortMode() == Player.FortMode.DOUBLE);
    },

    /**
     * 设置单炮
     */
    setFortModeSingle: function () {
        var self = this;
        var fortBkHeight = self._spFortBk.height;
        self._rightFort.setVisible(false);
        self._leftFort.setPosition(0, fortBkHeight / 2);
        this._playerModel.setFortMode(Player.FortMode.SINGLE);
    },

    /**
     * 设置双炮
     */
    setFortModeDouble: function () {
        var self = this;
        self._rightFort.setVisible(true);
        var fortBkHeight = self._spFortBk.height;
        self._leftFort.setPosition(-45, fortBkHeight / 2);
        //创建副炮
        self._rightFort.setPosition(45, fortBkHeight / 2);
        self._rightFort.setFortAngle(self._leftFort.getFortAngle());
        this._playerModel.setFortMode(Player.FortMode.DOUBLE);
    },

    /**
     * 切换单双炮消息
     */
    tansFormFortMode: function () {
        var msg = new $root.CS_ChangeFort();
        var wr = $root.CS_ChangeFort.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_ChangeFort, wr);
    },
    
    isUnlockUI:function () {
        if(this._bUnlock){
            if(this._playerModel.getFortType() == this._cannonLimit[0]) {
                var msg = new $root.CS_ModifyFort();
                msg.bUpFort = false;
                var wr = $root.CS_ModifyFort.encode(msg).finish();
                cf.NetMgr.sendSocketMsg(MessageCode.CS_ModifyFort, wr);
            }
            this.modifyFort(true, this._playerModel.getFortData());
            return true;
        }
        return false;
    }
});
Player.show = true;
/**
 * 玩家炮台模式
 */
Player.FortMode = {
    SINGLE: 1,       //< 单炮
    DOUBLE: 2        //< 双炮
};

Player.DataType = {
    GOLD: 0,                    //< 金币
    DIAMOND: 1,                 //< 钻石
    EXP: 2,                     //< 经验
    CUR_FORT: 3,                //< 当前使用炮倍
    MAX_FORT: 4,                //< 解锁最大炮倍
    LEVEL: 5,                   //< 玩家等级
    SKIN: 6,                    //< 炮台皮肤
    ADD_SKIN: 7,                //< 添加炮台皮肤
    FACC_Points: 8,              //< 玩家积分
    LOTTERY: 9,                  //< 奖券

    MAX: 999
};

Player.FastTag = {
    FAST: 0,            //< 快捷键
    SKIN: 1,            //< 换肤
    CHAT: 2,            //< 聊天
    AUTO: 3             //< 自动开炮
};