/**
 * Created by Administrator on 2017/5/18.
 */

/**
 * 帧动画鱼
 */
var SpineFish = BaseFish.extend({

    stateTimeCount:0,

    _animateName: null,

    //进行某一个动作的次数 //用于普通状态和攻击状态的切换
    _doAnimateTimes: 0,

    /**
     * 构造方法
     * @param id 鱼种类id
     * @param serverIndex 服务器索引
     */
    ctor: function (id, serverIndex) {
        var self = this;
        self._super();
        //创建精灵
        var spineJson, spineAtlas;
        var effectAnimation;
        self.initData(id, serverIndex);
        switch (id){
            case 23:
                // spineJson = Res.GAME.bossPangxie_Json;
                // spineAtlas = Res.GAME.bossPangxie_Atlas;
                spineJson = Res.GAME.crocodile_Json;
                spineAtlas = Res.GAME.crocodile_Atlas;
                
                
                break;
            case 24:
                
                spineJson = Res.GAME.boospangxie_Json;
                spineAtlas = Res.GAME.boospangxie_Atlas;
                
                
                // spineJson = Res.GAME.bossMeirenyu_Json;
                // spineAtlas = Res.GAME.bossMeirenyu_Atlas;
                break;

            case 25:
                spineJson = Res.GAME.mythology_Json;
                spineAtlas = Res.GAME.mythology_Atlas;
               
                break;

            case 26:
                spineJson = Res.GAME.bossCrocodile_Json;
                spineAtlas = Res.GAME.bossCrocodile_Atlas;
                break;
            case 27:
                spineJson = Res.GAME.hammer30_Json;
                spineAtlas = Res.GAME.hammer30_Atlas;
            break;

            case 28:
                spineJson = Res.GAME.hammer31_Json;
                spineAtlas = Res.GAME.hammer31_Atlas;
                break;
           case 29:
               spineJson = Res.GAME.god_of_wealth2_Json;
               spineAtlas = Res.GAME.god_of_wealth2_Altas;
               effectAnimation = "art_dice";
                break;
          case 30:
               spineJson = Res.GAME.god_of_wealth2_Json;
               spineAtlas = Res.GAME.god_of_wealth2_Altas;
               effectAnimation = "art_larbar";
                break;
            default:
                // spineJson = Res.GAME.bossPangxie_Json;
                // spineAtlas = Res.GAME.bossPangxie_Atlas;
                break;
        }
        self._sprite = ActionMgr.createSpine(spineJson,spineAtlas,1);
        self._sprite.setPosition(0, 0);
        self.addChild(self._sprite);

        //世界boss光环
        // if(this._fishModel._fishType == cf.FishType.MATCH) {
        //     var spEffect = new cc.Sprite("#bossMark.png");
        //     spEffect.setPosition(0, 0);
        //     spEffect.runAction(cc.repeatForever(cc.rotateBy(4, 360)));
        //     spEffect.setScale(0.8);
        //     self.addChild(spEffect, -1, FrameFish.Tag.EFFECT);
        // }
        switch (this._fishModel._fishType) {
            case cf.FishType.COMMON:
            case cf.FishType.BONUS:
            case cf.FishType.SPECIAL:
            case cf.FishType.BOSS:
            case cf.FishType.POKER:
            case cf.FishType.MATCH:
            case cf.FishType.DICESLOT:
                

                //世界boss光环
                if(this._fishModel._fishType == cf.FishType.MATCH || this._fishModel._fishType == cf.FishType.BOSS) {
                    var icName = "#bossring.png";
                    if(this._fishModel._fishType == cf.FishType.MATCH) {
                        icName = "#bossMark.png";
                    }
                    if(this._fishModel._fishId == 25){
                        icName = "#bossringw.png";
                    }
                    
                    spEffect = new cc.Sprite(icName);
                    spEffect.setPosition(0, 0);
                    spEffect.runAction(cc.repeatForever(cc.rotateBy(4, 360)));
                    spEffect.setScale(1.4);
                    self._sprite.addChild(spEffect, -1, FrameFish.Tag.EFFECT);
                }

                if(this._fishModel._fishType == cf.FishType.DICESLOT){
                    if(this._fishModel._fishId == 29){
                        effectJson = Res.GAME.god_of_wealth_Json;
                        effectAtlas = Res.GAME.god_of_wealth_Altas;
    
    
                    }else if(this._fishModel._fishId == 30){
                        effectJson = Res.GAME.god_of_wealth_Json;
                        effectAtlas = Res.GAME.god_of_wealth_Altas;
    
                    }
                    effect = ActionMgr.createSpine(effectJson,effectAtlas,1);
                    effect.setAnimation(0, effectAnimation , true);
                    effect.setPosition(0, 0);
                    self._sprite.addChild(effect);
                }
              
        }

        // self.width = self._sprite.width;
        // self.height = self._sprite.height;
        //移除手动生成骨骼动画影子的方式 保证效率 由美术自动添加到骨骼动画中
        self.doAnimate();
    },

    /**
     * 执行动画
     * @param aniName
     */
    doAnimate: function (aniName) {
        //创建动画 haidaochuan animation3 booszhangyu animation6 animation2 move
        //暂时特殊处理螃蟹的问题
       // if(this._fishModel._fishId != 23 && this._fishModel._fishId != 41 && this._fishModel._fishId != 45) {
        if(this._fishModel._fishId != 23   && this._fishModel._fishId != 41 && this._fishModel._fishId != 45) {
            this._sprite.setAnimation(0, "move", true);
            this._sprite._animateName = "move";
            this._sprite.setCompleteListener(this.animationEndListener.bind(this));
        }
        else {
            this._sprite.setAnimation(0, "move", true);
        }

    },

    animationEndListener: function () {
        if( this._fishModel._fishId == 42 || this._fishModel == 44) {
            return;
        }
        if(this._sprite._animateName == "move") {
            this._doAnimateTimes++;
            if(this._doAnimateTimes == 4) {
                this._doAnimateTimes = 0;
                this._sprite._animateName = "attack";
                this._sprite.setAnimation(0, "attack", false);
                this._sprite.setCompleteListener(this.animationEndListener.bind(this));
            }
        }
        else if(this._sprite._animateName == "attack") {
            this._sprite.setAnimation(0, "move", true);
            this._sprite._animateName = "move";
            this._sprite.setCompleteListener(this.animationEndListener.bind(this));
        }
    },

    /**
     * 是否显示冰冻效果
     * @param bShow
     */
    showFrozen: function (bShow) {
        if (bShow) {
            this.setFishColor(cc.color(0, 234, 255));
            if(this._sprite) {
                this._sprite.pause();
            }
        } else {
            if(this._sprite) {
                this._sprite.resume();
            }
            this.setFishColor(cc.color.WHITE);
        }
    }
});

SpineFish.create = function (id, serverIndex) {
    return new SpineFish(id, serverIndex);
};
