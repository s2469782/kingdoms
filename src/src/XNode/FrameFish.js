var FrameFish = BaseFish.extend({
    /**
     * 构造方法
     * @param id 鱼种类id
     * @param serverIndex 服务器索引
     */
    ctor: function (id, serverIndex, bFeatsFish) {

        this._super();

        var self = this;

        //初始化数据
        self.initData(id, serverIndex, bFeatsFish);

        //创建精灵
        var isDoAnimate = false;
        var frameName = "#" + self._fishModel.getMoveAction() + "_001.png";
        var shadowName = frameName;
        var spEffect = null;
        var shadowScale = 0.5;
        switch (this._fishModel._fishType) {
            case cf.FishType.COMMON:
            case cf.FishType.BONUS:
            case cf.FishType.SPECIAL:
            case cf.FishType.BOSS:
            case cf.FishType.POKER:
            case cf.FishType.MATCH:
            case cf.FishType.SKILL:
                var numAtlas, numJson;
                numAtlas = Res.GAME.stage_num_Atlas;
                numJson = Res.GAME.stage_num_Json;

                
                //spine
            
            // var spine = ActionMgr.createSpine(bg_Json, bg_Atlas);
            // spine.setAnimation(0, "ten", false);
            // console.log(spine._skeleton.bones[1].skeleton.slots);
            // //spine._skeleton.bones[1].skeleton.slots[1].setAttachment
            // spine.setAttachment("hundred", "1");
            // spine.setAttachment("ten", "2");
            // spine.setAttachment("digit", "1");
            // bonusNode.addChild(spine, 1);
            // console.log(spine);

               
                
                
                self._sprite = new cc.Sprite(frameName);
                self._sprite.setPosition(0, 0);
                
                self.addChild(self._sprite);

//                 var ani = ActionMgr.createSpine(numJson, numAtlas);
//                 ani.setAnimation(0,"animation", true);
//                 var skin = ani.findSkin("num");
// //ani.setAttachment("num", "num");
//                 self.addChild(ani);
//                 console.log(ani);

                //世界boss光环
                // if(this._fishModel._fishType == cf.FishType.MATCH || this._fishModel._fishType == cf.FishType.BOSS) {
                //     var icName = "#bossring.png";
                //     if(this._fishModel._fishType == cf.FishType.MATCH) {
                //         icName = "#bossMark.png";
                //     }
                //     spEffect = new cc.Sprite(icName);
                //     spEffect.setPosition(self._sprite.width / 2, self._sprite.height / 2);
                //     spEffect.runAction(cc.repeatForever(cc.rotateBy(4, 360)));
                //     spEffect.setScale(1);
                //     self._sprite.addChild(spEffect, -1, FrameFish.Tag.EFFECT);
                // }

                //执行动作
                isDoAnimate = true;
                break;
            case cf.FishType.LOTTERY:
                frameName = "#fisheryLottery.png";
                self._sprite = new cc.Sprite(frameName);
                self._sprite.setPosition(0, 0);
                var localSeatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
                if (localSeatId >= 2) {
                    self._sprite.setRotation(180);
                }
                self.addChild(self._sprite);

                var lotteryNum = new cc.LabelBMFont("x"+cf.Data.FISH_DATA[id].fishScore, Res.LOBBY.font_resource_090);
                //lotteryNum.setAnchorPoint(cc.p(0.5, 0.5));
                lotteryNum.setScale(0.68);
                lotteryNum.setLineBreakWithoutSpace(true);
                lotteryNum.setPosition(self._sprite.width-40, self._sprite.height / 2);
                //lotteryNum.setRotation(-90);
                self._sprite.addChild(lotteryNum);

                //添加阴影
                shadowName = "#fisheryLotteryEffect.png";
                shadowScale = 0.5;

                spEffect = new cc.Sprite("#fisheryLotteryEffect.png");
                spEffect.setPosition(self._sprite.width / 2, self._sprite.height / 2);
                spEffect.runAction(cc.repeatForever(cc.rotateBy(2, 360)));
		        spEffect.setScaleY(shadowScale);
                self._sprite.addChild(spEffect, -1, FrameFish.Tag.EFFECT);

                isDoAnimate = false;
                break;
        }

        //添加阴影
        self._spriteShadow = new cc.Sprite(shadowName);
        self._spriteShadow.setPosition(0, 0);
        self._spriteShadow.setColor(cc.color(0, 0, 0));
        self._spriteShadow.setOpacity(100);
        //self._spriteShadow.setContentSize(self._sprite.getContentSize().width/2, self._sprite.getContentSize().height/2 -300);
        self._spriteShadow.setScaleX(1.4);
        self._spriteShadow.setScaleY(shadowScale);
        //z-index 要再調整
        self.addChild(self._spriteShadow, -100);


        if (isDoAnimate) {
            self.doAnimate(self._fishModel.getMoveAction());
        }
        else {
            self._spriteShadow.runAction(cc.repeatForever(cc.rotateBy(2, 360)));
        }

        self.width = self._sprite.width;
        self.height = self._sprite.height;
    },

    /**
     * 重置鱼
     * @param id 鱼种类id
     * @param serverIndex 服务器索引
     */
    resetData: function (id, serverIndex,bFeatsFish) {
        var self = this;

        self._super(id, serverIndex,bFeatsFish);

        cc.log("重置后精灵的类型为" + this._fishModel._fishType);
        //重置精灵
        var isDoAnimate = false;
        var frameName = self._fishModel.getMoveAction() + "_001.png";
        var shadowName = frameName;
        var shadowScale = 0.5;
        var spEffect = null;
        self._sprite.removeAllChildrenWithCleanup(true);
        self._sprite.stopAllActions();
        switch (this._fishModel._fishType) {
            case cf.FishType.COMMON:
            case cf.FishType.BONUS:
            case cf.FishType.SPECIAL:
            case cf.FishType.BOSS:
            case cf.FishType.POKER:
            case cf.FishType.MATCH:
            case cf.FishType.SKILL:
                self._sprite.setSpriteFrame(frameName);
                isDoAnimate = true;
                // if(this._fishModel._fishType == cf.FishType.MATCH || this._fishModel._fishType == cf.FishType.BOSS) {
                //     var icName = "#bossring.png";
                //     if(this._fishModel._fishType == cf.FishType.MATCH) {
                //         icName = "#bossMark.png";
                //     }
                //     spEffect = new cc.Sprite(icName);
                //     spEffect.setPosition(self._sprite.width / 2, self._sprite.height / 2);
                //     spEffect.runAction(cc.repeatForever(cc.rotateBy(4, 360)));
                //     spEffect.setScale(1);
                //     self._sprite.addChild(spEffect, -1, FrameFish.Tag.EFFECT);
                // }
                break;
            case cf.FishType.LOTTERY:
                frameName = "fisheryLottery.png";
                self._sprite.setSpriteFrame(frameName);
                var localSeatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
                if (localSeatId >= 2) {
                    self._sprite.setRotation(180);
                }
                isDoAnimate = false;

                var lotteryNum = new cc.LabelBMFont("x"+cf.Data.FISH_DATA[id].fishScore, Res.LOBBY.font_resource_090);
                //lotteryNum.setAnchorPoint(cc.p(0.5, 0.5));
                lotteryNum.setScale(0.68);
                lotteryNum.setLineBreakWithoutSpace(true);
                lotteryNum.setPosition(self._sprite.width-40, self._sprite.height / 2);
                //lotteryNum.setRotation(-90);
                self._sprite.addChild(lotteryNum);

                //添加阴影
                shadowName = "fisheryLotteryEffect.png";
                shadowScale = 1.2;

                spEffect = new cc.Sprite("#fisheryLotteryEffect.png");
                spEffect.setPosition(self._sprite.width / 2, self._sprite.height / 2);
                spEffect.runAction(cc.repeatForever(cc.rotateBy(2, 360)));
                spEffect.setScaleY(shadowScale);
                self._sprite.addChild(spEffect, -1, FrameFish.Tag.EFFECT);
                break;
        }

        if (self._spriteShadow){
            self._spriteShadow.setSpriteFrame(shadowName);
            self._spriteShadow.setScaleY(shadowScale);
        }


        if (isDoAnimate) {
            self.doAnimate(self._fishModel.getMoveAction());
        }
    },

    /**
     * 执行动画
     * @param aniName
     */
    doAnimate: function (aniName) {
        if (aniName && cc.isString(aniName)) {
          
            //创建动画

         
            this._sprite.runAction(new cc.RepeatForever(ActionMgr.getAnimate(aniName)));
            
           
            if (this._spriteShadow) {
                this._spriteShadow.runAction(new cc.RepeatForever(ActionMgr.getAnimate(aniName)));
            }
        } else {
            cc.log("动画名称错误");
        }
    },

    /**
     * 是否显示冰冻效果
     * @param bShow
     */
    showFrozen: function (bShow) {
        var spEffect = null;
        switch (this._fishModel.getType()) {
            case cf.FishType.LOTTERY:
            case cf.FishType.MATCH:
                spEffect = this._sprite.getChildByTag(FrameFish.Tag.EFFECT);
                break;
        }
        if (bShow) {
            this.setFishColor(cc.color(0, 234, 255));
            this._sprite.pause();
            if (this._spriteShadow)
                this._spriteShadow.pause();

            if (spEffect)
                spEffect.pause();
        } else {
            this.setFishColor(cc.color.WHITE);
            this._sprite.resume();
            if (this._spriteShadow)
                this._spriteShadow.resume();
            if (spEffect)
                spEffect.resume();
        }
    }

});

FrameFish.Tag = {
    EFFECT: 0
};

FrameFish.create = function (id, serverIndex,bFeatsFish) {
    var exist = cc.pool.hasObject(FrameFish);
    if (exist) {
        return cc.pool.getFromPool(FrameFish, id, serverIndex,bFeatsFish);//调用reuse方法
    }
    else {
        return new FrameFish(id, serverIndex ,bFeatsFish);
    }
};