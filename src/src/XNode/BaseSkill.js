var BaseSkill = cc.Class.extend({

    _skillOpen: false,               //< 技能开启(持续性的技能)

    _skillCountTime: 0,               //< 技能计时

    _skillLastTime: 0,                //< 技能的剩余时间

    _skillEnd: true,                 //< 技能结束(服务器)

    _skillCooling: false,            //< 技能正在冷却中

    _skillCdTime: 0,                  //< 技能冷却时间

    _skillCdCountTime: 0,             //< 用于冷却时间计时

    ctor: function (skillId) {
        var self = this;
        self.init(skillId);
    },

    init: function (skillId) {
        var self = this;
        self._skillEnd = true;
        self._skillOpen = false;
        self._skillLastTime = 0;
        self._skillCountTime = 0;
        self._skillCooling = false;
        self._skillCdTime = cf.Data.SKILLDATA[skillId].skill_cd;
        self._skillCdCountTime = 0;
    },

    setSkillLastTime: function (lastTime) {
        this._skillLastTime = lastTime;
    },

    getSkillLastTime: function () {
        return this._skillLastTime;
    },

    setSkillOpen: function () {
        this._skillOpen = true;
        this._skillEnd = false;
    },

    getSkillState: function () {
        return this._skillOpen;
    },

    setSkillEnd: function () {
        this._skillEnd = true;
    },

    getSkillEnd: function () {
        return this._skillEnd;
    },

    resetCountTime: function () {
        this._skillCountTime = 0;
    },

    getSkillCountTime: function () {
        return this._skillCountTime;
    },

    checkLocalSkillIsEnd: function (dt) {
        var self = this;
        /*if(!(self._skillLastTime > 0)) {
            cc.log("设置的技能时间小于0");
            self.resetSkillData();
            return;
        }
        self._skillCountTime += dt;
        if (!self._skillEnd && self._skillOpen) {
            if (self._skillCountTime > self._skillLastTime + 0.5) {
                self.resetSkillData();
                //cc.log("技能计时结束");
            }
        }*/
        if(self._skillEnd) {
            self.resetSkillData();
            //cc.log("技能结束--");
        }
    },

    resetSkillData: function () {
        this._skillEnd = true;
        this._skillOpen = false;
        this._skillCountTime = 0;
        this._skillLastTime = 0;
    },

    setSkillCooling: function () {
        this._skillCooling = true;
    },

    getSkillCooling: function () {
        return this._skillCooling;
    },

    checkLocalSkillIsCd: function (dt) {
        var self = this;
        self._skillCdCountTime += dt;
        if (self._skillCooling) {
            if (self._skillCdCountTime > self._skillCdTime) {
                self._skillCooling = false;
                self._skillCdCountTime = 0;
            }
        }
    },

    resetSkillCdCountTime: function () {
        this._skillCdCountTime = 0;
    }
});