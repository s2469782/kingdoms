var Coin = cc.Sprite.extend({
    _time: 1,
    _doAction: null,

    setTime: function (endPt, speed) {
        var x = endPt.x - this.x;
        var y = endPt.y - this.y;

        var _time = (x * x + y * y) / (speed * speed);
        this._time = Math.min(1.3, _time);
        if (this._time < 1) {
            this._time = 1;
        }
    },

    doJump: function (actionName, delay, endPt, jumpBy,jumpTo, callback) {
        var self = this;
        self.runAction(cc.repeatForever(ActionMgr.getAnimate(actionName)));
        self.runAction(cc.sequence(
            cc.delayTime(delay),
            cc.jumpBy(0.6, 0, 0, jumpBy, 1),
            cc.delayTime(0.4),
            cc.spawn(cc.jumpTo(self._time, endPt, jumpTo, 1), cc.scaleTo(self._time, 0.7)),
            cc.callFunc(function () {
                //压入缓冲池
                cc.pool.putInPool(this);
                callback();
            }, this)
        ));
    },

    doBoom: function (actionName, endPos, radius, angle, speed, delay, callback) {
        var self = this;
        self.runAction(cc.repeatForever(ActionMgr.getAnimate(actionName)));

        //弹射点
        var _x = self.x + Math.sin(cc.degreesToRadians(angle)) * radius;
        var _y = self.y + Math.cos(cc.degreesToRadians(angle)) * radius;

        self.runAction(cc.sequence(
            cc.moveTo(speed, _x, _y).easing(cc.easeBackOut()),
            cc.delayTime(delay),
            spawn = cc.spawn(cc.moveTo(self._time, endPos).easing(cc.easeBackIn()), cc.scaleTo(self._time, 0.7)),
            cc.callFunc(function () {
                //压入缓冲池
                cc.pool.putInPool(this);
                callback();
            }, this)
        ));
    },

    doFall: function(actionName,controlPoint,delayTime, fallSpeed, endPos, callback,angle,radius) {
        var self = this;
        self.runAction(cc.repeatForever(ActionMgr.getAnimate(actionName)));
        var _x = self.x + Math.sin(cc.degreesToRadians(angle)) * radius;
        var _y = self.y + Math.cos(cc.degreesToRadians(angle)) * radius;
        this.setPosition(_x,_y),
        self.runAction(cc.sequence(
            cc.delayTime(delayTime),
            cc.spawn(cc.moveTo(0.1, controlPoint).easing(cc.easeBackIn()), cc.scaleTo(0.2, 0.5),cc.fadeIn(0.5)),
            cc.spawn(cc.scaleTo(0.1, 1.4),cc.jumpBy(0.1,cc.p(30 * this.initDir(),0),5,1)),
            cc.spawn(cc.moveTo(1.2, controlPoint.x,50).easing(cc.easeBackIn()), cc.scaleTo(1.2, 1.1)),
            cc.jumpBy(fallSpeed * 0.6,cc.p(30 * this.initDir(),0),140,1),
            cc.jumpBy(fallSpeed * 0.5,cc.p(30 * this.initDir(),0),115,1),
            cc.jumpBy(fallSpeed * 0.4,cc.p(30 * this.initDir(),0),90,1),
            cc.jumpBy(fallSpeed * 0.3,cc.p(30 * this.initDir(),0),65,1),
            cc.jumpBy(fallSpeed * 0.2,cc.p(30 * this.initDir(),0),40,1),
            cc.fadeOut(0.5),
            cc.callFunc(function () {
                //压入缓冲池
                cc.pool.putInPool(this);
                this.setOpacity(255);
                callback();
            }, this)
        ));
    },

    initDir: function () {
        if(Math.round(Math.random()) % 2 == 0) {
            return -1;
        }
        else {
            return 1;
        }
    },

    reuse: function () {
        //this.setVisible(true);
    },

    unuse: function () {
        this.setRotation(0);
        this.setScale(1);
        this.stopAllActions();
        //this.setVisible(false);
        this.removeFromParent(true);
    }
});

Coin.create = function (frameName) {
    var exist = cc.pool.hasObject(Coin);
    if (exist) {
        return cc.pool.getFromPool(Coin);//调用reuse方法
    } else {
        return new Coin(frameName);
    }
};