var Bullet = BaseNode.extend({
    _sprite: null,              //< 子弹精灵
    _bulletSpeed: null,         //< 子弹速度
    _bulletDir: null,           //< 子弹方向
    _bulletDirX: 0,             //< 子弹X方向
    _bulletDirY: 0,             //< 子弹Y方向
    _bulletState: null,         //< 子弹状态

    _screenW: 0,                //< 屏幕宽
    _screenH: 0,                //< 屏幕高
    _checkW: 0,                 //< 屏幕偏移宽
    _spWidth: 0,                //< 子弹宽
    _spHeight: 0,               //< 子弹高

    _model: null,         //< 子弹数据模型
    _bulletFlyTime: 0,          //< 子弹飞行时间
    _trackTime: 0,              //< 子弹运行时间

    _catchFishArray: null,      //< 碰撞数据

    _localPlayerModel: null,
    _collisionId: 501,

    _lockedElement: null,

    _hitCount: 0,               //< 子弹反弹次数
    _hitTotalCount: 0,          //< 子弹反弹总次数

    /**
     * 构造方法
     */
    ctor: function () {
        //初始化原始资源
        this._super();

        var self = this;

        self._sprite = new cc.Sprite("#cannon_bullet_1_001.png");
        self._sprite.setPosition(0, 0);
        self._sprite.setRotation(180);
        self.addChild(self._sprite, 0);

        self.width = self._sprite.width;
        self.height = self._sprite.height;

        self._screenW = cc.winSize.width;
        self._screenH = cc.winSize.height;
        self._checkW = 0;       
        self._model = BulletModel.create();
    },

    /**
     * 初始化数据  
     */
    initData: function (roomId, seatId) {
        var self = this;
        //设置子弹属性       
        self.setBulletState(Bullet.State.STANDBY);
        self.setBulletType(self._model.getBulletType(),roomId, seatId);
        self._lockedElement = self._model.getLockedElement();

        //重置数据
        self._catchFishArray = null;
        self._catchFishArray = [];
        self._spWidth = self.width;
        self._spHeight = self.height / 2;

        //计算子弹方向和位置
        var angle = self._model.getAngle();
        self.setRotation(angle);
        //求子弹方向分量       
        self._bulletDirX = Math.sin(cc.degreesToRadians(angle));
        self._bulletDirY = Math.cos(cc.degreesToRadians(angle));
        self.setPosition(self._model.getBulletPos());

        //计算子弹运行时间
        self._trackTime = (parseInt(self._model.getServerTime()) - parseInt(self._model.getBulletTime())) / 1000;
        self._bulletFlyTime = 0;

        self._localPlayerModel = cf.PlayerMgr.getLocalPlayer();
        //初始化碰撞数据
        this.initCollisionData();
        //开启逻辑更新
        this.scheduleUpdate();

        self.runAction(cc.sequence(
            cc.delayTime(0.1),
            cc.callFunc(function () {
                self.setBulletState(Bullet.State.MOVE);
            }, this)
        ));

        //根据玩家vip获取反弹次数
        this._hitCount = 0;
        this._hitTotalCount = this._model.getBulletHitCount();

    },

    /**
     * 初始化碰撞数据
     */
    initCollisionData: function () {
        var self = this;
        var collisionId = self._collisionId;

        //本地版暂时使用c++逻辑
        if (cc.sys.isNative) {
            //获取碰撞数据
            var collisionData = cf.Data.BULLET_COLLISION_DATA[collisionId];
            self._collisionLen = collisionData.circleArray.length;

            self._collisionData = new CollisionData;
            self._collisionData.clearData();
            for (var i = 0; i < self._collisionLen; ++i) {
                var circle = collisionData.circleArray[i];
                self._collisionData.addData(circle.pt, circle.r);
            }
        } else {
            self._collisionData = cf.clone(cf.Data.BULLET_COLLISION_DATA[collisionId]);
            self._collisionLen = self._collisionData.circleArray.length;
        }
    },

    /**
     * 更新碰撞数据
     */
    updateCollisionData: function () {
        var self = this;

        //本地版暂时使用c++逻辑
        if (cc.sys.isNative) {
            self._collisionData.updateData(this, self.getRotation() + 180);
        } else {
            var collisionId = self._collisionId;
            var collisionData = cf.Data.BULLET_COLLISION_DATA[collisionId];
            for (var i = 0; i < self._collisionLen; ++i) {
                var circle = self._collisionData.circleArray[i];
                //计算圆心相对位置
                circle.pt.x = self.x + collisionData.circleArray[i].pt.x;
                circle.pt.y = self.y + collisionData.circleArray[i].pt.y;

                //根据角度转换圆心位置
                cf.pointRotateByAngle(circle.pt, self.x, self.y, cc.degreesToRadians(self.getRotation() + 180));
            }
        }
    },

    /**
     * 锁定状态下根据锁定的对象更新子弹的角度实现跟踪的效果
     */
    updateBulletDir: function () {
        var lockElement = this.getLockedElement();
        var player = cf.RoomMgr.getRoomPlayer(this._model.getSeatID());
        if (!lockElement || !player.getLockElement() || lockElement != player.getLockElement()) {
            this._model.setLockedElement(null);
            return;
        }
        if (lockElement && lockElement.checkCanLock() && (lockElement == player.getLockElement())) {
            var dirX = lockElement.x - this.x;
            var dirY = lockElement.y - this.y;
            //计算方向夹角
            var atanVal = Math.atan2(dirY, dirX);
            var angle = 90 - cc.radiansToDegrees(atanVal);
            //根据座位转换角度           
            this._bulletDirX = Math.sin(cc.degreesToRadians(angle));
            this._bulletDirY = Math.cos(cc.degreesToRadians(angle));
            this.setRotation(angle);
        }
    },

    /**
     * 设置子弹类型
     * @param skinIndex 皮肤索引
     * @param fortIndex 炮倍索引
     */
    setBulletType: function (fortIndex,roomId, seatId) {
        var self = this;

        //读表
        var cannonLevelData = cf.Data.CANNON_LEVEL_DATA[fortIndex];
        var skinInfo = cannonLevelData.skinIndex.get(roomId);

        var cannonData = cf.Data.CANNON_DATA[skinInfo[0]];
        if(cf.dragon_gold){
            if(cf.PlayerMgr.getLocalPlayer().getSeatID() == seatId){
                var moveAction = "cannon_bullet_1";
            }else{
                var moveAction = cf.Data.ANIMATE_DATA[cannonData.bullet_icon].frameName;
            }
        }else if (cf.dragon_wood){
            if(cf.PlayerMgr.getLocalPlayer().getSeatID() == seatId){
                var moveAction = "cannon_bullet_2";
            }else{
                var moveAction = cf.Data.ANIMATE_DATA[cannonData.bullet_icon].frameName;
            }
        }else{
            var moveAction = cf.Data.ANIMATE_DATA[cannonData.bullet_icon].frameName;
        }
        
        self._model.setMoveAction(moveAction);
        self._model.setDiedAction(cf.Data.ANIMATE_DATA[cannonData.net_icon].frameName);

        //获取碰撞id
        self._collisionId = cannonData.bullet_icon;

        //初始化子弹
        self._sprite.setSpriteFrame(moveAction + "_001.png");
        self.setScale(1);
        var speed = this._model.getBulletSpeed();
        self._bulletSpeed = cc.p(speed, speed);

        self._sprite.runAction(cc.repeatForever(ActionMgr.getAnimate(moveAction)));
    },

    /**
     * 移动计算
     * @param dt
     */
    updateMove: function (dt) {
        var self = this;

        //计算速度
        self.x += self._bulletSpeed.x * self._bulletDirX * dt;
        self.y += self._bulletSpeed.y * self._bulletDirY * dt;

        var lockElement = this.getLockedElement();
        var player = cf.RoomMgr.getRoomPlayer(this._model.getSeatID());
        var condition = (lockElement == player.getLockElement());
        if (lockElement && condition) {
            return;
        }

        //x方向
        if (self.x > self._screenW - self._checkW) {
            //设置位置
            self.x = self._screenW - self._checkW;

            self._bulletSpeed.x *= -1;
            self.setRotation(-self.rotation);
            self.hit();
        } else if (self.x < 0 - self._checkW) {
            //设置位置
            self.x = 0 - self._checkW;
            self._bulletSpeed.x *= -1;
            self.setRotation(-self.rotation);
            self.hit();
        }

        //y方向
        if (self.y > self._screenH) {
            //设置位置
            self.y = self._screenH;

            self._bulletSpeed.y *= -1;
            if (self.rotation > 0) {
                self.setRotation(180 - self.rotation);
            }
            else {
                self.setRotation(Math.abs(self.rotation) - 180);
            }
            self.hit();
        } else if (self.y < 0) {
            //设置位置
            self.y = self._spHeight / 2;

            self._bulletSpeed.y *= -1;
            if (self.rotation > 0) {
                self.setRotation(180 - self.rotation);
            }
            else {
                self.setRotation(Math.abs(self.rotation) - 180);
            }
            self.hit();
        }
    },

    /**
     * 逻辑更新
     * @param dt
     */
    update: function (dt) {
        switch (this._bulletState) {
            case Bullet.State.STANDBY:
            case Bullet.State.MOVE:
                //暂时不同步初始化的子弹
                /*if (this.calculateBulletPos(dt)) {                    
                 }*/
                this.updateBulletDir(dt);
                this.updateMove(dt);

                if (this._bulletState == Bullet.State.MOVE) {
                    this.updateCollisionData();
                    cf.FishMgr.collisionWithBulletBefore(this);
                }
                break;
        }
    },

    /**
     * 计算子弹位置
     * @param dt
     * @returns {boolean}
     */
    calculateBulletPos: function (dt) {
        var self = this;

        //本地玩家也不计算位置
        if (this._model.getSeatID() == this._localPlayerModel.getSeatID()) {
            return true;
        }

        if (self._bulletFlyTime >= self._trackTime) {
            return true;
        }

        var isBreak = false;
        while (1) {
            if (self._bulletFlyTime >= self._trackTime) {
                isBreak = true;
                break;
            }
            self._bulletFlyTime += dt;
            this.updateMove(dt)
        }
        return isBreak;
    },

    /**
     * 获取服务器索引
     * @returns {null}
     */
    getServerIndex: function () {
        return this._model.getServerIndex();
    },

    getSeatID: function () {
        return this._model.getSeatID();
    },

    /**
     * 设置自动状态
     * @param state
     */
    setBulletState: function (state) {
        this._bulletState = state;
    },

    /**
     * 获取子弹状态
     * @returns {null}
     */
    getBulletState: function () {
        return this._bulletState;
    },


    

    /**
     * 张网抓鱼
     * @param fish 鱼对象
     */
    openNets: function (fish) {
        //做张网动作

        this.doDied();
        fish.showBeAttacked();
        //只有本地玩家才会进行概率捕捉并向服务器发消息
        if (this._model.getSeatID() == this._localPlayerModel.getSeatID()) {
          
            this._catchFishArray.push(fish.getModel().getServerIndex());
            var rand = this._localPlayerModel.getRandom();
            var randRate = rand.nextInt(0, 100000);

            //概率 = 鱼概率*（1+房间修正+个人修正）
            var fishKillRate = fish.getModel().getKillRate();
            //cc.log("张网：鱼击杀原始概率 = " + fishKillRate + ", 加成概率 = " + fishKillRate * 2 + ", 当前概率 = " + randRate + ", 当前次数 = " + rand.getCount());

            //判断是否捕捉,这里将击杀概率放大2倍
            if (randRate <= fishKillRate * 2000 && !(this._localPlayerModel.getStopSendMsg())) {
                
                //发捕鱼消息给服务器
                //cc.log("张网：发送子弹消息，索引 = " + this.getServerIndex());
                var msg = new $root.CS_PlayerCatch();
                msg.strBulletID = this.getServerIndex();
                msg.listFishID = this._catchFishArray;
                msg.listFishRandom[0] = randRate;
                msg.listFishBound[0] = rand.getCount();
                var wr = $root.CS_PlayerCatch.encode(msg).finish();
                cf.NetMgr.sendSocketMsg(MessageCode.CS_PlayerCatch, wr);
                
            }
        }
    
    },

    doDied: function () {
        if (this._bulletState == Bullet.State.OPEN) {
            return;
        }

        this.setBulletState(Bullet.State.OPEN);
        //已经死亡直接从map中移除
        cf.BulletMgr.removeBulletFromMap(this.getSeatID(), this.getServerIndex());

        //子弹次数记录
        var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
        if (localPlayerModel.getSeatID() == this._model.getSeatID()) {
            localPlayerModel.setBulletCountOffset(-1);
        }

        this.stopAllActions();
        this._sprite.stopAllActions();

        var diedAction = this._model.getDiedAction();
        this._sprite.y += this._spHeight / 2;
        this._sprite.setSpriteFrame(diedAction + "_001.png");

        this._sprite.runAction(cc.sequence(
            ActionMgr.getAnimate(diedAction),
            cc.delayTime(0.1),
            cc.callFunc(function () {
                //压入缓冲池
                cc.pool.putInPool(this);
            }, this)
        ));
    },

    /**
     * 碰撞检测
     * @param fish
     * @returns {boolean}
     */
    collisionWithFish: function (fish) {
        if (cc.sys.isNative) {
            if (this._collisionData.collisionWithNode(fish._collisionData)) {
                this.setBulletState(Bullet.State.COLLISION);
                return true;
            }
        } else {
            var collisionData = fish._collisionData;
            var fishColLength = collisionData.circleArray.length;
            var bulletColLength = this._collisionData.circleArray.length;
            for (var i = 1; i < fishColLength; ++i) {
                for (var j = 0; j < bulletColLength; ++j) {
                    if (cf.collisionCircleAndCircleEx(this._collisionData.circleArray[j], collisionData.circleArray[i])) {
                        this.setBulletState(Bullet.State.COLLISION);
                        return true;
                    }
                }
            }
        }
        return false;
    },

    getLockedElement: function () {
        var self = this;
        return self._lockedElement;
    },

    reuse: function (model) {
        //this.initData(model);
    },

    unuse: function () {
        this._sprite.y = 0;
        this._sprite.stopAllActions();
        this.stopAllActions();
        this.unscheduleUpdate();
        this.removeFromParent(true);
    },

    hit: function () {
        this._hitCount++;
        if (this._hitCount > this._hitTotalCount) {
            this.doDied();
        }
    },

    getModel: function () {
        return this._model;
    }


    /*debugInit: function () {
     this._debugSp = null;
     this._debugSp = [];
     for (var i = 0; i < this._collisionLen; ++i) {
     this._debugSp[i] = new cc.Sprite(Res.COMMON.DEBUG);
     var circle = this._collisionData.circleArray[i];
     this._debugSp[i].setPosition(circle.pt);
     this._debugSp[i].setScale(circle.r * 2 / 100);
     cf.debugNode.addChild(this._debugSp[i], 0);
     }
     },

     debugUpdate: function (pt) {
     self._debugSp[i].setPosition(pt);
     },

     debugClear: function () {
     for (var i = 0; i < this._collisionLen; ++i) {
     this._debugSp[i].removeFromParent(true);
     }
     }*/


});

/**
 * 子弹状态
 */
Bullet.State = {
    MOVE: 0,             //< 移动
    COLLISION: 1,        //< 碰撞
    OPEN: 2,             //< 张网
    STANDBY: 3,          //< 待机
    DIED: 4              //< 死亡
};

Bullet.create = function () {
    var exist = cc.pool.hasObject(Bullet);
    if (exist) {
        return cc.pool.getFromPool(Bullet);//调用reuse方法
    }
    else {
        return new Bullet();
    }
};