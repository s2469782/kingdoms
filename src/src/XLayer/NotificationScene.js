var NotificationScene = cc.Node.extend({
    _onLocalErrorListener: null,        //< 本地错误监听
    _onNetMsgListener: null,
    _intervalTime: 0,
    _toast: null,
    _bOnce: false,

    onEnter: function () {
        this._super();
        this.addListener();
        this.setVisible(false);
        this._intervalTime = 0;
        this._bOnce = false;
        this.scheduleUpdate();
    },

    onExit: function () {
        this._super();
        //这里不做移除监听的动作，native版本关闭游戏时会移除所有的监听
        //this.removeListener();
    },

    /**
     * 计算全局时间
     * @param dt
     */
    update: function (dt) {
        var localPlayer = cf.PlayerMgr.getLocalPlayer();
        if (localPlayer) {
            //转换成时间戳
            localPlayer.setOffsetGameTime(dt * 1000);

            //超过180秒未进行任务操作，则移除玩家
            if(cf.SceneMgr.getCurSceneFlag() != cf.SceneFlag.LOGIN){
                cf.RemoveUserTime += dt;
                var _time = 180 - Math.floor(cf.RemoveUserTime);
                if(_time <= 0 && !cf.bRemoveUser){
                    cf.RemoveUserTime = 0;
                    cf.bRemoveUser = true;
                    //通知弹窗移除用户
                    cf.dispatchEvent(NotificationScene.eventName, {
                        flag: NotificationScene.Error.ERROR_REMOVE_USER,
                        error: ""
                    });
                }
            }

        }
    },

    /**
     * 添加监听
     */
    addListener: function () {
        this._super();
        cc.log("NotificationLayer:addListener");
        var self = this;

        //创建网络消息
        var msgDataArray = [
            //错误消息监听
            {msgId: MessageCode.SC_ErrorMsg, callFunc: self._resError},
            //跑马灯消息监听
            {msgId: MessageCode.HC_ScrollInfoMsg, callFunc: self._resScrollInfo},
            //图片公告监听
            {msgId: MessageCode.HC_PicNoticeInfoMsg, callFunc: self._resPicAnnouncement},
            //邮件监听
            {msgId: MessageCode.SC_AskPlayerGetEmailMsg, callFunc: self._resEmail},
            //任务完成监听
            {msgId: MessageCode.SC_TaskFinishMsg, callFunc: self._resTaskFinish},
            //活动修改监听
            {msgId: MessageCode.HC_ModifyActScoreMsg, callFunc: self._resActivityChange},
            //活动剩余领取数量完成监听
            {msgId: MessageCode.HC_UpdateActRule, callFunc: self._resActPVolume},
            //请求活动监听(更新活动)
            {msgId: MessageCode.CH_AskActivityMsg_Res, callFunc: self._resActInfo},
            //福利金
            {msgId: MessageCode.SC_BillingWelfare, callFunc: self._resBillingWelfare},
            //全服聊天
            {msgId: MessageCode.SM_AllServerChat, callFunc: self._resAllServerChat},
            //获取大奖赛排行榜信息
            {msgId: MessageCode.CH_GrandPrixRankMsg_Res, callFunc: self._resGrandPrixRank},
            //大奖赛每日首次比赛积分超过2000分即可获得100钻石奖励
            {msgId: MessageCode.HC_GrandPrixRewardMsg, callFunc: self._resGrandPrixRewardMsg},           
            // HC_UpdateGrandRankMsg Gate通知客户端更新大奖赛数据
            {msgId: MessageCode.HC_UpdateGrandRankMsg, callFunc: self._resUpdateGrandRankMsg},
            // SM_EventNotice 事件公告比如玩家打到大鱼
            {msgId: MessageCode.SM_EventNotice, callFunc: self._resPlayerEventScrollInfo},
            // CH_AskExchangeInfoMsg_Res 客户端请求兑换奖品数据结果
            {msgId: MessageCode.CH_AskExchangeInfoMsg_Res, callFunc: self._resExchangeInfo},
            // HC_NewExchanInvenData 通知客户端新的兑换库存数据
            {msgId: MessageCode.HC_NewExchanInvenData, callFunc: self._resNewExchanInvenData},
            // HC_UpdateExchangeMsg 通知客户端更新兑换奖品数据
            {msgId: MessageCode.HC_UpdateExchangeMsg, callFunc: self._resUpdateExchange},
            // HC_RefreshLuckDraw 刷新客户端抽奖数据
            {msgId: MessageCode.HC_RefreshLuckDraw, callFunc: self._resRefreshLuckDraw},
        ];

        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener[i] = cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this));
        }

        //创建本地错误监听
        if (self._onLocalErrorListener == null) {
            self._onLocalErrorListener = cc.EventListener.create({
                event: cc.EventListener.CUSTOM,
                eventName: NotificationScene.eventName,
                callback: function (event) {
                    self._resLocalError(event.getUserData());
                }
            });
            cc.eventManager.addListener(self._onLocalErrorListener, 1);
        }

        cc.eventManager.addCustomListener(cc.game.EVENT_HIDE, function () {
            if(!self._bOnce){
                cc.log("EVENT_HIDE");
                self._bOnce = true;
                self._intervalTime = Math.floor(new Date().getTime());
                if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.GAME) {
                    //这里临时处理，以后要改成断线重连
                    cf.NetMgr.closeSocket();
                    SDKHelper.setInBack(true);                    
                }
                if (cc.sys.os === cc.sys.OS_IOS) {
                    cf.SoundMgr.stopMusic();
                }
            }
        });

        cc.eventManager.addCustomListener(cc.game.EVENT_SHOW, function () {
            if(self._bOnce) {
                cc.log("EVENT_SHOW");
                self._bOnce = false;

                var time = (Math.floor(new Date().getTime()) - self._intervalTime);
                //if (time > 120000) {
                if (time > 120000) {
                    cf.NetMgr.closeSocket();
                    self.removeAllChildren(true);
                    if (cc.sys.isNative) {
                        cf.SceneMgr.runScene(cf.SceneFlag.UPDATE, null);
                    } else {
                        if(cc.sys.browserType == cc.sys.BROWSER_TYPE_IE || cc._renderType == cc.game.RENDER_TYPE_CANVAS) {
                            cf.SceneMgr.runScene(cf.SceneFlag.LOGO,null);
                        }else{
                            cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                        }
                        
                    }
                    return;
                }

                var localPlayer = cf.PlayerMgr.getLocalPlayer();
                if (localPlayer) {
                    //补时间差                
                    localPlayer.setOffsetGameTime(time);
                    cf.SoundMgr.stopAllEffects();
                    if (cc.sys.os === cc.sys.OS_IOS) {
                        if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY) {
                            cf.SoundMgr.playMusic(2, true);
                        }
                    }
                }

                //断线重连逻辑
                if (SDKHelper.isInBack() && SDKHelper._resultState == -1) {
                    cf.dispatchEvent(GameScene.Reload, {bReturn: true});
                }
            }
        });
    },

    /**
     * 移除监听
     */
    removeListener: function () {
        this._super();
        cc.log("NotificationLayer:removeListener");
        //移除网络监听
        var len = this._onNetMsgListener.length;
        for (var i = 0; i < len; ++i) {
            cf.removeListener(this._onNetMsgListener[i]);
        }
    },

    /**
     * 請求活動訊息
     */     
    sendActivityMsg: function(){
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        
        var strRuleVer = [];
        
        //先从本地获取活动信息
        var actInfoList = cf.SaveDataMgr.getNativeData("actInfoList");
        if (actInfoList == null || actInfoList == "") {
            
        } else {
            actInfoList = JSON.parse(actInfoList);
            for (var i = 0; i < actInfoList.length; ++i) {
                var strSelf = {};
                var actRuleInfoList = actInfoList[i].actRuleInfoList;
                var strRule = [];
                for (var j = 0; j < actRuleInfoList.length; j++) {
                    if (actRuleInfoList[j].nIndex && actRuleInfoList[j].updateTime && actRuleInfoList[j].updateTime != "") {
                        var str = actRuleInfoList[j].nIndex + "," + actRuleInfoList[j].updateTime;
                        strRule.push(str);
                    }
                }
                strSelf.nActID = [];
                strSelf.nVersion = [];
                strSelf.strRuleTime = strRule;
                strRuleVer.push(strSelf);
            }
        }
        

        var msg = new $root.CH_AskActivity();
        msg.nidLogin = playerModel.getIdLogin();
        msg.strSafeCode = playerModel.getSafeCode();
        msg.strRuleVer = {};
        var wr = $root.CH_AskActivity.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_AskActivityMsg, wr);

    },


    _resPlayerEventScrollInfo: function (msg) {
        
        var resMsg = $root.SM_EventNotice.decode(msg);
        if(resMsg) {
            //中龍珠JP清空本地端
            if(resMsg.nEventType== 8){
                cf.SaveDataMgr.setNativeData("dragonHero", null);
                cf.SaveDataMgr.setNativeData("dragonHero1", null);
            }
           
            var scrollInfoMsg = {};
            scrollInfoMsg.strTitle = resMsg.playerName;
            scrollInfoMsg.strLevel = resMsg.nVipLv;
            //事件通知类型 2鱼事件通知 4为扑克鱼通知
            scrollInfoMsg.strEventType = resMsg.nEventType;
            //通知时间的条件
            //炮倍  //扑克鱼倍数索引
            scrollInfoMsg.strCondition1 = resMsg.nCondition1;
            //鱼id或者分值 //扑克鱼金币数 //如果nItemID不为0，弹头数量
            scrollInfoMsg.strCondition2 = resMsg.nCondition2;
            //鱼id
            scrollInfoMsg.strCondition3 = resMsg.nCondition3;
            scrollInfoMsg.nItemID = resMsg.nItemID;
            MsgPool.getInstance().addAnnouncementElement(scrollInfoMsg);
        }
    },

    /**
     * 跑马灯消息
     */
    _resScrollInfo: function (msg) {
        var resMsg = $root.DG_ScrollInfo.decode(msg);
        if (resMsg) {
            var length = resMsg.scrollInfoList.length;
            for (var i = 0; i < length; i++) {
                var scrollInfo = resMsg.scrollInfoList[i];
                if (scrollInfo.nState == 0) {
                    scrollInfo.nextPlayTime = scrollInfo.strStartTime;
                    var coverArray = MsgPool.getInstance().getAElementByScrollInfoId(scrollInfo.nScrollID);
                    if (coverArray) {
                        coverArray = scrollInfo;
                    }
                    else {
                        MsgPool.getInstance().addAnnouncementElement(scrollInfo);
                    }
                }
                else {
                    MsgPool.getInstance().removeAnnouncementElement(scrollInfo.nScrollID);
                }
            }
            var localArray = MsgPool.getInstance().getAnnouncementArray();
            var localLength = MsgPool.getInstance().getAnnouncementArrayLength();
            for (var i = 0; i < localLength - 1; i++) {
                for (var j = 0; j < localLength - 1 - i; j++) {
                    var localInfo1 = localArray[j];
                    var localInfo2 = localArray[j + 1];
                    if (localInfo1.strStartTime > localInfo2.strStartTime) {
                        var tmp = localInfo1;
                        localInfo1 = localInfo2;
                        localInfo2 = tmp;
                    }
                }
            }
        }
    },

    /**
     * 图片公告
     * @param msg
     * @private
     */
    _resPicAnnouncement: function (msg) {
        if (!msg) {
            cc.warn("图片公告内容为空");
            return;
        }
        var resMsg = $root.DG_PicNoticeInfo.decode(msg);
        var length = resMsg.picNoticeInfoList.length;
        //使用临时的数组进行排序
        var localArray = [];
        for (var i = 0; i < length; i++) {
            var localMsg = resMsg.picNoticeInfoList[i];
            var coverArray = MsgPool.getInstance().getAElementByPopupAnnounceInfoId(localMsg.nPicNoticeID);
            if (coverArray) {
                coverArray = resMsg.picNoticeInfoList[i];
            }
            else {
                localArray.push(localMsg);
                cc.log("添加图片公告" + localMsg.nPicNoticeID +localMsg.strStartTime + localMsg.strEndTime);
            }
        }
        for (var i = 0; i < length - 1; i++) {
            for (var j = 0; j < length - 1 - i; j++) {
                var localInfo1 = localArray[j];
                var localInfo2 = localArray[j + 1];
                if (localInfo1.nSort > localInfo2.nSort) {
                    var tmp = localInfo1;
                    localInfo1 = localInfo2;
                    localInfo2 = tmp;
                }
            }
        }
        MsgPool.getInstance().setPicAnnouncementElement(localArray);
    },

    /**
     * 邮件
     * @param msg
     * @private
     */
    _resEmail: function (msg) {
        cf.SaveDataMgr.setNativeData(cf.EmailNoReadNum, "1");
        if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY) {
            cf.dispatchEvent(LobbyLayer.DoEvent, {flag:LobbyLayer.EventFlag.UI_EMAILREAD,bShow:true});
        }
    },

    /**
     * 任务完成
     * @param msg
     * @private
     */
    _resTaskFinish: function (msg) {
        var resMsg = $root.SC_TaskFinish.decode(msg);
        cc.log("完成任务 id = " + resMsg.nTaskID);

        var localPlayer = cf.PlayerMgr.getLocalPlayer();
        var taskId = parseInt(resMsg.nTaskID);
        if (taskId < 20000) {
            //成长任务
            localPlayer.addTaskCount(cf.TaskType.GROW, 1);
        } else if (taskId < 30000) {
            //每日任务
            localPlayer.addTaskCount(cf.TaskType.DAY, 1);
        } else {
            //周常任务
            localPlayer.addTaskCount(cf.TaskType.WEEK, 1);
        }

        if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY) {
            cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UI_TASK_RED);
        }
    },

    /**
     * 解析全服消息
     */
    _resAllServerChat: function (msg) {
        var resMsg = $root.CS_ChatInAllServer_Res.decode(msg);
        if(resMsg) {
            // 座位id
            var seatId = resMsg.nSeatID;
            // 玩家昵称
            var name = resMsg.strNickname;
            // 玩家vip等级
            var vipLevel = resMsg.nVIPLevel;
            // 聊天内容
            var str = resMsg.strChat;
            // 当前服务器时间
            var strNowTime = resMsg.strNowTime;
            // 道具列表
            var itemReward = resMsg.itemReward;
            //获取本地玩家数据
            var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
            if(name == localPlayerModel.getNickName()) {
                var length = itemReward.length;
                var itemData = cf.Data.ITEM_DATA;
                for (var i = 0; i < length; i++) {
                    var _itemId = itemReward[i].nItemID;

                    var _num = itemReward[i].nNum;
                    if (itemReward[i].nError != 0)
                        _num = itemReward[i].nItemNum;
                    var strEndTime = null;
                    if (itemReward[i].strEndTime) {
                        strEndTime = itemReward[i].strEndTime;
                    }
                    localPlayerModel.setPlayerItem(_itemId, itemReward[i].nItemNum, strEndTime, itemReward[i].nError);                   
                }
                cf.dispatchEvent(BaseRoom.DoEvent,BaseRoom.EventFlag.UPDATE_ROOM_UI);
                cf.dispatchEvent(BaseRoom.DoEvent,BaseRoom.EventFlag.SENDSUCCESSOFALLSERVERCHAT);
            }
            var strInfo = {};
            strInfo.strTitle = name;
            strInfo.strLevel = vipLevel;
            strInfo.strContent = str;
            MsgPool.getInstance().addChatMsgALLElement(strInfo);
            MsgPool.getInstance().addChatRecordServer(strInfo);
            //cf.dispatchEvent(ChatLayer.DoEvent,ChatLayer.EventFlag.UPDATECHATRECORD);
        }
    },

    /**
     * 错误消息处理
     * @param msg
     * @private
     */
    _resError: function (msg) {
        this.setVisible(true);

        //隐藏toast
        cf.UITools.hideLoadingToast();

        //解析错误消息
        cf.ErrorMgr.parseGameError(msg);
        
        //解析错误消息
        /*var resMsg = $root.SC_ErrorMsg.decode(msg);
        var error = resMsg.nError;
        cc.error(error);
        
        //统计错误消息
        AnalyticsHelper.DCReportError("网络错误:"+resMsg.nError,resMsg.nError+"");

        var errorDlg = null;
        switch (resMsg.nError) {
            case MessageCode.ErrorCode_CostEnough:
                this.showToast("货币不足");
                break;
            case MessageCode.ErrorCode_NoGold://金币不足
                this.showToast(cf.Language.getText("text_1069"));
                break;
            case MessageCode.ErrorCode_NoLottery:
                this.showToast(cf.Language.getText("text_1450"));
                break;
            case MessageCode.ErrorCode_NoDiamond://钻石不足
                break;
            case MessageCode.ErrorCode_LevelLess://玩家等级不足
                break;
            case MessageCode.ErrorCode_FortLess://玩家炮等级不足
                errorDlg = UIDialog.createError(error, function () {
                    //如果不在大厅则返回大厅
                    if (cf.SceneMgr.getCurSceneFlag() != cf.SceneFlag.LOBBY) {
                        cf.SceneMgr.runScene(cf.SceneFlag.LOBBY, null);
                    }
                    this.setVisible(false);
                }, this);
                break;
            case MessageCode.ErrorCode_AlreadyReceive: // 已经领取，不能重复领取
                this.showToast(cf.Language.getText("text_1070"));
                break;
            case MessageCode.ErrorCode_NoJackpotDraw:// 个人奖金不足
                this.showToast(cf.Language.getText("text_1071"));
                break;
            case MessageCode.ErrorCode_NoBonusFish: //没打够彩金鱼
                this.showToast(cf.Language.getText("text_1072"));
                break;
            case MessageCode.ErrorCode_WaitJoin://已经在游戏中，请等待离开
                errorDlg = UIDialog.createError(error, function () {
                    //如果不在大厅则返回大厅
                    if (cf.SceneMgr.getCurSceneFlag() != cf.SceneFlag.LOBBY) {
                        cf.SceneMgr.runScene(cf.SceneFlag.LOBBY, null);
                    }
                    this.setVisible(false);
                }, this);
                break;
            case MessageCode.ErrorCode_PasswordError:
                if(cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY){
                    cf.UITools.showHintToast("旧密码错误");
                }else{
                    //断开连接
                    cf.NetMgr.closeSocket();
                    errorDlg = UIDialog.createError(error, function () {
                        //重置保存密码状态
                        cf.SaveDataMgr.setNativeData("game_checkState",0);
                        //直接返回登录界面
                        cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                        SDKHelper.logout();
                        this.setVisible(false);
                    }, this);
                }
                break;
            case MessageCode.ErrorCode_AccountError:
            case MessageCode.ErrorCode_Blockade:
            case MessageCode.ErrorCode_SafeCodeError:
            case MessageCode.ErrorCode_NoFreeGS:
            case MessageCode.ErrorCode_RoomError:
            case MessageCode.ErrorCode_RemoteLogin:
            case MessageCode.ErrorCode_LackData:
            case MessageCode.ErrorCode_NoPlayer:
            case MessageCode.ErrorCode_ReSignIn:
            case MessageCode.ErrorCode_RoomID:
            case MessageCode.ErrorCode_CheckError:
            case MessageCode.ErrorCode_LackIdentity:
            case MessageCode.ErrorCode_AppIDError:
            case MessageCode.ErrorCode_ScriptError:
            case MessageCode.ErrorCode_CodeError:
            case MessageCode.ErrorCode_UidError:
            case MessageCode.ErrorCode_TokenError:
            case MessageCode.ErrorCode_LinkTimeout:
                //断开连接
                cf.NetMgr.closeSocket();
                errorDlg = UIDialog.createError(error, function () {
                    //直接返回登录界面
                    cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                    SDKHelper.logout();
                    this.setVisible(false);
                }, this);
                break;
            case MessageCode.ErrorCode_NoTime:
                cf.UITools.showHintToast("在线奖励的时间不满足条件");
                break;
            case MessageCode.ErrorCode_NoVIP:
                errorDlg = UIDialog.create("error_" + error, "text_1125", "text_1098", false, function (type) {                    
                    this.setVisible(false);
                }, this);
                break;
            case MessageCode.ErrorCode_SkillNoTime:
                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.ERROR_LOCK_NOTIME);
                break;
            case MessageCode.ErrorCode_NoFish:
                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.ERROR_LOCK_NOFISH);
                break;
            case MessageCode.ErrorCode_NotFindAct:     // 没有找到活动
                this.showToast(cf.Language.getText("error_741"));
                break;
            case MessageCode.ErrorCode_NicknameRepeat:      //< 昵称错误
                this.showToast(cf.Language.getText("error_740"));
                break;
            case MessageCode.ErrorCode_LimitChar: //< 限制字符
                this.showToast(cf.Language.getText("error_738"));
                break;
            case MessageCode.ErrorCode_NicknameLong:    //< 昵称太长
                this.showToast(cf.Language.getText("error_739"));
                break;
            case MessageCode.ErrorCode_NoPay:          // 订单未支付                
                if(SDKHelper._orderId){
                    var isPolling = SDKHelper.pollingOrder();                   
                    if(!isPolling){
                        errorDlg = UIDialog.createError(error, function () {
                            this.setVisible(false);
                            SDKHelper.clearPayState();
                        }, this);
                    }
                }else{
                    errorDlg = UIDialog.createError(error, function () {
                        this.setVisible(false);
                        SDKHelper.clearPayState();
                    }, this);
                }
                break;
            case MessageCode.ErrorCode_NoOrder:        // 订单不存在
            case MessageCode.ErrorCode_NoPayCode:      // 商品不存在
            case MessageCode.ErrorCode_NoCardTime:     // 月卡过期
            case MessageCode.ErrorCode_RepeatReceive:  // 重复领取
                errorDlg = UIDialog.createError(error, function () {
                    this.setVisible(false);
                }, this);
                break;
            case MessageCode.ErrorCode_TreasureHuntOver: //进行寻宝请求时已经过期
                cf.dispatchEvent(BaseRoom.DoEvent,BaseRoom.EventFlag.ERROR_TREASUREHUNT_OUTOFTIME);
                break;
            case MessageCode.ErrorCode_FishFull:
                cf.dispatchEvent(BaseRoom.DoEvent,BaseRoom.EventFlag.ERROR_FISHNUM_OUTOFLIMIT);
                break;
            case MessageCode.ErrorCode_NoCondition:
                if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY) {
                    errorDlg = UIDialog.createError(error, function () {
                    this.setVisible(false);
                }, this);
                }
                else if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.GAME) {
                    cf.dispatchEvent(BaseRoom.DoEvent,BaseRoom.EventFlag.ERROR_CARDNUM_NOTENOUGH);
                }
                break;
            case MessageCode.ErrorCode_NoBullet:
                //关闭自动开炮
                var seatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
                var player = cf.RoomMgr.getRoomPlayer(seatId);
                if(player.getAutoFireState()){
                    player.setBAutoFireState(false);
                    cf.CanContinueAutoFire = false;
                }

                var jsonInfo = cf.SaveDataMgr.getNativeData("GrandPrixRank");

                if (jsonInfo) {
                    //将字符串转化为json
                    var jsonData = JSON.parse(jsonInfo);
                    if(jsonData.nIntegralNum >jsonData.nTodayIntegral){

                        jsonData.nTodayIntegral = jsonData.nIntegralNum;
                        //存入本地
                        cf.SaveDataMgr.setNativeData("GrandPrixRank", JSON.stringify(jsonData));
                    }
                }

                //子弹数量不足error
                errorDlg = UIDialog.createError(error, function () {
                    cf.PlayerMgr.getLocalPlayer().leaveRoom({flag: cf.SceneFlag.LOBBY});
                    this.setVisible(false);
                }, this);
                break;
            case MessageCode.ErrorCode_Version://版本与服务器不匹配
                //断开连接
                cf.NetMgr.closeSocket();
                errorDlg = UIDialog.create("error_" + error, "text_1098", null, false, function (type) {
                    if (type == UIDialog.Btn.LEFT) {
                        //忽略
                    }else {
                        //下载链接
                        if(cc.sys.os === cc.sys.OS_ANDROID){
                            //cf.JsbHelper.openURL(GameConfig.download_url);
                        }else if(cc.sys.os === cc.sys.OS_IOS){
                            //cf.JsbHelper.openURL(GameConfig.download_url);
                        }
                    }
                    this.setVisible(false);
                }, this);
                break;
            case MessageCode.ErrorCode_NoLuckDrawNum://抽奖次数不足
            case MessageCode.ErrorCode_NoAccPoints://积分不足
                errorDlg = UIDialog.createError(error, function () {
                    this.setVisible(false);
                }, this);               
                break;
            case MessageCode.ErrorCode_NoDoleNum://救济金次数不足
                cf.dispatchEvent(BaseRoom.DoEvent,BaseRoom.EventFlag.DOLEHASGETALLTIMES);
                break;
            case MessageCode.ErrorCode_NoChat:
                cf.UITools.showHintToast("您已被禁言");
                break;
            case MessageCode.ErrorCode_Inv:
                cf.UITools.showHintToast("邀请码错误",2);
                break;
            case MessageCode.ErrorCode_NoAuthorityEmail:
                cf.UITools.showHintToast("您无权限向此用户发送邮件",2);
                break;
            case MessageCode.ErrorCode_NoSell:
                errorDlg = UIDialog.createCustom("您拥有总金币数大于10亿，暂时无法出售", "确定", null, false, function (type) {
                    this.setVisible(false);
                }, this);
                break;
            case MessageCode.ErrorCode_ChipsIsNotEnough:
                errorDlg = UIDialog.createCustom("抱歉您的筹码不足", "确定", null, false, function (type) {
                    this.setVisible(false);
                }, this);
                break;
            case MessageCode.ErrorCode_CanNotBets:
                errorDlg = UIDialog.createCustom("下注已经达到上限,请选择其他类别", "确定", null, false, function (type) {
                    this.setVisible(false);
                }, this);
                break;
            case 0:
                break;
            default:
                errorDlg = UIDialog.createError(error, function () {
                    this.setVisible(false);
                }, this);
                break;
        }

        if (errorDlg) {
            this.removeAllChildren(true);
            this.addChild(errorDlg);
        }*/
    },

    /**
     * 本地消息错误解析
     * @param msg
     * @private
     */
    _resLocalError: function (msg) {
        this.setVisible(true);

        //隐藏Toast
        cf.UITools.hideLoadingToast();

        //解析错误消息
        cf.ErrorMgr.parseLocalError(msg);
        /*var flag = msg;
        if (typeof msg == "object") {
            flag = msg.flag;
        }
        //解析错误消息
        cc.error(flag);

        //统计错误
        AnalyticsHelper.DCReportError("本地错误:"+flag,flag+"");

        var errorDlg = null;
        switch (flag) {
            case NotificationScene.Error.ERROR_NET_CONNECT:
                cf.NetMgr.closeSocket();
                errorDlg = UIDialog.createCustom(cf.Language.getText("error_" + flag),
                    cf.Language.getText("text_1098"),
                    null, false,
                    function () {
                        //直接返回登录界面
                        if (cf.SceneMgr.getCurSceneFlag() != cf.SceneFlag.LOGIN) {
                            cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                        }
                        SDKHelper.logout();
                        this.setVisible(false);
                    }, this);
                break;
            case NotificationScene.Error.ERROR_SOCKET_CLOSE:
                if (cf.NetMgr.isCloseSocket()) {
                    //主动断开连接，不处理
                } else {
                    //直接返回登录界面
                    if (cf.SceneMgr.getCurSceneFlag() != cf.SceneFlag.LOGIN) {
                        cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                    }

                    errorDlg = UIDialog.createCustom(cf.Language.getText("error_" + flag),
                        cf.Language.getText("text_1098"),
                        null, false,
                        function () {
                            SDKHelper.logout();
                            this.setVisible(false);
                        }, this);
                }
                break;
            case NotificationScene.Error.ERROR_SOCKET_TIMEOUT:
                errorDlg = UIDialog.createError(flag, function () {
                    //直接返回登录界面
                    var flag = cf.SceneMgr.getCurSceneFlag();
                    if (flag == cf.SceneFlag.GAME_LOADING || flag == cf.SceneFlag.GAME) {
                        cf.SceneMgr.runScene(cf.SceneFlag.LOBBY, null);
                    } else {
                        cf.NetMgr.closeSocket();
                        if (flag != cf.SceneFlag.LOGIN) {
                            cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                        }
                        SDKHelper.logout();
                    }
                    this.setVisible(false);
                }, this);
                break;
            case NotificationScene.Error.PAY_SUCCESS:
                errorDlg = UIDialog.create("text_1220", "text_1222", "text_1221", false, function (type) {
                    if (type == UIDialog.Btn.RIGHT) {
                        SDKHelper.orderQuery(msg.orderId,true);
                    }
                    this.setVisible(false);
                }, this);
                break;
        }

        if (errorDlg) {
            this.removeAllChildren(true);
            this.addChild(errorDlg);
        }*/
    },
    /**
     * 活动数据修改
     * @param msg
     * @private
     */
    _resActivityChange: function (msg) {
        var resMsg = $root.HC_ModifyActScoreMsg.decode(msg);

        var playerModel = cf.PlayerMgr.getLocalPlayer();

        if (resMsg.nRes == 0) {
            if (resMsg.nidLogin == playerModel.getIdLogin()) {

                //修改活动分数数据
                var actRecordList = playerModel.getActRecordList();
                //cc.log(actRecordList);

                if (actRecordList) {
                    var value = actRecordList.get(resMsg.nActID);
                    //cc.log(value);
                    if(value) {
                        if (resMsg.nScore) {
                            value.nScore = resMsg.nScore;
                            actRecordList.set(resMsg.nActID, value);
                        }
                    }
                }
                else {
                    actRecordList = new Array();
                    var info = {};
                    info.nActID = resMsg.nActID;                                //活动id
                    if(resMsg.nScore){
                        info.nScore = resMsg.nScore;
                    }
                    info.strOneTimeData = "";
                    actRecordList.push(info);
                    playerModel.setActRecordList(actRecordList);
                }

                //cc.log(actRecordList);
                //cc.log(playerModel.getActRecordList());
                //判断是否有充值返利活动 有的话刷新充值数值
                var jsonInfo = cf.SaveDataMgr.getNativeData("actInfoList");
                var jsonData = JSON.parse(jsonInfo);
                for(var i in jsonData) {
                    if (jsonData[i].nType == cf.ACTTagFlag.TopUP) {
                        cf.dispatchEvent(ActTAccumulateBD.DoEvent, ActTAccumulateBD.EventFlag.UI_UPDATE_MONEY);
                    }
                }
                //刷新大厅小红点
                cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UI_ACTIVIYT_RED);
            }
            else {
                cc.log("错误！！！不是本人活动数据！");
            }
        }
        else {
            cc.log("活动无修改！");
        }
    },

    showToast: function (text, delay, callback) {
        this.hideToast();
        if (delay === undefined)
            delay = 1;
        if (this._toast == null) {
            this.setVisible(true);
            this._toast = UIHintToast.createDlg(text, delay, function () {
                this.hideToast();
                if (callback)
                    callback();
            }, this);
            this.addChild(this._toast, 100);
        }
    },

    hideToast: function () {
        if (this._toast) {
            this.setVisible(false);
            this._toast.removeFromParent(true);
            this._toast = null;
        }
    },

    /**
     * 活动规则更新
     * @param msg
     * @private
     */
    _resActPVolume:function(msg){
        var resMsg = $root.HC_UpdateActRule.decode(msg);
        cc.log(resMsg);
        if(resMsg){
            var jsonInfo = cf.SaveDataMgr.getNativeData("actInfoList");
            //将字符串转化为json
            var jsonData = JSON.parse(jsonInfo);
            for(var i in jsonData){
                var actRuleInfoList = jsonData[i].actRuleInfoList;
                for(var j = 0; j < actRuleInfoList.length; j ++){
                    for(var k = 0; k < resMsg.actRuleInfo.length; k++){
                        if(actRuleInfoList[j].idActivityRule == resMsg.actRuleInfo[k].idActivityRule&&
                            actRuleInfoList[j].idActivity == resMsg.actRuleInfo[k].idActivity){
                            cc.log(actRuleInfoList[j]);
                            cc.log(resMsg.actRuleInfo[k]);
                            actRuleInfoList[j] = resMsg.actRuleInfo[k];
                        }
                    }
                }
            }
            cc.log(jsonData);
            //修改活动规则
            cf.SaveDataMgr.setNativeData("actInfoList", JSON.stringify(jsonData));

            //刷新大厅UI
            cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UI_UPDATE);
        }
    },
    /**
     * 请求活动信息结果(更新活动信息结果)
     * @param msg
     * @private
     */
    _resActInfo: function (msg) {
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var resMsg = $root.CH_AskActivity_Res.decode(msg);
        
        if (resMsg) {
            
            cc.log(resMsg.actInfoList);
            cc.log(resMsg.actRecordList);
            cc.log(resMsg.actInfoList.length);
           
            if (resMsg.actInfoList.length > 0 && resMsg.actInfoList != null) {
                
                for(var a = 0; a <resMsg.actInfoList.length; a++){
                    if(resMsg.actInfoList[a].nActID == -999){
                        this.sendActivityMsg();

                    }
                    
                }
                cf.activityInfo = resMsg;
               
                //获取信息存入本地数据
                var actInfoList = cf.SaveDataMgr.getNativeData("actInfoList");
                if (actInfoList == null || actInfoList == "") {

                    var jsonData = resMsg.actInfoList;

                    var loop = jsonData.length;
                    for (var i =0; i < loop;i ++) {
                        for(j = i + 1; j < loop; j++){
                            if(jsonData[i].nType ==jsonData[j].nType ){
                                jsonData.splice(j,1);
                                cc.log(j);
                                j = j - 1;    //改变循环变量
                                loop = loop - 1;   //改变循环次数
                            }
                        }
                    }
                    
                    jsonData = this._delectRedundant(jsonData);
                    cf.SaveDataMgr.setNativeData("actInfoList", JSON.stringify(jsonData));
                }else{
                    //将字符串转化为json
                    var jsonData = JSON.parse(actInfoList);

                    //添加新活动
                    for (var i in resMsg.actInfoList) {

                        jsonData.push(resMsg.actInfoList[i]);
                        
                        var loop = jsonData.length;
                        //删除过期数据
                        for (var j =0; j < loop;j ++) {
                            if (jsonData[j].nType != cf.ACTTagFlag.Roulette && jsonData[j].nType != cf.ACTTagFlag.Champions) {
                                //判断活动是否在显示期内
                                if (jsonData[j].strEndShowTime < playerModel.getGameTime()) {
                                    jsonData.splice(j, 1);
                                    j = j - 1;    //改变循环变量
                                    loop = loop - 1;   //改变循环次数
                                }
                            }
                        }
                    }
                    
                    jsonData = this._delectRedundant(jsonData);

                    var loop = jsonData.length;
                    //删除失效活动
                    for (var j =0; j < loop;j ++) {
                        if (jsonData[j].nState  == 1) {
                            jsonData.splice(j,1);
                            j = j - 1;    //改变循环变量
                            loop = loop - 1;   //改变循环次数
                        }
                    }
                    cc.log(jsonData);
                    cf.SaveDataMgr.setNativeData("actInfoList", JSON.stringify(jsonData));
                }
            }

            if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY) {
                //如果是在大厅并且
                cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UPDATE_ACTIVITY);
            }
            else if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOGIN ||
                cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.UPDATE) {
            }
            if(resMsg.actRecordList.length != 0){
                playerModel.setActRecordList(resMsg.actRecordList);
            }

           
        }
        else {
            cc.log("活动数据获取失败");
        }
    },

    /**
     * 福利金
     * @param msg
     * @private
     */
    _resBillingWelfare:function(msg){

        var resMsg = $root.SC_BillingWelfare.decode(msg);
        cc.log("福利金的索引值为：" + resMsg.nBillingWelfare);
        if(resMsg){
            if(resMsg.nidLogin == cf.PlayerMgr.getLocalPlayer().getIdLogin()){
                cf.PlayerMgr.getLocalPlayer().setBillingWelfare(resMsg.nBillingWelfare);
            }
        }
    },

    /**
     * 获取大奖赛排行榜信息
     * @param msg
     * @private
     */
    _resGrandPrixRank:function(msg){
        var resMsg = $root.CH_GrandPrixRankMsg_Res.decode(msg);
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        if(resMsg){
            //存入本地
            cf.SaveDataMgr.setNativeData("GrandPrixRank", JSON.stringify(resMsg));

            var IsBUpdateGrandRank = playerModel.getIsBUpdateGrandRank();
            if(IsBUpdateGrandRank)
                cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UPDATE_GRANDPRIX);
            else
                cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UI_GRANDPRIX);


            playerModel.setIsBUpdateGrandRank(false);
        }
    },

    /**
     * 每日大奖赛积分超过2000获得奖励
     * @param msg
     * @private
     */
    _resGrandPrixRewardMsg:function(msg){
        var resMsg = $root.HC_GrandPrixRewardMsg.decode(msg);
        if(resMsg){
            this.setVisible(true);
            //获得奖励
            this.removeAllChildren(true);
            var dlg = UIAwardToast.create(resMsg.itemReward, function () {
                this.setVisible(false);},this,false,cf.DCType.DC_SYSTEM_ACT);
            this.addChild(dlg, 101);
        }
    },   
    
    _resUpdateGrandRankMsg:function(msg){
        var resMsg = $root.HC_UpdateGrandRankMsg.decode(msg);
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        if(playerModel) {
            if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY) { //将字符串转化为json
                var jsonInfo = cf.SaveDataMgr.getNativeData("GrandPrixRank");
                //将字符串转化为json
                var jsonData = JSON.parse(jsonInfo);
                var msg = new $root.CH_GrandPrixRankMsg();
                if (jsonInfo == null || jsonInfo == "") {

                    msg.nUpdateTime = 0;
                }
                else {
                    msg.nUpdateTime = parseInt(jsonData.nUpdateTime);
                }
                var wr = $root.CH_GrandPrixRankMsg.encode(msg).finish();
                cf.NetMgr.sendSocketMsg(MessageCode.CH_GrandPrixRankMsg, wr);
            }
            playerModel.setIsBUpdateGrandRank(true);
        }
    },

    /**
     * 获取新手引导的引导阶段
     * @param msg
     * @private
     */
    _resGuideHistory:function(msg){
        var resMsg = $root.CS_BeginnerGuide_Res.decode(msg);
        if(resMsg){
            var nIdLogin = resMsg.nIdLogin;
            var nGuideMarkRes = resMsg.nGuideMarkRes;
            if(nIdLogin == cf.PlayerMgr.getLocalPlayer().getIdLogin()) {
                cf.PlayerMgr.getLocalPlayer().setNoviceGuideMask(nGuideMarkRes);
            }
        }
    },
    /**
     * 获取兑换数据返回消息
     * @param msg
     * @private
     */
    _resExchangeInfo:function(msg){

        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var resMsg = $root.CH_AskExchangeInfoMsg_Res.decode(msg);
        if (resMsg) {
            if(resMsg.nRes == 0){
                playerModel.setExchangeInfo(resMsg.exchangeInfo);
            }
            else if(resMsg.nRes == 1){
                var arrSurplus = resMsg.strSurplus.split("|");
                var exchangeInfo =  playerModel.getExchangeInfo();
                for(var i in exchangeInfo){
                    for(var j in arrSurplus){
                        var arrSurp = arrSurplus[j].split(",");
                        if(exchangeInfo[i].id == arrSurp[0]){
                            exchangeInfo[i].nRemaNum = arrSurp[1]
                        }
                    }
                }
                playerModel.setExchangeInfo(exchangeInfo);
            }
            playerModel.setUpdateExchangeTime(resMsg.nUpdateTime);

            cf.dispatchEvent(LobbyLayer.DoEvent,  LobbyLayer.EventFlag.NEW_OPEN_EXCHANGE);
        }
        else{
            cc.log("CH_AskExchangeInfoMsg_Res 客户端请求兑换奖品数据失败");
        }
    },
    /**
     * 服务器主动发给客户端更新兑换余量消息
     * @param msg
     * @private
     */
    _resNewExchanInvenData:function(msg){

        var resMsg = $root.HC_NewExchanInvenData.decode(msg);
        if(resMsg){
            var playerModel = cf.PlayerMgr.getLocalPlayer();
            var exchangeInfo =  playerModel.getExchangeInfo();
            for(var i in exchangeInfo){
                    if(exchangeInfo[i].id == resMsg.nID){
                        exchangeInfo[i].nRemaNum = resMsg.nUpNum
                    }
                }
            }
        playerModel.setExchangeInfo(exchangeInfo);

        cf.dispatchEvent(LobbyLayer.DoEvent,  LobbyLayer.EventFlag.UPDATE_EXCHANGE);
    },
    /**
     * 通知客户端更新兑换奖品数据
     * @param msg
     * @private
     */
    _resUpdateExchange:function(msg){

        var resMsg = $root.HC_UpdateExchangeMsg.decode(msg);

        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var updateTime = playerModel.getUpdateExchangeTime();
        var msg = new $root.CH_AskExchangeInfoMsg();
        msg.nUpdateTime = updateTime ;
        var wr = $root.CH_AskExchangeInfoMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_AskExchangeInfoMsg, wr);
    },

    /**
     * CH_IsNewPlayerMsg 判断玩家是否为新玩家 从而决定开启点劵抽奖
     */
    _resIsNewPlayer: function (msg) {
        var resMsg = $root.CH_IsNewPlayerMsg.decode(msg);
        if (resMsg) {
            var localPlayer = cf.PlayerMgr.getLocalPlayer();
            //摇奖点券数
            var nCouponsNum = resMsg.nCouponsNum;//resMsg.nLotteryBase;

            var multipleNum = resMsg.nSignTimes;
            localPlayer.setMultipleLotteryNum(multipleNum)
            localPlayer.setLotteryGetSign(nCouponsNum);
            //是否为新玩家  0为新玩家，1为老玩家， 老玩家的话功能窗口不显示
            var nIsNewPlayer = resMsg.nIsNewPlayer;
            cc.log("摇奖兑换券数为" + nCouponsNum + "开始状态为" + nIsNewPlayer);
            localPlayer.setIsOpenLotteryGet(nIsNewPlayer == 0 ? true : false);
            localPlayer.setHasDrawLottery(nCouponsNum > 0 ? true : false);
        }
    },

    /**
     * 排序 除重
     * @param jsonData
     * @returns {*}
     * @private
     */
    _delectRedundant:function(jsonData){
        //活动列表排序
        var compare = function (obj1, obj2) {
            var val1 = parseInt(obj1.nSort);
            var val2 = parseInt(obj2.nSort);
            if (val1 > val2) {
                return -1;
            } else if (val1 < val2) {
                return 1;
            } else {
                return 0;
            }
        };

        jsonData.sort(compare);

        cc.log(jsonData);
        //除重(去掉同一时间的内出现版本的相同活动 去掉版本低)
        var loop = jsonData.length;
        for (var i =0; i < loop;i ++) {
            for(var j = i + 1; j < loop; j++){
                if(jsonData[j].nType == jsonData[i].nType) {
                    if (jsonData[j].nVersion > jsonData[i].nVersion) {
                        jsonData.splice(i, 1);
                        j = j - 1;    //改变循环变量
                        loop = loop - 1;   //改变循环次数
                    }
                }
            }
        }
        //除重(去掉同一时间的内出现活动ID的相同活动 去掉活动ID小)
        var loop01 = jsonData.length;
        for (var i =0; i < loop01;i ++) {
            for(var j = i + 1; j < loop01; j++){
                if(jsonData[j].nType == jsonData[i].nType) {
                    if (jsonData[j].nActID >= jsonData[i].nActID) {
                        jsonData.splice(i, 1);
                        j = j - 1;    //改变循环变量
                        loop01 = loop01 - 1;   //改变循环次数
                    }
                }
            }
        }
        //移除争霸赛显示
        /*var loop01 = jsonData.length;
        for (var k =0; k < loop01;k ++) {
            if (jsonData[k].nType  == cf.ACTTagFlag.Champions) {
                jsonData.splice(k,1);
                k = k - 1;    //改变循环变量
                loop01 = loop01 - 1;   //改变循环次数
            }
        }*/
        cc.log(jsonData);
        return jsonData;
    },
    /**
     * 刷新转盘记录
     * @param msg
     * @private
     */
    _resRefreshLuckDraw:function(msg){
        var resMsg = $root.HC_RefreshLuckDraw.decode(msg);
        if(resMsg){

            var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
            localPlayerModel.setRouletteRecord(resMsg.strLuckDraw);
            //刷新大厅小红点
            cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UI_ACTIVIYT_RED);
        }
    }
});

NotificationScene.eventName = "event.LocalError";

NotificationScene.Error = {
    ERROR_NET_CONNECT: 100,         //< 网络连接错误
    ERROR_SOCKET_CLOSE: 101,        //< socket关闭
    ERROR_SOCKET_TIMEOUT: 102,      //< 网络超时
    ERROR_REMOVE_USER:103,          //< 玩家长时间未操作，移除玩家

    PAY_SUCCESS: 200,               //< 支付成功消息
    ERROR_MAX: 999
};