var BaseScene = cc.Scene.extend({
    onEnter: function () {
        this._super();
        //添加错误节点
        cf.ErrorMgr.setTarget(this);
        
    },

    onEnterTransitionDidFinish: function () {
        this._super();
    },

    onExitTransitionDidStart: function () {
        this._super();

        //清理资源
        this.onClear();
    },

    update: function (dt) {

    },

    onExit: function () {
        this._super();
    },


    /**
     * 清理资源
     * */
    onClear: function () {
    }
});