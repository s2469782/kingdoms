var BaseLayer = cc.Layer.extend({
    _touchListener: null,
    _touchEnabled: false,

    ctor: function () {
        this._super();
        
    },

    onEnter: function () {
        this._super();
    },

    onEnterTransitionDidFinish: function () {
        this._super();
        this.addListener();
        if(!cc.sys.isNative){
            cc.eventManager.frameUpdateListeners();
        }
    },

    onExitTransitionDidStart: function () {
        this._super();

        //清理资源
        this.onClear();
        this.removeListener();
    },

    onExit: function () {
        this._super();
    },

    /**
     * 清理资源
     */
    onClear: function () {
    },

    /**
     * 添加监听
     */
    addListener: function () {

    },

    /**
     * 移除监听
     */
    removeListener: function () {
    },

    /**
     * 是否添加触摸事件
     * @remark 单点触控
     * @param enable
     */
    setTouchEnabled: function (enable) {
        if (this._touchEnabled === enable)
            return;

        this._touchEnabled = enable;
        if (this._touchEnabled) {
            if(!this._touchListener) {
                this._touchListener = cc.EventListener.create({
                    event: cc.EventListener.TOUCH_ONE_BY_ONE,
                    swallowTouches: true,
                    onTouchBegan: this.onTouchBegan.bind(this),
                    onTouchMoved: this.onTouchMoved.bind(this),
                    onTouchEnded: this.onTouchEnded.bind(this)
                });
                cc.eventManager.addListener(this._touchListener, this);
            }
        } else {
            cc.eventManager.removeListener(this._touchListener);
            this._touchListener = null;
        }
    },

    /**
     * 获取触摸是否开启
     * @returns {boolean}
     */
    isTouchEnabled:function () {
        return this._touchEnabled;
    },

    onTouchBegan: function (touch, event) {
        return true;
    },
    onTouchMoved: function (touch, event) {
    },
    onTouchEnded: function (touch, event) {
    },

    /**
     * 显示
     */
    show:function () {
        this.setVisible(true);
        this.setTouchEnabled(true);
    },

    /**
     * 隐藏
     */
    hide:function () {
        this.setVisible(false);
        this.setTouchEnabled(false); 
    }
});