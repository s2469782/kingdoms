var BaseLoader = BaseLayer.extend({
    _className: "BaseLoader",
    _resource: null,
    _target: null,
    _cb: null,
    _spBk: null,
    _spLoadBk: null,
    _spLoadBar: null,
    _nTotalCount: 0,             //总计数
    _nResTotalCount: 0,          //资源实际总计数
    _nCurCount: 0,               //当前计数
    _nLoaderCount: 100,          //加载计数长度
    _fPreStepRate: 0.0,
    _fLoadRate: 0.0,
    _bLoaderFile: false,         //< 文件加载标记
    _bLoaderFinish: false,
    _label: null,
    _strLabel: "",

    RAND_LOADER_MAX: 3,
    RAND_LOADER_ARRAY: [],

    /**
     * 初始化
     * @returns {boolean}
     */
    init: function () {
        var self = this;
        self._nLoaderCount = 100;

        cc.log("BaseLoader:init");

        if (cc.sys.isNative) {
            this.RAND_LOADER_MAX = 8;
            this.RAND_LOADER_ARRAY = ["#fishLibrary_007.png","#fishLibrary_005.png","#fishLibrary_008.png",
                "#fishLibrary_011.png","#fishLibrary_013.png","#fishLibrary_014.png","#fishLibrary_015.png",
                "#fishLibrary_020.png"
            ]
        }
        //初始化ui
        self._initUI();
        return true;
    },

    /**
     * 初始化ui
     * @private
     */
    _initUI: function () {
        var self = this;
        var winSize = cc.winSize;

        //创建背景
        // var spBk = self._spBk = new cc.LayerColor(cc.color(0, 0, 0, 255));
        // self.addChild(spBk);
        var spBk = new cc.Sprite(Res.LoadingImage);
        spBk.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        self.addChild(spBk);

        //创建精灵
        if (cf.RAND_LOADER >= this.RAND_LOADER_MAX) {
            cf.RAND_LOADER = 0;
        }

        if (cc.sys.isNative) {
            var logo = new cc.Sprite(this.RAND_LOADER_ARRAY[cf.RAND_LOADER]);
            logo.setPosition(winSize.width / 2, winSize.height / 2 + 60);
            logo.setScale(1.4);
            self.addChild(logo, 10);
            logo.runAction(cc.repeatForever(cc.jumpBy(2, 0, 0, 10, 1)));
        } else {
            if (cc._loaderImage[cf.RAND_LOADER]) {
                //loading logo
                
                // var num = 0;
                // var ran = Math.floor(Math.random() * 2) +1;
               
                // var brust = new cc.Sprite("#load"+ran+"_001.png");
                // brust.setAnchorPoint(0.5, 1);
                // //brust.setScale(2);
                // self.addChild(brust);

                // if(ran == 1){
                //     num = 9;
                //     brust.setPosition(cc.winSize.width/2, cc.winSize.height/2+240);
                // }else if(ran == 2){
                //     num = 11;
                //     brust.setPosition(cc.winSize.width/2, cc.winSize.height/2+220);
                // }else{
                //     num = 11;
                //     brust.setPosition(cc.winSize.width/2, cc.winSize.height/2+250);
                // }
               
                
                // var animFrames = [];
                // var str = "";
                // for (var i = 1; i < num; i++) {
                //     if(i < 10){
                //         str = "load"+ran+"_00" + i +".png" ;
                //     }else{
                        
                //         str = "load"+ran+"_0" + i +".png" ;
                //     }
                    
                //     var frame = cc.spriteFrameCache.getSpriteFrame(str);
                //     animFrames.push(frame);
                // }
                // var animation = new cc.Animation(animFrames, 1/15);
                // brust.runAction(cc.animate(animation).repeatForever());
                
            }
        }
        cf.RAND_LOADER++;

        //创建loading背景
        var spLoadBk = self._spLoadBk = new cc.Sprite("#resource_009.png");
        spLoadBk.setPosition(winSize.width / 2, winSize.height / 2 - 300);
        self.addChild(spLoadBk);

        var loadBkSize = spLoadBk.getContentSize();

        //创建loading条
        var spLoadBar = self._spLoadBar = new cc.ProgressTimer(new cc.Sprite("#resource_008.png"));
        spLoadBar.type = cc.ProgressTimer.TYPE_BAR;
        spLoadBar.midPoint = cc.p(0, 0);
        spLoadBar.barChangeRate = cc.p(1, 0);
        spLoadBar.setPosition(loadBkSize.width / 2, loadBkSize.height / 2);
        spLoadBk.addChild(spLoadBar);

        var hint = ["击破将领，有机会击破军营，击破七个便可得到宝物库......", "击破将领，有机会让友军士气上升，三次便可施放华丽友军无双......", "第二次集满友军无双时，便可施放最强的群体无双技......", "击破七个军营，便有机会找到传国玉玺，获得大彩金......"];
        var random = Math.floor(Math.random()*4);
        //self._strLabel = cf.Language.getText("text_1100");
        self._strLabel = hint[random];
        //var label = self._label = new cc.LabelTTF(self._strLabel + "0%", cf.Language.FontName, 20);
        var label = self._label = new cc.LabelTTF(self._strLabel + "0%", "MingLiU", 25);
        label.lineWidth = 0.8;
        label.lineHeight = 1.2;
        label.setPosition(spLoadBk.width / 2, spLoadBk.height + 40);
        spLoadBk.addChild(this._label, 10);
    },

    /**
     * 初始化资源配置
     * @param resources
     * @param cb
     * @param target
     */
    initWithResources: function (resources, cb, target) {
        var self = this;
        if (cc.isString(resources))
            resources = [resources];
        self._resources = resources || [];
        self._cb = cb;
        self._target = target;

        //初始化数据
        self._nCurCount = 0;
        self._fLoadRate = 0;
        self._nResTotalCount = self._resources.length;
        self._nTotalCount = self._nResTotalCount + self._nLoaderCount;
        self._fPreStepRate = 100.0 / self._nTotalCount;
        cc.log("初始化完成");
    },

    /**
     * 加载逻辑
     */
    startLoader: function () {
        var self = this;
        var res = self._resources;

        self._bLoaderFile = false;
        self._bLoaderFinish = false;
        cc.log("startLoader")
        //加载资源
        if (res != null) {
            cc.loader.load(res,
                function (result, count, loadedCount) {

                    if (self._spLoadBar) {
                        self._spLoadBar.setPercentage(self._fLoadRate);
                        var per = Math.floor(self._nCurCount / self._nTotalCount * 100);
                        self._label.setString(self._strLabel + per + "%");
                    }
                    self._fLoadRate += self._fPreStepRate;
                    self._nCurCount++;
                }, function () {
                    //文件加载结束
                    self._bLoaderFile = true;

                });
        }

    },

    /**
     * 进度条逻辑
     * @param dt
     */
    updateLoader: function (dt) {
        var self = this;

        if (self._nCurCount <= self._nTotalCount) {
            if (!self._bLoaderFile) {

            } else {
                //实现加载具体逻辑
                self.prepareLoader();

                //更新loading条
                if (self._spLoadBar) {
                    self._spLoadBar.setPercentage(self._fLoadRate);
                    var per = Math.floor(self._nCurCount / self._nTotalCount * 100);
                    per = Math.min(per, 100);
                    self._label.setString(self._strLabel + per + "%");
                }
                self._fLoadRate += self._fPreStepRate;
                self._nCurCount++;
            }

        } else {
            if (self._bLoaderFile) {
                //加载结束
                self.unschedule(self.updateLoader);
                self._nCurCount = 0;
                self._nTotalCount = 0;
                self._fLoadRate = 0;
                self._fPreStepRate = 0;
                self._bLoaderFile = false;
                self._bLoaderFinish = true;

                self.finishLoader();

                //回调函数
                if (self._cb) {
                    self._cb.call(self._target);
                }
            }
        }
    },

    onEnter: function () {
        this._super();

        var self = this;
        //启动loading逻辑
        self.startLoader();
        self.schedule(self.updateLoader);
    },

    onExit: function () {
        this._super();

        var self = this;
        if (self._spLoadBar) {
            self._spLoadBar.setPercentage(0);
        }
    },

    /**
     * 释放资源
     */
    onClear: function () {
        this._super();
    },

    /**
     * 资源加载逻辑
     * loader只实现文件的加载，具体的数据初始化在这里实现
     */
    prepareLoader: function () {

    },

    finishLoader: function () {

    },

    /**
     * 设置数据长度计数
     * @param count
     */
    setLoaderCount: function (count) {
        this._nLoaderCount = count;
    }
});
