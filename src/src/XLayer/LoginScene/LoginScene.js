var $protobuf = protobuf;
var LoginScene = BaseScene.extend({
    _bQuit: false,
    _onEventListener: null,

    onEnter: function () {
        this._super();
        cc.log("LoginScene:init");
        cc.loader.load(Res.LoginImage);
        var self = this;
        var winSize = cc.winSize;

        SDKHelper.setTarget(self);
        //初始化数据
        self._initData();
        self._addListener();

        //创建背景
        // var animateLayer = new AnimateLayer();
        // self.addChild(animateLayer, -100);
        
        var spBk = new cc.Sprite(Res.LoginImage);
        spBk.x = winSize.width / 2;
        spBk.y = winSize.height / 2;
        self.addChild(spBk, -10);

        //版本号显示
        var strVer = "v" + GameConfig.verName;
        if (GameConfig.serverIdx == 1) {
            strVer += " Beta";
        }
        var labelVer = new ccui.Text(strVer, cf.Language.FontName, 14);
        labelVer.setAnchorPoint(0, 1);
        labelVer.setPosition(0, winSize.height);
        labelVer.setOpacity(180);
        self.addChild(labelVer);

        

        //播放音乐
        cf.SoundMgr.initData();
        cf.SoundMgr.stopMusic();
        cf.SoundMgr.playMusic(1, true);

        //H5直接启动登录
        if (!cc.sys.isNative) {
            this.runLayer(LoginScene.Flag.LOGIN);
        }
    },

    onEnterTransitionDidFinish: function () {
        this._super();
        if (cc.sys.isNative) {
            if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.UPDATE) {
                this.runLayer(LoginScene.Flag.UPDATE)
            } else {
                this.runLayer(LoginScene.Flag.LOGIN)
            }
        }
    },

    onClear: function () {
        cc.log("LoginScene:onClear");
        cf.removeListener(this._onEventListener);
    },

    _initData: function () {
        
        //初始化通用UI
        ActionMgr.load(Res.LOGIN.commonPlist, Res.LOGIN.commonPng);
        ActionMgr.load(Res.LOGIN.newPlist, Res.LOGIN.newPng);
        ActionMgr.load(Res.LOGIN.fishLibPlist, Res.LOGIN.fishLibPng);
        ActionMgr.load(Res.LOGIN.loginPlist, Res.LOGIN.loginPng);
        ActionMgr.load(Res.LOGIN.resourcePlist, Res.LOGIN.resourcePng);
        ActionMgr.load(Res.LOGIN.loadPlist, Res.LOGIN.loadPng);
        ActionMgr.load(Res.LOGIN.scale9Plist, Res.LOGIN.scale9Png);
        DataMgr.readAnimateData(Res.LOGIN.AnimateDataJson);
        DataMgr.readCannonLevelData(Res.LOGIN.CannonLevelDataJson);
        //初始化通用UI

        cf.setDefaultTextureFormat(cc.Texture2D.PIXEL_FORMAT_RGB565);
        cc.loader.load(Res.BKImage);
        cf.setDefaultTextureFormat(cc.Texture2D.PIXEL_FORMAT_RGBA4444);
        if (!cf.bFirstInit) {
            cf.bFirstInit = true;
            //初始化多语言配置
            cf.Language.init(Res.LOGIN.LanguageDataJson);
            //初始化网络管理器
            cf.NetMgr.init();
        }

        //初始化一些参数
        cf.KeepAliveStartTime = 0;
        cf.NetDelayTime = 0;
        cf.bKeepAliveRes = false;
        cf.RemoveUserTime = 0;
        cf.bRemoveUser = false;
    },

    _addListener: function () {
        this._super();
        cc.log("LoginScene:addListener");
        var self = this;
        //按键监听
        if ('keyboard' in cc.sys.capabilities) {
            cc.eventManager.addListener({
                event: cc.EventListener.KEYBOARD,
                onKeyPressed: function (key, event) {
                    switch (key) {
                        case cc.KEY.back:
                            self._quitGame();
                            break;
                    }
                },
                onKeyReleased: function (key, event) {

                }
            }, this);
        } else {
            cc.log("KEYBOARD Not supported");
        }

        if (self._onEventListener == null) {
            self._onEventListener = cc.EventListener.create({
                event: cc.EventListener.CUSTOM,
                eventName: LoginScene.EventName,
                callback: function (event) {
                    self._doEvent(event.getUserData());
                }
            });
            cc.eventManager.addListener(self._onEventListener, 1);
        }
    },

    runLayer: function (flag) {
        switch (flag) {
            case LoginScene.Flag.LOGIN://登录相关               
                this.removeChildByTag(LoginScene.Flag.UPDATE);

                var loginLayer = new LoginLayer();
                loginLayer.setTag(LoginScene.Flag.LOGIN);
                this.addChild(loginLayer);
                break;
            case LoginScene.Flag.UPDATE://自动更新
                var updateLayer = new UpdateLayer();
                updateLayer.setTag(LoginScene.Flag.UPDATE);
                this.addChild(updateLayer);
                break;
        }
    },

    /**
     * 退出游戏
     * @private
     */
    _quitGame: function () {
        var self = this;
        if (self._bQuit) {
            return;
        }
        self._bQuit = true;
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
            cf.JsbHelper.doEvent(cf.EventMessage.EventId_Exit, "");

            //有些渠道需要特殊处理下，因为sdk没有提供退出取消的功能
            //这种情况下直接重置退出开关
            if (GameConfig.channelName == "OPPO" || GameConfig.channelName == "VIVO_CS" || GameConfig.channelName == "XiaoMi") {
                self._bQuit = false;
            }
        }
    },

    _doEvent: function (msg) {
        var self = this;
        var flag = msg;
        if (typeof msg == "object") {
            flag = msg.flag;
        }
        switch (flag) {
            case LoginScene.Event.QUIT:
                switch (msg.data) {
                    case 0://弹出对话框退出
                        var prompt = UIDialog.create("text_1269", "text_1224", "text_1225", true, function (type) {
                            if (type == UIDialog.Btn.RIGHT) {
                                cc.director.end();
                            }
                            self._bQuit = false;
                        }, self);
                        self.addChild(prompt);
                        break;
                    case 1://直接退出
                        cc.director.end();
                        break;
                    case 2://重置
                        self._bQuit = false;
                        break;
                }
                break;
        }
    }

});

LoginScene.Flag = {
    UPDATE: 0,
    LOGIN: 1
};

LoginScene.EventName = "LoginScene.EventName";
LoginScene.Event = {
    QUIT: 0
};
