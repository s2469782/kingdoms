//失败次数
var __failCount = 0;
//最大失败次数
var __maxFailCount = 30;

var UpdateLayer = BaseLayer.extend({
    _am: null,
    _percent: null,
    _spLoaderBk: null,
    _spLoaderBar: null,
    _labelLoader: null,
    _labelText: null,
    _totalSize: null,
    _checkListener: null,
    _updateListener: null,
    _btnUpdate: null,

    onClear: function () {
		this._super();
        cf.removeListener(this._checkListener);
        cf.removeListener(this._updateListener);
    },

    checkCb: function (event) {
        var bNewVersion = false;
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                cc.error("未找到本地manifest文件");
                this.loadGame();
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                cc.error("下载manifest文件错误");
                this.loadGame();
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                cc.error("已经是最新版本");
                this._labelText.setString(cf.Language.getText("text_1263"));
                this._percent = 100;
                this.loadGame();
                break;
            case jsb.EventAssetsManager.NEW_VERSION_FOUND:
                cc.error("发现最新版本");
                this._labelText.setString(cf.Language.getText("text_1264"));
                bNewVersion = true;
                break;
            default:
                return;
        }

        cc.eventManager.removeListener(this._checkListener);
        this._checkListener = null;
        this._updating = false;

        if(bNewVersion){
            this._labelLoader.setVisible(true);
            this._labelText.setString(cf.Language.getText("text_1271"));
            this.hotUpdate();
        }
    },

    updateCb: function (event) {
        var needRestart = false;
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                cc.error("未找到本地manifest文件");
                this.loadGame();
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                cc.error("下载manifest文件错误");
                this.loadGame();
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                cc.error("已经是最新版本");
                this._spLoaderBk.setVisible(false);
                this._labelText.setString(cf.Language.getText("text_1263"));
                this._percent = 100;
                this.loadGame();
                break;
            case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                var assetsId = event.getAssetId();
                if (assetsId == "@manifest" || assetsId == "@version") {

                } else {
                    this._percent = event.getPercent();
                    if (this._spLoaderBk) {
                        this._spLoaderBk.setVisible(true);
                        this._spLoaderBar.setPercentage(this._percent);

                        var totalSize = event.getTotalSize();
                        if (totalSize) {
                            totalSize = totalSize / (1024 * 1024);
                            this._totalSize = totalSize.toFixed(2);
                            var curSize = this._percent * totalSize / 100;
                            this._labelLoader.setString(curSize.toFixed(2) + "MB/" + this._totalSize + "MB");
                        }
                    }
                }
                break;
            case jsb.EventAssetsManager.UPDATE_FINISHED:
                cc.error("更新完成:" + event.getMessage());

                this._labelText.setString(cf.Language.getText("text_1263"));
                this._labelLoader.setVisible(false);
                this._spLoaderBar.setPercentage(100);
                needRestart = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FAILED:
                cc.error("更新失败:" + event.getMessage());

                __failCount++;
                if (__failCount < __maxFailCount) {
                    cc.error("尝试重新下载...");
                    this._am.downloadFailedAssets();
                }
                else {
                    cc.error("超过最大重试次数，退出更新");
                    __failCount = 0;
                    this.showError();
                }
                break;
            case jsb.EventAssetsManager.ERROR_UPDATING:
                cc.error("更新错误: " + event.getAssetId() + ", " + event.getMessage());
                //this.showError();
                break;
            case jsb.EventAssetsManager.ERROR_DECOMPRESS:
                cc.error("错误:" + event.getMessage());
                //this.showError();
                break;
            default:
                break;
        }

        if (needRestart) {
            cc.eventManager.removeListener(this._updateListener);
            this._updateListener = null;

            var searchPaths = jsb.fileUtils.getSearchPaths();
            var newPaths = this._am.getLocalManifest().getSearchPaths();
            Array.prototype.unshift(searchPaths, newPaths);
            cc.sys.localStorage.setItem('HotUpdateSearchPaths', JSON.stringify(searchPaths));
            jsb.fileUtils.setSearchPaths(searchPaths);

            this._am.release();
            //重启游戏
            cc.game.restart();
        }
    },

    ctor: function () {
        this._super();
        var self = this;
        var manifestPath = GameConfig.resPath + "project.manifest";
        var storagePath = (jsb.fileUtils ? jsb.fileUtils.getWritablePath() : "/") + "update_res";

        self._percent = 0;
        self._totalSize = 0;
        
        var winSize = cc.winSize;
        
        self._labelText = new ccui.Text(cf.Language.getText("text_1267"), cf.Language.FontName, 22);
        self._labelText.setPosition(winSize.width / 2, 135);
        self.addChild(self._labelText);

        //创建loading背景
        var spLoadBk = self._spLoaderBk = new cc.Sprite("#resource_009.png");
        spLoadBk.setAnchorPoint(0.5, 0);
        spLoadBk.setPosition(winSize.width / 2, 70);
        self.addChild(spLoadBk);

        //创建loading条
        self._spLoaderBar = new cc.ProgressTimer(new cc.Sprite("#resource_008.png"));
        self._spLoaderBar.type = cc.ProgressTimer.TYPE_BAR;
        self._spLoaderBar.midPoint = cc.p(0, 0);
        self._spLoaderBar.barChangeRate = cc.p(1, 0);
        self._spLoaderBar.setPosition(spLoadBk.width / 2, spLoadBk.height / 2);
        self._spLoaderBar.setPercentage(100);
        spLoadBk.addChild(self._spLoaderBar);

        //百分比
        self._labelLoader = new ccui.Text("0MB/0MB", cf.Language.FontName, 22);
        self._labelLoader.setPosition(spLoadBk.width / 2, spLoadBk.height/2);
        self._labelLoader.setVisible(false);
        spLoadBk.addChild(self._labelLoader, 10);

        //初始化更新资源管理器
        this._am = new jsb.AssetsManager(manifestPath, storagePath);
        this._am.retain();

        this.checkUpdate();
    },

    checkUpdate: function () {
        if (this._updating) {
            return;
        }

        if (!this._am.getLocalManifest() || !this._am.getLocalManifest().isLoaded()) {
            cc.error("更新失败，直接进入游戏");
            cc.director.getRunningScene().runLayer(LoginScene.Flag.LOGIN);
            return false;
        }
        this._checkListener = new jsb.EventListenerAssetsManager(this._am, this.checkCb.bind(this));
        cc.eventManager.addListener(this._checkListener, 1);

        this._am.checkUpdate();
        this._updating = true;

        return true;
    },

    hotUpdate: function () {
        if (this._am && !this._updating) {
            this._updateListener = new jsb.EventListenerAssetsManager(this._am, this.updateCb.bind(this));
            cc.eventManager.addListener(this._updateListener, 1);
            this._am.update();
            this._updating = true;
        }
    },

    loadGame: function () {
        var self = this;

        if (self._updateListener) {
            cc.eventManager.removeListener(self._updateListener);
            self._updateListener = null;
            self._updating = false;
        }

        this.runAction(cc.sequence(
            cc.delayTime(0.5),
            cc.callFunc(function () {
                self._am.release();
                cc.director.getRunningScene().runLayer(LoginScene.Flag.LOGIN);
            })
        ));
    },

    showError:function () {
        var prompt = UIDialog.create("text_1266", "text_1224", null, false, function (type) {
            if (type == UIDialog.Btn.RIGHT) {
                cc.director.end();
            }
        }, this);
        this.addChild(prompt);
    }
});
