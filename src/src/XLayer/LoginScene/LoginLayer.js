var LoginLayer = BaseLayer.extend({
    _onLoginListener: null,         //登录监听
    _nowTime: 0,
    _strEndTime: 0,
    ctor: function () {
        this._super();
        cc.log("LoginLayer:init");

        //清空心跳
        if(SDKHelper._keepAliveIntervalFunc){
            clearInterval(SDKHelper._keepAliveIntervalFunc);
        }

        var self = this;
        //屏幕大小
        var winSize = cc.winSize;

        // var logoSp = new cc.Sprite(Res.LOGIN.logoImage);
        // logoSp.setPosition(cc.winSize.width / 2, cc.winSize.height / 2+100);
        // self.addChild(logoSp);

        //快速开始
        var quickBtn = new ccui.Button();
        quickBtn.loadTextureNormal("button_012.png", ccui.Widget.PLIST_TEXTURE);
        quickBtn.setPosition(winSize.width / 2, 170);
        quickBtn.setTag(LoginLayer.Type.quickBtn);
        quickBtn.setTouchEnabled(false);
        quickBtn.addClickEventListener(function () {
            var targetEle = document.documentElement;
            
            //登录
            SDKHelper.login(self, 0);

            
            
            
             //全螢幕
             var onFullScreenCallBack = ()=>{
                if(cc.sys.os == cc.sys.OS_ANDROID){
                    window.screen.orientation.addEventListener('change', testttt);
                    window.screen.orientation.lock("portrait");
                }

            };
            var userAgentInfo = navigator.userAgent;
            var Agents = ["iPhone", "iPad", "iPod"];

            var agent = ()=>{
                for (var v = 0; v < Agents.length; v++)
            {
                if (userAgentInfo.indexOf(Agents[v]) > -1)
                {
                    return false;
                }
            }
            return true;
            }

                if(cc.sys.os == cc.sys.OS_ANDROID && agent== true){
                    cf.enableNoSleep();
                    if(!cc.screen.fullScreen()){

                        cc.screen.requestFullScreen(targetEle, onFullScreenCallBack);
                                    
                    }

                }
                
            
           
        });
        self.addChild(quickBtn, 1);

        cf.UITools.showLoadingToast(cf.Language.getText("text_1012"), 25, false, self);
        self._AskSeverState();
    },
    
    onClear: function () {
        this._super();
        cc.log("LoginLayer:onClear");
    },

    /**
     * 添加监听
     * @remark 此方法会自动调用，只继承实现具体逻辑
     */
    addListener: function () {
        this._super();
        cc.log("LoginLayer:addListener");

        var self = this;
        //创建登录监听
        self._onLoginListener = cf.addNetListener(MessageCode.CH_LoginMsg_Res, self._resLogin.bind(this));
    },

    /**
     * 移除监听
     * @remark 此方法会自动调用，只继承实现具体逻辑
     */
    removeListener: function () {
        this._super();
        cc.log("LoginLayer:removeListener");
        cf.removeListener(this._onLoginListener);
    },

    /**
     * 登录处理
     * @param msg
     * @private
     */
    _resLogin: function (msg) {
        cf.UITools.hideLoadingToast();
        var resMsg = $root.CH_LoginMsg_Res.decode(msg);
        console.log(resMsg);
        switch (resMsg.nRes) {
            case 0://登录成功
                cc.log("登录成功, loginId = " + resMsg.nidLogin);
                cc.log("未获取邮件数量：" + resMsg.strEmailData);

                //充值补单开关
                cf.AskOrder = true;
                cf.AskOrderTip = false;

                //保存用户id和密码
                if (cf.SaveDataMgr.getNativeData("idLogin") == null || cf.SaveDataMgr.getNativeData("password") == null) {
                    cf.SaveDataMgr.setNativeData("idLogin", resMsg.nidLogin);
                    cf.SaveDataMgr.setNativeData("password", resMsg.strAutoPassword);
                }

                //初始化玩家数据
                cf.PlayerMgr.createLocalPlayer(resMsg);
                this.checkActivityIsEnd();

                // 初始化邮件数量
                var nEmailNum = cf.SaveDataMgr.getNativeData(cf.EmailNoReadNum);
                var EmailNumArr = resMsg.strEmailData.split(",");
                if (nEmailNum == null || nEmailNum == "") {
                    cf.SaveDataMgr.setNativeData(cf.EmailNoReadNum, EmailNumArr[0]);
                }
                else {
                    if (EmailNumArr[0] != 0) {
                        cf.SaveDataMgr.setNativeData(cf.EmailNoReadNum, EmailNumArr[0]);
                    }
                }

                //请求活动信息
                this._askActivityData();
                
                //获取断线重连标记
                cf.reConnect.bReturn = resMsg.bReturn;
                if (cf.reConnect.bReturn) {
                    cf.reConnect.roomID = resMsg.nRoomType;
                }
                cf.SceneMgr.runScene(cf.SceneFlag.LOBBY);

                break;
            case 1://密码错误
                cc.log("密码错误");
                break;
            case 2://封号
                cc.log("封号");

                //检测封号日期
                break;
        }
    },

    /**
     * 获取服务器状态
     * @private
     */
    _AskSeverState: function () {
        var self = this;

        var nChannelID = GameConfig.channelId;
        var version = [];
        var msgList1 = JSON.parse(cf.SaveDataMgr.getNativeData("msgList1"));
        var msgList2 = JSON.parse(cf.SaveDataMgr.getNativeData("msgList2"));
        var msgList3 = JSON.parse(cf.SaveDataMgr.getNativeData("msgList3"));
        var tempNowTime = (new Date().getTime());
        if (msgList1 && msgList1.length > 0) {
            var tempStrEndTime = cf.dateToTime(msgList1[0].strEndTime);
            if (tempNowTime < tempStrEndTime) {
                var localVersionInfo = {};
                localVersionInfo.id = msgList1[0].id;
                localVersionInfo.version = msgList1[0].nVersion;
                version.push(localVersionInfo);
            }
        }
        if (msgList2 && msgList2.length > 0) {
            for (var i in msgList2) {
                var tempEndTime = cf.dateToTime(msgList2[i].strEndTime);
                if (tempNowTime < tempEndTime) {
                    var localVersionInfo = {};
                    localVersionInfo.id = msgList2[i].id;
                    localVersionInfo.version = msgList2[i].nVersion;
                    version.push(localVersionInfo);
                }
            }
        }
        if (msgList3 && msgList3.length > 0) {
            var tempStrEndTime2 = cf.dateToTime(msgList3[0].strEndTime);
            if (tempNowTime < tempStrEndTime2) {
                var localVersionInfo = {};
                localVersionInfo.id = msgList3[0].id;
                localVersionInfo.version = msgList3[0].nVersion;
                version.push(localVersionInfo);
            }
        }
        version = JSON.stringify(version);
        cc.log("nChannelID = " + nChannelID + ", version = " + version);

        var callFunc = function (jsonData, xhr) {
          
            //解析json
            var res = jsonData.res;

            if (res == 0) {
                //正常
                self.setEnableLoginBtn(true);
                xhr.abort();

                var popupLayer = self.getChildByTag(10000);
                if (popupLayer) {
                    popupLayer.removeFromParent();
                }
            } else {
                var msgList1 = jsonData.msgList1;
                var msgList2 = jsonData.msgList2;
                var msgList3 = jsonData.msgList3;
                var localMsg1, localMsg2, localMsg3;
                localMsg1 = JSON.parse(cf.SaveDataMgr.getNativeData("msgList1"));
                localMsg2 = JSON.parse(cf.SaveDataMgr.getNativeData("msgList2"));
                localMsg3 = JSON.parse(cf.SaveDataMgr.getNativeData("msgList3"));
                if (msgList1 && msgList1.length == 0 && localMsg1 && localMsg1.length > 0) {
                    msgList1 = localMsg1;
                }
                if (msgList2 && msgList2.length == 0 && localMsg2 && localMsg2.length > 0) {
                    msgList2 = localMsg2;
                }
                if (msgList3 && msgList3.length == 0 && localMsg3 && localMsg3.length > 0) {
                    msgList3 = localMsg3;
                }
                if (msgList1 && msgList1.length == 0 && msgList2 && msgList2.length == 0 && msgList3 && msgList3.length == 0) {
                    self.setEnableLoginBtn(true);
                    return;
                }
                cc.log(JSON.stringify(jsonData.msgList1), JSON.stringify(jsonData.msgList2), JSON.stringify(jsonData.msgList3) + "公告信息在此");
                //如果已经存对应的Id 则需要对已经存在的公告进行修改
                if (msgList1 && msgList1.length > 0) {
                    if (localMsg1 && localMsg1.length > 0) {
                        if (msgList1[0].id == localMsg1[0].id && (msgList1[0].nState == 1)) {
                            cf.SaveDataMgr.setNativeData("msgList1", JSON.stringify([]));
                            self.setEnableLoginBtn(true);
                            //重置msgList1
                            msgList1 = JSON.parse(cf.SaveDataMgr.getNativeData("msgList1"));
                        }
                    }
                }
                if (msgList1 && msgList1.length > 0) {
                    //停服公告
                    //本地记录停服公告信息
                    cf.SaveDataMgr.setNativeData("msgList1", JSON.stringify(msgList1));
                    var tempNowTime = (new Date().getTime());
                    var tempStrEndTime = cf.dateToTime(msgList1[0].strEndTime);

                    var announceMsg = {};
                    announceMsg.strTitle = msgList1[0].strTitle;
                    announceMsg.strContent = msgList1[0].strContent;
                    var popupAnnouncement;
                    popupAnnouncement = self.getChildByTag(10000);
                    if (popupAnnouncement) {
                        popupAnnouncement.initTextAnnouncement(announceMsg);
                    }
                    else {
                        var popupAnnouncement = new PopupAnnouncement(PopupAnnouncement.Enum.TEXT, announceMsg);
                        popupAnnouncement.setTag(10000);
                        self.addChild(popupAnnouncement, 101);
                    }

                    self.setEnableLoginBtn(false);
                    cc.log(tempNowTime + "------------------" + tempStrEndTime);
                    if (tempNowTime < tempStrEndTime) {
                        self._nowTime = parseInt(tempNowTime);
                        self.strEndTime = parseInt(tempStrEndTime);
                        self.scheduleUpdate();
                    }
                    else {
                        if (popupAnnouncement) {
                            popupAnnouncement.removeFromParent();
                        }
                        self.setEnableLoginBtn(true);
                        xhr.abort();

                        cf.SaveDataMgr.setNativeData("msgList1", JSON.stringify([]));
                        //重置msgList1
                        msgList1 = JSON.parse(cf.SaveDataMgr.getNativeData("msgList1"));
                        return;
                    }
                    if (msgList2 && msgList2.length > 0) {
                        cf.SaveDataMgr.setNativeData("msgList2", JSON.stringify(msgList2));
                    }
                    if (msgList3 && msgList3.length > 0) {
                        cf.SaveDataMgr.setNativeData("msgList3", JSON.stringify(msgList3));
                    }
                }
                else {
                    if (msgList2 && msgList2.length > 0) {
                        cf.SaveDataMgr.setNativeData("msgList2", JSON.stringify(msgList2));
                        self.setEnableLoginBtn(true);
                        xhr.abort();

                        for (var i in msgList2) {
                            if (localMsg2 && localMsg2.length > 0) {
                                for (var index in localMsg2) {
                                    if (msgList2[i].id == localMsg2[index].id && (msgList2[i].nState == 1)) {
                                        msgList2.splice(i, 1);
                                        cf.SaveDataMgr.setNativeData("msgList2", JSON.stringify(msgList2));
                                        continue;
                                    }
                                }
                            }
                            //游戏公告
                            var tempNowTime = (new Date().getTime());
                            var tempStrEndTime = cf.dateToTime(msgList2[i].strEndTime);

                            if (tempNowTime > tempStrEndTime) {
                                return;
                            }

                            var announceMsg = {};
                            announceMsg.strTitle = msgList2[i].strTitle;
                            announceMsg.strContent = msgList2[i].strContent;
                            cc.log(announceMsg.strContent);
                            var popupAnnouncement;
                            popupAnnouncement = self.getChildByTag(10000);
                            if (popupAnnouncement) {
                                popupAnnouncement.initTextAnnouncement(announceMsg);
                            }
                            else {
                                var popupAnnouncement = new PopupAnnouncement(PopupAnnouncement.Enum.TEXT, announceMsg);
                                popupAnnouncement.setTag(10000);
                                self.addChild(popupAnnouncement, 101);
                            }
                        }
                    }
                    if (msgList3 && msgList3.length > 0) {
                        //更新公告
                        cf.SaveDataMgr.setNativeData("msgList3", JSON.stringify(msgList3));
                        var announceMsg = {};
                        announceMsg.strTitle = msgList3[0].strTitle;
                        announceMsg.strContent = msgList3[0].strContent;
                        var popupAnnouncement;
                        popupAnnouncement = self.getChildByTag(10000);
                        if (popupAnnouncement) {
                            popupAnnouncement.initTextAnnouncement(announceMsg);
                        }
                        else {
                            var popupAnnouncement = new PopupAnnouncement(PopupAnnouncement.Enum.TEXT, announceMsg);
                            popupAnnouncement.setTag(10000);
                            self.addChild(popupAnnouncement, 101);
                        }
                        if (localMsg3 && localMsg3.length > 0) {
                            if (msgList3[0].id == localMsg3[0].id && (msgList3[0].nState == 1)) {
                                if (popupAnnouncement) {
                                    popupAnnouncement.removeFromParent();
                                    cf.SaveDataMgr.setNativeData("msgList3", JSON.stringify([]));
                                }
                            }
                        }
                    }
                }
            }
        };

        if (nChannelID == null) {
            nChannelID = 0;
        }
        if (version == null) {
            version = 0;
        }
        var data = "nChannelID=" + nChannelID + "&version=" + version;
        cc.log(data);

        //if (!cc.sys.isNative) {
        //发起http请求
        if(GameConfig.d == "test54667157"){
            
            callFunc({res : 0}, {abort:function(){}});
            cf.UITools.hideLoadingToast();
            self.setEnableLoginBtn(true);

        }else{
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.open("POST", cf.HTTP_SERVER_STATE_URL, true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.onreadystatechange = function () {
                cf.UITools.hideLoadingToast();
                if (xhr.readyState == 4) {
                    if (xhr.status >= 200 && xhr.status <= 207) {
                        if (xhr.responseText) {
                            cc.log("请求服务器状态成功");
                            //特殊处理下，因为h5是用ajax的jsonp请求的所以返回的字符串带括号，需要去掉括号
                            var str = xhr.responseText.replace(/[()]/ig, "");
                            var jsonData = JSON.parse(str);
                            callFunc(jsonData, xhr);
                            
                        }
                    } else {
                        cc.error("请求服务器状态失败");
                        //self.setEnableLoginBtn(true);
                    }
                    xhr.abort();
                }
            };
            xhr.ontimeout = function () {
                cc.error("请求服务器状态超时");
                cf.UITools.hideLoadingToast();
                //self.setEnableLoginBtn(true);
                xhr.abort();
            };
            xhr.timeout = 10000;
            xhr.send(data);
        }
        /*} else {
         $.ajax({
         url: "http://192.168.199.90/onlineServerState/getSign.php",
         dataType: 'jsonp',
         data: {nChannelID: nChannelID, version: version},
         jsonp: 'callback',
         success: function (jsonData) {
         cf.UITools.hideLoadingToast();
         callFunc(jsonData);
         },
         error:function (httpRequest, textStatus, errorThrown) {
         cc.error("请求服务器状态失败");
         cf.UITools.hideLoadingToast();
         if (btn) {
         btn.setTouchEnabled(true);
         btn.setColor(cc.color.WHITE);
         eff.setColor(cc.color.WHITE);
         }
         },
         timeout: 5000
         });
         }*/
    },

    update: function (dt) {
        this._super(dt);

        if (this._nowTime && this.strEndTime) {
            this._nowTime += dt * 1000;
            cc.log("_nowTime=" + this._nowTime);
            if (this._nowTime >= this.strEndTime) {
                cc.log("strEndTime=" + this.strEndTime);
                //获取服务器状态
                this._AskSeverState();
                this.unscheduleUpdate();
            }
        }
    },
    /**
     * 请求活动信息
     * @private
     */
    _askActivityData: function () {
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var strRuleVer = [];
        //先从本地获取活动信息
        var actInfoList = cf.SaveDataMgr.getNativeData("actInfoList");
        if (actInfoList == null || actInfoList == "") {

        } else {
            actInfoList = JSON.parse(actInfoList);
            for (var i = 0; i < actInfoList.length; ++i) {
                var strSelf = {};
                var actRuleInfoList = actInfoList[i].actRuleInfoList;
                var strRule = [];
                for (var j = 0; j < actRuleInfoList.length; j++) {
                    if (actRuleInfoList[j].nIndex && actRuleInfoList[j].updateTime && actRuleInfoList[j].updateTime != "") {
                        var str = actRuleInfoList[j].nIndex + "," + actRuleInfoList[j].updateTime;
                        strRule.push(str);
                    }
                }
                // strSelf.nActID = actInfoList[i].nActID;
                // strSelf.nVersion = actInfoList[i].nVersion;
                strSelf.nActID = [];
               strSelf.nVersion = [];
                strSelf.strRuleTime = strRule;
                strRuleVer.push(strSelf);
            }
        }
        cc.log(strRuleVer);
        var msg = new $root.CH_AskActivity();
        msg.nidLogin = playerModel.getIdLogin();
        msg.strSafeCode = playerModel.getSafeCode();
        msg.strRuleVer = strRuleVer;
        var wr = $root.CH_AskActivity.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_AskActivityMsg, wr);
    },
    /**
     * 检查过期活动数据
     */
    checkActivityIsEnd: function () {
        //获取信息存入本地数据
        var actInfoList = cf.SaveDataMgr.getNativeData("actInfoList");

        var playerModel = cf.PlayerMgr.getLocalPlayer();
        if (actInfoList != null && actInfoList != "") {
            //将字符串转化为json
            var jsonData = JSON.parse(actInfoList);
            var loop = jsonData.length;
            //删除过期数据
            for (var j = 0; j < loop; j++) {
                if (jsonData[j].nType != cf.ACTTagFlag.Roulette && jsonData[j].nType != cf.ACTTagFlag.Champions) {
                    //判断活动是否在显示期内
                    if (jsonData[j].strEndShowTime < playerModel.getGameTime()) {
                        jsonData.splice(j, 1);
                        j = j - 1;    //改变循环变量
                        loop = loop - 1;   //改变循环次数
                    }
                }
            }

            cf.SaveDataMgr.setNativeData("actInfoList", JSON.stringify(jsonData));
        }
    },

    /**
     * 设置登录按钮状态
     * @param bShow
     */
    setEnableLoginBtn: function (bShow) {
        if (GameConfig.channelName == "YYB") {
            //应用宝
            var qqBtn = this.getChildByTag(LoginLayer.Type.qqBtn);
            var wxBtn = this.getChildByTag(LoginLayer.Type.wxBtn);

            if (qqBtn && wxBtn) {
                if (bShow) {
                    qqBtn.setTouchEnabled(true);
                    qqBtn.setColor(cc.color.WHITE);
                    wxBtn.setTouchEnabled(true);
                    wxBtn.setColor(cc.color.WHITE);
                } else {
                    qqBtn.setTouchEnabled(false);
                    qqBtn.setColor(cc.color(105, 105, 105));
                    wxBtn.setTouchEnabled(false);
                    wxBtn.setColor(cc.color(105, 105, 105));
                }
            }

        } else {
            //通用渠道
            var btn = this.getChildByTag(LoginLayer.Type.quickBtn);
            if (btn) {
                if (bShow) {
                    btn.setTouchEnabled(true);
                    btn.setColor(cc.color.WHITE);
                } else {
                    btn.setTouchEnabled(false);
                    btn.setColor(cc.color(105, 105, 105));
                    btn.setColor(cc.color(105, 105, 105));
                    
                }
            }
        }
    }
});

LoginLayer.Type = {
    quickBtn: 0,
    quickEff: 1,
    qqBtn: 2,
    wxBtn: 3
};
var testttt = ()=>{

   
    window.screen.orientation.lock("landscape");
}