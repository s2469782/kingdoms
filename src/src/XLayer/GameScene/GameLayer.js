var GameLayer = BaseLayer.extend({
    ctor: function () {
        this._super();
        cc.log("GameLayer:init");

        //注册touch事件
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ALL_AT_ONCE,
            swallowTouches: true,
            onTouchesBegan: this.onTouchesBegan,
            onTouchesMoved: this.onTouchesMoved,
            onTouchesEnded: this.onTouchesEnded,
            onTouchesCancelled: this.onTouchesCancelled
        }, this);

        //初始化房间管理器
        cf.RoomMgr.init(this);      
        this.createClipRoundText();
        this.scheduleUpdate();

        //获取超限道具 并弹窗提示
        var locationPlayer = cf.PlayerMgr.getLocalPlayer();
        //获取道具列表
        var text = "";
        var ItemData = cf.Data.ITEM_DATA;
        var ItemList = locationPlayer.getItemList();
        if(ItemList != null) {
            var size = ItemList.size();
            for (var i = 0; i < size; i++) {
                if (ItemList.getValueByIndex(i) >= ItemData[ItemList.getKeyByIndex(i)].maxNum&&
                    ItemData[ItemList.getKeyByIndex(i)].uselifeType != 2) {
                    text += ItemData[ItemList.getKeyByIndex(i)].itemName;
                    if (i != size - 1) {
                        text += "、";
                    }
                }
            }

            if(text != "") {
                var genDlg = UIDialog.createCustom(
                    text +cf.Language.getText("text_1195"),
                    cf.Language.getText("text_1224"),
                    null,
                    false,
                    function () {                        
                    }
                );
                this.addChild(genDlg, 101);
            }
        }
        locationPlayer.setFTempAccPoints(parseInt(locationPlayer.getFAccPoints()));
    },

    addListener: function () {
        this._super();
        if ('keyboard' in cc.sys.capabilities) {
            cc.eventManager.addListener({
                event: cc.EventListener.KEYBOARD,
                onKeyPressed: function (key, event) {
                    switch (key) {
                        case cc.KEY.back:
                            cf.dispatchEvent(BaseRoom.DoEvent,BaseRoom.EventFlag.UI_OPTION);
                            break;
                    }
                },
                onKeyReleased: function (key, event) {

                }
            }, this);
        } else {
            cc.log("KEYBOARD Not supported");
        }
    },

    /**
     *  创建滚动字幕
     *  这里富文本的位置设置在native和web平台表现不同
     *  滚动条的层级应该设为最高
     */
    createClipRoundText: function () {
        var self = this;
        var gameAnnoucement = new GameAnnouncement();
        self.addChild(gameAnnoucement,cf.Zoder.MAX);
        var allServerChatInfo = new AllServerChatInfo();
        self.addChild(allServerChatInfo, cf.Zoder.MAX);
    },

    update: function (dt) {
        this._super(dt);

        // if(cf.EffectMgr.SpinStatus()){
            
        //     cf.EffectMgr.spin(dt);
        // }
        //房间逻辑更新
        if(cf.RoomMgr){
            cf.RoomMgr.updateLogic(dt);    
        }
    },

    onClear: function () {
        this._super();
        cc.log("GameLayer:onClear");
        this.unscheduleUpdate()
    },
    
    onTouchesBegan: function (touches, event) {
        cf.RoomMgr.onTouchesBegan(touches, event);
        return true;
    },

    onTouchesMoved: function (touches, event) {
        cf.RoomMgr.onTouchesMoved(touches, event);
    },

    onTouchesEnded: function (touches, event) {
        cf.RoomMgr.onTouchesEnded(touches, event);
    },

    onTouchesCancelled: function (touches, event) {
        cf.RoomMgr.onTouchesCancelled(touches, event);
    }
});