var GameScene = BaseScene.extend({
    _doReloadListener: null,
    _gameLayer: null,
    _roomData: null,

    /**
     * 构造方法
     * @param roomData 包含当前房间Id和活动Id的结构体
     */
    ctor: function (roomData) {
        cc.log("GameScene:init");
        this._super();
        
        //添加监听
        this._doReloadListener = cf.addListener(GameScene.Reload, this._doReload.bind(this));
        //初始化
        this._roomData = roomData;
    },

    onEnter: function () {        
        this._super();
        var self = this;

        //初始化管理器实例
        cf.RoomMgr = RoomMgr.getInstance();
        cf.FishMgr = FishMgr.getInstance();
        cf.BulletMgr = BulletMgr.getInstance();
        cf.EffectMgr = EffectMgr.getInstance();
        cf.SkillMgr = SkillMgr.getInstance();      

        //游戏主layer
        self._gameLayer = new GameLayer();
        self._gameLayer.setVisible(false);
        self._gameLayer.setTag(GameScene.LayerFlag.GAME);
        this.addChild(self._gameLayer);

        //运行
        this.runLayer(GameScene.LayerFlag.LOADING, self._roomData);
    },

    /**
     * 是否资源
     */
    onClear: function () {
        this._super();
        cc.log("GameScene:onClear");
        //移除监听
        cf.removeListener(this._doReloadListener);
        //移除所有管理器
        this._removeAllMgr();

        //移除贴图
        cf.removeTextureForKey(Res.GAME.sprite01Png);
        cf.removeSpritePlist(Res.GAME.sprite01Plist);
        cf.removeTextureForKey(Res.GAME.sprite02Png);
        cf.removeTextureForKey(Res.GAME.skillFishPng);
        cf.removeSpritePlist(Res.GAME.sprite02Plist);
        cf.removeTextureForKey(Res.GAME.sprite02_1Png);
        cf.removeSpritePlist(Res.GAME.sprite02_1Plist);
        cf.removeTextureForKey(Res.GAME.sprite02_2Png);
        cf.removeSpritePlist(Res.GAME.sprite02_2Plist);
        cf.removeTextureForKey(Res.GAME.sprite03Png);
        cf.removeSpritePlist(Res.GAME.sprite03Plist);
        cf.removeTextureForKey(Res.GAME.sprite03_1Png);
        cf.removeSpritePlist(Res.GAME.sprite03_1Plist);
        cf.removeTextureForKey(Res.GAME.sprite04Png);
        cf.removeSpritePlist(Res.GAME.sprite04Plist);
        cf.removeTextureForKey(Res.GAME.sprite04_1Png);
        cf.removeSpritePlist(Res.GAME.sprite04_1Plist);
        cf.removeTextureForKey(Res.GAME.sprite05Png);
        cf.removeSpritePlist(Res.GAME.sprite05Plist);
        cf.removeTextureForKey(Res.GAME.sprite05_1Png);
        cf.removeSpritePlist(Res.GAME.sprite05_1Plist);
        cf.removeTextureForKey(Res.GAME.sprite06Png);
        cf.removeSpritePlist(Res.GAME.sprite06Plist);
        cf.removeTextureForKey(Res.GAME.sprite07Png);
        cf.removeSpritePlist(Res.GAME.sprite07Plist);
        cf.removeTextureForKey(Res.GAME.sprite08Png);
        cf.removeSpritePlist(Res.GAME.sprite08Plist);
        cf.removeTextureForKey(Res.GAME.sprite09Png);
        cf.removeSpritePlist(Res.GAME.sprite09Plist);
        cf.removeTextureForKey(Res.GAME.sprite10Png);
        cf.removeSpritePlist(Res.GAME.sprite10Plist);
        cf.removeTextureForKey(Res.GAME.fisheryPng);
        cf.removeSpritePlist(Res.GAME.fisheryPlist);
        cf.removeTextureForKey(Res.GAME.bulletPng);
        cf.removeSpritePlist(Res.GAME.bulletPlist);
        cf.removeTextureForKey(Res.GAME.bullet1Png);
        cf.removeSpritePlist(Res.GAME.bullet1Plist);
        cf.removeTextureForKey(Res.GAME.goldAnimatePng);
        cf.removeSpritePlist(Res.GAME.goldAnimatePlist);
        cf.removeTextureForKey(Res.GAME.silverAnimatePng);
        cf.removeSpritePlist(Res.GAME.silverAnimatePlist);
       
        cf.removeSpritePlist(Res.GAME.bPlist);
        cf.removeTextureForKey(Res.GAME.coinPng);
        cf.removeSpritePlist(Res.GAME.coinPlist);
        cf.removeTextureForKey(Res.GAME.dragonBallPng);
        cf.removeSpritePlist(Res.GAME.dragonBallPlist);
        cf.removeTextureForKey(Res.GAME.brustPng);
        cf.removeSpritePlist(Res.GAME.brustPlist);
        cf.removeTextureForKey(Res.GAME.jpmoney1Png);
        cf.removeSpritePlist(Res.GAME.jpmoney1Plist);
        cf.removeTextureForKey(Res.GAME.jpmoney1_1Png);
        cf.removeSpritePlist(Res.GAME.jpmoney1_1Plist);
        cf.removeTextureForKey(Res.GAME.jpmoney2Png);
        cf.removeSpritePlist(Res.GAME.jpmoney2Plist);
        cf.removeTextureForKey(Res.GAME.jpmoney2_1Png);
        cf.removeSpritePlist(Res.GAME.jpmoney2_1Plist);
        
        cf.removeTextureForKey(Res.GAME.localTip_Png);
        cf.removeTextureForKey(Res.GAME.scrolls_Png);
        cf.removeTextureForKey(Res.GAME.wave_Png);
        cf.removeTextureForKey(Res.GAME.ef_Png);
        cf.removeTextureForKey(Res.GAME.lv16_Png);
        cf.removeTextureForKey(Res.GAME.lv17_Png);
        cf.removeTextureForKey(Res.GAME.lv18_Png);
        cf.removeTextureForKey(Res.GAME.lv19_Png);
        cf.removeTextureForKey(Res.GAME.lv20_Png);
        cf.removeTextureForKey(Res.GAME.lv21_Png);
        cf.removeTextureForKey(Res.GAME.lv22_Png);
        cf.removeTextureForKey(Res.GAME.lv23_Png);
        cf.removeTextureForKey(Res.GAME.dice_Png);
        cf.removeTextureForKey(Res.GAME.status_Png);
        cf.removeTextureForKey(Res.GAME.boss_text_Png);
        cf.removeTextureForKey(Res.GAME.skill_ZhaoYun_Png);
        cf.removeTextureForKey(Res.GAME.ZhaoYun_Png);
        cf.removeTextureForKey(Res.GAME.lv12_Png);
        cf.removeTextureForKey(Res.GAME.skill_bg_Png);
        cf.removeTextureForKey(Res.GAME.GuanYu_Png);
        cf.removeTextureForKey(Res.GAME.skill_GuanYu_Png);
        cf.removeTextureForKey(Res.GAME.Shu_Png);
        cf.removeTextureForKey(Res.GAME.LiuBei_Png);
        cf.removeTextureForKey(Res.GAME.ZhangFei_Png);
        cf.removeTextureForKey(Res.GAME.DiaoChan_Png);
        cf.removeTextureForKey(Res.GAME.skill_up_Png);
        cf.removeTextureForKey(Res.GAME.skill_center_Png);
        cf.removeTextureForKey(Res.GAME.skill_money1_Png);
        cf.removeTextureForKey(Res.GAME.XiahouDun_Png);
        cf.removeTextureForKey(Res.GAME.skill_XiahouDun_Png);
        cf.removeTextureForKey(Res.GAME.skill_txt_light_Png);
        cf.removeTextureForKey(Res.GAME.boss_coin_Png);
        cf.removeTextureForKey(Res.GAME.real_dynamic_Png);
        cf.removeTextureForKey(Res.GAME.skill_text_Png);
        cf.removeTextureForKey(Res.GAME.skill_sunJian_Png);
        cf.removeTextureForKey(Res.GAME.skill_MaChao_Png);
        cf.removeTextureForKey(Res.GAME.sunJian_Png);
        cf.removeTextureForKey(Res.GAME.MaChao_Png);
        cf.removeTextureForKey(Res.GAME.DongZhuo_Png);
        cf.removeTextureForKey(Res.GAME.moneywave_Png);
        cf.removeTextureForKey(Res.GAME.jpfortress_Png);
        cf.removeTextureForKey(Res.GAME.jpfortressall_Png);
        cf.removeTextureForKey(Res.GAME.jpbg_Png);
        cf.removeTextureForKey(Res.GAME.jpreward_Png);
        cf.removeTextureForKey(Res.GAME.LuBu_Png);
        cf.removeTextureForKey(Res.GAME.boss_appear_Png);
        
        cf.removeTextureForKey(Res.GAME.gamingTable_Png);
        cf.removeTextureForKey(Res.GAME.dicecup_Png);
        cf.removeTextureForKey(Res.GAME.god_wealth_Png);
        cf.removeTextureForKey(Res.GAME.god_of_wealth2_Png);
        cf.removeTextureForKey(Res.GAME.jpreward_Png);
        cf.removeTextureForKey(Res.GAME.larbar_Png);
        cf.removeTextureForKey(Res.GAME.bonus_Png);

        cf.removeTextureForKey(Res.GAME.BonusPng);
        cf.removeSpritePlist(Res.GAME.BonusPlist);



    },

    runLayer: function (flag, roomData) {
        var self = this;

        //如果gameLayer被删除，这里重新初始化
        if(self._gameLayer == null){
            self._gameLayer = new GameLayer();
            self._gameLayer.setVisible(false);
            self._gameLayer.setTag(GameScene.LayerFlag.GAME);
            self.addChild(self._gameLayer);
        }
        
        switch (flag) {
            case GameScene.LayerFlag.LOADING:
                //移除上层所有UI
                self.removeChildByTag(GameScene.LayerFlag.RECONNECT);

                //加载游戏
                var res = g_resGameScene;
                //判断是否为断线重连
                if (roomData.bReturn) {
                    res = null;
                }
                var gameLoader = GameLoader.preload(res, function () {
                    self._gameLayer.setVisible(true);
                }, this, roomData);
                gameLoader.setTag(GameScene.LayerFlag.LOADING);
                gameLoader.setTouchEnabled(true);
                self.addChild(gameLoader);
                break;
            case GameScene.LayerFlag.RECONNECT:
                var reConnectLayer = new ReConnectLayer();
                reConnectLayer.setTag(GameScene.LayerFlag.RECONNECT);
                self.addChild(reConnectLayer);
                break;
        }
    },

    /**
     * 移除管理器
     * @private
     */
    _removeAllMgr: function () {
        //移除房间管理器
        RoomMgr.purgeRelease();
        cf.RoomMgr = null;

        //移除鱼群管理器
        FishMgr.purgeRelease();
        cf.FishMgr = null;

        //移除子弹管理器
        BulletMgr.purgeRelease();
        cf.BulletMgr = null;

        //移除特效管理器
        EffectMgr.purgeRelease();
        cf.EffectMgr = null;

        //移除技能管理器
        SkillMgr.purgeRelease();
        cf.SkillMgr = null;

        CustomNumber.purgeRelease();
    },

    /**
     * 重新加载房间
     * @param roomData 房间信息
     * @private
     */
    _doReload: function (roomData) {
        if (roomData == null) {
            return;
        }
        //移除所有管理器和对象
        this._removeAllMgr();
        this.removeAllChildren();

        this._gameLayer = null;
        this._roomData = null;
        
        //初始化管理器实例
        cf.RoomMgr = RoomMgr.getInstance();
        cf.FishMgr = FishMgr.getInstance();
        cf.BulletMgr = BulletMgr.getInstance();
        cf.EffectMgr = EffectMgr.getInstance();
        cf.SkillMgr = SkillMgr.getInstance();       

        if (roomData.bReturn) {
            //断线重连
            this.runLayer(GameScene.LayerFlag.RECONNECT, roomData);
        } else {
            this.runLayer(GameScene.LayerFlag.LOADING, roomData);
        }
    }
});

GameScene.LayerFlag = {
    LOADING: 0,         //< loading
    GAME: 1,            //< 主游戏层
    RECONNECT: 2        //< 断线重连
};

GameScene.Reload = "GameScene.Reload";