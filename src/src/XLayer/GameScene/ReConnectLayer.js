var ReConnectLayer = BaseLayer.extend({
    _onNetMsgListener: null,
    _roomId: null,
    _toast: null,

    ctor: function () {
        this._super();
    },

    onClear: function () {
        this._super();
    },

    addListener: function () {
        this._super();

        var self = this;

        //创建网络消息
        var msgDataArray = [
            //登录消息
            {msgId: MessageCode.CH_LoginMsg_Res, callFunc: self._resLogin},
            //选择房间
            {msgId: MessageCode.CS_JoinCommStageMsg_Res, callFunc: self._resEnterRoom},
            //龙虎房间
            {msgId: MessageCode.CS_JoinDTStageMsg_Res, callFunc: self._resEnterSpeRoom}
        ];

        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener[i] = cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this));
        }

        //this._askActivityData();

        //开始重连
        this.askGate();
    },

    _askActivityData: function () {
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var strRuleVer = [];
        //先从本地获取活动信息
        var actInfoList = cf.SaveDataMgr.getNativeData("actInfoList");
        if (actInfoList == null || actInfoList == "") {

        } else {
            actInfoList = JSON.parse(actInfoList);
            for (var i = 0; i < actInfoList.length; ++i) {
                var strSelf = {};
                var actRuleInfoList = actInfoList[i].actRuleInfoList;
                var strRule = [];
                for (var j = 0; j < actRuleInfoList.length; j++) {
                    if (actRuleInfoList[j].nIndex && actRuleInfoList[j].updateTime && actRuleInfoList[j].updateTime != "") {
                        var str = actRuleInfoList[j].nIndex + "," + actRuleInfoList[j].updateTime;
                        strRule.push(str);
                    }
                }
                strSelf.nActID = [];
                strSelf.nVersion = [];
                strSelf.strRuleTime = strRule;
                strRuleVer.push(strSelf);
            }
        }
        cc.log(strRuleVer);
        var msg = new $root.CH_AskActivity();
        msg.nidLogin = playerModel.getIdLogin();
        msg.strSafeCode = playerModel.getSafeCode();
        msg.strRuleVer = strRuleVer;
        var wr = $root.CH_AskActivity.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_AskActivityMsg, wr);
    },

    removeListener: function () {
        this._super();
        var len = this._onNetMsgListener.length;
        for (var i = 0; i < len; ++i) {
            cf.removeListener(this._onNetMsgListener[i]);
        }
    },

    askGate: function () {
        var self = this;
        self._toast = UIToastDlg.create(cf.Language.getText("text_1468"), 20, function () {
            self._toast.removeFromParent(true);
            //超时
            cf.dispatchEvent(NotificationScene.eventName, NotificationScene.Error.ERROR_SOCKET_TIMEOUT);
        }, this);
        self._toast.setTouchEnabled(true);
        self._toast.setMask(false);
        self.addChild(self._toast, 1000);

        //延迟两秒请求
        setTimeout(function () {
            //请求门服务器
            SDKHelper.askGate(true);
        }, 2000);
    },

    /**
     * 登录处理
     * @param msg
     * @private
     */
    _resLogin: function (msg) {
        var resMsg = $root.CH_LoginMsg_Res.decode(msg);
        switch (resMsg.nRes) {
            case 0://登录成功
                //初始化玩家数据
                cf.PlayerMgr.createLocalPlayer(resMsg);

                //this.checkActivityIsEnd();

                // 初始化邮件数量
                var nEmailNum = cf.SaveDataMgr.getNativeData(cf.EmailNoReadNum);
                var EmailNumArr = resMsg.strEmailData.split(",");
                if (nEmailNum == null || nEmailNum == "") {
                    cf.SaveDataMgr.setNativeData(cf.EmailNoReadNum, EmailNumArr[0]);
                }
                else {
                    if (EmailNumArr[0] != 0) {
                        cf.SaveDataMgr.setNativeData(cf.EmailNoReadNum, EmailNumArr[0]);
                    }
                }

                //请求活动信息
                this._askActivityData();

                //断线重连
                if (resMsg.bReturn) {
                    var _msg = new $root.CH_ReturnLoginMsg();
                    var wr = $root.CH_ReturnLoginMsg.encode(_msg).finish();
                    cf.NetMgr.sendSocketMsg(MessageCode.CH_ReturnLoginMsg, wr);

                    this._roomId = resMsg.nRoomType;
                } else {
                    //连接错误，返回登录界面
                    cf.NetMgr.closeSocket();
                    cf.SceneMgr.runScene(cf.SceneFlag.LOGIN);
                }
                break;
            case 1://密码错误
                cc.log("密码错误");
                break;
            case 2://封号
                cc.log("封号");

                //检测封号日期
                break;
        }
    },

    /**
     * 解析进入龙虎房间消息
     */
    _resEnterSpeRoom: function (msg) {
        //收到消息
        this._bResMsg = true;
        //进入游戏
        var resMsg = $root.CS_JoinDTStageMsg_Res.decode(msg);
        if(resMsg) {
            //创建房间
            cf.RoomMgr.createRoom(2001,resMsg);

            //显示界面
            var _roomData = {
                roomId: 2001,
                roomType: 5,
                bReturn: true
            };
            var scene = cc.director.getRunningScene();
            scene.runLayer(GameScene.LayerFlag.LOADING, _roomData);
            cf.SceneMgr.setCurSceneFlag(cf.SceneFlag.GAME);
        }
    },

    _resEnterRoom: function (msg) {
        //进入游戏
        var resMsg = $root.CS_JoinCommStageMsg_Res.decode(msg);
       
        if (resMsg.nRes == 0) {
            //正确消息
            cc.log("进入房间成功");
           
            var localPlayer = cf.PlayerMgr.getLocalPlayer();
            localPlayer.setStopSendMsg(false);

          

            var roomId = this._roomId;
            cf.SoundMgr.stopMusic();
            //根据房间id播放音乐
            cf.SoundMgr.playMusic(cf.Data.ROOM_DATA[roomId].bgm, true);
            //设置场景标记
            cf.SceneMgr.setCurSceneFlag(cf.SceneFlag.GAME);
            //同步游戏时间
            localPlayer.setGameTime(resMsg.strTime);      
            //断线重连后恢复龙珠数量
            
            localPlayer.setDragonBallData(roomId,resMsg.attrBall, resMsg.attrBullet, resMsg.collectionNum);      
            //同步
            localPlayer.setSinglePairCannon(resMsg.nSinglePairCannon);           
            //同步奖金索引
            localPlayer.setBillingWelfare(resMsg.nBillingWelfare);

            //创建房间
            cf.RoomMgr.createRoom(roomId, resMsg);

            //显示界面
            var _roomData = {
                roomId: roomId,
                roomType: cf.Data.ROOM_DATA[roomId].room_type,
                bReturn: true
            };
            var scene = cc.director.getRunningScene();
            scene.runLayer(GameScene.LayerFlag.LOADING, _roomData);
        } else {
            //消息错误
            cc.error("_resEnterRoom错误:CS_JoinCommStageMsg_Res");
            //连接错误，返回登录界面
            cf.NetMgr.closeSocket();
            cf.SceneMgr.runScene(cf.SceneFlag.LOGIN);
        }
    }


});