var GameLoader = BaseLoader.extend({

    _onEnterRoomListener: null,         //< 进入房间监听
    _bResMsg: false,
    _roomData: null,

    ctor: function (roomData) {
        this._super();
        this._roomData = roomData;
    },

    prepareLoader: function () {
        //文件加载内部已经完成
        //这里处理实际的数据加载逻辑
        //根据roomid和活动id划分需要加载的数据，仅加载当前房间需要使用的数据
        if (this._roomData == null) {
            cc.error("房间信息错误");
        }

        //断线重连
        if (this._roomData.bReturn) {
            this._bResMsg = true;
        } else {
            var count = this._nCurCount - this._nResTotalCount;
            switch (count) {
                case 2:
                    //初始化json数据
                    DataMgr.readFishGroupData(Res.GAME.FishGroupDataJson);
                    DataMgr.readPathData(Res.GAME.PathDataJson);
                    DataMgr.readFishCollisionData(Res.GAME.FishCollisionDataJson);
                    DataMgr.readBulletCollisionData(Res.GAME.BulletCollisionDataJson);
                    DataMgr.readDoleData(Res.GAME.DoleDataJson);
                    DataMgr.readRoleLevelData(Res.GAME.roleLevelDataJson);
                    DataMgr.readSkillData(Res.GAME.skillDataJson);
                    DataMgr.readPlayMethodIntroduceData(Res.GAME.playMethodDataJson);                    
                    DataMgr.readPokerGameIntroduceData(Res.GAME.pokerDataJson);
                    DataMgr.readFishTideData(Res.GAME.fishTideDataJson);
                    DataMgr.readFishArrayData(Res.GAME.fishArrayDataJson);
                    DataMgr.readHideCoinData(Res.GAME.HideCoinDataJson);
                    DataMgr.readServerConfig(Res.GAME.SeverConfigDataJson);
                    DataMgr.readDropMissile(Res.GAME.DropMissileJson);
                    DataMgr.readAttrBallSkillData(Res.GAME.attrBallSkillDataJson);
                    break;
                case 5:
                    //资源加载
                    ActionMgr.load(Res.GAME.sprite01Plist, Res.GAME.sprite01Png);
                    ActionMgr.load(Res.GAME.sprite02Plist, Res.GAME.sprite02Png);
                    ActionMgr.load(Res.GAME.sprite02_1Plist, Res.GAME.sprite02_1Png);
                    ActionMgr.load(Res.GAME.sprite02_2Plist, Res.GAME.sprite02_2Png);
                    ActionMgr.load(Res.GAME.sprite03Plist, Res.GAME.sprite03Png);
                    ActionMgr.load(Res.GAME.sprite03_1Plist, Res.GAME.sprite03_1Png);
                    ActionMgr.load(Res.GAME.sprite04Plist, Res.GAME.sprite04Png);
                    ActionMgr.load(Res.GAME.sprite04_1Plist, Res.GAME.sprite04_1Png);
                    ActionMgr.load(Res.GAME.sprite05Plist, Res.GAME.sprite05Png);
                    ActionMgr.load(Res.GAME.sprite05_1Plist, Res.GAME.sprite05_1Png);
                    ActionMgr.load(Res.GAME.sprite06Plist, Res.GAME.sprite06Png);
                    ActionMgr.load(Res.GAME.sprite07Plist, Res.GAME.sprite07Png);
                    ActionMgr.load(Res.GAME.sprite08Plist, Res.GAME.sprite08Png);
                    ActionMgr.load(Res.GAME.sprite09Plist, Res.GAME.sprite09Png);
                    ActionMgr.load(Res.GAME.sprite10Plist, Res.GAME.sprite10Png);
                    ActionMgr.load(Res.GAME.skillFishPlist, Res.GAME.skillFishPng);
                    ActionMgr.load(Res.GAME.goldAnimatePlist, Res.GAME.goldAnimatePng);
                    ActionMgr.load(Res.GAME.silverAnimatePlist, Res.GAME.silverAnimatePng);
                    ActionMgr.load(Res.GAME.fisheryPlist, Res.GAME.fisheryPng);
                    ActionMgr.load(Res.GAME.bulletPlist, Res.GAME.bulletPng);
                    ActionMgr.load(Res.GAME.bullet1Plist, Res.GAME.bullet1Png);
                    ActionMgr.load(Res.GAME.dragonBallPlist, Res.GAME.dragonBallPng);
                    ActionMgr.load(Res.GAME.coinPlist, Res.GAME.coinPng);
                    ActionMgr.load(Res.GAME.brustPlist, Res.GAME.brustPng);
                    ActionMgr.load(Res.GAME.jpmoney1Plist, Res.GAME.jpmoney1Png);
                    ActionMgr.load(Res.GAME.jpmoney1_1Plist, Res.GAME.jpmoney1_1Png);
                    ActionMgr.load(Res.GAME.jpmoney2Plist, Res.GAME.jpmoney2Png);
                    ActionMgr.load(Res.GAME.jpmoney2_1Plist, Res.GAME.jpmoney2_1Png);
                    ActionMgr.load(Res.GAME.BonusPlist, Res.GAME.BonusPng);
                    break;
                case 8:
                    //动画加载
                    for (var i in cf.Data.ANIMATE_DATA) {
                        if (cf.Data.ANIMATE_DATA.hasOwnProperty(i)) {
                            var animateData = cf.Data.ANIMATE_DATA[i];
                            var aniId = parseInt(animateData.id);
                            if (aniId == 1001 || aniId == 1501) {
                                continue;
                            }
                            var ret = ActionMgr.createAction(
                                animateData.frameName,
                                animateData.frameNum,
                                animateData.frameSpeed);

                            if (ret == false)
                                continue;
                            cc.log("创建动作Action = " + animateData.frameName);
                        }
                    }
                    break;
                case 10:
                    if (cf.reConnect.bReturn) {
                        //退出游戏后的断线重连
                        var _msg = new $root.CH_ReturnLoginMsg();
                        var _wr = $root.CH_ReturnLoginMsg.encode(_msg).finish();
                        cf.NetMgr.sendSocketMsg(MessageCode.CH_ReturnLoginMsg, _wr);
                    } else {
                        //请求房间
                        var localPlayer = cf.PlayerMgr.getLocalPlayer();
                        if (localPlayer) {
                            var msg = new $root.CS_JoinCommStageMsg();
                            msg.nidLogin = localPlayer._nIdLogin;
                            msg.strSafeCode = localPlayer._strSafeCode;
                            msg.nStageType = this._roomData.roomType;//房间id，需要外部传入
                            msg.nActivityID = this._roomData.roomId;//活动id，需要外部传入
                            var wr = $root.CS_JoinCommStageMsg.encode(msg).finish();

                            cf.NetMgr.sendSocketMsg(MessageCode.CS_JoinCommStageMsg, wr);
                        } else {
                            cc.error("进入房间消息错误:CS_JoinCommStageMsg");

                            //弹出错误提示
                        }
                    }
                    break;
            }
        }
    },

    /**
     * 加载完成
     */
    finishLoader: function () {
        this._super();

        //加载完成，并收到进入房间消息
        if (this._bResMsg) {
            this.removeFromParent(true);
        }

    },

    /**
     * 释放资源
     */
    onClear: function () {
        this._super();
        cc.log("GameLoader:onClear");
    },

    addListener: function () {
        this._super();

        var self = this;

        if (this._roomData.bReturn) {
            return;
        }

        //创建网络消息
        var msgDataArray = [
            //选择房间
            {msgId: MessageCode.CS_JoinCommStageMsg_Res, callFunc: self._resEnterRoom},
            {msgId: MessageCode.CS_JoinDTStageMsg_Res, callFunc: self._resEnterSpeRoom}
        ];

        //创建网络监听
        self._onEnterRoomListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onEnterRoomListener[i] = cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this));
        }
        cc.log("网络监听创建完成");
    },

    removeListener: function () {
        this._super();

        if (!this._onEnterRoomListener) {
            return;
        }
        var len = this._onEnterRoomListener.length;
        for (var i = 0; i < len; ++i) {
            cf.removeListener(this._onEnterRoomListener[i]);
        }
    },

    /**
     * 解析进入龙虎房间消息
     */
    _resEnterSpeRoom: function (msg) {
        //收到消息
        this._bResMsg = true;
        //进入游戏
        var resMsg = $root.CS_JoinDTStageMsg_Res.decode(msg);
        if (resMsg) {
            //创建房间
            cf.RoomMgr.createRoom(2001, resMsg);
            cf.SoundMgr.stopMusic();
            //设置场景标记
            cf.SceneMgr.setCurSceneFlag(cf.SceneFlag.GAME);
            if (this._bLoaderFinish)
                this.removeFromParent(true);
            //还原断线重连状态
            cf.reConnect.bReturn = false;
            cf.reConnect.roomID = 0;
        }
    },

    /**
     * 解析进入房间消息
     * @param msg
     * @private
     */
    _resEnterRoom: function (msg) {
        //收到消息
        this._bResMsg = true;
        //进入游戏        
        var resMsg = $root.CS_JoinCommStageMsg_Res.decode(msg);
        if (resMsg) {
            if (resMsg.nRes == 0) {
                //正确消息
                cc.log("进入房间成功");
                
                var localPlayer = cf.PlayerMgr.getLocalPlayer();
                localPlayer.setStopSendMsg(false);

                if(localPlayer){
                    cf.btnDisable = false;
                }

                var roomId = this._roomData.roomId;
                cf.SoundMgr.stopMusic();
                //根据房间id播放音乐
                cf.SoundMgr.playMusic(cf.Data.ROOM_DATA[roomId].bgm, true);
                //设置场景标记
                cf.SceneMgr.setCurSceneFlag(cf.SceneFlag.GAME);
                
                //断线重连后恢复龙珠数量
                localPlayer.setDragonBallData(roomId,resMsg.attrBall, resMsg.attrBullet, resMsg.collectionNum);
                //同步游戏时间
                localPlayer.setGameTime(resMsg.strTime);
                //同步
                localPlayer.setSinglePairCannon(resMsg.nSinglePairCannon);
                //同步奖金索引
                localPlayer.setBillingWelfare(resMsg.nBillingWelfare);
                //创建房间
                cf.RoomMgr.createRoom(roomId, resMsg);

                if (this._bLoaderFinish)
                    this.removeFromParent(true);

                //还原断线重连状态
                cf.reConnect.bReturn = false;
                cf.reConnect.roomID = 0;

                
            
            } else {
                //消息错误
                cc.error("_resEnterRoom错误:CS_JoinCommStageMsg_Res");

                //弹出错误提示
            }
        }
    }
});

/**
 * 创建加载器
 * @param resources
 * @param cb
 * @param target
 * @param params
 * @returns {*}
 */
GameLoader.preload = function (resources, cb, target, params) {
    var gameLoader = new GameLoader(params);
    gameLoader.init();
    gameLoader.setLoaderCount(30);
    gameLoader.initWithResources(resources, cb, target);

    return gameLoader;
};
