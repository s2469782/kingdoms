cf.SceneMgr = {
    _curSceneFlag: 0,       //< 场景标记
    _curSceneFlagLast:0,    //< 场景上次标记

    /**
     * 初始化
     */
    init: function () {
        this._curSceneFlag = cf.SceneFlag.LOGO;
        this._curSceneFlagLast = cf.SceneFlag.LOGO;
    },

    /**
     * 运行场景
     * @param flag       {number} 场景标记
     * @param commonInfo {object} 生成特定场景需要的信息
     */
    runScene: function (flag, commonInfo) {
        var self = this;
        this._curSceneFlagLast = self._curSceneFlag;
        self._curSceneFlag = flag;

        switch (flag) {
            case cf.SceneFlag.LOGO:
                cc.director.runScene(new LogoScene());
                break;
            case cf.SceneFlag.UPDATE://这里更新场景是放在登录场景下的，特殊处理下
                cc.LoaderScene.preload(g_resLoginScene, function () {
                    cc.director.runScene(new cc.TransitionFade(1, new LoginScene()));
                }, this);
                break;
            case cf.SceneFlag.LOGIN:
                cc.LoaderScene.preload(g_resLoginScene, function () {
                    cc.director.runScene(new cc.TransitionFade(1, new LoginScene()));
                }, this);
                break;
            case cf.SceneFlag.LOBBY:
                cc.director.runScene(new LobbyScene());
                break;
            case cf.SceneFlag.GAME_LOADING:
                if (commonInfo)
                    cc.director.runScene(new GameScene(commonInfo));
                break;
        }
    },

    /**
     * 获取当前场景标记
     * @returns {number}
     */
    getCurSceneFlag: function () {
        return this._curSceneFlag;
    },

    /**
     * 设置当前场景标记
     * @param flag
     */
    setCurSceneFlag: function (flag) {
        this._curSceneFlag = flag;
    },
    /**
     * 获取上次场景标记
     * @returns {number}
     */
    getCurSceneFlagLast: function () {
        return this._curSceneFlagLast;
    },

};