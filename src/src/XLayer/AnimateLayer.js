/**
 * 动画层
 */
var AnimateLayer = BaseLayer.extend({
    _cloudNode:null,

    ctor:function () {
        this._super();
        var self = this;

        self._initBK();
        self._initBKAnimation();
        self.scheduleUpdate();
    },

    _initBK:function () {
        var self = this;
        var winSize = cc.winSize;
        //设置纹理格式
        cf.setDefaultTextureFormat(cc.Texture2D.PIXEL_FORMAT_RGB565);
        //创建背景
        var spBk = new cc.Sprite(Res.BKImage);
        spBk.x = winSize.width / 2;
        spBk.y = winSize.height / 2;
        self.addChild(spBk, -10);
        if(spBk.width) {
            spBk.setScaleX(winSize.width / spBk.width);
            spBk.setScaleY(winSize.height / spBk.height);
        }
        else {
            spBk.setScaleX(winSize.width / 1280);
            spBk.setScaleY(winSize.height / 720);
        }
        cf.setDefaultTextureFormat(cc.Texture2D.PIXEL_FORMAT_RGBA8888);

        var spWaterBehind = new cc.Sprite(Res.LOGIN.water_behind);
        spWaterBehind.setAnchorPoint(0.5, 0);
        spWaterBehind.setPosition(winSize.width / 2, 0);
        spWaterBehind.setScaleX(winSize.width / spWaterBehind.width);
        //canvas不支持liquid效果
        if (cc._renderType !== cc.game.RENDER_TYPE_CANVAS) {
            var nodeGrid = new cc.NodeGrid();
            nodeGrid.runAction(cc.repeatForever(cc.liquid(2, cc.size(16, 12), 1, 5)));
            this.addChild(nodeGrid, -8);

            //nodeGrid.addChild(spWaterFront);
            nodeGrid.addChild(spWaterBehind);
        } else {
            //self.addChild(spWaterFront, -8);
            self.addChild(spWaterBehind, -8);
        }
    },

    _initBKAnimation:function () {
        var self = this;
        var winSize = cc.winSize;
        var fishData = [
            {frame: "#login_ani_sp01.png", start:cc.p(-250,130), end:cc.p(winSize.width+250,130), scale: 0.6, opacity: 200,isFlippedX:false},
            {frame: "#login_ani_sp01.png", start:cc.p(winSize.width+250,30), end:cc.p(-250,30), scale: 1, opacity: 255,isFlippedX:true},
            {frame: "#login_ani_sp02.png", start:cc.p(winSize.width+250,120), end:cc.p(-250,120), scale: 0.7, opacity: 200,isFlippedX:true},
            {frame: "#login_ani_sp02.png", start:cc.p(-250,160), end:cc.p(winSize.width+250,160), scale: 0.4, opacity: 160,isFlippedX:false}
        ];
        for (var i = 0; i < fishData.length; ++i) {

            var spFish = new cc.Sprite(fishData[i].frame);
            spFish.setPosition(fishData[i].start);
            spFish.setScale(fishData[i].scale);
            spFish.setOpacity(fishData[i].opacity);
            spFish.setFlippedX(fishData[i].isFlippedX);
            spFish.runAction(cc.repeatForever(cc.sequence(
                cc.delayTime(cf.randomRange(1,5)),
                cc.moveTo(cf.randomRange(5,10), fishData[i].end),
                cc.callFunc(function () {
                    this.setFlippedX(!this.isFlippedX());
                }, spFish),
                cc.delayTime(cf.randomRange(2,4)),
                cc.moveTo(cf.randomRange(5,10), fishData[i].start),
                cc.callFunc(function () {
                    this.setFlippedX(!this.isFlippedX());
                }, spFish)
            )));
            self.addChild(spFish, -2);
        }

        var createCloud = function (node) {
            var cloudData = [
                {frame: "#cloud_01.png", start:cc.p(0,320),scale:1.4},
                {frame: "#cloud_02.png", start:cc.p(-100,winSize.height-100),scale:1.4},
                {frame: "#cloud_03.png", start:cc.p(700,320),scale:1.6},
                {frame: "#cloud_02.png", start:cc.p(900,winSize.height-100),scale:1.2}
            ];
            for(var j = 0; j < cloudData.length;++j){
                
                var spCloud = new cc.Sprite(cloudData[j].frame);
                spCloud.setAnchorPoint(0,0.5);
                spCloud.setPosition(cloudData[j].start);
                spCloud.setScale(cloudData[j].scale);
                node.addChild(spCloud);
            }
        };

        self._cloudNode = [];
        for(var i = 0; i < 2; ++i){
            self._cloudNode[i] = new cc.Node();
            createCloud(self._cloudNode[i]);
            self.addChild(self._cloudNode[i],-9);
        }
        self._cloudNode[0].setPosition(0,0);
        self._cloudNode[1].setPosition(2300,0);

        ActionMgr.createAction("bird",6,2);

        var spBird = new cc.Sprite("#bird_001.png");
        self.addChild(spBird);
        spBird.setPosition(winSize.width/2+80,winSize.height/2+80);
        spBird.setRotation(-20);
        spBird.runAction(cc.repeatForever(ActionMgr.getAnimate("bird")));
        spBird.runAction(cc.repeatForever(cc.sequence(
            cc.spawn(
                cc.moveTo(20,winSize.width/2+300,winSize.height+50),
                cc.scaleTo(20,0.5)
            ),
            cc.callFunc(function () {
                this.setPosition(winSize.width/2+80,winSize.height/2+80);
                this.setScale(1);
            },spBird)
        )));

        var spBird1 = new cc.Sprite("#bird_001.png");
        self.addChild(spBird1);
        spBird1.setFlippedX(true);
        spBird1.setRotation(30);
        spBird1.setPosition(cc.winSize.width/2,winSize.height/2);
        spBird1.runAction(cc.repeatForever(ActionMgr.getAnimate("bird")));
        spBird1.runAction(cc.repeatForever(cc.sequence(
            cc.spawn(
                cc.moveTo(20,-100,winSize.height/2+100),
                cc.scaleTo(20,0.4)
            ),
            cc.callFunc(function () {
                this.setPosition(cc.winSize.width/2,winSize.height/2);
                this.setScale(1);
            },spBird1)
        )));
    },

    _updateBKAnimation:function (dt) {
        var self = this;
        if(self._cloudNode){
            var cloud0 = this._cloudNode[0];
            var cloud1 = this._cloudNode[1];

            cloud0.x -= 80*dt;
            cloud1.x -= 80*dt;

            if(cloud0.x <= -2300)
                cloud0.setPositionX(cloud1.x+2300);
            if(cloud1.x <= -2300)
                cloud1.setPositionX(cloud0.x+2300);
        }
    },

    update:function (dt) {
        this._updateBKAnimation(dt);
    }
});
