var LobbyLoader = BaseLoader.extend({

    /**
     * 加载逻辑
     */
    prepareLoader: function () {
        //文件加载内部已经完成
        //这里处理实际的数据加载逻辑
        var count = this._nCurCount - this._nResTotalCount;
        switch (count) {
            case 2:
                DataMgr.readRoomData(Res.LOBBY.RoomDataJson);
                DataMgr.readItemData(Res.LOBBY.ItemDataJson);                
                DataMgr.readRewardData(Res.LOBBY.rewardDataJson);
                DataMgr.readbonusDrawData(Res.LOBBY.bonusDrawDataJson);
                DataMgr.readCannonData(Res.LOBBY.CannonDataJson);
                DataMgr.readHeadPortraitData(Res.LOBBY.HeadPortraitDataJson);
		        DataMgr.readShopData(Res.LOBBY.ShopDataJson);
                DataMgr.readSkeletonData(Res.LOBBY.skeletonDataJson);
                DataMgr.readVipData(Res.LOBBY.vipDataJson);               
                DataMgr.readCostData(Res.LOBBY.costDataJson);
                DataMgr.readMatchRewardData(Res.LOBBY.matchRewardDataJson);
                DataMgr.readFishData(Res.LOBBY.FishDataJson);
                DataMgr.readLoginRewardData(Res.LOBBY.loginRewardDataJson);
                DataMgr.readLuckDrawData(Res.LOBBY.luckDrawDataJson);
                DataMgr.readFishKindData(Res.LOBBY.FishLibDataJson);
                break;
            case 5:                
                DataMgr.readMainTaskData(Res.LOBBY.MainTaskData);                
                DataMgr.readRoleLevelData(Res.LOBBY.roleLevelDataJson);
                DataMgr.readRankData(Res.LOBBY.rankDataJson);
                break;
            case 10:
                ActionMgr.load(Res.LOBBY.hallPlist, Res.LOBBY.hallPng);
                ActionMgr.load(Res.LOBBY.itemIconPlist, Res.LOBBY.itemIconPng);
                ActionMgr.load(Res.LOBBY.cannonPlist, Res.LOBBY.cannonPng);             
               
                break;
            case 20:
                cf.SoundMgr.preloadEffect();
                break;
        }
    },

    /**
     * 加载完成
     */
    finishLoader:function () {
        this._super();
    },

    /**
     * 释放资源
     */
    onClear: function () {
        this._super();
    }
});

/**
 * 创建加载器
 * @param resources
 * @param cb
 * @param target
 * @returns {*}
 */
LobbyLoader.preload = function (resources, cb, target) {
    var lobbyLoader = new LobbyLoader();
    lobbyLoader.init();
    lobbyLoader.setLoaderCount(30);
    lobbyLoader.initWithResources(resources, cb, target);

    return lobbyLoader;
};
