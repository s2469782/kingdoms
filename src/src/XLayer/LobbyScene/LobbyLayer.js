var LobbyLayer = BaseLayer.extend({
    _uiArray: null,
    _roomId: 0,
    _activityId: 0,
    _gameAnnouncement: null,
    _doEventListener: null,          //< 大厅自定义事件监听  
    _bQuit: false,
    _actInfo: null,                 //< 活动信息
    //网络监听
    _onNetMsgListener: null,        //< 网络消息监听
    _doEventListener: null,         //< 事件监听

    _headGroup: null,
    _labelNetDelay: null,
    _redpackDotShow: false,         //<紅包領取通知
    _redpackDelay : false, 
    _redpackDot: null,              //<紅點
    _newNotice: null,               //<新活動提示
    _newNoticeShow: false,
    _dragonJP : [0 ,0,0,0,0,0,0,0,0],
    _dragonJP1 :[0 ,0,0,0,0,0,0,0,0],
    _nowNumStr : null,
    _nowNumStr1: null,
    _previousNum: 0,
    _previousNum1: 0,
    _hall: null,   
    _hall1: null,
    _hall2: null,
    _previousBtn: [0, 0, 0],


    /**
     * 初始化
     */
    ctor: function () {
        this._super();

        cc.log("LobbyLayer:init");
        var self = this;
        this.addListener();
        self.initData();

        self.addCustomEventListener();
        //创建背景
        var winSize = cc.winSize;
        cf.setDefaultTextureFormat(cc.Texture2D.PIXEL_FORMAT_RGB565);
        var spBk = new cc.Sprite(Res.BKImage);
        spBk.x = winSize.width / 2;
        spBk.y = winSize.height / 2;
        self.addChild(spBk, -100);

        var effect = ActionMgr.createSpine(Res.LOBBY.bg_effect_Json, Res.LOBBY.bg_effect_Atlas, 1);
        effect.setPosition(winSize.width / 2, winSize.height / 2);
        effect.setAnimation(0,"animation", true);
        self.addChild(effect, -100);
        //
        //
        // spBk.setScaleX(winSize.width / spBk.width);
        // spBk.setScaleY(winSize.height / spBk.height);
        //清除英雄榜資料
        cf.SaveDataMgr.setNativeData("dragonHero", null);
        cf.SaveDataMgr.setNativeData("dragonHero1", null);
        //动画层
        // var animateLayer = new AnimateLayer();
        // self.addChild(animateLayer, -100);

        cf.setDefaultTextureFormat(cc.Texture2D.PIXEL_FORMAT_RGBA4444);

        if (cf.autoOpenRemouldLayer) {
            cf.autoOpenRemouldLayer = false;
        }
      
        //初始化模组
        self.initRole();
        self.initLobbyUiBtn();
        self.createClipRoundText();
        self.initStageUI();

        self.popupAnnoucement();

        //大厅音乐
        cf.SoundMgr.stopMusic();
        cf.SoundMgr.playMusic(2, true);

        //判断是否断线重连
        if (cf.reConnect.bReturn) {
            cf.UITools.showLoadingToast(cf.Language.getText("text_1468"), 4, true, self);
            setTimeout(function () {
                cf.UITools.hideLoadingToast();
                var _roomId = cf.reConnect.roomID;
                var room_type;
                if (_roomId == 2001) {
                    room_type = 5;
                }
                else {
                    room_type = cf.Data.ROOM_DATA[_roomId].room_type;
                }
                var roomData = {
                    roomId: _roomId,
                    roomType: room_type,
                    bReturn: false//这里false因为从登陆界面重连回来需要重新loading资源
                };
                cf.SceneMgr.runScene(cf.SceneFlag.GAME_LOADING, roomData);
            }, 1500);
        } else {

            //请求是否有未到账订单，游戏生命周期只调用一次
            /*if (cf.AskOrder) {
             cf.AskOrder = false;
             SDKHelper.askOrderList(this);
             }*/
        }

        //////////////////////////////////////////////////////////////
        //特殊处理，商店中BMPFont无法获取宽度，这里提前创建后就能正常获取
        //////////////////////////////////////////////////////////////
        if (!cc.sys.isNative) {
            var goldPrice = new cc.LabelBMFont("1", Res.LOBBY.font_resource_090);
            goldPrice.setVisible(false);
            self.addChild(goldPrice);
        }
        //////////////////////////////////////////////////////////////

        self._askActivityData();
        
        //启动心跳检测
        SDKHelper.sendKeepAliveMsg();

        //创建网络消息
        var msgDataArray = [
           
            //龍珠JP更新
            {msgId: MessageCode.JP_Total_Res, callFunc: self._resUpdateDragonJP}
           

        ];

        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener.push(cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this)));
        }


        self.getJP();
       
        
        
    },

    setWinMoney: function(num, num1){
        for (var a = 0; a< this._dragonJP.length; a++) {
            
               
               this._dragonJP[a].string = 0;
               this._dragonJP1[a].string = 0;
          
            
        }
        

        this._nowNumStr = num+"";
        this._nowNumStr1 = num1+"";
        
        let str = ""+this._nowNumStr.split("").reverse().join("");
      
      
         for (var a = 0, b = 0, len = str.length; a < len; a++) {
             if (str[a] !== '.' ) {
                
                this._dragonJP[a].string = parseInt(str[a]);
                
             }else {
                
                
             }
             b++;
         }

         this._previousNum = parseInt(this._nowNumStr);

         let str1 = ""+this._nowNumStr1.split("").reverse().join("");
      
        
         for (var a = 0, b = 0, len = str1.length; a < len; a++) {
             if (str1[a] !== '.' ) {
                
                this._dragonJP1[a].string = parseInt(str1[a]);
                
             }else {
                
                
             }
             b++;
         }

         this._previousNum1 = parseInt(this._nowNumStr1);


    },
   
   
    
    /**
     * 移除监听
     * @private
     */
    removeListener: function () {
        this._super();
        //移除网络监听
        var len = this._onNetMsgListener.length;
        for (var i = 0; i < len; ++i) {
            cf.removeListener(this._onNetMsgListener[i]);
        }
       // cf.removeListener(this._doEventListener);
    },
    _resUpdateDragonJP: function(msg){
        
        var resMsg = $root.JP_Total_Res.decode(msg);
        
        var jpMoney = Math.floor(resMsg.JP[0]);
        var jpMoney1 = Math.floor(resMsg.JP[1]);

        
            if(jpMoney >= 9999999999){
                jpMoney = 9999999999;
                
            }
            if(jpMoney1 >= 9999999999){
                jpMoney1 = 9999999999;
            }
           
            this.setWinMoney(jpMoney, jpMoney1);
        
        
    },

    getJP: function(){
        var _msg = new $root.JP_Total();
        var wr = $root.JP_Total.encode(_msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.JP_Total, wr);
       

       
        var t2 = setInterval(function(){
            var _msg = new $root.JP_Total();
            var wr = $root.JP_Total.encode(_msg).finish();
            cf.NetMgr.sendSocketMsg(MessageCode.JP_Total, wr);

        },5000);

        
    },

    initData: function () {
        var self = this;

        self._uiArray = [];
        self._uiArray[1001] = [];
        self._uiArray[1002] = [];
        self._uiArray[1004] = [];
        self._uiArray[1005] = [];
        self._roomId = 1002;
    },

    initStageUI: function () {
        var self = this;
        var _posX = cc.winSize.width / 2, _posY = cc.winSize.height / 2;

        

        var _roomInfo = [
            {   
                spineJson: Res.LOBBY.LuBu_Json,
                spineAtlas: Res.LOBBY.LuBu_Atlas,
                index: 10017
                
            },
            {
                spineJson: Res.LOBBY.DiaoChan_Json,
                spineAtlas: Res.LOBBY.DiaoChan_Atlas,
                index: 10018
            },
            {
                
                spineJson: Res.LOBBY.DongZhuo_Json,
                spineAtlas: Res.LOBBY.DongZhuo_Atlas,
                index: 10019
            }
        ];

        var len = _roomInfo.length;
        
        //var scaleArr = [cc.p(_posX - 400, _posY - 10), cc.p(_posX, _posY - 125), cc.p(_posX + 400, _posY)];
       var scaleArr = [cc.p(_posX-150 , _posY + 40), cc.p(_posX-150 , _posY + 40), cc.p(_posX-150 , _posY + 40)];
        var posArr = [1, 1, 1];
       
        var tipArr = [cc.p(0, -30), cc.p(0, -40), cc.p(0, -40)];
        var btnArr = [cc.p(0, 10), cc.p(0, 0), cc.p(0, 0)];
        var jpArr = [cc.p(520, 550), cc.p(330, 480), cc.p(1230, 550)];

        var jpSprite = ["#lobby_jp1.png", "#lobby_jp2.png", "#lobby_jp3.png"];
        var s = ["1002","1003", "1004"];
        for (var i = 0; i < len; ++i) {

            var info = _roomInfo[i];
            var spine = ActionMgr.createSpine(info.spineJson, info.spineAtlas, 1);
            spine.setAnimation(0, "lobby1", false);
            spine.addAnimation(0, "lobby2", true);
            spine.setPosition(scaleArr[i]);
            spine.setTag(30000 + i);
            spine.setVisible(false);
            // spine.setScale(size[i]);
            self.addChild(spine, cf.Zoder.UI);

            
            var roomBtn = new ccui.Button("resource_007_9.png", null, null, 1);
            roomBtn.setScaleX(1.6);
            roomBtn.setScaleY(2.5);
            roomBtn.setPosition(btnArr[i]);
            roomBtn.setTag(info.index);
            roomBtn.setOpacity(0);
            roomBtn.addTouchEventListener(self._enterRoomCallBack, self);
            spine.addChild(roomBtn);

            
            
             var dragonJp = new cc.Sprite(jpSprite[i]);
             dragonJp.setPosition(spine.width/2-jpArr[i].x, spine.height/2-jpArr[i].y);
             spine.addChild(dragonJp, 1); 
            if(i > 0){
             var heroBtn = new ccui.Button();
             heroBtn.loadTextureNormal("dragon_icon.png", ccui.Widget.PLIST_TEXTURE);
             heroBtn.setPosition(dragonJp.width /2+10 , dragonJp.height/2 -40);
             heroBtn.setPressedActionEnabled(true);
             heroBtn.pressIndex = s[i];
             heroBtn.addClickEventListener((sender, type)=>{
               
                var heroLayer = new UIDragonHeroRecord(sender.pressIndex);
                this.addChild(heroLayer, 100);
                    // if (heroLayer == null) {
                    //     heroLayer = new UIDragonHeroRecord();
                    //     this.addChild(heroLayer, 100, LobbyLayer.LayerTag.DRAGONRECORD);
                    // } else {
                    //     heroLayer.show();
                    // }

            });
             dragonJp.addChild(heroBtn);
            }
            //  var roomData = cf.Data.ROOM_DATA[1002 + i];
            // var tipBK = new cc.Sprite("#resource_022.png");
            // tipBK.setScaleX(0.4);
            // tipBK.setPosition(tipArr[i]);
            // tipBK.setOpacity(200);
            // spine.addChild(tipBK);

            //条件
            // var low = cf.Data.CANNON_LEVEL_DATA[roomData.cannonlevel_limit[0]].cannonlevel;
            // var high = cf.Data.CANNON_LEVEL_DATA[roomData.cannonlevel_limit[1]].cannonlevel;
            // var strTip = cf.Language.getText("text_1046");
            // var labelFort = new ccui.LabelBMFont(strTip.format(low, high), Res.LOBBY.font_resource_083);
            // labelFort.setPosition(tipBK.width / 2, tipBK.height / 2);
            // labelFort.setScaleX(1 / 0.4);
            // tipBK.addChild(labelFort);
            
            /*if (i > 0) {
             var tipDropBK = new cc.Sprite("#resource_022.png");
             tipDropBK.setPosition(tipBK.width / 2, - 40);
             //tipDropBK.setOpacity(200);
             tipBK.addChild(tipDropBK);

             var labelDrop = new ccui.Text("掉落：" + roomData.dropItem, cf.Language.FontName, 24);
             labelDrop.setPosition(tipDropBK.width / 2, tipDropBK.height / 2);
             labelDrop.setScaleX(1 / 0.4);
             labelDrop.setTextColor(cc.color.GREEN);
             //labelDrop.enableOutline(cc.color.RED,2);
             tipDropBK.addChild(labelDrop);
             }*/ 

            // var promptBk = new cc.Sprite("#hall_018.png");
            // promptBk.setScaleX(1 / 0.4);
            // promptBk.setPosition(tipBK.width / 2, -65);
            // tipBK.addChild(promptBk);

            // var promptIcon = new cc.Sprite("#hall_0" + (22 + i) + ".png");
            // promptIcon.setScale(1);
            // promptIcon.setPosition(roomBtn.width / 2-90, roomBtn.height / 2-180);
            // spine.addChild(promptIcon);
        }

        

        // for(var a = 0; a <2; a++){
        //     var dragonJp = new cc.Sprite("#lobby_jp.png");
        //      dragonJp.setPosition(_posX+(a*400), _posY - 180 );
        //      self.addChild(dragonJp,cf.Zoder.UI); 

        //      var heroBtn = new ccui.Button();
        //      heroBtn.loadTextureNormal("dragon_icon.png", ccui.Widget.PLIST_TEXTURE);
        //      heroBtn.setPosition(dragonJp.width /2 , dragonJp.height/2 -33);
        //      heroBtn.setPressedActionEnabled(true);
        //      heroBtn.pressIndex = s[a];
        //      heroBtn.addClickEventListener((sender, type)=>{
               
        //         var heroLayer = new UIDragonHeroRecord(sender.pressIndex);
        //         this.addChild(heroLayer, 100);
        //             // if (heroLayer == null) {
        //             //     heroLayer = new UIDragonHeroRecord();
        //             //     this.addChild(heroLayer, 100, LobbyLayer.LayerTag.DRAGONRECORD);
        //             // } else {
        //             //     heroLayer.show();
        //             // }
            
            
                


        //     });
        //      dragonJp.addChild(heroBtn);
             
             
        //}

            for(var a = 0; a < this._dragonJP.length; a++){
                this._dragonJP[a] = new cc.LabelBMFont(0, Res.LOBBY.font_resource_090);
                this._dragonJP[a] .setPosition((_posX )-30-(a*32), _posY/2+71); //-(a*22)
                this._dragonJP[a] .setScaleX(0.9);
                this._dragonJP[a] .setScaleY(0.9);
                this._dragonJP[a] .setVisible(false)
                this.addChild(this._dragonJP[a]);

                this._dragonJP1[a] = new cc.LabelBMFont(0, Res.LOBBY.font_resource_090);
                this._dragonJP1[a] .setPosition((_posX )-32-(a*32), _posY/2+68); //-(a*22)
                this._dragonJP1[a] .setScaleX(0.9);
                this._dragonJP1[a] .setScaleY(0.9);
                this.addChild(this._dragonJP1[a]);

             }
            var hallAtlas, hallJson;
            hallAtlas = Res.LOBBY.hall_ui01_Atlas;
            hallJson = Res.LOBBY.hall_ui01_Json;

            var AniArr = [cc.p(_posX+380 , _posY + 140), cc.p(_posX+330, _posY-55), cc.p(_posX +280, _posY-225)];

            var hallAni = this.hall = ActionMgr.createSpine(hallJson, hallAtlas, 1);
            hallAni.setAnimation(0, "102_start", false);
            hallAni.setPosition(AniArr[0]);
            this.addChild(hallAni);
            hallAni.setCompleteListener(()=>{
                hallAni.setAnimation(0, "103_loop", false);
            })

            this.getChildByTag(30002).setVisible(true);
            this._previousBtn[0] = 0;
            this._previousBtn[1] = 0;
            this._previousBtn[2] = 0;
            this._previousBtn[2] = this._previousBtn[2]+1;
           
            var hallAni1 = this.hall1 = ActionMgr.createSpine(hallJson, hallAtlas, 1);
            hallAni1.setAnimation(0, "201_normal", false);
            hallAni1.setPosition(AniArr[1]);
            this.addChild(hallAni1);

            var hallAni2 = this.hall2 = ActionMgr.createSpine(hallJson, hallAtlas, 1);
            hallAni2.setAnimation(0, "301_normal", false);
            hallAni2.setPosition(AniArr[2]);
            this.addChild(hallAni2);

   
            var roomBtn = new ccui.Button("resource_007_9.png", null, null, 1);
            roomBtn.setScaleX(2.2);
            roomBtn.setScaleY(1);
            roomBtn.setPosition(hallAni.width/2, hallAni.height/2);
            roomBtn.setTag(2);
            roomBtn.setOpacity(0);
            roomBtn.addTouchEventListener(self._hallBtnCallBack, self);
            this.hall.addChild(roomBtn);

            
            var roomBtn1 = new ccui.Button("resource_007_9.png", null, null, 1);
            roomBtn1.setScaleX(2);
            roomBtn1.setScaleY(1);
            roomBtn1.setPosition(hallAni1.width/2, hallAni1.height/2);
            roomBtn1.setTag(1);
            roomBtn1.setOpacity(0);
            roomBtn1.addTouchEventListener(self._hallBtnCallBack, self);
            this.hall1.addChild(roomBtn1);

            
            var roomBtn2 = new ccui.Button("resource_007_9.png", null, null, 1);
            roomBtn2.setScaleX(2);
            roomBtn2.setScaleY(1);
            roomBtn2.setPosition(hallAni2.width/2, hallAni2.height/2);
            roomBtn2.setTag(0);
            roomBtn2.setOpacity(0);
            roomBtn2.addTouchEventListener(self._hallBtnCallBack, self);
            this.hall2.addChild(roomBtn2);
            
    },        
    _hallBtnCallBack: function(sender, type){
        var self = this;
        for(var a = 0; a < 3; a++){
            this.getChildByTag(30000+a).setVisible(false);
        }
        for(var a = 0; a < this._dragonJP.length; a++){
            this._dragonJP[a].setVisible(false);
            this._dragonJP1[a].setVisible(false);
        }
        this.getChildByTag(30000+sender.getTag()).setVisible(true);
        switch(type){
            case ccui.Widget.TOUCH_ENDED:
                switch(sender.getTag()){
                    case 2:
                        this._previousBtn[0] = 0;
                        this._previousBtn[1] = 0;
                        this._previousBtn[2] = this._previousBtn[2]+1;
                        for(var a = 0; a < this._dragonJP.length; a++){
                            //this._dragonJP[a].setVisible(false);
                            this._dragonJP1[a].setVisible(true);
                         }
                        this.getChildByTag(30000+sender.getTag()).setAnimation(0, "lobby1", false);
                        this.getChildByTag(30000+sender.getTag()).addAnimation(0, "lobby2", true); 
                        this.hall.setAnimation(0, "102_start", false);
                        this.hall.setCompleteListener(()=>{
                            this.hall.setAnimation(0, "103_loop", true);
                        })
                        
                        this.hall1.setAnimation(0, "201_normal", true);
                        this.hall2.setAnimation(0, "301_normal", true);

                        if(this._previousBtn[2] == 2){
                            this._enterRoomFromIcon(10019);
                        }
                    
                        break;

                    case 1:
                       // self.getChildByTag(30002).setVisible(true);
                       this._previousBtn[0] = 0;
                       this._previousBtn[2] = 0;
                       this._previousBtn[1] = this._previousBtn[1]+1
                       for(var a = 0; a < this._dragonJP.length; a++){
                        this._dragonJP[a].setVisible(true);
                       // this._dragonJP1[a].setVisible(false);
                     }
                        
                        this.hall.setAnimation(0, "101_normal", true);

                        this.hall1.setAnimation(0, "202_start", false);
                        this.hall1.setCompleteListener(()=>{
                            this.hall1.setAnimation(0, "203_loop", true);
                        })
                        
                        this.hall2.setAnimation(0, "301_normal", true);
                        
                        if(this._previousBtn[1] == 2){
                            this._enterRoomFromIcon(10018);
                        }
                        break;
                    case 0:
                        this._previousBtn[2] = 0;
                        this._previousBtn[1] = 0;
                        this._previousBtn[0] = this._previousBtn[0]+1
                     //   self.getChildByTag(30002).setVisible(true);
                    //  for(var a = 0; a < this._dragonJP.length; a++){
                    //     this._dragonJP[a].setVisible(false);
                    //     this._dragonJP1[a].setVisible(false);
                    //  }
                        
                        this.hall.setAnimation(0, "101_normal", true);
                        this.hall1.setAnimation(0, "201_normal", true);
                        
                        this.hall2.setAnimation(0, "302_start", false);
                        this.hall2.setCompleteListener(()=>{
                            this.hall2.setAnimation(0, "303_loop", true);
                        });

                        if(this._previousBtn[0] == 2){
                            this._enterRoomFromIcon(10017);
                        }
                        break;
                }
                
            break;
        }
    },
    _enterRoomFromIcon: function(tag){
        var self = this;

        
        var roomId = tag - 10017 + 1002;
        this._roomId = roomId;
       
        //判断是否可以进入房间
        var errorId = this._isEnterRoom(roomId);
        switch (errorId) {
            case LobbyLayer.EnterRoomError.Level:
                this._showLevelError(roomId);
                return;
            case LobbyLayer.EnterRoomError.GoldMin:
                this._showGoldMinError(roomId);
                return;
            case LobbyLayer.EnterRoomError.GoldMax:
                this._showGoldMaxError(roomId);
                return;
            case LobbyLayer.EnterRoomError.Fort:
                this._showFortError(roomId);
                return;
        }
    
        this._enterRoom();
        //重置移除时间
        cf.RemoveUserTime = 0;
                
        
           
        
    },
    _enterRoomCallBack: function (sender, type) {
        var self = this;
        switch (type) {
            
            case ccui.Widget.TOUCH_ENDED:
                //房間動畫 之後會改
                
                // self.getChildByTag(sender.getTag()-10017+30000).runAction(cc.spawn(
                //     cc.sequence(
                //         cc.scaleTo(0.2, 1.2),
                //         cc.scaleTo(0.2, 1),
                //         cc.callFunc(()=>{
                            var roomId = sender.getTag() - 10017 + 1002;
                            this._roomId = roomId;
                            //判断是否可以进入房间
                            var errorId = this._isEnterRoom(roomId);
                            switch (errorId) {
                                case LobbyLayer.EnterRoomError.Level:
                                    this._showLevelError(roomId);
                                    return;
                                case LobbyLayer.EnterRoomError.GoldMin:
                                    this._showGoldMinError(roomId);
                                    return;
                                case LobbyLayer.EnterRoomError.GoldMax:
                                    this._showGoldMaxError(roomId);
                                    return;
                                case LobbyLayer.EnterRoomError.Fort:
                                    this._showFortError(roomId);
                                    return;
                            }
            
                            this._enterRoom();
                            //重置移除时间
                            cf.RemoveUserTime = 0;
                //         })
                // ))
                   
                //)
               
                break;
        }
    },

    /**
     * 初始化角色的相关信息，包含可弹出头像的信息和金币信息等
     */
    initRole: function () {
        var self = this;
        var headData = cf.Data.HEADPORTRAIT_DATA;
        var locationPlayer = cf.PlayerMgr.getLocalPlayer();
        var goldNum = locationPlayer.getGoldNum();

        //默认头像为0
        var nHeadID = locationPlayer.getHeadId();
        cc.log("nHeadID = " + nHeadID);

        self._headGroup = new cc.Scale9Sprite(ActionMgr.getFrame("resource_036.png"));
        var tempSp = self._headGroup;
        // tempSp.width = 98;
        // tempSp.height = 98;
        tempSp.setAnchorPoint(cc.p(0, 0));
        tempSp.setPosition(cc.p(10, cc.winSize.height - 130));
        self.addChild(tempSp, 2);

        var tempSpNameBg = new cc.Sprite(ActionMgr.getFrame("hall_017.png"));
        tempSpNameBg.setAnchorPoint(cc.p(0, 0));
        tempSpNameBg.setPosition(cc.p(tempSp.x + 50.5, tempSp.y+25));
        self.addChild(tempSpNameBg, 1);

        //人物头像按钮用于弹出人物属性界面
        self._headGroup._btnHead = new cc.Sprite(ActionMgr.getFrame("headPortrait_001.png"));
        self._headGroup._btnHead.setPosition(cc.p(tempSp.width / 2, tempSp.height / 2+5));
        //self._headGroup._btnHead.setScale(0.6);
        tempSp.addChild(self._headGroup._btnHead);


        var nickName = self._headGroup._nickNameLabel = new cc.LabelBMFont(locationPlayer.getNickName(),Res.LOBBY.font_resource_093);
        nickName.setAnchorPoint(cc.p(0, 0.5));
        nickName.setScale(0.8);
        nickName.setColor(cc.color.YELLOW);
        nickName.x = 60;
        nickName.y = tempSpNameBg.height * 0.5;
        tempSpNameBg.addChild(nickName);

        // var propsShowBar = new cc.Sprite("#hall_019.png");
        // propsShowBar.setAnchorPoint(cc.p(0, 0));
        // //propsShowBar.setRotation(180);
        // propsShowBar.setPosition(cc.p(tempSp.x + 150 , tempSp.y + tempSp.height - propsShowBar.height));
        // self.addChild(propsShowBar, 1);

        var roleGold = new cc.Sprite("#resource_069.png");
        roleGold.setScale(0.9);
        roleGold.setAnchorPoint(cc.p(0,0));
        roleGold.setPosition(cc.p(60, 40));
        tempSpNameBg.addChild(roleGold, -1);

        var roleGoldLabelBar = new cc.Sprite("#hall_025.png");
        roleGoldLabelBar.setScale(1.1);
        roleGoldLabelBar.setAnchorPoint(cc.p(0, 0));
        roleGoldLabelBar.setPosition(cc.p(-60, -12));
        roleGold.addChild(roleGoldLabelBar, -1);

        //显示金币的label
        var goldLabel = self._headGroup._goldLabel = new cc.LabelBMFont(cf.numberToY(goldNum, 2) + "",Res.LOBBY.font_resource_090);
        goldLabel.setScale(0.5);
        goldLabel.setColor(cc.color.YELLOW);
        goldLabel.x = roleGoldLabelBar.width/2+30;
        goldLabel.y = roleGoldLabelBar.height/2-5;
        roleGoldLabelBar.addChild(goldLabel);
    },

    /**
     * 初始化ccui.Button
     */
    initLobbyUiBtn: function () {
        var self = this;
        var localPlayerModel = cf.PlayerMgr.getLocalPlayer();

        var imgSrc = [
            {imgName: "hall_002.png", tag: LobbyLayer.BtnEnum.TABLE, isRed: false},
           // {imgName: "hall_006.png", tag: LobbyLayer.BtnEnum.RANK, isRed: false},
            {imgName: "activityicon.png", tag: LobbyLayer.BtnEnum.REDPACK, isRed: false}
        ];
        var redPackLayer = new UIActivityRedPack();
        redPackLayer.addUpdateGoldNotify(this.updateGoldNotify , this);
        
        var length = imgSrc.length;
        for (var i = 0; i < length; i++) {
            var localBtn = new ccui.Button(imgSrc[i].imgName, null, null, 1);
            localBtn.setAnchorPoint(0, 0);
            localBtn.setPosition(cc.p(135 * (i) + 5, 50));
            localBtn.setScale(2.4);
            localBtn.setZoomScale(0.1);
            localBtn.setPressedActionEnabled(true);
            self.addChild(localBtn);
            localBtn.setTag(imgSrc[i].tag);
            localBtn.setOpacity(0);
            localBtn.addTouchEventListener(self.functionalBtnCallBack, self);
            localBtn.runAction(cc.sequence(
                ActionMgr.createUiShowAction(0.1 * (i + 2)).clone(),
                cc.callFunc( ()=> {
                    this._redpackDelay = true;
                    self.addChild(redPackLayer, 100, LobbyLayer.LayerTag.REDPACK);
                    redPackLayer.removeFromParent();
                    self.redpackDotShow();
                    self.activity_Notice();
                })
            ));
        }

        //设置
        var setBtn = new ccui.Button("hall_004.png", null, null, 1);
        setBtn.setAnchorPoint(1, 1);
        setBtn.setPosition(cc.winSize.width - 110, cc.winSize.height - 15);
        setBtn.setPressedActionEnabled(true);
        setBtn.setTag(LobbyLayer.BtnEnum.OPTION);
        setBtn.addTouchEventListener(self.functionalBtnCallBack, self);
        self.addChild(setBtn);

        var targetEle = document.documentElement; 
        var fullBtn = new ccui.Button("enableFullScreen.png", null, null, 1);
        fullBtn.setAnchorPoint(1, 1);
        fullBtn.setPosition(cc.winSize.width - 310, cc.winSize.height - 15);
        fullBtn.setPressedActionEnabled(true);
        fullBtn.setVisible(false);
        var userAgentInfo = navigator.userAgent;
        var Agents = ["iPhone", "iPad", "iPod"];

        var agent = ()=>{
            for (var v = 0; v < Agents.length; v++)
        {
            if (userAgentInfo.indexOf(Agents[v]) > -1)
            {
                return false;
            }
        }
        return true;
        }
        fullBtn.addClickEventListener(()=>{
            fullBtn.setVisible(false);
            difullBtn.setVisible(true);
            if(cc.sys.os == cc.sys.OS_ANDROID && agent== true){
                cc.screen.requestFullScreen(targetEle, onFullScreenCallBack);
            }


        });
        self.addChild(fullBtn);


        //disable全螢幕
        var difullBtn = new ccui.Button("disableFullScreen.png", null, null, 1);
        difullBtn.setAnchorPoint(1, 1);
        difullBtn.setPosition(cc.winSize.width - 310, cc.winSize.height - 15);
        difullBtn.setPressedActionEnabled(true);
        difullBtn.setVisible(false);
        difullBtn.addClickEventListener(()=>{
            fullBtn.setVisible(true);
            difullBtn.setVisible(false);
            if(cc.sys.os == cc.sys.OS_ANDROID && agent== true){
                cc.screen.exitFullScreen();
            }
                        
        });
                    
              
        self.addChild(difullBtn);
                //全螢幕
        var onFullScreenCallBack = ()=>{
            if(cc.sys.os == cc.sys.OS_ANDROID && agent== true){
                window.screen.orientation.addEventListener('change', testttt);
                window.screen.orientation.lock("portrait");
            }
        };


           
        if(!cc.screen.fullScreen()){
            fullBtn.setVisible(true);            
        }else{
            difullBtn.setVisible(true);
        }

            
        
        
        //disable全螢幕


        //帮助
        var helpBtn = new ccui.Button("hall_001.png", null, null, 1);
        helpBtn.setAnchorPoint(1, 1);
        helpBtn.setPosition(cc.winSize.width - 210, cc.winSize.height - 15);
        helpBtn.setPressedActionEnabled(true);
        helpBtn.setTag(LobbyLayer.BtnEnum.HELP);
        helpBtn.addTouchEventListener(self.functionalBtnCallBack, self);
        self.addChild(helpBtn);

        //退出
        var exitBtn = new ccui.Button("fishery_024.png", null, null, 1);
        exitBtn.setAnchorPoint(1, 1);
        exitBtn.setPosition(cc.winSize.width-10, cc.winSize.height - 15);
        exitBtn.addClickEventListener(function () {
            self._backLastPage();
        });
        self.addChild(exitBtn);

        var spNetDelay = new cc.Sprite("#resource_024_10.png");
        spNetDelay.setAnchorPoint(1,1);
        spNetDelay.setPosition(cc.winSize.width,cc.winSize.height);
        spNetDelay.setColor(cc.color.BLACK);
        spNetDelay.setOpacity(150);
        spNetDelay.setScaleX(0.8);
        self.addChild(spNetDelay);       

        var labelNetDelay = self._labelNetDelay = new cc.LabelBMFont(cf.NetDelayTime+"ms",Res.LOBBY.font_resource_093);
        labelNetDelay.setColor(cc.color.GREEN);
        labelNetDelay.setPosition(spNetDelay.width/2,spNetDelay.height/2);
        labelNetDelay.setScaleX(1/0.8*0.5);
        labelNetDelay.setScaleY(0.5);
        spNetDelay.addChild(labelNetDelay);

        this._redpackDot = new cc.Scale9Sprite("redpack_receive.png");
        this._redpackDot.setPosition(cc.winSize.width / 2-420, cc.winSize.height / 2-225);
                //spBK.setScale(0.9);
        this._redpackDot.setVisible(false);
        self.addChild(this._redpackDot);

        this._newNotice = new cc.Scale9Sprite("redpack_new.png");
        this._newNotice.setPosition(cc.winSize.width / 2-420, cc.winSize.height / 2-225);
                //spBK.setScale(0.9);
        this._newNotice.setVisible(false);
        self.addChild(this._newNotice);
        
    },
    _checkFullScreen(){
        


    },
    _backLastPage:function () {
        window.history.go(-1);
        return false;
    },

    /**
     * 功能按钮的触摸回调
     */
    functionalBtnCallBack: function (sender, type) {
        var self = this;
        var tag = sender.getTag();
        if (type != ccui.Widget.TOUCH_ENDED) {
            return;
        }
        //重置移除时间
        cf.RemoveUserTime = 0;
        //开关界面
        cf.SoundMgr.playEffect(8);
        switch (tag) {
            case LobbyLayer.BtnEnum.TABLE:
                //显示报表
                self.initTableLayer();
                break;
            case LobbyLayer.BtnEnum.RANK:
                //排行榜入口
                self.initRankLayer();
                break;
            case LobbyLayer.BtnEnum.HELP:
                self.initHelpLayer();
                break;
            case LobbyLayer.BtnEnum.OPTION:
                self.initOptionLayer();
                break;
            case LobbyLayer.BtnEnum.REDPACK:
                self.initRedPackLayer();
               
                break;
            
        }
    },
    /**
     * 全螢幕
     * @private
     */
    initFullScreen: function(){


    },

    /**
     * 请求活动信息
     * @private
     */
    _askActivityData: function () {
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var strRuleVer = [];
        //先从本地获取活动信息
        var actInfoList = cf.SaveDataMgr.getNativeData("actInfoList");
        if (actInfoList == null || actInfoList == "") {

        } else {
            actInfoList = JSON.parse(actInfoList);
            for (var i = 0; i < actInfoList.length; ++i) {
                var strSelf = {};
                var actRuleInfoList = actInfoList[i].actRuleInfoList;
                var strRule = [];
                for (var j = 0; j < actRuleInfoList.length; j++) {
                    if (actRuleInfoList[j].nIndex && actRuleInfoList[j].updateTime && actRuleInfoList[j].updateTime != "") {
                        var str = actRuleInfoList[j].nIndex + "," + actRuleInfoList[j].updateTime;
                        strRule.push(str);
                    }
                }
               // strSelf.nActID = actInfoList[i].nActID;
                //strSelf.nVersion = actInfoList[i].nVersion;
               strSelf.nActID = [];
               strSelf.nVersion = [];
                strSelf.strRuleTime = strRule;
                strRuleVer.push(strSelf);
            }
        }
        cc.log(strRuleVer);
        var msg = new $root.CH_AskActivity();
        msg.nidLogin = playerModel.getIdLogin();
        msg.strSafeCode = playerModel.getSafeCode();
        msg.strRuleVer = strRuleVer;
        var wr = $root.CH_AskActivity.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_AskActivityMsg, wr);
    },

    initOptionLayer: function () {
        var optionLayer = this.getChildByTag(LobbyLayer.LayerTag.OPTION);
        if (optionLayer == null) {
            optionLayer = new UIOptionLayer();
            this.addChild(optionLayer, 100, LobbyLayer.LayerTag.OPTION);
        } else {
            optionLayer.show();
        }
    },

    /**
     * 初始化紅包活動
     */
    initRedPackLayer: function(){
        
        var redPackLayer = new UIActivityRedPack();
        redPackLayer.addUpdateGoldNotify(this.updateGoldNotify , this);
        this.addChild(redPackLayer, 100, LobbyLayer.LayerTag.REDPACK);
    },
    updateGoldNotify:function(){
        var localPlayer = cf.PlayerMgr.getLocalPlayer();
       // console.log(cf.numberToY(localPlayer.getGoldNum(), 2));
       
        this._headGroup._goldLabel.setString(localPlayer.getGoldNum()+ "");
        
    },
    /**
     * 初始化报表
     */
    initTableLayer: function () {
        var tabLayer = new TableLayer();
        this.addChild(tabLayer, 100, LobbyLayer.LayerTag.TABLE);
    },

    /**
     * 初始化排行榜
     */
    initRankLayer: function () {
        var rankList = this.getChildByTag(LobbyLayer.LayerTag.RANK);
        if (rankList == null) {
            rankList = new UIRankLayer();
            this.addChild(rankList, 100, LobbyLayer.LayerTag.RANK);
        } else {
            rankList.show();
        }
    },
    
    initHelpLayer: function () {
        var helpLayer = this.getChildByTag(LobbyLayer.LayerTag.HELP);
        if (helpLayer == null) {
            helpLayer = new UIHelpLayer();
            this.addChild(helpLayer, 100, LobbyLayer.LayerTag.HELP);
        } else {
            helpLayer.show();
        }
    },

    /**
     * 解析活动数据
     * @returns {*}
     */
    parseActivityInfo: function () {
        var jsonInfo = cf.SaveDataMgr.getNativeData("actInfoList");
        var roulette = {
            actRuleInfoList: [],
            bRank: false,
            channelList: [],
            nActID: 0,
            nSort: -1,
            nState: 0,
            nType: 0,
            nVersion: 0,
            strBtnBKUrl: "",
            strContent: "",
            strContentBKUrl: "",
            strEndShowTime: "",
            strEndTime: "",
            strName: "",
            strStartShowTime: "",
            strStartTime: ""
        };
        cc.log(roulette);
        var actInfoList = [];
        if (jsonInfo == null || jsonInfo == "") {
            actInfoList.push(roulette);
        } else {
            //将字符串转化为json
            var jsonData = JSON.parse(jsonInfo);
            cc.log(jsonData);
            for (var i in jsonData) {
                actInfoList.push(jsonData[i]);
            }
            actInfoList.push(roulette);

            var loop = actInfoList.length;
            var playerModel = cf.PlayerMgr.getLocalPlayer();
            //移除还未到显示期和已过期的数据
            for (var j = 0; j < loop; j++) {
                if (actInfoList[j].nType != cf.ACTTagFlag.Roulette && actInfoList[j].nType != cf.ACTTagFlag.Champions) {
                    //判断活动是否在显示期内
                    if (actInfoList[j].strStartShowTime > playerModel.getGameTime() ||
                        actInfoList[j].strEndShowTime < playerModel.getGameTime()) {
                        actInfoList.splice(j, 1);
                        j = j - 1;    //改变循环变量
                        loop = loop - 1;   //改变循环次数
                    }
                }
            }
            cc.log(actInfoList);
            cc.log("活动JSON解析成功");
        }
        return actInfoList;
    },

    /**
     *  创建滚动字幕
     *  这里富文本的位置设置在native和web平台表现不同
     *  滚动条的层级应该设为最高
     */
    createClipRoundText: function () {
        var self = this;
        self._gameAnnouncement = new GameAnnouncement();
        self.addChild(self._gameAnnouncement, 101);
        var allServerChatInfo = new AllServerChatInfo();
        self.addChild(allServerChatInfo, 101);
    },

    //生成所有图片弹框
    popupAnnoucement: function () {
        var self = this;
        var picAnnoucementArray = MsgPool.getInstance().getPicAnnouncementArray();
        var presentTime = (new Date().getTime());
        var length = picAnnoucementArray.length;
        var satisifyPicArray = [];
        for (var i = 0; i < length; i++) {
            if (presentTime > picAnnoucementArray[i].strStartTime) {
                if (presentTime > picAnnoucementArray[i].strEndTime) {
                    MsgPool.getInstance().removePicAnnouncementElement(picAnnoucementArray[i].nPicNoticeID);
                    i -= 1;
                    length -= 1;
                }
                else {
                    satisifyPicArray.push(picAnnoucementArray[i])
                }
            }
        }
        if (satisifyPicArray.length > 0) {
            self._popupAnnouncement = new PopupAnnouncement(PopupAnnouncement.Enum.PIC, satisifyPicArray);
            self.addChild(self._popupAnnouncement, cf.Zoder.MAX);
        }
    },

    /**
     * 释放资源
     */
    onClear: function () {
        var self = this;
        self._super();
       
        this.removeListener();
        cc.log("LobbyLayer:onClear");
        var tag = cf.SceneMgr.getCurSceneFlag();
        if (tag == cf.SceneFlag.LOGIN) {
            //大厅
            cf.removeTextureForKey(Res.LOBBY.cannonPng);
            cf.removeSpritePlist(Res.LOBBY.cannonPlist);
        }

        cf.removeTextureForKey(Res.BKImage);
        cf.removeTextureForKey(Res.LoginImage);
        cf.removeTextureForKey(Res.LOBBY.hallPng);
        cf.removeSpritePlist(Res.LOBBY.hallPlist);
        cf.removeTextureForKey(Res.LOBBY.DongZhuo_Png);
        cf.removeTextureForKey(Res.LOBBY.LuBu_Png);
        cf.removeTextureForKey(Res.LOBBY.DiaoChan_Png);
        cf.removeTextureForKey(Res.LOBBY.hall_ui01_Png);
        cf.removeTextureForKey(Res.LOBBY.bg_effect_Png);
        cf.removeTextureForKey(Res.LOBBY.red_reward_Png);

       
    },

    /**
     * 添加监听
     * @remark 此方法会自动调用，只继承实现具体逻辑
     */
    addListener: function () {

        var self = this;
        self._super();

        //按键监听
        if ('keyboard' in cc.sys.capabilities) {
            cc.eventManager.addListener({
                event: cc.EventListener.KEYBOARD,
                onKeyPressed: function (key, event) {
                    switch (key) {
                        case cc.KEY.back:
                            self._quitGame();
                            break;
                    }
                },
                onKeyReleased: function (key, event) {

                }
            }, this);
        } else {
            cc.log("KEYBOARD Not supported");
        }
    },

    /**
     * 添加自定义监听
     */
    addCustomEventListener: function () {
        this._doEventListener = cf.addListener(LobbyLayer.DoEvent, this._doEvent.bind(this));
    },

    /**
     * 移除监听
     * @remark 此方法会自动调用，只继承实现具体逻辑
     */
    // removeListener: function () {
    //     this._super();
    //     cf.removeListener(this._doEventListener);
    // },

    /**
     * 请求Gate服务器
     * @private
     */
    _enterRoom: function () {
        var roomData = {
            roomId: this._roomId,
            roomType: cf.Data.ROOM_DATA[this._roomId].room_type,
            bReturn: false
        };
        cf.SceneMgr.runScene(cf.SceneFlag.GAME_LOADING, roomData);
    },

    /**
     * 刷新道具Ui
     */
    _doEvent: function (msg) {
        if (msg == null) {
            cc.error("大厅自定义事件数据错误");
            return;
        }
        var self = this;

        //判断是否为结构
        var flag = msg;
        if (typeof msg == "object") {
            flag = msg.flag;
        }

        switch (flag) {
            case LobbyLayer.EventFlag.UI_UPDATE://刷新大厅UI
                var localPlayer = cf.PlayerMgr.getLocalPlayer();
                this._headGroup._goldLabel.setString(cf.numberToY(localPlayer.getGoldNum(), 2) + "");
                this._headGroup._nickNameLabel.setString(localPlayer.getNickName());
                break;
            case LobbyLayer.EventFlag.EFFECT_GET_GOLD:
                //粒子特效
                var goldParticle = new cc.ParticleSystem(Res.LOBBY.get_gold_plist);
                goldParticle.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
                goldParticle.setAutoRemoveOnFinish(true);
                self.addChild(goldParticle, 100);
                cf.SoundMgr.playEffect(15);
                break;

            case  LobbyLayer.EventFlag.UPDATE_NET_DELAY:
                if(self._labelNetDelay){
                    if(cf.NetDelayTime > 500){
                        this._labelNetDelay.setColor(cc.color.RED);
                    }else if(cf.NetDelayTime > 200){
                        this._labelNetDelay.setColor(cc.color.YELLOW);
                    }else{
                        this._labelNetDelay.setColor(cc.color.GREEN);
                    }
                    self._labelNetDelay.setString(cf.NetDelayTime+"ms");
                }
                break;
                case  LobbyLayer.EventFlag.UPDATE_REDPACK_ENABLE:
                    this._redpackDotShow = true;
                    this.redpackDotShow();
                    break;
                case  LobbyLayer.EventFlag.UPDATE_REDPACK_DISABLE:
                    this._redpackDotShow = false;
                    this.redpackDotShow();
                    break;
                case   LobbyLayer.EventFlag.UPDATE_ACTIVITY_NOTICE_SHOW:
                    this._newNoticeShow = true;
                    break;
                case    LobbyLayer.EventFlag.UPDATE_ACTIVITY_NOTICE_NOTSHOW:   
                    this._newNoticeShow = false;
                    break;
            
            
        }
    },

    
    activity_Notice: function(){
        this._newNotice.setVisible(this._newNoticeShow);

    },

    redpackDotShow: function(){
        
        if(this._redpackDelay){
            
            if(this._redpackDotShow){
            
                this._redpackDot.setVisible(this._redpackDotShow);

            }else{
                

                 this._redpackDot.setVisible(this._redpackDotShow);
                


            }


        }
    },

    /**
     * 关闭LobbyUI(跳转)
     */
    closeLobbyUI: function () {
    },

    /**
     * 获取活动状态
     * @returns {boolean}
     */
    getActivityState: function () {

        var jsonInfo = cf.SaveDataMgr.getNativeData("actInfoList");

        //将字符串转化为json
        var jsonData = JSON.parse(jsonInfo);
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var actRecordList = playerModel.getActRecordList();
        if (jsonData) {
            var TopUPIndex = cf.getJSONIndex(jsonData, cf.ACTTagFlag.TopUP);
            if (TopUPIndex != -1) {
                var arrActData = jsonData[TopUPIndex].actRuleInfoList;
                //判断充值返利领取状态
                for (var k = 0; k < arrActData.length; ++k) {
                    var arrPrice = arrActData[k].nValue;
                    cc.log(actRecordList);
                    var value = null;
                    var nScore = 0;                      //< 充值记录
                    var strOneTimeData = [];
                    if (actRecordList != null && actRecordList.size() > 0) {
                        value = actRecordList.get(jsonData[TopUPIndex].nActID);
                        if (value) {
                            nScore = value.nScore;
                            strOneTimeData = value.strOneTimeData;
                            //标记
                            var taskFlag = 0;
                            if (arrPrice > nScore) {
                                taskFlag = 0;
                            }
                            else {
                                taskFlag = 1;
                                var temp = strOneTimeData.split(",");
                                if (temp[k] == 1) {
                                    taskFlag = 2;
                                }
                            }
                            if (taskFlag == 1) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
        var RouletteRecord = localPlayerModel.getRouletteRecord();
        var freeNum = parseInt(RouletteRecord[0][0]);
        var balanceNum = parseInt(cf.MaxRouletteNum - RouletteRecord[1][0]);

        if (balanceNum == 0) {
            return false;
        }

        if (freeNum > 0) {
            return true;
        }
        else {
            if (localPlayerModel.getFAccPoints() >= cf.MaxRouletteConsumePoints) {
                return true;
            }
        }
        return false;
    },

    _quickStart: function () {
        for (var i = 1002; i < 1005; ++i) {
            if (this._isEnterRoom(i) == LobbyLayer.EnterRoomError.OK) {
                this._roomId = i;
            }
        }
        this._enterRoom();
    },

    _isEnterRoom: function (roomId) {
        var roomData = cf.Data.ROOM_DATA[roomId];

        var localPlayer = cf.PlayerMgr.getLocalPlayer();
        //金币条件
        var curGold = localPlayer.getGoldNum();
        var roomGoldMin = Math.floor(roomData.gold_limit[0]);
        var roomGoldMax = Math.floor(roomData.gold_limit[1]);
        //低级房且等级小于10，不受金币限制
        /*if(roomId == 1002 && localPlayer.getPlayerLevel() < 10){
         return LobbyLayer.EnterRoomError.OK;
         }*/
        if (roomGoldMax != 0) {
            if (curGold > roomGoldMax) {
                //超出金币限制
                return LobbyLayer.EnterRoomError.GoldMax;
            } else {
                if (curGold < roomGoldMin) {
                    //金币不足
                    return LobbyLayer.EnterRoomError.GoldMin;
                }
            }
        } else {
            if (curGold < roomGoldMin) {
                //金币不足
                return LobbyLayer.EnterRoomError.GoldMin;
            }
        }
        return LobbyLayer.EnterRoomError.OK;
    },

    _showLevelError: function (roomId) {
        var self = this;
        var roomData = cf.Data.ROOM_DATA[roomId];
        var strTip = "{0}级解锁此房间，是否前往升级?";
        var dlg = UIDialog.createCustom(strTip.format(roomData.playerlevel_limit), cf.Language.getText("text_1115"), "取消", false, function (type) {
            if (type == UIDialog.Btn.RIGHT) {
                //快速开始
                self._quickStart();
            }
        }, this);
        self.addChild(dlg, 10);
    },

    _showGoldMaxError: function (roomId) {
        var self = this;
        var roomData = cf.Data.ROOM_DATA[roomId];
        var strTip = "金币大于{0}，请前往高级渔场";
        strTip = strTip.format(cf.numberToW(roomData.gold_limit[1]));
        var dlg = UIDialog.createCustom(strTip, cf.Language.getText("text_1115"), cf.Language.getText("text_1225"), false, function (type) {
            if (type == UIDialog.Btn.RIGHT) {
                //快速开始
                self._quickStart();
            }
        }, this);
        self.addChild(dlg, 10);
    },

    _showGoldMinError: function (roomId) {
        var roomData = cf.Data.ROOM_DATA[roomId];
        var strTip = cf.Language.getText("text_1374");
        var gold = Math.floor(roomData.gold_limit[0]) - cf.PlayerMgr.getLocalPlayer().getGoldNum();
        strTip = strTip.formatEx(gold);
        var color = [cc.color.WHITE, cc.color.YELLOW, cc.color.WHITE];
        var dlg = UIDialog.createCustomWithRichText(strTip, color, cf.Language.getText("text_1125"), cf.Language.getText("text_1225"), false, function (type) {
            if (type == UIDialog.Btn.RIGHT) {
                cf.UITools.showHintToast("请联系客服");
            }
        }, this);
        this.addChild(dlg, 10);
    },

    _showFortError: function (roomId) {
        var roomData = cf.Data.ROOM_DATA[roomId];
        var strTip = "需解锁{0}倍炮，充值任意金额解锁全部炮倍，是否前往解锁?";
        var fortNum = cf.Data.CANNON_LEVEL_DATA[roomData.cannonlevel_limit[0]].cannonlevel;
        var dlg = UIDialog.createCustom(strTip.format(fortNum), cf.Language.getText("text_1004"), cf.Language.getText("text_1225"), false, function (type) {
        }, this);
        this.addChild(dlg, 10);
    },

    /**
     * 退出游戏
     * @private
     */
    _quitGame: function () {
        if (this._bQuit) {
            return;
        }
        var prompt = UIDialog.create("text_1270", "text_1224", "text_1225", true, function (type) {
            if (type == UIDialog.Btn.RIGHT) {
                cf.NetMgr.closeSocket();
                cf.SaveDataMgr._idLogin = null;
                cf.PlayerMgr._localPlayer = null;
                SDKHelper.logout();
                if (cc.sys.isNative) {
                    cf.SceneMgr.runScene(cf.SceneFlag.UPDATE, null);
                } else {
                    cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                }
            }
            this._bQuit = false;
        }, this);
        this.addChild(prompt, 100);
        this._bQuit = true;
    }
});

LobbyLayer.LayerTag = {
    RANK: 20001,        //<排行榜面板
    TABLE: 20002,       //<报表面板
    ROLE: 20003,        //<角色面板
    HELP: 20004,        //<帮助面板
    REDPACK: 20005,     //<紅包活動
    MAX: 29999
};

LobbyLayer.BtnEnum = {
    OPTION: 10001,      //<设置
    RANK: 10002,        //<排行榜
    TABLE: 10003,       //<报表
    HELP: 10004,        //<帮助
    REDPACK: 10005,     //<紅包活動
    MAX: 19999
};

LobbyLayer.DoEvent = "LobbyLayer.DoEvent";
LobbyLayer.EventFlag = {
    UI_UPDATE: 0,           //< 刷新大厅UI
    UPDATE_ACTIVITY: 1,      //< 刷新活动信息
    EFFECT_GET_GOLD: 19,      //< 获得金币特效
    UPDATE_EXCHANGE: 20,          //更新兑换界面
    UPDATE_NET_DELAY:21,        //<更新网络延迟
    UPDATE_REDPACK_ENABLE:22,          //<更新紅包可領取
    UPDATE_REDPACK_DISABLE:23,      //<更新紅包不可領取
    UPDATE_ACTIVITY_NOTICE_SHOW: 24,       //<新活動通知開啟
    UPDATE_ACTIVITY_NOTICE_NOTSHOW: 25,   //<新活動不通知
    
};

LobbyLayer.EnterRoomError = {
    Level: 0,     //<等级不满足
    GoldMin: 1,   //<金币不足
    GoldMax: 2,    //<金币超出
    Fort: 3,      //<炮倍不满足

    OK: 10
};
var testttt = ()=>{

    window.screen.orientation.lock("landscape");
};