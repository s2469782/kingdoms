var LobbyScene = BaseScene.extend({
    onEnter: function () {
        this._super();
        cc.log("LobbyScene:init");
        
        //添加layer
        var lobbyLoader = LobbyLoader.preload(g_resLobbyScene, function () {
            var lobbyLayer = new LobbyLayer();
            this.addChild(lobbyLayer);
            //移除loading界面
            this.removeChildByTag(1);
        }, this);
        lobbyLoader.setTag(1);
        this.addChild(lobbyLoader);
    },
    
    onClear: function () {
        this._super();
        cc.log("LobbyScene:onClear");
    }
});