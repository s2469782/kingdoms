var LogoLayer = BaseLayer.extend({
    ctor: function () {
        this._super();
        //初始化
        cc.log("LogoLayer:init");
        //对象
        var self = this;
        //屏幕大小
        var winSize = cc.winSize;

        //创建背景层
        var layerBk = new cc.LayerColor(cc.color(255, 255, 255, 255));
        self.addChild(layerBk);
        
        var labelTip = new ccui.Text("您的浏览器不支持本游戏\n建议更换为Chrome（谷歌浏览器）或者Firefox（火狐浏览器）\n为您带来更好的游戏体验!",cf.Language.FontName,32);
        labelTip.setPosition(winSize.width/2,winSize.height/2);
        labelTip.setTextColor(cc.color.BLACK);
        self.addChild(labelTip);
    },

    /**
     * 释放资源
     */
    onClear: function () {
        this._super();
    }
});

/**
 * LogoScene
 */
var LogoScene = BaseScene.extend({
    onEnter: function () {
        this._super();

        cc.log("LogoScene:init");
        //初始化
        var logoLayer = new LogoLayer();
        this.addChild(logoLayer);
    },

    /**
     * 释放资源
     */
    onClear: function () {
        this._super();
        cc.log("LogoScene:clear");

        //释放
    }
});