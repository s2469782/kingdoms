var UIRoomRoleInfoBD = cc.Node.extend({

    _spBk: null,            //<背景
    _nickName: null,        //<昵称
    _isLocal: null,

    ctor: function (playerModel, isLocal) {
        this._super();
        var self = this;
        self._isLocal = isLocal;

        self._spBk = new cc.Scale9Sprite("resource_012_9.png");
        self._spBk.setPosition(0, 0);
        self._spBk.setContentSize(290,136);
        self.addChild(self._spBk);

        //头像，根据头像id读表获取
        var headData = cf.Data.HEADPORTRAIT_DATA;
        //遍历头像表获取头像ID所在下标
        var tempHeadID = playerModel.getHeadId();
        var spHead = new cc.Sprite("#" + headData[tempHeadID].headIcon);
        spHead.setAnchorPoint(0, 1);
        spHead.setPosition(15, self._spBk.height - 30);
        spHead.setScale(0.6);
        self._spBk.addChild(spHead);
        
        var _str = [
            playerModel.getNickName(),
            cf.Language.getText("text_1089") + playerModel.getVipLevel(),
            cf.Language.getText("text_1059") + playerModel.getGamePlayerId(),
            cf.Language.getText("text_1060") + "：" + playerModel.getPlayerLevel(),
        ];
        for (var i = 0; i < 4; ++i) {
            var label = new cc.LabelBMFont(_str[i], Res.LOBBY.font_resource_093);
            label.setAnchorPoint(0, 1);
            label.setPosition(self._spBk.width / 2 - 25, self._spBk.height - 8 - 30 * i);
            label.setTag(UIRoomRoleInfoBD.Tag.NAME+i);
            label.setScale(0.65);
            self._spBk.addChild(label);
            if(i == 0){
                label.setPosition(self._spBk.width / 2,self._spBk.height - 20);
                label.setAnchorPoint(0.5, 0.5);
                label.setColor(cc.color.YELLOW);
            }
        }
    },

    showRoleInfo: function (bShow) {
        var self = this;
        self.stopAllActions();
        self.setVisible(bShow);
        if (bShow && !self._isLocal) {
            self.runAction(cc.sequence(
                cc.delayTime(3),
                cc.callFunc(function () {
                    self.setVisible(false);
                })
            ))
        }
    },

    updateRoleInfo: function (playerModel) {        
        var levelLabel = this._spBk.getChildByTag(UIRoomRoleInfoBD.Tag.LEVEL);
        if(levelLabel){
            levelLabel.setString(cf.Language.getText("text_1060") + "：" +playerModel.getPlayerLevel());
        }

        var VIPLabel = this._spBk.getChildByTag(UIRoomRoleInfoBD.Tag.VIP);
        if(VIPLabel){
            VIPLabel.setString(cf.Language.getText("text_1089") +playerModel.getVipLevel());
        }
    }

});

UIRoomRoleInfoBD.Tag = {
    NAME: 0,
    VIP: 1,
    ID: 2,
    LEVEL: 3,
};