var UIToastDlg = BaseLayer.extend({
    _maskLayer: null,
    _callback: null,
    _target: null,
    /**
     * 构造函数
     * @param text
     * @param time
     * @param callback
     * @param target
     */
    ctor: function (text, time, callback, target) {
        this._super();
        var self = this;
        self._callback = callback;
        self._target = target;

        //遮罩
        self._maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        self._maskLayer.setVisible(false);
        self.addChild(self._maskLayer);

        //toast背景
        var spToast = new cc.Scale9Sprite("resource_028_9.png");
        spToast.setContentSize(300, 40);
        spToast.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        self.addChild(spToast);

        var textLabel = new cc.LabelTTF(text, cf.Language.FontName, 18);
        textLabel.setPosition(spToast.width / 2, spToast.height / 2-5);
        textLabel.setFontFillColor(cc.color.WHITE);
        //textLabel.enableStroke(cc.color.BLACK, 2);
        spToast.addChild(textLabel);

        spToast.setCascadeOpacityEnabled(true);
        spToast.runAction(cc.sequence(
            cc.delayTime(time),
            cc.fadeOut(1),
            cc.callFunc(function () {
                self._callback.call(self._target);
            })
        ));
    },

    setMask: function (bShow) {
        if (this._maskLayer) {
            this._maskLayer.setVisible(bShow);
        }
    }
});

/**
 * 创建Toast框
 * @param text
 * @param time
 * @param callback
 * @param target
 * @returns {*}
 */
UIToastDlg.create = function (text, time, callback, target) {
    return new UIToastDlg(text, time, callback, target);
};
