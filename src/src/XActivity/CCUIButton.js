//ccui.Button


var CCUIButton=ccui.Button.extend({


    ctor: function(){
        this._super();
    },
    _onPressStateChangedToNormal : function (){
        this._buttonScale9Renderer.setSpriteFrame(this._buttonNormalSpriteFrame);

        this._buttonScale9Renderer.setState( ccui.Scale9Sprite.state.NORMAL);

        if (this._pressedTextureLoaded) {
            if (this.pressedActionEnabled){
               // this._buttonScale9Renderer.stopAllActions();
               // this._buttonScale9Renderer.setScale(1.0);
               this._buttonScale9Renderer.stopAllActions();
               this._buttonScale9Renderer.setScale(1.0);
               
                if(this._titleRenderer) {
                    this._titleRenderer.stopAllActions();

                    if (this._unifySize){
                        var zoomTitleAction = cc.scaleTo(ccui.Button.ZOOM_ACTION_TIME_STEP, 1, 1);
                        this._titleRenderer.runAction(zoomTitleAction);
                    }else{
                        this._titleRenderer.setScaleX(1);
                        this._titleRenderer.setScaleY(1);
                    }
                }

            }
        } else {
            // this._buttonScale9Renderer.stopAllActions();
            // this._buttonScale9Renderer.setScale(1.0);
            
                this.stopAllActions();
                this.setScale(1.0);
            
            if (this._scale9Enabled) {
                this._buttonScale9Renderer.setColor(cc.color.WHITE);
            }

            if(this._titleRenderer) {
                this._titleRenderer.stopAllActions();

                this._titleRenderer.setScaleX(1);
                this._titleRenderer.setScaleY(1);
            }
        }
    },
    _onPressStateChangedToPressed: function () {
        this._buttonScale9Renderer.setState(ccui.Scale9Sprite.state.NORMAL);

        if (this._pressedTextureLoaded) {
            this._buttonScale9Renderer.setSpriteFrame(this._buttonClickedSpriteFrame);

            if (this.pressedActionEnabled) {
                this.stopAllActions();

                var zoomAction = cc.scaleTo(ccui.Button.ZOOM_ACTION_TIME_STEP,
                                            1.0 + this._zoomScale,
                                            1.0 + this._zoomScale);
                this.runAction(zoomAction);
                   
                if(this._titleRenderer) {
                    this._titleRenderer.stopAllActions();
                    this._titleRenderer.runAction(cc.scaleTo(ccui.Button.ZOOM_ACTION_TIME_STEP,
                                                             1 + this._zoomScale,
                                                             1 + this._zoomScale));
                }
            }
        } else {
            
            this._buttonScale9Renderer.setSpriteFrame(this._buttonClickedSpriteFrame);
            this.stopAllActions();
            this.setScale(1.0 + this._zoomScale, 1.0 + this._zoomScale);
            
            if (this._titleRenderer) {
                this._titleRenderer.stopAllActions();
                this._titleRenderer.setScaleX(1 + this._zoomScale);
                this._titleRenderer.setScaleY(1 + this._zoomScale);
            }
        }
    },
});