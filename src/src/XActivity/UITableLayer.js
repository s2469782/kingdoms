var TableLayer = BaseLayer.extend({

    _pageNum: 1,//总页数
    _pageIndex: 1, //当前页数
    _checkDate: 0, //当前显示数据的日期
    _onNetMsgListener: null, //网络监听
    _costInfo: {}, //用于临时存储当日数据
    _newCostInfo: {}, //仅用于临时存储最新数据
    _oldCostInfo: {}, //用于存储旧数据
    _fishingInfo: {},//用于临时存储当日数据
    _newFishingInfo: {}, //仅用于临时存储最新数据
    _oldFishingInfo: {},//用于存储旧数据
    _detaileInfo: {},
    _detaileCheckId: {}, //详细查询的唯一Id
    _roomId: 0,          //场景ID
    _showInfoMod: 0,     //当前显示数据的类型
    _recordIndex: 0,     //跳转时的index
    _btnBackShow: null,
    _detailRecordBtn: null,
    _detailRecordlight: null,
    _fishingRecordBtn: null,
    _fishingRecordlight: null,
    
    
    ctor: function () {
        this._super();
        var self = this;
        self.initData();
        //遮罩层
        var maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        self.addChild(maskLayer, -100);
        self.setTouchEnabled(true);

        self._date = new Date();
        var startTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._date.getDate() + " " + "00:00:00";
        var endTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._date.getDate() + " " + "23:59:59";
        self.askForFishRecord(startTimeStr, endTimeStr);

        self.initBaseUI();

        self._oldCostInfo = JSON.parse(cf.SaveDataMgr.getNativeData(cf.localCostInfo)) || {};
        self._oldFishingInfo = JSON.parse(cf.SaveDataMgr.getNativeData(cf.localFishingInfo)) || {};
        self._detaileInfo = JSON.parse(cf.SaveDataMgr.getNativeData(cf.localDetaileInfo)) || {};
    },

    initData: function () {
        var self = this;
        self._pageNum = 1;//总页数
        self._pageIndex = 1; //当前页数
        self._checkDate = 0; //当前显示数据的日期
        self._onNetMsgListener = null; //网络监听
        self._costInfo = {}; //用于临时存储当日数据
        self._oldCostInfo = {}; //用于存储旧数据
        self._fishingInfo = {};//用于临时存储当日数据
        self._newCostInfo = {};
        self._newFishingInfo = {};
        self._oldFishingInfo = {};//用于存储旧数据
        self._detaileInfo = {};
        self._detaileCheckId = {}; //详细查询的唯一Id
        self._roomId = 0;
        self._showInfoMod = 1;     //当前显示数据的类型
    },

    initBaseUI: function () {
        var baseBg = this.baseBg = new cc.Scale9Sprite("resource_019_9.png");
        baseBg.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        baseBg.setContentSize(1050, 650);
        this.addChild(baseBg);

        // var decorateBg = new cc.Scale9Sprite("resource_012_9.png");
        // decorateBg.setPosition(baseBg.width / 2, baseBg.height / 2);
        // decorateBg.setContentSize(1010, 490);
        // baseBg.addChild(decorateBg);
        // for(var a = 0; a < 7; a++){
        //     var back  = new cc.Sprite("#report_back.png");
        //     back.setPosition(520, baseBg.height - 220 - 50 * a);
        //     back.setScale(0.95);
        //     //back.setVisible(false);
        //     baseBg.addChild(back);
        // }

        var titleBg = new cc.Sprite("#title_report.png");
        titleBg.setPosition(baseBg.width / 2, baseBg.height -40);
        baseBg.addChild(titleBg);

        // var titleText = new ccui.LabelBMFont("游戏报表", Res.LOBBY.font_resource_093);
        // titleText.setPosition(titleBg.width / 2, titleBg.height / 2);
        // titleText.setScale(1.3);
        // titleBg.addChild(titleText);

        //生成装饰线
        var factorArray = [40, 120, 190, 240, 290, 340, 390, 440];
        // for (var i = 0; i < 8; i++) {
        //     var leftLineSp = new cc.Sprite("#blinkLline.png");
        //     leftLineSp.setPosition(baseBg.width / 2 - 234, baseBg.height - factorArray[i]);
        //     leftLineSp.setScale(0.9);
        //     baseBg.addChild(leftLineSp);

        //     var rightLineSp = new cc.Sprite("#blinkLline.png");
        //     rightLineSp.setPosition(baseBg.width / 2 + 234, baseBg.height - factorArray[i]);
        //     rightLineSp.setScale(0.9);
        //     rightLineSp.setFlippedX(true);
        //     baseBg.addChild(rightLineSp);
        // }

        this.initBtn();
        this.addAllTextChildOfTable();
        this.setCostTableState(false, false);
        this.setFishTableState(false, false);
        this.setDetaileTableState(false, false);
    },

    initBtn: function () {
        var self = this;
        var baseBg = this.baseBg;

        var top_line = new cc.Sprite("#resource_050_1.png");
        top_line.setPosition(baseBg.width-37, baseBg.height - 80);
        baseBg.addChild(top_line);  

        for(var a = 0; a < 2; a++){
            var top_show = new cc.Sprite("#resource_012.png");
            top_show.setScale(1);
            top_show.setPosition(baseBg.width-37, -20+(baseBg.height-150 *(a+1) )  );
            baseBg.addChild(top_show);

        }
        
        var bottom_line = new cc.Sprite("#resource_050.png");
        bottom_line.setPosition(baseBg.width-37, baseBg.height - 410);
        baseBg.addChild(bottom_line);  

        

        //添加对应选项按钮
        var detaileRecordShow = new ccui.Button("resource_012.png", "resource_012.png", "resource_012.png", ccui.Widget.PLIST_TEXTURE);
        detaileRecordShow.setPosition(baseBg.width-37, baseBg.height - 165);
        detaileRecordShow.setZoomScale(0.1);
        detaileRecordShow.setScale(1);
        detaileRecordShow.setOpacity(0);
        detaileRecordShow.setPressedActionEnabled(true);
        baseBg.addChild(detaileRecordShow);

        var detailclick = self._detailRecordlight =  new cc.Sprite("#resource_light.png");
        detailclick.setAnchorPoint(0, 0);
        detailclick.setPosition(baseBg.width-75, baseBg.height - 240);
        detailclick.setScale(1);
        detailclick.setVisible(false);
        detailclick.setBlendFunc( cc.SRC_ALPHA,cc.ONE);
        baseBg.addChild(detailclick); 

        
        var detaileBtn =self._detailRecordBtn = new cc.Sprite("#resource_014_1.png");
        detaileBtn.setPosition(baseBg.width-37, baseBg.height - 165);
        baseBg.addChild(self._detailRecordBtn);
        // var detaileRecordShowName = new ccui.LabelBMFont("流水报表", Res.LOBBY.font_resource_093);
        // detaileRecordShowName.setPosition(detaileRecordShow.width / 2 + 2, detaileRecordShow.height / 2);
        // detaileRecordShowName.setName("btnName");
        // detaileRecordShow.addChild(detaileRecordShowName);

        //添加对应选项按钮
        var fishingRecordShow = new ccui.Button("resource_012.png", "resource_012.png", "resource_012.png", ccui.Widget.PLIST_TEXTURE);
        fishingRecordShow.setPosition(baseBg.width-37, baseBg.height - 320);
        fishingRecordShow.setZoomScale(0.1);
        fishingRecordShow.setScale(1);
        fishingRecordShow.setOpacity(0);
        fishingRecordShow.setPressedActionEnabled(true);
        baseBg.addChild(fishingRecordShow);

        var fishingclick = self._fishingRecordlight =  new cc.Sprite("#resource_light.png");
        fishingclick.setAnchorPoint(0, 0);
        fishingclick.setPosition(baseBg.width-75, baseBg.height - 390);
        fishingclick.setScale(1);
        fishingclick.setVisible(true);
       fishingclick.setBlendFunc( cc.SRC_ALPHA,cc.ONE);
        baseBg.addChild(fishingclick); 

        var fishingBtn =self._fishingRecordBtn = new cc.Sprite("#resource_032.png");
        fishingBtn.setPosition(baseBg.width-37, baseBg.height - 320);
        baseBg.addChild(self._fishingRecordBtn);

        // var fishingRecordShowName = new ccui.LabelBMFont("捕鱼报表", Res.LOBBY.font_resource_093);
        // fishingRecordShowName.setPosition(fishingRecordShow.width / 2 + 2, fishingRecordShow.height / 2);
        // fishingRecordShowName.setName("btnName");
        // fishingRecordShow.addChild(fishingRecordShowName);
        // fishingRecordShowName.setColor(cc.color(0, 255, 255));

        detaileRecordShow.addClickEventListener(function () {
            self._showInfoMod = 0;
            //更新流水报表 并归零
            self.setFishTableState(false, false);
            self.setDetaileTableState(false, false);

            //隐藏返回按钮
            if(self._btnBackShow){
                self._btnBackShow.setVisible(false);
            }
            self._fishingRecordBtn.setSpriteFrame("resource_032_1.png");
            self._detailRecordlight.setVisible(true);
            self._fishingRecordlight.setVisible(false);
            self._detailRecordBtn.setSpriteFrame("resource_014.png");
            // var title1 = fishingRecordShow.getChildByName("btnName");
            // title1.setColor(cc.color.WHITE);
            // var title2 = detaileRecordShow.getChildByName("btnName");
            // title2.setColor(cc.color(0, 255, 255));

            var startTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._checkDate + " " + "00:00:00";
            var endTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._checkDate + " " + "23:59:59";
            if (self._checkDate == self._date.getDate()) {
                if (self._newCostInfo.length > 0) {
                    self._costInfo = self._newCostInfo;
                    self.setCostTableState(true, true);
                    self._noDataPrompt.setVisible(false);
                    self.refreshPageNum(self._newCostInfo);
                    self.updateItemReord(self._newCostInfo);
                }
                else {
                    self.askForGameRecord(startTimeStr, endTimeStr);
                }
            }
            else {
                var oldCostInfo = self._oldCostInfo[self._date.getFullYear() + (self._date.getMonth() + 1) + self._checkDate];
                if (oldCostInfo) {
                    self._costInfo = oldCostInfo;
                    self.setCostTableState(true, true);
                    self._noDataPrompt.setVisible(false);
                    self.refreshPageNum(oldCostInfo);
                    self.updateItemReord(oldCostInfo);
                }
                else {
                    self.askForGameRecord(startTimeStr, endTimeStr);
                }
            }
        });

        fishingRecordShow.addClickEventListener(function () {
            self._showInfoMod = 1;
            //更新捕鱼报表 并归零
            self.setCostTableState(false, false);
            self.setDetaileTableState(false, false);
            // var title1 = detaileRecordShow.getChildByName("btnName");
            // title1.setColor(cc.color.WHITE);
            // var title2 = fishingRecordShow.getChildByName("btnName");
            // title2.setColor(cc.color(0, 255, 255));
            self._fishingRecordBtn.setSpriteFrame("resource_032.png");
            self._detailRecordBtn.setSpriteFrame("resource_014_1.png");
            self._detailRecordlight.setVisible(false);
            self._fishingRecordlight.setVisible(true);;
            //隐藏返回按钮
            if(self._btnBackShow){
                self._btnBackShow.setVisible(false);
            }

            var startTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._checkDate + " " + "00:00:00";
            var endTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._checkDate + " " + "23:59:59";

            if (self._checkDate == self._date.getDate()) {
                if (self._newFishingInfo.length > 0) {
                    self._fishingInfo = self._newFishingInfo;
                    self._noDataPrompt.setVisible(false);
                    self.setFishTableState(true, true);
                    self.refreshPageNum(self._newFishingInfo);
                    self.updateItemReord(self._newFishingInfo);
                }
                else {
                    self.askForFishRecord(startTimeStr, endTimeStr);
                }
            }
            else {
                var oldFishInfo = self._oldFishingInfo[self._date.getFullYear() + (self._date.getMonth() + 1) + self._checkDate];
                if (oldFishInfo) {
                    self._fishingInfo = oldFishInfo;
                    self._noDataPrompt.setVisible(false);
                    self.setFishTableState(true, true);
                    self.refreshPageNum(oldFishInfo);
                    self.updateItemReord(oldFishInfo);
                }
                else {
                    self.askForFishRecord(startTimeStr, endTimeStr);
                }
            }
        });

        //数据页面切换

        var pageShow = new cc.Scale9Sprite("resource_028_9.png");
        pageShow.setContentSize(100, 40);
        pageShow.setPosition(baseBg.width / 2, 49);
        baseBg.addChild(pageShow);

        var pageText = self._pageNumLabel = new ccui.LabelBMFont(this._pageIndex + "/" + this._pageNum, Res.LOBBY.font_resource_093);
        pageText.setPosition(pageShow.width / 2, pageShow.height / 2);
        pageShow.addChild(pageText);

        var pageUpBtn = new ccui.Button("resource_018.png", "resource_018.png", "resource_018.png", ccui.Widget.PLIST_TEXTURE);
        pageUpBtn.setPosition(450, 49);
        pageUpBtn.setZoomScale(0.1);
        pageUpBtn.setScale(1);
        pageUpBtn.setRotation(180);
        pageUpBtn.setPressedActionEnabled(true);
        baseBg.addChild(pageUpBtn);
        pageUpBtn.addClickEventListener(function () {
            //上一页 非初始页面时生效
            if (self._pageIndex > 1) {
                self._pageIndex -= 1;
                //重置数据
                self._noDataPrompt.setVisible(false);
                pageText.setString(self._pageIndex + "/" + self._pageNum);
                switch (self._showInfoMod) {
                    case 0:
                        self.updateItemReord(self._costInfo);
                        break;
                    case 1:
                        self.updateItemReord(self._fishingInfo);
                        break;
                    case 2:
                        self.updateItemReord(self._detaileInfo[self._detaileCheckId]);
                        break;
                }
            }
        });

        var pageDownBtn = new ccui.Button("resource_018.png", "resource_018.png", "resource_018.png", ccui.Widget.PLIST_TEXTURE);
        pageDownBtn.setPosition(600, 49);
        pageDownBtn.setZoomScale(0.1);
        pageDownBtn.setScale(1);
        //pageDownBtn.setRotation(180);
        //pageDownBtn.setFlippedX(true);
        pageDownBtn.setPressedActionEnabled(true);
        baseBg.addChild(pageDownBtn);
        pageDownBtn.addClickEventListener(function () {
            //下一页
            if (self._pageIndex < self._pageNum) {
                self._pageIndex += 1;
                //重置数据
                self._noDataPrompt.setVisible(false);
                pageText.setString(self._pageIndex + "/" + self._pageNum);
                switch (self._showInfoMod) {
                    case 0:
                        self.updateItemReord(self._costInfo);
                        break;
                    case 1:
                        self.updateItemReord(self._fishingInfo);
                        break;
                    case 2:
                        self.updateItemReord(self._detaileInfo[self._detaileCheckId]);
                        break;
                }
            }
        });

        //添加日期切换
        var dateShow = new cc.Scale9Sprite("resource_028_9.png");
        dateShow.setContentSize(140, 40);
        dateShow.setPosition(baseBg.width - 200, baseBg.height - 43);
        baseBg.addChild(dateShow);

        self._checkDate = self._date.getDate();
        var dateText = new ccui.LabelBMFont(self._date.getFullYear() + "/" + (self._date.getMonth() + 1) + "/" + self._date.getDate(), Res.LOBBY.font_resource_093);
        dateText.setScale(0.9);
        dateText.setPosition(dateShow.width / 2, pageShow.height / 2);
        dateShow.addChild(dateText);

        var dateUpBtn = new ccui.Button("resource_018.png", "resource_018.png", "resource_018.png", ccui.Widget.PLIST_TEXTURE);
        dateUpBtn.setPosition(baseBg.width - 290, baseBg.height - 43);
        dateUpBtn.setZoomScale(0.1);
        dateUpBtn.setScale(1);
        dateUpBtn.setRotation(180);
        dateUpBtn.setPressedActionEnabled(true);
        baseBg.addChild(dateUpBtn);
        dateUpBtn.addClickEventListener(function () {
            if (self._showInfoMod == 2) {
                return;
            }
            if (self._date.getDate() - self._checkDate < 3) {
                self._checkDate -= 1;
                //刷新日期显示 前一天 根据日期重新显示数据
                dateText.setString(self._date.getFullYear() + "/" + (self._date.getMonth() + 1) + "/" + self._checkDate);
                var startTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._checkDate + " " + "00:00:00";
                var endTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._checkDate + " " + "23:59:59";
                switch (self._showInfoMod) {
                    case 0:
                        var oldCostInfo = self._oldCostInfo[self._date.getFullYear() + (self._date.getMonth() + 1) + self._checkDate];
                        if (oldCostInfo) {
                            self._noDataPrompt.setVisible(false);
                            self.setCostTableState(true, true);
                            self.refreshPageNum(oldCostInfo);
                            self.updateItemReord(oldCostInfo);
                            self._costInfo = oldCostInfo;
                        }
                        else {
                            self.askForGameRecord(startTimeStr, endTimeStr);
                        }
                        break;
                    case 1:
                        var oldFishInfo = self._oldFishingInfo[self._date.getFullYear() + (self._date.getMonth() + 1) + self._checkDate];
                        if (oldFishInfo) {
                            self._noDataPrompt.setVisible(false);
                            self.setFishTableState(true, true);
                            self.refreshPageNum(oldFishInfo);
                            self.updateItemReord(oldFishInfo);
                            self._fishingInfo = oldFishInfo;
                        }
                        else {
                            self.askForFishRecord(startTimeStr, endTimeStr);
                        }
                        break;
                }
            }
            else {
                cf.UITools.showHintToast("最多可查询3天内数据");
            }
        });

        var dateDownBtn = new ccui.Button("resource_018.png", "resource_018.png", "resource_018.png", ccui.Widget.PLIST_TEXTURE);
        dateDownBtn.setPosition(baseBg.width - 110, baseBg.height - 43);
        dateDownBtn.setZoomScale(0.1);
        dateDownBtn.setScale(1);
       // dateDownBtn.setRotation(-90);
        dateDownBtn.setPressedActionEnabled(true);
        baseBg.addChild(dateDownBtn);
        dateDownBtn.addClickEventListener(function () {
            if (self._showInfoMod == 2) {
                return;
            }
            //刷新日期显示 后一天 根据日期重新显示数据
            if (self._checkDate < self._date.getDate()) {
                self._checkDate += 1;
                dateText.setString(self._date.getFullYear() + "/" + (self._date.getMonth() + 1) + "/" + self._checkDate);
                var startTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._checkDate + " " + "00:00:00";
                var endTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._checkDate + " " + "23:59:59";
                switch (self._showInfoMod) {
                    case 0:
                        var oldCostInfo = self._oldCostInfo[self._date.getFullYear() + (self._date.getMonth() + 1) + self._checkDate];
                        if (oldCostInfo) {
                            self._noDataPrompt.setVisible(false);
                            self.setCostTableState(true, true);
                            self.refreshPageNum(oldCostInfo);
                            self.updateItemReord(oldCostInfo);
                            self._costInfo = oldCostInfo;
                        }
                        else {
                            self.askForGameRecord(startTimeStr, endTimeStr);
                        }
                        break;
                    case 1:
                        var oldFishInfo = self._oldFishingInfo[self._date.getFullYear() + (self._date.getMonth() + 1) + self._checkDate];
                        if (oldFishInfo) {
                            self._noDataPrompt.setVisible(false);
                            self.setFishTableState(true, true);
                            self.refreshPageNum(oldFishInfo);
                            self.updateItemReord(oldFishInfo);
                            self._fishingInfo = oldFishInfo;
                        }
                        else {
                            self.askForFishRecord(startTimeStr, endTimeStr);
                        }
                        break;
                }
            }
        });

        //关闭按钮
        var closeBtn = new ccui.Button("resource_021.png", "resource_021.png", "resource_021.png", ccui.Widget.PLIST_TEXTURE);
        closeBtn.setPosition(baseBg.width - 30, baseBg.height - 40);
        closeBtn.setZoomScale(0.1);
        closeBtn.setScale(1);
        closeBtn.setFlippedX(true);
        closeBtn.setPressedActionEnabled(true);
        baseBg.addChild(closeBtn);
        closeBtn.addClickEventListener(function () {
            self.removeFromParent(true);
        });
    },

    //添加流水报表以及捕鱼报表的所有子类显示UI
    addAllTextChildOfTable: function () {
        var self = this;
        var baseBg = this.baseBg;

        //添加详情的返回按钮
        var backShow = self._btnBackShow = new ccui.Button("tableButton_1.png", "tableButton_1.png", "tableButton_1.png", ccui.Widget.PLIST_TEXTURE);
        backShow.setPosition(baseBg.width / 2 + 200, 43);
        backShow.setZoomScale(0.1);
        backShow.setScale(1);
        backShow.setVisible(false);
        backShow.setPressedActionEnabled(true);
        baseBg.addChild(backShow);
        // var backShowName = new ccui.LabelBMFont("返回", Res.LOBBY.font_resource_093);
        // backShowName.setPosition(backShow.width / 2 + 2, backShow.height / 2);
        // backShowName.setName("btnName");
        // backShow.addChild(backShowName);

        backShow.addClickEventListener(function () {
            self._showInfoMod = 1;
            self._pageIndex = self._recordIndex;
            if (self._fishingInfo.length % 5 > 0) {
                self._pageNum = (self._fishingInfo.length - self._fishingInfo.length % 5) / 5 + 1;
            }
            else {
                self._pageNum = (self._fishingInfo.length - self._fishingInfo.length % 5) / 5;
            }

            if (self._pageNum == 0) {
                self._pageNum = 1;
            }
            self._pageNumLabel.setString(self._pageIndex + "/" + self._pageNum);
            backShow.setVisible(false);
            self.setDetaileTableState(false, false);
            self._noDataPrompt.setVisible(false);
            self.setFishTableState(true, true);
            self.updateItemReord(self._fishingInfo);
        });

        //添加文字描述项 并生成原始文字显示项 捕鱼报表分页
        var textArr = ['场景编号', '子弹消耗', '战场获利', '盈利', '开始时间', '结束时间', ''];
        var textPos = [180, 340, 460, 560, 660, 780, 920];
        //描述TextUI的数组
        var textUiArr = this.textUiArr = [];
        //显示详细数据的UI数组
        var showTextUiArr = this.showTextUiArr = [];
        for (var i = 0; i < 7; i++) {
            

            textUiArr[i] = new ccui.LabelBMFont(textArr[i], Res.LOBBY.font_resource_093);
            textUiArr[i].setPosition(textPos[i]+15, baseBg.height - 160);
            textUiArr[i].setColor(cc.color(255, 255, 255));
            textUiArr[i].setScale(0.85);
            baseBg.addChild(textUiArr[i]);

            showTextUiArr[i] = [];
            for (var j = 0; j < 5; j++) {
                if (i == 6) {
                    showTextUiArr[i][j] = new ccui.Button("tableButton.png", "tableButton.png", "tableButton.png", ccui.Widget.PLIST_TEXTURE);
                    showTextUiArr[i][j].setPosition(textPos[i] -20 , baseBg.height - 230 - 68 * j);
                    showTextUiArr[i][j].setZoomScale(0.1);
                    showTextUiArr[i][j].setScale(0.7);
                    showTextUiArr[i][j].setPressedActionEnabled(true);
                    baseBg.addChild(showTextUiArr[i][j]);
                    // var _btnName = new ccui.LabelBMFont("详情", Res.LOBBY.font_resource_093);
                    // _btnName.setPosition(showTextUiArr[i][j].width / 2 + 2, showTextUiArr[i][j].height / 2);
                    // _btnName.setName("btnName");
                    // showTextUiArr[i][j].addChild(_btnName);


                    showTextUiArr[i][j].addClickEventListener(function () {
                        self._showInfoMod = 2;
                        backShow.setVisible(true);
                        self._recordIndex = self._pageIndex;
                        self._noDataPrompt.setVisible(false);
                        self.setCostTableState(false, false);
                        self.setFishTableState(false, false);
                        var strTimeArr = this.getName();
                        self._detaileCheckId = this.getTag();
                        if (strTimeArr && self._detaileCheckId) {
                            var timeArray = strTimeArr.split("|");
                            self._roomId = timeArray[2].substr(timeArray[2].length - 4, timeArray[2].length);
                            if (self._detaileInfo[self._detaileCheckId]) {
                                self.setDetaileTableState(true, true);
                                self.refreshPageNum(self._detaileInfo[self._detaileCheckId]);
                                self.updateItemReord(self._detaileInfo[self._detaileCheckId]);
                            }
                            else {
                                var startTime = timeArray[0];
                                var endTime = timeArray[1];
                                var strRoomId = timeArray[2];
                                self.askForFishDetaileRecord(startTime, endTime, strRoomId);
                            }
                        }
                    });
                }
                else {
                    showTextUiArr[i][j] = new ccui.LabelBMFont("", Res.LOBBY.font_resource_093);
                    showTextUiArr[i][j].setPosition(textPos[i]+15, baseBg.height - 230 - 68 * j);
                    showTextUiArr[i][j].setColor(cc.color.WHITE);
                    showTextUiArr[i][j].setScale(0.75);
                    baseBg.addChild(showTextUiArr[i][j]);
                }
            }
        }

        // 添加文字描述项 并生成原始文字显示项 流水报表分页
        var textCostArr = ['单号', '订单类型', '订单金额', '交易类型', '交易时间'];
        var textCostPos = [170, 340, 500, 660, 860];
        //描述TextUI的数组
        var textCostUiArr = this.textCostUiArr = [];
        //显示详细数据的UI数组
        var showTextCostUiArr = this.showTextCostUiArr = [];
        for (var index = 0; index < 5; index++) {
           

            textCostUiArr[index] = new ccui.LabelBMFont(textCostArr[index], Res.LOBBY.font_resource_093);
            textCostUiArr[index].setPosition(textCostPos[index], baseBg.height - 160);
            textCostUiArr[index].setColor(cc.color(255, 255, 255));
            textCostUiArr[index].setScale(0.85);
            baseBg.addChild(textCostUiArr[index]);

            showTextCostUiArr[index] = [];
            for (var k = 0; k < 5; k++) {
                
                showTextCostUiArr[index][k] = new ccui.LabelBMFont("", Res.LOBBY.font_resource_093);
                showTextCostUiArr[index][k].setPosition(textCostPos[index], baseBg.height - 230 - 68 * k);
                showTextCostUiArr[index][k].setColor(cc.color.WHITE);
                showTextCostUiArr[index][k].setScale(0.75);
                baseBg.addChild(showTextCostUiArr[index][k]);
            }
        }

        // 添加文字描述项 并生成原始文字显示项 流水报表分页
        var textDetaileArr = ['单号', '发射时间', '子弹消耗', '盈利', '倍场', "击破敌军"];
        var textDetailPos = [170, 340, 490, 650, 780, 920];
        //描述TextUI的数组
        var textDetaileUiArr = this.textDetaileFishUiArr = [];
        //显示详细数据的UI数组
        var showDetaileCostUiArr = this.showDetaileFishUiArr = [];
        for (var l = 0; l < 6; l++) {
            

            textDetaileUiArr[l] = new ccui.LabelBMFont(textDetaileArr[l], Res.LOBBY.font_resource_093);
            textDetaileUiArr[l].setPosition(textDetailPos[l]-10, baseBg.height - 160);
            textDetaileUiArr[l].setColor(cc.color(255, 255, 255));
            textDetaileUiArr[l].setScale(0.85);
            baseBg.addChild(textDetaileUiArr[l]);

            showDetaileCostUiArr[l] = [];
            for (var m = 0; m < 5; m++) {
                

                showDetaileCostUiArr[l][m] = new ccui.LabelBMFont("", Res.LOBBY.font_resource_093);
                showDetaileCostUiArr[l][m].setPosition(textDetailPos[l]-10, baseBg.height - 230 - 68 * m);
                showDetaileCostUiArr[l][m].setColor(cc.color.WHITE);
                showDetaileCostUiArr[l][m].setScale(0.85);
                baseBg.addChild(showDetaileCostUiArr[l][m]);

                
            }
        }

        //暂无数据提示
        var noDataPrompt = self._noDataPrompt = new ccui.LabelBMFont("暂无数据", Res.LOBBY.font_resource_093);
        noDataPrompt.setPosition(baseBg.width / 2, baseBg.height - 315);
        noDataPrompt.setVisible(false);
        baseBg.addChild(noDataPrompt);
    },

    //捕鱼报表的详细显示 true 显示 false 隐藏
    setFishTableState: function (state1, state2) {
        var textUiArr = this.textUiArr;
        var showTextUiArr = this.showTextUiArr;
        
        for (var index = 0; index < 7; index++) {
            // back[index].setVisible(state1);
            // back2[index].setVisible(false);
            // back3[index].setVisible(false);
            textUiArr[index].setVisible(state1);
            
            
            for (var j = 0; j < 5; j++) {
                showTextUiArr[index][j].setVisible(state2);
            }
        }
    },

    //流水报表的显示状态 true 显示 false 隐藏
    setCostTableState: function (state1, state2) {
        var textCostUiArr = this.textCostUiArr;
        var showTextCostUiArr = this.showTextCostUiArr;
       
        for (var index = 0; index < 5; index++) {
            // back[index].setVisible(false);
            // back3[index].setVisible(false);
            // back2[index].setVisible(state1);
            textCostUiArr[index].setVisible(state1);
            
            for (var j = 0; j < 5; j++) {
                showTextCostUiArr[index][j].setVisible(state2);
            }
        }
    },

    //捕鱼详细信息的显示状态 true 显示 false 隐藏
    setDetaileTableState: function (state1, state2) {
        var textDetaileUiArr = this.textDetaileFishUiArr;
        var showTextDetaileUiArr = this.showDetaileFishUiArr;
        
        for (var index = 0; index < 6; index++) {
            // back[index].setVisible(false);
            // back2[index].setVisible(false);
            // back3[index].setVisible(state1);
            textDetaileUiArr[index].setVisible(state1);
            
            for (var j = 0; j < 5; j++) {
                showTextDetaileUiArr[index][j].setVisible(state2);
            }
        }
    },

    //在初始化界面向服务器问询默认的捕鱼报表
    askForGameRecord: function (startTimeStr, endTimeStr) {
        var msg = new $root.HM_AskShangXiaFenRecord();
        msg.strStartTime = startTimeStr;
        msg.strEndTime = endTimeStr;
        msg.nidLogin = cf.PlayerMgr.getLocalPlayer().getIdLogin();
        var wr = $root.HM_AskShangXiaFenRecord.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_AskShangXiaFen, wr);
    },

    //向服务器问询详细捕鱼报表
    askForFishDetaileRecord: function (startTimeStr, endTimeStr, strRoomId) {
        var msg = new $root.CH_AskCatchFishLog();
        msg.strStartTime = startTimeStr;
        msg.strEndTime = endTimeStr;
        msg.strRoomID = strRoomId;
        var wr = $root.CH_AskCatchFishLog.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_AskCatchFishLog, wr);
    },

    //在初始化界面向服务器问询默认的捕鱼报表
    askForFishRecord: function (startTimeStr, endTimeStr) {
        var msg = new $root.CH_AskRoomLog();
        msg.strStartTime = startTimeStr;
        msg.strEndTime = endTimeStr;
        var wr = $root.CH_AskRoomLog.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_AskRoomLog, wr);
    },

    /**
     * 添加监听
     * @remark 此方法会自动调用，只继承实现具体逻辑
     */
    addListener: function () {
        this._super();
        var self = this;
        //创建网络消息
        var msgDataArray = [
            //流水报表
            {msgId: MessageCode.CS_AskShangXiaFen_Res, callFunc: self._resGameRecord},
            //捕鱼报表
            {msgId: MessageCode.CH_AskRoomLog_Res, callFunc: self._resFishRecord},
            //捕鱼详细信息
            {msgId: MessageCode.CH_AskCatchFishLog_Res, callFunc: self._resFishDetaileRecord},
        ];


        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener[i] = cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this));
        }
    },

    _resGameRecord: function (msg) {
        var resMsg = $root.HM_AskShangXiaFenRecord_Res.decode(msg);
        cc.log(resMsg);
        if (resMsg) {
            var self = this;
            // 上下分列表
            var sxFenRecordList = resMsg.sxFenRecordList.reverse();
            if (sxFenRecordList.length <= 0) {
                //提示暂未数据
                self._noDataPrompt.setVisible(true);
                self._noDataPrompt.setPosition(self.baseBg.width / 2, self.baseBg.height - 315);
                self.setCostTableState(true, false);
                self.refreshPageNum(sxFenRecordList);
            }
            else {
                // 本地数据存储
                self.setCostTableState(true, true);
                var date = new Date();
                self._noDataPrompt.setVisible(false);
                if (self._checkDate < date.getDate()) {
                    self._oldCostInfo[date.getFullYear() + (date.getMonth() + 1) + self._checkDate] = sxFenRecordList;
                    cf.SaveDataMgr.setNativeData(cf.localCostInfo, JSON.stringify(self._oldCostInfo));
                }
                else {
                    self._newCostInfo = sxFenRecordList;
                }
                self._costInfo = sxFenRecordList;
                self.refreshPageNum(sxFenRecordList);
                self.updateItemReord(sxFenRecordList);
            }
        }
    },

    _resFishRecord: function (msg) {
        var resMsg = $root.CH_AskRoomLog_Res.decode(msg);
        cc.log(resMsg);
        if (resMsg) {
            var self = this;
            var roomLogList = resMsg.roomLogList.reverse();

            if (roomLogList.length <= 0) {
                //提示暂未数据
                self._noDataPrompt.setVisible(true);
                self._noDataPrompt.setPosition(self.baseBg.width / 2, self.baseBg.height - 315);
                self.refreshPageNum(roomLogList);
                self.setFishTableState(true, false);
            }
            else {
                // 本地数据存储
                self.setFishTableState(true, true);
                var date = new Date();
                self._noDataPrompt.setVisible(false);
                if (self._checkDate < date.getDate()) {
                    self._oldFishingInfo[date.getFullYear() + (date.getMonth() + 1) + self._checkDate] = roomLogList;
                    cf.SaveDataMgr.setNativeData(cf.localFishingInfo, JSON.stringify(self._oldFishingInfo));
                }
                else {
                    self._newFishingInfo = roomLogList;
                }
                self._fishingInfo = roomLogList;
                self.refreshPageNum(roomLogList);
                self.updateItemReord(roomLogList);
            }
        }
    },

    _resFishDetaileRecord: function (msg) {
        
        var resMsg = $root.CH_AskCatchFishLog_Res.decode(msg);
        cc.log(resMsg);
        if (resMsg) {
            var self = this;
            var catchFishRecordList = resMsg.catchFishRecordList.reverse();
            if (catchFishRecordList.length <= 0) {
                //提示暂未数据
                self._noDataPrompt.setVisible(true);
                self._noDataPrompt.setPosition(self.baseBg.width / 2, self.baseBg.height - 315);
                self.setDetaileTableState(true, false);
                self.refreshPageNum(catchFishRecordList);
            }
            else {
                self.setDetaileTableState(true, true);
                self._noDataPrompt.setVisible(false);
                self._detaileInfo[self._detaileCheckId] = catchFishRecordList;
                self.refreshPageNum(catchFishRecordList);
                self.updateItemReord(catchFishRecordList);
                cf.SaveDataMgr.setNativeData(cf.localDetaileInfo, JSON.stringify(self._detaileInfo));
            }
        }
    },

    refreshPageNum: function (data) {
        var self = this;
        self._pageIndex = 1;
        if (data.length % 5 > 0) {
            self._pageNum = (data.length - data.length % 5) / 5 + 1;
        }
        else {
            self._pageNum = (data.length - data.length % 5) / 5;
        }

        if (self._pageNum == 0) {
            self._pageNum = 1;
        }
        self._pageNumLabel.setString(self._pageIndex + "/" + self._pageNum);
    },

    resetShowCostUiDate: function () {
        var showTextCostUiArr = this.showTextCostUiArr;
        for (var index = 0; index < 5; index++) {
            for (var j = 0; j < 5; j++) {
                showTextCostUiArr[index][j].setString("");
            }
        }
    },

    resetShowFishUiDate: function () {
        var showTextUiArr = this.showTextUiArr;
        for (var index = 0; index < 6; index++) {
            for (var j = 0; j < 5; j++) {
                showTextUiArr[index][j].setString("");
            }
        }
    },

    resetShowDetaileUiDate: function () {
        var showTextDetaileUiArr = this.showDetaileFishUiArr;
        for (var index = 0; index < 6; index++) {
            for (var j = 0; j < 5; j++) {
                showTextDetaileUiArr[index][j].setString("");
            }
        }
    },

    updateItemReord: function (data) {
        var self = this;
        switch (self._showInfoMod) {
            case 0:
                self.resetShowCostUiDate();
                //流水
                for (var i = (self._pageIndex - 1) * 5; i < (self._pageIndex - 1) * 5 + 5; i++) {
                    // if(i > data.length - 1) {
                    //     if(i % 3 == 2) {
                    //         self._noDataPrompt.setVisible(true);
                    //         self._noDataPrompt.setPosition(self.baseBg.width / 2,self.baseBg.height - 400);
                    //     }
                    //     else {
                    //         self._noDataPrompt.setVisible(true);
                    //         self._noDataPrompt.setPosition(self.baseBg.width / 2,self.baseBg.height - 315);
                    //     }
                    //     break;
                    // }
                    if (data[i]) {
                        // 订单号
                        var strOrderID = data[i].strOrderID;
                        // 金额(为兼容金币和钱，这里用Double)
                        var dMondy = data[i].dMondy;
                        // 时间
                        var strTime = data[i].strTime;
                        // 类型,0:登录上分   1:独立上分   2:独立下分
                        var nPayType = data[i].nPayType;
                        //刷新对应的数据
                        self.showTextCostUiArr[0][i % 5].setString(strOrderID);
                        self.showTextCostUiArr[1][i % 5].setString("CNY");
                        self.showTextCostUiArr[2][i % 5].setString(dMondy);
                        if (nPayType == 0 || nPayType == 1) {
                            self.showTextCostUiArr[3][i % 5].setString("存款");
                        }
                        else {
                            self.showTextCostUiArr[3][i % 5].setString("取款");
                        }
                        self.showTextCostUiArr[4][i % 5].setString(strTime);
                    }
                }
                break;
            case 1:
                self.resetShowFishUiDate();
                //捕鱼收益
                for (var i = (self._pageIndex - 1) * 5; i < (self._pageIndex - 1) * 5 + 5; i++) {
                    if (i > data.length - 1) {
                        self.showTextUiArr[6][i % 5].setVisible(false);
                        continue;
                    }
                    // 记录id
                    var strOrderID = data[i].strOrderID;
                    // 房间编号
                    var strRoomID = data[i].strRoomID;
                    // 消耗
                    var nConsume = data[i].nConsume;
                    // 获得
                    var nGet = data[i].nGet;
                    //盈利
                    var reward = nGet - nConsume;
                    //进出房间时间(格式：进时间|出时间)
                    var strInOutTime = data[i].strInOutTime.split("|");

                    //刷新对应的数据
                    self.showTextUiArr[0][i % 5].setString(strRoomID);
                    self.showTextUiArr[1][i % 5].setString(nConsume);
                    self.showTextUiArr[2][i % 5].setString(nGet);
                    self.showTextUiArr[3][i % 5].setString(reward);
                    self.showTextUiArr[4][i % 5].setString(strInOutTime[0].replace(/ /g, "\n  "));
                    self.showTextUiArr[5][i % 5].setString(strInOutTime[1].replace(/ /g, "\n  "));

                    //处理详情按钮逻辑
                    self.showTextUiArr[6][i % 5].setVisible(true);
                    self.showTextUiArr[6][i % 5].setTag(strOrderID);
                    self.showTextUiArr[6][i % 5].setName(data[i].strInOutTime + "|" + strRoomID);
                }
                break;
            case 2:
                self.resetShowDetaileUiDate();
                //收益详情
                for (var i = (self._pageIndex - 1) * 5; i < (self._pageIndex - 1) * 5 + 5; i++) {
                    if (data[i]) {
                        // 单号（logid）
                        var strOrderID = data[i].strOrderID;
                        // 时间
                        var strTime = data[i].strTime;
                        // 子弹价值
                        var nBulletValue = data[i].nBulletValue;
                        // 盈利
                        var nGet = data[i].nGet;
                        // 鱼id
                        var nFishID = data[i].nFishID;
                        //刷新对应的数据
                        self.showDetaileFishUiArr[0][i % 5].setString(strOrderID);
                        self.showDetaileFishUiArr[1][i % 5].setString(strTime.replace(/ /g, "\n   "));
                        self.showDetaileFishUiArr[3][i % 5].setString(nGet);
                        //倍场
                        self.showDetaileFishUiArr[4][i % 5].setString(cf.Data.ROOM_DATA[self._roomId].room_name);

                        if(nFishID != 0){
                            // if(nBulletValue <= 5){
                            //     self.showDetaileFishUiArr[2][i % 5].setString("0");
                            //     if(nBulletValue == 0){
                            //         self.showDetaileFishUiArr[5][i % 5].setString("铁炮金枪");
                            //     }else if(nBulletValue == 1){
                            //         self.showDetaileFishUiArr[5][i % 5].setString("万箭穿心");
                            //     }else if(nBulletValue == 2){
                            //         self.showDetaileFishUiArr[5][i % 5].setString("地裂天崩");
                            //     }else if(nBulletValue == 3){
                            //         self.showDetaileFishUiArr[5][i % 5].setString("玄冰连爆");
                            //     }else if(nBulletValue == 4){
                            //         self.showDetaileFishUiArr[5][i % 5].setString("凰焰焚海");
                            //     }else if(nBulletValue == 5){
                            //         self.showDetaileFishUiArr[5][i % 5].setString("龙雷落");
                            //     }
                            // }else{
                                self.showDetaileFishUiArr[2][i % 5].setString(nBulletValue);
                                self.showDetaileFishUiArr[5][i % 5].setString(cf.Data.FISH_DATA[nFishID].fishName);
                          //  }
                        
                            
                        }else{
                            self.showDetaileFishUiArr[2][i % 5].setString("倍率: "+nBulletValue);
                            self.showDetaileFishUiArr[5][i % 5].setString("宝物库开奖");
                        }

                        if(nBulletValue == 1003 || nBulletValue == 1004 && nFishID ==1){

                            self.showDetaileFishUiArr[2][i % 5].setString(0);
                            self.showDetaileFishUiArr[5][i % 5].setString("玉玺彩金");

                        }
                    }
                }
                break;
        }
    },

    /**
     * 移除监听
     */
    removeListener: function () {
        this._super();
        cc.log("NotificationLayer:removeListener");
        //移除网络监听
        var len = this._onNetMsgListener.length;
        for (var i = 0; i < len; ++i) {
            cf.removeListener(this._onNetMsgListener[i]);
        }
    },

    hide: function () {
        this._super();
    },

    onClear: function () {
        this._super();
    }
});

