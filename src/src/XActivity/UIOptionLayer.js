var UIOptionLayer = BaseLayer.extend({
    _layerBg: null,
    _sliderBar: null,
    ctor: function () {
        this._super();

        var self = this;

        //遮罩层
        var maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        self.addChild(maskLayer, -100);

        self._initLayerBoard();
        self._initSlider();
        self._initGameCheckUi();
        self.setTouchEnabled(true);
    },

    /**
     * 初始化面板
     * @private
     */
    _initLayerBoard: function () {
        var self = this;
        //背景
        self._layerBg = new cc.Scale9Sprite("resource_036_9.png");
        self._layerBg.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        self._layerBg.setContentSize(614, 420);
        self.addChild(self._layerBg);

        //标题文字
        // var titleLabel = new ccui.LabelBMFont(cf.Language.getText("text_1390"), Res.LOBBY.font_resource_093);
        // titleLabel.setPosition(self._layerBg.width/2, self._layerBg.height-40);
        // titleLabel.setScale(1.3);
        // self._layerBg.addChild(titleLabel);
        var titleLabel = new cc.Sprite("#title_setting.png")
        titleLabel.setPosition(self._layerBg.width/2+20, self._layerBg.height-10);
        self._layerBg.addChild(titleLabel);
    },

    /**
     * 初始化滑动条
     * @private
     */
    _initSlider: function () {
        var self = this;
        //创建滑动条
        self._sliderBar = [];
        var btnInfo = [{text: "text_1096", val: cf.SoundMgr.getEffectVolume()}, {
            text: "text_1097",
            val: cf.SoundMgr.getMusicVolume()
        }];
        for (var i = 0; i < 2; ++i) {
            //滑动条名称
            var title = new ccui.LabelBMFont(cf.Language.getText(btnInfo[i].text), Res.LOBBY.font_resource_093);
            title.setAnchorPoint(0, 0.5);
            title.setPosition(60, self._layerBg.height - 150 - i * 60);
            self._layerBg.addChild(title);

            //滑动条背景
            var loadBar = new cc.Scale9Sprite("resource_024_9.png");
            loadBar.setAnchorPoint(0, 0.5);
            loadBar.setPosition(title.x + 110, title.y);
            loadBar.setContentSize(360, 28);
            self._layerBg.addChild(loadBar, 0);

            //滑动条，获取本地记录音乐、音效值
            var per = btnInfo[i].val;
            var musicBar = new ccui.Slider();
            musicBar.setScale9Enabled(true);
            musicBar.loadBarTexture("resource_024_9.png", 1);
            musicBar.loadSlidBallTextures("quit_002.png", null, null, 1);
            musicBar.loadProgressBarTexture("resource_025_9.png", 1);
            musicBar.setCapInsetsBarRenderer(cc.rect(10, 13, 23, 4));
            musicBar.setCapInsetProgressBarRenderer(cc.rect(8, 13, 27, 4));
            musicBar.setContentSize(350, 30);
            musicBar.getVirtualRenderer().setOpacity(0);
            musicBar.setAnchorPoint(0, 0.5);
            musicBar.setPosition(title.x + 108, title.y);
            musicBar.setPercent(per);
            musicBar.setTag(UIOptionLayer.Btn.SOUND + i);
            musicBar.addEventListener(this._menuCallBack, this);
            self._layerBg.addChild(musicBar, 0);

            self._sliderBar[UIOptionLayer.Btn.SOUND + i] = musicBar;
        }
    },

    _initGameCheckUi: function () {
        //渔场内处理
        var btnCancel = new ccui.Button("button_005.png", null, null, 1);
        btnCancel.setPressedActionEnabled(true);
        btnCancel.setPosition(this._layerBg.width/2, 70);
        btnCancel.setTitleOffset(0, 0);
        btnCancel.setTitleText("确定");
        btnCancel.setTitleFontName(cf.Language.FontName);
        btnCancel.setTitleFontSize(30);
        btnCancel.setTag(UIOptionLayer.Btn.CANCEL);
        var title = btnCancel.getTitleRenderer();
        title.enableStroke(cf.BtnColor.YELLOW,2);
        this._layerBg.addChild(btnCancel);
        btnCancel.addTouchEventListener(this._menuCallBack, this);
    },

    /**
     * 按钮回调函数
     * @param sender
     * @param type
     * @private
     */
    _menuCallBack: function (sender, type) {
        var self = this;
        var tag = sender.getTag();
        if (type == ccui.Widget.TOUCH_ENDED) {
            cf.SoundMgr.playEffect(8);
            switch (tag) {
                case UIOptionLayer.Btn.CANCEL://大厅取消
                    self.hide();
                    if(cf.SceneMgr.getCurSceneFlag()== cf.SceneFlag.GAME){
                        cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UI_CONTINUEGAME);
                    }
                    break;
            }
        } else if (type == ccui.Slider.EVENT_PERCENT_CHANGED) {
            switch (tag) {
                case UIOptionLayer.Btn.SOUND://音效
                    var soundVolume = this._sliderBar[UIOptionLayer.Btn.SOUND].getPercent();
                    cf.SoundMgr.setEffectVolume(soundVolume);
                    break;
                case UIOptionLayer.Btn.MUSIC://音乐
                    var musicVolume = this._sliderBar[UIOptionLayer.Btn.MUSIC].getPercent();
                    cf.SoundMgr.setMusicVolume(musicVolume);
                    break;
            }
        }
    },

    /**
     * 记录音乐和音效
     * @private
     */
    _saveSound: function () {
        cf.SaveDataMgr.setNativeData("sound", cf.SoundMgr.getEffectVolume());
        cf.SaveDataMgr.setNativeData("music", cf.SoundMgr.getMusicVolume());
    },

    hide: function () {
        cf.layerHasShowInGame = false;
        this._super();
        this._saveSound();
    },

    onClear: function () {
        this._super();
        this._saveSound();
    }
});

/**
 * 按钮标记
 */
UIOptionLayer.Btn = {
    CANCEL: 0,          //< 取消


    SOUND: 30,          //< 音效
    MUSIC: 31,          //< 音乐


    MAX: 998
};
