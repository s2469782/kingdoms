var UIGeneralDlg = BaseLayer.extend({
    _target: null,
    _callback: null,
    _type: null,
    _spBK: null,
    _localPlayerModel: null,
    _inputBox: null,
    _inputBoxEx: null,
    _inputText: null,
    _onNetMsgListener: null,
    _nHeadIndex: -1,
    _nItemId: 0,
    _btnOK: null,

    init: function (type, itemId, target, callback) {
        var self = this;
        self._type = type;
        self._callback = callback;
        self._target = target;
        self._nItemId = itemId;
        //遮罩
        var maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        self.addChild(maskLayer);

        //获取玩家数据
        self._localPlayerModel = cf.PlayerMgr.getLocalPlayer();

        //添加背景
        var spBk = self._spBK = new cc.Scale9Sprite("resource_036_9.png");
        spBk.setContentSize(500, 350);
        spBk.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        self.addChild(spBk);

        var strTitle = "";
        switch (type) {
            case cf.DlgType.NICKNAME_DLG://修改昵称
                spBk.setContentSize(580, 420);
                self._initNickname();
                strTitle = cf.Language.getText("text_1395");
                break;
            case cf.DlgType.ROLE_INFO_DLG:
                spBk.setContentSize(580, 420);
                self._initRoleInfo();
                strTitle = cf.Language.getText("text_1396");
                break;
            case cf.DlgType.HEAD_DLG:
                spBk.setContentSize(690, 540);
                self._initHead();
                strTitle = cf.Language.getText("text_1399");
                break;
            case cf.DlgType.SELL_DLG:
            case cf.DlgType.SELL_GAME_DLG:
                spBk.setContentSize(600, 440);
                self._initSell();
                strTitle = cf.Language.getText("text_1394");
                break;
            case cf.DlgType.SEND_DLG:
                spBk.setContentSize(600, 520);
                self._initSend();
                strTitle = cf.Language.getText("text_1403");
                break;
            case cf.DlgType.CODE_DLG://兑换码
                spBk.setContentSize(510, 320);
                self._initCode();
                strTitle = cf.Language.getText("text_1045");
                break;
            case cf.DlgType.SUPPORT_DLG:
                spBk.setContentSize(580, 420);
                self._initSupport();
                strTitle = cf.Language.getText("text_1210");
                break;
            case cf.DlgType.SIGNATURE:
                spBk.setContentSize(580, 420);
                self._initSignature();
                strTitle = cf.Language.getText("text_1522");
                break;
        }

        var titleLabel = new ccui.Text(strTitle, cf.Language.FontName, 30);
        titleLabel.setPosition(spBk.width / 2, spBk.height - 40);
        titleLabel.setTextColor(cf.TitleColor.FONT_2);
        titleLabel.enableOutline(cf.TitleColor.OUT_2, cf.TitleColor.SIZE);
        spBk.addChild(titleLabel);

        //关闭按钮
        var closeBtn = new ccui.Button();
        closeBtn.loadTextureNormal("resource_021.png", 1);
        closeBtn.setPosition(spBk.width - 60, spBk.height - 60);
        closeBtn.addTouchEventListener(self._btnCallBack, self);
        closeBtn.setTag(UIGeneralDlg.Btn.CLOSE);
        closeBtn.setScale(0.8);
        spBk.addChild(closeBtn);

        //确定按钮
        var okBtn = self._btnOK = new ccui.Button();
        okBtn.loadTextureNormal("button_005.png", 1);
        okBtn.setTitleText(cf.Language.getText("text_1098"));
        okBtn.setTitleFontSize(32);
        okBtn.setTitleFontName(cf.Language.FontName);
        var title01 = okBtn.getTitleRenderer();
        title01.enableStroke(cf.BtnColor.YELLOW, 2);
        okBtn.setPosition(spBk.width / 2, 80);
        okBtn.addTouchEventListener(self._btnCallBack, self);
        okBtn.setTag(UIGeneralDlg.Btn.OK);
        okBtn.setScale(0.9);
        spBk.addChild(self._btnOK);

        //开启触摸
        self.setTouchEnabled(true);
        return true;
    },

    addListener: function () {
        this._super();
        var self = this;

        //创建网络消息
        var msgDataArray = null;
        if(self._type == cf.DlgType.SELL_GAME_DLG){
            msgDataArray = [              
                //出售
                {msgId: MessageCode.CH_SellItemMsg_Res, callFunc: self._resSellItemWithGame}
            ];
        }else{
            msgDataArray = [
                //修改个人信息（电话）
                {msgId: MessageCode.CH_ModifyPlayerinfoMsg_Res, callFunc: self._resEditInfo},
                //修改昵称
                {msgId: MessageCode.CH_ModifyNickname_Res, callFunc: self._resEditName},
                //检测玩家id
                {msgId: MessageCode.CS_playerIsExistsMsg_Res, callFunc: self._resCheckPlayerId},
                //赠送
                {msgId: MessageCode.CS_FriendsGiftsMsg_Res, callFunc: self._resSendItem},
                //出售
                {msgId: MessageCode.CH_SellItemMsg_Res, callFunc: self._resSellItem},
                //兑换码
                {msgId: MessageCode.CS_UseDuiHuanCode_Res, callFunc: self._resCodeGift},
                //修改签名
                {msgId: MessageCode.CH_PlayerSignature_Res, callFunc: self._resEditSignature},
                //道具使用
                {msgId: MessageCode.CH_UseItem_Res, callFunc: self._resUseItem}
            ];
        }
        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener.push(cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this)));
        }
    },

    removeListener: function () {
        this._super();
        //移除网络监听
        var len = this._onNetMsgListener.length;
        for (var i = 0; i < len; ++i) {
            cf.removeListener(this._onNetMsgListener[i]);
        }
    },

    _btnCallBack: function (sender, type) {
        var self = this;

        if (type == ccui.Widget.TOUCH_ENDED) {
            switch (sender.tag) {
                case UIGeneralDlg.Btn.OK:
                    switch (self._type) {
                        case cf.DlgType.NICKNAME_DLG://昵称修改
                            self._updateNickname();
                            break;
                        case cf.DlgType.ROLE_INFO_DLG://修改信息
                            self._updateRoleInfo();
                            break;
                        case cf.DlgType.HEAD_DLG://修改头像
                            self._askEditInfo(self._nHeadIndex, self._localPlayerModel.getPhone());
                            break;
                        case cf.DlgType.SELL_DLG://出售道具
                        case cf.DlgType.SELL_GAME_DLG:
                            self._askSellItem();
                            break;
                        case cf.DlgType.SEND_DLG://赠送道具
                            self._askSendItem();
                            break;
                        case cf.DlgType.CODE_DLG://兑换码
                            self._askCodeGift();
                            break;
                        case cf.DlgType.SUPPORT_DLG://客服
                            self.removeFromParent(true);
                            break;
                        case cf.DlgType.SIGNATURE://签名
                            self._updateSignature();
                            break;
                    }
                    break;
                case UIGeneralDlg.Btn.CLOSE:
                    self.removeFromParent(true);
                    break;
                case UIGeneralDlg.Btn.ORDER:
                    cf.AskOrderTip = true;
                    SDKHelper.askOrderList(this);

                    var _time = 0;
                    var timeCallBack = function (dt) {
                        _time += dt;
                        var time = 10 - Math.floor(_time);

                        if (time <= 0) {
                            sender.unschedule(timeCallBack);
                            sender.setTouchEnabled(true);
                            sender.setColor(cc.color.WHITE);
                            sender.setTitleFontSize(32);
                            sender.setTitleText(cf.Language.getText("text_1461"));
                        } else {
                            sender.setTitleFontSize(24);
                            sender.setTitleText(cf.Language.getText("text_1461")+"(" + time + "s)");
                        }
                    };

                    sender.schedule(timeCallBack, 1);
                    sender.setTouchEnabled(false);
                    sender.setColor(cc.color(150, 150, 150));
                    break;
            }
        }
    },

    ////////////////////////////////////////////////////////////
    //昵称相关
    ////////////////////////////////////////////////////////////
    /**
     * 初始化修改昵称UI
     * @private
     */
    _initNickname: function () {
        var self = this;

        var size = self._spBK.getContentSize();

        var strLabel = new ccui.Text(cf.Language.getText("text_1061"), cf.Language.FontName, 26);
        strLabel.setAnchorPoint(0, 0.5);
        strLabel.setPosition(50, size.height / 2 + 30);
        strLabel.setTextColor(cc.color(106, 234, 244));
        self._spBK.addChild(strLabel);

        //输入框
        var editBox;
        if(cc.sys.isNative) {
            editBox = cc.Scale9Sprite.createWithSpriteFrameName("resource_067_9.png");
        }
        else {
            editBox = new cc.Scale9Sprite("resource_067_9.png");
        }
        editBox.setPosition(-5, 0);
        editBox.setContentSize(320, 48);

        var inputBox = self._inputBox = new cc.EditBox(cc.size(343, 56), editBox);
        inputBox.setPosition(size.width / 2 + 65, size.height / 2 + 30);
        inputBox.setFontSize(25);
        inputBox.setPlaceHolder(cf.Language.getText("text_1068"));
        inputBox.setPlaceholderFontSize(25);
        inputBox.setMaxLength(20);
        inputBox.setDelegate(this);
        inputBox.setReturnType(cc.KEYBOARD_RETURNTYPE_DONE);
        inputBox.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        inputBox.setInputFlag(cc.EDITBOX_INPUT_FLAG_SENSITIVE);

        self._spBK.addChild(self._inputBox);

        //提示        
        var editNum = self._localPlayerModel.getModifyNickname();
        var costData = cf.Data.COST_DATA;
        var ItemData = cf.Data.ITEM_DATA;
        var strTip = "";
        if (editNum < 1) {
            strTip = cf.Language.getText("text_1066");
        }
        else {
            strTip = cf.Language.getText("text_1153") + costData[1].costNum + ItemData[costData[1].costType].itemName;
        }
        var tipLabel = new ccui.Text("(" + strTip + ")", cf.Language.FontName, 22);
        tipLabel.setPosition(size.width / 2, size.height / 2 - 30);
        tipLabel.setTextColor(cc.color.YELLOW);
        self._spBK.addChild(tipLabel);
    },

    /**
     * 更新昵称逻辑
     * @private
     */
    _updateNickname: function () {
        var self = this;
        var inputText = self._inputBox.getString();

        if (inputText == "") {
            cf.UITools.showHintToast(cf.Language.getText("text_1136"));
        }
        else if (inputText == self._localPlayerModel.getNickName()) {
            cf.UITools.showHintToast(cf.Language.getText("text_1216"));
        } else {
            //昵称是否小于4位字符
            if (inputText.length < 4) {
                cf.UITools.showHintToast(cf.Language.getText("text_1083"));
            }
            //昵称是否大于8位字符
            else if (inputText.length > 12) {
                cf.UITools.showHintToast(cf.Language.getText("text_1084"));
            }
            else {
                //昵称修改成功
                self._inputText = inputText;
                self._askEditName(inputText);
            }
        }
    },

    /**
     * 初始化签名
     * @private
     */
    _initSignature: function () {
        var self = this;

        var size = self._spBK.getContentSize();

        var strLabel = new ccui.Text(cf.Language.getText("text_1524"), cf.Language.FontName, 26);
        strLabel.setAnchorPoint(0, 0.5);
        strLabel.setPosition(50, size.height / 2 + 30);
        strLabel.setTextColor(cc.color(106, 234, 244));
        self._spBK.addChild(strLabel);

        //输入框
        var editBox;
        if(cc.sys.isNative) {
            editBox = cc.Scale9Sprite.createWithSpriteFrameName("resource_067_9.png");
        }
        else {
            editBox = new cc.Scale9Sprite("resource_067_9.png");
        }
        editBox.setPosition(-5, 0);
        editBox.setContentSize(320, 48);

        var inputBox = self._inputBox = new cc.EditBox(cc.size(343, 56), editBox);
        inputBox.setPosition(size.width / 2 + 35, size.height / 2 + 30);
        inputBox.setFontSize(25);
        inputBox.setPlaceHolder(cf.Language.getText("text_1523"));
        inputBox.setPlaceholderFontSize(25);
        inputBox.setMaxLength(30);
        inputBox.setDelegate(this);
        inputBox.setReturnType(cc.KEYBOARD_RETURNTYPE_DONE);
        inputBox.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
        inputBox.setInputFlag(cc.EDITBOX_INPUT_FLAG_SENSITIVE);

        self._spBK.addChild(self._inputBox);
    },

    /**
     * 更新签名逻辑
     * @private
     */
    _updateSignature: function () {
        var self = this;
        var inputText = self._inputBox.getString();

        if (inputText == "") {
            cf.UITools.showHintToast(cf.Language.getText("text_1136"));
        }
        else if (inputText == self._localPlayerModel.getSignature()) {
            cf.UITools.showHintToast(cf.Language.getText("text_1216"));
        } else {
            //昵称是否大于30位字符
            if (inputText.length > 30) {
                cf.UITools.showHintToast(cf.Language.getText("text_1084"));
            }
            else {
                //签名修改成功
                self._inputText = inputText;
                self._askEditSignature(inputText);
            }
        }
    },

    /**
     * 请求修改昵称
     * @param inputText
     * @private
     */
    _askEditName: function (inputText) {
        var self = this;
        cf.UITools.showLoadingToast(cf.Language.getText("text_1012"), 10, false, this);
        var idLogin = self._localPlayerModel.getIdLogin();
        var safeCode = self._localPlayerModel.getSafeCode();
        var msg = new $root.CH_ModifyNickname();
        msg.nidLogin = idLogin;
        msg.strNickname = inputText;
        msg.strSafeCode = safeCode;
        var wr = $root.CH_ModifyNickname.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_ModifyNickname, wr);
    },

    /**
     * 获取昵称修改
     * @param msg
     * @private
     */
    _resEditName: function (msg) {
        var self = this;
        cf.UITools.hideLoadingToast();
        //修改成功
        cf.UITools.showHintToast(cf.Language.getText("text_1086"));
        self._localPlayerModel.setNickName(self._inputText);
        var costData = cf.Data.COST_DATA;
        var editNum = self._localPlayerModel.getModifyNickname();
        if (editNum < 1) {
            self._localPlayerModel.setModifyNickname(1);
        } else {
            //self._localPlayerModel.setLottery(self._localPlayerModel.getLottery() - parseInt(costData[1].costNum));
            self._localPlayerModel.setGoldNum(self._localPlayerModel.getGoldNum()-parseInt(costData[1].costNum));
        }

        //刷新大厅UI
        cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UI_UPDATE);
        self._callback.call(self._target);
        self.removeFromParent(true);
    },

    /**
     * 请求修改签名
     * @param inputText
     * @private
     */
    _askEditSignature: function (inputText) {
        cf.UITools.showHintToast(cf.Language.getText("text_1086"));
        var msg = new $root.CH_PlayerSignature();
        msg.strSignature = inputText;
        var wr = $root.CH_PlayerSignature.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_ModifySignature, wr);
    },

    /**
     * 获取昵称修改
     * @param msg
     * @private
     */
    _resEditSignature: function (msg) {
        var self = this;
        cf.UITools.hideLoadingToast();
        //修改成功
        var resMsg = $root.CH_PlayerSignature_Res.decode(msg);
        if(resMsg) {
            switch (resMsg.nRes){
                case 0:
                    //修改成功
                    self._localPlayerModel.setSignature(self._inputText);
                    cf.UITools.showHintToast(cf.Language.getText("text_1086"));
                    self._callback.call(self._target);
                    self.removeFromParent();
                    break;
            }
        }
    },

    ////////////////////////////////////////////////////////////
    //个人信息相关
    ////////////////////////////////////////////////////////////
    /**
     * 初始化个人信息
     * @private
     */
    _initRoleInfo: function () {
        var self = this;

        var size = self._spBK.getContentSize();

        var strLabel = new ccui.Text(cf.Language.getText("text_1397"), cf.Language.FontName, 26);
        strLabel.setAnchorPoint(0, 0.5);
        strLabel.setPosition(50, size.height / 2 + 30);
        strLabel.setTextColor(cc.color(106, 234, 244));
        self._spBK.addChild(strLabel);

        //输入框
        var editBox;
        if(cc.sys.isNative) {
            editBox = cc.Scale9Sprite.createWithSpriteFrameName("resource_067_9.png");
        }
        else {
            editBox = new cc.Scale9Sprite("resource_067_9.png");
        }
        editBox.setPosition(-5, 0);
        editBox.setContentSize(320, 48);

        var inputBox = self._inputBox = new cc.EditBox(cc.size(343, 56), editBox);
        inputBox.setPosition(size.width / 2 + 65, size.height / 2 + 30);
        inputBox.setFontSize(25);
        inputBox.setPlaceHolder(cf.Language.getText("text_1080"));
        inputBox.setPlaceholderFontSize(25);
        inputBox.setMaxLength(11);
        inputBox.setDelegate(this);
        inputBox.setInputMode(cc.EDITBOX_INPUT_MODE_PHONENUMBER);
        inputBox.setReturnType(cc.KEYBOARD_RETURNTYPE_DONE);

        self._spBK.addChild(self._inputBox);

        var strPhone = self._localPlayerModel.getPhone();
        var strTip = cf.Language.getText("text_1082") + cf.Language.getText("text_1398");
        if (strPhone != null && strPhone != "") {
            strTip = cf.Language.getText("text_1082") + strPhone;
        }
        var tipLabel = new ccui.Text("(" + strTip + ")", cf.Language.FontName, 22);
        tipLabel.setPosition(size.width / 2, size.height / 2 - 30);
        tipLabel.setTextColor(cc.color.YELLOW);
        self._spBK.addChild(tipLabel);
    },

    /**
     * 更新个人信息
     * @private
     */
    _updateRoleInfo: function () {
        var self = this;
        var inputText = self._inputBox.getString();
        //检测是否未做操作
        if (inputText == "") {
            cf.UITools.showHintToast(cf.Language.getText("text_1136"));
        } else {
            //检测是否满11位电话号
            var regNumber = /^1[3456789][0-9]{9}$/;
            if (!regNumber.test(inputText)) {
                cf.UITools.showHintToast(cf.Language.getText("text_1081"));
                return;
            }
            self._inputText = inputText;
            //发送修改电话号消息
            self._askEditInfo(self._localPlayerModel.getHeadId(), inputText);
        }
    },

    /**
     * 请求个人信息
     * @param nHeadId
     * @param strPhone
     * @private
     */
    _askEditInfo: function (nHeadId, strPhone) {
        var self = this;
        cf.UITools.showLoadingToast(cf.Language.getText("text_1012"), 10, false, this);
        var msg = new $root.CH_ModifyPlayerInfo();
        msg.nidLogin = self._localPlayerModel.getIdLogin();
        msg.nSex = self._localPlayerModel.getSex();
        msg.nHeadType = nHeadId;
        msg.strSafeCode = self._localPlayerModel.getSafeCode();
        msg.strPhone = strPhone;
        var wr = $root.CH_ModifyPlayerInfo.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_ModifyPlayerinfoMsg, wr);
    },

    /**
     * 获取个人信息结果
     * @param msg
     * @private
     */
    _resEditInfo: function (msg) {
        var self = this;
        //隐藏loading
        cf.UITools.hideLoadingToast();
        cf.UITools.showHintToast(cf.Language.getText("text_1086"));
        //修改信息成功
        if (self._nHeadIndex != -1) {
            var data = cf.Data.HEADPORTRAIT_DATA;
            var headId = data[self._nHeadIndex].index;
            self._localPlayerModel.setHeadId(headId);
        }
        if(self._inputText == null){
            self._localPlayerModel.setPhone("");
        }
        else{

            self._localPlayerModel.setPhone(self._inputText );
        }
        //刷新大厅UI
        cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UI_UPDATE);
        self._callback.call(self._target);
        self.removeFromParent(true);
    },

    ////////////////////////////////////////////////////////////
    //头像相关
    ////////////////////////////////////////////////////////////
    /**
     * 初始化头像界面
     * @private
     */
    _initHead: function () {
        var self = this;
        self._nHeadIndex = self._localPlayerModel.getHeadId();
        var headData = cf.Data.HEADPORTRAIT_DATA;

        var size = self._spBK.getContentSize();
        var _count = 0;
        var _offsetH = 120;
        for (var i in headData) {
            var _idx = headData[i].index;
            if (_idx > 10) {
                _count = 11;
                _offsetH = -50;
            }
            var headIconBtn = new ccui.Button(headData[i].headIcon, headData[i].headIcon, headData[i].headIcon, ccui.Widget.PLIST_TEXTURE);
            var width = headIconBtn.width;

            headIconBtn.setPosition((size.width - (width + 20) * 2) / 2 + (_idx - _count) * (width + 20), size.height / 2 - 25 + _offsetH);
            headIconBtn.addTouchEventListener(self._headCallBack, self);
            headIconBtn.setTag(_idx);
            self._spBK.addChild(headIconBtn);

            //选择框
            var spSelect = new cc.Sprite(ActionMgr.getFrame("resource_036.png"));
            spSelect.setPosition(headIconBtn.width / 2, headIconBtn.height / 2);
            if (self._nHeadIndex != _idx)
                spSelect.setVisible(false);
            spSelect.setScale(1.3);
            spSelect.setColor(cc.color.YELLOW);
            spSelect.setTag(1);
            headIconBtn.addChild(spSelect);
        }
    },

    /**
     * 头像按钮回调
     * @param sender
     * @param type
     * @private
     */
    _headCallBack: function (sender, type) {
        var self = this;

        if (type == ccui.Widget.TOUCH_ENDED) {
            var tag = sender.tag;
            var headData = cf.Data.HEADPORTRAIT_DATA;
            for (var i in headData) {
                var idx = headData[i].index;
                var headBtn = self._spBK.getChildByTag(idx);
                if (headBtn) {
                    var spSelect = headBtn.getChildByTag(1);
                    if (spSelect) {
                        spSelect.setVisible(false);
                        if (tag == idx) {
                            spSelect.setVisible(true);
                            self._nHeadIndex = tag;
                        }
                    }
                }
            }
        }
    },

    ////////////////////////////////////////////////////////////
    //出售相关
    ////////////////////////////////////////////////////////////
    _createItem: function () {
        var self = this;
        var size = self._spBK.getContentSize();
        var itemData = cf.Data.ITEM_DATA[self._nItemId];

        //道具背景
        var itemBK = new cc.Sprite("#resource_103.png");
        itemBK.setAnchorPoint(1, 1);
        itemBK.setPosition(size.width / 2 - 20, size.height - 73);
        itemBK.setScale(1.1);
        self._spBK.addChild(itemBK);

        //道具icon
        var spIcon = new cc.Sprite("#" + itemData.itemIcon);
        spIcon.setPosition(itemBK.width / 2, itemBK.height / 2);
        itemBK.addChild(spIcon);

        var _offsetX = -20, _offsetY = 0;
        //道具名称
        var nameLabel = new ccui.Text(itemData.itemName, cf.Language.FontName, 30);
        nameLabel.setAnchorPoint(0, 1);
        nameLabel.setPosition(size.width / 2 + _offsetX, size.height - 80);
        self._spBK.addChild(nameLabel);

        var strPrice = (itemData.price).toString().split(",");
        var strNum = strPrice[0] + cf.Language.getText("text_1170");
        if (strPrice.length > 1 && parseInt(strPrice[0]) < parseInt(strPrice[1])) {
            strNum = cf.numberToW(strPrice[0]) + "~" + cf.numberToW(strPrice[1]) + cf.Language.getText("text_1170");
        }
        //道具价格
        if (strPrice[0] <= 0) {
            _offsetY = -30;
        } else {
            var amountLabel = new ccui.RichText();
            amountLabel.pushBackElement(new ccui.RichElementText(0, cc.color.WHITE, 255,
                cf.Language.getText("text_1033"), cf.Language.FontName, 24));
            amountLabel.pushBackElement(new ccui.RichElementText(0, cc.color.YELLOW, 255,
                strNum, cf.Language.FontName, 24));
            amountLabel.formatText();
            amountLabel.setPosition((size.width + amountLabel.width) / 2 + _offsetX, size.height - 195);
            self._spBK.addChild(amountLabel);
        }

        //道具数量
        var _itemNum = self._localPlayerModel.getPlayerItemNum(self._nItemId);
        var numLabel = new ccui.RichText();
        numLabel.pushBackElement(new ccui.RichElementText(0, cc.color.WHITE, 255,
            cf.Language.getText("text_1141"), cf.Language.FontName, 24));
        numLabel.pushBackElement(new ccui.RichElementText(0, cc.color.YELLOW, 255,
            "" + _itemNum, cf.Language.FontName, 24));
        numLabel.formatText();
        numLabel.setPosition((size.width + numLabel.width) / 2 + _offsetX, size.height - 135 + _offsetY);
        self._spBK.addChild(numLabel);

        //道具最大限制
        var maxNumLabel = new ccui.RichText();
        maxNumLabel.pushBackElement(new ccui.RichElementText(0, cc.color.WHITE, 255,
            cf.Language.getText("text_1196"), cf.Language.FontName, 24));
        maxNumLabel.pushBackElement(new ccui.RichElementText(0, cc.color.YELLOW, 255,
            "" + itemData.maxNum, cf.Language.FontName, 24));
        maxNumLabel.formatText();
        maxNumLabel.setPosition((size.width + maxNumLabel.width) / 2 + _offsetX, size.height - 165 + _offsetY);
        self._spBK.addChild(maxNumLabel);


    },
    /**
     * 初始化出售界面
     * @private
     */
    _initSell: function () {
        var self = this;
        var size = self._spBK.getContentSize();

        self._createItem();

        var strLabel = new ccui.Text(cf.Language.getText("text_1400"), cf.Language.FontName, 26);
        strLabel.setAnchorPoint(0, 0.5);
        strLabel.setPosition(70, size.height / 2 - 70);
        strLabel.setTextColor(cc.color(106, 234, 244));
        self._spBK.addChild(strLabel);

        //输入框
        var editBox;
        if(cc.sys.isNative) {
            editBox = cc.Scale9Sprite.createWithSpriteFrameName("resource_067_9.png");
        }
        else {
            editBox = new cc.Scale9Sprite("resource_067_9.png");
        }
        editBox.setPosition(-3, 0);
        editBox.setContentSize(240, 38);

        var inputBox = self._inputBox = new cc.EditBox(cc.size(240, 50), editBox);
        inputBox.setPosition(size.width / 2 + 25, size.height / 2 - 70);
        inputBox.setFontSize(25);
        inputBox.setPlaceholderFontSize(22);
        inputBox.setPlaceHolder(cf.Language.getText("text_1067"));
        inputBox.setMaxLength(4);
        inputBox.setDelegate(this);
        inputBox.setInputMode(cc.EDITBOX_INPUT_MODE_DECIMAL);
        inputBox.setReturnType(cc.KEYBOARD_RETURNTYPE_DEFAULT);//设置返回类型

        self._spBK.addChild(self._inputBox);

        var strTip = new ccui.Text("(最大999)", cf.Language.FontName, 22);
        strTip.setAnchorPoint(1, 0.5);
        strTip.setPosition(size.width-45, size.height / 2 - 70);
        //strLabel.setTextColor(cc.color(106, 234, 244));
        self._spBK.addChild(strTip);

        /*
         //-按钮
         var minusBtn = new ccui.Button("button_002.png", "button_002.png", "button_002.png", ccui.Widget.PLIST_TEXTURE);
         minusBtn.setPosition(size.width/2-100, size.height / 2 - 70);
         //minusBtn.addTouchEventListener(self.menuItemCallback, self);
         //minusBtn.setTag(enGeneraBtnType.EMinusBtn);
         self._spBK.addChild(minusBtn);

         //+按钮
         var plusBtn = new ccui.Button("button_001.png", "button_001.png", "button_001.png", ccui.Widget.PLIST_TEXTURE);
         plusBtn.setPosition(size.width/2+100, size.height / 2 - 70);
         //plusBtn.addTouchEventListener(self.menuItemCallback, self);
         //plusBtn.setTag(enGeneraBtnType.EPlusBtn);
         self._spBK.addChild(plusBtn);
         */
    },

    /**
     * 请求出售
     * @private
     */
    _askSellItem: function () {
        var self = this;

        if (self._nItemId == 0) {
            cf.UITools.showHintToast(cf.Language.getText("text_1157"));
            return;
        }

        //获取数量
        var strNum = self._inputBox.getString();
        if (strNum == "")
            strNum = "0";
        var itemNum = Math.floor(strNum);
        var curNum = self._localPlayerModel.getPlayerItemNum(self._nItemId);
        if (curNum > 0) {
            if (itemNum <= 0) {
                //强制变为1
                itemNum = 1;
                self._inputBox.setString("" + itemNum);
            } else if (itemNum > 999) {
                //强制变为全部数量
                if(curNum > 999){
                    itemNum = 999;
                }else{
                    itemNum = curNum;
                }
                self._inputBox.setString("" + itemNum);
            }
        } else {
            //道具数量不足
            cf.UITools.showHintToast(cf.Language.getText("text_1156"));
            return;
        }

        var itemData = cf.Data.ITEM_DATA[self._nItemId];
        var strTip = cf.Language.getText("text_1401");
        strTip = strTip.formatEx(itemNum, itemData.itemName);
        var color = [cc.color.WHITE, cc.color.GREEN, cc.color.GREEN, cc.color.GREEN];
        var dlg = UIDialog.createCustomWithRichText(strTip, color, cf.Language.getText("text_1442"), cf.Language.getText("text_1225"), false, function (type) {
            if (type == UIDialog.Btn.RIGHT) {
                cf.UITools.showLoadingToast(cf.Language.getText("text_1012"), 10, false, this);
                var msg = new $root.CH_SellItem();
                msg.nItemID = self._nItemId;
                msg.nItemNum = itemNum;
                if(self._nItemId == cf.ItemID.MAP01 || self._nItemId == cf.ItemID.MAP02 || self._nItemId == cf.ItemID.MAP03 || self._nItemId == cf.ItemID.MAP04) {
                    msg.strSafeCode = self._localPlayerModel.getSafeCode();
                    var wr = $root.CH_SellItem.encode(msg).finish();
                    cf.NetMgr.sendSocketMsg(MessageCode.CH_SellItemMsg, wr);
                }
                else {
                    // 其他道具的使用例如宝箱
                    var wr = $root.CH_UseItem_Msg.encode(msg).finish();
                    cf.NetMgr.sendSocketMsg(MessageCode.CH_UseItem, wr);
                }
            }
        }, self);
        self.addChild(dlg);
    },

    /**
     * 获取出售消息
     * @param msg
     * @private
     */
    _resSellItem: function (msg) {
        var self = this;
        cf.UITools.hideLoadingToast();
        var resMsg = $root.CH_SellItem_Res.decode(msg);
        var itemReward = resMsg.itemReward;
        var tempReward = [];
        for (var i = 0; i < itemReward.length; i++) {
            if (self._nItemId != itemReward[i].nItemID) {
                tempReward.push(itemReward[i]);
            } else {                
                //设置数量
                self._localPlayerModel.setPlayerItem(itemReward[i].nItemID, itemReward[i].nItemNum, null, itemReward[i].nError);
            }
        }
        self._callback.call(self._target);
        self.removeFromParent(true);

        self._target.addChild(UIAwardToast.create(tempReward, function () {
        }, self._target, false), 101);
    },

    /**
     * 获取道具使用信息
     */
    _resUseItem: function (msg) {
        var self = this;
        cf.UITools.hideLoadingToast();
        var resMsg = $root.CH_UseItemRes_Msg.decode(msg);
        if(resMsg) {
            var itemReward = resMsg.itemReward;
            var tempReward = [];
            for (var i = 0; i < itemReward.length; i++) {
                if (self._nItemId != itemReward[i].nItemID) {
                    tempReward.push(itemReward[i]);
                }
                else {
                    //设置数量
                    self._localPlayerModel.setPlayerItem(itemReward[i].nItemID, itemReward[i].nItemNum, null, itemReward[i].nError);
                }
            }

            self.removeFromParent(true);
            self._target.addChild(UIAwardToast.create(tempReward, function () {
                self._callback.call(self._target);
            }, self._target, false), 101);
        }
    },

    /**
     * 游戏中获取出售消息
     * @param msg
     * @private
     */
    _resSellItemWithGame:function (msg) {
        var self = this;
        cf.UITools.hideLoadingToast();
        var resMsg = $root.CH_SellItem_Res.decode(msg);
        var itemReward = resMsg.itemReward;
        var itemId = 0, goldNum = 0;
        for (var i = 0; i < itemReward.length; i++) {
            switch (itemReward[i].nItemID){
                case cf.ItemID.GOLD:
                    goldNum = itemReward[i].nNum;
                    cf.PlayerMgr.getLocalPlayer().setGoldNum(itemReward[i].nItemNum);
                    break;
                case cf.ItemID.VIP_EXP:
                    var exp = itemReward[i].nItemNum;
                    var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
                    localPlayerModel.setVipExp(exp);

                    //计算vip等级
                    var vipLevel = localPlayerModel.getVipLevel();
                    if (vipLevel >= cf.VipMaxLv) {
                        break;
                    }
                    var levelCost = cf.Data.VIP_DATA[vipLevel].totalPay;
                    while (exp >= levelCost) {
                        localPlayerModel.setVipLevel(vipLevel + 1);
                        //isUpgrade = true;
                        vipLevel = localPlayerModel.getVipLevel();
                        levelCost = cf.Data.VIP_DATA[vipLevel].totalPay;
                        if (vipLevel >= cf.VipMaxLv) {
                            break;
                        }
                    }
                    break;
                default:
                    cf.PlayerMgr.getLocalPlayer().setPlayerItem(itemReward[i].nItemID, itemReward[i].nItemNum, null, itemReward[i].nError);
                    itemId = itemReward[i].nItemID;
                    break;
            }
        }

        //播放特效
        if(goldNum != 0 && itemId != 0){
            cf.EffectMgr.doMissileAction(itemId,goldNum);
        }
        cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UPDATE_ROOM_UI);

        self._callback.call(self._target);
        self.removeFromParent(true);
    },

    ////////////////////////////////////////////////////////////
    //赠送相关
    ////////////////////////////////////////////////////////////
    /**
     * 初始化赠送UI
     * @private
     */
    _initSend: function () {
        var self = this;
        var size = self._spBK.getContentSize();

        //创建道具
        self._createItem();

        //玩家ID
        var strLabel1 = new ccui.Text(cf.Language.getText("text_1139"), cf.Language.FontName, 26);
        strLabel1.setAnchorPoint(0, 0.5);
        strLabel1.setPosition(70, size.height / 2 - 40);
        strLabel1.setTextColor(cc.color(106, 234, 244));
        self._spBK.addChild(strLabel1);

        //输入框
        var editBox1;
        if(cc.sys.isNative) {
            editBox1 = cc.Scale9Sprite.createWithSpriteFrameName("resource_067_9.png");
        }
        else {
            editBox1 = new cc.Scale9Sprite("resource_067_9.png");
        }
        editBox1.setPosition(-3, 0);
        editBox1.setContentSize(240, 38);

        var inputBox1 = self._inputBox = new cc.EditBox(cc.size(240, 50), editBox1);
        inputBox1.setPosition(size.width / 2 + 65, size.height / 2 - 40);
        inputBox1.setFontSize(25);
        inputBox1.setPlaceholderFontSize(22);
        inputBox1.setPlaceHolder(cf.Language.getText("text_1372"));
        inputBox1.setMaxLength(11);
        inputBox1.setDelegate(this);
        inputBox1.setInputMode(cc.EDITBOX_INPUT_MODE_DECIMAL);
        inputBox1.setReturnType(cc.KEYBOARD_RETURNTYPE_DEFAULT);//设置返回类型
        self._spBK.addChild(self._inputBox);

        //赠送数量
        var strLabel2 = new ccui.Text(cf.Language.getText("text_1142"), cf.Language.FontName, 26);
        strLabel2.setAnchorPoint(0, 0.5);
        strLabel2.setPosition(70, size.height / 2 - 110);
        strLabel2.setTextColor(cc.color(106, 234, 244));
        self._spBK.addChild(strLabel2);

        //输入框
        var editBox2;
        if(cc.sys.isNative) {
            editBox2 = cc.Scale9Sprite.createWithSpriteFrameName("resource_067_9.png");
        }
        else {
            editBox2 = new cc.Scale9Sprite("resource_067_9.png");
        }
        editBox2.setPosition(-3, 0);
        editBox2.setContentSize(240, 38);

        var itemData = cf.Data.ITEM_DATA[self._nItemId];
        var strSend = cf.Language.getText("text_1404");
        strSend = strSend.format(itemData.sendNum);
        var inputBox2 = self._inputBoxEx = new cc.EditBox(cc.size(240, 50), editBox2);
        inputBox2.setPosition(size.width / 2 + 65, size.height / 2 - 110);
        inputBox2.setFontSize(25);
        inputBox2.setPlaceholderFontSize(22);
        inputBox2.setPlaceHolder(strSend);
        inputBox2.setMaxLength(4);
        inputBox2.setDelegate(this);
        inputBox2.setInputMode(cc.EDITBOX_INPUT_MODE_DECIMAL);
        inputBox2.setReturnType(cc.KEYBOARD_RETURNTYPE_DEFAULT);//设置返回类型
        self._spBK.addChild(self._inputBoxEx);

    },

    /**
     * 请求赠送道具逻辑，检测各种限制
     * @private
     */
    _askSendItem: function () {
        var self = this;

        if (self._nItemId == 0) {
            cf.UITools.showHintToast(cf.Language.getText("text_1157"));
            return;
        }

        //获取玩家id
        var playerId = self._inputBox.getString();
        if (playerId == null || playerId == "") {
            cf.UITools.showHintToast(cf.Language.getText("text_1372"));
            return;
        } else {
            if (playerId == self._localPlayerModel.getIdLogin()) {
                cf.UITools.showHintToast(cf.Language.getText("text_1134"));
                return;
            }
        }

        //获取数量
        var itemData = cf.Data.ITEM_DATA[self._nItemId];
        var strNum = self._inputBoxEx.getString();
        if (strNum == "")
            strNum = "0";
        var itemNum = Math.floor(strNum);
        var curNum = self._localPlayerModel.getPlayerItemNum(self._nItemId);
        if (curNum >= itemData.sendNum) {
            if (itemNum < itemData.sendNum) {
                //强制变为赠送基数
                itemNum = itemData.sendNum;
                self._inputBoxEx.setString("" + itemNum);
            } else if (itemNum > curNum) {
                //强制变为全部数量
                itemNum = curNum;
                self._inputBoxEx.setString("" + itemNum);
            }
        } else {
            //道具数量不足
            cf.UITools.showHintToast(cf.Language.getText("text_1156"));
            return;
        }

        //检测玩家id是否合法
        cf.UITools.showLoadingToast(cf.Language.getText("text_1012"), 10, false, this);
        var msg = new $root.CS_playerIsExistsMsg();
        msg.nIdLogin = self._localPlayerModel.getIdLogin();
        msg.strSafeCode = self._localPlayerModel.getSafeCode();
        msg.nOtherPlayerId = playerId;
        var wr = $root.CS_playerIsExistsMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_playerIsExistsMsg, wr);
    },

    /**
     * 玩家id检测
     * @param msg
     * @private
     */
    _resCheckPlayerId: function (msg) {
        var self = this;
        //隐藏loading
        cf.UITools.hideLoadingToast();

        var resMsg = $root.CS_playerIsExistsMsg_Res.decode(msg);
        if (resMsg.nSendRes == 0) {
            //合法，继续赠送逻辑
            //赠送道具数量
            var strNum = self._inputBoxEx.getString();
            var itemNum = Math.floor(strNum);
            //赠送对象id
            var playerId = self._inputBox.getString();
            //赠送对话框
            var itemData = cf.Data.ITEM_DATA[self._nItemId];
            var strTip = cf.Language.getText("text_1402");
            strTip = strTip.formatEx(playerId, itemNum, itemData.itemName);
            var color = [cc.color.WHITE, cc.color.YELLOW, cc.color.WHITE, cc.color.GREEN, cc.color.GREEN, cc.color.GREEN, cc.color.WHITE];
            var dlg = UIDialog.createCustomWithRichText(strTip, color, cf.Language.getText("text_1137"), cf.Language.getText("text_1225"), false, function (type) {
                if (type == UIDialog.Btn.RIGHT) {
                    cf.UITools.showLoadingToast(cf.Language.getText("text_1012"), 10, false, this);
                    var msg = new $root.CS_FriendsGiftsMsg();
                    msg.nIdLogin = self._localPlayerModel.getIdLogin();
                    msg.strSafeCode = self._localPlayerModel.getSafeCode();
                    msg.nReceiverId = playerId;
                    msg.strSubject = null;
                    msg.strContent = null;
                    msg.strProp = self._nItemId + "," + itemNum;
                    var wr = $root.CS_FriendsGiftsMsg.encode(msg).finish();
                    cf.NetMgr.sendSocketMsg(MessageCode.CS_FriendsGiftsMsg, wr);
                }
            }, self);
            self.addChild(dlg);

        } else {
            //不存在提示
            cf.UITools.showHintToast(cf.Language.getText("text_1143"));
        }
    },

    /**
     * 获取赠送道具消息
     * @param msg
     * @private
     */
    _resSendItem: function (msg) {
        var self = this;
        //隐藏loading
        cf.UITools.hideLoadingToast();
        cf.UITools.showHintToast(cf.Language.getText("text_1144"));

        var resMsg = $root.CS_FriendsGiftsMsg_Res.decode(msg);
        var strSave = cf.Language.getText("text_1376");
        var strItem = " ";
        var len = resMsg.itemReward.length;
        for (var i = 0; i < len; ++i) {
            var item = resMsg.itemReward[i];
            var sendNum = self._localPlayerModel.getPlayerItemNum(item.nItemID) - item.nItemNum;
            self._localPlayerModel.setPlayerItem(item.nItemID, item.nItemNum, null, 0);

            var _itemData = cf.Data.ITEM_DATA[item.nItemID];
            strItem += " [ " + _itemData.itemName + " ] X " + sendNum + "  ";
        }
        strSave = strSave.format(resMsg.nIdLogin, strItem);
        self._GiftJsonSave(strSave, cf.TimeToDate(self._localPlayerModel.getGameTime()));

        self._callback.call(self._target);
        self.removeFromParent(true);
    },

    /**
     * 保存赠送记录
     * @param sCon
     * @param sTime
     * @private
     */
    _GiftJsonSave: function (sCon, sTime) {
        var jsonInfo = cf.SaveDataMgr.getNativeData(UIEmailLayer.GiftRecord);
        var GiftRecordInfoArray = [];
        var info = {};
        if (jsonInfo == null || jsonInfo == "" || jsonInfo == "undefined") {
            info.sContent = sCon;
            info.sTime = sTime;
            GiftRecordInfoArray.push(info);
        } else {
            //将字符串转化为json
            GiftRecordInfoArray = JSON.parse(jsonInfo);
            info.sContent = sCon;
            info.sTime = sTime;
            GiftRecordInfoArray.push(info);
        }
        cf.SaveDataMgr.setNativeData(UIEmailLayer.GiftRecord, JSON.stringify(GiftRecordInfoArray));
    },

    ////////////////////////////////////////////////////////////
    //兑换码相关
    ////////////////////////////////////////////////////////////
    /**
     * 初始化兑换码界面
     * @private
     */
    _initCode: function () {
        var self = this;

        var size = self._spBK.getContentSize();

        var strLabel = new ccui.Text(cf.Language.getText("text_1045") + ":", cf.Language.FontName, 26);
        strLabel.setAnchorPoint(0, 0.5);
        strLabel.setPosition(25, size.height / 2);
        strLabel.setTextColor(cc.color(106, 234, 244));
        self._spBK.addChild(strLabel);

        //输入框
        var editBox;
        if(cc.sys.isNative) {
            editBox = cc.Scale9Sprite.createWithSpriteFrameName("resource_067_9.png");
        }
        else {
            editBox = new cc.Scale9Sprite("resource_067_9.png");
        }
        editBox.setPosition(-5, 0);
        editBox.setContentSize(320, 48);

        var inputBox = self._inputBox = new cc.EditBox(cc.size(343, 56), editBox);
        inputBox.setPosition(size.width / 2 + 65, size.height / 2);
        inputBox.setFontSize(25);
        inputBox.setPlaceHolder(cf.Language.getText("text_1343"));
        inputBox.setPlaceholderFontSize(25);
        inputBox.setMaxLength(20);
        inputBox.setDelegate(this);
        inputBox.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);

        self._spBK.addChild(self._inputBox);
    },

    /**
     * 请求兑换码奖励
     * @private
     */
    _askCodeGift: function () {
        var self = this;
        cf.UITools.showLoadingToast(cf.Language.getText("text_1012"), 20, true, this);

        var msg = new $root.CS_UseDuiHuanCode();
        msg.strCode = self._inputBox.getString();
        msg.nidLogin = self._localPlayerModel.getIdLogin();
        var wr = $root.CS_UseDuiHuanCode.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_UseDuiHuanCode, wr);
    },

    /**
     * 获取兑换码奖励
     * @param msg
     * @private
     */
    _resCodeGift: function (msg) {
        var self = this;
        cf.UITools.hideLoadingToast();
        var resMsg = $root.CS_UseDuiHuanCode_Res.decode(msg);

        self._callback.call(self._target);
        self.removeFromParent(true);

        self._target.addChild(UIAwardToast.create(resMsg.itemReward, function () {
        }, self._target, false, cf.DCType.DC_SYSTEM_CODE), 101);
    },

    ////////////////////////////////////////////////////////////
    //客服相关
    ////////////////////////////////////////////////////////////
    _initSupport: function () {
        var self = this;
        var size = self._spBK.getContentSize();

        var str = cf.Language.getText("text_1448");
        str = str.format(GameConfig.QQ, GameConfig.verName);

        var gameInfo = new ccui.Text(str, cf.Language.FontName, 28);
        gameInfo.setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        //gameInfo.setAnchorPoint(0.5, 0);
        gameInfo.setPosition(size.width / 2, size.height / 2 + 16);
        self._spBK.addChild(gameInfo);
    },

    _supportCallBack: function (sender, type) {
        if (type == ccui.Widget.TOUCH_ENDED) {
            var str = "";
            switch (sender.tag) {
                case 0://QQ
                    str = GameConfig.QQ;
                    break;
                case 1://客服电话
                    str = GameConfig.Phone;
                    break;
                case 2://微信
                    str = GameConfig.WeChat;
                    break;
            }
            if (cc.sys.isNative) {
                cf.JsbHelper.doEvent(cf.EventMessage.EventId_Copy, str);
            }
        }
    },
    //当编辑框文本改变时调用
    editBoxTextChanged: function (editBox, text) {
        var self = this;
        //if (cc.sys.isNative) {
        if (self._type == cf.DlgType.NICKNAME_DLG || self._type == cf.DlgType.SIGNATURE) {
            self._inputBox.setString(cf.checkCHWord(self._inputBox.getString()));
        }
        //}
    }

});


UIGeneralDlg.Btn = {
    CLOSE: 0,
    OK: 1,
    ORDER: 2
};

UIGeneralDlg.create = function (type, itemId, target, callback) {
    var dlg = new UIGeneralDlg();
    if (dlg && dlg.init(type, itemId, target, callback)) {
        return dlg;
    }
    return null;
};


