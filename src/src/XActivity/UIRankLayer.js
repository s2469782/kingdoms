var UIRankLayer = BaseLayer.extend({
    _spBK: null,
    _rankNode: null,
    _onRankListener: null,

    ctor: function () {
        this._super();

        var self = this;

        //创建遮罩层
        self._maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        self.addChild(self._maskLayer, -100);

        //背景
        var spBK = self._spBK = new cc.Scale9Sprite("longBK_9.png");
        spBK.setCapInsets(cc.rect(57, 54, 88, 92));
        spBK.setContentSize(800, 515);
        spBK.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        self.addChild(spBK);

        var spFrame = new cc.Scale9Sprite("resource_012_9.png");
        spFrame.setPosition(spBK.width / 2, spBK.height / 2 - 15);
        spFrame.setContentSize(780, 470);
        //spBK.addChild(spFrame);

        var spTitle = new cc.Sprite("#signReward_002.png");
        spTitle.setAnchorPoint(0.5, 0);
        spTitle.setPosition(spBK.width / 2, spBK.height - 30);
        spBK.addChild(spTitle);

        var titleLabel = new ccui.LabelBMFont("龙珠盈利榜", Res.LOBBY.font_resource_093);
        titleLabel.setPosition(spTitle.width / 2, spTitle.height / 2);
        titleLabel.setScale(1.2);
        spTitle.addChild(titleLabel);

        var leftLong01 = new cc.Sprite("#longBar01.png");
        leftLong01.setAnchorPoint(0,0.5);
        leftLong01.setPosition(-80,spBK.height/2+220);
        spBK.addChild(leftLong01,10);

        var leftLong02 = new cc.Sprite("#longBar02.png");
        leftLong02.setAnchorPoint(0,0.5);
        leftLong02.setPosition(-40,spBK.height/2-230);
        spBK.addChild(leftLong02,10);

        var rightLong01 = new cc.Sprite("#longBar01.png");
        rightLong01.setFlippedX(true);
        rightLong01.setAnchorPoint(1,0.5);
        rightLong01.setPosition(spBK.width+80,spBK.height/2+220);
        spBK.addChild(rightLong01,10);

        var rightLong02 = new cc.Sprite("#longBar02.png");
        rightLong02.setFlippedX(true);
        rightLong02.setAnchorPoint(1,0.5);
        rightLong02.setPosition(spBK.width+40,spBK.height/2-230);
        spBK.addChild(rightLong02,10);

        //退出按钮
        var closeBtn = new ccui.Button();
        closeBtn.loadTextureNormal("resource_021.png", ccui.Widget.PLIST_TEXTURE);
        closeBtn.setPosition(spBK.width - 20, spBK.height - 20);
        closeBtn.setPressedActionEnabled(true);
        closeBtn.addClickEventListener(function () {
            //判断下是否是渔场
            self.hide();
        });
        spBK.addChild(closeBtn,11);

        //信息框
        var spInfo = new cc.Sprite("#resource_124.png");
        spInfo.setPosition(spBK.width / 2, spBK.height - 67);
        spBK.addChild(spInfo);

        //名次
        var labelRank = new ccui.LabelBMFont(cf.Language.getText("text_1367"), Res.LOBBY.font_resource_093);
        labelRank.setPosition(90, spInfo.height / 2);
        spInfo.addChild(labelRank);

        //人物信息
        var labelName = new ccui.LabelBMFont(cf.Language.getText("text_1368"), Res.LOBBY.font_resource_093);
        labelName.setPosition(250, spInfo.height / 2);
        spInfo.addChild(labelName);

        //排行榜类型
        var labelGold = new ccui.LabelBMFont("盈利", Res.LOBBY.font_resource_093);
        labelGold.setPosition(spInfo.width - 150, spInfo.height / 2);
        spInfo.addChild(labelGold);

        //排行榜节点
        this._rankNode = new cc.Node();
        this._spBK.addChild(this._rankNode);
        //请求排行榜数据
        this._askRankInfo();

        //屏蔽触摸
        this.setTouchEnabled(true);
    },

    show:function () {
        this._super();
        //请求排行榜数据
        this._askRankInfo();
    },

    /**
     * 添加监听
     */
    addListener: function () {
        this._onRankListener = cf.addNetListener(MessageCode.CS_ShowLzRank_Res, this._resRankInfo.bind(this));
    },

    /**
     * 移除监听
     */
    removeListener: function () {
        cf.removeListener(this._onRankListener);
    },

    /**
     * 请求排行榜数据
     * @private
     */
    _askRankInfo: function () {
        //本地存储一个时间差，大于30分钟后才向服务器请求数据，否则使用本地数据
        var _localRankInfo = cf.SaveDataMgr.getNativeData("dragonBallRank");
        var _rankTime = cf.SaveDataMgr.getNativeData("dragonBallRankTime");
        var _checkTime = 3600000;
        if(_rankTime){
            _checkTime = new Date().getTime() - _rankTime;
        }
        if(_localRankInfo && _checkTime <= 1800000){
            //本地存在数据
            this._initRank(JSON.parse(_localRankInfo));
        }else{
            //注意：请求龙珠排行榜数据，只需对应消息号，消息自身结构体无意义，这里只是使用了一个相对较短的消息来向服务器请求数据
            cf.UITools.showLoadingToast("请稍后...", 10, false, this);
            var msg = new $root.CS_playerIsExistsMsg_Res();
            var wr = $root.CS_playerIsExistsMsg_Res.encode(msg).finish();
            cf.NetMgr.sendSocketMsg(MessageCode.CS_ShowLzRank, wr);
        }
    },

    /**
     * 解析排行榜数据
     * @param msg
     * @private
     */
    _resRankInfo: function (msg) {
        var resMsg = $root.DG_PlayerRankInfo.decode(msg);
        if (resMsg) {
            cf.UITools.hideLoadingToast();
            //保存本地数据
            cf.SaveDataMgr.setNativeData("dragonBallRank",JSON.stringify(resMsg.treasureMapRankInfo));
            cf.SaveDataMgr.setNativeData("dragonBallRankTime",new Date().getTime());
            this._initRank(resMsg.treasureMapRankInfo);
        }
    },

    /**
     * 初始化排行榜信息
     * @param rankInfo
     * @private
     */
    _initRank: function (rankInfo) {
        //自己排名
        var localModel = cf.PlayerMgr.getLocalPlayer();
        //移除所有节点
        this._rankNode.removeAllChildren();
        //排行榜列表
        var listView = this._listView = new ccui.ListView();
        listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        listView.setTouchEnabled(true);
        listView.setBounceEnabled(true);

        listView.setContentSize(cc.size(780, 280));
        listView.setPosition(50, 30);
        this._rankNode.addChild(listView);

        //自己默认排行榜数据
        var _mySelfRankInfo = {
            strNickname: localModel.getNickName(),
            nCurrNum: 0,
            nCurrRank: -1
        };
        for (var i in rankInfo) {
            var _info = rankInfo[i];

            var item = this._createRankInfo("resource_047.png", _info);
            listView.pushBackCustomItem(item);

            //判断自己是否在排行榜中
            if (_info.nidLogin == localModel.getIdLogin()) {
                //在排行榜中，直接赋值为排行榜数据
                _mySelfRankInfo = _info;
            }
        }

        listView.setGravity(ccui.ListView.GRAVITY_CENTER_VERTICAL);
        listView.setItemsMargin(5);

        //填充自己排行榜数据
        var itemMySelf = this._createRankInfo("rank_005.png", _mySelfRankInfo);
        itemMySelf.setPosition((this._spBK.width - itemMySelf.width) / 2, this._spBK.height - 190);
        this._rankNode.addChild(itemMySelf);
    },

    /**
     * 创建单个数据
     * @param frameName
     * @param rankInfo
     * @returns {*}
     * @private
     */
    _createRankInfo: function (frameName, rankInfo) {
        var itemLayout = new ccui.Layout();
        itemLayout.setContentSize(700, 99);
        //背景
        var itemBK = new ccui.ImageView(frameName, ccui.Widget.PLIST_TEXTURE);
        itemBK.setPosition(itemLayout.width / 2, itemLayout.height / 2);
        itemLayout.addChild(itemBK);
        //排名
        if (rankInfo.nCurrRank <= 3 && rankInfo.nCurrRank != -1) {
            var cupName = "";
            switch (rankInfo.nCurrRank) {
                case 1:
                    cupName = "resource_039.png";
                    break;
                case 2:
                    cupName = "resource_040.png";
                    break;
                case 3:
                    cupName = "resource_041.png";
                    break;
            }
            var cup = new ccui.ImageView(cupName, ccui.Widget.PLIST_TEXTURE);
            cup.setPosition(100, itemLayout.height / 2);
            itemLayout.addChild(cup);
        } else {
            var strRank = "";
            if (rankInfo.nCurrRank == -1) {
                //未上榜
                strRank = "未上榜";
            } else {
                strRank = rankInfo.nCurrRank + "";
            }
            var cupLabel = new ccui.LabelBMFont(strRank, Res.LOBBY.font_resource_093);
            cupLabel.setPosition(100, itemLayout.height / 2);
            cupLabel.setScale(1.2);
            itemLayout.addChild(cupLabel);
        }
        //头像
        var head = new ccui.ImageView("headPortrait_001.png", ccui.Widget.PLIST_TEXTURE);
        head.setScale(0.5);
        head.setPosition(200, itemLayout.height / 2);
        itemLayout.addChild(head);
        //昵称
        var nickName = new ccui.LabelBMFont(rankInfo.strNickname, Res.LOBBY.font_resource_093);
        nickName.setPosition(350, itemLayout.height / 2);
        itemLayout.addChild(nickName);
        //盈利
        var gold = new ccui.LabelBMFont(cf.numberToY(rankInfo.nCurrNum), Res.LOBBY.font_resource_083);
        gold.setPosition(itemLayout.width - 120, itemLayout.height / 2);
        itemLayout.addChild(gold);

        return itemLayout;
    }


});