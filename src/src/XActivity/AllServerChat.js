var AllServerChatInfo = cc.Node.extend({
    _cliperNode: null,
    _waitForPlayList: null,
    _scrollInfoLength: null,
    _scrolling: null,

    ctor: function () {
        var self = this;
        self._super();
        self.init();
        self.scheduleUpdate();
    },

    update: function () {
        this.checkScrollInfoHasChange();
        this.checkRichTextCanScroll();
    },

    checkScrollInfoHasChange: function () {
        var length = MsgPool.getInstance().getChatMsgALLElementLength();
        var scrollInfoArray = MsgPool.getInstance().getChatMsgALLArray();
        if (length > this._scrollInfoLength && ( length > 0 && this._scrollInfoLength > 0)) {
            //直接将数据插入到等待数组中
            this._scrollInfoLength = length;
            this._waitForPlayList.unshift(scrollInfoArray[length - 1]);
        }
    },

    checkRichTextCanScroll: function () {
        var scrollInfoArray = MsgPool.getInstance().getChatMsgALLArray();
        if (this._waitForPlayList.length > 0) {
            var waitLength = this._waitForPlayList.length;
            for (var i = 0; i < waitLength; i++) {
                if (!this._scrolling) {
                    this.addServiceRichTextFunction(this._waitForPlayList[i]);
                    this._waitForPlayList.splice(i, 1);
                    scrollInfoArray.splice(scrollInfoArray.indexOf(this._waitForPlayList[i]), 1);
                    waitLength -= 1;
                }
            }
        }
        else {
            var baseLength = MsgPool.getInstance().getChatMsgALLElementLength();
            if (baseLength > 0) {
                if (!this._scrollInfoLength) {
                    this._scrollInfoLength = baseLength;
                }
                for (var index = 0; index < baseLength; index++) {
                    //cc.log(presentTime + ";;;;;;;;;;;;;;;;;;;;" + scrollInfoArray[index]._nextPlayTime + ",,,,,,,,,,,,,,,,,,,,,,,,,,,");
                    if (!this._scrolling) {
                        cc.log("do scroll=========");
                        this.addServiceRichTextFunction(scrollInfoArray[index]);
                        scrollInfoArray.splice(index, 1);
                        continue;
                    }
                    else {
                        cc.log("push scroll=========" + index);
                        if (this._waitForPlayList.indexOf(scrollInfoArray[index]) == -1) {
                            this._waitForPlayList.push(scrollInfoArray[index]);
                        }
                    }
                }
            }
        }
    },

    init: function () {
        this._waitForPlayList = [];
        this.initScrollGameAnnounment();
    },

    //初始化跑马灯
    initScrollGameAnnounment: function () {
        var self = this;
        self._cliperNode = new cc.ClippingNode();

        var fakebg = new cc.Sprite("#resource_022.png");
        fakebg.setAnchorPoint(0.5,0);
        fakebg.setPosition(cc.winSize.width/2, 525);
        fakebg.setTag(10001);
        fakebg.setCascadeOpacityEnabled(true);
        fakebg.setOpacity(0);
        self.addChild(fakebg);

        var horn = new cc.Sprite("#resource_011.png");
        horn.setAnchorPoint(1,0.5);
        horn.setPosition(0, fakebg.height/2);
        fakebg.addChild(horn);

        var stencil = new cc.LayerColor(cc.color(0, 0, 0, 0));
        self._cliperNode.setStencil(stencil);
        stencil.setContentSize(fakebg.width, fakebg.height);
        stencil.setAnchorPoint(0, 0);
        stencil.setPosition((cc.winSize.width-fakebg.width)/2, 525);

        self._cliperNode.setPosition(0, 0);
        self.addChild(self._cliperNode);
    },

    /**
     * 将属性从单一的文本更改为文本数组
     */
    addServiceRichTextFunction: function (scrollInfo) {
        if(!scrollInfo) {
            return;
        }
        var richText = new ccui.RichText();
        richText.ignoreContentAdaptWithSize(true);
        richText.anchorX = 0.5;
        richText.anchorY = 0;
        if (cc.sys.isNative) {
            richText.setAnchorPoint(cc.p(0.5, -0.5));
        }
        var strTitle = scrollInfo.strTitle + ":" + " ";
        var strContent = scrollInfo.strContent;
        var strLevel = scrollInfo.strLevel;
        var level = new ccui.RichElementText(1, cc.color.RED, 255,  cf.Language.getText("text_1154") + strLevel + " ", cf.Language.FontName, 20);
        var title = new ccui.RichElementText(1, cc.color.YELLOW, 255, strTitle, cf.Language.FontName, 20);
        var content = new ccui.RichElementText(1, cc.color.YELLOW, 255, strContent, cf.Language.FontName, 20);
        richText.pushBackElement(level);
        richText.pushBackElement(title);
        richText.pushBackElement(content);
        richText.y = 520;
        richText.x = cc.winSize.width + richText.width / 2;
        this._cliperNode.addChild(richText, 1);
        this.doScrollAction(richText);
    },

    doScrollAction: function (richText) {
        var self = this;
        self._scrolling = true;
        self.getChildByTag(10001).stopAllActions();
        self.getChildByTag(10001).setOpacity(150);
        if (!richText) {
            return;
        }
        var moveto = cc.moveTo(15, cc.p(-richText.width / 2, 520));
        var callfuncRemove = cc.callFunc(function () {
            self._scrolling = false;
            if (MsgPool.getInstance().getChatMsgALLElementLength() == 0) {
                var fadeOut = cc.fadeOut(2);
                self.getChildByTag(10001).runAction(fadeOut);
            }
            this.removeFromParent();
        }, richText);
        //文本移动的序列化动作
        var sequence = cc.sequence(moveto, callfuncRemove);
        richText.runAction(sequence);
    }
});
