var UIDragonBall = BaseLayer.extend({

    //龙珠数组
    _spBallArray: [],

    //儲存龍珠
    _savedBallArray: [],

    //特效
    _spEffect: null,
    //规则
    _ruleNode: null,

    _dragonAttr: ["ball_gold.png", "ball_wood.png","ball_earth.png","ball_water.png","ball_fire.png"],

    _rouletteAttr: ["attribute_003.png", "attribute_004.png", "attribute_005.png","attribute_001.png","attribute_002.png"],

    _skillAttrBtn: ["sky", "pink", "green", "orange", "blue", "thunder"],

    _skillNotice: null,

    _skillNoticeBack: null,

    _prgressAttr: ["energy_0.png", "energy_1.png", "energy_2.png", "energy_3.png"],

    _goldbulletNum: 0,
   
    _woodbulletNum: 0,

    _dragonSkillNum: [],

    _progressBar: [],

    _savedBall: [],

    _rouletteEffect: null,

    _currentBall: null,

    ctor: function (roomId) {
        this._super();
       
        this._initUI(roomId);
    },

    //初始化UI
    _initUI: function (roomId) {
        var self = this;
        var spineJson, spineAtlas;
        spineJson = Res.GAME.skill_up_Json;
        spineAtlas = Res.GAME.skill_up_Atlas;
        
        
        this._skillNoticeBack = new cc.Sprite("#notice_background.png");
        this._skillNoticeBack.setAnchorPoint(0.5, 1);
        this._skillNoticeBack.setScaleX(10);
        this._skillNoticeBack.setScaleY(7);
        // this._skillNoticeBack.setScaleX(15);
        // this._skillNoticeBack.setScaleY(9);
        this._skillNoticeBack.setVisible(false);
        this._skillNoticeBack.setPosition(cc.winSize.width / 2, cc.winSize.height/2+10);
        this.addChild(this._skillNoticeBack);

        this._skillNotice = new cc.Sprite("#skill_notice.png");
        this._skillNotice.setAnchorPoint(0.5, 1);
        this._skillNotice.setScale(0.8);
        this._skillNotice.setVisible(false);
        this._skillNotice.setPosition(cc.winSize.width / 2, cc.winSize.height/2);
        this.addChild(this._skillNotice);
        
        
        //背景
        // var spBK = new cc.Sprite("#dragonBK_001.png");
        // spBK.setAnchorPoint(0.5, 1);
        // spBK.setPosition(cc.winSize.width / 2, cc.winSize.height/2-170);
        // spBK.runAction(cc.repeatForever(ActionMgr.getAnimate("dragonBK")));
        // this.addChild(spBK);
        // var spBK = new cc.Sprite("#dragonBK_BackGround.png");
        // spBK.setAnchorPoint(0.5, 1);
        // spBK.setPosition(cc.winSize.width / 2, cc.winSize.height/2-223);
        // spBK.setTag("spBK");
        // this.addChild(spBK);
         

        var spBACK = new cc.Sprite("#top_bar.png");
        spBACK.setPosition(cc.winSize.width /2 , cc.winSize.height/2 + 320);
        spBACK.setTag("spBACK");
        this.addChild(spBACK);

        var sptitle = new cc.Sprite("#top_bar_label.png");
        sptitle.setPosition(spBACK.width/2, spBACK.height/2+17);
        spBACK.addChild(sptitle);

        
        var spBK = new cc.Sprite("#ultimate.png");
        spBK.setAnchorPoint(0.5,1);
        spBK.setTag("spBK");
        spBK.setPosition(cc.winSize.width / 2, cc.winSize.height/2 -240);
        this.addChild(spBK);

        for(var a = 0; a< 5; a++){

            var spSide = new cc.Sprite("#skill_"+a+".png");
            spSide.setAnchorPoint(0.5, 1);
            spSide.setTag("spSide");
            spSide.setPosition(cc.winSize.width / 2-580, (cc.winSize.height/2+250) - (a*100));
            this.addChild(spSide, cf.Zoder.EFFECT);

            self._progressBar[a] = ActionMgr.createSpine(spineJson,spineAtlas,1);
            self._progressBar[a].setAnchorPoint(0.5, 0.5);
            self._progressBar[a].setPosition(spSide.width / 2+60, spSide.height/2+50);
            self._progressBar[a].setTag("spProgress"+a);
            self._progressBar[a].setAnimation(0, "up0", false);
            spSide.addChild(self._progressBar[a]);

        
        }

       
        //破城位置
        var pos = [
            cc.p(61, 25),//1星
            cc.p(105, 25),//2星
            cc.p(148, 25),//3星
            cc.p(191, 25),//4星
            cc.p(235, 25),//5星
            cc.p(278, 25),//6星
            cc.p(322, 25)//7星
        ];


        var savedPos = [
            cc.p(5, 155),//1星
            cc.p(40, 170),//2星
            cc.p(78, 180)//3星
        ];

       

    

            // self._spBallArray = [];
        //破城
        for (var i = 0; i < 7; ++i) {
            for(var a = 0; a < 7; a++){
                var town = new cc.Sprite("#broke.png");
                town.setPosition(pos[i].x, pos[i].y);
                town.setVisible(false);
                spBACK.addChild(town);
                self._spBallArray[i] = town;

    
            }    
            // var spBall = new cc.Sprite("#ball_empty.png");
            // spBall.setAnchorPoint(0,1);
            
            // spBall.setPosition(pos[i].x, pos[i].y);
            // //spBall.setVisible(false);
            // spBall.setTag(1);
            // spBK.addChild(spBall);
            // self._spBallArray[i] = spBall;
            // self._spBallArray[i].setName("ball_empty.png");
           
        }
       //箭头
       var spArrow = new cc.Sprite("#explainationicon.png");
       spArrow.setAnchorPoint(0.5,1);
       spArrow.setPosition(spBK.width/2,spBK.height/2-30);
       //spArrow.setRotation(180);
       spArrow.setScale(0.8);
       spBK.addChild(spArrow);

       //规则
       var ruleBtn = new ccui.Button("explainationicon.png",null,null,1);
       ruleBtn.setColor(cc.color.BLACK);
       ruleBtn.setAnchorPoint(0.5,1);
       ruleBtn.setPosition(spBK.width/2,spBK.height/2-30);
      // ruleBtn.setScaleY(1.2);
       ruleBtn.setScale(0.8);
       ruleBtn.setOpacity(0);
       ruleBtn.addClickEventListener(function () {
           self._ruleNode.setVisible(!self._ruleNode.isVisible());
       });
       spBK.addChild(ruleBtn);

       var ruleNode = self._ruleNode = new cc.Scale9Sprite("resource_028_9.png");
       ruleNode.setContentSize(700,165);
       ruleNode.setAnchorPoint(0.5,1);
       ruleNode.setPosition(spBK.width/2,spBK.height/2+280);
       spBK.addChild(ruleNode);

       var closeBtn = new ccui.Button("resource_028_9.png", null, null, 1);
       closeBtn.setScale(5);
       closeBtn.setAnchorPoint(0.5,1);
       closeBtn.setOpacity(0);
       closeBtn.setPosition(ruleNode.width/2,ruleNode.height/2+100);
       closeBtn.addClickEventListener(function (){
            self._ruleNode.setVisible(!self._ruleNode.isVisible());
       });
       ruleNode.addChild(closeBtn);
       

       var ruleLabel = new ccui.LabelBMFont("1.击破BOSS级或将领级贼军时，有机会击破敌营。\n2.击破7座敌营，便可得到宝物库，获得大彩金或最高100倍的额外奖金。\n3.额外奖金的倍率会堆叠加成，3次后重置。\n4.击破BOSS或将领时，有机会将其讨取或俘虏，随机友军士气会上升。\n5.友军士气满3格可施放友军无双。第二次集满时，加送最强的群体无双技。\n6.每日24点时，已击破的敌营数量会清空，请留意。",Res.LOBBY.font_resource_093);
       ruleLabel.setScale(0.9);
       ruleLabel.setPosition(ruleNode.width/2,ruleNode.height/2 );
       ruleNode.addChild(ruleLabel);
       self._ruleNode.setVisible(false);

      
        
      
        //解析龙珠数据
        var _dragonBallData = cf.PlayerMgr.getLocalPlayer().getDragonBallData(roomId);
        if(_dragonBallData){
            
            var _num = _dragonBallData[2];
            this.updateDragonBall(_num.length, false);
         
            var ani = _dragonBallData[3];
            var collection = _dragonBallData[4];
            var msg = [];
            var skill = [];


            //集氣格處理
            this.energyAmount(collection);
           
            msg.attrBall = _num;
            msg.attrBallHit = [];
            for(var i = 0; i < _num.length; ++i){
                // self._spBallArray[i].setVisible(true);
                 
                 if(_num[i] == -1){
                         
                    var num = this.calculateNum(msg);
                    self._progressBar[0].setAnimation(0, "up"+num[_num[i]], false);
                     
                 }else if(_num[i] > -1){
                     
                    var num = this.calculateNum(msg);
                    self._progressBar[_num[i]].setAnimation(0, "up"+num[_num[i]], false);
                     
                 }else if(_num[i] == -2){
 
                     
                 }
              }
        
            
                
            var msg = [];
             
            if(ani[5] > 0){
               
               skill.push(5);
               ani[5] = ani[5] - 1;
               
               // cf.PlayerMgr.getLocalPlayer().setDragonSkill(ani);
               self._dragonSkillNum = ani;
               msg.attrBallHit = skill;
               cf.PlayerMgr.getLocalPlayer().setDragonBonusString(5);
               cf.btnDisable = true;
               cf.autolock = false;
               this.energyAmount(2);
            }else{
                
               for(var a = 0; a< ani.length; a++){
                   if(ani[a] > 0){
                           skill.push(a); 
                         //   cf.PlayerMgr.getLocalPlayer().setDragonSkill(ani);      
                   }
               }
               msg.attrBall = _num;
               msg.attrBallHit = skill;
             
                  if(msg.attrBallHit.length > 0){
                       cf.btnDisable = true;
                       cf.autolock = false;
                        self.skillNotice(msg);
                        cf.PlayerMgr.getLocalPlayer().setDragonBonusString(msg.attrBallHit[0]);
                       
                        ani[msg.attrBallHit[0]] -= 1;
                         
                         
                         self._dragonSkillNum = ani;
                   }
               
           }   
                
                   
                    
                       
        }
                
        // var time = setTimeout(()=>{
        //     this.energyAmount(2);

        // }, 4000);
        
        
        
        
    },
    skillNotice: function(msg){
       var self = this;
        var locPlayer = cf.PlayerMgr.getLocalPlayer();
       
        if(locPlayer){
            cf.btnDisable = true;

            if(cf.autolock){
                var commonRoom = cf.RoomMgr.getRoom();
                var player = commonRoom._locPlayer;
                cf.autolock = false;
                player.closeAutoLockEffect();
                cf.EffectMgr.stopLockEffect();
                player._lockSkillEffEnd();
                cf.SkillMgr.resetLockSkill(player.getSeatID());
            }

        }
        
            this._skillNoticeBack.setVisible(true);
            this._skillNoticeBack.setScaleX(10);
            this._skillNoticeBack.setScaleY(7);
            this._skillNoticeBack.setPosition(cc.winSize.width/2, cc.winSize.height/2 + 10);
            this._skillNotice.setVisible(true);
            this._skillNotice.setPosition(cc.winSize.width/2, cc.winSize.height/2);
            this._skillNotice.setSpriteFrame("skill_notice.png");
            
               self._progressBar[msg.attrBallHit[0]].setAnimation(0, "up3", false);
                self._progressBar[msg.attrBallHit[0]].addAnimation(0, "up4", true);
                self._progressBar[msg.attrBallHit[0]].runAction(cc.sequence(
                    cc.delayTime(2),
                    cc.callFunc(()=>{
                        var roomBtn = new ccui.Button("resource_007_9.png", null, null, 1);
                        roomBtn.setContentSize(300,300);
                        roomBtn.setScale(0.5);
                        roomBtn.setOpacity(0);
                        roomBtn.setPosition( self._progressBar[msg.attrBallHit[0]].width/2-45 ,  self._progressBar[msg.attrBallHit[0]].height/2 -30);
                        roomBtn.setTag("roomBtn");
                        self._progressBar[msg.attrBallHit[0]].addChild(roomBtn);
                        roomBtn.pressIndex = msg.attrBallHit[0];
                        self.skillBtnShowed();
                        roomBtn.addTouchEventListener(self._enterRoomCallBack, self);
                        cf.PlayerMgr.getLocalPlayer().setDragonBonusString(msg.attrBallHit[0]);

                    })

                ))
                   
                   
          

            
            
           
        
    },
   
    _recheckDragonSkill: function(msg){
        
        var self = this;
     //   var _dragonSkillData = cf.PlayerMgr.getLocalPlayer().getDragonSkill();
        var _dragonSkillData = self._dragonSkillNum;
        
        if(_dragonSkillData){
            var commonRoom = cf.RoomMgr.getRoom();
            var player = commonRoom._locPlayer;
            if(player.getSeatID() == msg.seatId){
            var msg = [];
            var skill = [];
            var ani = _dragonSkillData;
            
            if(ani[5] > 0){
               msg.collectAttrBall = 2;
               
               skill.push(5);
               msg.attrBallHit = skill;
               cf.btnDisable = true;
               cf.autolock = false;
               
               cf.PlayerMgr.getLocalPlayer().setDragonBonusString(5);
             //  ani[5] = ani[5] - 1;
              self._dragonSkillNum = ani;
            //   cf.PlayerMgr.getLocalPlayer().setDragonSkill(ani);
               
            }else{
                if(ani.length > 0){
                    
                    for(var a = 0; a< ani.length; a++){
                        
                        if(ani[a] > 0){
                            skill.push(a);
                   //        cf.PlayerMgr.getLocalPlayer().setDragonSkill(ani);
                        }
                    }

                    msg.attrBallHit = skill ;
                    
                    if(msg.attrBallHit.length > 0){
                        cf.PlayerMgr.getLocalPlayer().setDragonBonusString(msg.attrBallHit[0]);
                        
                        cf.btnDisable = true;
                        cf.autolock = false;
                         self.skillNotice(msg);

                        
                        ani[msg.attrBallHit[0]] -= 1;
                          
                    }
                    
              
                    self._dragonSkillNum = ani;
               
                
               // self.skillBtn(msg);
               
            }
           }
            
           
        }
        }

    },
    
    updateDragonBall: function(num, load){
        var self = this;
        
        
        if(num >= 7){
            
            for(var a = 0 ; a < self._spBallArray.length; a++){
                self._spBallArray[a].setVisible(false);
            }
            for(var a = 0; a < self._progressBar.length; a++){
                 self._progressBar[a].setAnimation(0, "up0", false);
                
             }
            if(load){
                
                for(var a = 0; a < self._progressBar.length; a++){
                    // self._progressBar[a].setAnimation(0, "up0", false);
                     self._progressBar[a].runAction(cc.sequence(
                         cc.delayTime(0.5),
                         cc.callFunc(function(){
                             this.setAnimation(0, "jp2", false);
                            // this.clearTrack(0);
                             
                         }, self._progressBar[a])
                     ))
                     
                 }
            }
            
        }else{

            if(load){

                var spineJson , spineAtlas;
                spineJson = Res.GAME.jpfortress_Json;
                spineAtlas = Res.GAME.jpfortress_Atlas;
                
                self._spBallArray[num-1].setVisible(true);
                var brokeAnimate = ActionMgr.createSpine(spineJson, spineAtlas, 1);
                // console.log(self._spBallArray[num-1], self._spBallArray[num-1].getPosition());
                brokeAnimate.setPosition(self._spBallArray[num-1].getPosition());
                brokeAnimate.setAnimation(0, "jpup", false);
                cf.SoundMgr.playEffect(46,false);
                self.getChildByTag("spBACK").addChild(brokeAnimate);
                brokeAnimate.setCompleteListener(()=>{
                    brokeAnimate.removeFromParent();
                    
                });
            }else{
                for(var a = 0; a< num; a++){
                    
                    self._spBallArray[a].setVisible(true);
                }
                
            }
            
         
            
        }

    },
    energyAmount: function(enerygyCount){
        var self = this;
        var energyJson, energyAtlas;
        energyJson = Res.GAME.skill_center_Json;
        energyAtlas = Res.GAME.skill_center_Atlas;
        
        if(this.getChildByTag("spBK").getChildByTag("spEnergy")){
            this.getChildByTag("spBK").getChildByTag("spEnergy").removeFromParent();
        }

        var spEnergy = ActionMgr.createSpine(energyJson,energyAtlas,1);
        spEnergy.setAnchorPoint(0.5, 1);
        spEnergy.setTag("spEnergy");
        spEnergy.setAnimation(3, "up0", true);
        spEnergy.setPosition(this.getChildByTag("spBK").width/2+120, this.getChildByTag("spBK").height/2+70);
        this.getChildByTag("spBK").addChild(spEnergy);
        
        
        if(enerygyCount == 0){
            spEnergy.setAnimation(3, "up0", false);
        }else if(enerygyCount == 1 ){
            
            spEnergy.setAnimation(3, "up"+enerygyCount, false);
        }else if(enerygyCount == 2){
            this._skillNoticeBack.setVisible(true);
            this._skillNoticeBack.setScaleX(10);
            this._skillNoticeBack.setScaleY(7);
            this._skillNoticeBack.setPosition(cc.winSize.width/2, cc.winSize.height/2 + 10);
            this._skillNotice.setVisible(true);
            this._skillNotice.setPosition(cc.winSize.width/2, cc.winSize.height/2);
            this._skillNotice.setSpriteFrame("ultimate_notice.png");
            
            spEnergy.addAnimation(3, "up3", false);
            spEnergy.addAnimation(3, "up4", true);
            spEnergy.runAction(cc.sequence(
                cc.delayTime(2),
                cc.callFunc(()=>{
                    var energyBtn = new ccui.Button("resource_007_9.png", null, null, 1);
                    energyBtn.setContentSize(400,400);
                    energyBtn.setScale(0.5);
                    energyBtn.setOpacity(0);
                    energyBtn.setPosition( this.getChildByTag("spBK").width/2-80 ,  this.getChildByTag("spBK").height/2 +40);
                    energyBtn.setTag("energyBtn");
                    spEnergy.addChild(energyBtn);
                    energyBtn.pressIndex = 5;
                    self.skillBtnShowed();
                    energyBtn.addTouchEventListener(self._enterRoomCallBack, self);
                })

            ))
                    

            

            
        }

    },

    
   
   
    resetProgress: function(){
        var self = this;
        for(var a =0 ; a < self._progressBar.length; a++){
            self._progressBar[a].clearTrack();
            self._progressBar[a].setAnimation(0, "up0", false);
        }
    },

    SkillUsed: function(msg){
        var self = this;
        
        var commonRoom = cf.RoomMgr.getRoom();
        var player = commonRoom._locPlayer;
        
      
        var skillBtn = this.getChildByTag("skillBtn");
       
        if(player.getSeatID() == msg.seatId){
           // player.modifyFortSkin();
            
            if(msg.sender != 5){
                cf.btnDisable = false;
                self._progressBar[msg.sender].clearTrack();
                self._progressBar[msg.sender].setAnimation(0, "up0", false);
            }else{
                this.energyAmount(0);
            }
            
        }
        
        

       
            
        
        

        var obj = setTimeout(()=>{

            this._recheckDragonSkill(msg);
            clearTimeout(obj);
        }, 3000);

    },

    updateUI:function (msg) {
        
        var self = this;
        console.log(msg);
      
       
        
        if(msg.ballNum >= 7){
          //  self.resetProgress();
          self.updateDragonBall(msg.ballNum, true);


          var dragonJson, dragonAtlas;
          dragonJson = Res.GAME.jpfortressall_Json;
          dragonAtlas = Res.GAME.jpfortressall_Atlas;

          var seven = ActionMgr.createSpine(dragonJson, dragonAtlas, 1);
          seven.setPosition(self.getChildByTag("spBACK").width/2, self.getChildByTag("spBACK").height/2-20);
          seven.setAnimation(0, "jp", false);
          self.getChildByTag("spBACK").addChild(seven);
          
          seven.runAction(cc.sequence(
              cc.delayTime(3),
              cc.callFunc( ()=> {
                 seven.removeFromParent();
                 
                 self.updateDragonBall(msg.ballNum, false);
              
            })
          ))

        }else{
            self.updateDragonBall(msg.ballNum, true);
            self.UpdateProgress(msg);
        }
        
    },
    
    
    
    UpdateProgress: function(msg){
        var self = this;

        if(msg.colletAttrBall > 0){
            self.energyAmount(msg.colletAttrBall);
        }
        
        if(msg.attrBall.length > 0){

            var p = [0, 0, 0, 0 ,0, 0];
            if(msg.attrBallHit[1] == 5){
                cf.PlayerMgr.getLocalPlayer().setDragonBonusString(5);
                //self.skillBtn(msg);
                cf.btnDisable = true;
                cf.autolock = false;
                //self.skillNotice(msg);
                
                msg.attrBallHit.pop();
            
                
                    
                p[msg.attrBallHit[0]]  += 1;
                
              
                 self._dragonSkillNum = p;
                 

                // cf.PlayerMgr.getLocalPlayer().setDragonSkill(p);
          //  }else if(msg.attrBallHit[0] > 0){
               // cf.PlayerMgr.getLocalPlayer().setDragonSkill(ani);
                // self.skillBtn(msg);
                
            }else{
                if(msg.attrBallHit[0] != -1 && msg.attrBallHit.length > 0){
                    //self.skillBtn(msg);
                    cf.btnDisable = true;
                    cf.autolock = false;
                    cf.PlayerMgr.getLocalPlayer().setDragonBonusString(msg.attrBallHit[0]);
                    self.skillNotice(msg);
                  
                    
                    if(msg.attrBallHit[1] == 5){
                        p[5]  += 1;
                    }
                    self._dragonSkillNum = p;
                    // cf.PlayerMgr.getLocalPlayer().setDragonSkill(p);

                }


            }
            if(msg.attrBallHit[0] == -1 || msg.attrBallHit[1] > 0){
                for(var a=  0; a < msg.attrBall.length; a++){
                
                    if(self._progressBar[msg.attrBall[a]]){
                        if(msg.attrBallHit[0] == -1){
    
                            var num = this.calculateNum(msg);
                            if(msg.attrBall[msg.attrBall.length-1] != -2){
                                self._progressBar[msg.attrBall[msg.attrBall.length-1]].setAnimation(0, "up"+num[msg.attrBall[a]], false);
                            }
                            
                        }else{

                        }
                        
                        
                    }
                 }
            }
           
                         
             
        }
    },

    skillBtnShowed: function(){
        var localSeatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
        var seatId = cf.convertSeatId(localSeatId);
        var commonRoom = cf.RoomMgr.getRoom();
        var player = commonRoom._locPlayer;
        if(player){
            cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UPDATE_AUTO_UI);
            player.onFireEnd();
            player._bAutoFire = false;
            player._lastAutoOnFire = false;
            player.addPromptOfAutoFire();
        }
    },

    _enterRoomCallBack: function (sender, type) {
        cf.SoundMgr.playEffect(55);
        
        var self = this;
        
        switch (type) {
            case ccui.Widget.TOUCH_ENDED:
                switch(sender.pressIndex){
                    
                    case 0: //金
                    this._skillNotice.setVisible(false);
                    this._skillNoticeBack.setVisible(false);
                    var commonRoom = cf.RoomMgr.getRoom();
                    var player = commonRoom._locPlayer;
                    var roomid = cf.RoomMgr.getRoomId();
                    if(player){
                        if(self._progressBar[sender.pressIndex].getChildByTag("roomBtn")){
                            self._progressBar[sender.pressIndex].getChildByTag("roomBtn").removeFromParent();
                        }
                        cf.SoundMgr.playEffect(63);
                        //player.modifySkillFortSkin(roomid, 3);
                       
                        cf.SoundMgr.playEffect(58);
                        
                       // skillBtn.removeFromParent();
                        player.dragonSkillFire(sender.pressIndex);
                       

                    }

                    break;

                    case 1: //木
                    this._skillNotice.setVisible(false);
                    this._skillNoticeBack.setVisible(false);
                    var commonRoom = cf.RoomMgr.getRoom();
                    var player = commonRoom._locPlayer;
                    var roomid = cf.RoomMgr.getRoomId();
                    if(player){
                        if(self._progressBar[sender.pressIndex].getChildByTag("roomBtn")){
                            self._progressBar[sender.pressIndex].getChildByTag("roomBtn").removeFromParent();
                        }
                        cf.SoundMgr.playEffect(63);
                        //player.modifySkillFortSkin(roomid, 3);
                       
                        cf.SoundMgr.playEffect(58);
                        
                       // skillBtn.removeFromParent();
                        player.dragonSkillFire(sender.pressIndex);
                       

                    }

                        

                    break;


                    case 2: //土
                    this._skillNotice.setVisible(false);
                    this._skillNoticeBack.setVisible(false);
                        var commonRoom = cf.RoomMgr.getRoom();
                        var player = commonRoom._locPlayer;
                        var roomid = cf.RoomMgr.getRoomId();
                        if(player){
                            if(self._progressBar[sender.pressIndex].getChildByTag("roomBtn")){
                                self._progressBar[sender.pressIndex].getChildByTag("roomBtn").removeFromParent();
                            }
                            cf.SoundMgr.playEffect(63);
                            //player.modifySkillFortSkin(roomid, 3);
                           
                            cf.SoundMgr.playEffect(58);
                            
                           // skillBtn.removeFromParent();
                            player.dragonSkillFire(sender.pressIndex);
                           

                        }

                    break;

                    case 3: //水
                    this._skillNotice.setVisible(false);
                    this._skillNoticeBack.setVisible(false);
                        var commonRoom = cf.RoomMgr.getRoom();
                        var player = commonRoom._locPlayer;
                        var roomid = cf.RoomMgr.getRoomId();
                        if(player){
                            if(self._progressBar[sender.pressIndex].getChildByTag("roomBtn")){
                                self._progressBar[sender.pressIndex].getChildByTag("roomBtn").removeFromParent();
                            }
                            cf.SoundMgr.playEffect(63);
                           // player.modifySkillFortSkin(roomid, 3);
                          
                            cf.SoundMgr.playEffect(59);
                            //skillBtn.removeFromParent();
                            player.dragonSkillFire(sender.pressIndex);
                        }

                    break;

                    case 4://火
                    this._skillNotice.setVisible(false);
                    this._skillNoticeBack.setVisible(false);
                        var commonRoom = cf.RoomMgr.getRoom();
                        var player = commonRoom._locPlayer;
                        var roomid = cf.RoomMgr.getRoomId();
                        if(player){
                            if(self._progressBar[sender.pressIndex].getChildByTag("roomBtn")){
                                self._progressBar[sender.pressIndex].getChildByTag("roomBtn").removeFromParent();
                            }
                            cf.SoundMgr.playEffect(63);      
                          //  player.modifySkillFortSkin(roomid, 3);
                           
                            cf.SoundMgr.playEffect(60);
                          //  skillBtn.removeFromParent();
                            player.dragonSkillFire(sender.pressIndex);
                        }

                    break;


                    case 5://集氣
                    this._skillNotice.setVisible(false);
                    this._skillNoticeBack.setVisible(false);
                        var commonRoom = cf.RoomMgr.getRoom();
                        var player = commonRoom._locPlayer;
                        var roomid = cf.RoomMgr.getRoomId();
                        if(player){
                            if(self.getChildByTag("spBK").getChildByTag("spEnergy")){
                                self.getChildByTag("spBK").getChildByTag("spEnergy").getChildByTag("energyBtn").removeFromParent();
                            }
                            cf.SoundMgr.playEffect(63);
                           // player.modifySkillFortSkin(roomid, 3);
                            cf.SoundMgr.playEffect(61);
                           // skillBtn.removeFromParent();
                            player.dragonSkillFire(sender.pressIndex);
                        }
                    break;

                }

               
                break;
        }
    },

    calculateNum: function(msg){
        var obj = {};

        if(msg.attrBall.length > 0){
        for(var a=  0; a < msg.attrBall.length; a++){
            var item = msg.attrBall[a];
            obj[item] = (obj[item]+1) || 1;
        }

        return obj;
        }
    },
        
    

    UpdateBulletNum:function (msg){
       

        switch(msg.type){
            case 0:
                // var goldbullet = this.getChildByTag("goldBullet");
                if(msg.bulletNum <= 0){
                    var arr = [];
                    arr.sender = 0;
                    arr.seatId = msg.seatId;
                    cf.dragon_gold = false;
                   // goldbullet.removeFromParent();
                    this.SkillUsed(arr);
                    cf.goldwoodDisable = false;

 
                }else{

                    // if(goldbullet){
                    //     goldbullet.setString(msg.bulletNum);
                    // }
                  
                }
                
            break;

            case 1:
              //  var woodbullet = this.getChildByTag("woodBullet");
                
                if(msg.bulletNum <= 0){
                    var arr = [];
                    arr.sender = 1;
                    arr.seatId = msg.seatId;
                    cf.dragon_wood = false;
                //    woodbullet.removeFromParent();
                    this.SkillUsed(arr);
                    cf.goldwoodDisable = false;
                   
                    
                }else{
                   
                    // if(woodbullet){
                    //     woodbullet.setString(msg.bulletNum);
                    // }
                }
                
            break;

        
        }


    },
    

    showRule:function () {
       // this._ruleNode.setVisible(true);
    },
    
    hideRule:function () {
       // this._ruleNode.setVisible(false);
    }


});