var PopupAnnouncement = BaseLayer.extend({
    _maskLayer: null,
    _popupAnnouncementIsOn: false,
    _firstPopup: true,
    _popCount: 0,
    _pathArray: [], //下载的图片的引用地址
    _picBoard: null,    //基础的图片面板
    _baseCloseBtn: null, //基础的移除按钮
    _countTime: 0,      //游戏计时时间
    _canremoveAnnouncement: false, //可以点击移除关闭按钮 需要等待下请求图片的时间 这里设定为一秒

    ctor: function (type,msg) {
        var self = this;
        self._super();
        self.addBlackLayer();
        self._PopMsg = msg;
        self.initPopUpAnnounceMent(type,msg);
        self.setTouchEnabled(true);
        self.scheduleUpdate();
    },

    //添加遮罩界面
    addBlackLayer: function () {
        var self = this;
        var pFadeBoard = new cc.LayerColor(cc.color(0, 0, 0, 200));
        pFadeBoard.setPosition(cc.winSize.width * 0.5, cc.winSize.height * 0.5);
        //pFadeBoard.setVisible(false);
        pFadeBoard.setAnchorPoint(1, 1);
        pFadeBoard.setScale(2.0);
        self.addChild(pFadeBoard);
    },

    update:function (dt) {
        this._countTime += dt;
        if(this._countTime > 1) {
            this._countTime = 0;
            this._canremoveAnnouncement = true;
            this.unscheduleUpdate();
        }
    },

    /**
     * @param type 确定公告类型 由此生成对应的UI元素
     * msg 需要显示的信息
     */
    initPopUpAnnounceMent: function (type,msg) {
        switch (type) {
            case PopupAnnouncement.Enum.TEXT:
                //初始化文字文字公告
                this.initTextAnnouncement(msg);
                break;
            case PopupAnnouncement.Enum.PIC:
                this.initPicAnnouncement();
                break;
        }
    },

    initPicAnnouncement: function () {
        var self = this;
        //添加背景
        //将精灵作为参数传递进来
        this._popupAnnouncementIsOn = true;

        if (!cc.sys.isNative) {
            var length = self._PopMsg.length;
            for(var i = 0;i < length; i++) {
                var url = self._PopMsg[i] .strPicUrl;
                self.loadImgInHtml(url, this);
            }
        }
        else {
            var length = self._PopMsg.length;
            for(var i = 0;i < length; i++) {
                var url = self._PopMsg[i] .strPicUrl;
                self.loadImgInNative(url, this);
            }
        }
    },

    loadImgInHtml: function (url, pNode) {
        var self = this;
        cc.loader.loadImg(url, {isCrossOrigin: true}, function (err, img) {
            if (err != null) {
                cc.log(err);
                self.removeFromParent();
                return;
            }
            var texture2d = new cc.Texture2D();
            texture2d.initWithElement(img);
            texture2d.handleLoadedTexture();
            var logo = new cc.Sprite(texture2d);
            logo.setPosition(pNode.width / 2, pNode.height / 2);
            logo.setTag(self._popCount);
            cc.log("加载成功");
            pNode.addChild(logo);

            var closeBtn = new ccui.Button();
            closeBtn.loadTextureNormal("resource_021.png", 1);
            closeBtn.setPosition(logo.width, logo.height);
            closeBtn.addClickEventListener(function () {
                logo.removeFromParent(true);
                var pNode = this.getParent();
                MsgPool.getInstance().removePicAnnouncementElement( self._PopMsg[pNode.getTag()].nPicNoticeID);
                if (MsgPool.getInstance().getPicAnnouncementArrayLength() == 0) {
                    self.removeFromParent();
                }
            });
            logo.addChild(closeBtn);

            self._popCount++;
        });
    },

    loadImgInNative: function (url, pNode) {
        var self = this;
        self.addBaseActivityUi();
        var dirpath = jsb.fileUtils.getWritablePath() + 'img/';
        var picName = this.getPicName(url);
        cc.log(picName + "popAnnouncement - code line 104");
        var filepath = dirpath + picName;
        cc.log(jsb.fileUtils.getWritablePath());

        if (jsb.fileUtils.isFileExist(filepath)) {
            cc.log('Remote is find' + filepath);
            this.loadFileHasExist(filepath, pNode);
            return;
        }
        else {
            filepath = dirpath + ".png"
        }
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function () {
            cc.log("xhr.readyState  " + xhr.readyState + "...................................sddsdasdsda...................");
            cc.log("xhr.status  " + xhr.status);
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    xhr.responseType = 'arraybuffer';
                    cc.log("开始请求");
                    this.saveFile(xhr.response, picName, dirpath, filepath, pNode);
                } else {
                    cc.log("请求数据失败");
                    self.removeFromParent();
                }
            }
        }.bind(this);
        xhr.open("GET", url, true);
        xhr.send();
    },

    addBaseActivityUi:function () {
        var self = this;
        self._picBoard = new cc.Sprite("res/image/picActivity.png");
        self._picBoard.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        this.addChild(self._picBoard);

        self._baseCloseBtn = new ccui.Button();
        self._baseCloseBtn.loadTextureNormal("resource_021.png", 1);
        self._baseCloseBtn.setPosition(self._picBoard.width, self._picBoard.height * 0.95);
        self._baseCloseBtn.setScale(0.8);
        self._baseCloseBtn.addClickEventListener(function () {
            if(self._canremoveAnnouncement) {
                self.removeFromParent();
            }
        });
        self._picBoard.addChild(self._baseCloseBtn);
    },

    getPicName: function (url) {
        var length = url.length;
        var picName = [];
        for (var i = length - 1; i >= 0; i--) {
            if (url[i] != "/") {
                picName.push(url[i]);
            }
            else {
                break;
            }
        }
        cc.log("/");
        return picName.reverse().join("");
    },

    saveFile: function (data, picName, dirpath, filepath, pNode) {
        var self = this;
        if (typeof data !== 'undefined') {
            if (!jsb.fileUtils.isDirectoryExist(dirpath)) {
                jsb.fileUtils.createDirectory(dirpath);
            }

            if (jsb.fileUtils.writeDataToFile(new Uint8Array(data), filepath)) {
                cc.log('Remote write file succeed.');
                jsb.fileUtils.renameFile(dirpath, ".png", picName);
                this.loadFileHasExist(dirpath + picName);
            } else {
                cc.log('Remote write file failed.');
                self.removeFromParent();
            }
        } else {
            cc.log('Remote download file failed.');
        }
    },

    loadFileHasExist: function (path) {
        cc.log(path);
        var self = this;

        self._picBoard.setVisible(false);
        self._baseCloseBtn.setVisible(false);
        self._baseCloseBtn.setTouchEnabled(false);
        self._picBoard.removeFromParent();


        var picBoard = new cc.Sprite("res/image/picActivity.png");
        picBoard.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        picBoard.setTag(self._popCount);
        self.addChild(picBoard);
        if(self._popCount > 0) {
            picBoard.setVisible(false);
            picBoard.setScale(0.6)
        }

        var logo = new cc.Sprite(path);
        logo.setPosition(picBoard.width / 2, picBoard.height / 2);
        picBoard.addChild(logo);

        var closeBtn = new ccui.Button();
        closeBtn.loadTextureNormal("resource_021.png", 1);
        closeBtn.setPosition(picBoard.width, picBoard.height * 0.95);
        closeBtn.setScale(0.8);
        closeBtn.addClickEventListener(function () {
            var pNode = this.getParent();

            var nextBoard = self.getChildByTag(pNode.getTag() + 1);
            if(nextBoard) {
                nextBoard.setVisible(true);
                nextBoard.runAction(cc.sequence(
                    cc.scaleTo(0.15, 1.2),
                    cc.scaleTo(0.15, 1)
                ));
            }

            MsgPool.getInstance().removePicAnnouncementElement( self._PopMsg[pNode.getTag()].nPicNoticeID );
            if (MsgPool.getInstance().getPicAnnouncementArrayLength() == 0) {
                self.removeFromParent();
                return;
            }

            pNode.removeFromParent(true);

        });
        picBoard.addChild(closeBtn,1);
        self._popCount++;
    },

    initTextAnnouncement: function (popMsg) {
        var self = this;
        //添加背景
        //将精灵作为参数传递进来
        self._popCount++;
        var strTitle,strConment;
        strTitle = popMsg.strTitle;
        strConment = popMsg.strContent;
        self._popupAnnouncementIsOn = true;
        var spBk = new cc.Scale9Sprite("resource_036_9.png");
        spBk.setContentSize(650, 480);
        spBk.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        self.addChild(spBk);

        var noticeLabel = new cc.Sprite("#title_notice.png");
        noticeLabel.setPosition(spBk.width/2, spBk.height-40);
        spBk.addChild(noticeLabel);

        if (!self._firstPopup) {
            //动画
            spBk.runAction(cc.sequence(
                cc.scaleTo(0.1, 1.1),
                cc.scaleTo(0.1, 1.0)
            ));
        }

        //提示
        var title = new cc.LabelTTF(strTitle, cf.Language.FontName, 35);
        title.setPosition(spBk.width / 2, spBk.height);
        spBk.addChild(title);

        var closeBtn = new ccui.Button();
        closeBtn.loadTextureNormal("resource_021.png", 1);
        closeBtn.setPosition(spBk.width - 40, spBk.height - 40);
        closeBtn.setScale(0.9);
        closeBtn.addClickEventListener(function () {
            self._popCount--;
            spBk.removeFromParent(true);
            if(self._popCount == 0) {
                self.removeFromParent();
            }
        });
        spBk.addChild(closeBtn);

        //右按钮，确定
        var rightBtn = new ccui.Button();
        rightBtn.loadTextureNormal("button_005.png", 1);
        rightBtn.setTitleText(cf.Language.getText("text_1224"));
        rightBtn.setTitleFontSize(32);
        rightBtn.setTitleFontName(cf.Language.FontName);
        var title02 = rightBtn.getTitleRenderer();
        title02.enableStroke(cf.BtnColor.YELLOW, 2);
        rightBtn.setPosition(spBk.width / 2, 40);
        rightBtn.setTag(10001);
        rightBtn.addClickEventListener(function () {
            self._popCount--;
            spBk.removeFromParent(true);
            if(self._popCount == 0) {
                self.removeFromParent();
            }
        });
        spBk.addChild(rightBtn);

        //提示内容
        var label = new cc.LabelTTF(strConment, cf.Language.FontName, 25);
        label.setAnchorPoint(cc.p(0.5, 0.5));
        label.setColor(cc.color.YELLOW);
        label.setPosition(spBk.width / 2,spBk.height / 2 + 40);
        if(strConment.length > 13) {
            label.setDimensions(spBk.width - 150,spBk.height - 120);
            label.setPosition(spBk.width / 2,spBk.height / 2);
        }
        spBk.addChild(label);
    },
});

PopupAnnouncement.Enum = {
    //文字公告
    TEXT:0,
    //图片公告
    PIC:1,
};