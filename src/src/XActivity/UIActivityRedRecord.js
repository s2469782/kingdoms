var UIActivityRedRecord= BaseLayer.extend({

    _pageNum: 1,//总页数
    _pageIndex: 1, //当前页数
    _checkDate: 0, //当前显示数据的日期
    _onNetMsgListener: null, //网络监听
    _fishingInfo: {},//用于临时存储当日数据
    _detaileInfo: {},
    _detaileCheckId: {}, //详细查询的唯一Id
    _roomId: 0,          //场景ID
    _showInfoMod: 0,     //当前显示数据的类型
    _newFishingInfo: {},
    _oldFishingInfo: {},

    _btnBackShow: null,

    _ActivityRedPack: null,
    _actID: null,
    _state: null,
    ctor: function (state) {
        this._super();
        var self = this;
     //   self._ActivityRedPack = new UIActivityRedPack();
        
        
        
        self.initData();
        //遮罩层
        var maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        self.addChild(maskLayer, -100);
        self.setTouchEnabled(true);

        self._date = new Date();
        var startTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._date.getDate() + " " + "00:00:00";
        var endTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._date.getDate() + " " + "23:59:59";
       // self.askForFishRecord(startTimeStr, endTimeStr);

       var msgDataArray = [        
        //请求活动监听(更新活动)
            {msgId: MessageCode.CH_AskActivityAwardLog_Res, callFunc: self._resActivityDetaileRecord},
            
        ];


        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener[i] = cf.addNetListener(msgData.msgId, msgData.callFunc.bind(self));
        }
            this._state = state;
            self.initBaseUI();

        },
    
        _resActivityDetaileRecord:function(msg){
            var resMsg = $root.CH_AskActivityAwardLog_Res.decode(msg);
            
            if(resMsg){

                
                for(var a = 0; a < resMsg.actAwardList.length; a++){
                   
                    if(a < 8){
                        
                        if(this._state == "0"){
                            var namepoint = (Math.floor(resMsg.actAwardList[a].name/100 +0.0000001))/100;
                            var name = new ccui.LabelBMFont("每日"+namepoint+"积分", Res.LOBBY.font_resource_093);
                            name.setPosition(285, 510-(50*a));
                            name.setScale(1);
                            this.addChild(name);

                        }else if (this._state == "1"){
                            var namepoint = (Math.floor(resMsg.actAwardList[a].name/100 +0.0000001))/100;
                            var name = new ccui.LabelBMFont("累积"+namepoint+"积分", Res.LOBBY.font_resource_093);            
                            name.setPosition(285, 510-(50*a));
                            name.setScale(1);
                            this.addChild(name);

                        }
                        

                    var receivedgold = new ccui.LabelBMFont(resMsg.actAwardList[a].recivedGold , Res.LOBBY.font_resource_093);
                    receivedgold.setPosition(500, 510-(50*a));
                    receivedgold.setScale(1);
                    this.addChild(receivedgold);

                    var time = new ccui.LabelBMFont(resMsg.actAwardList[a].time , Res.LOBBY.font_resource_093);
                    time.setPosition(730, 510-(50*a));
                    time.setScale(1);
                    this.addChild(time);

                    var mygold = new ccui.LabelBMFont(resMsg.actAwardList[a].mygold , Res.LOBBY.font_resource_093);
                    mygold.setPosition(950, 510-(50*a));
                    mygold.setScale(1);
                    this.addChild(mygold);
                    }
                }
                


            }
            
        },

    initData: function () {
        var self = this;
        
        self._onNetMsgListener = null; //网络监听
        
        self._fishingInfo = {};//用于临时存储当日数据
        self._newFishingInfo = {};
        self._oldFishingInfo = {};//用于存储旧数据
        self._roomId = 0;
        self._showInfoMod = 1;     //当前显示数据的类型
    },

    initBaseUI: function () {
        var baseBg = this.baseBg = new cc.Scale9Sprite("redpack_recordWin.png");
        baseBg.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        baseBg.setContentSize(1050, 530);
        this.addChild(baseBg);

        // var decorateBg = new cc.Scale9Sprite("resource_012_9.png");
        // decorateBg.setPosition(baseBg.width / 2, baseBg.height / 2);
        // decorateBg.setContentSize(1010, 490);
        // baseBg.addChild(decorateBg);

        //生成装饰线
        var factorArray = [90, 140, 190, 240, 290, 340, 390, 440];
        for (var i = 0; i < 8; i++) {
            var leftLineSp = new cc.Sprite("#redpack_line.png");
            leftLineSp.setPosition(baseBg.width / 2 - 221, baseBg.height - factorArray[i]);
            leftLineSp.setScale(0.9);
            baseBg.addChild(leftLineSp);

            var rightLineSp = new cc.Sprite("#redpack_line.png");
            rightLineSp.setPosition(baseBg.width / 2 + 221, baseBg.height - factorArray[i]);
            rightLineSp.setScale(0.9);
            rightLineSp.setFlippedX(true);
            baseBg.addChild(rightLineSp);
        }

        this.initBtn();
        this.addAllTextChildOfTable();
        
      //  this.setFishTableState(false, false);
       // this.setDetaileTableState(false, false);
       
    },

    initBtn: function () {
        var self = this;
        var baseBg = this.baseBg;

        //关闭按钮
        var closeBtn = new ccui.Button("redpack_close.png", "redpack_close.png", "redpack_close.png", ccui.Widget.PLIST_TEXTURE);
        closeBtn.setPosition(baseBg.width - 20, baseBg.height - 20);
        closeBtn.setZoomScale(0.1);
        closeBtn.setScale(1);
        closeBtn.setFlippedX(true);
        closeBtn.setPressedActionEnabled(true);
        baseBg.addChild(closeBtn);
        closeBtn.addClickEventListener(function () {
          //  self._ActivityRedPack.BtnRecall();
            self.removeFromParent(true);
        });
    },

    //添加流水报表以及捕鱼报表的所有子类显示UI
    addAllTextChildOfTable: function () {
        var self = this;
        var baseBg = this.baseBg;

        //添加文字描述项 并生成原始文字显示项 捕鱼报表分页
        var textArr = ['奖励项目', '领取金币', '领取时间', '领取后身上金币'];
        var textPos = [170, 390, 610, 840];
        //描述TextUI的数组
        var textUiArr = this.textUiArr = [];
        //显示详细数据的UI数组
        var showTextUiArr = this.showTextUiArr = [];
        for (var i = 0; i < 7; i++) {
            textUiArr[i] = new ccui.LabelBMFont(textArr[i], Res.LOBBY.font_resource_093);
            textUiArr[i].setPosition(textPos[i], baseBg.height-65 );
           // textUiArr[i].setColor(cc.color(0, 255, 255));
            textUiArr[i].setScale(1);
            baseBg.addChild(textUiArr[i]);

            showTextUiArr[i] = [];
            for (var j = 0; j < 5; j++) {
                if (i == 6) {
                    showTextUiArr[i][j] = new ccui.Button("tableButton.png", "tableButton.png", "tableButton.png", ccui.Widget.PLIST_TEXTURE);
                    showTextUiArr[i][j].setPosition(textPos[i] + 5, baseBg.height - 220 - 50 * j);
                    showTextUiArr[i][j].setZoomScale(0.1);
                    showTextUiArr[i][j].setScale(0.7);
                    showTextUiArr[i][j].setPressedActionEnabled(true);
                    baseBg.addChild(showTextUiArr[i][j]);
                
            
                    var _btnName = new ccui.LabelBMFont("详情", Res.LOBBY.font_resource_093);
                    _btnName.setPosition(showTextUiArr[i][j].width / 2 + 2, showTextUiArr[i][j].height / 2);
                    _btnName.setName("btnName");
                    showTextUiArr[i][j].addChild(_btnName);


                    // showTextUiArr[i][j].addClickEventListener(function () {
                    //     self._showInfoMod = 3;
                    //     backShow.setVisible(true);
                    //     self._recordIndex = self._pageIndex;
                    //     self._noDataPrompt.setVisible(false);
                    //     self.setCostTableState(false, false);
                    //     self.setFishTableState(false, false);
                    //     var strTimeArr = this.getName();
                    //     self._detaileCheckId = this.getTag();
                    //     if (strTimeArr && self._detaileCheckId) {
                    //         var timeArray = strTimeArr.split("|");
                    //         self._roomId = timeArray[2].substr(timeArray[2].length - 4, timeArray[2].length);
                    //         if (self._detaileInfo[self._detaileCheckId]) {
                    //             self.setDetaileTableState(true, true);
                    //             self.refreshPageNum(self._detaileInfo[self._detaileCheckId]);
                    //             self.updateItemReord(self._detaileInfo[self._detaileCheckId]);
                    //         }
                    //         else {
                    //             var startTime = timeArray[0];
                    //             var endTime = timeArray[1];
                    //             var strRoomId = timeArray[2];
                    //             self.askForFishDetaileRecord(startTime, endTime, strRoomId);
                    //         }
                    //     }
                    // });
                }
                else {
                    showTextUiArr[i][j] = new ccui.LabelBMFont("", Res.LOBBY.font_resource_093);
                    showTextUiArr[i][j].setPosition(textPos[i], baseBg.height - 220 - 50 * j);
                    showTextUiArr[i][j].setScale(0.6);
                    baseBg.addChild(showTextUiArr[i][j]);
                }
             }
        }

     

    },


    hide: function () {
        this._super();
    },

    onClear: function () {
        this._super();
    }
});

