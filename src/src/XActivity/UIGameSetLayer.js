var UIGameSetLayer = BaseLayer.extend({

    ctor: function (_target) {
        this._super();

        var self = this;
        self._target = _target;

        //按钮
        var setBtn = new ccui.Button();
        setBtn.loadTextureNormal("hall_015.png", ccui.Widget.PLIST_TEXTURE);
        setBtn.setAnchorPoint(1, 0.5);
        setBtn.setTag("setBtn");
        setBtn.setPosition(0, 0);
        setBtn.addClickEventListener(function () {
            //显示面板逻辑
            
            self.show();
        });
        self.addChild(setBtn);

        this._initSet();
    },


    _initSet: function () {
        var self = this;
        //背板
        var spBk = self._spBK = new cc.Sprite("#hall_014.png");
        spBk.setAnchorPoint(1, 0.5);

        spBk.setPosition(-5, 0);
        spBk.setVisible(false);
        self.addChild(spBk, -1);
        self._btnArray = [];

        var info = ["hall_001.png", "hall_004.png", "fishery_024.png"];
        for (var i = 0; i < 3; i++) {
            var fakeBtn = self._btnArray[i] = new ccui.Button();
            fakeBtn.loadTextureNormal(info[i], ccui.Widget.PLIST_TEXTURE);
            fakeBtn.setScale(0);
            fakeBtn.setAnchorPoint(0.5, 0.5);
            fakeBtn.setPosition(130 + i * 90, spBk.height / 2 );
            fakeBtn.setPressedActionEnabled(true);
            fakeBtn.setTag(UIGameSetLayer.Btn.HELP+i);
            fakeBtn.addTouchEventListener(self._btnCallBack, self);
            spBk.addChild(fakeBtn);
        }
    },

    /**
     * 面板展开
     */
    show: function () {
        var self = this;
        self._spBK.stopAllActions();
        if (self._spBK.isVisible()) {
            self._spBK.setVisible(false);
            self.getChildByTag("setBtn").setVisible(true);
        } else {
            self.getChildByTag("setBtn").setVisible(false);
            self._spBK.setVisible(true);

            for (var i = 0; i < 3; i++) {
                self._btnArray[i].setScale(0);
                self._btnArray[i].stopAllActions();
                self._btnArray[i].runAction(cc.sequence(
                    cc.delayTime(i * 0.1),
                    cc.scaleTo(0.25, 1.1),
                    cc.scaleTo(0.2, 1)
                    
                ));
            }
        }
    },

    hide: function () {
        var self = this;
        self._spBK.stopAllActions();
        self._spBK.setVisible(false);
        self.getChildByTag("setBtn").setVisible(true);
    },

    _btnCallBack: function (sender, type) {
        var self = this;
        var tag = sender.getTag();
        if (type == ccui.Widget.TOUCH_ENDED) {
            cf.SoundMgr.playEffect(8);
            switch (tag) {
                case UIGameSetLayer.Btn.EXIT://返回大厅
                    cf.autolock = false;
                    cf.btnDisable = false;
                    cf.dragon_gold = false;
                    cf.dragon_wood = false;
                    cf.goldwoodDisable = false;
                    cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UI_LOBBY);
                    break;
                case UIGameSetLayer.Btn.OPTION://打开设置
                    self.hide();
                    cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UI_OPTION);
                    break;
                case UIGameSetLayer.Btn.HELP://打开帮助
                    self.hide();
                    cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UI_HELP);
                    /*cf.EffectMgr.showDragonEffect(2568);
                    cf.dispatchEvent(BaseRoom.DoEvent, {
                        flag: BaseRoom.EventFlag.UPDATE_DRAGON_BALL_UI,
                        ballNum: 0
                    });*/
                    break;
            }
        }
    }


});

UIGameSetLayer.Btn = {
    HELP: 0,
    OPTION: 1,
    EXIT: 2     //< 返回大厅
};