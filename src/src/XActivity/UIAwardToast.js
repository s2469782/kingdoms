var UIAwardToast = BaseLayer.extend({
    _target: null,          //< 创建窗口对象
    _callback: null,        //< 确定按钮回调函数
    _arrReward: null,        //< 奖励列表
    _node: null,
    _bClick: false,
    _bGold: false,
    _dcType: -1,             //< 统计类型
    _includeLottery: false, //< 奖励中包含兑换券
    _skinOfFort: false,

    init: function (arrReward, callback, target, isGold, dcType,showAnimate) {
        var self = this;
        //开启触摸
        self.setTouchEnabled(true);
        self._target = target;
        self._callback = callback;
        self._arrReward = arrReward;
        self._dcType = dcType;

        self._bGold = isGold;
        // 压黑遮罩
        var pFadeBoard = new cc.LayerColor(cc.color(0, 0, 0, 180));
        self.addChild(pFadeBoard);

        //根节点
        self._node = new cc.Node();
        self._node.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        self.addChild(self._node);

        var baseNode = new cc.Node();
        baseNode.setPosition(0, 0);
        baseNode.setScale(0);
        self._node.addChild(baseNode, -1);

        //showAnimate参数 确认是否执行获得动画
        if(showAnimate == null || showAnimate == 1) {
            //获得动画
            var rewardSpine = ActionMgr.createSpine(Res.LOBBY.reward_Json, Res.LOBBY.reward_Atlas);
            rewardSpine.setAnimation(0, "reward", true);
            rewardSpine.setPosition(0, 180);
            baseNode.addChild(rewardSpine);
        }
        else {
            var spAwardShow = new cc.Sprite("#lostScene.png");
            spAwardShow.setPosition(0,0);
            baseNode.addChild(spAwardShow);
        }

        //底图
        var bkNode = new cc.Node();
        bkNode.setPosition(0, -40);
        baseNode.addChild(bkNode, -1);

        // var spBK = new cc.Sprite("#reward_bk.png");
        // spBK.setPosition(0, 130);
        // bkNode.addChild(spBK);
        baseNode.runAction(ActionMgr.createDelayBounce(0.1, 1, 0.3, 0.1, 0.1, 0));


        //道具信息
        var ItemData = cf.Data.ITEM_DATA;
        var length = self._arrReward.length;
        //获取本地玩家数据
        var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
        for (var i = 0; i < length; i++) {
            var _itemId = self._arrReward[i].nItemID;
            cc.log("获得奖励itemId = " + _itemId);

            //创建奖品道具
            var spNode = new cc.Node();
            spNode.setScale(0);
            self._node.addChild(spNode);
            //创建特效
            var spEffect = new cc.Sprite("#reward_002.png");
            spEffect.setPosition(0, 0);
            spEffect.setScale(1.5);
            spEffect.runAction(cc.repeatForever(cc.rotateBy(4,360)));
            spNode.addChild(spEffect, -1);

            //创建道具
            var spItem = new cc.Sprite("#" + ItemData[_itemId].itemIcon);
            spNode.addChild(spItem);

            var _num = self._arrReward[i].nNum;
            if (self._arrReward[i].nError != 0)
                _num = self._arrReward[i].nItemNum;
            //奖品数量
            var labelNum = new ccui.Text(ItemData[_itemId].itemName + "x" + _num,  cf.Language.FontName, 30);
            labelNum.setPosition(0, -80);
            labelNum.setTextColor(cc.color.YELLOW);
            labelNum.enableOutline(cc.color(165, 42, 42),2);
            spNode.addChild(labelNum);

            //弹出
            spNode.setPosition((i * 2 - (length - 1)) * 120, -30);
            if(showAnimate != null) {
                //参数为0 执行失败动画
                if(showAnimate == 0) {
                    spEffect.setVisible(false);
                    spNode.setPosition((i * 2 - (length - 1)) * 120, 50);
                    labelNum.setString(ItemData[_itemId].itemName + _num);
                }
                else {
                    //修改筹码获取数量显示
                    labelNum.setString(ItemData[_itemId].itemName + "+" + _num);
                }
            }
            spNode.runAction(ActionMgr.createDelayBounce(0.2 * i + 0.1, 1.2, 0.25, 0.1, 0.1, 0));

            //判断道具类型进行逻辑处理
            var _itemType = parseInt(ItemData[_itemId].Type);
            switch (_itemType) {
                case cf.ItemType.ItemType01://礼包
                    break;
                case cf.ItemType.ItemType02://装备，限时皮肤
                    localPlayerModel.addFortSkin(_itemId, null, self._arrReward[i].strEndTime);
                    labelNum.setString(ItemData[_itemId].itemName + "x1");
                    break;
                case cf.ItemType.ItemType03://道具
                    var strEndTime = null;
                    if (self._arrReward[i].strEndTime) {
                        strEndTime = self._arrReward[i].strEndTime;
                    }
                    localPlayerModel.setPlayerItem(_itemId, self._arrReward[i].nItemNum, strEndTime, self._arrReward[i].nError);
                    break;
                case cf.ItemType.ItemType04://其他
                    break;
                case cf.ItemType.ItemType05://货币
                    switch (_itemId) {
                        case cf.ItemID.GOLD://金币
                            localPlayerModel.setGoldNum(self._arrReward[i].nItemNum);
                            break;
                        case cf.ItemID.DIAMOND://钻石
                            localPlayerModel.setDiamondNum(self._arrReward[i].nItemNum);
                            break;
                        case cf.ItemID.LOTTERY://奖券
                            self._includeLottery = true;
                            localPlayerModel.setLottery(self._arrReward[i].nItemNum);
                            break;
                        case cf.ItemID.ACCPOINTS://积分
                            localPlayerModel.setFAccPoints(self._arrReward[i].nItemNum);
                            break;
                        case cf.ItemID.VIP_EXP:
                            var exp = self._arrReward[i].nItemNum;
                            localPlayerModel.setVipExp(exp);

                            //计算vip等级
                            var vipLevel = localPlayerModel.getVipLevel();
                            if (vipLevel >= cf.VipMaxLv) {
                                break;
                            }
                            var levelCost = cf.Data.VIP_DATA[vipLevel].totalPay;
                            while (exp >= levelCost) {
                                localPlayerModel.setVipLevel(vipLevel + 1);
                                //isUpgrade = true;
                                vipLevel = localPlayerModel.getVipLevel();
                                levelCost = cf.Data.VIP_DATA[vipLevel].totalPay;
                                if (vipLevel >= cf.VipMaxLv) {
                                    break;
                                }
                            }
                            break;
                        case cf.ItemID.CHIPS:
                            localPlayerModel.setPlayerChipNum(self._arrReward[i].nItemNum);
                            if(dcType == cf.DCType.DC_EXCHANGE) {
                                localPlayerModel.setPlayerChipNum(self._arrReward[i].nItemNum / 100);
                            }
                            break;
                    }
                    break;
                case cf.ItemType.ItemType06://炮台皮肤
                    localPlayerModel.addFortSkin(_itemId, null, self._arrReward[i].strEndTime);
                    labelNum.setString(ItemData[_itemId].itemName + "x1");
                    this._skinOfFort = true;
                    break;
            }

            switch (cf.SceneMgr.getCurSceneFlag()) {
                case cf.SceneFlag.LOBBY:
                    //刷新大厅UI
                    cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UI_UPDATE);
                    break;
                case cf.SceneFlag.GAME:
                    //刷新游戏UI
                    cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UPDATE_ROOM_UI);
                    break;
            }
        }
        var closeLabel = self._closeLabel = cc.LabelTTF.create(cf.Language.getText("text_1031"), cf.Language.FontName, 38);
        closeLabel.setPosition(0, -230);
        setTimeout(function () {
            self._bClick = true;
            //点击屏幕收取
            self._closeLabel.setOpacity(0);
            self._closeLabel.runAction(cc.fadeIn(0.5));
            self._node.addChild(self._closeLabel);
        }, 800);

        //获得奖励
        cf.SoundMgr.playEffect(10);
        return true;
    },

    onTouchBegan: function (touch, event) {
        return true;
    },

    onTouchMoved: function (touch, event) {
    },

    onTouchEnded: function (touch, event) {
        var self = this;
        //道具信息
        var ItemData = cf.Data.ITEM_DATA;
        var length = self._arrReward.length;
        for (var i = 0; i < length; i++) {
            if (self._arrReward[i].nError != 0 && ItemData[self._arrReward[i].nItemID].Type == cf.ItemType.ItemType03) {

                cf.UITools.showHintToast(ItemData[self._arrReward[i].nItemID].itemName + cf.Language.getText("text_1195"));
            }
        }

        if (self._bClick) {
            self._onFinish();
        }
    },

    _onFinish: function () {
        var self = this;
        if (self._callback) {
            self._callback.call(self._target);
        }
        self._node.runAction(cc.sequence(
            cc.scaleTo(0.2, 0),
            cc.callFunc(function () {
                self.removeFromParent(true);
            })
        ));

        if (self._bGold) {
            switch (cf.SceneMgr.getCurSceneFlag()) {
                case cf.SceneFlag.LOBBY:
                    //刷新大厅UI
                    cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.EFFECT_GET_GOLD);
                    break;
                case cf.SceneFlag.GAME:
                    //刷新游戏UI
                    cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.EFFECT_GET_GOLD);
                    break;
            }
        }
    }
});

/**
 *
 * @param arrReward
 * @param callback
 * @param target
 * @param isGold
 * @param dcType
 * @returns {*}
 */
UIAwardToast.create = function (arrReward, callback, target, isGold, dcType,showAnimate) {
    if (dcType == undefined || dcType == null)
        dcType = -1;
    var Dlg = new UIAwardToast();
    if (Dlg && Dlg.init(arrReward, callback, target, isGold, dcType,showAnimate)) {
        return Dlg;
    }
    Dlg = null;
    return null;
};