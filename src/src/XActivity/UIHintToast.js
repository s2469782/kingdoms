var UIHintToast = BaseLayer.extend({
    _callback: null,
    _target: null,
    ctor: function (text, time, callback, target) {
        this._super();
        var self = this;

        self._callback = callback;
        self._target = target;

        var _label = new cc.LabelTTF(text, "Arial Bold", 35);
        _label.setAnchorPoint(0.5, 0.5);//讲锚点设置成中心
        _label.setPosition(cc.winSize.width / 2, cc.winSize.height / 2-17);
        _label.setColor(cc.color(255, 255, 255, 255));
        self.addChild(_label);


        // 压黑遮罩
        var pFadeBoard = new cc.Scale9Sprite("resource_028_9.png");
        pFadeBoard.ignoreAnchorPointForPosition(false);//在这里设置成false代表可以更改
        pFadeBoard.setAnchorPoint(0.5, 0.5);//讲锚点设置成中心
        pFadeBoard.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        pFadeBoard.setContentSize(_label.getContentSize().width + 50, _label.getContentSize().height + 20);
        self.addChild(pFadeBoard, -1);

        var action1 = new cc.FadeIn(0.2);
        var action2 = new cc.FadeOut(0.4);
        var action3 = new cc.DelayTime(time);

        self.runAction(new cc.Sequence(
            action1, action3, action2, cc.callFunc(function () {
                if (self._callback) {
                    self._callback.call(self._target);
                }
            }, this)));

    }
});

/**
 * 创建UIHintToast
 * @param text  提示内容
 * @param time  提示的持续时间
 * @param callback
 * @param target
 * @returns {*}
 */
UIHintToast.createDlg = function (text, time, callback, target) {
    return new UIHintToast(text, time, callback, target);
};