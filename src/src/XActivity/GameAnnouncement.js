var GameAnnouncement = cc.Node.extend({
    _cliperNode: null,
    _waitForPlayList: null,
    _scrollInfoLength: null,
    _scrolling: null,

    ctor: function () {
        var self = this;
        self._super();
        self.init();
        self.scheduleUpdate();
    },

    update: function () {
        this.checkScrollInfoHasChange();
        this.checkRichTextCanScroll();
        
        
    },

    checkScrollInfoHasChange: function () {
        var length = MsgPool.getInstance().getAnnouncementArrayLength();
        var scrollInfoArray = MsgPool.getInstance().getAnnouncementArray();
        if (length > this._scrollInfoLength && ( length > 0 && this._scrollInfoLength > 0)) {
            //直接将数据插入到等待数组中
            this._scrollInfoLength = length;
            this._waitForPlayList.unshift(scrollInfoArray[length - 1]);
        }
    },

    checkRichTextCanScroll: function () {
        var presentTime = (new Date().getTime());
        var scrollInfoArray = MsgPool.getInstance().getAnnouncementArray();
        if (this._waitForPlayList.length > 0 && !this._scrolling) {
            var waitLength = this._waitForPlayList.length;
            for (var i = 0; i < waitLength; i++) {
                if (presentTime > this._waitForPlayList[i].strEndTime) {
                    this._waitForPlayList.splice(i, 1);
                    waitLength -= 1;
                    continue;
                }
                else {
                    this.addServiceRichTextFunction(this._waitForPlayList[i]);
                    this._waitForPlayList.splice(i, 1);
                    break;
                }
            }
        }
        else {
            var baseLength = MsgPool.getInstance().getAnnouncementArrayLength();
            if (baseLength > 0) {
                if (!this._scrollInfoLength) {
                    this._scrollInfoLength = baseLength;
                }
                for (var index = 0; index < baseLength; index++) {
                    //cc.log(presentTime + ";;;;;;;;;;;;;;;;;;;;" + scrollInfoArray[index]._nextPlayTime + ",,,,,,,,,,,,,,,,,,,,,,,,,,,");
                    if (presentTime > scrollInfoArray[index].strEndTime) {
                        scrollInfoArray.splice(index, 1);
                        baseLength -= 1;
                        continue;
                    }
                    if (presentTime > scrollInfoArray[index].nextPlayTime) {
                        scrollInfoArray[index].nextPlayTime = presentTime + parseFloat(scrollInfoArray[index].nIntercycle) * 1000;
                        if (!this._scrolling) {
                            cc.log("do scroll=========");
                            this.addServiceRichTextFunction(scrollInfoArray[index]);
                            continue;
                        }
                        else {
                            cc.log("push scroll=========" + index);
                            if (this._waitForPlayList.indexOf(scrollInfoArray[index]) == -1) {
                                this._waitForPlayList.push(scrollInfoArray[index]);
                            }
                        }
                    }
                    else  if(scrollInfoArray[index].strEventType != undefined) {
                        if (this._waitForPlayList.indexOf(scrollInfoArray[index]) == -1) {
                            this._waitForPlayList.push(scrollInfoArray[index]);
                        }
                        scrollInfoArray.splice(index, 1);
                        baseLength -= 1;
                        continue;
                    }
                }
            }
        }
    },

    init: function () {
        this._waitForPlayList = [];
        this.initScrollGameAnnounment();
    },

    //初始化跑马灯
    initScrollGameAnnounment: function () {
        var self = this;
        self._cliperNode = new cc.ClippingNode();

        var fakebg = new cc.Sprite("#resource_022.png");
        fakebg.setAnchorPoint(0.5,0);
        fakebg.setPosition(cc.winSize.width/2, 570);
        fakebg.setTag(10001);
        fakebg.setCascadeOpacityEnabled(true);
        fakebg.setOpacity(0);
        self.addChild(fakebg);

        var horn = new cc.Sprite("#resource_011.png");
        horn.setAnchorPoint(1,0.5);
        horn.setPosition(0, fakebg.height/2);
        fakebg.addChild(horn);

        var stencil = new cc.LayerColor(cc.color(0, 0, 0, 0));
        self._cliperNode.setStencil(stencil);
        stencil.setContentSize(fakebg.width, fakebg.height);
        stencil.setAnchorPoint(0, 0);
        stencil.setPosition((cc.winSize.width-fakebg.width)/2, 570);

        self._cliperNode.setPosition(0, 0);
        self.addChild(self._cliperNode);
    },

    /**
     * 将属性从单一的文本更改为文本数组
     */
    addServiceRichTextFunction: function (scrollInfo) {
        var richText = new ccui.RichText();
        //richText.ignoreContentAdaptWithSize(true);
        richText.anchorX = 0.5;
        richText.anchorY = 0;
        if (cc.sys.isNative) {
            richText.setAnchorPoint(cc.p(0.5, -0.5));
        }
        var strTitle = scrollInfo.strTitle;
        if(scrollInfo.strEventType != undefined) {
            switch (scrollInfo.strEventType) {
                case cf.AnnouncementEventType.fishEvent:
                    var strTitle =scrollInfo.strTitle;
                    var strLevel = scrollInfo.strLevel;
                    cc.log(scrollInfo.strCondition3 + "=======================================");
                    var fishName = cf.Data.FISH_DATA[scrollInfo.strCondition3].fishName;
                    var cannonLevel = cf.Data.CANNON_LEVEL_DATA[scrollInfo.strCondition1].cannonlevel;
                    var goldNumOfGet = cannonLevel * scrollInfo.strCondition2;
                    var show = true;//是否显示金币文字
                    if(scrollInfo.nItemID != 0){
                        var itemName = cf.Data.ITEM_DATA[scrollInfo.nItemID].itemName;
                        goldNumOfGet = itemName+"x"+scrollInfo.strCondition2;
                        show = false;
                    }
                    var content1 = new ccui.RichElementText(1, cc.color.WHITE, 255, "手起刀落,", cf.Language.FontName, 23);
                   // var content2 = new ccui.RichElementText(1, cc.color(64,224,205), 255, cannonLevel, cf.Language.FontName, 20);
                    var content3 = new ccui.RichElementText(1, cc.color.WHITE, 255, "击破", cf.Language.FontName, 23);
                    var content4 = new ccui.RichElementText(1, cc.color(64,224,205), 255, "敌军"+fishName, cf.Language.FontName, 23);
                    var content5 = new ccui.RichElementText(1, cc.color.WHITE, 255, "！获得", cf.Language.FontName, 23);
                    var content6 = new ccui.RichElementText(1, cc.color.YELLOW, 255, goldNumOfGet, cf.Language.FontName, 23);
                    var content7 = new ccui.RichElementText(1, cc.color.YELLOW, 255, "赏金！", cf.Language.FontName, 23);

                    var level = new ccui.RichElementText(1, cc.color.RED, 255, cf.Language.getText("text_1154") + strLevel + "", cf.Language.FontName, 23);
                    var localtitle = new ccui.RichElementText(1, cc.color(245,245,10), 255, cf.Language.getText("text_1445"), cf.Language.FontName, 23);
                    var title = new ccui.RichElementText(1, cc.color(0,255,6), 255, strTitle, cf.Language.FontName, 23);
                   
                   
                    richText.pushBackElement(title);
                    richText.pushBackElement(content1);
                    richText.pushBackElement(content3);
                    richText.pushBackElement(content4);
                    richText.pushBackElement(content5);
                    richText.pushBackElement(content6);
                    if(show)
                        richText.pushBackElement(content7);
                    break;
                case cf.AnnouncementEventType.pokerEvent://这里变成龙珠跑马灯
               
                    var title = new ccui.RichElementText(1, cc.color(0,255,6), 255, scrollInfo.strTitle, cf.Language.FontName, 23);
                    var content1 = new ccui.RichElementText(1, cc.color.WHITE, 255, "已无人能挡！", cf.Language.FontName, 23);
                    var content2 = new ccui.RichElementText(1, cc.color.WHITE, 255, "连破七阵发现宝物库", cf.Language.FontName, 23);
                  //  var content4 = new ccui.RichElementText(1, cc.color.WHITE, 255, scrollInfo.strCondition1, cf.Language.FontName, 20);
                    //var content4 = new ccui.RichElementText(1, cc.color(255,0,240), 255, "一甕财宝", cf.Language.FontName, 23);
                    var content5 = new ccui.RichElementText(1, cc.color.WHITE, 255, ",获得", cf.Language.FontName, 23);
                    var content6 = new ccui.RichElementText(1, cc.color.YELLOW, 255, scrollInfo.strCondition2, cf.Language.FontName, 23);
                    var content7 = new ccui.RichElementText(1, cc.color.YELLOW, 255, "金币！！", cf.Language.FontName, 23);
                    richText.pushBackElement(title);

                    richText.pushBackElement(content1);
                    richText.pushBackElement(content2);
                    //richText.pushBackElement(content4);
                    richText.pushBackElement(content5);
                    richText.pushBackElement(content6);
                    richText.pushBackElement(content7);
                    break;
                case cf.AnnouncementEventType.lotteryEvent:
                case cf.AnnouncementEventType.scoreEvent:
                    var strTitle = scrollInfo.strTitle;
                    var strLevel = scrollInfo.strLevel;
                    var itemData = cf.Data.ITEM_DATA[scrollInfo.strCondition2];
                    //使用
                    var content1 = new ccui.RichElementText(1, cc.color(245,245,10), 255, cf.Language.getText("text_1442"), cf.Language.FontName, 23);
                    //兑换券/积分数量
                    var content2 = new ccui.RichElementText(1, cc.color(64,224,205), 255, scrollInfo.strCondition1, cf.Language.FontName, 23);
                    //兑换卷 赢得了
                    var consumableName;
                    if(scrollInfo.strEventType == cf.AnnouncementEventType.lotteryEvent) {
                        consumableName = cf.Language.getText("text_1486");
                    }
                    else {
                        consumableName = "筹码";
                    }
                    var content3 = new ccui.RichElementText(1, cc.color(245,245,10), 255, consumableName + cf.Language.getText("text_1444"), cf.Language.FontName, 23);
                    //物品
                    var content4 = new ccui.RichElementText(1, cc.color(26,245,10), 255, itemData.itemName, cf.Language.FontName, 23);

                    var level = new ccui.RichElementText(1, cc.color.RED, 255, cf.Language.getText("text_1154") + strLevel + "", cf.Language.FontName, 23);
                    var localtitle = new ccui.RichElementText(1, cc.color(245,245,10), 255, cf.Language.getText("text_1445"), cf.Language.FontName, 23);
                    var title = new ccui.RichElementText(1, cc.color.RED, 255, strTitle, cf.Language.FontName, 23);

                    richText.pushBackElement(level);
                    richText.pushBackElement(localtitle);
                    richText.pushBackElement(title);

                    richText.pushBackElement(content1);
                    richText.pushBackElement(content2);
                    richText.pushBackElement(content3);
                    richText.pushBackElement(content4);
                    if(scrollInfo.strCondition3 != 0) {
                        //数量
                        var content5 = new ccui.RichElementText(1, cc.color(64,224,205), 255, "x" + scrollInfo.strCondition3, cf.Language.FontName, 20);
                        richText.pushBackElement(content5);
                    }
                    break;
                case cf.AnnouncementEventType.rouletteEvent:
                case cf.AnnouncementEventType.prizesLuckeyEvent:
                    var strTitle = scrollInfo.strTitle;
                    var strLevel = scrollInfo.strLevel;
                    var itemData = cf.Data.ITEM_DATA[scrollInfo.strCondition1];
                    if(scrollInfo.strEventType == cf.AnnouncementEventType.rouletteEvent) {
                        consumableName = cf.Language.getText("text_1492");
                    }
                    else {
                        consumableName = cf.Language.getText("text_1493");
                    }
                    var content1 = new ccui.RichElementText(1, cc.color(245,245,10), 255, consumableName, cf.Language.FontName, 23);
                    //物品
                    var content2 = new ccui.RichElementText(1, cc.color(26,245,10), 255, itemData.itemName, cf.Language.FontName, 23);

                    var level = new ccui.RichElementText(1, cc.color.RED, 255, cf.Language.getText("text_1154") + strLevel + "", cf.Language.FontName, 23);
                    var localtitle = new ccui.RichElementText(1, cc.color(245,245,10), 255, cf.Language.getText("text_1445"), cf.Language.FontName, 23);
                    var title = new ccui.RichElementText(1, cc.color.RED, 255, strTitle, cf.Language.FontName, 23);

                    richText.pushBackElement(level);
                    richText.pushBackElement(localtitle);
                    richText.pushBackElement(title);

                    richText.pushBackElement(content1);
                    richText.pushBackElement(content2);
                    if(scrollInfo.strCondition2 != 0) {
                        //数量
                        var content3 = new ccui.RichElementText(1, cc.color(64,224,205), 255, "x" + scrollInfo.strCondition2, cf.Language.FontName, 20);
                        richText.pushBackElement(content3);
                    }
                    break;
                case cf.AnnouncementEventType.dragonJP:
                   
                    var strTitle =scrollInfo.strTitle;
                    var condition2 = scrollInfo.strCondition2;
                    var condition3 = scrollInfo.strCondition3+"";

                    var content1 = new ccui.RichElementText(1, cc.color.WHITE, 255, "受命于天！", cf.Language.FontName, 23);
                    var content2 = new ccui.RichElementText(1, cc.color.WHITE, 255, "在", cf.Language.FontName, 23);
                    var content3 = new ccui.RichElementText(1, cc.color.WHITE, 255, "之际，寻获", cf.Language.FontName, 23);
                    var content4 = new ccui.RichElementText(1, cc.color.WHITE, 255, "，获得", cf.Language.FontName, 23);
                    var content5 = new ccui.RichElementText(1, cc.color.YELLOW, 255, "金币！", cf.Language.FontName, 23);
                    var content6 = new ccui.RichElementText(1, cc.color(255,0,240), 255, "传国玉玺", cf.Language.FontName, 23)
                   // var content6 = new ccui.RichElementText(1, cc.color(64,224,205), 255, cf.Language.getText("text_1170")+"!!", cf.Language.FontName, 23);

                    if(condition3 == "1003"){
                        var content7 = new ccui.RichElementText(1, cc.color.RED, 255, "火烧洛阳", cf.Language.FontName, 23);
                    }else if(condition3 == "1004"){
                        var content7 = new ccui.RichElementText(1, cc.color.RED, 255, "决战长安", cf.Language.FontName, 23);
                    }
                
                    var jp = new ccui.RichElementText(1, cc.color(245,245,10), 255, condition2, cf.Language.FontName, 23);
                    var localtitle = new ccui.RichElementText(1, cc.color(64,224,205), 255, cf.Language.getText("text_1445"), cf.Language.FontName, 23);
                    var title = new ccui.RichElementText(1, cc.color(0,255,6), 255, strTitle, cf.Language.FontName, 23);
                    
                    richText.pushBackElement(content1);
                   // richText.pushBackElement(localtitle);
                    richText.pushBackElement(title);
                    richText.pushBackElement(content2);
                    richText.pushBackElement(content7);
                    richText.pushBackElement(content3);
                    richText.pushBackElement(content6);
                    richText.pushBackElement(content4);
                    richText.pushBackElement(jp);
                    richText.pushBackElement(content5);

                    break;

                case cf.AnnouncementEventType.dragonSkill:
                   
                        var strTitle =scrollInfo.strTitle;
                        var skill = scrollInfo.strCondition1;
                        var money = scrollInfo.strCondition2;
                        var cannonLevel = scrollInfo.strCondition3+"";
                    


                        //var content0 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "玩家", cf.Language.FontName, 23);
                        var name = new ccui.RichElementText(1, cc.color(0,255,6), 255, strTitle, cf.Language.FontName, 23); 
                       
                        var content2 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "使出", cf.Language.FontName, 23);
                        var cannon = new ccui.RichElementText(1, cc.color(0,128,255), 255, cannonLevel, cf.Language.FontName, 23); 
                        var content3 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "倍炮使出", cf.Language.FontName, 23);

                        if(skill == 0){
                            var content1 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "指挥孙坚出击，", cf.Language.FontName, 23);
                            var content4 = new ccui.RichElementText(1, cc.color(76,153, 0), 255, "暴虎旋风斩", cf.Language.FontName, 23);
                        }else if(skill == 1){
                            var content1 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "指挥夏侯惇出击，", cf.Language.FontName, 23);
                            var content4 = new ccui.RichElementText(1, cc.color(76,153, 0), 255, "无限烈冰剑", cf.Language.FontName, 23);
                        }
                        else if(skill == 2){
                            var content1 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "指挥赵云出击，", cf.Language.FontName, 23);
                            var content4 = new ccui.RichElementText(1, cc.color(76,153, 0), 255, "龙胆流星击", cf.Language.FontName, 23);
                        }
                        else if(skill == 3){
                            var content1 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "指挥马超出击，", cf.Language.FontName, 23);
                            var content4 = new ccui.RichElementText(1, cc.color(76,153, 0), 255, "神枪飞焰袭", cf.Language.FontName, 23);
                        }else if(skill == 4){
                            var content1 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "指挥关羽出击，", cf.Language.FontName, 23);
                            var content4 = new ccui.RichElementText(1, cc.color(76,153, 0), 255, "青龙偃月", cf.Language.FontName, 23);
                        }else if(skill == 5){
                            var content1 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "指挥三兄弟出击，", cf.Language.FontName, 23);
                            var content4 = new ccui.RichElementText(1, cc.color(76,153, 0), 255, "桃园之誓", cf.Language.FontName, 23);
                        }
                        
                        var content5 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "，获得了", cf.Language.FontName, 23);
                        var gold =  new ccui.RichElementText(1, cc.color(0,128,255), 255, money, cf.Language.FontName, 23);
                        var content6 = new ccui.RichElementText(1, cc.color(255,255,0), 255, "金币!", cf.Language.FontName, 23);
    
                       
                        //richText.pushBackElement(content0);
                        richText.pushBackElement(name);
                        richText.pushBackElement(content1);
                        richText.pushBackElement(content2);
                        //richText.pushBackElement(cannon);
                        //richText.pushBackElement(content3);
                        richText.pushBackElement(content4);
                        richText.pushBackElement(content5);
                        richText.pushBackElement(gold);
                        richText.pushBackElement(content6);
    
                    break;

            }
        }
        else {
            var strContent = scrollInfo.strContent;
            var title = new ccui.RichElementText(1, cc.color.YELLOW, 255, strTitle, cf.Language.FontName, 20);
            //var content = new ccui.RichElementText(1, cc.color.RED, 255, strContent, cf.Language.FontName, 20);
            if (title) {
                richText.pushBackElement(title);
                ParserOfString.getInstance().parsing(strContent,richText);
            }
        }
        richText.y = 565;
        richText.x = cc.winSize.width + richText.width / 2;
        this._cliperNode.addChild(richText, 1);
        this.doScrollAction(richText);
    },

    doScrollAction: function (richText) {
        var self = this;
        self._scrolling = true;
        self.getChildByTag(10001).stopAllActions();
        self.getChildByTag(10001).setOpacity(150);
        if (!richText) {
            return;
        }
        var moveto = cc.moveTo(14, cc.p(-richText.width / 2, 565));
        var callfuncRemove = cc.callFunc(function () {
            self._scrolling = false;

            //在公告移动播放后如果间隔时间大于3s则暂时消失
            var fadeOut = cc.fadeOut(3);
            self.getChildByTag(10001).runAction(fadeOut);
            this.removeFromParent();
        }, richText);
        //文本移动的序列化动作
        var sequence = cc.sequence(moveto, callfuncRemove);
        richText.runAction(sequence);
    },

    initPopupsGameAnnouncement: function (textInfo) {
        var dialog = UIDialog.createCustom(textInfo, cf.Language.getText("text_1224"), null, false, function () {
            this.removeFromParent();
        }, this);
        this.addChild(dialog);
    }

});