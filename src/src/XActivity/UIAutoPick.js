var  UIAutoPick = BaseLayer.extend({
    _spBK: null,
    _maskLayer: null,
    _tabBtnArray: [],
    _tabNodeArray: [],
    _checkSprite: [],
    _checkCircle: [],
    _showBulletNum: 2,
    _bulletNum: [100, 500, 1000, 5000, "无限"],
    _SkillIcon: null,
    
    ctor: function () {
        this._super();
        var self = this;
        
        

        //创建遮罩层
        self._maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        self._maskLayer.setPosition(-543-cc.winSize.width/2, cc.winSize.height/2 - 560 );
        self.addChild(self._maskLayer, -100);

        //背景
        var spBK = self._spBK = new cc.Scale9Sprite("resource_019_9.png");
       // spBK.setCapInsets(cc.rect(57, 54, 88, 92));
        spBK.setContentSize(600 * 2, 564);
       // spBK.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        spBK.setPosition(-550, 170 );
        //spBK.setScale(0.9);
        self.addChild(spBK);

        // var title = new cc.Scale9Sprite("resource_075.png");
        // title.setPosition(spBK.width / 2, spBK.height / 2 +290);
        // //title.setContentSize(1070, 480);
        // //spFrame.setScale(0.73);
        // spBK.addChild(title);

        var titlelabel = new cc.Scale9Sprite("title.png");
        titlelabel.setPosition(spBK.width/2, spBK.height-30);
        spBK.addChild(titlelabel);
        

        // var spFrame = new cc.Scale9Sprite("resource_012_9.png");
        // spFrame.setPosition(spBK.width / 2, spBK.height / 2);
        // spFrame.setContentSize(1150, 520);
        // //spFrame.setScale(0.73);
        // spBK.addChild(spFrame);

        //退出按钮
        var closeBtn = new ccui.Button();
        closeBtn.loadTextureNormal("resource_021.png", ccui.Widget.PLIST_TEXTURE);
        closeBtn.setPosition(spBK.width - 35, spBK.height-40);
        closeBtn.setPressedActionEnabled(true);
        closeBtn.addClickEventListener(function () {
            var commonRoom = cf.RoomMgr.getRoom();
            var player = commonRoom._locPlayer;
            cf.SkillMgr.resetLockSkill(player.getSeatID());
            player._lockSkillEffEnd();
            cf.autofishData = [];
            //判断下是否是渔场
            if(cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.GAME){
                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UI_CONTINUEGAME);
                
            }
            
            this._showBulletNum = 2;
            self.hide();
        });
        spBK.addChild(closeBtn);    
        self._initFishMap(); 
        this.setTouchEnabled(true);
    

    },
  
    /**
     * 初始化说明
     * @private
     */
    _initFishMap:function () {
        

        cf.autolock = false;
        cf.autofishData = [];
        
        

        var row = 0,col = 0,_fishType = -1, _h = 0;
        var prizeFishData = cf.Data.PRIZE_FISH_DATA;
        for(var i = 0; i < prizeFishData.length; ++i){
            var _data = prizeFishData[i];
            
            if(i != 25){
            //var _itemBK = new ccui.ImageView("resource_121.png",ccui.Widget.PLIST_TEXTURE);
            var _itemBK = new ccui.Button();
            _itemBK.loadTextureNormal("resource_012_9.png",ccui.Widget.PLIST_TEXTURE);
            
            this.addChild(_itemBK);
            _h = 116;
            
            _itemBK.setAnchorPoint(0,1);
            _itemBK.setScale(0.7);
            _itemBK.setOpacity(0);
            _itemSp = new ccui.ImageView(_data.fishPng,ccui.Widget.PLIST_TEXTURE);
            _itemSp.setPosition(_itemBK.width/2,_itemBK.height/2+5);
            _itemSp.setScale(1);
          
            _itemBK.addChild(_itemSp);
        
            
            this._checkCircle[i] = new ccui.Button();
            this._checkCircle[i].loadTextureNormal("resource_122.png",ccui.Widget.PLIST_TEXTURE);
            this._checkCircle[i].setOpacity(0);
            this._checkCircle[i].setScale(0.82);
            this._checkCircle[i].setPosition(_itemBK.width/2, _itemBK.height/2-10);
            this._checkCircle[i].pressIndex = i;
            this._checkCircle[i].addClickEventListener((sender, type)=>{
                
                // console.log("index",this._fishData.indexOf(prizeFishData[sender.pressIndex].index));
                if(cf.autofishData.includes(prizeFishData[sender.pressIndex].index)){
                    
                    cf.autofishData.splice(cf.autofishData.indexOf(prizeFishData[sender.pressIndex].index), 1);
                }
               // this._checkCircle[i].setScale(0.7);
                this._checkCircle[sender.pressIndex].setVisible(false);
                this._checkSprite[sender.pressIndex].setVisible(false);
                        

            });
           
           // this._checkCircle[i].setAnchorPoint(0,1);
            this._checkCircle[i].setVisible(false);
            //this._checkCircle[i].setScale(1.2);
            _itemBK.addChild(this._checkCircle[i]);

            

            if(col>=10){
                col = 0;
                row++;
            }
            
            //_itemBK.setPosition(5+(_itemBK.width+10)*col, _h-(_itemBK.height+10)*row);
            _itemBK.setPosition(-1030+(_itemBK.width-50)*col, _h-(_itemBK.height-50)*row+180);
            _itemLabel = new cc.LabelBMFont(_data.fishScore[0]+"倍",Res.LOBBY.font_resource_083);
            _itemLabel.setAnchorPoint(1,1);
            _itemLabel.setPosition(_itemBK.width/2+40,_itemBK.height/2-30);
            _itemLabel.setScale(1);
            _itemBK.addChild(_itemLabel);

            

            
            this._checkSprite[i] = new cc.Scale9Sprite("resource_122.png");
            this._checkSprite[i].setAnchorPoint(1,1);
            this._checkSprite[i].setPosition(_itemBK.width/2+95, _itemBK.height+9);
            this._checkSprite[i].setBlendFunc( cc.SRC_ALPHA,cc.ONE); 
            this._checkSprite[i].setScale(1.2);
            this._checkSprite[i].setVisible(false);
            _itemBK.addChild(this._checkSprite[i]);
            

            _itemBK.pressIndex = i;
            
            _itemBK.addClickEventListener((sender, type)=>{
                if(!cf.autofishData.includes(prizeFishData[sender.pressIndex].index)){
                    cf.autofishData.push(prizeFishData[sender.pressIndex].index);
                }
                
                this._checkCircle[sender.pressIndex].setVisible(true);
                this._checkSprite[sender.pressIndex].setVisible(true);
                
            });
            col++;

        }

        }

        var targetlabel = new cc.Scale9Sprite("targetlabel.png");
        targetlabel.setPosition(-900, 330);
        //title.setContentSize(1070, 480);
        //spFrame.setScale(0.73);
        this.addChild(targetlabel);

        

        // var line = new cc.Scale9Sprite("blinkLline.png");
        // line.setPosition(-800, 335);
        // line.setRotation(180);
        // //title.setContentSize(1070, 480);
        // //spFrame.setScale(0.73);
        // this.addChild(line);

        //全部清除按鈕
        var eraseBtn = new ccui.Button();
        eraseBtn.loadTextureNormal("button_003.png",ccui.Widget.PLIST_TEXTURE);
        eraseBtn.setPosition(-340, 330);
        
        eraseBtn.addClickEventListener((sender, type)=>{
            for(var a = 0; a < 25; a++){
                
                
                if(cf.autofishData.includes(prizeFishData[a].index)){
                    cf.autofishData =  cf.autofishData.splice(prizeFishData[a].index, 1);
                }
                this._checkCircle[a].setVisible(false);
                this._checkSprite[a].setVisible(false);
                

            }
            
            
            

        });
        this.addChild(eraseBtn);

        var eraseLabel = new cc.Scale9Sprite("erase.png");
        eraseLabel.setPosition(eraseBtn.width/2, eraseBtn.height/2);
        eraseBtn.addChild(eraseLabel);

        //全選按鈕
        var AllSelectBtn = new ccui.Button();
        AllSelectBtn.loadTextureNormal("button_003.png",ccui.Widget.PLIST_TEXTURE);
       // AllSelectBtn.setContentSize(200,200);
        AllSelectBtn.setPosition(-150, 330);
        AllSelectBtn.addTouchEventListener((sender, type)=>{
            for(var a = 0; a < 25; a++){
               
                if(!cf.autofishData.includes(prizeFishData[a].index)){
                    cf.autofishData.push(prizeFishData[a].index);
                }

                this._checkCircle[a].setVisible(true);
                this._checkSprite[a].setVisible(true);
                
            }
        });
        this.addChild(AllSelectBtn);

        var AllSelectLabel = new cc.Scale9Sprite("allselect.png");
        AllSelectLabel.setPosition(AllSelectBtn.width/2, AllSelectBtn.height/2);
        AllSelectBtn.addChild(AllSelectLabel);
           // this.addChild(_itemLayout[0]);

        //底背景
        var bottom =  new cc.Scale9Sprite("resource_047.png");
        bottom.setPosition(-555, -100);
        bottom.setContentSize(1200, 100);
        bottom.setOpacity(0);
        //title.setContentSize(1070, 480);
        //spFrame.setScale(0.73);
        this.addChild(bottom);

        var chooseLabel = new cc.Scale9Sprite("chooselabel.png");
        chooseLabel.setPosition(bottom.width/2-380,bottom.height/2+30);
        bottom.addChild(chooseLabel);

        var bulletBack = new cc.Scale9Sprite("resource_028_9.png");
        bulletBack.setContentSize(200, 40);
        bulletBack.setPosition(bottom.width/2-100,bottom.height/2+30);
        bottom.addChild(bulletBack);

        var bulletLabel = new cc.LabelBMFont(this._bulletNum[this._showBulletNum],Res.LOBBY.font_resource_093);
        bulletLabel.setScale(1.3);
        bulletLabel.setPosition(bulletBack.width/2, bulletBack.height/2+2);
        bulletBack.addChild(bulletLabel);

        var minus = new ccui.Button();
        minus.loadTextureNormal("button_002.png",ccui.Widget.PLIST_TEXTURE);
        minus.setPosition(bottom.width/2-200,bottom.height/2+30);
        minus.addClickEventListener((sender, type)=>{
            
            if(this._showBulletNum > 0){
                this._showBulletNum = this._showBulletNum -1;
            }else{
                this._showBulletNum = 0;
            }
            bulletLabel.setString(this._bulletNum[this._showBulletNum]);
        });
        bottom.addChild(minus);

        var plus = new ccui.Button();
        plus.loadTextureNormal("button_001.png",ccui.Widget.PLIST_TEXTURE);
        plus.setPosition(bottom.width/2,bottom.height/2+30);
        plus.addClickEventListener((sender, type)=>{
            
            if(this._showBulletNum < 4){
                this._showBulletNum = this._showBulletNum +1;
            }else{
                this._showBulletNum = 4;
            }
                bulletLabel.setString(this._bulletNum[this._showBulletNum]);
            
        });
        bottom.addChild(plus);



    
        var yesBtn = new ccui.Button();
        yesBtn.loadTextureNormal("button_005.png",ccui.Widget.PLIST_TEXTURE);
        yesBtn.setPosition(bottom.width/2+350,bottom.height/2+30);
        yesBtn.addClickEventListener((sender, type)=>{
        

            //判断下是否是渔场
            if(cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.GAME){
                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UI_CONTINUEGAME);
                
            }
            var localSeatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
            cf.SkillMgr.setLockSkillOpen(localSeatId);
            cf.SkillMgr.setLockSkillLastTime(localSeatId, 0);
            
            this.checkBeiShuLV();
            // var commonRoom = cf.RoomMgr.getRoom();
            // var player = commonRoom._locPlayer;
            // var skill = this.getChildByTag(10001);
            //     if (!player._bAutoFire) {
            //         player._bAutoFire = true;
            //         player._lastAutoOnFire = player._bAutoFire;
            //        //skill.setSpriteFrame("fishery_028.png");

            //     }
            //     else {
            //         player._bAutoFire = false;
            //         player._lastAutoOnFire = false;
            //         //skill.setSpriteFrame("fishery_015.png");
            //     }
            //     player.addPromptOfAutoFire();

            this.hide();

        });
        bottom.addChild(yesBtn);

        var yesLabel = new cc.Scale9Sprite("yeslabel.png");
        yesLabel.setScale(0.8);
        yesLabel.setPosition(yesBtn.width/2,yesBtn.height/2);
        yesBtn.addChild(yesLabel);
        

    },

//判斷魚群存在的最高倍數魚
checkBeiShuLV: function(){

    if(cf.autolock == false){
        if(this._bulletNum[this._showBulletNum] == "无限"){
            cf.autolockBulletNum = 9999990;
        }else{
            cf.autolockBulletNum = this._bulletNum[this._showBulletNum];
        }
    }

    
    
    
    var fishMgr = cf.FishMgr;
    var fishMap = fishMgr.getFishMap();
    var length = fishMap.size();
    var commonRoom = cf.RoomMgr.getRoom();
    var player = commonRoom._locPlayer;
    var maxV = [];
    var position = [];
    
    if(player._bAutoFire){
        player._bAutoFire = false;
        player._bTouchFire = false;
        player._lastAutoOnFire = false;
        player.addPromptOfAutoFire();
  
        cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UPDATE_AUTO_UI);
    }

    var fishData = cf.autofishData.sort(function(a,b){
        return b - a;
    });
    
    if(fishData.length > 0){
    player.openAutoLockEffect();
    
    
    for(var a = 0; a < fishMap._elements.length; a++){
        //for(var b = 0; b< fishData.length; b++){
      // console.log(this._fishData[a], fishMap._elements[a].value._fishModel._fishId);
            if(fishData.includes(fishMap._elements[a].value._fishModel._fishId)){
                   // cf.autolock = true;
                    
                   // player.setLockElement(fishMap._elements[a].value);
                   // player.setAutoLock(fishMap._elements[a].value);
                    maxV.push({index:fishMap._elements[a].value._fishModel._fishId , value:fishMap._elements[a].value });
                    
            }
       // console.log( fishMap._elements[a].value._fishModel._fishId  );
       // console.log(prizeFishData[fishMap._elements[a].value._fishModel._fishId]);

       // }
        
    }
    

    maxV.sort((a ,b )=>{
        return b.index -a.index
    });
        if(maxV.length > 0){
            cf.autolock = true;
            
            player.setAutoLock(maxV[0].value);
            
            
            
        }else{
           var obj = setTimeout(()=>{
            
            this.checkBeiShuLV();
           }, 2000) ;
           
        }
    }else{
        cf.SkillMgr.resetLockSkill(player.getSeatID());
        player._lockSkillEffEnd();
        player.closeAutoLockEffect();

    }

   
    

},
    
        
});
