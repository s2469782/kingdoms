var SkillIcon = ccui.Button.extend({
    _spEffect: null,
    _gunSkill: null,
    _skillId: null,
    _bDoubleSchedule:false,
    _autolockWin: false,
    _autoEffect: null,
    bUse:false,
    ctor: function (skillId, pos) {
        this._super();
        this._skillId = skillId;
        this.initSkillIcon(skillId, pos);
    },
    initSkillIcon: function (skillId, pos) {
        switch (skillId) {
            case cf.SkillId.lockSkill:
                
                    this.initLockSkillIcon(pos);
                
                break;
            case cf.SkillId.iceSkill:
                
                    this.initIceSkillIcon(pos);
                
                
                break;
            case cf.SkillId.summonSkill:
                
                    this.initSummonSkillIcon(pos);
                
                
                break;
            case cf.SkillId.rageSkill:
                
                    this.initRageSkillIcon(pos);
                
                
                break;
            case cf.SkillId.gunSkill:
               
                    this.initGunSkillIcon(pos);
                
               
                break;
            case cf.SkillId.auto:
                
                    this.initAutoIcon(pos);
                
                
                break;
            case cf.SkillId.autolock:
                
                    this.initAutoLockIcon(pos);
                
                break;
        }
    },
    initAutoLockIcon: function(pos){
        this.loadTextureNormal("auto_btn.png", ccui.Widget.PLIST_TEXTURE);
        this.setPressedActionEnabled(true);
        this.setAnchorPoint(0.5, -0.1);
       // this.setColor(cc.color(255, 105, 105));
        this.setPosition(pos);
        this.setTag(10007);
        this.addTouchEventListener(this.skillBtnCallBack);

        // var lockSkill = new cc.ProgressTimer(new cc.Sprite("#auto_btn.png"));
        // lockSkill.type = cc.ProgressTimer.TYPE_RADIAL;
        // lockSkill.setAnchorPoint(0, 0);
        // lockSkill.x = 0;
        // lockSkill.y = 0;
        // lockSkill.setPercentage(100);
        // lockSkill.setTag(10007);
        // this.addChild(lockSkill);

        // var lockSkillNum = new ccui.Text("自動", cf.Language.FontName,20);
        // lockSkillNum.setAnchorPoint(0.5, 0.5);
        // lockSkillNum.setPosition(this.width/2, 18);
        // lockSkillNum.setTag(10007);
        // lockSkillNum.setScale(1.2);
        // //lockSkillNum.setTextColor(cc.color(189,239,343));
        // lockSkillNum.enableOutline(cc.color(5, 103, 168),2);
        // this.addChild(lockSkillNum);
        //lockSkillNum.setVisible(false);


        
    },
    initLockSkillIcon: function (pos) {
        this.loadTextureNormal("fishery_004.png", ccui.Widget.PLIST_TEXTURE);
        this.setPressedActionEnabled(true);
        this.setAnchorPoint(0.5, 0);
        this.setColor(cc.color(105, 105, 105));
        this.setPosition(pos);
        this.setTag(10001);
        this.addTouchEventListener(this.skillBtnCallBack);
        
        var lockSkill = new cc.ProgressTimer(new cc.Sprite("#fishery_004.png"));
        lockSkill.type = cc.ProgressTimer.TYPE_RADIAL;
        lockSkill.setAnchorPoint(0, 0);
        lockSkill.x = 0;
        lockSkill.y = 0;
        lockSkill.setPercentage(100);
        lockSkill.setTag(10001);
        this.addChild(lockSkill);

        // var lockSkillNum = new ccui.Text("锁定", cf.Language.FontName,20);
        // lockSkillNum.setAnchorPoint(0.5, 0.5);
        // lockSkillNum.setPosition(this.width/2, 18);
        // lockSkillNum.setTag(10001);
        // lockSkillNum.setScale(1.2);
        // //lockSkillNum.setTextColor(cc.color(189,239,343));
        // lockSkillNum.enableOutline(cc.color(5, 103, 168),2);
        // lockSkill.addChild(lockSkillNum);
        //lockSkillNum.setVisible(false);

        this._spEffect = new cc.Sprite("#fishery_022.png");
        this._spEffect.setPosition(this.width / 2, this.height / 2);
        this._spEffect.runAction(cc.repeatForever(cc.rotateBy(3, 360)));
        this._spEffect.setVisible(false);
        this._spEffect.setScale(1.2);
        this.addChild(this._spEffect, -1);

        //this._updateItemShowUi(cf.SkillId.lockSkill);

    },

    initIceSkillIcon: function (pos) {
        this.loadTextureNormal("fishery_006.png", ccui.Widget.PLIST_TEXTURE);
        this.setPressedActionEnabled(true);
        this.setAnchorPoint(0, 0);
        this.setColor(cc.color(105, 105, 105));
        this.setPosition(pos);
        this.setTag(10002);
        this.addTouchEventListener(this.skillBtnCallBack);

        var iceSkill = new cc.ProgressTimer(new cc.Sprite("#fishery_006.png"));
        iceSkill.type = cc.ProgressTimer.TYPE_RADIAL;
        iceSkill.setAnchorPoint(0, 0);
        iceSkill.x = 0;
        iceSkill.y = 0;
        iceSkill.setPercentage(100);
        iceSkill.setTag(10001);
        this.addChild(iceSkill);

        var iceSkillNum = new cc.LabelBMFont("x0".itemNum, Res.LOBBY.font_resource_086);
        iceSkillNum.setAnchorPoint(0.5, 0.5);
        iceSkillNum.setPosition(60, 18);
        iceSkillNum.setTag(10001);
        iceSkillNum.setScale(1.2);
        iceSkill.addChild(iceSkillNum);

        this._updateItemShowUi(cf.SkillId.iceSkill);
    },

    initSummonSkillIcon: function (pos) {
        this.loadTextureNormal("fishery_007.png", ccui.Widget.PLIST_TEXTURE);
        this.setPressedActionEnabled(true);
        this.setAnchorPoint(1, 0);
        this.setColor(cc.color(105, 105, 105));
        this.setPosition(pos);
        this.setTag(10003);
        this.addTouchEventListener(this.skillBtnCallBack);

        var summonSkill = new cc.ProgressTimer(new cc.Sprite("#fishery_007.png"));
        summonSkill.type = cc.ProgressTimer.TYPE_RADIAL;
        summonSkill.setAnchorPoint(0, 0);
        summonSkill.x = 0;
        summonSkill.y = 0;
        summonSkill.setPercentage(100);
        summonSkill.setTag(10001);
        this.addChild(summonSkill);

        var summonSkillNum = new cc.LabelBMFont("x 0".itemNum, Res.LOBBY.font_resource_086);
        summonSkillNum.setAnchorPoint(0.5, 0.5);
        summonSkillNum.setPosition(60, 18);
        summonSkillNum.setTag(10001);
        summonSkillNum.setScale(1.2);
        summonSkill.addChild(summonSkillNum);

        this._updateItemShowUi(cf.SkillId.summonSkill);
    },

    initRageSkillIcon: function (pos) {
        this.loadTextureNormal("fishery_005.png", ccui.Widget.PLIST_TEXTURE);
        this.setPressedActionEnabled(true);
        this.setAnchorPoint(0, 0);
        this.setColor(cc.color(105, 105, 105));
        this.setPosition(pos);
        this.setTag(10004);
        this.addTouchEventListener(this.skillBtnCallBack);


        var rageSkill = new cc.ProgressTimer(new cc.Sprite("#fishery_005.png"));
        rageSkill.type = cc.ProgressTimer.TYPE_RADIAL;
        rageSkill.setAnchorPoint(0, 0);
        rageSkill.x = 0;
        rageSkill.y = 0;
        rageSkill.setPercentage(100);
        rageSkill.setTag(10001);
        this.addChild(rageSkill);

        var rageSkillNum = new cc.LabelBMFont("x 0".itemNum, Res.LOBBY.font_resource_086);
        rageSkillNum.setAnchorPoint(0.5, 0.5);
        rageSkillNum.setPosition(60, 18);
        rageSkillNum.setTag(10001);
        rageSkillNum.setScale(1.2);
        rageSkillNum.setVisible(false);
        rageSkill.addChild(rageSkillNum);

        this._spEffect = new cc.Sprite("#fishery_022.png");
        this._spEffect.setPosition(this.width / 2, this.height / 2);
        this._spEffect.runAction(cc.repeatForever(cc.rotateBy(3, 360)));
        this._spEffect.setVisible(false);
        this._spEffect.setScale(1.2);
        this.addChild(this._spEffect, -1);

        this._updateItemShowUi(cf.SkillId.rageSkill);
    },

    initGunSkillIcon: function (pos) {
        this.loadTextureNormal("fishery_031.png", ccui.Widget.PLIST_TEXTURE);
        this.setPressedActionEnabled(false);
        this.setAnchorPoint(1, 0);
        this.setColor(cc.color(105, 105, 105));
        this.setOpacity(0);
        this.setPosition(pos);
        this.setTag(10005);
        this.addTouchEventListener(this.skillBtnCallBack);

        var gunSkill = this._gunSkill = new cc.Sprite("#fishery_031.png");
        gunSkill.setAnchorPoint(0, 0);
        gunSkill.x = 0;
        gunSkill.y = 0;
        gunSkill.setTag(10001);
        this.addChild(gunSkill);

        var gunDiamondIcon = new cc.Sprite();
        gunDiamondIcon.setScale(0.5);
        gunDiamondIcon.setAnchorPoint(0.5, 0.5);
        gunDiamondIcon.setPosition(30, 15);
        gunDiamondIcon.setTag(10002);
        gunDiamondIcon.setVisible(false);
        gunDiamondIcon.setFlippedX(true);
        gunSkill.addChild(gunDiamondIcon);

        /*var titSkill = new ccui.Text("0", cf.Language.FontName, 20);
        titSkill.setVisible(false);
        titSkill.setPosition(gunSkill.width / 2, gunSkill.height/2);
        titSkill.enableOutline(cc.color(30, 144, 255), 1.5);
        titSkill.setTag(10003);
        gunSkill.addChild(titSkill);*/

        this._updateItemShowUi(cf.SkillId.gunSkill);
        this._spEffect = new cc.Sprite("#fishery_022.png");
        this._spEffect.setPosition(this.width / 2, this.height / 2);
        this._spEffect.runAction(cc.repeatForever(cc.rotateBy(3, 360)));
        this._spEffect.setVisible(false);
        this._spEffect.setScale(1.2);
        this.addChild(this._spEffect, -1);
        if (cf.PlayerMgr.getLocalPlayer().getFortMode() == Player.FortMode.DOUBLE) {
            this._spEffect.setVisible(true);
            gunSkill.setSpriteFrame("fishery_031.png");
        }
        else {
            this._spEffect.setVisible(false);
            gunSkill.setSpriteFrame("fishery_032.png");
        }
    },
    initAutoIcon: function (pos) {
        this.loadTextureNormal("fishery_015.png", ccui.Widget.PLIST_TEXTURE);
        this.setPressedActionEnabled(false);
        this.setAnchorPoint(1, 0);
        this.setColor(cc.color(105, 105, 105));
        this.setOpacity(0);
        this.setPosition(pos);
        this.setTag(10006);
        this.addTouchEventListener(this.skillBtnCallBack);

        var autoSkill = new cc.Sprite("#fishery_015.png");
        autoSkill.setAnchorPoint(0, 0);
        autoSkill.x = 0;
        autoSkill.y = 0;
        autoSkill.setTag(10001);
        this.addChild(autoSkill);
        //this._updateItemShowUi(cf.SkillId.auto);
    },
    skillBtnCallBack: function (sender, type) {
        var tag = sender.getTag();
        if (type != ccui.Widget.TOUCH_ENDED) {
            return;
        }
        

        
            var player = cf.RoomMgr.getRoom()._locPlayer;
        switch (tag) {
            case 10001:
                
                    var itemLockUnBind = player.getPlayerModel().getPlayerItemNum(cf.ItemID.SKILL_1030101) || "";
                    var itemLockBind = player.getPlayerModel().getPlayerItemNum(cf.ItemID.SKILL_1030111) || "";
                    var itemLockNum = itemLockUnBind + itemLockBind;
                    this.lockSkillCall(itemLockNum);
                    break;
                

            case 10002:
                if(!cf.btnDisable){
                    var itemIceUnBind = player.getPlayerModel().getPlayerItemNum(cf.ItemID.SKILL_1030102) || "";
                    var itemIceBind = player.getPlayerModel().getPlayerItemNum(cf.ItemID.SKILL_1030112) || "";
                    var itemIceNum = itemIceUnBind + itemIceBind;
                    this.iceSkillCall(itemIceNum);
                    break;
                }
            case 10003:
                if(!cf.btnDisable){
                    var itemSummonUnBind = player.getPlayerModel().getPlayerItemNum(cf.ItemID.SKILL_1030103) || "";
                    var itemSummonBind = player.getPlayerModel().getPlayerItemNum(cf.ItemID.SKILL_1030113) || "";
                    var itemSummonNum = itemSummonUnBind + itemSummonBind;
                    this.summonSkillCall(itemSummonNum);
                    break;
                }
            case 10004:
                if(!cf.btnDisable){
                    var itemRageUnBind = player.getPlayerModel().getPlayerItemNum(cf.ItemID.SKILL_1030104) || "";
                    var itemRageBind = player.getPlayerModel().getPlayerItemNum(cf.ItemID.SKILL_1030114) || "";
                    var itemRageNum = itemRageUnBind + itemRageBind;
                    this.rageSkillCall(itemRageNum);
                break;
                }
            case 10005:
                if(!cf.btnDisable){
                    this.gunSkillCall();
                }
                
                break;
            case 10006:
                if(!cf.btnDisable){
                    this.autoCallBack(player);
                }
                break;
            case 10007:
                // var itemLockUnBind = player.getPlayerModel().getPlayerItemNum(cf.ItemID.SKILL_1030101) || "";
                // var itemLockBind = player.getPlayerModel().getPlayerItemNum(cf.ItemID.SKILL_1030111) || "";
                // var itemLockNum = itemLockUnBind + itemLockBind;
               // this.lockSkillCall(itemLockNum);
               if(!cf.btnDisable){
                    this.autolockCallBack(player);
               }
                break;
        
    }
    },
    autolockeffectReset: function(){
        this._autoEffect.setVisible(false);
        this._spEffect.setVisible(false);
    },
    autolockCallBack: function(player){
        
        // var commonRoom = cf.RoomMgr.getRoom();
        // var player = commonRoom._locPlayer;
        if (player._bankrupt || !player._bFortCanFire) {
            //给出金币不足提示
            //cf.UITools.showHintToast(cf.Language.getText("text_1069"));
            return;
        }
        var win = new UIAutoPick();
        win.setPosition(3, -120);
        this.addChild(win);
        player.closeAutoLockEffect();
        if (!cf.SkillMgr.getLockSkillState(player.getSeatID())) {
            
            //如果道具为空 且炮倍满足条件
            /*if (!(itemLockNum > 0)) {
                //道具不足
                commonRoom.promptOfItemIsNotEnough();
                return;
            }*/
             //win.setVisible(true);
            // this._autoEffect.setVisible(true);
            // this._spEffect.setVisible(true);
            
            // this._spEffect.setVisible(true);
            this._consumePropsOfSkill(cf.SkillId.lockSkill);
            //  helpLayer = new UIAutoPick();
           // this._consumePropsOfSkill(cf.SkillId.autolock);
        }else{
            
            //  this._spEffect.setVisible(false);
            //  this._autoEffect.setVisible(false);
            
            if(player.getLockElement()){
                //停止锁定
                var msg = new $root.SC_SkillEndMsg();
                msg.nSeatId = player.getSeatID();
                msg.nSkillId = cf.SkillId.lockSkill;

                var wr = $root.SC_SkillEndMsg.encode(msg).finish();
                cf.NetMgr.sendSocketMsg(MessageCode.SC_IceEnd, wr);
           }else{
                cf.SkillMgr.resetLockSkill(player.getSeatID());
                player._lockSkillEffEnd();
                cf.SkillMgr.resetLockSkill(player.getSeatID());
                player._lockSkillEffEnd();

            }
        }
    },
    lockSkillCall: function (itemLockNum) {
        var commonRoom = cf.RoomMgr.getRoom();
        var player = commonRoom._locPlayer;
        if (player._bankrupt || !player._bFortCanFire) {
            //给出金币不足提示
            cf.UITools.showHintToast(cf.Language.getText("text_1069"));
            return;
        }

        if (!cf.SkillMgr.getLockSkillState(player.getSeatID())) {
            //如果道具为空 且炮倍满足条件
            /*if (!(itemLockNum > 0)) {
                //道具不足
                commonRoom.promptOfItemIsNotEnough();
                return;
            }*/
            
           
            this._spEffect.setVisible(true);
            this._consumePropsOfSkill(cf.SkillId.lockSkill);
        }else{
            
            cf.autolock = false;
            this._spEffect.setVisible(false);
            player.AutoLockEffectClose();
            player.closeAutoLockEffect();
            if(player.getLockElement()){
                //停止锁定
                var msg = new $root.SC_SkillEndMsg();
                msg.nSeatId = player.getSeatID();
                msg.nSkillId = cf.SkillId.lockSkill;

                var wr = $root.SC_SkillEndMsg.encode(msg).finish();
                cf.NetMgr.sendSocketMsg(MessageCode.SC_IceEnd, wr);
            }else{
                cf.SkillMgr.resetLockSkill(player.getSeatID());
                player._lockSkillEffEnd();

            }
            
        }
    },

    iceSkillCall: function (itemIceNum) {
        var commonRoom = cf.RoomMgr.getRoom();
        var player = commonRoom._locPlayer;
        //冰冻未被使用时才可以
        if (!cf.SkillMgr.getIceSkillCooling()) {
            if (!(itemIceNum > 0)) {
                //道具数量不足               
                return;
            }
            this._consumePropsOfSkill(cf.SkillId.iceSkill);
        }
    },

    summonSkillCall: function (itemSummonNum) {
        var commonRoom = cf.RoomMgr.getRoom();
        if (!cf.SkillMgr.getSummonSkillCooling()) {
            if (!(itemSummonNum > 0)) {
                //道具不足              
                return;
            }
            this._consumePropsOfSkill(cf.SkillId.summonSkill);
        }
    },

    rageSkillCall: function (itemRageNum) {
        var commonRoom = cf.RoomMgr.getRoom();
        var player = commonRoom._locPlayer;
        var skillData = cf.Data.SKILLDATA[cf.SkillId.rageSkill];
        if (player._bankrupt || !player._bFortCanFire) {
            //给出金币不足提示
            cf.UITools.showHintToast(cf.Language.getText("text_1069"));
            return;
        }
        if (player._bAutoFire) {
            cf.UITools.showHintToast("自动开炮中，无法使用狂暴");
            return;
        }
        //只有在狂暴未开启状态下才可以使用技能
        if (!cf.SkillMgr.getRageSkillState(player.getSeatID())) {           
            this._spEffect.setVisible(true);
            this._consumePropsOfSkill(cf.SkillId.rageSkill);
        }else{
            this._spEffect.setVisible(false);
            //停止锁定
            var msg = new $root.SC_SkillEndMsg();
            msg.nSeatId = player.getSeatID();
            msg.nSkillId = cf.SkillId.rageSkill;

            var wr = $root.SC_SkillEndMsg.encode(msg).finish();
            cf.NetMgr.sendSocketMsg(MessageCode.SC_IceEnd, wr);
        }
    },
    /**
     * 切换单双炮
     */
    gunSkillCall: function () {
        this.changeGun();
    },
    autoCallBack: function (player) {
        //如何打断自动开炮
        if (player._bankrupt) {       
            return;
        }
        if (cf.SkillMgr.getRageSkillState(player.getSeatID())) {
            cf.UITools.showHintToast("狂暴中，无法使用自动开炮");
           return;
        }

        var skill = this.getChildByTag(10001);
        if (!player._bAutoFire) {
            player._bAutoFire = true;
            player._lastAutoOnFire = player._bAutoFire;
            skill.setSpriteFrame("fishery_028.png");

        }
        else {
            player._bAutoFire = false;
            player._lastAutoOnFire = false;
            skill.setSpriteFrame("fishery_015.png");
        }
        player.addPromptOfAutoFire();
    },
    /**
     * 将索引转换成为炮倍
     */
    indexToCannonLevel: function (index) {
        var data = cf.Data.CANNON_LEVEL_DATA;
        return data[index].cannonlevel;
    },

    /**
     * 检测技能的道具类型并显示
     * @param costLabel  { label }      显示道具数量的文本
     * @param itemNum    { number }     道具数量
     * @param skillId    { number }     技能id
     */
    _checkSkillItemCost: function (costLabel, itemNum) {
        if (itemNum != undefined && itemNum != "" && itemNum >= 0) {
            costLabel.setString("x" + itemNum);
        }
        else {
            costLabel.setString("x0");
        }
    },

    _updateItemShowUi: function (skillId) {
        var self = this;
        var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
        var skill = self.getChildByTag(10001);
        var skillNum = skill.getChildByTag(10001);
        switch (skillId) {
            case cf.SkillId.lockSkill:
                var itemLockUnBind = localPlayerModel.getPlayerItemNum(cf.ItemID.SKILL_1030101) || "";
                var itemLockBind = localPlayerModel.getPlayerItemNum(cf.ItemID.SKILL_1030111) || "";
                var itemLockNum = itemLockUnBind + itemLockBind;
                //cc.log("数据显示如下" + itemLockUnBind + "---------" + itemLockBind + itemLockNum);
                self._checkSkillItemCost(skillNum, itemLockNum);
                break;

            case cf.SkillId.iceSkill:
                var itemIceUnBind = localPlayerModel.getPlayerItemNum(cf.ItemID.SKILL_1030102) || "";
                var itemIceBind = localPlayerModel.getPlayerItemNum(cf.ItemID.SKILL_1030112) || "";
                var itemIceNum = itemIceUnBind + itemIceBind;
                self._checkSkillItemCost(skillNum, itemIceNum);
                break;

            case cf.SkillId.summonSkill:
                var itemSummonUnBind = localPlayerModel.getPlayerItemNum(cf.ItemID.SKILL_1030103) || "";
                var itemSummonBind = localPlayerModel.getPlayerItemNum(cf.ItemID.SKILL_1030113) || "";
                var itemSummonNum = itemSummonUnBind + itemSummonBind;
                self._checkSkillItemCost(skillNum, itemSummonNum);
                break;

            case cf.SkillId.rageSkill:
                var itemRageUnBind = localPlayerModel.getPlayerItemNum(cf.ItemID.SKILL_1030104) || "";
                var itemRageBind = localPlayerModel.getPlayerItemNum(cf.ItemID.SKILL_1030114) || "";
                var itemRageNum = itemRageUnBind + itemRageBind;
                self._checkSkillItemCost(skillNum, itemRageNum);
                break;
            case cf.SkillId.gunSkill:
                /*var titSkill = skill.getChildByTag(10003);
                var vipLimited = cf.Data.SKILLDATA[cf.SkillId.gunSkill].vip_limit;
                if (cf.PlayerMgr.getLocalPlayer().getVipLevel() < vipLimited) {
                    self._gunTimeOut(titSkill);
                }
                else {
                    titSkill.setVisible(false);
                }*/
                break;
            case cf.SkillId.auto:
                break;
            case cf.SkillId.autolock:
                
                break;
        }
    },

    /**
     *  道具消耗消息
     */
    _consumePropsOfSkill: function (skillId) {
        //使用技能消息
        cc.log("使用技能" + ".........................................................");
        var msg = new $root.CS_SkillUseMsg();
        msg.nIdLogin = cf.PlayerMgr.getLocalPlayer().getIdLogin();
        msg.nSkillId = skillId;
        var wr = $root.CS_SkillUseMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_SkillUseMsg, wr);
        cf.UITools.showLoadingToast(cf.Language.getText("text_1012"), 10, false, cf.RoomMgr.getRoom()._delegate);
    },

    /**
     * 显示限时双炮倒计时
     * @param label
     * @private
     */
    _gunTimeOut: function (label) {

        var _Num = cf.PlayerMgr.getLocalPlayer().getSinglePairCannon();
        if (_Num > 1) {
            if(this._bDoubleSchedule)
                return;
            //限时
            var skinTime = cf.timeFormat(_Num - cf.PlayerMgr.getLocalPlayer().getGameTime(), false);
            if (_Num > 0) {
                label.setVisible(true);
                //限时显示的时间
                label.setString(skinTime);
            }

            //启动倒计时
            var timeCallBack = function () {
                var _timeNum = _Num - cf.PlayerMgr.getLocalPlayer().getGameTime();
                if (_timeNum <= 0) {
                    //取消所有定时器
                    this.unschedule(timeCallBack);
                    label.setVisible(false);

                    this._bDoubleSchedule = false;
                } else {
                    //限时显示的时间
                    label.setString(cf.timeFormat(_timeNum, false));
                }
            };
            this.schedule(timeCallBack, 1);
            this._bDoubleSchedule = true;
        }
        else if (_Num == 0) {

            label.setVisible(true);
            //限时显示的时间

            var vipLimited = cf.Data.SKILLDATA[cf.SkillId.gunSkill].vip_limit;
            var strTip = cf.Language.getText("text_1355");
            strTip = strTip.format(vipLimited.toString());
            label.setString(strTip);
        }
    },
    /**
     * 切换炮逻辑
     */
    changeGun: function () {
        var commonRoom = cf.RoomMgr.getRoom();
        var player = commonRoom._locPlayer;
        if (player.isDoubleFort()) {
            //player.setFortModeSingle();
            player.tansFormFortMode();
        } else {
            //开启双炮
            //player.setFortModeDouble();
            player.tansFormFortMode();
        }
    },

    /**
     * 执行技能的冷却
     * @param skillid
     */
    doCoolAction: function (skillid, percentage) {
        var self = this;
        if (skillid != this._skillId) {
            return
        }
        var cdTime = cf.Data.SKILLDATA[skillid].skill_cd;
        var localSeatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
        var skillPer = 0;
        if (percentage && percentage > 0) {
            skillPer = percentage;
        }
        switch (skillid) {
            case cf.SkillId.lockSkill:
            case cf.SkillId.iceSkill:
            case cf.SkillId.summonSkill:
                if (skillid == cf.SkillId.lockSkill) {
                    cdTime = cf.SkillMgr.getLockSkillLastTime(localSeatId);
                }
                var skillIcon = self.getChildByTag(10001);
                skillIcon.setPercentage(skillPer);
                skillIcon.runAction(cc.progressTo(cdTime, 100));
                break;
            case cf.SkillId.rageSkill:
                var cdTime1 = cf.SkillMgr.getRageSkillLastTime(localSeatId);
                var rageSkill = self.getChildByTag(10001);
                rageSkill.setPercentage(skillPer);
                rageSkill.runAction(cc.progressTo(cdTime1, 100));
                break
        }
    }
});
