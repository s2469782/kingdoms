cf.UITools = {

    _tipToast: null,
    _loadingToast: null,
    _HintToast: null,

    /**
     * 显示Loading提示
     * @param text
     * @param time
     * @param mask
     * @param target
     * @param callback
     */
    showLoadingToast: function (text, time, mask, target,callback) {
        this.hideLoadingToast();

        if (this._loadingToast == null) {
            this._loadingToast = UIToastDlg.create(text, time,function () {
                this.hideLoadingToast();
                if(callback)
                    callback();
            },this);
            this._loadingToast.setTouchEnabled(true);
            this._loadingToast.setMask(mask);
            target.addChild(this._loadingToast,1000);
        }
    },

    /**
     * 隐藏loading提示
     */
    hideLoadingToast: function () {
        if (this._loadingToast) {
            this._loadingToast.removeFromParent(true);
            this._loadingToast = null;
        }
    },

    /**
     * 显示A区提示
     * @param text 提示文字
     * @param target 挂接父节点
     */
    showToast: function (text, target) {
        this.hideToast();

        if (this._tipToast == null) {
            //固定3秒提示时间
            this._tipToast = UIToastDlg.create(text, 2,function () {
                this.hideToast();
            },this);
            target.addChild(this._tipToast);
        }
    },

    /**
     * 隐藏A区提示
     */
    hideToast: function () {
        if (this._tipToast) {
            this._tipToast.removeFromParent(true);
            this._tipToast = null;
        }
    },

    /**
     * 显示HintToast提示
     * @param text 提示内容
     * @param delay 延迟消失时间
     * @param callback 回调函数
     */
    showHintToast: function (text, delay, callback) {
        cc.director.getNotificationNode().showToast(text,delay,callback);
    },
    
    hideHintToast:function () {
        cc.director.getNotificationNode().hideToast();
    }
};