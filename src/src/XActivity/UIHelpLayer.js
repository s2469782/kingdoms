var UIHelpLayer = BaseLayer.extend({
    _spBK: null,
    _maskLayer: null,
    _tabBtnArray: [],
    _tabNodeArray: [],
    _tabTagArray: [],
    _tabLightArray: [],
    _tag : ["#resource_013.png", "#resource_030.png", "#resource_031.png"],
    _tagTitle : ["resource_013.png", "resource_030.png", "resource_031.png"],
    _tagTitleBack: ["resource_013_1.png", "resource_030_1.png", "resource_031_1.png"],
    _titleAnnounce: ["title_layout.png", "title_rule.png", "title_library.png"],
    _title: null,
    ctor: function () {
        this._super();
        var self = this;

        //创建遮罩层
        self._maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        self.addChild(self._maskLayer, -100);

        //背景
        var spBK = self._spBK = new cc.Scale9Sprite("resource_019_9_2.png");
        // spBK.setCapInsets(cc.rect(57, 54, 88, 92));
        // spBK.setContentSize(551 * 2, 515);
        spBK.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        self.addChild(spBK);

        var spFrame = new cc.Scale9Sprite("resource_012_9.png");
        spFrame.setPosition(spBK.width / 2, spBK.height / 2);
        spFrame.setContentSize(1070, 480);


        var line_bottom = new cc.Sprite("#resource_050.png");
        line_bottom.setPosition(spBK.width - 25, spBK.height/2 - 245);
        spBK.addChild(line_bottom);

        var title = self._title =  new cc.Sprite("#title_layout.png");
        title.setPosition(spBK.width/2, spBK.height-40);
        spBK.addChild(title);
        //创建Tab按钮
        var tabText = ["界面说明", "游戏规则", "鱼图鉴"];
        for (var i = 0; i < 3; ++i) {
            
            var b = new cc.Sprite("#resource_012.png");
            b.setAnchorPoint(0, 0);
            //b.setPosition(50 + (b.width - 30) * i, spBK.height - 65);
            b.setPosition(spBK.width - 60 , 110 + (b.height -3 ) * i);
            spBK.addChild(b);

           
            var click = self._tabLightArray[i] =  new cc.Sprite("#resource_light.png");
            click.setAnchorPoint(0, 0);
            click.setPosition(spBK.width - 62 , 113 + (b.height ) * i);
            click.setScale(1);
            click.setBlendFunc( cc.SRC_ALPHA,cc.ONE);
            spBK.addChild(self._tabLightArray[i]);

            var tag = self._tabTagArray[i] =  new cc.Sprite(self._tag[i]);
            tag.setAnchorPoint(0, 0);
            tag.setPosition(spBK.width - 45 , 150 + (tag.height +70) * i);
            spBK.addChild(self._tabTagArray[i]);

            var btn = self._tabBtnArray[i] = new ccui.Button();
            btn.loadTextures("resource_light.png", "resource_light.png", "resource_light.png", ccui.Widget.PLIST_TEXTURE);
            //btn.setTitleOffset(0, -6);
            //btn.setTitleText(tabText[i]);
            //btn.setTitleFontSize(32);
            //btn.setTitleFontName(cf.Language.FontName);
            btn.setOpacity(0);
            btn.setAnchorPoint(0, 0);
            btn.setPosition(spBK.width - 45 , 150 + (tag.height +70) * i);
            //btn.setPosition(50 + (btn.width - 30) * i, spBK.height - 65);
            spBK.addChild(self._tabBtnArray[i]);

            // var btnName = new ccui.LabelBMFont(tabText[i],Res.LOBBY.font_resource_093);
            // btnName.setScale(1.2);
            // btnName.setPosition(btn.width/2,btn.height/2+15);
            // btn.addChild(btnName);
            //创建节点数组
            self._tabNodeArray[i] = new cc.Node();
            self._tabNodeArray[i].setPosition(spBK.width/2, spBK.height/2);
            spBK.addChild(self._tabNodeArray[i], 1);
        }
        var line_top = new cc.Sprite("#resource_050_1.png");
        line_top.setPosition(spBK.width - 25, spBK.height -100);
        spBK.addChild(line_top);
        
        //点击事件
        self._tabBtnArray[UIHelpLayer.TabFlag.INTRODUCTION].addClickEventListener(function () {
            self._showTabBtn(UIHelpLayer.TabFlag.INTRODUCTION);
            self._showTabContent(UIHelpLayer.TabFlag.INTRODUCTION);
        });
        self._tabBtnArray[UIHelpLayer.TabFlag.RULE].addClickEventListener(function () {
            self._showTabBtn(UIHelpLayer.TabFlag.RULE);
            self._showTabContent(UIHelpLayer.TabFlag.RULE);
        });
        self._tabBtnArray[UIHelpLayer.TabFlag.FISH_MAP].addClickEventListener(function () {
            self._showTabBtn(UIHelpLayer.TabFlag.FISH_MAP);
            self._showTabContent(UIHelpLayer.TabFlag.FISH_MAP);
        });

      //  spBK.addChild(spFrame);

        //退出按钮
        var closeBtn = new ccui.Button();
        closeBtn.loadTextureNormal("resource_021.png", ccui.Widget.PLIST_TEXTURE);
        closeBtn.setPosition(spBK.width - 20, spBK.height- 35);
        closeBtn.setPressedActionEnabled(true);
        closeBtn.addClickEventListener(function () {
            //判断下是否是渔场
            if(cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.GAME){
                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UI_CONTINUEGAME);
            }
            self.hide();
        });
        spBK.addChild(closeBtn);

        self._initIntroduction();
        self._initRule();
        self._initFishMap();
       // self._initScrollBar();
        //初始化显示Tab按钮
        self._showTabBtn(UIHelpLayer.TabFlag.INTRODUCTION);
        self._showTabContent(UIHelpLayer.TabFlag.INTRODUCTION);

        //屏蔽触摸
        this.setTouchEnabled(true);
      
    },

    show: function () {
        this._super();
        this._showTabBtn(UIHelpLayer.TabFlag.INTRODUCTION);
        this._showTabContent(UIHelpLayer.TabFlag.INTRODUCTION);
    },

    _showTabContent: function (tabFlag) {
        for (var i = 0; i < 3; ++i) {
            if (i == tabFlag) {
                this._tabNodeArray[tabFlag].setVisible(true);
                this._tabLightArray[tabFlag].setVisible(true);
            } else {
                this._tabNodeArray[i].setVisible(false);
                this._tabLightArray[i].setVisible(false);
            }
        }
    },

    _showTabBtn: function (tabFlag) {
        for (var i = 0; i < 3; ++i) {
            if (i == tabFlag) {
                this._tabBtnArray[tabFlag].setBright(true);
                this._tabBtnArray[tabFlag].setColor(cc.color.WHITE);
                this._tabTagArray[tabFlag].setSpriteFrame(this._tagTitle[tabFlag]);
                this._title.setSpriteFrame(this._titleAnnounce[tabFlag]);
                

            } else {
                this._tabBtnArray[i].setBright(false);
                this._tabTagArray[i].setSpriteFrame(this._tagTitleBack[i]);
                
            }
        }
    },

    

    

    
    /**
     * 初始化说明
     * @private
     */
    _initIntroduction: function () {
        var self = this;
        var spHelp = new cc.Sprite(Res.LOBBY.helperImg);
        spHelp.setPosition(0,0);
        spHelp.setScale(0.85);
        self._tabNodeArray[UIHelpLayer.TabFlag.INTRODUCTION].addChild(spHelp);
    },

    _initRule: function () {
        var listView = new ccui.ListView();
        listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        listView.setTouchEnabled(true);
        listView.setBounceEnabled(true);

        listView.setContentSize(cc.size(1060, 450));
        listView.setPosition(-this._spBK.width/2+80, -this._spBK.height/2+100);
        this._tabNodeArray[UIHelpLayer.TabFlag.RULE].addChild(listView);

        var item = new ccui.LabelBMFont(
            "    击破贼军将领金币多重送:\n"+
            "        1. 有机会击破敌营：击破7座敌营，便可得到宝物库，获得奖金。\n"+
            "        2. 贼军宝物库有机会找到传国玉玺，获得大彩金。\n"+
            "        3. 有机会讨取或俘虏贼军将领：随机友军会士气上升，集满可施放友军无双。\n"+
            "        4. 第二次集满友军无双时，加送最强的群体无双技，无条件轻松无双连击。\n"+
            "        5. 每日24点时，已击破的敌营数量会清空，请留意。\n\n"+
            "    常见问题与说明:\n"+
            "        1. 奖金计算方式:\n"+
            "            a. 玩家可选择武器倍率，越高则奖金越高\n"+
            "            b. 划面上贼军皆有其赏金倍率，越高则奖金越高。\n"+
            "            c. 点击划面便可发射砲弹，并扣除当前武器倍率数字的金币。\n"+
            "            d. 砲弹到边框会反弹，不会消失。\n"+
            "            e. 奖金计算公式：武器倍率x赏金倍率。\n"+
            "            例：用10000倍武器倍率击破300倍贼军将领，奖金为3000000金币。\n"+
            "        2. 关于击破敌营:\n"+
            "            a. 击破Boss级及将领级贼军，皆有机率能击破敌营，获得宝物库。\n"+
            "            b. 宝物库可获得最高100倍的额外奖金，此倍率会堆叠，3次后重置。\n"+
            "            c. 使用的武器倍率，直接影响额外奖金的金额与堆叠效果。\n"+
            "            d. 击破7座敌营，可能找到最大奖传国玉玺并获得大彩金。\n"+
            "            e. 获得大彩金时，不会同时得到额外奖金。\n"+
            "        3. 关于友军士气:\n"+
            "            a. 讨取及俘虏贼军将领时，会有掷骰机会，高机率让随机友军士气上升。\n"+
            "            b. 骰子若掷出1点时，代表没有友军士气上升。\n"+
            "            c. 当击破7座敌营时，会消耗所有友军士气来开启宝物库。\n"+
            "            d. 当一位友军士气满3格时，会使用友军无双技能来协助玩家。\n"+
            "                 此时最强的群体无双技的集气格也会增加一格。\n "+
            "            e. 再次集满3格士气时，群体无双技会气满，\n"+
            "                 并先加送一次最强的群体无双技。后续仍可施放集满3格的友军无双，\n"+
            "                 让您轻松无双连击。\n"+
            "            f. 玩家可自行选择无双发动的适当时机，再点击并发动技能。\n"+
            "            g. 当集满友军无双时，部分设定如自动索敌、连射等会先行关闭。\n"+
            "            h. 不同点数的友军，出场武将会不同，并有专属的台词及无双个人技：\n"+
            "                 无限烈冰剑：夏侯惇无双技。\n"+
            "                 暴虎旋风斩：孙坚无双技。\n"+
            "                 龙胆流星击：赵云无双技。\n"+
            "                 神枪飞燄袭：马超无双技。\n"+
            "                 青龙偃月：关羽无双技。\n"+
            "                 桃园之誓：集满集气格可施放。刘关张三兄弟共同施放的最强无双技。\n"
           
            
            
, Res.LOBBY.font_resource_093);
        listView.pushBackCustomItem(item);

        listView.setGravity(ccui.ListView.GRAVITY_CENTER_VERTICAL);
    },
    
    selectedItemEvent:function(sender, type){
       // cc.log("type:"+type);//type类型为0,1
       
        switch (type) {
            case ccui.ListView.EVENT_SELECTED_ITEM://选中item项
                
                console.log("select child index = ",            sender         );
                break;
            case ccui.ListView.ON_SELECTED_ITEM_END://单击ITEM项（没有拉动列表滚动）
                console.log("ccui.ListView.ON_SELECTED_ITEM_END");
                break;
            default:
                break;
        }
    },




    
    _initFishMap:function () {

        
        var listView = new ccui.ListView();
        listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        listView.setTouchEnabled(true);
        listView.setBounceEnabled(true);
        
        listView.setContentSize(cc.size(1060, 450));
        listView.setPosition(-this._spBK.width/2+80, -this._spBK.height/2+130);
        listView.addEventListener(this.selectedItemEvent, this);
       
                              
        this._tabNodeArray[UIHelpLayer.TabFlag.FISH_MAP].addChild(listView);
                         

        //滑动条背景
       
        // var loadBar = new cc.Scale9Sprite("resource_025_9.png");
        // loadBar.setAnchorPoint(0, 0.5);
        // //loadBar.setPosition(1160,560);
        // loadBar.setPosition(525, -this._spBK.height/2+450);
        // loadBar.setContentSize(360, 15);
        // loadBar.setRotation(90);

        // this._tabNodeArray[UIHelpLayer.TabFlag.FISH_MAP].addChild(loadBar, 0);

        // var button  = new ccui.Slider();
        // button.setScale9Enabled(true);
        // button.loadBarTexture("resource_025_9.png", 1);
        // button.loadProgressBarTexture("resource_024_9.png", 1);
        // button.loadSlidBallTextures("scrollbar.png", null, null, 1);
        // button.setCapInsetsBarRenderer(cc.rect(10, 13, 23, 4));
        // button.setCapInsetProgressBarRenderer(cc.rect(8, 13, 27, 4));
        // button.setContentSize(350, 13);
        // button.getVirtualRenderer().setOpacity(0);
        // button.setAnchorPoint(0, 0.5);
        
        // button.setRotation(90);
        // button.setPercent(0);
        // button.setPosition(525, -this._spBK.height/2+450);
        // button.addEventListener(this.selectedItemEvent, this);
        
        // this._tabNodeArray[UIHelpLayer.TabFlag.FISH_MAP].addChild(button);

        
        

        var _itemLayout = [];
        for(var i = 0; i <4;++i){
            _itemLayout[i] =  new ccui.Layout();
        }

        var row = 0,col = 0,_fishType = -1, _h = 0;
        var prizeFishData = cf.Data.PRIZE_FISH_DATA;
        for(var i = 0; i < prizeFishData.length; ++i){
            var _data = prizeFishData[i];

            var _itemBK = new ccui.ImageView("resource_121.png",ccui.Widget.PLIST_TEXTURE);
            switch (_data.fishType){
                case cf.FishType.COMMON:
                    _itemLayout[3].addChild(_itemBK);
                    _h = 140*3-30;
                    break;
                case cf.FishType.SKILL:
                    _itemLayout[1].addChild(_itemBK);
                    _h = 110;
                    break;
                case cf.FishType.BONUS:
                    _itemLayout[2].addChild(_itemBK);
                    _h = 110;
                    break;
                case cf.FishType.BOSS:
                    _itemLayout[0].addChild(_itemBK);
                    _h = 110;
                    break;
            }

            if(_fishType != _data.fishType){
                _fishType = _data.fishType;
                col = 0;
                row = 0;
            }

            _itemBK.setAnchorPoint(0,1);
            _itemBK.setOpacity(0);
            var _itemSp = new ccui.ImageView(_data.fishPng,ccui.Widget.PLIST_TEXTURE);
            _itemSp.setPosition(_itemBK.width/2,_itemBK.height/2);
            _itemSp.setScale(0.8);
            _itemBK.addChild(_itemSp);

            if(col>=7){
                col = 0;
                row++;
            }
            if(_data.fishType == cf.FishType.BOSS){
                _itemBK.setPosition(315+(_itemBK.width-10)*col, _h-(_itemBK.height+10)*row);
            }else if( _data.fishType == cf.FishType.SKILL){
                _itemBK.setPosition(375+(_itemBK.width-10)*col, _h-(_itemBK.height+10)*row);
            }else{
                _itemBK.setPosition(60+(_itemBK.width-10)*col, _h-(_itemBK.height+10)*row);
            }
            

            var _itemLabel = new cc.LabelBMFont(_data.fishScore[0]+"倍",Res.LOBBY.font_resource_083);
            _itemLabel.setAnchorPoint(1,1);
            _itemLabel.setPosition(_itemBK.width/2+35,_itemBK.height/2-30);
            _itemLabel.setScale(0.85);
            _itemBK.addChild(_itemLabel);

            //  var _itemName = new cc.LabelBMFont(_data.fishName,Res.LOBBY.font_resource_084);
            // _itemName.setAnchorPoint(0.5,0);
            // _itemName.setPosition(_itemBK.width/2,2);
            // _itemName.setScale(0.85);
            // _itemBK.addChild(_itemName);

            col++;


        }

        var _info = [
            {w:1060,h:176,text:"BOSS鱼", img: "library_boss.png"},
            {w:1060,h:176,text:"技能", img: "library_special.png"},
            {w:1060,h:176,text:"彩金鱼", img: "library_captain.png"},
            {w:1060,h:146*3+30,text:"普通鱼", img: "library_solider.png"}
        ];
        for(var i = 0; i < 4;++i){
            _itemLayout[i].setContentSize(_info[i].w,_info[i].h);

            var _titleBK = new ccui.ImageView(_info[i].img,ccui.Widget.PLIST_TEXTURE);
            _titleBK.setPosition(_info[i].w/2-20,_info[i].h-50);

            // var _titleBK2 = new ccui.ImageView("blinkLline.png",ccui.Widget.PLIST_TEXTURE);
            // _titleBK2.setPosition(_info[i].w/2+239,_info[i].h-50);
            // _titleBK2.scaleY = -1;
            // _titleBK2.setRotation(180);

            
           
            _itemLayout[i].addChild(_titleBK);
            //_itemLayout[i].addChild(_titleBK2);

            
           // listView.setItemModel(_itemLayout[i]);
            listView.pushBackCustomItem(_itemLayout[i]);
        
        }
       
        
        listView.setGravity(ccui.ListView.GRAVITY_CENTER_VERTICAL);
        listView.setItemsMargin(30);
    }

});

UIHelpLayer.TabFlag = {
    INTRODUCTION: 0,        //<说明
    RULE: 1,                //<规则
    FISH_MAP: 2             //<图鉴
};