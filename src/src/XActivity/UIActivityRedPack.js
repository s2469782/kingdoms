var  UIActivityRedPack = BaseLayer.extend({
    _spBK: null,
    _maskLayer: null,
    _stackPoint: [],
     _localBtn: [],
    state : [],
    rewardLighting: [],
    _feedBackLabel: [],
    receive_Enable: [],
    receive_disable: [],
    _rewardBtn: [],
    callback: null,
    callbackObj : null,
    _activityID: null,
    _ruleNode: null,
    _label: null,
    _statetype: null,
    _stackPointLabel: null,
    _actinfo: null,
    _resMsg: null,
    _noActivityBoolean: null,
    _actstring : null,
    _rewardScene: [],
    _noticelabel: null,
    
    
    ctor: function () {
        this._super();
        var self = this;
        


        //创建网络消息
        var msgDataArray = [
            
            //请求活动监听(更新活动)
            {msgId: MessageCode.CH_ReceiveActAwardMsg_Res, callFunc: self._resRewardMsg},
          
        ];

        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener[i] = cf.addNetListener(msgData.msgId, msgData.callFunc.bind(self));
        }

        // self.sendActivityMsg();
         
       
        self._resSendActivityMsg();
    },

    initUI:function(actInfo, resMsg){
        var self = this;
     
        self.state = [];


         //创建遮罩层
         self._maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        
         self.addChild(self._maskLayer, -100);

         //背景
         var spBK = self._spBK = new ccui.Layout();
         //spBK.setCapInsets(cc.rect(57, 54, 88, 92));
         //spBK.setContentSize(600 * 2, 564);
         // spBK.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
         spBK.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
         //spBK.setScale(0.9);
         self.addChild(spBK);

         var back = new cc.Scale9Sprite("redpack_window.png");
         back.setPosition(310, 0);
         back.scaleX= -1;
         //title.setContentSize(1070, 480);
         //spFrame.setScale(0.73);
         spBK.addChild(back);

         var back1 = new cc.Scale9Sprite("redpack_window.png");
         back1.setPosition(-310, 0);
         //title.setContentSize(1070, 480);
         //spFrame.setScale(0.73);
         spBK.addChild(back1);

         var roof = new cc.Scale9Sprite("roof.png");
        roof.setPosition(310, 300);
        spBK.addChild(roof);

        var roof1 = new cc.Scale9Sprite("roof.png");
        roof1.setPosition(-310, 300);
        spBK.addChild(roof1);

         var title = new cc.Scale9Sprite("redpack_title.png");
        title.setPosition(0,285);
        //title.setContentSize(1070, 480);
         //spFrame.setScale(0.73);
        spBK.addChild(title);

        var maskback = new cc.Scale9Sprite("bottomplate_Redenvelope.png");
        maskback.setPosition(0,-80);
        maskback.setScaleX(80);
        maskback.setScaleY(20);
        //title.setContentSize(1070, 480);
         //spFrame.setScale(0.73);
        spBK.addChild(maskback);

        var line = new cc.Scale9Sprite("bottomplate.png");
        line.setPosition(0,100);
        line.setScaleX(8);
        spBK.addChild(line);
        
        var bottom = new cc.Scale9Sprite("bottomplate_RL.png");
        bottom.setPosition(-400, 100);
        bottom.scaleX= -1;
        spBK.addChild(bottom);

        var bottom1 = new cc.Scale9Sprite("bottomplate_RL.png");
        bottom1.setPosition(400, 100);
        spBK.addChild(bottom1);

        var line1 = new cc.Scale9Sprite("bottomplate.png");
        line1.setPosition(0,-270);
        line1.setScaleX(10);
        spBK.addChild(line1);

        var botto = new cc.Scale9Sprite("bottomplate_RL.png");
        botto.setPosition(-500, -270);
        botto.scaleX= -1;
        spBK.addChild(botto);

        var botto1 = new cc.Scale9Sprite("bottomplate_RL.png");
        botto1.setPosition(500, -270);
        spBK.addChild(botto1);

        

        


        //退出按钮
        var closeBtn = new ccui.Button();
        closeBtn.loadTextureNormal("redpack_close.png", ccui.Widget.PLIST_TEXTURE);
        closeBtn.setPosition(spBK.width +580, spBK.height+300);
        closeBtn.setPressedActionEnabled(true);
        closeBtn.addClickEventListener(() =>{
            self.checkValid(actInfo, resMsg);
                   
            self.removeFromParent();
        });
            spBK.addChild(closeBtn);

        var imageLabel = ["dayreward.png","stackrewardlabel.png", "recordlabel.png"];
        
            for(var a = 0; a < 3; a++ ){
                self._rewardBtn[a]= new CCUIButton();
                self._rewardBtn[a].loadTextureNormal("button_on.png",ccui.Widget.PLIST_TEXTURE);
                self._rewardBtn[a].setAnchorPoint(0, 0);
                self._rewardBtn[a].setTag(a);
                self._rewardBtn[a].setPosition(200+(a*230), 510);
                self._rewardBtn[a].addTouchEventListener(this.functionalBtnCallBack, this);
                self._rewardBtn[a].setScale(1);
                self.addChild(self._rewardBtn[a]);
                self._rewardBtn[0].setVisible(false);
                       
                var rewardLabel= new cc.Scale9Sprite(imageLabel[a]);
                rewardLabel.setAnchorPoint(0, 0);
                rewardLabel.setPosition(self._rewardBtn[a].width/2-55, self._rewardBtn[a].height/2-13);
                rewardLabel.setTag("rewardBtnLabel"+a);
                rewardLabel.setScale(1);
                self._rewardBtn[a].addChild(rewardLabel);

                
        }
        self._actinfo = actInfo;
        self._resMsg = resMsg;

        self.clean();
        //self.initDaily(actInfo, resMsg);
        self.initStack(actInfo, resMsg);
        self.anotherButton(1);
        this._actstring = "0";
        self.setNoticeTimeout();

        
    },

    setNoticeTimeout: function(){
        var self = this;

        if(timer != null || timer){
            
            clearTimeout(timer);

        }

        var date = new Date();
        var nowtime = Date.parse(new Date());
        
        var endTimeStr = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + "23:50:00";
        var timestamp = Date.parse(new Date(endTimeStr));
        
        
        var time = timestamp - nowtime;
        
        
        if(time <= 0){
            


        }else{
            var timer = setTimeout(function () {
            
            if(cf.SceneMgr.getCurSceneFlag()== cf.SceneFlag.GAME){

                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.ACTIVITY_TIMEOUT_NOTICE);

            }else{
                
            } 
                
            }, time);
            
        }
        
    },

    initStack: function(actInfo, resMsg){
        var self = this;
        
        if(self._noActivityBoolean != null){
            self._noActivityBoolean.removeFromParent();
        }    
        
        
        var nowTime = new Date().getTime();
        self._noActivityBoolean  = new cc.LabelTTF("暂无此类型活动", "YaHei", 40);
        self._noActivityBoolean.lineWidth = 0.8;
        self._noActivityBoolean.setPosition(650, 300);
        self.addChild(self._noActivityBoolean);

        if(self._localBtn.length > 0){
            for(var a = 0; a < self._localBtn.length; a++){
                self._localBtn[a].removeFromParent();
            }
           
        }
        var total = 0;

        for(var a = 0; a < resMsg.actInfoList.length; a++){
        
               if(resMsg.actInfoList[a].actRuleInfoList[0].nConditionType == 13 && resMsg.actInfoList[a].actRuleInfoList[0].nUpdateType == 0 && nowTime > resMsg.actInfoList[a].strStartShowTime && nowTime < resMsg.actInfoList[a].strEndShowTime){
               
                  self.checkValid(resMsg.actInfoList[a], resMsg);
                  this._activityID = resMsg.actInfoList[a].nActID;
                   if(self._noActivityBoolean != null){
                       self._noActivityBoolean.removeFromParent();
                   }

                   

                   if(resMsg.actRecordList.length > 0 ){
                    
                       for(var b = 0; b < resMsg.actRecordList.length; b++){
                           if(resMsg.actRecordList.length > 0){
                               
                                   if(resMsg.actInfoList[a].nActID == resMsg.actRecordList[b].nActID){
                              
                                        total = (Math.floor(resMsg.actRecordList[b].nScore/100 +0.0000001))/100;

                                        self.redPack_disable(resMsg.actInfoList[a].actRuleInfoList, resMsg.actRecordList[b]);
                                   }else{
                                    self.redPack_disable(resMsg.actInfoList[a].actRuleInfoList, {
                                        nActID: resMsg.actInfoList[a].nActID,
                                        nScore: 0,
                                        strOneTimeData: "",
                                        nRecordTime: 1589772831000,
                                    });
                                   }
                               
                           }
                       }
                       
                   }else{
                     
                       self.redPack_disable(resMsg.actInfoList[a].actRuleInfoList, {
                            nActID: resMsg.actInfoList[a].nActID,
                            nScore: 0,
                            strOneTimeData: "",
                            nRecordTime: 1589772831000,
                        });
                   }

                   self._stackPointLabel  = new cc.LabelTTF("活动期间投注得积分好礼回馈！目前累积积分  ", "Arial Bold", 25);
                    self._stackPointLabel.setPosition(580, 460);
                    self._stackPointLabel.strokeEnabled= true;
                    self._stackPointLabel.lineWidth=0.6;
                    self.addChild(self._stackPointLabel);

                    
                    self.setTouchEnabled(true);
                    var _itemLabel = new cc.LabelBMFont(total,Res.LOBBY.font_resource_083);
                    _itemLabel.setAnchorPoint(1,1);
                    _itemLabel.setScale(1);
                    _itemLabel.setPosition(self._stackPointLabel.width +100,self._stackPointLabel.height+8);
                   // _itemLabel.setScale(0.55);
                    self._stackPointLabel.addChild(_itemLabel);

                    self.noticelabel = new cc.LabelTTF("注意事项:\n活动期间每投注一万金币可获得一点积分，各层级回馈活动期间每人限领一次，领取后可在记录内查询领取纪录", "Arial Bold", 25);
                    // label.lineWidth = 0.8;
                    self.noticelabel.setScale(0.7);
                    self.noticelabel.strokeEnabled= true;
                    self.noticelabel.lineWidth=0.6;
                    self.noticelabel.setPosition(620, 90);
                    self.addChild(self.noticelabel);
                   
                   
       
                   self.setTouchEnabled(true);
               }
               
               
           }
         
               


    },

    clean: function(){
        var self = this;
            self.state = [];
            
            if(self.noticelabel != null){
                self.noticelabel.removeFromParent();
            }

            // if(self.receive_Enable.length > 0){
            //     for(var a = 0; a < self.receive_Enable.length; a++){
            //         self.receive_Enable[a].removeFromParent();
            //     }
            // }

            // if(self.receive_disable.length > 0){
            //     for(var a = 0; a < self.receive_disable.length; a++){
            //         self.receive_disable[a].removeFromParent();
            //     }
            // }
            if(self._stackPoint.length > 0){
                for(var a = 0; a < self._stackPoint.length; a++){
                    self._stackPoint[a].removeFromParent();
                }
                
            }
            if(self._feedBackLabel.length > 0){
                for(var a = 0; a < self._feedBackLabel.length; a++){
                    self._feedBackLabel[a].removeFromParent();
                }
                
            }
            if(self.rewardLighting.length > 0){
                for(var a = 0; a < self.rewardLighting.length; a++){
                    self.rewardLighting[a].removeFromParent();
                }
            }
            if(self._localBtn.length > 0){
                for(var a = 0; a < self._localBtn.length; a++){
                    self._localBtn[a].removeFromParent();
                }
               
            }
            if(self._stackPointLabel != null){
                self._stackPointLabel.removeFromParent();
                
            }
            
            


    },
    initDaily: function(actInfo, resMsg){
        var self = this;
        
        if(self._noActivityBoolean != null){
            self._noActivityBoolean.removeFromParent();
        }
        

        var nowTime = new Date().getTime();
        self._noActivityBoolean  = new cc.LabelTTF("暂无此类型活动", "YaHei", 40);
        self._noActivityBoolean.lineWidth = 0.8;
        self._noActivityBoolean.setPosition(650, 300);
        self.addChild(self._noActivityBoolean);

        if(self._localBtn.length > 0){
            for(var a = 0; a < self._localBtn.length; a++){
                self._localBtn[a].removeFromParent();
            }
           
        }
        var total = 0;
         for(var a = 0; a < resMsg.actInfoList.length; a++){
            
                if(resMsg.actInfoList[a].actRuleInfoList[0].nConditionType == 13 && resMsg.actInfoList[a].actRuleInfoList[0].nUpdateType == 4 && nowTime > resMsg.actInfoList[a].strStartShowTime && nowTime < resMsg.actInfoList[a].strEndShowTime){
                    
                    self.checkValid(resMsg.actInfoList[a], resMsg);
                    this._activityID = resMsg.actInfoList[a].nActID;
                    if(self._noActivityBoolean != null){
                       self._noActivityBoolean.removeFromParent();
                   }    
                   
                    

                    if(resMsg.actRecordList.length > 0){
                     
                        for(var b = 0; b < resMsg.actRecordList.length; b++){
                            if(resMsg.actRecordList.length > 0){
                                

                                    if(resMsg.actInfoList[a].nActID == resMsg.actRecordList[b].nActID){
                               
                                        total = (Math.floor(resMsg.actRecordList[b].nScore/100 +0.0000001))/100;
                                    
                                        self.redPack_disable(resMsg.actInfoList[a].actRuleInfoList, resMsg.actRecordList[b]);
                                    }
                                
                            }
                        }
                        
                    }else{
                      
                        self.redPack_disable(resMsg.actInfoList[a].actRuleInfoList, null);
                    }
                    self._stackPointLabel  = new cc.LabelTTF("活动期间每日投注得积分好礼回馈！目前每日积分  "+total, "YaHei", 25);
                    self._stackPointLabel.setPosition(500, 430);
                    self.addChild(self._stackPointLabel);
                    self.setTouchEnabled(true);

                    self.noticelabel = new cc.LabelTTF("注意事项:\n活动期间每日00:00~23:59结算当日投注金币，每1万金币可获得1点积分，积分达到可获得奖励，\n最多每日可获得6个福利，跨日后积分将清空，前一日没有兑换的奖励，将由系统回收，请把握时间兑换！", "YaHei", 25);
                    // label.lineWidth = 0.8;
                    self.noticelabel.setScale(0.7);
                    self.noticelabel.setPosition(500, 70);
                    self.addChild(self.noticelabel);
        
                    
                }
                
                
            }
            

    },

    
    
    mouseEvent:function (that){
        cc.eventManager.addListener({
                event:cc.EventListener.MOUSE,
                onMouseDown: function (event){
                        that.removeFromParent();
                }
            }, that);
        },



    


    //恭喜獲得介面
    rewardWin: function(msg){
        var spineJson, spineAtlas;
        spineJson = Res.LOBBY.red_reward_Json;
        spineAtlas = Res.LOBBY.red_reward_Atlas;

       

        //创建遮罩层
        var layer = new cc.LayerColor(cc.color(0,0,0,150));
       // var btnCancel = new ccui.Button("button_005.png", null, null, 1);
        
        //点击画面领奖

        var rewardResult =  ActionMgr.createSpine(spineJson,spineAtlas,1);
       rewardResult.setAnimation(0, "animation", false);
       rewardResult.setPosition(cc.winSize.width / 2, cc.winSize.height / 2 );
       layer.addChild(rewardResult);

        //點擊畫面關閉label
        var labl = new cc.LabelTTF( "点击画面领奖", "YaHei", 20);
        labl.setAnchorPoint(0,0);
        labl.lineWidth = 0.4;
        labl.setPosition(rewardResult.width / 2 -615,rewardResult.height / 2 -560 );
        labl.setColor(cc.color.WHITE);
        rewardResult.addChild(labl);

        //msg.itemReward[0].nNum
        var reward = new cc.LabelTTF( "金币X"+msg.itemReward[0].nNum, "YaHei", 25);
      // var reward = new cc.LabelTTF( "金币 X"+0, "YaHei", 30);
        reward.setAnchorPoint(0.5,0.5);
        reward.lineWidth = 0.4;
        reward.setPosition(rewardResult.width / 2-555,rewardResult.height / 2 -515 );
        reward.setColor(cc.color.YELLOW);
        rewardResult.addChild(reward);

        var coin = new cc.Scale9Sprite("bobo.png");
        coin.setAnchorPoint(0, 0);
        coin.setScale(1);
        coin.setPosition(rewardResult.width / 2-675, rewardResult.height / 2-491);
        coin.setBlendFunc( cc.SRC_ALPHA,cc.ONE);  
        rewardResult.addChild(coin);
        
        rewardResult.runAction( cc.sequence(
            cc.scaleTo(0.3, 1.2),
            cc.callFunc(function(){

                // win.runAction( cc.sequence(
                //     cc.callFunc(function(){
                //         win.removeFromParent();
                //     })
                    
                // ));
            })
            
           ));

           this.addChild(layer);
           this.mouseEvent(layer);
       // }
    },

    // sendActivityMsg: function(){
    //     var playerModel = cf.PlayerMgr.getLocalPlayer();
        
    //     var strRuleVer = [];
        
    //     //先从本地获取活动信息
    //     var actInfoList = cf.SaveDataMgr.getNativeData("actInfoList");
    //     if (actInfoList == null || actInfoList == "") {
            
    //     } else {
    //         actInfoList = JSON.parse(actInfoList);
    //         for (var i = 0; i < actInfoList.length; ++i) {
    //             var strSelf = {};
    //             var actRuleInfoList = actInfoList[i].actRuleInfoList;
    //             var strRule = [];
    //             for (var j = 0; j < actRuleInfoList.length; j++) {
    //                 if (actRuleInfoList[j].nIndex && actRuleInfoList[j].updateTime && actRuleInfoList[j].updateTime != "") {
    //                     var str = actRuleInfoList[j].nIndex + "," + actRuleInfoList[j].updateTime;
    //                     strRule.push(str);
    //                 }
    //             }
    //             strSelf.nActID = [];
    //             strSelf.nVersion = [];
    //             strSelf.strRuleTime = strRule;
    //             strRuleVer.push(strSelf);
    //         }
    //     }
        

    //     var msg = new $root.CH_AskActivity();
    //     msg.nidLogin = playerModel.getIdLogin();
    //     msg.strSafeCode = playerModel.getSafeCode();
    //     msg.strRuleVer = {};
    //     var wr = $root.CH_AskActivity.encode(msg).finish();
    //     cf.NetMgr.sendSocketMsg(MessageCode.CH_AskActivityMsg, wr);

    // },
    _resRewardMsg: function(msg){
        var resMsg = $root.CH_ReceiveActAward_Res.decode(msg);
        var playerModel = cf.PlayerMgr.getLocalPlayer();
       
        if(resMsg){

            if(!this._rewardScene ){       
                this._rewardScene = [];
            }
            this._rewardScene[resMsg.nActID] = resMsg;   
            this.rewardWin(resMsg);
            playerModel.setGoldNum(resMsg.itemReward[0].nItemNum);
            this.callback.apply(this.callbackObj );

            
        }
        
       
        

    },
    //沒有活動提示
    _noActivityHint: function(){

            this._ruleNode = new cc.Scale9Sprite("resource_036_9.png");
            this._ruleNode.setContentSize(650, 480);
            this._ruleNode.setAnchorPoint(0.5,1);
            this._ruleNode.setPosition(cc.winSize.width/2,cc.winSize.height/2+200);
            this.addChild(this._ruleNode);

            this._label = new cc.LabelTTF( "目前无累积投注活动，敬请期待!", "YaHei", 35);
            this._label.setAnchorPoint(0.5,0.5);
            this._label.lineWidth = 1;
            this._label.setPosition(this._ruleNode.width / 2,this._ruleNode.height / 2 );
            this._ruleNode.addChild(this._label);

            var closeBtn = new ccui.Button();
            closeBtn.loadTextureNormal("button_005.png", 1);
            closeBtn.setTitleText(cf.Language.getText("text_1224"));
            closeBtn.setTitleFontSize(32);
            closeBtn.setTitleFontName(cf.Language.FontName);
            closeBtn.setPosition(this._ruleNode.width/2 , this._ruleNode.height/2 - 150);
            closeBtn.setScale(0.9);
            this._ruleNode.addChild(closeBtn);
            closeBtn.addClickEventListener(() =>{
                this.removeFromParent();
            });
            
            this.mouseEvent(this._ruleNode);
        

    },
    _resSendActivityMsg: function(){
        
        if(cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.GAME){
            cf.dispatchEvent(BaseRoom.DoEvent, BaseRosom.EventFlag.UI_CONTINUEGAME);
            
        }else{
        var self = this;
        var playerModel = cf.PlayerMgr.getLocalPlayer();
       // var resMsg = $root.CH_AskActivity_Res.decode(msg);
        var resMsg = cf.activityInfo;
        
        if (resMsg) {
            
            if (resMsg.actInfoList.length > 0 && resMsg.actInfoList != null) {
                //获取信息存入本地数据
                var actInfoList = cf.SaveDataMgr.getNativeData("actInfoList");
                if (actInfoList == null || actInfoList == "") {

                    var jsonData = resMsg.actInfoList;

                    var loop = jsonData.length;
                    for (var i =0; i < loop;i ++) {
                        for(j = i + 1; j < loop; j++){

                            if(jsonData[i].nType ==jsonData[j].nType ){
                                jsonData.splice(j,1);
                                cc.log(j);
                                j = j - 1;    //改变循环变量
                                loop = loop - 1;   //改变循环次数
                            }
                        }
                    }
                    
                    jsonData = this._delectRedundant(jsonData);
                    cf.SaveDataMgr.setNativeData("actInfoList", JSON.stringify(jsonData));
                }else{
                    //将字符串转化为json
                    var jsonData = JSON.parse(actInfoList);
                    
                    //添加新活动
                    for (var i in resMsg.actInfoList) {

                        jsonData.push(resMsg.actInfoList[i]);
                        
                        var loop = jsonData.length;
                        //删除过期数据
                        for (var j =0; j < loop;j ++) {
                            if (jsonData[j].nType != cf.ACTTagFlag.Roulette && jsonData[j].nType != cf.ACTTagFlag.Champions) {
                                //判断活动是否在显示期内
                                if (jsonData[j].strEndShowTime < playerModel.getGameTime()) {
                                    jsonData.splice(j, 1);
                                    j = j - 1;    //改变循环变量
                                    loop = loop - 1;   //改变循环次数
                                }
                            }
                        }
                    }
                    
                    jsonData = this._delectRedundant(jsonData);
                  
                    var loop = jsonData.length;
                    //删除失效活动
                    for (var j =0; j < loop;j ++) {
                        if (jsonData[j].nState  == 1) {
                            jsonData.splice(j,1);
                            j = j - 1;    //改变循环变量
                            loop = loop - 1;   //改变循环次数
                        }
                    }
                  
                    cf.SaveDataMgr.setNativeData("actInfoList", JSON.stringify(jsonData));
                }
            }
           
            var nowTime = new Date().getTime();
            
            for(var a = 0; a < resMsg.actInfoList.length; a++){
                
                if(resMsg.actInfoList[a].actRuleInfoList[0].nConditionType == 13 && resMsg.actInfoList[a].actRuleInfoList[0].nUpdateType == 4  || resMsg.actInfoList[a].actRuleInfoList[0].nUpdateType == 0 && nowTime > resMsg.actInfoList[a].strStartShowTime && nowTime < resMsg.actInfoList[a].strEndShowTime){

                    self.initUI(resMsg.actInfoList[a].actRuleInfoList, resMsg);
                        
                }

               
            }
        }
    }
    },


    checkValid: function(msg , record){
     
        var s = [];
        var array = [];
        for(var a = 0; a < msg.length; a++){
            s.push(0);
        }
       

        if(record.actRecordList.length > 0){
           
            var result = record.actRecordList[0].strOneTimeData.split(",");
            for(var c = 0; c < result.length; c++){
                s[c] = result[c];
                    
            }

            for(var a = 0 ; a < msg.length; a++){
                if(msg[a].nValue[0] < record.actRecordList[0].nScore){
                    
                    if(s[a] != "1"){
                        array.push(true);
                    }
                
                }else{

                    array.push(false);
                }


            }
            
            if(array.includes(true)){
                
                cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UPDATE_REDPACK_ENABLE);
                cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UPDATE_ACTIVITY_NOTICE_NOTSHOW);
                
            }else{
               
                cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UPDATE_REDPACK_DISABLE);
                cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UPDATE_ACTIVITY_NOTICE_NOTSHOW);
                
            }


        }else{
            cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UPDATE_ACTIVITY_NOTICE_SHOW);


        }
        
            

        
    
        

    },
    
    
    _delectRedundant:function(jsonData){
        //活动列表排序
        var compare = function (obj1, obj2) {
            var val1 = parseInt(obj1.nSort);
            var val2 = parseInt(obj2.nSort);
            if (val1 > val2) {
                return -1;
            } else if (val1 < val2) {
                return 1;
            } else {
                return 0;
            }
        };

        jsonData.sort(compare);

        cc.log(jsonData);
        //除重(去掉同一时间的内出现版本的相同活动 去掉版本低)
        var loop = jsonData.length;
        for (var i =0; i < loop;i ++) {
            for(var j = i + 1; j < loop; j++){
                if(jsonData[j].nType == jsonData[i].nType) {
                    if (jsonData[j].nVersion > jsonData[i].nVersion) {
                        jsonData.splice(i, 1);
                        j = j - 1;    //改变循环变量
                        loop = loop - 1;   //改变循环次数
                    }
                }
            }
        }
        //除重(去掉同一时间的内出现活动ID的相同活动 去掉活动ID小)
        var loop01 = jsonData.length;
        for (var i =0; i < loop01;i ++) {
            for(var j = i + 1; j < loop01; j++){
                if(jsonData[j].nType == jsonData[i].nType) {
                    if (jsonData[j].nActID >= jsonData[i].nActID) {
                        jsonData.splice(i, 1);
                        j = j - 1;    //改变循环变量
                        loop01 = loop01 - 1;   //改变循环次数
                    }
                }
            }
        }
        //移除争霸赛显示
        /*var loop01 = jsonData.length;
        for (var k =0; k < loop01;k ++) {
            if (jsonData[k].nType  == cf.ACTTagFlag.Champions) {
                jsonData.splice(k,1);
                k = k - 1;    //改变循环变量
                loop01 = loop01 - 1;   //改变循环次数
            }
        }*/
        cc.log(jsonData);
        return jsonData;
    },
    sendActivityRewardMsg: function(sender){
        
      
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var msg = new $root.CH_ReceiveActAward();
        msg.strSafeCode = playerModel.getSafeCode();
        msg.nActID = this._activityID;
        msg.nAwardIndex = sender+1;
        msg.nidLogin = cf.PlayerMgr.getLocalPlayer().getIdLogin();
        var wr = $root.CH_ReceiveActAward.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_ReceiveActAwardMsg, wr);

    },
    functionalBtnCallBack: function(sender, type){
        var self = this;
        var tag = sender.getTag();
        if (type != ccui.Widget.TOUCH_ENDED) {
            return;
        }
        
            
        
        
        //开关界面
        cf.SoundMgr.playEffect(8);
        switch (tag) {
            case 0:
                //每日獎勵
                self.clean();
                self.anotherButton(0);
                
                this._actstring = "0";
                self.initDaily(self._actinfo, self._resMsg);
                
                break;
            case 1:
                //累積獎勵
                self.clean();
                self.anotherButton(1);
                
                this._actstring = "1";
                self.initStack(self._actinfo, self._resMsg);
                
                
                break;
            case 2:
                //紀錄
                self.initRecord();
                break;
            
           
        }

    },

    anotherButton: function(num){
        var self = this;
        
        if(num == 0){
            self._rewardBtn[0].setBright(true);
            self._rewardBtn[1].setBright(false);
            self._rewardBtn[2].setBright(false);

        }else if (num == 1){
            self._rewardBtn[1].setBright(true);
            self._rewardBtn[0].setBright(false);
            self._rewardBtn[2].setBright(false);

        }   

    },
    
    initRecord: function(){
        var self = this;
        // self._rewardBtn[2].setBright(true);
        // self._rewardBtn[1].setBright(false);

        self.sendRecordMsg();
        var rewardRecord = new UIActivityRedRecord(self._actstring);
        self.addChild(rewardRecord, 100);
    },
    
    sendRecordMsg: function(){
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        var msg = new $root.CH_AskActivityAwardLog();
        msg.nidLogin = cf.PlayerMgr.getLocalPlayer().getIdLogin();
        msg.safecode = playerModel.getSafeCode();
        msg.actId = this._activityID;
        var wr = $root.CH_AskActivityAwardLog.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_AskActivityAwardLog, wr);

    },

    redPack_disable: function(msg, recordmsg){
        var self  = this;
        
        if(self._localBtn.length > 0){
            for(var a = 0; a < self._localBtn.length; a ++){
                self._localBtn[a].removeFromParent();
                
            }
            self._localBtn = [];
            self.state = [];
        }
        
        for(var a = 0 ; a < msg.length; a++){
            
            self.state.push(0);
        
            self._localBtn[a]=new CCUIButton();
            self._localBtn[a].setAnchorPoint(0.5, 0.5);
            self._localBtn[a].setPosition(150+(a*195), 280);
            self._localBtn[a].setPressedActionEnabled(false);
            self._localBtn[a].setScale(1);
            self._localBtn[a].setTag("btn"+a);
           
            self.rewardLighting[a] = new cc.Scale9Sprite("redpack_light.png");
            self.rewardLighting[a].setScaleY(1.08);
            self.rewardLighting[a].setPosition(self._localBtn[a].width/2+90, self._localBtn[a].height /2+138);
            self.rewardLighting[a].setVisible(false);
            self.rewardLighting[a].setBlendFunc( cc.SRC_ALPHA,cc.ONE);    
            self._localBtn[a].addChild(self.rewardLighting[a], 10);

            var point = (Math.floor(msg[a].nValue[0]/100 +0.0000001))/100;
           
            
            self._stackPoint[a] = new cc.LabelTTF( point, "YaHei", 25);
            self._stackPoint[a].setAnchorPoint(0.5,0.5);
            self._stackPoint[a].setPosition(self._localBtn[a].width / 2+82,self._localBtn[a].height / 2 +110 );
            self._stackPoint[a].setColor(cc.color.WHITE);
            self._localBtn[a].addChild(self._stackPoint[a]);
            

            self._feedBackLabel[a] = new cc.LabelBMFont(msg[a].strAward[0].split(",")[1],Res.LOBBY.font_resource_083);
            self._feedBackLabel[a].setAnchorPoint(0.5,0.5);
            self._feedBackLabel[a].setPosition(self._localBtn[a].width / 2 +88,self._localBtn[a].height / 2 +31);
            self._feedBackLabel[a].setScale(1);
            self._localBtn[a].addChild(self._feedBackLabel[a]);
            
            // self.receive_Enable[a] = new cc.Scale9Sprite("redpack_enableLabel.png");
            // self.receive_Enable[a].setAnchorPoint(0, 0);
            // self.receive_Enable[a].setScale(1);
            // self.receive_Enable[a].setPosition(self._localBtn[a].width / 2+57,self._localBtn[a].height / 2 +190 );
            // self._localBtn[a].addChild(self.receive_Enable[a]);
            // self.receive_Enable[a].setVisible(false);

            // self.receive_disable[a] = new cc.Scale9Sprite("redpack_disable.png");
            // self.receive_disable[a].setAnchorPoint(0,0);
            // //receive_Enable.setAnchorPoint(0, 0);
            // self.receive_disable[a].setScale(1);
            // self.receive_disable[a].setPosition(self._localBtn[a].width / 2+57,self._localBtn[a].height / 2 +190 );
            // self.receive_disable[a].setVisible(false);
            // self._localBtn[a].addChild(self.receive_disable[a]);


            
        }

        
        if(recordmsg != null && msg.length > 0){
            // self.localBtn.loadTextureNormal("redpack_enable.png",ccui.Widget.PLIST_TEXTURE);
            // self.addChild(self.localBtn);
               
                if(this._rewardScene.length > 0 && this._rewardScene[recordmsg.nActID]){
                    var result = this._rewardScene[recordmsg.nActID].strRecord.split(",");
                    
                }else{
                    var result = recordmsg.strOneTimeData.split(",");
                }
                
                for(var c = 0; c < result.length; c++){
                    self.state[c] = result[c];
                    
                }
              
                  
                    for(var b = 0; b < self.state.length; b++){
                        
                        if( recordmsg.nScore >=  msg[b].nValue[0]){    

                            if(self.state[b] == "1"){
                            
                                self._localBtn[b].loadTextureNormal("redpack_enable.png",ccui.Widget.PLIST_TEXTURE);
                                self._localBtn[b].setOpacity(190);
                                self.rewardLighting[b].setVisible(false);
                                //self.receive_Enable[b].setVisible(false);
                               // self.receive_disable[b].setVisible(false);
                                self.addChild(self._localBtn[b]);
                        
                            }else{

                        

                                self._localBtn[b].loadTextureNormal("redpack.png",ccui.Widget.PLIST_TEXTURE);
                                self.addChild(self._localBtn[b]);
                                
                                // if(recordmsg[0].strOneTimeData)
                                self._localBtn[b].setOpacity(255);
                                //self.receive_Enable[b].setVisible(true);
                                self.rewardLighting[b].setVisible(true);
                                    //receive_Enable.setVisible(true);
                                    //self._localBtn[b].setRotation(-2);
                        
                                self.shakeRoom(self._localBtn[b]);

                                //self.receive_disable[b].setVisible(false);
                                self._localBtn[b].pressIndex = b;


                                
                                self._localBtn[b].addClickEventListener((sender, type)=>{
                                    
                                    cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UPDATE_REDPACK_DISABLE);
                                    this.sendActivityRewardMsg(sender.pressIndex );
                                
                                    cf.SoundMgr.playEffect(10);
                                    self._localBtn[sender.pressIndex].removeFromParent();
                                    self._localBtn[sender.pressIndex].setOpacity(190);
                                    self._localBtn[sender.pressIndex].loadTextureNormal("redpack_enable.png",ccui.Widget.PLIST_TEXTURE);
                                    self.addChild(self._localBtn[sender.pressIndex]);
                                    self.rewardLighting[sender.pressIndex].setVisible(false);
                                   // self.receive_Enable[sender.pressIndex].setVisible(false);
                                    self._localBtn[sender.pressIndex].setRotation(0);
                                    
                                    
                                    
                                });
                            }
                        }else{

                            self._localBtn[b].loadTextureNormal("redpack.png",ccui.Widget.PLIST_TEXTURE);
                            self.addChild(self._localBtn[b]);

                            self._localBtn[b].setRotation(0);
                            self._localBtn[b].setOpacity(255);
                           // self.receive_Enable[b].setVisible(false);
                           // self.receive_disable[b].setVisible(true);
                            self.rewardLighting[b].setVisible(false);
                        }
                }
               
                        
            
                
            }else{
                
                for(var c = 0; c < msg.length; c++){
                    self._localBtn[c].loadTextureNormal("redpack.png",ccui.Widget.PLIST_TEXTURE);
                    self.addChild(self._localBtn[c]);
                   // self.receive_disable[c].setVisible(true);
                }
            }
       

    },
    
    shakeRoom: function (btn) {
        // var action = btn;
        // if (action != null && !action.isDone()) {
        //     return;
        // }
        var oldpos = btn.getRotation();
        
        var _shake = //cc.sequence(
            cc.repeatForever(cc.sequence(
                cc.rotateTo(0.5,3), 
                cc.rotateTo(0.5,-3)
            ));
            // cc.callFunc(function () {
            //     //这里的this是mapArray
            //     btn.stopAllActions();
            //     btn.setRotation(oldpos);
            // }, btn)
       // );
        btn.runAction(_shake);
    },
    addUpdateGoldNotify: function(callback , obj){
        this.callback = callback;
        this.callbackObj = obj;
    }

     
    
    
        
});

