var UIDragonHeroRecord= BaseLayer.extend({

    _pageNum: 1,//总页数
    _pageIndex: 1, //当前页数
    _checkDate: 0, //当前显示数据的日期
    _onNetMsgListener: null, //网络监听
    _fishingInfo: {},//用于临时存储当日数据
    _detaileInfo: {},
    _detaileCheckId: {}, //详细查询的唯一Id
    _roomId: 0,          //场景ID
    _showInfoMod: 0,     //当前显示数据的类型
    _newFishingInfo: {},
    _oldFishingInfo: {},
    _waitLabel: null,  //稍等提示
    _btnBackShow: null,
    _index: null,
    _ActivityRedPack: null,

    ctor: function (index) {
        this._super();
        var self = this;
     
        var msgDataArray = [        
        //请求活动监听(更新活动)
             {msgId: MessageCode.CH_JP_log_Res, callFunc: self._resDragonHeroRecord}
            
        ];

        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener[i] = cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this));
        }

        self.initData();
        //遮罩层
        var maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        self.addChild(maskLayer, -100);
        self.setTouchEnabled(true);

        var baseBg = this.baseBg = new cc.Scale9Sprite("dragon_hero_background.png");
        baseBg.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
       // baseBg.setContentSize(1050, 530);
        self.addChild(baseBg);

        

        self._date = new Date();
        var startTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._date.getDate() + " " + "00:00:00";
        var endTimeStr = self._date.getFullYear() + "-" + (self._date.getMonth() + 1) + "-" + self._date.getDate() + " " + "23:59:59";
       // self.askForFishRecord(startTimeStr, endTimeStr);
        this.initBtn();
        this.addAllTextChildOfTable();
        this.mouseEvent(this);

        //請稍後label
        self._waitLabel =this._waitLabel= new cc.LabelTTF("资料读取中...请稍候...", cf.Language.FontName, 40);
        self._waitLabel.lineWidth = 0.3;
        self._waitLabel.setPosition(baseBg.width/2+130,baseBg.height/2+50);
        self.addChild(self._waitLabel);

        
        
        self.getRecord(index);

        },

        getRecord: function(index){
            var self = this;

        
            if(index == 1003){
                var dragonhero = cf.SaveDataMgr.getNativeData("dragonHero");
                
            }else if(index == 1004){
                var dragonhero = cf.SaveDataMgr.getNativeData("dragonHero1");  
                
            }
            this._index = index;
            if (dragonhero != null && dragonhero != "") {
                if(self._waitLabel != null){
                    self._waitLabel.removeFromParent();
                }
                
                jsondata = JSON.parse(dragonhero);
                this.initBaseUI(jsondata);

            }else{

                var _msg = new $root.CH_JP_log();
                _msg.roomType = parseInt(index);
                var wr = $root.CH_JP_log.encode(_msg).finish();
                cf.NetMgr.sendSocketMsg(MessageCode.CH_JP_log, wr);
                
            }
            
             
           
        },


        _resDragonHeroRecord:function(msg){
            var self = this;
            var resMsg = $root.CH_JP_log_Res.decode(msg);
            
           
            if(resMsg.jplogs.length>0){
                
                if(this._index == 1003){
                    //将字符串转化为json
                    if (cf.SaveDataMgr.getNativeData("dragonHero") == null) {
                        cf.SaveDataMgr.setNativeData("dragonHero1", null);
                        cf.SaveDataMgr.setNativeData("dragonHero", JSON.stringify(resMsg));
                    }
    
                }else if(this._index == 1004){
                    if (cf.SaveDataMgr.getNativeData("dragonHero1") == null) {
                        cf.SaveDataMgr.setNativeData("dragonHero", null);
                        cf.SaveDataMgr.setNativeData("dragonHero1", JSON.stringify(resMsg));
                    }
    
                }
                

                if(self._waitLabel != null){
                    self._waitLabel.removeFromParent();
                }
            
             
                this.initBaseUI(resMsg);
            
            }
       
        },

    mouseEvent:function (that){
        cc.eventManager.addListener({
            event:cc.EventListener.MOUSE,
            onMouseDown: function (event){
                that.removeFromParent();
            }
        }, that);
    },
    
    initData: function () {
        var self = this;   
        self._onNetMsgListener = null; //网络监听
        self._roomId = 0;
      
    },

    initBaseUI: function (resMsg) {
        

        if(resMsg){

                
            for(var a = 0; a < resMsg.jplogs.length; a++){
                
                var str = resMsg.jplogs[a].idlogin+"";
                var result = str.split("");
                var str3 = result.splice(2, 3, "x", "x", "x");
                
                var name = new ccui.LabelBMFont(result.join(""), Res.LOBBY.font_resource_093);
                name.setPosition(410, 450-(30*a));
                name.setScale(0.8);
                this.addChild(name);

                var receivedgold = new ccui.LabelBMFont(resMsg.jplogs[a].hitmoney , Res.LOBBY.font_resource_093);
                receivedgold.setPosition(585, 450-(30*a));
                receivedgold.setScale(0.8);
                this.addChild(receivedgold);

                var time = new ccui.LabelBMFont(resMsg.jplogs[a].time , Res.LOBBY.font_resource_093);
                time.setPosition(810, 450-(30*a));
                time.setScale(0.8);
                this.addChild(time);
    
                
            }
            


        }


        
        
     
       
    },

    initBtn: function () {
        var self = this;
        var baseBg = this.baseBg;

        //关闭按钮
        var closeBtn = new ccui.Button("resource_021.png", "resource_021.png", "resource_021.png", ccui.Widget.PLIST_TEXTURE);
        closeBtn.setPosition(baseBg.width - 100, baseBg.height - 80);
        closeBtn.setZoomScale(0.1);
        closeBtn.setScale(1);
        closeBtn.setFlippedX(true);
        closeBtn.setPressedActionEnabled(true);
        baseBg.addChild(closeBtn);
        closeBtn.addClickEventListener(function () {
          //  self._ActivityRedPack.BtnRecall();
            self.removeFromParent(true);
        });
    },

    //添加流水报表以及捕鱼报表的所有子类显示UI
    addAllTextChildOfTable: function () {
        var self = this;
        var baseBg = this.baseBg;

        //添加文字描述项 并生成原始文字显示项 捕鱼报表分页
        var textArr = ['玩家ID', '皇城祕宝金额', '中奖时间'];
        var textPos = [300, 470, 690];
        //描述TextUI的数组
        var textUiArr = this.textUiArr = [];
        //显示详细数据的UI数组
        var showTextUiArr = this.showTextUiArr = [];
        for (var i = 0; i < 7; i++) {
            textUiArr[i] = new ccui.LabelBMFont(textArr[i], Res.LOBBY.font_resource_093);
            textUiArr[i].setPosition(textPos[i], baseBg.height-145 );
            textUiArr[i].setColor(cc.color(254, 224, 114));
            textUiArr[i].setScale(1);
            baseBg.addChild(textUiArr[i]);

            showTextUiArr[i] = [];
            for (var j = 0; j < 5; j++) {
                if (i == 6) {
                    showTextUiArr[i][j] = new ccui.Button("tableButton.png", "tableButton.png", "tableButton.png", ccui.Widget.PLIST_TEXTURE);
                    showTextUiArr[i][j].setPosition(textPos[i] + 5, baseBg.height - 250 +10 * j);
                    showTextUiArr[i][j].setZoomScale(0.1);
                    showTextUiArr[i][j].setScale(0.7);
                    showTextUiArr[i][j].setPressedActionEnabled(true);
                    baseBg.addChild(showTextUiArr[i][j]);
                
            
                    var _btnName = new ccui.LabelBMFont("详情", Res.LOBBY.font_resource_093);
                    _btnName.setPosition(showTextUiArr[i][j].width / 2 + 2, showTextUiArr[i][j].height / 2);
                    _btnName.setName("btnName");
                    showTextUiArr[i][j].addChild(_btnName);

                }
                else {
                    showTextUiArr[i][j] = new ccui.LabelBMFont("", Res.LOBBY.font_resource_093);
                    showTextUiArr[i][j].setPosition(textPos[i], baseBg.height - 250 + 10 * j);
                    showTextUiArr[i][j].setScale(0.6);
                    baseBg.addChild(showTextUiArr[i][j]);
                }
             }
        }

     

    },


    hide: function () {
        this._super();
    },

    onClear: function () {
        this._super();
    }
});

