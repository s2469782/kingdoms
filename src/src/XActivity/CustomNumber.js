var CustomNumber = cc.Class.extend({

    numberArray: [],

    _delegate: null,

    _startPos: null,

    _offset: 0,

    ctor: function () {

    },

    /**
     *  //数字精灵数量
     * @param
     * delegate          父节点
     * num               精灵数字数量
     * startPos          初始位置
     * offset            偏移量
     */
    initNumber: function (delegate,startPos,offset) {
        this.numberArray = [];
        this._delegate = delegate;
        this._startPos = startPos;
        this._offset = offset;
        for(var i = 0; i < 10; i++) {
            var numSp = new cc.Sprite("#number_001.png");
            numSp.setPosition(startPos.x + offset * i,startPos.y);
            numSp.setTag(i);
            delegate.addChild(numSp);
            this.numberArray.push(numSp);
        }
    },

    setSpriteNumber: function (number) {
        if(number < 0) {
            for(var j = 0;j < 10;j++) {
                this.numberArray[j].setSpriteFrame(ActionMgr.getFrame("number_001.png"));
            }
            return;
        }
        else if(number > 9999999999) {
            for(var j = 0;j < 10;j++) {
                this.numberArray[j].setSpriteFrame(ActionMgr.getFrame("number_010.png"));
            }
            return;
        }
        var str = number.toString();
        for(var index = 0;index < 10 - str.length;index++) {
            this.numberArray[index].setSpriteFrame(ActionMgr.getFrame("number_001.png"));
        }
        for(var i = 0;i < str.length;i++){
            var frameNum = parseInt(str[i]) + 1;
            var frameName = "number" + (frameNum < 10 ? ("_00" + frameNum) : ("_0" + frameNum)) + ".png";
            this.numberArray[(10 - str.length) + i].setSpriteFrame(ActionMgr.getFrame(frameName));
        }
    },

});

/**
 * 替换数字类的实例
 * @type {undefined}
 * @private
 */
CustomNumber._sInstance = undefined;
CustomNumber.getInstance = function () {
    if (!CustomNumber._sInstance) {
        CustomNumber._sInstance = new CustomNumber();
    }
    return CustomNumber._sInstance;
};

/**
 * 移除实例
 */
CustomNumber.purgeRelease = function () {
    if (CustomNumber._sInstance) {
        CustomNumber._sInstance = null;
    }
};