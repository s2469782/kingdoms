var UIDialog = BaseLayer.extend({
    _target: null,
    _callback: null,
    _strRight: null,
    _spBk: null,

    _initBase: function (strRight, strLeft, bClose, callback, target) {
        var self = this;

        self._target = target;
        self._callback = callback;
        self._strRight = strRight;

        //创建遮罩层
        var maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        self.addChild(maskLayer);

        //添加背景
        var spBk = self._spBk = new cc.Scale9Sprite("resource_036_9.png");
        spBk.setContentSize(580, 420);
        spBk.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        spBk.setTag(10001);
        self.addChild(spBk);

        //动画
        spBk.runAction(cc.sequence(
            cc.scaleTo(0.1, 1.1),
            cc.scaleTo(0.1, 1.0)
        ));

        //提示
        var noticeLabel = new cc.Sprite("#title_notice.png");
        noticeLabel.setPosition(spBk.width/2, spBk.height-25);
        spBk.addChild(noticeLabel);
        // var titleLabel = new ccui.Text(cf.Language.getText("text_1035"), cf.Language.FontName, 36);
        // titleLabel.setPosition(spBk.width / 2, spBk.height-40);
        // titleLabel.setTextColor(cf.TitleColor.FONT_2);
        // titleLabel.enableOutline(cf.TitleColor.OUT_2, cf.TitleColor.SIZE - 2);
        // spBk.addChild(titleLabel);

        //右按钮，确定
        var rightBtn = new ccui.Button();
        rightBtn.loadTextureNormal("button_005.png", 1);
        // rightBtn.setTitleText(strRight);
        // rightBtn.setTitleFontSize(32);
        // rightBtn.setTitleFontName(cf.Language.FontName);
        var title02 = new cc.Sprite("#yeslabel.png");
        title02.setPosition(rightBtn.width/2, rightBtn.height/2);
        rightBtn.addChild(title02);
        // var title02 = rightBtn.getTitleRenderer();
        // title02.enableStroke(cf.BtnColor.YELLOW, 2);
        rightBtn.setPosition(spBk.width / 2, 80);
        rightBtn.setTag(10001);
        rightBtn.setScale(0.9);
        rightBtn.addClickEventListener(function () {
            if (self._callback && typeof self._callback == 'function') {
                self._callback.call(self._target, UIDialog.Btn.RIGHT);
            }
            self.removeFromParent(true);
        });
        spBk.addChild(rightBtn);

        //左按钮，取消
        if (strLeft != null) {
            rightBtn.setPosition(spBk.width / 2 + 100, 80);

            var leftBtn = new ccui.Button();
            leftBtn.loadTextureNormal("button_003.png", 1);
            leftBtn.setTitleText(strLeft);
            leftBtn.setTitleFontSize(32);
            leftBtn.setTitleFontName(cf.Language.FontName);
            var title = leftBtn.getTitleRenderer();
            title.enableStroke(cf.BtnColor.BLUE, 2);
            leftBtn.setPosition(spBk.width / 2 - 100, 80);
            leftBtn.addClickEventListener(function () {
                if (self._callback && typeof self._callback == 'function') {
                    self._callback.call(self._target, UIDialog.Btn.LEFT);
                }
                self.removeFromParent(true);
            });
            leftBtn.setScale(0.9);
            spBk.addChild(leftBtn);
        }

        //创建关闭按钮
        if (bClose) {
            var closeBtn = new ccui.Button();
            closeBtn.loadTextureNormal("resource_021.png", 1);
            closeBtn.setPosition(spBk.width-20, spBk.height-20);
            closeBtn.addClickEventListener(function () {
                if (self._callback && typeof self._callback == 'function') {
                    self._callback.call(self._target, UIDialog.Btn.CLOSE);
                }
                self.removeFromParent(true);
            });
            closeBtn.setScale(0.8);
            spBk.addChild(closeBtn);
        }

        self.setTouchEnabled(true);
        return true;
    },

    /**
     * 初始化普通文本
     * @param text
     * @param strRight
     * @param strLeft
     * @param bClose
     * @param callback
     * @param target
     * @returns {boolean}
     */
    init: function (text, strRight, strLeft, bClose, callback, target) {
        var self = this;

        if (self._initBase(strRight, strLeft, bClose, callback, target)) {

            //提示内容
            var label = new cc.LabelTTF(text, cf.Language.FontName, 25, cc.size(400, 300),
                cc.TEXT_ALIGNMENT_CENTER, cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
            label.setAnchorPoint(0.5, 0);
            label.setPosition(self._spBk.width / 2, 100);
            self._spBk.addChild(label);

            return true;
        }
        return false;
    },

    /**
     * 初始化富文本
     * @param text
     * @param color
     * @param strRight
     * @param strLeft
     * @param bClose
     * @param callback
     * @param target
     * @returns {boolean}
     */
    initRichText: function (text, color, strRight, strLeft, bClose, callback, target) {
        var self = this;
        if (self._initBase(strRight, strLeft, bClose, callback, target)) {
            //提示内容 
            var richText = new ccui.RichText();
            richText.ignoreContentAdaptWithSize(false);
            //richText.setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
            //richText.setTextVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
            richText.setContentSize(410, 50);
            richText.setTag(10002);
            self._spBk.addChild(richText, 1);

            var strArray = text.split("|");
            var len = strArray.length;
            for (var i = 0; i < len; ++i) {
                richText.pushBackElement(new ccui.RichElementText(i, color[i], 255, strArray[i], cf.Language.FontName, 25));
            }
            richText.formatText();
            richText.setPosition(self._spBk.width/2, self._spBk.height / 2 + 20);
            return true;
        }
        return false;
    }
});

UIDialog.Btn = {
    RIGHT: 0,
    LEFT: 1,
    CLOSE: 2
};

/**
 * 创建错误对话框
 * @param text 错误代码
 * @param callback 回调函数
 * @param target 父节点
 * @returns {*}
 */
UIDialog.createError = function (text, callback, target) {
    var dlg = new UIDialog();
    var _tmp = cf.Language.getText("error_" + text);
    if (dlg && dlg.init(_tmp, cf.Language.getText("text_1098"), null, false, callback, target)) {
        return dlg;
    }
    dlg = null;
    return null;
};

/**
 * 创建自定义对话框
 * @param text 提示内容
 * @param strRight 右按钮
 * @param strLeft 左按钮
 * @param bClose 是否显示关闭按钮：true显示
 * @param callback 回调函数
 * @param target 父节点
 * @returns {*}
 */
UIDialog.create = function (text, strRight, strLeft, bClose, callback, target) {
    var dlg = new UIDialog();
    text = cf.Language.getText(text);
    strRight = cf.Language.getText(strRight);
    if (strLeft != null)
        strLeft = cf.Language.getText(strLeft);
    if (dlg && dlg.init(text, strRight, strLeft, bClose, callback, target)) {
        return dlg;
    }
    dlg = null;
    return null;
};

/**
 * 创建自定义对话框
 * @param text 提示内容
 * @param strRight 右按钮
 * @param strLeft 左按钮
 * @param bClose 是否显示关闭按钮：true显示
 * @param callback 回调函数
 * @param target 父节点
 * @returns {*}
 */
UIDialog.createCustom = function (text, strRight, strLeft, bClose, callback, target) {
    var dlg = new UIDialog();
    if (dlg && dlg.init(text, strRight, strLeft, bClose, callback, target)) {
        return dlg;
    }
    dlg = null;
    return null;
};

/**
 * 创建富文本对话框
 * @param text
 * @param color
 * @param strRight
 * @param strLeft
 * @param bClose
 * @param callback
 * @param target
 * @returns {*}
 */
UIDialog.createCustomWithRichText = function (text, color, strRight, strLeft, bClose, callback, target) {
    var dlg = new UIDialog();
    if (dlg && dlg.initRichText(text, color, strRight, strLeft, bClose, callback, target)) {
        return dlg;
    }
    dlg = null;
    return null;
};