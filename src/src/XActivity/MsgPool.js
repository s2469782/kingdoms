var MsgPool = cc.Class.extend({
    _chatMsgALLArray: null,
    _chatMsgINRArray: null,
    _announcementArray: null,
    _picAnnoucementArray: null,
    _textAnnouncementArray: null,
    _chatReCordServer: null,
    _chatReCordRoom: null,
    ctor: function () {
        var self = this;
        self._chatMsgALLArray = [];
        self._chatMsgINRArray = [];
        self._chatReCordServer = [];
        self._chatReCordRoom = [];
        self._announcementArray = [];
        self._picAnnouncementArray = [];
        self._textAnnouncementArray = [];
    },
    addChatMsgALLElement: function (element) {
        var self = this;
        if (!element) {
            cc.log("The argument maybe null!");
        }
        else {
            self._chatMsgALLArray.push(element);
        }
    },

    addChatMsgINRElement: function (element) {
        var self = this;
        if (!element) {
            cc.log("The argument maybe null!");
        }
        else {
            self._chatMsgINRArray.push(element);
        }
    },

    /**
     * 添加本地的全服聊天记录 最多保存30条
     * element: {struct}
     * property name 玩家姓名 content聊天内容
     */
    addChatRecordServer: function (element) {
        var self = this;
        if (!element) {
            cc.log("The argument maybe null!");
        }
        else {
            if(self._chatReCordServer.length >= 30) {
                self._chatReCordServer.splice(0, 1);
                self._chatReCordServer.push(element);
            }
            else {
                self._chatReCordServer.push(element);
            }
        }
    },

    /**
     * 添加本地的房间聊天记录 最多保存30条
     * element: {struct}
     * property name 玩家姓名 content聊天内容
     */
    addChatRecordRoom: function (element) {
        var self = this;
        if (!element) {
            cc.log("The argument maybe null!");
        }
        else {
            if(self._chatReCordRoom.length >= 30) {
                self._chatReCordRoom.splice(0, 1);
                self._chatReCordRoom.push(element);
            }
            else {
                self._chatReCordRoom.push(element);
            }
        }
    },

    addAnnouncementElement: function (element) {
        var self = this;
        if (!element) {
            cc.log("The argument maybe null!");
        }
        else {
            self._announcementArray.push(element);
            self._announcementArray.sort(function(a, b){
                return parseInt( b.strEventType)- parseInt( a.strEventType );
            });

        }
    },
    setPicAnnouncementElement: function (array) {
        var self = this;
        if (!array) {
            console.log("The argument maybe null!");
        }
        else {
            self._picAnnouncementArray = array;
        }
    },
    setTextAnnouncementArray: function (array) {
        if (!array) {
            console.log("The argument maybe null!");
        }
        else {
            this._textAnnouncementArray = array;
        }
    },
    removeChatMsgALLElement: function () {
        var self = this;
        if (self._chatMsgALLArray[0]) {
            self._chatMsgALLArray.splice(0, 1);
        }
    },
    removeChatMsgINRElement: function () {
        var self = this;
        if (self._chatMsgINRArray[0]) {
            self._chatMsgINRArray.splice(0, 1);
        }
    },
    removeAnnouncementElement: function (scrollId) {
        var self = this;
        if (self._announcementArray) {
            var length = self._announcementArray.length;
            for (var i = 0; i < length; i++) {
                cc.log(scrollId , self._announcementArray[i].nScrollID);
                if (scrollId == self._announcementArray[i].nScrollID) {
                    self._announcementArray.splice(i, 1);
                    return;
                }
            }
        }
    },

    removePicAnnouncementElement: function (picInfoId) {
        var self = this;
        if (self._picAnnouncementArray) {
            var length = self._picAnnouncementArray.length;
            for (var i = 0; i < length; i++) {
                if (picInfoId == self._picAnnouncementArray[i].nPicNoticeID) {
                    self._picAnnouncementArray.splice(i, 1);
                    return;
                }
            }
        }
    },

    removeTextAnnouncementElement: function (textInfoId) {
        var self = this;
        if (this._textAnnouncementArray) {
            var length = self._picAnnouncementArray.length;
            for (var i = 0; i < length; i++) {
                if (textInfoId == self._picAnnouncementArray[i]._nPicNoticeID) {
                    this._picAnnouncementArray.splice(i, 1);
		            return;
                }
            }
        }
    },

    getChatMsgALLElementLength: function () {
        var self = this;
        return self._chatMsgALLArray.length;
    },

    getChatMsgINRArrayLength: function () {
        var self = this;
        return self._chatMsgINRArray.length;
    },

    getChatRecordServerElementLength: function () {
        var self = this;
        return self._chatReCordServer.length;
    },

    getChatRecordRoomElementLength: function () {
        var self = this;
        return self._chatReCordRoom.length;
    },

    getAnnouncementArrayLength: function () {
        var self = this;
        return self._announcementArray.length;
    },

    getPicAnnouncementArrayLength: function () {
        var self = this;
        return self._picAnnouncementArray.length;
    },

    getTextAnnouncementArrayLength: function () {
        return this._textAnnouncementArray.length;
    },

    getChatMsgALLArray: function () {
        var self = this;
        return self._chatMsgALLArray;
    },

    getChatMsgINRArray: function () {
        var self = this;
        return self._chatMsgINRArray;
    },

    getChatRecordServer: function () {
        var self = this;
        return self._chatReCordServer;
    },

    getChatRecordRoom: function () {
        var self = this;
        return self._chatReCordRoom;
    },

    getAnnouncementArray: function () {
        var self = this;
        return self._announcementArray;
    },
    getPicAnnouncementArray: function () {
        var self = this;
        return self._picAnnouncementArray;
    },
    getTextAnnouncementArray: function () {
        return this._textAnnouncementArray;
    },
    getAElementByScrollInfoId: function (scrollInfoId) {
        if (this._announcementArray.length > 0) {
            var length = this._announcementArray.length;
            for (var i = 0; i < length; i++) {
                if (scrollInfoId == this._announcementArray[i].nScrollID) {
                    return this._announcementArray[i];
                }
            }
            return null;
        }
    },
    getAElementByPopupAnnounceInfoId: function (picInfoId) {
        if (this._picAnnouncementArray.length > 0) {
            var length = this._picAnnouncementArray.length;
            for (var i = 0; i < length; i++) {
                if (picInfoId == this._picAnnouncementArray[i].nPicNoticeID) {
                    return this._picAnnouncementArray[i];
                }
            }
            return null;
        }
    },

    /**
     * 每次退出房间时需要重置房间内的聊天记录
     */
    resetChatRecordRoom: function () {
        this._chatReCordRoom = [];
    }
});

/**
 * 消息池实例
 * @type {undefined}
 * @private
 */
MsgPool._sInstance = undefined;
MsgPool.getInstance = function () {
    if (!MsgPool._sInstance) {
        MsgPool._sInstance = new MsgPool();
    }
    return MsgPool._sInstance;
};

/**
 * 移除实例
 */
MsgPool.purgeRelease = function () {
    if (MsgPool._sInstance) {
        MsgPool._sInstance.onClear();
        MsgPool._sInstance = null;
    }
};