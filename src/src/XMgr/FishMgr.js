var FishMgr = cc.Class.extend({
    _delegate: null,
    _fishNode: null,                //< 鱼节点
    _fishMap: null,               //< 鱼数组
    _serverTime: 0,                 //< 服务器时间
    _localTime: 0,                  //< 本地时间
    _pathOffset: null,               //< 路径偏移
    _fishInfoList: null,            //< 鱼信息
    _onNewFishListener: null,       //< 新鱼刷新监听

    //_quadTree:null,
    //_nodeMap:null,
    /**
     * 初始化
     * @param delegate
     */
    init: function (delegate) {
        this._delegate = delegate;
        //this._quadTree = quadTree;
        //this._nodeMap = nodeMap;
    },

    /**
     * 释放资源
     */
    onClear: function () {
        cc.loader.releaseAll();
        if (this._fishNode) {
            this._fishNode.removeAllChildren(true);
            this._fishNode.removeFromParent(true);
        }
        this._fishMap = null;
        //移除监听
        this.removeListener();
    },

    /**
     * 初始化数据
     * @param fishInfoList 鱼信息列表
     * @param serverTime 服务器时间
     */
    createFishData: function (fishInfoList, serverTime) {
        var self = this;
        self._fishNode = new cc.Node();
        self._delegate.addChild(self._fishNode, cf.Zoder.FISH);
        self._fishMap = cf.Map.create();
        //获取消息
        self._fishInfoList = fishInfoList;
        self._serverTime = serverTime;
        self._pathOffset = cc.p(0, 0);
        self.addListener();
        self.createFish();
    },

    /**
     * 创建鱼
     */
    createFish: function () {
        var self = this;
        //收到鱼群列表，根据鱼群列表创建鱼群
        for (var i = 0; i < self._fishInfoList.length; ++i) {
            //创建
            var fishInfo = self._fishInfoList[i];
          
            //本地时间差值
            self._createGroupFish(fishInfo);
        }
    },

    /**
     * 逻辑更新
     * @param dt
     */
    updateLogic: function (dt) {

    },

    /**
     * 子弹碰撞前
     * @param bullet
     */
    collisionWithBulletBefore: function (bullet) {
        //检测子弹状态
        if (bullet.getBulletState() != Bullet.State.MOVE)
            return;

        /*this._quadTree.clear();
         this._quadTree.insert(this._nodeMap.values());
         var item;
         var items = this._quadTree.retrieve(bullet);
         var len = items.length;
         cc.log("quad = "+len+", size = "+this._nodeMap.size());
         for ( var j = 0; j < len; ++j ){
         item = items[j];
         if(item.getGroupIndex() == 1)
         continue;
         //屏幕内并且鱼可以检测碰撞
         if ( item.isVisible() && cf.checkInScreen(item) ) {
         if (cc.sys.isNative) {
         item.updateCollisionData();
         bullet.updateCollisionData();
         if (bullet.collisionWithFish(item)) {
         bullet.openNets(item);
         break;
         }
         } else {
         var collisionData = item._collisionData;
         //先检测外围圆
         if (cf.collisionPointAndCircleEx(bullet.x, bullet.y,collisionData.circleArray[0])) {
         if (bullet.collisionWithFish(item)) {
         //抓鱼
         bullet.openNets(item);
         break;
         }
         }
         }
         }
         }*/
        var lockedElement = bullet.getLockedElement();
        var player = cf.RoomMgr.getRoomPlayer(bullet.getSeatID());
        var fishArrayLength = this._fishMap.size();
        if (lockedElement && player.getLockElement()) {
            var condition = (lockedElement == player.getLockElement());
            if (lockedElement.checkCanLock() && condition) {
                var fishCenter = lockedElement.getPosition();
                var bulletCenter = bullet.getPosition();
                //按照距离更新位置
                if (cc.pDistance(fishCenter, bulletCenter) < 50) {
                    bullet.openNets(lockedElement);
                }
                return;
            }
        }

        for (var i = 0; i < fishArrayLength; ++i) {
            var fish = this._fishMap._elements[i].value;
            if (cc.sys.isNative) {
                //本地版碰撞检测
                if (fish._fishState == BaseFish.State.MOVE) {
                    if (bullet.collisionWithFish(fish)) {
                        bullet.openNets(fish);
                        break;
                    }
                }
            } else {
                //h5版碰检检测
                if (cf.checkInScreen(fish) && fish._fishState == BaseFish.State.MOVE) {
                    var collisionData = fish._collisionData;
                    //先检测外围圆
                    if(!fish._collisionData){
                        return;
                    }
                    var bulletCircle = bullet._collisionData.circleArray[0];
                    if (cf.collisionCircleAndCircleEx(bulletCircle, collisionData.circleArray[0])) {
                        if (bullet.collisionWithFish(fish)) {
                            //抓鱼
                            bullet.openNets(fish);
                            break;
                        }
                    }
                }
            }
        }
    },

    /**
     * 移除鱼精灵
     * @param msg 玩家捕捉消息
     */
    removeFish: function (msg) {
        var len = msg.listFish.length;
        for (var i = 0; i < len; ++i) {
            var serverIndex = msg.listFish[i].strFishID;
            //cc.log("被捕捉鱼 index = " + serverIndex);
            var fish = this._fishMap.get(serverIndex);
            if (fish) {
               
                fish.doDied(msg);
                this._fishMap.remove(serverIndex);
                //this._nodeMap.remove(serverIndex);
            }
            else {
                if(msg.nSeatID == -1) {
                    cf.EffectMgr.playGetItem(cc.p(cc.winSize.width / 2,cc.winSize.height / 2),msg);
                }
            }
        }
    },

    removeFishFromMap: function (serverIndex) {
        this._fishMap.remove(serverIndex);
        //this._nodeMap.remove(serverIndex);
    },

    /**
     * 设置本地时间
     * @param val
     */
    setLocalTime: function (val) {
        this._localTime = val;
    },

    /**
     * 创建鱼群
     * @param fishInfo 鱼群信息
     * @param trackTime 时间戳
     * @private
     */
    _createGroupFish: function (fishInfo) {
        var self = this;
        //查表获取数据
        if(!fishInfo.bTide) {

            var groupData = cf.Data.FISH_GROUP_DATA[fishInfo.nFishGroupID];
            if (groupData == null || groupData == undefined) {
                cc.error("创建鱼组错误 nFishGroupID = " + fishInfo.nFishGroupID);
                return;
            }
        }
        //服务器时间
        var serverTime = Math.floor(self._serverTime);
        //冰冻时间结束 如果为0或者""则表示没有处于冰冻状态
        var iceSkillEndTime = fishInfo.strSkillIceEndTime;
        //这里数组内保存是的鱼索引，乱序
        var lastFishLen = fishInfo.vecFishInfo.length;
        for (var i = 0; i < lastFishLen; ++i) {
            var localFishInfo = fishInfo.vecFishInfo[i];
            //获取鱼组内索引
            var index = localFishInfo.nFishIndex;
            //构建服务器索引
            var _tmpIndex = localFishInfo.strFishIndex;
            var fishId;
            if(fishInfo.bTide) {
                var fishTideId = fishInfo.nFishGroupID;
                var offsetArray = cf.Data.FISHARRAYDATA;
                var localArray = offsetArray[fishTideId];
                var fishIdArray = localArray.fishId;
                fishId = fishIdArray[index];
               
                cc.log(fishId + "鱼潮中鱼的id为--------------------------------------------" + _tmpIndex);
                if(fishId == undefined) {
                    cc.log("获取鱼的id为空" + "fishTide 。。。。。。。。。。。。。。。。。。。。");
                }
            }
            else {
                
                fishId = groupData.fishId;
                
            }
            //创建鱼
            var spFish = null;
            var fishData = cf.Data.FISH_DATA[fishId];
            switch (fishData.animateType) {
                case cf.AnimateType.FRAME://帧动画
                   
                    cc.log(fishId + "鱼的id为--------------------------------------------" + _tmpIndex);
                    spFish = FrameFish.create(fishId, _tmpIndex , fishInfo.bFeatsFish);
                    break;
                case cf.AnimateType.BONES://骨骼动画
                
                    cc.log(fishId + "鱼的id为--------------------------------------------" + _tmpIndex);
                    spFish = SpineFish.create(fishId, _tmpIndex);
                    break;
            }
            //创建路径
            var pathData = null, teamTime = 0;
            var groupType;
            if(fishInfo.bTide) {
                groupType = cf.FishGroupType.TEAM;
            }
            else {
                groupType = groupData.groupType;
            }
            if(fishId == 27 || fishId == 28){
                console.log(fishInfo.nPathID);
            }
            switch (groupType) {
                case cf.FishGroupType.GROUP:
                    //fishInfo.nPathID
                    //var num = Math.floor((Math.random()*4)+16);
                    
                    pathData = self._getPath(fishInfo.nPathID, true, groupData.distance, i);
                    break;
                case cf.FishGroupType.TEAM:
                    if(fishInfo.bTide){
                       // fishInfo.nPathID = 5;
                     
                        pathData = self._getPath(fishInfo.nPathID, false, 0);
                    }
                    else {
                       
                        teamTime = groupData.interval * index;
                       
                        //fishInfo.nPathID = 5;
                        pathData = self._getPath(fishInfo.nPathID, false, 0);
                    }
                    break;
            }

            //鱼开始时间
            var strStartTime = Math.floor(localFishInfo.strStartTime);
            if(fishInfo.bTide){
                cc.log(strStartTime + fishId + "----------------------" + _tmpIndex);
            }
            //loading损失的间隔时间
            var interval = new Date().getTime() - self._localTime;
            //鱼如果被锁定当前的速度
            var nNowSpeed = localFishInfo.nNowSpeed;
            var nNormalSpeed = pathData.duration;
            //当前作用效果的玩家座位id 如果为-1表示没有锁定技能作用于该精灵
            var nNowSeatID = localFishInfo.nNowSeatID;
            //锁定技能效果的剩余时间 "" 为空代表并没有进行作用
            var nLockLastTime = localFishInfo.nLockLastTime;
            var strLockStartTime = Math.floor(localFishInfo.strLockStartTime);
            //作用在鱼身上的所有锁定效果
            var lockInfo = localFishInfo.lockInfo;

            var isLockEffect = false, isIceEffect = true;
            //锁定效果
            if (nNowSeatID != -1 || lockInfo.length > 0) {
                isLockEffect = true;
            }

            //冰冻效果
            if (iceSkillEndTime == 0 || iceSkillEndTime == "") {
                isIceEffect = false;
            }

            //计算时间
            var trackTime = 0;
            if (!isLockEffect && !isIceEffect) {
                //完全无效果
                trackTime = serverTime - strStartTime - teamTime;
                if (interval > 0) {
                    trackTime = trackTime + interval;
                }
                if(spFish._fishModel.getType() == cf.FishType.MATCH) {
                    cc.log(trackTime);
                }
            } else {
                //冰冻效果
                if (isIceEffect) {
                    trackTime = iceSkillEndTime - cf.Data.SKILLDATA[cf.SkillId.iceSkill].skill_last * 1000 - strStartTime - teamTime;
                    spFish.setFrozenEndTime(iceSkillEndTime - serverTime);
                    spFish.comStageIce = true;
                }
                //锁定效果
                if (isLockEffect) {
                    //两种效果同时存在，则只执行冰冻效果
                    if (!isIceEffect) {
                        var localTime = serverTime - strLockStartTime;
                        var startTime = strLockStartTime - strStartTime;
                        trackTime = startTime + localTime * (nNormalSpeed / nNowSpeed);
                    }
                    spFish.setLockInfo(lockInfo);
                    spFish.comStageLock = true;
                }
            }
            cc.log(fishInfo.bTide);
            if(fishInfo.bTide) {
                trackTime = serverTime - strStartTime;
                var offsetPointArray = cf.Data.FISHARRAYDATA[fishInfo.nFishGroupID].offsetPoint;
                var maxOffsetX = cf.Data.FISHARRAYDATA[fishInfo.nFishGroupID].maxOffsetX;
                cc.log(offsetPointArray[index].x,offsetPointArray[index].y);
                spFish.doPath(pathData, trackTime, offsetPointArray[index],maxOffsetX);
            }
            else {
                cc.log("精灵执行动作" + "精灵类型" + spFish._fishModel.getType() + "精灵id" + spFish._fishModel._fishId);
                spFish.doPath(pathData, trackTime);
            }

            
            //添加进数组
            if (spFish) {
                var commonRoom = cf.RoomMgr.getRoom();
                if(commonRoom){
                    if(fishInfo.nFishGroupID > 500){
                        spFish.doSummonSkillAction();
                    }
                }
                self._fishNode.addChild(spFish, fishData.showPriority, _tmpIndex);
                self._fishMap.put(_tmpIndex, spFish);
            }
            //this._quadTree.insert(spFish);
            //this._nodeMap.put(_tmpIndex,spFish);
        }
    },

    /**
     * 获取路径信息
     * @param pathId 路径id
     * @param isOffset 是否偏移
     * @param offset 偏移量
     * @param styleIndex 阵型索引
     * @returns {*}
     * @private
     */
    _getPath: function (pathId, isOffset, offset, styleIndex) {
        //克隆路径信息，避免传递对象引用使值发生变化
        var test = cf.Data.PATH_DATA[pathId];
        if (test == null)
            cc.error("111111111");
        var pathData = cf.clone(test);
        if (isOffset && styleIndex != 0) {
            //根据鱼群数据修正路径信息
            this._pathOffset.y = offset * FishMgr.GroupStyle[styleIndex];
            this._pathOffset.x = offset * FishMgr.GroupStyle1[styleIndex];

            //起始点
            if (cf.randomBool())
                cc.pAddIn(pathData.startPoint, this._pathOffset);
            else
                cc.pAddIn(pathData.startPoint, this._pathOffset);
            //终点
            if (cf.randomBool())
                cc.pAddIn(pathData.endPoint, this._pathOffset);
            else
                cc.pAddIn(pathData.endPoint, this._pathOffset);
            //中间点
            var len = pathData.wayPoint.length;
            for (var i = 0; i < len; ++i) {

                if (cf.randomBool())
                    cc.pAddIn(pathData.wayPoint[i], this._pathOffset);
                else
                    cc.pAddIn(pathData.wayPoint[i], this._pathOffset);
            }
        }
        return pathData;
    },

    /**********************************************************
     * 网络逻辑
     **********************************************************/
    /**
     * 创建监听
     */
    addListener: function () {
        var self = this;
        //创建新鱼进场监听
        self._onNewFishListener = cf.addNetListener(
            MessageCode.SC_NewFishJoinRoom,
            function (msg) {
                self._resNewFishJoinRoom(msg);
            }
        );
    },

    /**
     * 移除监听
     */
    removeListener: function () {
        cf.removeListener(this._onNewFishListener);
    },

    /**
     * 解析新鱼进入房间消息
     * @param msg
     * @private
     */
    _resNewFishJoinRoom: function (msg) {
        var self = this;

        var resMsg = $root.SC_NewFishJoinRoom.decode(msg);
        if (resMsg) {
            //cc.log("新鱼进场");
            self._fishInfoList = resMsg.newFishGroupRoom;
            self._serverTime = resMsg.strTime;
            self._localTime = new Date().getTime();
            self.createFish();

            //同步服务器时间
            cf.PlayerMgr.getLocalPlayer().setGameTime(resMsg.strTime);
        } else {
            cc.log("_resNewFishJoinRoom错误:SC_NewFishJoinRoom");
        }
    },

    /**
     * 获取包含鱼的容器Map
     */
    getFishMap: function () {
        var self = this;
        return self._fishMap || null;
    },

    /**
     * 获取承载鱼的节点
     */
    getFishNode: function () {
        return this._fishNode;
    },

    /**
     * 获取当前技能优先级最高的精灵对象
     */
    getThePriorityOfAllElement: function () {
        var self = this;
        var length = self._fishMap.size();
        if(length <= 0) {
            return null;
        }
        var maxFish;
        for (var i = 0; i < length; i++) {
            var fish = self._fishMap._elements[i].value;
            if (fish.checkInScreen()) {
                if (!maxFish) {
                    maxFish = fish;
                }
                if (maxFish.getLockPriority() < fish.getLockPriority()) {
                    maxFish = fish;
                }
            }
        }
        if (maxFish) {
            //cc.log("优先级为" + maxFish.getLockPriority() + "========---------------------==========");
        }
        return maxFish;
    },

    /**
     * 选取特定优先级的鱼
     */
    getSpecialPriorityOfAllElement: function () {
        var self = this;
        var length = self._fishMap.size();
        if(length <= 0) {
            return null;
        }
        var lockPriority = [150,140,130,120,110,100,90,80,70,60,50];
        for(var index in lockPriority) {
            for (var i = 0; i < length; i++) {
                var fish = self._fishMap._elements[i].value;
                if (fish.checkInScreen()) {
                    if (fish.getLockPriority() == lockPriority[index]) {
                        return fish;
                    }
                }
            }
        }

        return self._fishMap._elements[0].value
    },

    /**
     * 根据阵列Id生成相应的整列
     */
    createFishArrayByArrayId: function(fishTideMsg) {
        cc.log("创建鱼潮");
        
        var self = this;
        var serverTime = fishTideMsg.strTime;
        var arrayId = fishTideMsg.nFishTideID;
        var fishTideArray = cf.Data.FISHTIDEDATA;
        var birthInterval = fishTideArray[arrayId].fishTideInterval;
        var fishArray = fishTideArray[arrayId].fishTideArray;
        for(var i in fishArray) {
            self._birthFishInFishTide(i,birthInterval,serverTime,arrayId);
        }
    },

    _birthFishInFishTide: function (fishArrayId,intervalList,serverTime,fishTideId) {
        var self = this;
        //鱼潮单一阵列的间隔时间
        var intervalList = intervalList;
        var offsetArray = cf.Data.FISHARRAYDATA;
        var fishTideArray = cf.Data.FISHTIDEDATA;
        var fishArray = fishTideArray[fishTideId].fishTideArray;
        var localArray = offsetArray[fishArray[fishArrayId]];
        var fishIdArray = localArray.fishId;
        var fishNum = fishIdArray.length;
        var trackTime = 0;
        for(var i = 0;i < parseInt(fishArrayId) + 1;i++) {
            trackTime = trackTime + intervalList[i] * 1000;
        }
        cc.log(fishTideId + "-------------------------------------" + trackTime);
        for(var index = 0;index < fishNum;index ++) {
            //根据路径形成相应的对应精灵的路径
            var spFish = null;
            var _tmpIndex = fishTideId.toString() + "-" + fishArrayId.toString() + "-" + index.toString();
            cc.log(_tmpIndex + "鱼潮中鱼的索引为" + fishIdArray[index] + "鱼id为");
            var fishData = cf.Data.FISH_DATA[fishIdArray[index]];
            switch (fishData.animateType) {
                case cf.AnimateType.FRAME://帧动画
                    spFish = FrameFish.create(fishIdArray[index], _tmpIndex);
                    break;
                case cf.AnimateType.BONES://骨骼动画
                    spFish = SpineFish.create(fishIdArray[index], _tmpIndex);
                    break;
            }
            //创建路径
            var pathData = null;
            pathData = self._getPath(localArray.pathId, false, 0);
            //完全无效果
            var offsetPointArray = localArray.offsetPoint;
            var maxOffsetX = localArray.maxOffsetX;
            spFish.doPath(pathData, -trackTime,offsetPointArray[index],maxOffsetX);
            
            //_tmpIndex
            //处理锁定冰冻并执行路径运动
            //添加进数组
            if (spFish) {
                self._fishNode.addChild(spFish, fishData.showPriority, _tmpIndex);
                self._fishMap.put(_tmpIndex, spFish);
            }
        }
    }
});

FishMgr.GroupStyle = [0, -1, 1.2, 0.2, -0.5, 1, 0, -1.6, -0.6, 1.5, 2.2, 0.3];
FishMgr.GroupStyle1 = [0, -1.3, -1.8, -2.3, 1, 1, 2, -3, -4.5, -5, -3.5, -6.5];

/**
 * 鱼管理器实例
 * @type {undefined}
 * @private
 */
FishMgr._sInstance = undefined;
FishMgr.getInstance = function () {
    if (!FishMgr._sInstance) {
        FishMgr._sInstance = new FishMgr();
    }
    return FishMgr._sInstance;
};

/**
 * 移除实例
 */
FishMgr.purgeRelease = function () {
    if (FishMgr._sInstance) {
        FishMgr._sInstance.onClear();
        FishMgr._sInstance = null;
    }
};