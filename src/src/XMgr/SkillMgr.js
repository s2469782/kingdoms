var SkillMgr = cc.Class.extend({

    //冰冻和召唤为房间内适用不需要记录多个玩家的技能数据
    lockSkillList: null,
    rageSkillList: null,
    iceSkill: null,
    summonSkill: null,

    init: function (delegate) {
        var self = this;
        self.addListener();
        self.initData();
        self._delegate = delegate;
    },

    initData: function () {
        var self = this;
        self.initLockSkillList();
        self.initRageSkillList();
        self.initIceSkill();
        self.initSummonSkill();
    },

    initLockSkillList: function () {
        this.lockSkillList = [];
        for (var i = 0; i < 4; i++) {
            var lockSkill = new BaseSkill(cf.SkillId.lockSkill);
            this.lockSkillList.push(lockSkill);
        }
    },

    initRageSkillList: function () {
        this.rageSkillList = [];
        for (var i = 0; i < 4; i++) {
            var lockSkill = new BaseSkill(cf.SkillId.rageSkill);
            this.rageSkillList.push(lockSkill);
        }
    },

    initIceSkill: function () {
        this.iceSkill = new BaseSkill(cf.SkillId.iceSkill);
    },

    initSummonSkill: function () {
        this.summonSkill = new BaseSkill(cf.SkillId.summonSkill);
    },

    getLockSkillState: function (seatId) {
        if (!this.lockSkillList[seatId].getSkillState()) {
            return false;
        }
        return true;
    },

    checkLockSkillTime: function (seatId, dt) {
        this.lockSkillList[seatId].checkLocalSkillIsEnd(dt);
        this.lockSkillList[seatId].checkLocalSkillIsCd(dt);
    },

    setLockSkillOpen: function (seatId) {
        this.lockSkillList[seatId].setSkillOpen();
    },

    setLockSkillLastTime: function (seatId, lastTime) {
        this.lockSkillList[seatId].setSkillLastTime(lastTime);
    },

    getLockSkillLastTime: function (seatId) {
        return this.lockSkillList[seatId].getSkillLastTime();
    },

    getLockSkillCooling: function (seatId) {
        return this.lockSkillList[seatId].getSkillCooling();
    },

    getLockSkillCountTime: function (seatId) {
        return this.lockSkillList[seatId].getSkillLastTime() - this.lockSkillList[seatId].getSkillCountTime();
    },

    stopLockSkill: function (seatId) {
        this.lockSkillList[seatId].setSkillEnd();
    },

    resetLockSkill:function (seatId) {
        this.lockSkillList[seatId].resetSkillData();
    },

    getIceSkillState: function () {
        if (!this.iceSkill.getSkillState()) {
            return false;
        }
        return true;
    },

    getIceSkillCooling: function () {
        if (!this.iceSkill.getSkillCooling()) {
            return false;
        }
        return true;
    },

    checkIceSkillTime: function (dt) {
        this.iceSkill.checkLocalSkillIsEnd(dt);
        this.iceSkill.checkLocalSkillIsCd(dt);
    },

    setIceSkillOpen: function () {
        this.iceSkill.setSkillOpen();
    },

    setIceSkillLastTime: function (lastTime) {
        this.iceSkill.setSkillLastTime(lastTime);
    },

    checkSummonSkillTime: function (dt) {
        this.summonSkill.checkLocalSkillIsCd(dt);
    },

    getSummonSkillCooling: function () {
        return this.summonSkill.getSkillCooling();
    },

    getRageSkillState: function (seatId) {
        if (!this.rageSkillList[seatId].getSkillState()) {
            return false;
        }
        return true;
    },

    checkRageSkillTime: function (seatId, dt) {
        this.rageSkillList[seatId].checkLocalSkillIsEnd(dt);
        this.rageSkillList[seatId].checkLocalSkillIsCd(dt);
    },

    setRageSkillOpen: function (seatId) {
        this.rageSkillList[seatId].setSkillOpen();
    },

    setRageSkillLastTime: function (seatId, lastTime) {
        this.rageSkillList[seatId].setSkillLastTime(lastTime);
    },

    getRageSkillLastTime: function (seatId) {
        return this.rageSkillList[seatId].getSkillLastTime();
    },

    getRageSkillCooling: function (seatId) {
        return this.rageSkillList[seatId].getSkillCooling();
    },

    getRageSkillCountTime: function (seatId) {
        return this.rageSkillList[seatId].getSkillLastTime() - this.rageSkillList[seatId].getSkillCountTime();
    },

    stopRageSkill: function (seatId) {
        this.rageSkillList[seatId].setSkillEnd();
    },

    checkPlayerCanFire: function (seatId) {
        if (!cf.SkillMgr.getLockSkillState(seatId) || !cf.SkillMgr.getRageSkillState(seatId)) {
            return true;
        }
        return false;
    },

    /**
     * 添加监听
     */
    addListener: function () {
        var self = this;
        //创建网络消息
        var msgDataArray = [
            //玩家使用技能
            {msgId: MessageCode.CS_SkillUseMsg_Res, callFunc: self._resConsumeSkill},
            //玩家锁定精灵
            {msgId: MessageCode.CS_SkillLockMsg_Res, callFunc: self._resLockSkill},
            //玩家技能结束
            {msgId: MessageCode.SC_IceEnd, callFunc: self._resSkillEnd},
        ];

        //创建网络监听
        self._onNetMsgListener = [];
        var len = msgDataArray.length;
        for (var i = 0; i < len; ++i) {
            var msgData = msgDataArray[i];
            self._onNetMsgListener[i] = cf.addNetListener(msgData.msgId, msgData.callFunc.bind(this));
        }
    },

    /**
     * 锁定技能使用消息处理
     * @param msg
     * @private
     */
    _resLockSkill: function (msg) {
        var resMsg = $root.FishInfo_Room.decode(msg);
        if (resMsg) {
            //cc.log("锁定效果切换消息" + "-------------------------------------------------");
            var strFishIndex = resMsg.strFishIndex;
            var commonRoom = cf.RoomMgr.getRoom();
            if (!commonRoom) {
                cc.log("获取当前房间实例出错");
                return;
            }
            //var normalSpeed  = resMsg._nNormalSpeed;
            var lockInfo = resMsg.lockInfo;
            var fish = cf.FishMgr.getFishMap().get(strFishIndex);
            if (!fish) {
                cc.log("获取锁定的精灵出现错误");
                return;
            }
            var normalSpeed = fish._originalDuration;
            var maxSpeed = resMsg.nNowSpeed;
            if (lockInfo.length == 0) {
                //cc.log("解除锁定");
                fish.unLockElement();
                return;
            }
            //cc.log("锁定列表的长度为" + lockInfo.length + ".............................................");
            var seatIdArray = [];
            for (var i in lockInfo) {
                var analysisMsg = lockInfo[i];
                var seatId = analysisMsg.nSeatID;
                seatIdArray.push(seatId);
                var lastTime = analysisMsg.nLastTime;
                var player = commonRoom._roomPlayer[seatId];
                if (seatId == cf.PlayerMgr.getLocalPlayer().getSeatID()) {
                    player.setHasSendLockObject(false);
                }
                this.lockSkillList[seatId].resetCountTime();
                if (lastTime >= 0) {
                    this.lockSkillList[seatId].setSkillLastTime(lastTime / 1000);
                }
                else {
                    //给出一个比较大的值用来维持锁定的状态
                    this.lockSkillList[seatId].setSkillLastTime(50);
                }
                //cc.log("技能的持续时间为" + lastTime + "当前的精灵速度" + analysisMsg._nSpeed + "当前精灵的初始速度为" + normalSpeed + seatId);
                //移除精灵的锁定效果图标
                player.setLockElement(fish);
                //cc.log("锁定效果生效的玩家座位Id为" + seatId + ".............................................");
            }
            //cc.log("锁定效果切换消息" + normalSpeed + "-----------------------------------");
            if (!fish.getIsFrozen()) {
                //cc.log("非冰冻的情况下设置速度");
                fish.setActionSpeedChange(false, maxSpeed);
            }
            else {
                fish.setSlowFactor(normalSpeed / maxSpeed);
            }
        }
    },

    /**
     * 技能使用消息处理
     * @param msg
     * @private
     */
    _resConsumeSkill: function (msg) {
        cf.UITools.hideLoadingToast();
        var resMsg = $root.SC_SkillUseMsg.decode(msg);
        if (resMsg) {
            var seatId = resMsg.nSeatId;
            var skillId = resMsg.nSkillId;
            var length = resMsg.itemReward.length;
            var commonRoom = cf.RoomMgr.getRoom();
            if (!commonRoom) {
                cc.log("获取当前房间实例出错");
                return;
            }
            var playerSeatId = commonRoom._locPlayer.getSeatID();
            for (var i = 0; i < length; i++) {
                var itemReward = resMsg.itemReward[i];
                if (seatId == playerSeatId) {
                    commonRoom._skillNumUpdate(skillId, itemReward);
                }
            }
            var strEndTime = resMsg.strEndTime;
            commonRoom.setRoomServerTime(parseFloat(resMsg.strNowTime));
            var presentTimeClient = (new Date()).getTime();

            var lastTime = (strEndTime - commonRoom.getRoomServerTime()) / 1000;
            commonRoom.setRoomCSTimeDif(presentTimeClient - commonRoom.getRoomServerTime());
            switch (skillId) {
                case cf.SkillId.lockSkill:
                    this.lockSkillList[seatId].setSkillLastTime(lastTime);
                    this._lockedSkillCall(seatId);
                    break;
                case cf.SkillId.iceSkill:
                    this.iceSkill.setSkillLastTime(lastTime);
                    this._iceSkillCall(seatId);
                    break;
                case cf.SkillId.summonSkill:
                    this._summonSkillCall(seatId);
                    //召唤是瞬时技能 不需要结束时间
                    break;
                case cf.SkillId.rageSkill:
                    this.rageSkillList[seatId].setSkillLastTime(lastTime);
                    this._rageSkillCall(seatId);
                    break;
            }
        }
    },

    /**
     * 玩家技能结束目前仅为锁定技能和狂暴技能
     */
    _resSkillEnd: function (msg) {
        var resMsg = $root.SC_SkillEndMsg.decode(msg);
        //作为id暂时无用
        var seatId = resMsg.nSeatId;
        var skillId = resMsg.nSkillId;
        var cdTime = cf.Data.SKILLDATA[skillId].skill_cd;
        switch (skillId) {
            case cf.SkillId.lockSkill:
                cc.log("玩家的锁定技能结束");
                var seatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
                /*if(this.getLockSkillState(seatId) && ((cdTime - this.lockSkillList[seatId].getSkillCountTime()) > 1)) {
                 this.lockSkillList[seatId].setSkillEnd();
                 }*/
                if (this.getLockSkillState(seatId)) {
                    this.lockSkillList[seatId].setSkillEnd();
                }
                break;
            case cf.SkillId.iceSkill:
                cc.log("玩家的冰冻技能结束");
                this.iceSkill.setSkillEnd();
                break;
            case cf.SkillId.rageSkill:
                if (this.getRageSkillState(seatId) /*&& ((cdTime - this.rageSkillList[seatId].getSkillCountTime()) > 1)*/) {
                    this.rageSkillList[seatId].setSkillEnd();
                }
                break;
        }
    },

    //使用锁定技能后的回调
    _lockedSkillCall: function (seatId) {
        var commonRoom = cf.RoomMgr.getRoom();
        if (!commonRoom) {
            cc.log("获取当前房间实例出错");
            return;
        }
        //cc.log("使用技能的Id" + seatId + (new Date()).getTime());
        var player = commonRoom._locPlayer;
        //如果是本地玩家则将技能置为cd状态
        if (player.getSeatID() == seatId) {
            //cf.dispatchEvent(BaseRoom.DoEvent,BaseRoom.EventFlag.LOCKCOOL);
            this.lockSkillList[seatId].setSkillOpen();
            this.lockSkillList[seatId].resetCountTime();
            player._touch = null;
            // player.setBAutoFireState(false);
            //player.addPromptOfAutoFire();
            //cc.log("lockSkillCall");
        }
        else {
            //非本地玩家则仅开启技能状态
            this.lockSkillList[seatId].setSkillOpen(true);
            var useSkillMsg = {};
            useSkillMsg.flag = BaseRoom.EventFlag.SHOWSKILLUSEICON;
            useSkillMsg.seatId = seatId;
            useSkillMsg.skillId = cf.SkillId.lockSkill;
            cf.dispatchEvent(BaseRoom.DoEvent, useSkillMsg);
        }
    },


    _iceSkillCall: function (seatId) {
        //如果是本地玩家使用技能则进入技能cd 如果时其他使用技能则刷新冰冻的持续时间
        var localSeatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
        cf.EffectMgr.playIceEffect();
        if (seatId == localSeatId) {
            cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.ICECOOL);
            this.iceSkill.setSkillOpen();
            this.iceSkill.setSkillCooling();
            this.iceSkill.resetCountTime();
            this.iceSkillLogic();
        }
        else {
            this.iceSkill.setSkillOpen();
            this.iceSkill.resetCountTime();
            this.iceSkillLogic();
            var useSkillMsg = {};
            useSkillMsg.flag = BaseRoom.EventFlag.SHOWSKILLUSEICON;
            useSkillMsg.seatId = seatId;
            useSkillMsg.skillId = cf.SkillId.iceSkill;
            cf.dispatchEvent(BaseRoom.DoEvent, useSkillMsg);
        }
    },

    iceSkillLogic: function () {
        var fishMgr = cf.FishMgr;
        var fishMap = fishMgr.getFishMap();
        var length = fishMap.size();
        if (length > 0) {
            for (var i = 0; i < length; ++i) {
                var fish = fishMap._elements[i].value;
                fish.setFrozen(true);
            }
        }
    },


    _summonSkillCall: function (seatId) {
        //播放召唤特效
        var localSeatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
        if (localSeatId == seatId) {
            cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.SUMMONCOOL);
            this.summonSkill.setSkillCooling();
        }
    },

    _rageSkillCall: function (seatId) {
        var self = this;
        var commonRoom = cf.RoomMgr.getRoom();
        if (!commonRoom) {
            cc.log("获取当前房间实例出错");
            return;
        }
        var player = commonRoom._locPlayer;
        if (player.getSeatID() == seatId) {
            //cc.log("使用技能的Id" + seatId + (new Date()).getTime());
            //cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.RAGECOOL);
            this.rageSkillList[seatId].resetCountTime();
            this.rageSkillList[seatId].setSkillOpen();
            player.rageSkillLogic();
            player.setBAutoFireState(false);
            player.addPromptOfAutoFire();
            player.addPromptOfRage();
            player._touch = null;
        }
        else {
            this.rageSkillList[seatId].setSkillOpen();
            commonRoom._roomPlayer[seatId].addPromptOfRage();
            var useSkillMsg = {};
            useSkillMsg.flag = BaseRoom.EventFlag.SHOWSKILLUSEICON;
            useSkillMsg.seatId = seatId;
            useSkillMsg.skillId = cf.SkillId.rageSkill;
            cf.dispatchEvent(BaseRoom.DoEvent, useSkillMsg);
        }
    },

    onClear: function () {
        this.removeListener();
    },

    /**
     * 移除监听
     */
    removeListener: function () {
        if (this._onNetMsgListener) {
            var len = this._onNetMsgListener.length;
            for (var i = 0; i < len; ++i) {
                cf.removeListener(this._onNetMsgListener[i]);
            }
        }
    },
});

/**
 * 任务管理实例
 * @type {undefined}
 * @private
 */
SkillMgr._sInstance = undefined;
SkillMgr.getInstance = function () {
    if (!SkillMgr._sInstance) {
        SkillMgr._sInstance = new SkillMgr();
    }
    return SkillMgr._sInstance;
};

/**
 * 移除实例
 */
SkillMgr.purgeRelease = function () {
    if (SkillMgr._sInstance) {
        SkillMgr._sInstance.onClear();
        SkillMgr._sInstance = null;
    }
};
