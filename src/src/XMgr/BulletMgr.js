var BulletMgr = cc.Class.extend({
    _delegate: null,
    _bulletNode: null,
    _bulletMap: null,

    /**
     * 初始化
     * @param delegate
     */
    init: function (delegate) {
        this._delegate = delegate;
        this._bulletNode = new cc.Node();
        this._delegate.addChild(this._bulletNode, cf.Zoder.BULLET);
        this._bulletMap = [];
        for (var i = 0; i < 4; ++i) {
            this._bulletMap[i] = cf.Map.create();
        }
    },

    /**
     * 释放资源
     */
    onClear: function () {
        this._delegate = null;
        this._bulletNode = null;
        this._bulletMap = null;
    },

    /**
     * 添加子弹
     * @param bullet
     * @param seatId
     * @param serverIndex
     */
    addBullet: function (bullet,seatId,serverIndex) {
        //添加子弹        
        this._bulletNode.addChild(bullet, 0);
        this._bulletMap[seatId].put(serverIndex, bullet);
    },

    removeBullet: function (seatId, serverIndex) {
        cc.log("移除子弹，座位id = " + seatId + ", index = " + serverIndex);
        if(this._bulletMap[seatId]) {
            var bullet = this._bulletMap[seatId].get(serverIndex);
            if (bullet) {
                bullet.doDied();
                //this._bulletMap[seatId].remove(serverIndex);
            }
        }
    },

    removeBulletFromMap: function (seatId, serverIndex) {
        this._bulletMap[seatId].remove(serverIndex);
    },

    removeAllBullet: function (seatId) {
        var len = this._bulletMap[seatId].size();
        for (var i = 0; i < len; ++i) {
            var bullet = this._bulletMap[seatId]._elements[i].value;
            if (bullet && bullet.getBulletState() != Bullet.State.OPEN) {                
                bullet.removeFromParent(true);
            }
        }
        this._bulletMap[seatId].clear();
    },

    bulletIsEmpty: function (seatId) {
        return this._bulletMap[seatId].isEmpty();
    },
    
    getBulletNode:function () {
        return this._bulletNode;
    }
});

/**
 * 子弹理器实例
 * @type {undefined}
 * @private
 */
BulletMgr._sInstance = undefined;
BulletMgr.getInstance = function () {
    if (!BulletMgr._sInstance) {
        BulletMgr._sInstance = new BulletMgr();
    }
    return BulletMgr._sInstance;
};

/**
 * 移除实例
 */
BulletMgr.purgeRelease = function () {
    if (BulletMgr._sInstance) {
        BulletMgr._sInstance.onClear();
        BulletMgr._sInstance = null;
    }
};