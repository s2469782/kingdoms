var DataSaveMgr = cc.Class.extend({

    _idLogin: null,

    init: function () {
        var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
        if(localPlayerModel) {
            this._idLogin = localPlayerModel.getIdLogin();
            var ls = cc.sys.localStorage;
            ls.setItem(this._idLogin, "{}");
        }
    },

    setNativeData: function (key,value) {
        var ls = cc.sys.localStorage;
        var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
        if(!ls.getItem(key) && localPlayerModel) {
            this._idLogin = localPlayerModel.getIdLogin();
            //获取集合
            var jsonData = ls.getItem(this._idLogin);
            if(!jsonData) {
                jsonData = "{}";
            }
            var nativePlayerData = JSON.parse(jsonData);
            if(nativePlayerData) {
                //在集合中添加相应的数据
                nativePlayerData[key] = value;
                ls.setItem(this._idLogin, JSON.stringify(nativePlayerData));
            }
            else {
                nativePlayerData = {};
                nativePlayerData[key] = value;
                ls.setItem(this._idLogin,JSON.stringify(nativePlayerData));
            }
            return;
        }
        ls.setItem(key,value);
    },

    getNativeData: function (key) {
        var ls = cc.sys.localStorage;
        if(!this._idLogin) {
            var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
            if(localPlayerModel) {
                this._idLogin = localPlayerModel.getIdLogin();
            }
        }
        if(!ls.getItem(key)) {
            //获取集合
            var nativePlayerData = JSON.parse(ls.getItem(this._idLogin));
            if(!nativePlayerData) {
                return null;
            }
            //在集合中添加相应的数据
            if(nativePlayerData[key] == undefined) {
                return null;
            }
            return nativePlayerData[key];
        }
        return ls.getItem(key);
    }
});

/**
 * 生成实例
 * @type {undefined}
 * @private
 */
DataSaveMgr._sInstance = undefined;
DataSaveMgr.getInstance = function () {
    if (!DataSaveMgr._sInstance) {
        DataSaveMgr._sInstance = new DataSaveMgr();
        DataSaveMgr._sInstance.init();
    }
    return DataSaveMgr._sInstance;
};

/**
 * 移除实例
 */
DataSaveMgr.purgeRelease = function () {
    if (DataSaveMgr._sInstance) {
        DataSaveMgr._sInstance.onClear();
        DataSaveMgr._sInstance = null;
    }
};