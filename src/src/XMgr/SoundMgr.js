var SoundMgr = cc.Class.extend({
    _audioEngine: null,
    _curPriority: 0,
    _timeOutFunc: null,
    _soundVolume: null,
    _musicVolume: null,
    _firstLoad: true,

    initData: function () {
        DataMgr.readSoundData(Res.LOGIN.SoundDataJson);
        this._audioEngine = cc.audioEngine;

        var sound = cf.SaveDataMgr.getNativeData("sound");
        var music = cf.SaveDataMgr.getNativeData("music");
        if (sound != null && music != null) {
            this._soundVolume = sound;
            this._musicVolume = music;

            this._audioEngine.setEffectsVolume(sound / 100);
            this._audioEngine.setMusicVolume(music / 100);
        } else {
            this._soundVolume = 50;
            this._musicVolume = 50;

            this._audioEngine.setEffectsVolume(0.5);
            this._audioEngine.setMusicVolume(0.5);

            cf.SaveDataMgr.setNativeData("sound",50);
           cf.SaveDataMgr.setNativeData("music",50);
        }
        this._timeOutFunc = null;
        this._firstLoad = true;
    },

    onClear: function () {
        
        if (this._timeOutFunc != null) {
            clearTimeout(this._timeOutFunc);
            this._timeOutFunc = null;
        }
        this._curPriority = 0;
    },

    playEffect: function (id, loop, delay) {
        if (loop === undefined)
            loop = false;
        if (delay === undefined)
            delay = 0;
        var self = this;

        //逻辑判断
        //7开炮，15掉金币
        /*if ((id == 7 || id == 17) && cf.isPlayGameVoice) {
         return;
         }*/
        if (delay != 0) {
            if (cf.nGameVoiceCount > 4)
                return;
        }

        var effect = cf.Data.SOUND_DATA[id];
        if (effect.priority < self._curPriority)
            return;
        self._curPriority = effect.priority;

        if (!cc.sys.isNative) {
            if (cc.loader.getRes(effect.path)) {
                self._audioEngine.playEffect(effect.path, loop);
            } else {
                cc.loader.load(effect.path, function () {
                    self._audioEngine.playEffect(effect.path, loop);
                });
            }
        } else {
            self._audioEngine.playEffect(effect.path, loop);
        }

        if (delay != 0) {
            cf.nGameVoiceCount++;
            //一个延迟机制，有高优先级的进入则清除当前延迟，重新计时
            if (self._timeOutFunc != null) {
                clearTimeout(self._timeOutFunc);
            }
            self._timeOutFunc = setTimeout(function () {
                self._curPriority = 0;
                cf.nGameVoiceCount = 0;
                self._timeOutFunc = null;
            }, delay * 1000);

        } else {
            this._curPriority = 0;
        }
    },

    stopEffect: function (id) {

    },

    setEffectVolume: function (volume) {
        this._soundVolume = volume;
        this._audioEngine.setEffectsVolume(volume/100);
    },
    
    getEffectVolume:function () {
        return this._soundVolume;
    },

    playMusic: function (id, loop) {
        var self = this;
        var effect = cf.Data.SOUND_DATA[id];
        if (cc.loader.getRes(effect.path)) {
            self._audioEngine.playMusic(effect.path, loop);
        } else {
            self._audioEngine.playMusic(effect.path, loop);
        }
    },

    stopMusic: function () {
        this._audioEngine.stopMusic();
    },

    setMusicVolume: function (volume) {
        this._musicVolume = volume;
        this._audioEngine.setMusicVolume(volume/100);
    },
    
    getMusicVolume:function () {
        return this._musicVolume;
    },

    isMusicPlay: function () {
        return this._audioEngine.isMusicPlaying();
    },

    stopAllEffects: function () {
        this._audioEngine.stopAllEffects();
    },

    unloadEffect: function (id) {
        var self = this;
        var effect = cf.Data.SOUND_DATA[id];
        if (!cc.sys.isNative) {
            if (cc.loader.getRes(effect.path)) {
                self._audioEngine.unloadEffect(effect.path);
            } else {
                cc.loader.load(effect.path, function () {
                    self._audioEngine.unloadEffect(effect.path);
                });
            }
        } else {
            self._audioEngine.unloadEffect(effect.path);
        }
    },

    preloadEffect: function () {
        if (cc.sys.isNative) {
            var self = this;
            if(self._firstLoad){
               self._firstLoad = false;
                var soundData = cf.Data.SOUND_DATA;
                for (var i in soundData) {
                    var info = soundData[i];
                    if (info.preload == 1) {
                        self._audioEngine.preloadEffect(info.path);
                    }
                }
            }
        }
    }
});

SoundMgr.PRIORITY = {
    LOW: 0,
    MIDDLE: 1,
    HIGH: 2
};

/**
 * 特效管理器实例
 * @type {undefined}
 * @private
 */
SoundMgr._sInstance = undefined;
SoundMgr.getInstance = function () {
    if (!SoundMgr._sInstance) {
        SoundMgr._sInstance = new SoundMgr();
    }
    return SoundMgr._sInstance;
};

/**
 * 移除实例
 */
SoundMgr.purgeRelease = function () {
    if (SoundMgr._sInstance) {
        SoundMgr._sInstance.onClear();
        SoundMgr._sInstance = null;
    }
};
