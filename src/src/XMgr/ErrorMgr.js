var ErrorMgr = cc.Class.extend({

    _target: null,
    _nodeError: null,

    setTarget: function (target) {
        this._target = target;

        this._nodeError = new cc.Node();
        this._nodeError.setPosition(0,0);
        this._target.addChild(this._nodeError,1000);
    },

    parseGameError: function (msg) {
        //解析错误消息
        var resMsg = $root.SC_ErrorMsg.decode(msg);
        var error = resMsg.nError;
        cc.error(error);
       
        var errorDlg = null;
        switch (resMsg.nError) {
            case MessageCode.ErrorCode_CostEnough:
                cf.UITools.showHintToast("货币不足");
                break;
            case MessageCode.ErrorCode_NoGold://金币不足
                cf.UITools.showHintToast(cf.Language.getText("text_1069"));
                break;
            case MessageCode.ErrorCode_NoLottery:
                cf.UITools.showHintToast(cf.Language.getText("text_1450"));
                break;
            case MessageCode.ErrorCode_NoDiamond://钻石不足
                break;
            case MessageCode.ErrorCode_LevelLess://玩家等级不足
                break;
            case MessageCode.ErrorCode_FortLess://玩家炮等级不足
                errorDlg = UIDialog.createError(error, function () {
                    //如果不在大厅则返回大厅
                    if (cf.SceneMgr.getCurSceneFlag() != cf.SceneFlag.LOBBY) {
                        cf.SceneMgr.runScene(cf.SceneFlag.LOBBY, null);
                    }
                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_AlreadyReceive: // 已经领取，不能重复领取
                cf.UITools.showHintToast(cf.Language.getText("text_1070"));
                break;
            case MessageCode.ErrorCode_NoJackpotDraw:// 个人奖金不足
                cf.UITools.showHintToast(cf.Language.getText("text_1071"));
                break;
            case MessageCode.ErrorCode_NoBonusFish: //没打够彩金鱼
                cf.UITools.showHintToast(cf.Language.getText("text_1072"));
                break;
            case MessageCode.ErrorCode_WaitJoin://已经在游戏中，请等待离开
                errorDlg = UIDialog.createError(error, function () {
                    //如果不在大厅则返回大厅
                    if (cf.SceneMgr.getCurSceneFlag() != cf.SceneFlag.LOBBY) {
                        cf.SceneMgr.runScene(cf.SceneFlag.LOBBY, null);
                    }
                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_PasswordError:
                if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY) {
                    cf.UITools.showHintToast("旧密码错误");
                } else {
                    //断开连接
                    cf.NetMgr.closeSocket();
                    errorDlg = UIDialog.createError(error, function () {
                        //重置保存密码状态
                        cf.SaveDataMgr.setNativeData("game_checkState", 0);
                        //直接返回登录界面
                        cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                        SDKHelper.logout();
                    }, this._nodeError);
                }
                break;
            case MessageCode.ErrorCode_AccountError:
            case MessageCode.ErrorCode_Blockade:
            case MessageCode.ErrorCode_SafeCodeError:
            case MessageCode.ErrorCode_NoFreeGS:
            case MessageCode.ErrorCode_RoomError:
            case MessageCode.ErrorCode_RemoteLogin:
            case MessageCode.ErrorCode_LackData:
            case MessageCode.ErrorCode_NoPlayer:
            case MessageCode.ErrorCode_ReSignIn:
            case MessageCode.ErrorCode_RoomID:
            case MessageCode.ErrorCode_CheckError:
            case MessageCode.ErrorCode_LackIdentity:
            case MessageCode.ErrorCode_AppIDError:
            case MessageCode.ErrorCode_ScriptError:
            case MessageCode.ErrorCode_CodeError:
            case MessageCode.ErrorCode_UidError:
            case MessageCode.ErrorCode_TokenError:
            case MessageCode.ErrorCode_LinkTimeout:
            case MessageCode.ErrorCode_ReturnFail:
                //断开连接
                cf.NetMgr.closeSocket();
                errorDlg = UIDialog.createError(error, function () {
                    //直接返回登录界面
                    cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                    SDKHelper.logout();
                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_NoTime:
                cf.UITools.showHintToast("在线奖励的时间不满足条件");
                break;
            case MessageCode.ErrorCode_NoVIP:
                errorDlg = UIDialog.create("error_" + error, "text_1125", "text_1098", false, function (type) {
                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_SkillNoTime:
                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.ERROR_LOCK_NOTIME);
                break;
            case MessageCode.ErrorCode_NoFish:
                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.ERROR_LOCK_NOFISH);
                break;
            case MessageCode.ErrorCode_NotFindAct:     // 没有找到活动
                cf.UITools.showHintToast(cf.Language.getText("error_741"));
                break;
            case MessageCode.ErrorCode_NicknameRepeat:      //< 昵称错误
                cf.UITools.showHintToast(cf.Language.getText("error_740"));
                break;
            case MessageCode.ErrorCode_LimitChar: //< 限制字符
                cf.UITools.showHintToast(cf.Language.getText("error_738"));
                break;
            case MessageCode.ErrorCode_NicknameLong:    //< 昵称太长
                cf.UITools.showHintToast(cf.Language.getText("error_739"));
                break;
            case MessageCode.ErrorCode_NoPay:          // 订单未支付
                if (SDKHelper._orderId) {
                    var isPolling = SDKHelper.pollingOrder();
                    if (!isPolling) {
                        errorDlg = UIDialog.createError(error, function () {
                            SDKHelper.clearPayState();
                        }, this._nodeError);
                    }
                } else {
                    errorDlg = UIDialog.createError(error, function () {
                        SDKHelper.clearPayState();
                    }, this._nodeError);
                }
                break;
            case MessageCode.ErrorCode_NoOrder:        // 订单不存在
            case MessageCode.ErrorCode_NoPayCode:      // 商品不存在
            case MessageCode.ErrorCode_NoCardTime:     // 月卡过期
            case MessageCode.ErrorCode_RepeatReceive:  // 重复领取
                errorDlg = UIDialog.createError(error, function () {
                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_TreasureHuntOver: //进行寻宝请求时已经过期
                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.ERROR_TREASUREHUNT_OUTOFTIME);
                break;
            case MessageCode.ErrorCode_FishFull:
                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.ERROR_FISHNUM_OUTOFLIMIT);
                break;
            case MessageCode.ErrorCode_NoCondition:
                if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY) {
                    errorDlg = UIDialog.createError(error, function () {
                    }, this._nodeError);
                }
                else if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.GAME) {
                    cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.ERROR_CARDNUM_NOTENOUGH);
                }
                break;
            case MessageCode.ErrorCode_NoBullet:
                //关闭自动开炮
                var seatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
                var player = cf.RoomMgr.getRoomPlayer(seatId);
                if (player.getAutoFireState()) {
                    player.setBAutoFireState(false);
                    cf.CanContinueAutoFire = false;
                }

                var jsonInfo = cf.SaveDataMgr.getNativeData("GrandPrixRank");

                if (jsonInfo) {
                    //将字符串转化为json
                    var jsonData = JSON.parse(jsonInfo);
                    if (jsonData.nIntegralNum > jsonData.nTodayIntegral) {

                        jsonData.nTodayIntegral = jsonData.nIntegralNum;
                        //存入本地
                        cf.SaveDataMgr.setNativeData("GrandPrixRank", JSON.stringify(jsonData));
                    }
                }

                //子弹数量不足error
                errorDlg = UIDialog.createError(error, function () {
                    cf.PlayerMgr.getLocalPlayer().leaveRoom({flag: cf.SceneFlag.LOBBY});
                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_Version://版本与服务器不匹配
                //断开连接
                cf.NetMgr.closeSocket();
                errorDlg = UIDialog.create("error_" + error, "text_1098", null, false, function (type) {
                    if (type == UIDialog.Btn.LEFT) {
                        //忽略
                    } else {
                        //下载链接
                        if (cc.sys.os === cc.sys.OS_ANDROID) {
                            //cf.JsbHelper.openURL(GameConfig.download_url);
                        } else if (cc.sys.os === cc.sys.OS_IOS) {
                            //cf.JsbHelper.openURL(GameConfig.download_url);
                        }
                    }
                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_NoLuckDrawNum://抽奖次数不足
            case MessageCode.ErrorCode_NoAccPoints://积分不足
                errorDlg = UIDialog.createError(error, function () {
                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_NoDoleNum://救济金次数不足
                cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.DOLEHASGETALLTIMES);
                break;
            case MessageCode.ErrorCode_NoChat:
                cf.UITools.showHintToast("您已被禁言");
                break;
            case MessageCode.ErrorCode_Inv:
                cf.UITools.showHintToast("邀请码错误", 2);
                break;
            case MessageCode.ErrorCode_NoAuthorityEmail:
                cf.UITools.showHintToast("您无权限向此用户发送邮件", 2);
                break;
            case MessageCode.ErrorCode_NoSell:
                errorDlg = UIDialog.createCustom("您拥有总金币数大于10亿，暂时无法出售", "确定", null, false, function (type) {

                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_ChipsIsNotEnough:
                errorDlg = UIDialog.createCustom("抱歉您的筹码不足", "确定", null, false, function (type) {

                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_CanNotBets:
                errorDlg = UIDialog.createCustom("下注已经达到上限,请选择其他类别", "确定", null, false, function (type) {

                }, this._nodeError);
                break;
            case MessageCode.ErrorCode_WaitforLeave://等待离开
                //断线重连时返回的错误消息，这里需要延迟3秒等待服务器保存数据完毕，才继续进行登录操作
                setTimeout(function () {
                    SDKHelper.reConnectLogin();
                },3000);
                break;
            case 0:
                break;
            default:
                errorDlg = UIDialog.createError(error, function () {
                }, this._nodeError);
                break;
        }

        if (errorDlg) {
            this._nodeError.removeAllChildren(true);
            this._nodeError.addChild(errorDlg);
        }
    },

    parseLocalError: function (msg) {
        var flag = msg;
        if (typeof msg == "object") {
            flag = msg.flag;
        }
        //解析错误消息
        cc.error(flag);

        var errorDlg = null;
        switch (flag) {
            case NotificationScene.Error.ERROR_NET_CONNECT:
                cf.NetMgr.closeSocket();
                errorDlg = UIDialog.createCustom(cf.Language.getText("error_" + flag),
                    cf.Language.getText("text_1098"),
                    null, false,
                    function () {
                        //直接返回登录界面
                        if (cf.SceneMgr.getCurSceneFlag() != cf.SceneFlag.LOGIN) {
                            cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                        }
                        SDKHelper.logout();
                    }, this._nodeError);
                break;
            case NotificationScene.Error.ERROR_SOCKET_CLOSE:
                if (cf.NetMgr.isCloseSocket()) {
                    //主动断开连接，不处理
                } else {
                    
                    
                    errorDlg = UIDialog.createCustom(cf.Language.getText("error_" + flag),
                        cf.Language.getText("text_1098"),
                        null, false,
                        function () {
                            //直接返回登录界面
                            if (cf.SceneMgr.getCurSceneFlag() != cf.SceneFlag.LOGIN) {
                                cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                            }
                            SDKHelper.logout();
                        }, this._nodeError);
                }
                break;
            case NotificationScene.Error.ERROR_SOCKET_TIMEOUT:
                errorDlg = UIDialog.createError(flag, function () {
                    //直接返回登录界面
                    var _sceneFlag = cf.SceneMgr.getCurSceneFlag();
                    if (_sceneFlag == cf.SceneFlag.GAME_LOADING || _sceneFlag == cf.SceneFlag.GAME) {
                        cf.SceneMgr.runScene(cf.SceneFlag.LOBBY, null);
                    } else {
                        cf.NetMgr.closeSocket();
                        if (_sceneFlag != cf.SceneFlag.LOGIN) {
                            cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                        }
                        SDKHelper.logout();
                    }
                }, this._nodeError);
                break;
            case NotificationScene.Error.ERROR_REMOVE_USER:
                cf.NetMgr.closeSocket();
                errorDlg = UIDialog.createError(flag, function () {
                    //直接返回登录界面
                    cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
                    SDKHelper.logout();
                }, this._nodeError);
                break;
            case NotificationScene.Error.PAY_SUCCESS:
                errorDlg = UIDialog.create("text_1220", "text_1222", "text_1221", false, function (type) {
                    if (type == UIDialog.Btn.RIGHT) {
                        SDKHelper.orderQuery(msg.orderId, true);
                    }
                }, this._nodeError);
                break;
        }

        if (errorDlg) {
            this._nodeError.removeAllChildren(true);
            this._nodeError.addChild(errorDlg);
        }
    },

    onClear: function () {
    }
});


ErrorMgr._sInstance = undefined;
ErrorMgr.getInstance = function () {
    if (!ErrorMgr._sInstance) {
        ErrorMgr._sInstance = new ErrorMgr();
    }
    return ErrorMgr._sInstance;
};

ErrorMgr.purgeRelease = function () {
    if (ErrorMgr._sInstance) {
        ErrorMgr._sInstance.onClear();
        ErrorMgr._sInstance = null;
    }
};