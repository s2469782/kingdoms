var ActionMgr = ActionMgr || {};

//精灵帧缓存实例
var spriteFrameCache = cc.spriteFrameCache;
//
var ANIMATION_FRAME_PER_SECOND = 12.0;

/**
 * 加载plist
 * @param resPlist
 * @param resTex
 */
ActionMgr.load = function (resPlist, resTex) {
   
    if (spriteFrameCache) {
        spriteFrameCache.addSpriteFrames(resPlist, resTex);
    }
};

/**
 * 获取精灵帧
 * @param key 精灵图片名称
 * @returns 返回SpriteFrame对象
 */
ActionMgr.getFrame = function (key) {
    if (spriteFrameCache) {
        return spriteFrameCache.getSpriteFrame(key);
    }
    return null;
};

/**
 * 创建动画动作：这里帧动作从1开始
 * @param frameName 动画名称
 * @param frameNum 动画帧数
 * @param frameSpeed 动画速度
 */
ActionMgr.createAction = function (frameName, frameNum, frameSpeed) {
    //检测是否存在这个动画
    var ret = cc.animationCache.getAnimation(frameName);
    if (ret) {
        return false;
    }

    var animFrames = [];
    var frame, animFrame;
    var str = "";
    for (var i = 1; i <= frameNum; i++) {
        str = frameName + (i < 10 ? ("_00" + i) : ("_0" + i)) + ".png";
        
        frame = spriteFrameCache.getSpriteFrame(str);
        if(frame==null){
           
            cc.error("创建动作出错 frameName = "+frameName);
            return false;
        }
        animFrame = new cc.AnimationFrame(frame, 1);
        animFrames.push(animFrame);
    }

    var animation = new cc.Animation(animFrames, frameSpeed * 1.0 / ANIMATION_FRAME_PER_SECOND);

    // Add an animation to the Cache
   
    cc.animationCache.addAnimation(animation, frameName);
    return true;
};

/**
 * @param spineJson spine的配置信息
 * @param spineAtlas spine的图集信息
 * @param scale 缩放值
 */
ActionMgr.createSpine = function( spineJson, spineAtlas,scale ){
    if(scale == undefined){
        scale = 1.0;
    }
    //这里请使用createWithJsonFile，因为native版本读取问题
    return (sp.SkeletonAnimation.createWithJsonFile(spineJson,spineAtlas,scale));
};

/**.
 * @param spine         {sp.skeleton}     spine执行的动画名称
 * @param animateName   {string}          spine执行的动画名称
 * @param state         {boolean}         是否循环执行动画
 */
ActionMgr.playSpineAnimate = function( spine, trackIndex, animateName, state ){
    spine.setAnimation(trackIndex, animateName, state);
};

/**
 * 获取动画
 * @param aniName 动画名称
 * @returns {*}
 */
ActionMgr.getAnimate = function (aniName) {

    var animation = cc.animationCache.getAnimation(aniName);
    if (animation) {
        return cc.animate(animation);
    }
    return null;
};

/**
 * 创建回弹动作
 * @param scale 回弹最大缩放值:1.2
 * @param time1 缩放时间:0.25
 * @param time2 缩放时间:0.2
 * @param time3 缩放时间:0.1
 * @param ratio 缩放时间比率
 * @returns {cc.Sequence}
 */
ActionMgr.createBounce = function (scale,time1,time2,time3,ratio) {
    return cc.sequence(
        cc.scaleTo(time1, scale),
        cc.scaleTo(time2, 0.8+ratio),
        cc.scaleTo(time3, 1+ratio)
    );
};

/**
 * 创建延迟回弹动作
 * @param delay 延迟时间
 * @param scale 回弹最大缩放值:1.2
 * @param time1 缩放时间:0.25
 * @param time2 缩放时间:0.2
 * @param time3 缩放时间:0.1
 * @param ratio 缩放时间比率
 * @returns {cc.Sequence}
 */
ActionMgr.createDelayBounce = function (delay,scale,time1,time2,time3,ratio) {
    return cc.sequence(
        cc.delayTime(delay),
        cc.scaleTo(time1, scale),
        cc.scaleTo(time2, 0.8+ratio),
        cc.scaleTo(time3, 1+ratio)
    );
};
/**
 * 创建圆形动作
 * @param time
 * @param circleX
 * @param circleY
 * @returns {cc.Sequence}
 */
ActionMgr.createCircle = function(time,circleX,circleY){
    // 贝塞尔移动
    var bezier01 = [cc.p(0, circleY), cc.p(-circleX, circleY), cc.p(-circleX, 0)];
    var bezier02 = [cc.p(0, -circleY), cc.p(circleX, -circleY), cc.p(circleX, 0)];
    var action01 = cc.bezierBy(time,bezier01);
    var action02 = cc.bezierBy(time,bezier02);

    return cc.sequence(action01,action02);
};

/**
 * 创建矩形路径动作
 * @param timeX x方向时间
 * @param timeY y方向时间
 * @param circleX 宽
 * @param circleY 高
 * @returns {cc.Sequence}
 */
ActionMgr.createSquare = function(timeX,timeY,circleX,circleY){

    var action01 = cc.moveBy(timeX,cc.p(-circleX,0));
    var action02 = cc.moveBy(timeY,cc.p(0,-circleY));
    var action03 = cc.moveBy(timeX,cc.p(circleX,0));
    var action04 = cc.moveBy(timeY,cc.p(0,circleY));

    return cc.sequence(action01,action02,action03,action04);
};

/**
 * 创建圆角矩形路径动作
 * @param timeX x方向运动时间
 * @param timeY y方向运动时间
 * @param controlX 圆角宽度
 * @param controlY 圆角高度
 * @param width 矩形宽
 * @returns {cc.Sequence}
 */
ActionMgr.createRoundRect = function (timeX,timeY,controlX,controlY,width) {
    var action01 = cc.bezierBy(timeY,[cc.p(-controlX, 0), cc.p(-controlX, controlY), cc.p(0, controlY)]);
    var action02 = cc.moveBy(timeX,cc.p(width,0));
    var action03 = cc.bezierBy(timeY,[cc.p(controlX, 0), cc.p(controlX, -controlY), cc.p(0, -controlY)]);
    var action04 = cc.moveBy(timeX,cc.p(-width,0));

    return cc.sequence(action01,action02,action03,action04);
};

/**
 * 大厅界面中ui出现的动态动作  temporary
 */
ActionMgr.createUiShowAction = function (delay) {
    //弹跳
    var delay = cc.delayTime(delay);
    var action1 = cc.scaleTo(0.2,1.02);
    var action2 = cc.scaleTo(0.2,1);
    var action3 = cc.scaleTo(0.2,0.98);
    var action4 = cc.scaleTo(0.2,1);
    var action5 = cc.scaleTo(0.2,1.02);
    var action6 = cc.scaleTo(0.2,1);
    var fadeIn = cc.fadeIn(0.1);
    var spawn = cc.spawn(cc.sequence(action1,action2,action3,action4,action5,action6),fadeIn);
    //上下缓动
    //var action1 = cc.moveTo(2,cc.p(posx + 5,30));
    //var action2 = cc.moveTo(2,cc.p(posx,20));
    //var action3 = cc.moveTo(2,cc.p(posx + 5,30));
    //var action4 = cc.moveTo(2,cc.p(posx,20));
    return cc.sequence(delay,spawn);
};