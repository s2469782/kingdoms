
var EffectMgr = cc.Class.extend({
    _sceneNode: null,       //< 场景节点
    _itemNode: null,        //< 道具节点
    _labelNode: null,       //< label节点
    _lockEffect: null,      //< 锁定特效
    _iceEffect: null,       //< 冰冻特效
    _summonEffect: null,    //< 召唤特效
    _localPlayer: null,
    _cannonLevelUpEff: null, //< 炮倍升级特效
    _coinCount: 0,          //< 金币计数器
    _iceActionIsOn: false,
    _bonusPos: null,
    _diceAnimatePlay: false,
    _diceAnimatePlay2: false,
    _gameVoiceId: 0,            //< 游戏语音起始id
    _gameVoiceMaxId: 0,         //< 游戏语音最大id
    _blinkEffect: null,
    _skillNotice: null,
    _skillNoticeBack: null,
    _bossRateNum:[],
    _bossCount:[],
    
    _fishdata: null,
    
    _dragonJPRateNum: [], //<龍珠金幣跳動
    _dragonJPCount: [],
    _IsScroll: false,
    _labarNode: null,
    /**
     * 初始化
     * @param mainNode
     */
    init: function (mainNode) {
        this._sceneNode = mainNode;

        this._itemNode = new cc.Node();
        mainNode.addChild(this._itemNode, cf.Zoder.EFFECT + 1);

        this._labelNode = new cc.Node();
        mainNode.addChild(this._labelNode, cf.Zoder.EFFECT - 1);

        this._localPlayer = cf.PlayerMgr.getLocalPlayer();

        //初始化金币计数器
        this._coinCount = 0;
        this._bonusPos = cc.p(50, cc.winSize.height / 2 - 100);

        this._gameVoiceId = 0;
        this._gameVoiceMaxId = cf.Data.GAME_VOICE_DATA.length;
    },

    onClear: function () {
        
      
    },

    setBonusPos: function (x, y) {
        this._bonusPos.x = x;
        this._bonusPos.y = y;
    },

    /**
     * 播放特效
     * @param fishPos
     * @param fishModel
     * @param msg
     */
    play: function (fishPos, fishModel, msg) {
        
        if(msg.listFish.length> 1){
            if(this._fishdata != msg){
                this._fishdata = msg;
               
                if (fishModel.getType() != cf.FishType.BOSS) {
                    this.playFishShowGold( msg);
                }
                
                //彩金特效
                this.playShowBonus(fishModel, msg);
                //物品掉落
                this.playGetItem(fishPos, msg);
                //游戏语音
                this.playGameVoice(fishModel);
                
                //龙珠掉落
               // this.playDropDragonBall(fishPos, msg);
                if(cf.SkillfishBoolean == true){
                    var time = setTimeout(()=>{
                    
                        this.playDragonBonusGold(fishModel, msg);
                            
                        clearTimeout(time);
                    }, 1000);
                }
               
            }
        }else{
            
            if (fishModel.getType() != cf.FishType.BOSS) {
                this.playShowGold(fishPos, fishModel, msg);
            }
            //彩金特效
            this.playShowBonus(fishModel, msg);
            //物品掉落
            this.playGetItem(fishPos, msg);
            //游戏语音
            this.playGameVoice(fishModel);
            //龙珠掉落
            
            this.playDropDragonBall(fishModel._fishId, fishPos, msg);
            

        }
            //播放特效
        
            
        
        
        
    },

    playDragonBonusGold: function(fishModel, msg){
        var result = cf.PlayerMgr.getLocalPlayer().getDragonBonusString();
        var player = cf.PlayerMgr.getLocalPlayer();
        
        cf.SkillfishBoolean =false;

        if(player.getSeatID() == msg.nSeatID){
        
        var bonusNode = new cc.Node();
        bonusNode.setPosition(cc.winSize.width/2, cc.winSize.height/2);

        //旋转节点
        var rotateNode = new cc.Node();
        bonusNode.addChild(rotateNode, 1);
        

        
        // //皇冠
        if(result == 0){
            //背景
            var bonusBk = new cc.Sprite("#bonus_002.png");
            bonusBk.setScale(0);
            bonusBk.setTag("bonusBk");
            bonusNode.addChild(bonusBk, 0);

            var bonusTitle = new cc.Sprite("#sunJian.png");
           // bonusTitle.setScale(0);
            bonusTitle.setPosition(18, 55);
            rotateNode.addChild(bonusTitle, 0);

            
            
        }else if(result ==1){
            //背景
            var bonusBk = new cc.Sprite("#bonus_003.png");
            bonusBk.setScale(0);
            bonusBk.setTag("bonusBk");
            bonusNode.addChild(bonusBk, 0);

            var bonusTitle = new cc.Sprite("#XiahouDun.png");
            //bonusTitle.setScale(0);
            bonusTitle.setPosition(0, 85);
            rotateNode.addChild(bonusTitle, 0);
            
        }else if(result == 2){
             //背景
             var bonusBk = new cc.Sprite("#bonus_004.png");
             bonusBk.setScale(0);
             bonusBk.setTag("bonusBk");
             bonusNode.addChild(bonusBk, 0);
 
             var bonusTitle = new cc.Sprite("#ZhaoYun.png");
             //bonusTitle.setScale(0);
             bonusTitle.setPosition(-22, 80);
             rotateNode.addChild(bonusTitle, 0);

        }
        else if( result == 3){
            //背景
            var bonusBk = new cc.Sprite("#bonus_005.png");
            bonusBk.setScale(0);
            bonusBk.setTag("bonusBk");
            bonusNode.addChild(bonusBk, 0);

            var bonusTitle = new cc.Sprite("#MaChao.png");
            //bonusTitle.setScale(0);
            bonusTitle.setPosition(27, 20);
            rotateNode.addChild(bonusTitle, 0);
            
        }else if( result == 4){
             //背景
             var bonusBk = new cc.Sprite("#bonus_006.png");
             bonusBk.setScale(0);
             bonusBk.setTag("bonusBk");
             bonusNode.addChild(bonusBk, 0);
 
             var bonusTitle = new cc.Sprite("#guan.png");
             //bonusTitle.setScale(0);
             bonusTitle.setPosition(0, 10);
             rotateNode.addChild(bonusTitle, 0);

        }
        else if(result == 5){
            //背景
            var bonusBk = new cc.Sprite("#bonus_007.png");
            bonusBk.setScale(0);
            bonusBk.setTag("bonusBk");
            bonusNode.addChild(bonusBk, 0);

            var bonusTitle = new cc.Sprite("#shu.png");
           // bonusTitle.setScale(0);
            bonusTitle.setPosition(0, 90);
            rotateNode.addChild(bonusTitle, 0);
            
        }else{
           
            
            //背景
            var bonusBk = new cc.Sprite("#bonus_007.png");
            bonusBk.setScale(0);
            bonusBk.setTag("bonusBk");
            bonusNode.addChild(bonusBk, 0);

            
        }

        
        //彩带
        var bonusBar = new cc.Sprite("#bonus_result.png");
        bonusBar.setScale(1);
        bonusBar.setPosition(0, -130);
        bonusNode.addChild(bonusBar, 1);

        //bonus字
        var bonusFish = new cc.Sprite("#ui_dragon_text_0" + result + ".png");
        bonusFish.setScale(0);
        bonusFish.setPosition(0, -60);
        bonusNode.addChild(bonusFish, 2);


        
        // //倍数
        // var labelRate = new cc.LabelBMFont(1000 + "倍", Res.LOBBY.font_resource_083);
        // labelRate.setPosition(bonusTitle.width / 2 - 20, bonusTitle.height / 2 + 20);
        // labelRate.setScale(0.85);
        // labelRate.setRotation(-45);
        // bonusTitle.addChild(labelRate);

        //执行动作
        rotateNode.runAction(cc.repeatForever(cc.sequence(cc.rotateTo(0.5, -5), cc.rotateTo(0.5, 5))));
        bonusBk.runAction(cc.repeatForever(cc.rotateBy(1, 360)));
        bonusBk.runAction(ActionMgr.createDelayBounce(0.1, 1.2, 0.25, 0.2, 0.1, 0));
        bonusFish.runAction(ActionMgr.createDelayBounce(0.1, 1.2 + 0.2, 0.25, 0.2, 0.1, 0.2));

        
        var _itemLen = msg.itemReward.length;
        if (_itemLen > 0) {
            for (var j = 0; j < _itemLen; ++j) {
                var item = msg.itemReward[j];
                if (item) {
                    var _itemData = cf.Data.ITEM_DATA[item.nItemID];

                    var awardNum = new ccui.Text(_itemData.itemName + "x" + item.nNum, cf.Language.FontName, 30);
                    awardNum.setPosition(bonusBar.width / 2, bonusBar.height / 2 + 8 + j * 35);
                    awardNum.setTextColor(cc.color.YELLOW);
                    awardNum.enableOutline(cc.color(165, 42, 42), 2);
                    bonusBar.addChild(awardNum);
                }
            }

            bonusBar.runAction(ActionMgr.createBounce(1.2, 0.25, 0.2, 0.1, 0.2));
        } else {
            //得分
            var fortIndex = msg.nBulletLevel;
           var fortNum = cf.Data.CANNON_LEVEL_DATA[fortIndex].cannonlevel;
            var label = new cc.LabelBMFont(msg.nScore, Res.LOBBY.font_resource_090);
            label.setPosition(bonusBar.width / 2, bonusBar.height / 2 - 10);
            label.setScale(0.9);
            bonusBar.addChild(label);

            //数字动作
            for (var i = 0; i < label.getChildrenCount(); ++i) {
                var child = label.getChildByTag(i);
                if (child) {
                    child.setScale(0);
                    child.runAction(cc.sequence(ActionMgr.createDelayBounce(0.1 * i, 1.7, 0.25, 0.2, 0.1, 0)));
                }
            }
            
                bonusTitle.runAction(cc.sequence(
                    // ActionMgr.createDelayBounce(0.2, 1, 0.25, 0.2, 0.1, 0.5),
                    cc.delayTime(0.2),
                    cc.scaleTo(0.25, 1),
                    cc.scaleTo(0.2, 1.2),
                    cc.scaleTo(0.3, 1)
                
                ));
            
            bonusBar.runAction(ActionMgr.createBounce(1.2, 0.25, 0.2, 0.1, 0));
        }

        //父节点动作
        bonusNode.runAction(cc.sequence(
            cc.delayTime(2),
            //cc.spawn(cc.moveTo(0.5, player._curPos), cc.scaleTo(0.4, 0.3)),
            cc.callFunc(function () {
                this.removeFromParent(true);
              
            }, bonusNode)
        ));

        this._sceneNode.addChild(bonusNode, cf.Zoder.EFFECT);
        
        }
    
    },
    /**
     * @param 播放複數捕鱼金币特效
     */    

    playFishShowGold: function (msg) {

        var self = this;
        var width = cc.winSize.width;
        var height = cc.winSize.height;
        var _posX = [];
        var _posY = [];
        
        for(var k = 0; k < msg.listFish.length; k++){
            var num = Math.floor(Math.random()*msg.listFish.length +1);
            var random = Math.floor(Math.random()*2);
            if(random == 0){
                _posX.push(width/2-(num* 70));
                _posY.push(height/2 - (num *30));
            }else{
                _posX.push(width/2+(num*70));
                _posY.push(height/2+(num*30));
            }

            var localSeatId = this._localPlayer.getSeatID();
            if (localSeatId >= 2) {
                _posX[a] = width - _posX[a];
                _posY[a] = height - _posY[a];
            }
            
        }
        //金币label
        var font = Res.LOBBY.font_resource_089;
        var gold = "effect_silver";
        var scale = 0.9;
       
        if (localSeatId == msg.nSeatID) {
            //本地玩家
            font = Res.LOBBY.font_resource_090;
            gold = "effect_gold";
            scale = 1;
           // action = ActionMgr.createBounce(1.1, 0.25, 0.1, 0.1, -0.1);
        } else {
            //其他玩家
           // action = ActionMgr.createBounce(1, 0.25, 0.1, 0.1, -0.2);
        }
        
        for(var a =0; a < msg.listFish.length; a++){
            
            var goldLabel = new cc.LabelBMFont(msg.listFish[a].nFishScore, font);
            goldLabel.setPosition(_posX[a], _posY[a]);
            goldLabel.setScale(0);
            goldLabel.runAction(cc.sequence(
                //action,
            //cc.moveBy(0.5,0,65),
            //cc.delayTime(0.5),
                cc.scaleTo(0.3, 1, 1),
                cc.delayTime(1),
                cc.fadeOut(0.5),
                
                cc.callFunc(function () {
                    goldLabel.removeFromParent(true);
                })
            ));
         self._labelNode.addChild(goldLabel, 0);
            
         //金币喷泉
         var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
         if (player) {
             //方向
             var _dir = 1, _angle = 0;
             if (msg.nSeatID >= 2) {
                 _dir = -1;
                 _angle = 180;
             }
         
             var offsetX = 0;
            
                     offsetX = offsetX+40;
                     var spineJson, spineAtlas;
                     spineJson = Res.GAME.money_Json;
                     spineAtlas = Res.GAME.money_Atlas;
 
                     
 
 
                     if(gold == "effect_gold"){
                        // spBoomCoin.setAnimation(0, "gold", false);
                        // spBoomCoin._animateName = "gold";
 
                     }else if(gold == "effect_silver"){
                       //  spBoomCoin.setAnimation(0, "silver", false);
                       //  spBoomCoin._animateName = "silver";
 
                     }
 
                     var total = msg.listFish.length;
                    for (var i = 0; i < total; ++i) {
                         var spBoomCoin1 = Coin.create("#" + gold + "_001.png");
                         spBoomCoin1.setTag("sp1");
                         var _scale = cf.randomRange(8, 13) / 10;
                         spBoomCoin1.setScale(_scale);
                         //计算角度
                         var angle = 360 / total * i;
                         spBoomCoin1.setRotation(_angle);
 
                         //随机半径
                         var radius = Math.floor(Math.random()* 5 +1) * _scale;
                        
                         if (i % 2 == 0) {
                             radius = cf.randomRange(10, radius * 0.65);
                         } else {
                             radius = cf.randomRange(radius * 0.75, radius);
                         }

                         //执行动作
                         spBoomCoin1.setPosition(_posX[a]+offsetX, _posY[a]);
                        // spBoomCoin1.setTime(player._goldPos, 500);
                         spBoomCoin1.doBoom(gold, player._goldPos, radius, angle, 0.8, 0.02 * i, function () {
                             player.doGoldAction();
                             self._coinCount--;
                         });
                         cf.BulletMgr.getBulletNode().addChild(spBoomCoin1, 100);
                         
 
                         //金币计数
                         self._coinCount++;
                     }
 
                  //   cf.BulletMgr.getBulletNode().addChild(spBoomCoin, 100);
                   

                    //  spBoomCoin.setCompleteListener(()=>{
                    //      spBoomCoin.runAction(cc.sequence(
                    //          cc.fadeOut(2),
                    //          cc.callFunc(function() {
                    //              cf.BulletMgr.getBulletNode().removeChildByTag("sp");
                    //              cf.BulletMgr.getBulletNode().removeChildByTag("sp1");
                                 
                    //          },spBoomCoin)
                    //      ));
                    //  });

        }
        
                            cf.RoomMgr.shakeRoom(5, 12, 0.03);
                         
                  
            }
        
        //金币掉落特效
        cf.SoundMgr.playEffect(17);
    },


    /**
     * 播放捕鱼金币特效
     * @param fishPos 鱼位置
     * @param fishModel 鱼信息
     * @param msg 捕鱼消息
     */
    playShowGold: function (fishPos, fishModel, msg) {
        var self = this;
       
        

        //金币label
        var font = Res.LOBBY.font_resource_089;
        var gold = "effect_silver";
        var scale = 0.9;
        var action = null;
        if (localSeatId == msg.nSeatID) {
            //计算坐标，因为鱼的坐标是跟随场景旋转的所以需要通过座位id转换坐标
        var _posX = fishPos.x, _posY = fishPos.y;
        var localSeatId = this._localPlayer.getSeatID();
        if (localSeatId >= 2) {
            _posX = cc.winSize.width - _posX;
           _posY = cc.winSize.height - _posY;
        }
        
            //本地玩家
            font = Res.LOBBY.font_resource_090;
            gold = "effect_gold";
            scale = 1;
            action = ActionMgr.createBounce(1.1, 0.25, 0.1, 0.1, -0.1);
        } else {
            //其他玩家
            action = ActionMgr.createBounce(1, 0.25, 0.1, 0.1, -0.2);
        }
        
        for(var a =0; a < msg.listFish.length; a++){
            var goldLabel = new cc.LabelBMFont(msg.listFish[a].nFishScore, font);
            goldLabel.setPosition(_posX, _posY + 60);
            goldLabel.setScale(0);
            goldLabel.runAction(cc.sequence(
                action,
                cc.delayTime(1),
            //cc.moveBy(0.5,0,65),
            //cc.delayTime(0.5),
                cc.fadeOut(0.5),
                cc.callFunc(function () {
                    goldLabel.removeFromParent(true);
                })
            ));
        this._labelNode.addChild(goldLabel, 0);
        }
        //h5版本金币数量大于阈值就不显示
        if (!cc.sys.isNative && this._coinCount > 80) {
            return;
        }
        //金币喷泉
        var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
        if (player) {
            //方向
            var _dir = 1, _angle = 0;
            if (msg.nSeatID >= 2) {
                _dir = -1;
                _angle = 180;
            }
            var type = Math.floor(fishModel._showGold[0]);
            var offsetX = 0;
            
            switch (type) {
                case 1://横排
                    for (var i = 0; i < fishModel._showGold[1]; ++i) {
                        for (var j = 0; j < fishModel._showGold[2]; ++j) {
                            var spCoin = Coin.create("#" + gold + "_001.png");
                            spCoin.setScale(scale);
                            spCoin.setRotation(_angle);

                            //计算偏移
                            offsetX = j * 40;
                            if (player._goldPos.x > fishPos.x) {
                                offsetX *= -1;
                            }
                            //执行动作
                            spCoin.setPosition(fishPos.x + offsetX, fishPos.y + i * 50 * _dir);
                            spCoin.setTime(player._goldPos, 500);
                            spCoin.doJump(gold, j * 0.1, player._goldPos, 50 * _dir, 150 * _dir, function () {
                                player.doGoldAction();
                                self._coinCount--;
                            });
                           cf.BulletMgr.getBulletNode().addChild(spCoin, 100);
                            //金币计数
                            self._coinCount++;
                         }
                   }
                   
                    break;
                case 2://圆形
                    //金幣爆炸骨骼

                    var spineJson, spineAtlas;
                    spineJson = Res.GAME.money_Json;
                    spineAtlas = Res.GAME.money_Atlas;

                    var moneywaveJson, moneywaveAtlas;
                    moneywaveJson = Res.GAME.moneywave_Json;
                    moneywaveAtlas =  Res.GAME.moneywave_Atlas;

                    //var spBoomCoin =  ActionMgr.createSpine(spineJson,spineAtlas,1);

                    var wave = ActionMgr.createSpine(moneywaveJson, moneywaveAtlas, 1);
                    

                    var spBoomCoin = new cc.Sprite();
                    spBoomCoin.setTag("sp");
                    spBoomCoin.setScale(1.2);
                    spBoomCoin.runAction(cc.sequence(
                            ActionMgr.getAnimate("coin"),
                            cc.fadeOut(0.2),
                            cc.callFunc(function () {
                                this.removeFromParent(true);
                            }, spBoomCoin)
                        ));
                    spBoomCoin.setPosition(fishPos);
                    
                        

                    if(gold == "effect_gold"){
                        // spBoomCoin.setAnimation(0, "gold", false);
                        // spBoomCoin._animateName = "gold";

                    }else if(gold == "effect_silver"){
                        // spBoomCoin.setAnimation(0, "silver", false);
                        // spBoomCoin._animateName = "silver";

                    }

                    var total = fishModel._showGold[2];
                   for (var i = 0; i < total; ++i) {
                        var spBoomCoin1 = Coin.create("#" + gold + "_001.png");

                        var _scale = cf.randomRange(8, 13) / 10;
                        spBoomCoin1.setScale(_scale);
                        //计算角度
                        var angle = 360 / total * i;
                        spBoomCoin1.setRotation(_angle);

                        //随机半径
                        var radius = fishModel._showGold[1] * scale;
                       
                        if (i % 2 == 0) {
                            radius = cf.randomRange(10, radius * 0.65);
                        } else {
                            radius = cf.randomRange(radius * 0.75, radius);
                        }

                        //执行动作
                        spBoomCoin1.setPosition(fishPos);
                        spBoomCoin1.setTime(player._goldPos, 500);
                        spBoomCoin1.doBoom(gold, player._goldPos, radius, angle, 0.8, 0.02 * i, function () {
                            player.doGoldAction();
                            self._coinCount--;
                        });


                        cf.BulletMgr.getBulletNode().addChild(spBoomCoin1, 100);

                        //金币计数
                        self._coinCount++;
                    }
                    wave.setPosition(fishPos);
                    wave.setAnimation(0, "money", false);
                    wave.runAction(cc.sequence(
                        cc.delayTime(2),
                        cc.callFunc(()=>{
                            wave.removeFromParent();
                        })
                    ))
                    
                     cf.BulletMgr.getBulletNode().addChild(spBoomCoin, 100);
                     cf.BulletMgr.getBulletNode().addChild(wave, 100);

                    // spBoomCoin.setCompleteListener(()=>{
                    //     spBoomCoin.runAction(cc.sequence(
                    //         cc.fadeOut(2),
                    //         cc.callFunc(function () {
                    //             cf.BulletMgr.getBulletNode().removeChildByTag("sp");
                    //         }, spBoomCoin)
                    //     ));
                    // });

                    //爆炸特效
                    // var boomEffect = new cc.Sprite("#firework_001.png");
                    // //这里200是标准
                    // //var _effect_scale = (scale * (fishModel._showGold[1] / 200)) * 2;
                    // boomEffect.setScale(4);
                    // boomEffect.setPosition(fishPos);
                    // boomEffect.setOpacity(255);
                    // cf.BulletMgr.getBulletNode().addChild(boomEffect, 99);

                    // boomEffect.runAction(cc.sequence(
                    //     ActionMgr.getAnimate("firework"),
                    //     cc.fadeOut(0.2),
                    //     cc.callFunc(function () {
                    //         this.removeFromParent(true);
                    //     }, boomEffect)
                    // ));

                    switch (fishModel.getType()) {
                        case cf.FishType.COMMON:
                            //cf.SoundMgr.playEffect(13, false, 2.5);
                            //普通振动
                            cf.RoomMgr.shakeRoom(5, 12, 0.03);
                            break;
                        case cf.FishType.BONUS:
                        case cf.FishType.BOSS:
                            cf.RoomMgr.shakeRoom(5, 15, 0.05);
                            break;
                    }
                    //金币掉落特效
                   // cf.SoundMgr.playEffect(76);
                    break;
            }
        }
        //金币掉落特效
        cf.SoundMgr.playEffect(17);
        
    },

    /**
     * 播放奖金特效
     * @param fishModel
     * @param msg
     */
    playShowBonus: function (fishModel, msg) {

        var roomId = cf.RoomMgr.getRoomId();
        
        /**
         *数组类型数据
         * 0：位置[0-无位置，1-正上方，2-屏幕中心]
         * 1：文件名索引
         * 2：播放时间
         */
        var killData = fishModel.getKillEffect(roomId);
        if (killData == null) {
            return;
        }

        //根据索引获取骨骼动画数据
        var skeletonData = cf.Data.SKELETON_DATA[killData[1]];
        if (!skeletonData) {
            return;
        }
        // var res = [
        //     createVerPath("animate/" + skeletonData.json),
        //     createVerPath("animate/" + skeletonData.arts),
        //     createVerPath("animate/" + skeletonData.png)
        // ];

        if (cc.sys.isNative) {
            this._playBonus(fishModel, msg, killData, skeletonData);
        } else {
            this._playBonus(fishModel, msg, killData, skeletonData);
            // if (cc.loader.getRes(res[0]) && cc.loader.getRes(res[1]) && cc.loader.getRes(res[2])) {
            //     this._playBonus(fishModel, msg, killData, skeletonData, res);
            // } else {
            //     cc.loader.load(res, function () {
            //         this._playBonus(fishModel, msg, killData, skeletonData, res);
            //     }, this);
            // }
        }
    },

    _playBonus: function (fishModel, msg, killData, skeletonData, res) {
        var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
        var realAtlas,realJson;
        realAtlas = Res.GAME.real_dynamic_Atlas;
        realJson = Res.GAME.real_dynamic_Json;


        if (player) {
            var fishRate = fishModel.getScore();
            if (fishRate <= player.getLastBonusScore()) {
                return;
            }

            var playerModel = player.getPlayerModel();
            var _child = this._sceneNode.getChildByName("bonusNode" + playerModel.getGamePlayerId());
            if (_child) {
                _child.removeFromParent(true);
            }

            player.setLastBonusScore(fishRate);

            //位置偏移
            var offsetH = 220, offsetW = 0;
            var seatId = cf.convertSeatId(msg.nSeatID);
            if (seatId >= 2) {
                offsetH *= -1;
            }
            if (seatId == 0 || seatId == 3) {
                offsetW *= -1;
            }

            //父节点
            var bonusNode = new cc.Node();
            var _killDataVal = Math.floor(killData[0]);
            switch (_killDataVal) {
                case 1:
                    bonusNode.setPosition(player._curPos.x + offsetW, player._curPos.y + offsetH+40);
                    //区分本地玩家特效大小
                    if (this._localPlayer.getSeatID() == msg.nSeatID) {
                        bonusNode.setScale(0.6);
                    } else {
                        bonusNode.setScale(0.55);
                    }
                    bonusNode.setName("bonusNode" + playerModel.getGamePlayerId());

                    var rotateNode = new cc.Node();
                    bonusNode.addChild(rotateNode, 1);

                    var spine = ActionMgr.createSpine(realJson, realAtlas, false);
                    spine.setAnimation(0, "Reel_dynamic", false);
            
                    spine.setAttachment("txt", "txt_0"+Math.round(cf.randomRange(1, 7)))
                    bonusNode.addChild(spine);

                    var label = new cc.LabelBMFont(msg.listFish[0].nFishScore, Res.LOBBY.font_resource_090);
                    label.setPosition(spine.width / 2-200, spine.height / 2 -252);
                    label.setScale(1.1);
                    spine.addChild(label);

                    bonusNode.runAction(cc.sequence(
                        cc.delayTime(parseFloat(killData[2])), 
                       cc.spawn(cc.moveTo(0.5, player._curPos), cc.scaleTo(0.4, 0.3)),
                       cc.callFunc(function () {
                           this.removeFromParent(true);
                           player.setLastBonusScore(0);
                       }, bonusNode)
                   ));
                   this._sceneNode.runAction(cc.sequence(
                       cc.delayTime(2),
                       cc.callFunc(()=>{
                           this._sceneNode.addChild(bonusNode, cf.Zoder.EFFECT);
                       })
                   ))

                    break;
                case 2:
                     if (this._localPlayer.getSeatID() == msg.nSeatID) {
                        cf.SoundMgr.playEffect(89,false);
                        var coinJson, coinAtlas;
                        coinJson = Res.GAME.boss_coin_Json;
                        coinAtlas = Res.GAME.boss_coin_Atlas;
                        var coin = ActionMgr.createSpine(coinJson, coinAtlas, 1);
                        coin.setAnimation(3, "01_boss", false);
                        coin.setPosition(cc.winSize.width/2, cc.winSize.height/2); 
                        this._sceneNode.addChild(coin);
                        coin.runAction(cc.sequence(
                            cc.delayTime(1.5),
                            cc.callFunc(()=>{
                            }),
                            cc.delayTime(1),
                            cc.callFunc(()=>{
                            coin.setAnimation(3, "02_no", false);
                            }),
                            cc.delayTime(5),
                            cc.callFunc(()=>{
                            coin.removeFromParent();
                        })

                        ))

                         bonusNode.setPosition(cc.winSize.width / 2, cc.winSize.height / 2+100);
                         bonusNode.setScale(1);
                         
                         var rotateNode = new cc.Node();
                         bonusNode.addChild(rotateNode, 1);
     
                         var spine = ActionMgr.createSpine(realJson, realAtlas, false);
                         spine.setAnimation(0, "Reel_dynamic", false);
                         spine.setAttachment("txt", "txt_0"+Math.round(cf.randomRange(1, 7)));
                         bonusNode.addChild(spine);
     
                         var label = new cc.LabelBMFont(msg.listFish[0].nFishScore, Res.LOBBY.font_resource_090);
                         label.setPosition(spine.width / 2-200, spine.height / 2 -252);
                         label.setScale(1.1);
                         spine.addChild(label);
     
                         bonusNode.runAction(cc.sequence(
                             cc.delayTime(parseFloat(killData[2])), 
                            cc.spawn(cc.moveTo(0.5, player._curPos), cc.scaleTo(0.4, 0.3)),
                            cc.callFunc(function () {
                                this.removeFromParent(true);
                                player.setLastBonusScore(0);
                            }, bonusNode)
                        ));
                        this._sceneNode.runAction(cc.sequence(
                            cc.delayTime(3.5),
                            cc.callFunc(()=>{
                                this._sceneNode.addChild(bonusNode, cf.Zoder.EFFECT);
                            })
                        ))
                    //     cf.SoundMgr.playEffect(31, false, 3);
                    //     //粒子特效
                    //     var goldParticle = new cc.ParticleSystem(Res.LOBBY.gold_boom_plist);
                    //     goldParticle.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
                    //     goldParticle.setAutoRemoveOnFinish(true);
                    //     this._sceneNode.addChild(goldParticle, cf.Zoder.EFFECT - 1);
                    //     //速度线
                    //     var spShake = new cc.Sprite("#shake02.png");
                    //     spShake.setScaleX(cc.winSize.width / spShake.width);
                    //     spShake.setScaleY(cc.winSize.height / spShake.height);
                    //     spShake.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
                    //     spShake.runAction(cc.sequence(
                    //         cc.repeat(cc.sequence(cc.fadeIn(0.3), cc.fadeOut(0.3)), 5),
                    //         cc.callFunc(function () {
                    //             this.removeFromParent();
                    //         }, spShake)
                    //     ));
                    //     this._sceneNode.addChild(spShake, cf.Zoder.EFFECT);

                    // } else {
                    //     bonusNode.setPosition(player._curPos.x, player._curPos.y + offsetH);
                    //     bonusNode.setScale(0.65);
                     }
                    bonusNode.setName("bigBonusNode" + playerModel.getGamePlayerId());
                    break;
            }


            //旋转节点
            // var rotateNode = new cc.Node();
            // bonusNode.addChild(rotateNode, 1);
            //背景
            // var bonusBk = new cc.Sprite("#bonus_007.png");
            // bonusBk.setScale(0);
            // bonusNode.addChild(bonusBk, 0);
            // //皇冠
            // var bonusTitle = new cc.Sprite("#bonus_002.png");
            // bonusTitle.setOpacity(0);
            // bonusTitle.setScale(0);
            // bonusTitle.setPosition(-90, 115);
            // rotateNode.addChild(bonusTitle, 0);
            // //彩带
            // var bonusBar = new cc.Sprite("#bonus_003.png");
            // bonusBar.setScale(0);
            // bonusBar.setPosition(0, -50);
            // bonusNode.addChild(bonusBar, 2);

            // var spine = ActionMgr.createSpine(realJson, realAtlas, false);
            // spine.setAnimation(0, "Reel_dynamic", false);
            
            // spine.setAttachment("txt", "txt_0"+Math.round(cf.randomRange(1, 7)))
            // bonusNode.addChild(spine);
            //鱼
            // var bonusFish = ActionMgr.createSpine(res[0], res[1]);
            // bonusFish.setAnimation(0, skeletonData.action, true);
            // bonusFish.setScale(0);
            // bonusFish.setPosition(skeletonData.offsetx, skeletonData.offsety);
            // bonusNode.addChild(bonusFish, 1);

            //bonus字
            // var bonusFish = new cc.Sprite("#ui_bonus_text_00" + Math.round(cf.randomRange(1, 8)) + ".png");
            // bonusFish.setScale(0);
            // bonusFish.setPosition(0, bonusFish.height / 2);
            // bonusNode.addChild(bonusFish, 1);

            //倍数
            // var labelRate = new cc.LabelBMFont(fishRate + "倍", Res.LOBBY.font_resource_083);
            // labelRate.setPosition(bonusTitle.width / 2 - 20, bonusTitle.height / 2 + 10);
            // labelRate.setScale(0.85);
            // labelRate.setRotation(-45);
            // bonusTitle.addChild(labelRate);
            // var label = new cc.LabelBMFont(msg.listFish[0].nFishScore, Res.LOBBY.font_resource_090);
            //     label.setPosition(spine.width / 2-200, spine.height / 2 -252);
            //     label.setScale(1.1);
            //     spine.addChild(label);
            
            //执行动作
            // rotateNode.runAction(cc.repeatForever(cc.sequence(cc.rotateTo(0.5, -5), cc.rotateTo(0.5, 5))));
            // bonusBk.runAction(cc.repeatForever(cc.spawn(cc.scaleTo(1.2, 1), cc.scaleTo(1, 1.2), cc.rotateBy(0.5, 360))));
            // bonusBk.runAction(ActionMgr.createDelayBounce(0.1, 1.2, 0.25, 0.2, 0.1, 0));
            // bonusFish.runAction(ActionMgr.createDelayBounce(0.1, 1.2 + skeletonData.scale, 0.25, 0.2, 0.1, skeletonData.scale));

            // var _itemLen = msg.itemReward.length;
            // if (_itemLen > 0) {
            //     for (var j = 0; j < _itemLen; ++j) {
            //         var item = msg.itemReward[j];
            //         if (item) {
            //             var _itemData = cf.Data.ITEM_DATA[item.nItemID];
            //             var awardNum = new ccui.Text(_itemData.itemName + "x" + item.nNum, cf.Language.FontName, 30);
            //             awardNum.setPosition(bonusBar.width / 2, bonusBar.height / 2 + 8 + j * 35);
            //             awardNum.setTextColor(cc.color.YELLOW);
            //             awardNum.enableOutline(cc.color(165, 42, 42), 2);
            //             bonusBar.addChild(awardNum);
            //         }
            //     }

            //     bonusBar.runAction(ActionMgr.createBounce(1.2, 0.25, 0.2, 0.1, 0.2));
            // } else {
            //     //得分
            //     // var fortIndex = msg.nBulletLevel;
            //     // var fortNum = cf.Data.CANNON_LEVEL_DATA[fortIndex].cannonlevel;
            //    // var label = new cc.LabelBMFont(fortNum * fishRate, Res.LOBBY.font_resource_088);
                
            //     var label = new cc.LabelBMFont(msg.listFish[0].nFishScore, Res.LOBBY.font_resource_090);
            //     label.setPosition(bonusBar.width / 2, bonusBar.height / 2 -5);
            //     label.setScale(0.9);
            //     bonusBar.addChild(label);

            //     //数字动作
            //     for (var i = 0; i < label.getChildrenCount(); ++i) {
            //         var child = label.getChildByTag(i);
            //         if (child) {
            //             child.setScale(0);
            //             child.runAction(cc.sequence(ActionMgr.createDelayBounce(0.1 * i, 1.7, 0.25, 0.2, 0.1, 0)));
            //         }
            //     }
            //     bonusTitle.runAction(ActionMgr.createDelayBounce(0.2, 1.2, 0.25, 0.2, 0.1, 0));
            //     bonusBar.runAction(ActionMgr.createBounce(1.2, 0.25, 0.2, 0.1, 0));
            // }

            //父节点动作
           
            
        }
    },

    playLevelUpEffect: function (resMsg) {
        var player = cf.RoomMgr.getRoomPlayer(resMsg.nSeatID);
        if (player) {
            var dlg = UIUpgradeReward.create(player, resMsg.itemReward, resMsg.nLevel, function () {
            }, this._sceneNode);
            this._sceneNode.addChild(dlg, cf.Zoder.UI + 10);

            //升级
            cf.SoundMgr.playEffect(11, false, 2.2);
        }
    },

    /**
     * 锁定技能效果执行特效
     */
    playLockEffect: function (pos) {
        if (this._lockEffect) {
            if (!this._lockEffect.isVisible()) {
                this._lockEffect.setScale(1);
                this._lockEffect.setVisible(true);
                this._lockEffect.setBlendFunc( cc.SRC_ALPHA,cc.ONE);  
                this._lockEffect.runAction(cc.scaleTo(0.5, 0.8));
            }
            this._lockEffect.setPosition(pos);
        } else {
            this._lockEffect = new cc.Sprite();
            this._lockEffect.setPosition(pos);
            this._lockEffect.setBlendFunc( cc.SRC_ALPHA,cc.ONE);  
            this._lockEffect.runAction(cc.repeatForever(ActionMgr.getAnimate("lock")));
            this._lockEffect.runAction(cc.repeatForever(cc.rotateBy(6, 360)));
            this._lockEffect.setScale(1);
            this._lockEffect.runAction(cc.scaleTo(0.5, 0.8));
            cf.BulletMgr.getBulletNode().addChild(this._lockEffect, 1000);
        }
    },

    /**
     * 停止锁定技能
     */
    stopLockEffect: function () {
        if (this._lockEffect) {
            this._lockEffect.setVisible(false);
        }
    },

    playIceEffect: function () {
        if (!this._iceEffect) {
            this._iceEffect = new cc.Sprite("#skill_002.png");
            this._iceEffect.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
            this._iceEffect.setScaleX(cc.winSize.width / this._iceEffect.width);
            this._iceEffect.setScaleY(cc.winSize.height / this._iceEffect.height);
            cf.FishMgr.getFishNode().addChild(this._iceEffect, 1000);
        }
        else {
            this._iceEffect.stopAllActions();
            this._iceEffect.setOpacity(255);
        }
    },

    stopIceEffect: function () {
        if (this._iceEffect) {
            var fadeOut = cc.fadeOut(2);
            this._iceEffect.runAction(fadeOut);
        }
    },

    playSummonEffect: function (position) {
        var summonEffect = new cc.Sprite("#summon.png");
        cf.FishMgr.getFishNode().addChild(summonEffect);
        summonEffect.stopAllActions();
        summonEffect.setPosition(position);
        summonEffect.setScale(0);
        summonEffect.setVisible(true);
        summonEffect.setOpacity(230);
        summonEffect.runAction(cc.repeatForever(cc.rotateBy(0.8, 360)));
        summonEffect.runAction(cc.sequence(
            cc.scaleTo(1, 1.2),
            cc.delayTime(1),
            cc.fadeOut(0.5),
            cc.callFunc(function () {
                this.removeFromParent();
            }, summonEffect)
        ));
    },

    /**
     * 播放捕鱼道具获取特效
     * @param fishPos 鱼位置
     * @param msg 捕鱼消息
     */
    playGetItem: function (fishPos, msg) {
        var itemLen = msg.itemReward.length;
        if (itemLen <= 0)
            return;

        var _posX = fishPos.x, _posY = fishPos.y;
        var localSeatId = this._localPlayer.getSeatID();
        if (localSeatId >= 2) {
            _posX = cc.winSize.width - _posX;
            _posY = cc.winSize.height - _posY;
        }

        if (msg.nSeatID == -1) {
            this.doOthersGetItem(fishPos, msg, itemLen, _posX, _posY);
            return;
        }

        var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
        if (player) {
            //道具奖励
            for (var i = 0; i < itemLen; i++) {
                var item = msg.itemReward[i];
                //如果是掉落的是钻石，需要刷新玩家信息
                switch (item.nItemID) {
                    case cf.ItemID.MAP01:
                    case cf.ItemID.MAP02:
                    case cf.ItemID.MAP03:
                        //只刷新本地玩家
                        if (msg.nSeatID == localSeatId) {
                            this._localPlayer.setPlayerItem(item.nItemID, item.nItemNum, item.strEndTime, item.nError);
                        }
                        break;
                    case cf.ItemID.SKILL_1030101://技能
                    case cf.ItemID.SKILL_1030102:
                    case cf.ItemID.SKILL_1030103:
                    case cf.ItemID.SKILL_1030104:
                    case cf.ItemID.SKILL_1030111:
                    case cf.ItemID.SKILL_1030112:
                    case cf.ItemID.SKILL_1030113:
                    case cf.ItemID.SKILL_1030114:
                        //只刷新本地玩家
                        if (msg.nSeatID == localSeatId) {
                            this._localPlayer.setPlayerItem(item.nItemID, item.nItemNum, item.strEndTime, item.nError);
                        }
                        break;
                }

                var drop = cf.Data.DROP_MISSILE_DATA[item.nNum];
                var len;
                if (drop) {
                    len = drop.num.length;
                }
                //弹头数量过多没有数据这里特殊处理
                if (item.nNum > 100) {
                    drop = [];
                    len = 10;
                    var localItem = item.nNum;
                    for (var k = 0; k < 10; k++) {
                        if (k == 0) {
                            var last = localItem % 10;
                            drop.push(last);
                            localItem = localItem - last;
                        }
                        else {
                            drop.push(localItem / 10);
                        }
                    }
                }

                for (var j = 0; j < len; ++j) {
                    var _itemNum;
                    if (drop.num) {
                        _itemNum = drop.num[j];
                    }
                    else {
                        _itemNum = drop[j];
                    }

                    //创建道具图标
                    var _itemData = cf.Data.ITEM_DATA[item.nItemID];
                    var spItem = new cc.Node();
                    spItem.setScale(0);
                    this._itemNode.addChild(spItem, 0);

                    //计算角度
                    var angle = 360 / len * j;
                    //随机半径
                    var radius = 250;
                    if (len < 5) {
                        radius = 180;
                    }
                    if (j % 2 == 0) {
                        radius = cf.randomRange(10, radius * 0.65);
                    } else {
                        radius = cf.randomRange(radius * 0.75, radius);
                    }

                    var x = _posX + Math.sin(cc.degreesToRadians(angle)) * radius;
                    var y = _posY + Math.cos(cc.degreesToRadians(angle)) * radius;
                    if (msg.strBulletID == -1) {
                        x = cc.winSize.width / 2 + Math.sin(cc.degreesToRadians(angle)) * radius;
                        y = cc.winSize.height / 2 + Math.cos(cc.degreesToRadians(angle)) * radius;
                    }

                    spItem.setPosition(x, y);

                    var sp = new cc.Sprite("#" + _itemData.dropIcon);
                    sp.setScale(0.8);
                    spItem.addChild(sp);

                    //特效
                    var spEffect = new cc.Sprite("#effect_around_bk.png");
                    spEffect.runAction(cc.repeatForever(cc.rotateBy(5, 360)));
                    spEffect.setScale(1.4);
                    spItem.addChild(spEffect, -1);

                    //道具获得数量
                    var label = new ccui.Text(_itemData.itemName + "x" + _itemNum, cf.Language.FontName, 24);
                    label.setPosition(0, -50);
                    label.setTextColor(cc.color(255, 255, 145));
                    label.enableOutline(cc.color(232, 28, 19), 2);
                    spItem.addChild(label);

                    //世界boss掉落特效
                    if (msg.strBulletID == -1 && j == len - 1) {
                        var awardNum = new ccui.Text(_itemData.itemName + "x" + item.nNum, cf.Language.FontName, 34);
                        awardNum.setTextColor(cc.color.YELLOW);
                        awardNum.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2));
                        awardNum.enableOutline(cc.color(165, 42, 42), 2);
                        this._itemNode.addChild(awardNum, 0);
                        awardNum.runAction(cc.sequence(cc.delayTime(2.8), cc.callFunc(function () {
                            awardNum.removeFromParent();
                        })));

                        //粒子特效
                        var goldParticle = new cc.ParticleSystem(Res.LOBBY.gold_boom_plist);
                        goldParticle.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
                        goldParticle.setAutoRemoveOnFinish(true);
                        this._sceneNode.addChild(goldParticle, cf.Zoder.EFFECT - 1);
                        //速度线
                        var spShake = new cc.Sprite("#shake02.png");
                        spShake.setScaleX(cc.winSize.width / spShake.width);
                        spShake.setScaleY(cc.winSize.height / spShake.height);
                        spShake.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
                        spShake.runAction(cc.sequence(
                            cc.repeat(cc.sequence(cc.fadeIn(0.3), cc.fadeOut(0.3)), 5),
                            cc.callFunc(function () {
                                this.removeFromParent();
                            }, spShake)
                        ));
                        this._sceneNode.addChild(spShake, cf.Zoder.EFFECT);
                    }

                    //执行特效动作
                    var speed = this.getFlyTime(_posX, _posY, player._curPos.x, player._curPos.y, 500);
                    var moveAction = cc.moveTo(speed, player._curPos).easing(cc.easeBackIn());
                    spItem.runAction(cc.sequence(
                        ActionMgr.createBounce(1, 0.35, 0.2, 0.2, 0),
                        cc.delayTime(2.5),
                        cc.callFunc(function () {
                            //this是标题
                            this.removeFromParent(true);
                        }, label),
                        cc.callFunc(function () {
                            //this是旋转特效
                            this.removeFromParent(true);
                        }, spEffect),
                        moveAction,
                        cc.callFunc(function () {
                            //this是道具本身
                            this.removeFromParent(true);
                        }, spItem)
                    ));
                }
            }
            //通知刷新UI
            cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UPDATE_ROOM_UI);
        }
    },

     //在拿到连线鱼列表后进行随机的连线排序并且执行相应的连线动作
     addRandomLineFishOrder: function (fishList, msg) {
        var self = this;

        var  spineJson = Res.GAME.hammer31_Json;
        var spineAtlas = Res.GAME.hammer31_Atlas;
       
        var thunder = ActionMgr.createSpine(spineJson,spineAtlas,1);
        thunder.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        thunder.setAnimation(0, "attack31", false);
        this._sceneNode.addChild(thunder, cf.Zoder.EFFECT+1);

        cf.SoundMgr.playEffect(97);
        // var delayTime = setTimeout(()=>{

        
            if (!fishList || fishList.length <= 0) {
                return;
            }
            var exchangeIndex = function (index1, index2) {
                var temp = fishList[index1];
                fishList[index1] = fishList[index2];
                fishList[index2] = temp;
            };
            for (var i = fishList.length - 1; i >= 1; i--) {
                exchangeIndex(Math.round(Math.random() * (fishList.length - 2) + 1), i);
            }
            var listLength = fishList.length;
            var lightFrameName = "#mixlightning_001.png";
            var lightAnimateName = "mixlightning";
            switch (cf.flashColor) {
                case 0:
                    lightFrameName = "#mixlightning_001.png";
                    lightAnimateName = "mixlightning";
                    break;
                case 1:
                    lightFrameName = "#purplelightning_001.png";
                    lightAnimateName = "purplelightning";
                    break;
            }
            cf.flashColor++;
            if (cf.flashColor == 2) {
                cf.flashColor = 0;
            }
    
            //暂停开火音效
           // cf.SoundMgr.pauseOnFire();
            for (var i = 0; i < listLength - 1; i++) {
                var flashLine = new cc.Sprite(lightFrameName);
                var lineFish1 = fishList[i];
                var lineFish2 = fishList[i + 1];
    
                //转换坐标
                var _posX = lineFish1.x, _posY = lineFish1.y;
                var localSeatId = this._localPlayer.getSeatID();
                if (localSeatId >= 2) {
                    _posX = cc.winSize.width - _posX;
                    _posY = cc.winSize.height - _posY;
                }
    
                flashLine.setAnchorPoint(0.5, 0);
                flashLine.setScale(0);
                flashLine.setPosition(_posX, _posY);
                this._sceneNode.addChild(flashLine, cf.Zoder.EFFECT);
    
                //计算角度
                flashLine.setRotation(self.getLineAngle(lineFish1.x, lineFish1.y, lineFish2.x, lineFish2.y));
    
                //计算长度
                var fLen = cc.pDistance(lineFish1.getPosition(), lineFish2.getPosition()) / flashLine.height;
                flashLine.runAction(cc.repeatForever(ActionMgr.getAnimate(lightAnimateName)));
                flashLine.runAction(cc.sequence(
                    cc.delayTime(0.15 * i),
                    cc.callFunc(function () {
                        //播放音效
                       //' cf.SoundMgr.playEffect(52);
                    }),
                    cc.scaleTo(0.15, 1, fLen),
                    cc.delayTime(0.15 * (listLength - i)),
                    cc.callFunc(function (nodeExecutingAction, value) {
                        //爆炸特效
                        self.doSkillFishEffect(true, value, msg, 6);
                    }, flashLine, lineFish1.getPosition()),
                    cc.delayTime(0.5),
                    cc.fadeOut(0.2),
                    cc.callFunc(function () {
                        this.removeFromParent();
                    }, flashLine)
                ));
            }
            var time = 0.15 * listLength;
            self._sceneNode.runAction(cc.sequence(
                cc.delayTime(time),
                cc.callFunc(function () {
                    ///cf.SoundMgr.playEffect(51, false, 2);
                    cf.RoomMgr.shakeRoom(5, 15, 0.05);
                }),
                cc.delayTime(1.2),
                cc.callFunc(function () {
                    self.addHammerAnimation(msg, 28);
                    // self.addRewardLabel(msg.nScore, 350, 1, msg, fishList[0].getPosition());
                    // cf.SoundMgr.resumeOnFire();
                })
            ));
        
        // },1100);
    },
    /**
     * 
     *  
     * 轟雷戢 烈焰垂
     * 
     */
    addHammerAnimation: function(msg, fishid){
        var self = this;
        console.log(msg);
        var localSeatId = this._localPlayer.getSeatID();
        
        var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
        var rewardScoreNum = msg.nScore;
        var effectAtlas = Res.GAME.effects_Atlas;
        var effectJson = Res.GAME.effects_Json;

        
        var _posX = player._curPos.x , _posY = player._curPos.y;
        // if (localSeatId >= 2) {
        //      _posX = cc.winSize.width - _posX;
        //      _posY = cc.winSize.height - player._curPos.y-y;
        // }
        if(localSeatId == msg.nSeatID){
            var node = new cc.Node();
           // node.setPosition(_posX , _posY );
            node.setPosition(cc.winSize.width/2 , cc.winSize.height/2 );
            node.setScale(0);
            this._sceneNode.addChild(node, cf.Zoder.UI);
            var ani = ActionMgr.createSpine(Res.GAME.bonusAni_Json, Res.GAME.bonusAni_Atlas);
            var effect = ActionMgr.createSpine(effectJson, effectAtlas);
            if(fishid == 27){
                ani.setAnimation(0, "fishLibrary_030", false);
                effect.setAnimation(0, "effects_030", false);
            }else{
                ani.setAnimation(0, "fishLibrary_031", false);
                effect.setAnimation(0, "effects_031", false);
            }
            node.addChild(effect);
            ani.setScale(0.8);
            node.addChild(ani);

            var _scale = 0.75;
            if (rewardScoreNum >= 1000000) {
                _scale = 0.55
            }
            //得分
            var label = new cc.LabelBMFont("" + rewardScoreNum, Res.LOBBY.font_resource_088);
            label.setPosition(0, -90);
            label.setScale(_scale);
            node.addChild(label, 1);

            node.runAction(cc.sequence(
             cc.scaleTo(0.25, 1.4),
             cc.scaleTo(0.2, 1.3),
             cc.scaleTo(0.1, 1.4),
             cc.delayTime(2),
            //cc.scaleTo(0.2, 0),
            cc.spawn(cc.moveTo(1.2, player._curPos.x ,player._curPos.y), cc.scaleTo(1.2, 0)),
            cc.callFunc(function () {
                node.removeFromParent();
            })
        ));

        //数字动作
        for (var i = 0; i < label.getChildrenCount(); ++i) {
            var child = label.getChildByTag(i);
            if (child) {
                child.setScale(0);
                child.runAction(cc.sequence(ActionMgr.createDelayBounce(0.1 * i, 1.7, 0.25, 0.2, 0.1, 0)));
            }
        }

     
        }
    },
    
  

        

    /**
     * 放射性连线鱼表现形式
     */
    addRedioActivityLineFishOrder: function (fishList, msg) {
        var self = this;

       // var fish = cf.FishMgr.getFishMap().get(fishList[i].strFishID);
       var  spineJson = Res.GAME.hammer31_Json;
        var spineAtlas = Res.GAME.hammer31_Atlas;
       
        var thunder = ActionMgr.createSpine(spineJson,spineAtlas,1);
        thunder.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        thunder.setAnimation(0, "attack31", false);
        this._sceneNode.addChild(thunder, cf.Zoder.EFFECT+1);

        cf.SoundMgr.playEffect(97);
       // var time = setTimeout(()=>{
            
        var fishid = 28;
        if (!fishList || fishList.length <= 0) {
            return;
        }
        var lightFrameName = "#mixlightning_001.png";
        var lightAnimateName = "mixlightning";
        switch (cf.flashColor) {
            case 0:
                lightFrameName = "#mixlightning_001.png";
                lightAnimateName = "mixlightning";
                break;
            case 1:
                lightFrameName = "#purplelightning_001.png";
                lightAnimateName = "purplelightning";
                break;
        }
        cf.flashColor++;
        if (cf.flashColor == 2) {
            cf.flashColor = 0;
        }
        //暂停开炮音效
        //cf.SoundMgr.pauseOnFire();
        var listLength = fishList.length;
        for (var i = 1; i < listLength; i++) {
            var flashLine = new cc.Sprite(lightFrameName);
            var lineFish1 = fishList[0];
            var lineFish2 = fishList[i];
            //转换坐标
            var _posX = lineFish1.x, _posY = lineFish1.y;
            var localSeatId = this._localPlayer.getSeatID();
            if (localSeatId >= 2) {
                _posX = cc.winSize.width - _posX;
                _posY = cc.winSize.height - _posY;
            }

            flashLine.setAnchorPoint(0.5, 0);
            flashLine.setScale(0);
            flashLine.setPosition(_posX, _posY);
            this._sceneNode.addChild(flashLine, cf.Zoder.EFFECT);

            //计算角度
            flashLine.setRotation(self.getLineAngle(lineFish1.x, lineFish1.y, lineFish2.x, lineFish2.y));

            //计算长度
            var fLen = cc.pDistance(lineFish1.getPosition(), lineFish2.getPosition()) / flashLine.height;
            flashLine.runAction(cc.repeatForever(ActionMgr.getAnimate(lightAnimateName)));
            flashLine.runAction(cc.sequence(
                cc.scaleTo(0.1, 1, fLen),
                cc.callFunc(function (nodeExecutingAction, value) {
                    self.doSkillFishEffect(true, value, msg, 6);
                }, flashLine, lineFish2.getPosition()),
                cc.delayTime(1),
                cc.callFunc(function () {
                    this.removeFromParent();
                }, flashLine)
            ));
        }
        cf.SoundMgr.playEffect(51, false, 2);
        cf.RoomMgr.shakeRoom(5, 15, 0.05);
        var time = 1.3;
        self._sceneNode.runAction(cc.sequence(
            cc.delayTime(time),
            cc.callFunc(function () {
               // self.addRewardLabel(msg.nScore, 350, 1, msg, fishList[0].getPosition(), fishid);
               self.addHammerAnimation(msg, 28);
                //cf.SoundMgr.resumeOnFire();
            })
        ));
    
    // },1100);
    },
    doAimBoomEffect: function (fishList, msg) {
        var self = this;

        var  spineJson = Res.GAME.hammer30_Json;
        var spineAtlas = Res.GAME.hammer30_Atlas;
       
        var boom = ActionMgr.createSpine(spineJson,spineAtlas,1);
        boom.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        boom.setAnimation(0, "attack30", false);
        this._sceneNode.addChild(boom, cf.Zoder.EFFECT+1);

        
       var delayTime = setTimeout(()=>{
        cf.SoundMgr.playEffect(96);

        var listLength = fishList.length;
        var fishid = 27;
        cf.RoomMgr.shakeRoom(6, 15, 0.05);
       // cf.SoundMgr.playEffect(32);
       // cf.SoundMgr.pauseOnFire();
        for (var i = 0; i < listLength; i++) {
            var fishPos = fishList[i].getPosition();
            //爆炸特效
            var boomEffect = new cc.Sprite("#bombFish_001.png");
            boomEffect.setPosition(cc.winSize.width/2, cc.winSize.height/2);
            boomEffect.setOpacity(0);
            cf.BulletMgr.getBulletNode().addChild(boomEffect, 99);

            var time = 0;
            if (i == 0) {
                //爆炸特效
                boomEffect.setScale(10);
            } else {
                time = 0.2;
                boomEffect.setScale(cf.randomRange(10, 80) / 10);
            }
            boomEffect.runAction(cc.sequence(
                cc.delayTime(0),
                cc.callFunc(function () {
                    this.setOpacity(200);
                }, boomEffect),
                ActionMgr.getAnimate("bombFish"),
                cc.fadeOut(0.2),
                cc.callFunc(function () {
                    this.removeFromParent(true);
                }, boomEffect)
            ));

            self.doSkillFishEffect(false, fishPos, msg, 6);
        }
        cf.RoomMgr.shakeRoom(5, 15, 0.05);
        var time = 1.3;
        self._sceneNode.runAction(cc.sequence(
            cc.delayTime(time),
            cc.callFunc( function() {
               // self.addRewardLabel(msg.nScore, 350, 2, msg, fishList[0].getPosition(), fishid);
               self.addHammerAnimation(msg, 27);
               // cf.SoundMgr.resumeOnFire();
            })
        ));
        
        clearTimeout(delayTime);
    },500);
    },
    /**
     * 恭喜获得彩金标题动画
     * @param rewardScoreNum 获得彩金数量
     * @param y 高度偏移
     */
     addRewardLabel: function (rewardScoreNum, y, num, msg, position, fishid) {
        var self = this;
        
        var localSeatId = this._localPlayer.getSeatID();
        
        var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);

        
        var _posX = player._curPos.x , _posY = player._curPos.y ;
        // if (localSeatId >= 2) {
        //      _posX = cc.winSize.width - _posX;
        //      _posY = cc.winSize.height - player._curPos.y-y;
        // }
        if(localSeatId == msg.nSeatID){
            var node = new cc.Node();
            node.setPosition(_posX , _posY );
            node.setScale(0);
            this._sceneNode.addChild(node, cf.Zoder.UI);
            var ani = ActionMgr.createSpine(Res.GAME.bonusAni_Json, Res.GAME.bonusAni_Atlas);
            if(fishid == 27){
                ani.setAnimation(0, "fishLibrary_030", true);
            }else{
                ani.setAnimation(0, "fishLibrary_031", true);
            }
            ani.setScale(0.8);
            node.addChild(ani);

            var _scale = 0.75;
            if (rewardScoreNum >= 1000000) {
                _scale = 0.55
            }
            //得分
            var label = new cc.LabelBMFont("" + rewardScoreNum, Res.LOBBY.font_resource_088);
            label.setPosition(0, -70);
            label.setScale(_scale);
            node.addChild(label, 1);

            node.runAction(cc.sequence(
             cc.scaleTo(0.25, 1.4),
             cc.scaleTo(0.2, 1.3),
             cc.scaleTo(0.1, 1.4),
             cc.delayTime(2),
            //cc.scaleTo(0.2, 0),
            cc.spawn(cc.moveTo(1.2, player._curPos.x ,player._curPos.y), cc.scaleTo(1.2, 0)),
            cc.callFunc(function () {
                node.removeFromParent();
            })
        ));

        //数字动作
        for (var i = 0; i < label.getChildrenCount(); ++i) {
            var child = label.getChildByTag(i);
            if (child) {
                child.setScale(0);
                child.runAction(cc.sequence(ActionMgr.createDelayBounce(0.1 * i, 1.7, 0.25, 0.2, 0.1, 0)));
            }
        }

        var goldParticle = new cc.ParticleSystem(Res.LOBBY.gold_boom_plist);
        goldParticle.setPosition(_posX , _posY );
        goldParticle.setScale(0.75);
        goldParticle.setAutoRemoveOnFinish(true);
        this._sceneNode.addChild(goldParticle, cf.Zoder.EFFECT - 1);
        this.pokerParticle();
        }
        
    },
    doSkillFishEffect: function (isBoom, fishPos, msg, total,boomR,scale,frameName) {
        var localSeatId = this._localPlayer.getSeatID();
        var total = 3;
        var _angle = 0;
        if (localSeatId >= 2) {
            _angle = 180;
        }

        //金币
        var goldAction = "effect_silver";
        if (localSeatId == msg.nSeatID) {
            //本地玩家
            goldAction = "effect_gold";
        }
        

        var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
        if (player) {
            //for (var i = 0; i < total; ++i) {
                var coin = new cc.Sprite("#effect_gold_001.png");
                coin.setPosition(cc.winSize.width/2, cc.winSize.height/2);

                var offsetX = 290 + cf.randomRange(0, 120);
                var offsetY = cf.randomRange(-60, 60);
                if (localSeatId >= 2) {
                    offsetX *= -1;
                    offsetY *= -1;
                    coin.setRotation(180);
                }
                if (localSeatId == 1 || localSeatId == 3)
                    //_time = 1;
                coin.runAction(cc.repeatForever(ActionMgr.getAnimate("coin")));
                coin.runAction(cc.sequence(
                    cc.delayTime(0.1),
                    cc.jumpBy(0.5, offsetX, offsetY, 50, 1),
                    //cc.delayTime((20 - i) * 0.1+i * 0.1),
                    cc.spawn(cc.moveTo(1, player._goldPos).easing(cc.easeBackIn()), cc.scaleTo(1, 0.5)),
                    cc.callFunc(function () {
                        player.doGoldAction();
                        this.removeFromParent(true);
                        //cf.SoundMgr.playEffect(17);
                    }, coin)
                ));
                cf.BulletMgr.getBulletNode().addChild(coin);
            //}
        }
       // cf.SoundMgr.playEffect(15);

        // //爆炸特效
        // if (isBoom) {
        //     var boomEffect = new cc.Sprite("#firework_001.png");
        //     //这里200是标准
        //     boomEffect.setScale(4);
        //     boomEffect.setPosition(fishPos);
        //     boomEffect.setOpacity(255);
        //     cf.BulletMgr.getBulletNode().addChild(boomEffect, 99);

        //     boomEffect.runAction(cc.sequence(
        //         ActionMgr.getAnimate("firework"),
        //         cc.fadeOut(0.1),
        //         cc.callFunc(function () {
        //             this.removeFromParent(true);
        //         }, boomEffect)
        //     ));
        // }

        /*if(msg.nSeatID == localSeatId){
         //粒子特效
         var goldParticle = new cc.ParticleSystem(Res.LOBBY.gold03_plist);
         goldParticle.setPosition(fishPos);
         goldParticle.setAutoRemoveOnFinish(true);
         cf.BulletMgr.getBulletNode().addChild(goldParticle, 99);
         }else{
         var goldParticle = new cc.ParticleSystem(Res.LOBBY.gold03_plist);
         goldParticle.setPosition(fishPos);
         goldParticle.setAutoRemoveOnFinish(true);
         cf.BulletMgr.getBulletNode().addChild(goldParticle, 99);
         }*/
    },
    getLineAngle: function (beginX, beginY, endX, endY) {
        //这里因为鱼位置是在旋转的画布上，但是闪电位置是在非旋转画布上，这里需要转换一下坐标
        var _beginX = beginX, _beginY = beginY, _endX = endX, _endY = endY;
        var localSeatId = this._localPlayer.getSeatID();
        if (localSeatId >= 2) {
            _beginX = cc.winSize.width - _beginX;
            _beginY = cc.winSize.height - _beginY;

            _endX = cc.winSize.width - _endX;
            _endY = cc.winSize.height - _endY;
        }

        if (_beginX != _endX || _beginY != _endY) {
            //计算方向分量
            var dirX = _beginX - _endX;
            var dirY = _beginY - _endY;
            //计算方向夹角
            var atanVal = Math.atan2(dirY, dirX);
            var angle = cc.radiansToDegrees(atanVal);

            //这里向下为正方向，做个处理转换下角度
            return (-90 - angle);
        }
        return 0;
    },


    addRollEffect: function (rollNum, clipper, nodeNum) {
        for (var i = 0; i < rollNum; i++) {
            var node = clipper.getChildByTag(10000 + nodeNum);
            var num = 1 + Math.round(Math.random() * 8);
            node.setPosition(node.x, 100);
            var sp = new cc.LabelBMFont(num.toString(), Res.LOBBY.font_resource_088);
            sp.setScale(1.4);
            sp.setPosition(node.x, 100 + (sp.height + 50) * i);
            clipper.addChild(sp);

            var dis = cc.pDistance(cc.p(node.x, 100 + (sp.height + 50) * i), cc.p(node.x, -100));

            var time = dis / 900;

            sp.runAction(cc.sequence(cc.moveTo(time, cc.p(node.x, -100)), cc.callFunc(function () {
                this.removeFromParent();
            }, sp)));
        }
    },

    //于其他房间击杀世界boss的掉落逻辑
    doOthersGetItem: function (fishPos, msg, itemLen, _posX, _posY) {
        for (var i = 0; i < itemLen; i++) {
            var item = msg.itemReward[i];
            var drop;
            var len;
            if (item.nNum > 100) {
                drop = [];
                len = 10;
                var localItem = item.nNum;
                for (var k = 0; k < 10; k++) {
                    if (k == 0) {
                        var last = localItem % 10;
                        drop.push(last);
                        localItem = localItem - last;
                    }
                    else {
                        drop.push(localItem / 10);
                    }
                }
            }
            else {
                drop = cf.Data.DROP_MISSILE_DATA[item.nNum];
                len = drop.num.length;
            }

            for (var j = 0; j < len; ++j) {
                var _itemNum;
                if (drop.num) {
                    _itemNum = drop.num[j];
                }
                else {
                    _itemNum = drop[j];
                }

                //创建道具图标
                var _itemData = cf.Data.ITEM_DATA[item.nItemID];
                var spItem = new cc.Node();
                spItem.setScale(0);
                this._itemNode.addChild(spItem, 0);

                //计算角度
                var angle = 360 / len * j;
                //随机半径
                var radius = 250;
                if (len < 5) {
                    radius = 180;
                }
                if (j % 2 == 0) {
                    radius = cf.randomRange(10, radius * 0.65);
                } else {
                    radius = cf.randomRange(radius * 0.75, radius);
                }

                var x = cc.winSize.width / 2 + Math.sin(cc.degreesToRadians(angle)) * radius;
                var y = cc.winSize.height / 2 + Math.cos(cc.degreesToRadians(angle)) * radius;

                spItem.setPosition(x, y);

                var sp = new cc.Sprite("#" + _itemData.dropIcon);
                sp.setScale(0.8);
                spItem.addChild(sp);

                //特效
                var spEffect = new cc.Sprite("#effect_around_bk.png");
                spEffect.runAction(cc.repeatForever(cc.rotateBy(5, 360)));
                spEffect.setScale(1.4);
                spItem.addChild(spEffect, -1);

                //道具获得数量
                var label = new ccui.Text(_itemData.itemName + "x" + _itemNum, cf.Language.FontName, 24);
                label.setPosition(0, -50);
                label.setTextColor(cc.color(255, 255, 145));
                label.enableOutline(cc.color(232, 28, 19), 2);
                spItem.addChild(label);

                if (j == len - 1) {
                    var awardNum = new ccui.Text(_itemData.itemName + "x" + item.nNum, cf.Language.FontName, 34);
                    awardNum.setTextColor(cc.color.YELLOW);
                    awardNum.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2));
                    awardNum.enableOutline(cc.color(165, 42, 42), 2);
                    this._itemNode.addChild(awardNum, 0);
                    awardNum.runAction(cc.sequence(cc.delayTime(2.5), cc.callFunc(function () {
                        awardNum.removeFromParent();
                    })))
                }

                //执行特效动作
                var moveAction = cc.delayTime(0);
                spItem.runAction(cc.sequence(
                    ActionMgr.createBounce(1, 0.35, 0.2, 0.2, 0),
                    cc.delayTime(1.5),
                    cc.callFunc(function () {
                        //this是标题
                        this.removeFromParent(true);
                    }, label),
                    cc.callFunc(function () {
                        //this是旋转特效
                        this.removeFromParent(true);
                    }, spEffect),
                    moveAction,
                    cc.callFunc(function () {
                        //this是道具本身
                        this.removeFromParent(true);
                    }, spItem)
                ));
            }
        }

        var playerId = msg.nidLogin;
        var rewardLab = new ccui.Text("恭喜玩家" + playerId + "击杀世界Boss" + "获得了" + cf.Data.ITEM_DATA[item.nItemID].itemName + "x" + item.nNum, cf.Language.FontName, 36);
        rewardLab.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        rewardLab.setScale(2);
        rewardLab.setTextColor(cc.color.YELLOW);
        rewardLab.enableOutline(cc.color(165, 42, 42), 2);
        this._itemNode.addChild(rewardLab);

        rewardLab.runAction(cc.sequence(cc.spawn(cc.scaleTo(0.5, 1), cc.moveTo(0.5, cc.p(cc.winSize.width / 2, cc.winSize.height / 2 + 150))), cc.delayTime(2.5), cc.callFunc(function () {
            rewardLab.removeFromParent();
        })));

        //粒子特效
        var goldParticle = new cc.ParticleSystem(Res.LOBBY.gold_boom_plist);
        goldParticle.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        goldParticle.setAutoRemoveOnFinish(true);
        this._sceneNode.addChild(goldParticle, cf.Zoder.EFFECT - 1);
        //速度线
        var spShake = new cc.Sprite("#shake02.png");
        spShake.setScaleX(cc.winSize.width / spShake.width);
        spShake.setScaleY(cc.winSize.height / spShake.height);
        spShake.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        spShake.runAction(cc.sequence(
            cc.repeat(cc.sequence(cc.fadeIn(0.3), cc.fadeOut(0.3)), 5),
            cc.callFunc(function () {
                this.removeFromParent();
            }, spShake)
        ));
        this._sceneNode.addChild(spShake, cf.Zoder.EFFECT);
    },

    getFlyTime: function (startX, startY, endX, endY, speed) {
        var x = endX - startX;
        var y = endY - startY;

        var _time = (x * x + y * y) / (speed * speed);
        _time = Math.min(1.3, _time);
        if (_time < 1) {
            _time = 1;
        }
        return _time;
    },

    /**
     * 这里添加延时的原因时需要player生成完毕
     */
    playLocalTip: function (pos) {
        if (cc._renderType !== cc.game.RENDER_TYPE_CANVAS) {
            var clipper = new cc.ClippingNode();
            clipper.attr({x: 0, y: 0});
            clipper.inverted = true;
            clipper.alphaThreshold = 0.5;
            var maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
            clipper.addChild(maskLayer);

            var stencil = new cc.Sprite("#mask.png");
            stencil.setPosition(pos);
            stencil.setScale(5.5);
            stencil.runAction(cc.repeatForever(cc.sequence(
                cc.scaleTo(0.4, 5.7),
                cc.scaleTo(0.4, 5.5)
            )));
            clipper.stencil = stencil;
            this._sceneNode.addChild(clipper, cf.Zoder.UI + 1);
            clipper.runAction(cc.sequence(
                cc.delayTime(4),
                cc.callFunc(function () {
                    clipper.removeFromParent(true);
                })
            ));
        }
     
        var localTip = ActionMgr.createSpine(Res.GAME.localTip_Json, Res.GAME.localTip_Atlas);
        localTip.setAnimation(0, "localTipAni", true);
        localTip.setPosition(pos.x, pos.y + 100);
        localTip.runAction(cc.sequence(
            cc.delayTime(4),
            cc.callFunc(function () {
                this.removeFromParent(true);
            }, localTip)
        ));
        this._sceneNode.addChild(localTip, cf.Zoder.UI + 1);
        
    },

    playRewardGold: function (x, y) {
        var _posX = x, _posY = y, _dir = 1, _time = 1;
        var localSeatId = this._localPlayer.getSeatID();
        if (localSeatId >= 2) {
            _posX = cc.winSize.width - x;
            _posY = cc.winSize.height - y;
            _dir = -1;
        }
        var player = cf.RoomMgr.getRoomPlayer(localSeatId);
        if (player) {
            for (var i = 0; i < 15; ++i) {
                var coin = new cc.Sprite("#effect_gold_001.png");
                coin.setPosition(_posX, _posY);

                var offsetX = 290 + cf.randomRange(0, 120);
                var offsetY = cf.randomRange(-60, 60);
                if (localSeatId >= 2) {
                    offsetX *= -1;
                    offsetY *= -1;
                    coin.setRotation(180);
                }
                if (localSeatId == 1 || localSeatId == 3)
                    _time = 1;
                coin.runAction(cc.repeatForever(ActionMgr.getAnimate("coin")));
                coin.runAction(cc.sequence(
                    cc.delayTime(i * 0.1),
                    cc.jumpBy(0.5, offsetX, offsetY, 50 * _dir, 1),
                    //cc.delayTime((20 - i) * 0.1+i * 0.1),
                    cc.spawn(cc.moveTo(_time, player._goldPos).easing(cc.easeBackIn()), cc.scaleTo(_time, 0.5)),
                    cc.callFunc(function () {
                        player.doGoldAction();
                        this.removeFromParent(true);
                        //cf.SoundMgr.playEffect(17);
                    }, coin)
                ));
                cf.BulletMgr.getBulletNode().addChild(coin);
            }
        }
        cf.SoundMgr.playEffect(15);
    },

    /**
     * 救济金奖励特效
     * @param pos
     */
    playDoleGold: function (pos) {
        var _posX = pos.x, _posY = pos.y + 200, _dir = 1;
        var localSeatId = this._localPlayer.getSeatID();
        if (localSeatId >= 2) {
            _posX = cc.winSize.width - pos.x;
            _posY = cc.winSize.height - pos.y - 200;
            _dir = -1;
        }
        var player = cf.RoomMgr.getRoomPlayer(localSeatId);
        if (player) {
            for (var i = 0; i < 15; ++i) {
                var coin = new cc.Sprite("#effect_gold_001.png");

                var offsetX = cf.randomRange(-50, 50);
                var offsetY = cf.randomRange(0, 60);
                if (localSeatId >= 2) {
                    offsetX *= -1;
                    offsetY *= -1;
                    coin.setRotation(180);
                }
                coin.setPosition(_posX + offsetX, _posY + offsetY);
                coin.setScale(0);
                coin.runAction(cc.repeatForever(ActionMgr.getAnimate("coin")));
                coin.runAction(cc.sequence(
                    cc.delayTime(i * 0.1),
                    cc.spawn(cc.jumpBy(0.6, 0, 0, _dir * 50, 1), cc.scaleTo(0.6, 1)),
                    cc.delayTime(0.3),
                    cc.spawn(cc.jumpTo(1, player._goldPos, _dir * 50, 1), cc.scaleTo(1, 0.5)),
                    cc.callFunc(function () {
                        player.doGoldAction();
                        this.removeFromParent(true);
                    }, coin)
                ));
                cf.BulletMgr.getBulletNode().addChild(coin);
            }
        }
        cf.SoundMgr.playEffect(15);
    },

    /**
     * 播放游戏语音
     * @param fishModel
     */
    playGameVoice: function (fishModel) {
        
        //判断是否有语言
        if(fishModel._fishType == cf.FishType.COMMON){
            if (fishModel.getMusic() === 0 && this._gameVoiceMaxId > 0) {
                if (this._gameVoiceId >= this._gameVoiceMaxId) {
                    this._gameVoiceId = 0;
                }
                var info = cf.Data.GAME_VOICE_DATA[this._gameVoiceId];
                cf.SoundMgr.playEffect(info.id, false, 2.5);
                this._gameVoiceId++;
            }
        }
    },

    playBossComing: function (fishModel, type) {

        var bossComing, appear, lubu, diaochan;

        var bg_Atlas, bg_Json;
        bg_Atlas = Res.GAME.skill_bg_Atlas;
        bg_Json = Res.GAME.skill_bg_Json;

        var boss_appear_Atlas, boss_appear_Json;
        boss_appear_Atlas = Res.GAME.boss_appear_Atlas;
        boss_appear_Json = Res.GAME.boss_appear_Json;

        
        cf.SoundMgr.stopMusic();
        cf.SoundMgr.playMusic(77, true);


        var bosstextAtlas, bosstextJson;
        bosstextJson = Res.GAME.boss_text_Json;
        bosstextAtlas = Res.GAME.boss_text_Atlas;


        if (type == cf.FishType.BOSS) {
           
            switch(fishModel._fishId){
                case 23:
                    var DiaoChanAtlas, DiaoChanJson;
                    DiaoChanJson = Res.GAME.DiaoChan_Json;
                    DiaoChanAtlas = Res.GAME.DiaoChan_Atlas;

                    cf.SoundMgr.playEffect(73, false);
                    bossComing = ActionMgr.createSpine(bg_Json, bg_Atlas);
                    appear = ActionMgr.createSpine(boss_appear_Json, boss_appear_Atlas);
                    diaochan = ActionMgr.createSpine(DiaoChanJson, DiaoChanAtlas);

                    bossComing.setAnimation(0, "DiaoChan", false);
                    bossComing.setPosition(cc.winSize.width/2, cc.winSize.height/2);
                    appear.setAnimation(1, "DiaoChan", false);
                    diaochan.setAnimation(2, "DiaoChan", false);
        
       
                    bossComing.addChild(diaochan);
                    bossComing.addChild(appear);
        
                    
                    bossComing.runAction(cc.sequence(
                        cc.delayTime(6),
                        cc.callFunc(function () {
                        //这里的this是bossComing
                        this.removeFromParent(true);
                        }, bossComing)
                    ));
                    this._sceneNode.runAction(cc.sequence(
                        cc.delayTime(13),
                        cc.callFunc(()=>{
                            var arr = [92, 93];
                            
                            var random = Math.floor(Math.random() * 2);
                            cf.SoundMgr.playEffect(arr[random], false);
                        }),
                        cc.delayTime(13),
                        cc.callFunc(()=>{
                            var arr = [92, 93];
                            
                            var random = Math.floor(Math.random() * 2);
                            cf.SoundMgr.playEffect(arr[random], false);
                        }),
                        cc.delayTime(13),
                        cc.callFunc(()=>{
                            var arr = [92, 93];
                            
                            var random = Math.floor(Math.random() * 2);
                            cf.SoundMgr.playEffect(arr[random], false);
                        })
                    ));
        
                    this._sceneNode.addChild(bossComing, cf.Zoder.UI);
                    break
                case 24:
                    var LuBuAtlas, LuBuJson;
                    LuBuJson = Res.GAME.LuBu_Json;
                    LuBuAtlas = Res.GAME.LuBu_Atlas;

                    cf.SoundMgr.playEffect(12, false);
                    //董卓音效
                   // cf.SoundMgr.playEffect(74, false, 2.5);
                    bossComing = ActionMgr.createSpine(bg_Json, bg_Atlas);
                    appear = ActionMgr.createSpine(boss_appear_Json, boss_appear_Atlas);
                    lubu = ActionMgr.createSpine(LuBuJson, LuBuAtlas);
                    
                    bossComing.setAnimation(0, "LuBu", false);
                    bossComing.setPosition(cc.winSize.width/2, cc.winSize.height/2);
                    appear.setAnimation(1, "LuBu", false);
                    lubu.setAnimation(2, "appear", false);
        
        
                    bossComing.addChild(lubu);
                    bossComing.addChild(appear);
        

                    bossComing.runAction(cc.sequence(
                        cc.delayTime(6),
                        cc.callFunc(function () {
                        //这里的this是bossComing
                        this.removeFromParent(true);
                    }, bossComing)
                    ));
                    
                    this._sceneNode.runAction(cc.sequence(
                        cc.delayTime(13),
                        cc.callFunc(()=>{
                            var arr = [90, 91];
                            
                            var random = Math.floor(Math.random() * 2);
                            cf.SoundMgr.playEffect(arr[random], false);
                        }),
                        cc.delayTime(13),
                        cc.callFunc(()=>{
                            var arr = [90, 91];
                            
                            var random = Math.floor(Math.random() * 2);
                            cf.SoundMgr.playEffect(arr[random], false);
                        }),
                        cc.delayTime(13),
                        cc.callFunc(()=>{
                            var arr = [90, 91];
                            
                            var random = Math.floor(Math.random() * 2);
                            cf.SoundMgr.playEffect(arr[random], false);
                        })
                    ));
                    this._sceneNode.addChild(bossComing, cf.Zoder.UI);
                    break;

                case 25:
                    var DongZhuoAtlas, DongZhuoJson;
                    DongZhuoJson = Res.GAME.DongZhuo_Json;
                    DongZhuoAtlas = Res.GAME.DongZhuo_Atlas;
                    //董卓音效
                    cf.SoundMgr.playEffect(74, false);

                    bossComing = ActionMgr.createSpine(bg_Json, bg_Atlas);
                    appear = ActionMgr.createSpine(boss_appear_Json, boss_appear_Atlas);
                    DongZhuo = ActionMgr.createSpine(DongZhuoJson, DongZhuoAtlas);
                    
                    bossComing.setAnimation(0, "DongZhuo", false);
                    bossComing.setPosition(cc.winSize.width/2, cc.winSize.height/2);
                    appear.setAnimation(1, "DongZhuo", false);
                    DongZhuo.setAnimation(2, "DongZhuo", false);
        
        
                    bossComing.addChild(DongZhuo);
                    bossComing.addChild(appear);
        

                    bossComing.runAction(cc.sequence(
                        cc.delayTime(6),
                        cc.callFunc(function () {
                        //这里的this是bossComing
                        this.removeFromParent(true);
                    }, bossComing)
                    ));
                    
                    this._sceneNode.runAction(cc.sequence(
                        cc.delayTime(13),
                        cc.callFunc(()=>{
                            var arr = [94, 95];
                            
                            var random = Math.floor(Math.random() * 2);
                            cf.SoundMgr.playEffect(arr[random], false);
                        }),
                        cc.delayTime(13),
                        cc.callFunc(()=>{
                            var arr = [94, 95];
                            
                            var random = Math.floor(Math.random() * 2);
                            cf.SoundMgr.playEffect(arr[random], false);
                        }),
                        cc.delayTime(13),
                        cc.callFunc(()=>{
                            var arr = [94, 95];
                            
                            var random = Math.floor(Math.random() * 2);
                            cf.SoundMgr.playEffect(arr[random], false);
                        })
                    ));
                    this._sceneNode.addChild(bossComing, cf.Zoder.UI);
                    break;
            }
          
        }
        else {
            //bossComing = ActionMgr.createSpine(Res.GAME.wbossComingAni_Json, Res.GAME.wbossComingAni_Atlas);
        }
        
        
       
    },

    playBlink: function (bShow, color) {
        if (this._blinkEffect == null) {
            this._blinkEffect = new cc.Sprite("#shake.png");
            var winSize = cc.winSize;
            this._blinkEffect.setScaleX(winSize.width / this._blinkEffect.width);
            this._blinkEffect.setScaleY(winSize.height / this._blinkEffect.height);
            this._blinkEffect.setPosition(winSize.width / 2, winSize.height / 2);
            this._sceneNode.addChild(this._blinkEffect, cf.Zoder.UI - 1);
        }

        this._blinkEffect.setVisible(bShow);
        if (!bShow) {
            this._blinkEffect.stopAllActions();
            this._blinkEffect.setOpacity(255);
        } else {
            this._blinkEffect.setColor(color);
            this._blinkEffect.runAction(cc.repeatForever(cc.sequence(cc.fadeTo(0.4, 100), cc.fadeTo(0.4, 255))));
        }
    },

    /**
     * 金币粒子特效
     */
    pokerParticle: function () {
        //粒子特效
        var goldParticle = new cc.ParticleSystem(Res.LOBBY.get_gold_plist);
        goldParticle.setPosition(cc.winSize.width / 2, cc.winSize.height / 2 + 90);
        goldParticle.setAutoRemoveOnFinish(true);
        this._sceneNode.addChild(goldParticle, cf.Zoder.UI);
    },

    

    /**
     * 弹头特效
     * @param type 数量
     * @param score 获得分数
     */
    doMissileAction: function (type, score) {
        var self = this;
        var node = new cc.Node();
        this._sceneNode.addChild(node, cf.Zoder.FISH - 1);

        var maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 150));
        node.addChild(maskLayer, 0);

        self.addMissileEffect(cc.p(cc.winSize.width / 2, cc.winSize.height / 2), 1);
        node.runAction(cc.sequence(
            cc.delayTime(0.5),
            cc.callFunc(function () {
                self.addRewardLabel(score, 50);
                self.pokerParticle();
            }),
            cc.delayTime(2),
            cc.callFunc(function () {
                this.removeFromParent(true);
            }, node)
        ));
    },

    /**
     * 创建弹头
     * @param pos 爆炸位置
     * @param time 延迟爆炸时间
     */
    addMissileEffect: function (pos, time) {
        var self = this;
        var boomAction = new cc.Sprite("#missile_001.png");
        boomAction.setScale(1.2);
        boomAction.setPosition(pos.x, pos.y + cc.winSize.height / 2 + 200);
        this._sceneNode.addChild(boomAction, cf.Zoder.BULLET - 1);
        boomAction.runAction(cc.sequence(
            cc.moveTo(0.5, pos),
            cc.callFunc(function () {
                //振动
                cf.RoomMgr.shakeRoom(5, 15, 0.05);
                cf.SoundMgr.playEffect(43, false, 4.5);
                this.removeFromParent();
                self.boomGold();
            }, boomAction)
        ));

    },

    boomGold: function () {
        //爆炸特效
        var boomEffect = new cc.Sprite("#boom_001.png");
        //这里200是标准
        boomEffect.setScale(4);
        boomEffect.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        boomEffect.setOpacity(255);
        this._sceneNode.addChild(boomEffect, cf.Zoder.BULLET - 1);

        boomEffect.runAction(cc.sequence(
            ActionMgr.getAnimate("boom"),
            cc.fadeOut(0.1),
            cc.callFunc(function () {
                this.removeFromParent(true);
            }, boomEffect)
        ));

    },

    /**
     * 展示炮倍全部解锁提示
     * @param pos
     * @returns {boolean}
     */
    showUnlockTip: function (pos) {
        /*cf.unlockShow = false;
        cf.layerHasShowInGame = true;
        var tip = new UIUnlockShow(pos);
        this._sceneNode.addChild(tip, cf.Zoder.UI);*/
    },

    /**
     * 龙珠掉落
     * @param fishPos
     * @param msg
     */
    playDropDragonBall: function (fishId,fishPos, msg) {
        var self = this;
        
        
        if (msg.nLzNum >= 1) {
            
            var _posX = fishPos.x, _posY = fishPos.y;
            
            var localSeatId = this._localPlayer.getSeatID();
            if (localSeatId >= 2) {
                _posX = cc.winSize.width - _posX;
                _posY = cc.winSize.height - _posY;
                
            }

            var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
            if (player) {
                
                //创建龙珠
                var spDragonBall = new cc.Node();
                spDragonBall.setScale(1);
                spDragonBall.setPosition(_posX, _posY);
                this._sceneNode.addChild(spDragonBall, 0);

                
                
                var dicethr = new cc.Sprite("#dicethrow_001.png");
                dicethr.runAction(cc.repeatForever(ActionMgr.getAnimate("dicethrow")));
                spDragonBall.addChild(dicethr);
                var _targetPos = player._curPos;
                var _diceX = player._curPos.x, _diceY= -player._curPos.y+300;
                var _dice1X = player._curPos.x +200, _dice1Y= -player._curPos.y+200;
                var _dice2X = player._curPos.x -200, _dice2Y= -player._curPos.y+200;
                if (localSeatId >= 2) {
                    _dice1X = player._curPos.x -200;
                    _dice2X = player._curPos.x +200;
                    _diceY = player._curPos.y + 300;
                    _dice1Y =  player._curPos.y + 200;
                    _dice2Y = player._curPos.y + 200;
                }
                    //判断下是否是自己
                    // if (localSeatId == msg.nSeatID) {
                    //     //飞向龙珠UI
                    //     _targetPos = cc.p(cc.winSize.width / 2, cc.winSize.height-600);
                    // }
                    //执行特效动作
                    if(localSeatId == msg.nSeatID){
                        
                        var time = setTimeout(()=>{
                            this.triggerDice(fishId, _posX, _posY);
                            clearTimeout(time);
                        }, 500);
                    }
                   

                    var speed = this.getFlyTime(_posX, _posY, _targetPos.x, _targetPos.y, 500);
                    var moveAction = cc.moveTo(speed, _targetPos).easing(cc.easeBackIn());
                    spDragonBall.runAction(cc.sequence(
                        cc.scaleTo(0.45, 1).easing(cc.easeBackOut()),
                        cc.delayTime(1),
                        
                        moveAction,
                        cc.callFunc(function () {
                            //this是道具本身
                            this.removeFromParent(true);
                            
                            //只有自己获得才刷新UI
                            var spineJson, spineAtlas;
                            spineJson = Res.GAME.dice_Json;
                            spineAtlas = Res.GAME.dice_Atlas;
                            
                            var dice = ActionMgr.createSpine(spineJson,spineAtlas,1 );
                            
                            if(self._sceneNode.getChildByTag("dice2")){
                                
                                dice.setTag("dice");
                                dice.setPosition(_diceX, _diceY);
                            }else if(self._sceneNode.getChildByTag("dice1")){
                                
                                dice.setTag("dice2");
                                dice.setPosition(_dice2X, _dice2Y);
                            }
                            else if(self._sceneNode.getChildByTag("dice")){
                               
                                dice.setTag("dice1");
                                dice.setPosition(_dice1X, _dice1Y);
                            }else{
                                dice.setTag("dice");
                                dice.setPosition(_diceX, _diceY);
                            }
                            
                           
                            if(msg.attrBall[msg.attrBall.length-1] == -2){
                                // dice.setAnimation(0, "dice1", false);
                                var count = msg.attrBallHit[0] +2;
                                var ani = "dice"+count;
                                dice.setAnimation(0, ani, false);
                            }else if(msg.attrBall[msg.attrBall.length-1] == -1){
                                
                                dice.setAnimation(0, "dice1", false);

                            }else{
                                var count = (msg.attrBall[msg.attrBall.length-1]+2);
                                var animation = "dice"+count;
                                dice.setAnimation(0, animation, false);
                            }
                            cf.SoundMgr.playEffect(75,false);
                            self._sceneNode.addChild(dice);
                            if(localSeatId == msg.nSeatID){
                                var _skillNoticeBack = new cc.Sprite("#notice_background.png");
                                _skillNoticeBack.setAnchorPoint(0.5, 1);
                                _skillNoticeBack.setScaleX(8);
                                _skillNoticeBack.setScaleY(5);
                                // this._skillNoticeBack.setScaleX(15);
                                // this._skillNoticeBack.setScaleY(9);
                                _skillNoticeBack.setPosition(cc.winSize.width / 2, cc.winSize.height/2-20);
                                self._sceneNode.addChild(_skillNoticeBack);
        
                                var _skillNotice = new cc.Sprite("#break_notice.png");
                                _skillNotice.setAnchorPoint(0.5, 1);
                                _skillNotice.setPosition(cc.winSize.width / 2, cc.winSize.height/2-30);
                                self._sceneNode.addChild(_skillNotice);
        
                                _skillNoticeBack.runAction(cc.sequence(
                                    cc.delayTime(3),
                                    cc.callFunc(()=>{
                                        _skillNotice.removeFromParent();
                                        _skillNoticeBack.removeFromParent();
        
                                    })
                                ))
                            }
                            dice.setCompleteListener(()=>{
                                dice.removeFromParent();

                                if (localSeatId == msg.nSeatID) {
                                    if (msg.nLzNum >= 7) {
        
                                        if(msg.nJackpot == 0){
                                            //打开龙珠
                                            cf.SoundMgr.playEffect(88,false);
                                            self.showDragonEffect(msg.nLzGold);
        
                                        }else{
                                            cf.SoundMgr.playEffect(53,false);
                                            self.showDragonJPEffect(msg.nLzGold);
                                        }
                                       
                                                cf.dispatchEvent(BaseRoom.DoEvent, {
                                                    flag: BaseRoom.EventFlag.UPDATE_DRAGON_BALL_UI,
                                                     ballNum:  msg.nLzNum,
                                                     attrBall: msg.attrBall,
                                                     attrBallHit: msg.attrBallHit,
                                                     colletAttrBall: msg.colletAttrBall
                                                     
                                                 });
        
                                    } else {
                                       
                                        //刷新UI
                                        cf.dispatchEvent(BaseRoom.DoEvent, {
                                            flag: BaseRoom.EventFlag.UPDATE_DRAGON_BALL_UI,
                                            ballNum: msg.nLzNum,
                                            attrBall: msg.attrBall,
                                            attrBallHit: msg.attrBallHit,
                                            colletAttrBall: msg.colletAttrBall
                                        });
                                        
                                       
                                    }
                                }

                    


                            });
                        },spDragonBall)
                            
                        
                    ));


                
               
            }
        }
    },

    triggerDice: function(fishId, _posX, _posY){
        var self = this;
        var iconJson, iconAtlas;
        
        var statusJson, statusAtlas;
        statusJson = Res.GAME.status_Json;
        statusAtlas = Res.GAME.status_Atlas;
        
        var statusBgJson, statusBgAtlas;
        statusBgJson = Res.GAME.statusBg_Json;
        statusBgAtlas = Res.GAME.statusBg_Atlas;

        
        
        switch(fishId){
            case 16:
                iconJson = Res.GAME.lv16_Json;
                iconAtlas = Res.GAME.lv16_Atlas;
                var status = ActionMgr.createSpine(statusJson, statusAtlas, 1);
                status.setAnimation(0, "captive", false);
                status.setPosition(_posX, _posY);
                
                var statusBg = ActionMgr.createSpine(statusBgJson, statusBgAtlas, 1);
                statusBg.setAnimation(2, "die", false);
                statusBg.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(statusBg, -1);

                var icon = ActionMgr.createSpine(iconJson,iconAtlas, 1 );
                icon.setAnimation(1, "01", false);
                icon.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(icon, -1);
                
                    self._sceneNode.addChild(status);
                    icon.setCompleteListener(()=>{
                    status.removeFromParent();  
                });
            break;
            case 17:
            
                iconJson = Res.GAME.lv17_Json;
                iconAtlas = Res.GAME.lv17_Atlas;
                var status = ActionMgr.createSpine(statusJson, statusAtlas, 1);
                status.setAnimation(0, "captive", false);
                status.setPosition(_posX, _posY);
                
                var statusBg = ActionMgr.createSpine(statusBgJson, statusBgAtlas, 1);
                    statusBg.setAnimation(2, "die", false);
                    statusBg.setPosition(status.width/2-130, status.height/2-43);
                    status.addChild(statusBg, -1);

                var icon = ActionMgr.createSpine(iconJson,iconAtlas, 1 );
                icon.setAnimation(1, "captive", false);
                icon.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(icon, -1);
                
                    self._sceneNode.addChild(status);
                    icon.setCompleteListener(()=>{
                    status.removeFromParent();  
                });
            break;
            case 18:
                iconJson = Res.GAME.lv18_Json;
                iconAtlas = Res.GAME.lv18_Atlas;
                var status = ActionMgr.createSpine(statusJson, statusAtlas, 1);
                status.setAnimation(0, "die", false);
                status.setPosition(_posX, _posY);
                
                var statusBg = ActionMgr.createSpine(statusBgJson, statusBgAtlas, 1);
                    statusBg.setAnimation(2, "die", false);
                    statusBg.setPosition(status.width/2-130, status.height/2-43);
                    status.addChild(statusBg, -1);

                var icon = ActionMgr.createSpine(iconJson,iconAtlas, 1 );
                icon.setAnimation(1, "die", false);
                icon.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(icon, -1);
                
                    self._sceneNode.addChild(status);
                    icon.setCompleteListener(()=>{
                    status.removeFromParent();  
                });
            break;

            case 19:
                iconJson = Res.GAME.lv19_Json;
                iconAtlas = Res.GAME.lv19_Atlas;
                var status = ActionMgr.createSpine(statusJson, statusAtlas, 1);
                status.setAnimation(0, "die", false);
                status.setTimeScale(0.7); 
                status.setPosition(_posX, _posY);
                
                var statusBg = ActionMgr.createSpine(statusBgJson, statusBgAtlas, 1);
                statusBg.setAnimation(2, "die", false);
                statusBg.setTimeScale(0.7); 
                statusBg.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(statusBg, -1);

                var icon = ActionMgr.createSpine(iconJson,iconAtlas, 1 );
                icon.setAnimation(1, "die", false);
                icon.setTimeScale(0.7); 
                icon.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(icon, -1);
        
                    self._sceneNode.addChild(status);
                    icon.setCompleteListener(()=>{
                    status.removeFromParent();  
                });
            break;
            case 20:
                iconJson = Res.GAME.lv20_Json;
                iconAtlas = Res.GAME.lv20_Atlas;
                var status = ActionMgr.createSpine(statusJson, statusAtlas, 1);
                status.setAnimation(0, "die", false);
                status.setTimeScale(0.7); 
                status.setPosition(_posX, _posY);
                
                var statusBg = ActionMgr.createSpine(statusBgJson, statusBgAtlas, 1);
                statusBg.setAnimation(2, "die", false);
                statusBg.setTimeScale(0.7); 
                statusBg.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(statusBg, -1);

                var icon = ActionMgr.createSpine(iconJson,iconAtlas, 1 );
                icon.setAnimation(1, "die", false);
                icon.setTimeScale(0.7); 
                icon.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(icon, -1);
        
                self._sceneNode.addChild(status);
                    icon.setCompleteListener(()=>{
                    status.removeFromParent();  
                });
            break;

            case 21:
                iconJson = Res.GAME.lv21_Json;
                iconAtlas = Res.GAME.lv21_Atlas;
                var status = ActionMgr.createSpine(statusJson, statusAtlas, 1);
                status.setAnimation(0, "die", false);
                status.setTimeScale(0.7);
                status.setPosition(_posX, _posY);
                
                var statusBg = ActionMgr.createSpine(statusBgJson, statusBgAtlas, 1);
                statusBg.setAnimation(2, "die", false);
                statusBg.setTimeScale(0.7);
                statusBg.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(statusBg, -1);

                var icon = ActionMgr.createSpine(iconJson,iconAtlas, 1 );
                icon.setAnimation(1, "die", false);
                icon.setTimeScale(0.7);
                icon.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(icon, -1);
                
                self._sceneNode.addChild(status);  
                icon.setCompleteListener(()=>{
                    status.removeFromParent();  
                });
            break;
            case 22:
                
                iconJson = Res.GAME.lv22_Json;
                iconAtlas = Res.GAME.lv22_Atlas;

                var status = ActionMgr.createSpine(statusJson, statusAtlas, 1);
                status.setAnimation(0, "captive", false);
                status.setPosition(_posX, _posY);
                
                var statusBg = ActionMgr.createSpine(statusBgJson, statusBgAtlas, 1);
                statusBg.setAnimation(2, "die", false);
                statusBg.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(statusBg, -1);

                var icon = ActionMgr.createSpine(iconJson,iconAtlas, 1 );
                icon.setAnimation(1, "captive", false);
                icon.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(icon, -1);
                
                    self._sceneNode.addChild(status);
                    icon.setCompleteListener(()=>{
                    status.removeFromParent();  
                });
                        
                        

                   
                
            

            break;
            case 23:
                
                iconJson = Res.GAME.lv23_Json;
                iconAtlas = Res.GAME.lv23_Atlas;

                
                self._sceneNode.runAction(cc.sequence(
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        var status = ActionMgr.createSpine(statusJson, statusAtlas, 1);
                        status.setAnimation(0, "captive_b", false);
                        status.setPosition(cc.winSize.width/2, cc.winSize.height/2);
                        status.setTimeScale(1);

                        var statusBg = ActionMgr.createSpine(statusBgJson, statusBgAtlas, 1);
                        statusBg.setAnimation(2, "die", false);
                        statusBg.setPosition(status.width/2-130, status.height/2-43);
                        statusBg.setTimeScale(1);
                        status.addChild(statusBg, -1);

                        var icon = ActionMgr.createSpine(iconJson,iconAtlas, 1 );
                        icon.setAnimation(1, "DiaoXain", false);
                        icon.setTimeScale(1);
                        icon.setPosition(status.width/2-130, status.height/2-43);
                        status.addChild(icon, -1);

                        self._sceneNode.addChild(status);  
                
                
                        icon.setCompleteListener(()=>{
                            status.removeFromParent();  
                        });
                    })
                ))
                        
                        

                   
                
            

            break;
            case 24:
                
                iconJson = Res.GAME.LuBu_Json;
                iconAtlas = Res.GAME.LuBu_Atlas;

                self._sceneNode.runAction(cc.sequence(
                    cc.delayTime(1),
                    cc.callFunc(()=>{

                var status = ActionMgr.createSpine(statusJson, statusAtlas, 1);
                status.setAnimation(0, "die_b", false);
                status.setPosition(cc.winSize.width/2, cc.winSize.height/2);
                status.setTimeScale(0.4);

                var statusBg = ActionMgr.createSpine(statusBgJson, statusBgAtlas, 1);
                statusBg.setAnimation(2, "die", false);
                statusBg.setPosition(status.width/2-130, status.height/2-43);
                statusBg.setTimeScale(0.4);
                status.addChild(statusBg, -1);

                var icon = ActionMgr.createSpine(iconJson,iconAtlas, 1 );
                icon.setAnimation(1, "die", false);
                icon.setTimeScale(0.4);
                icon.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(icon, -1);
                
                
                self._sceneNode.addChild(status);  
                
                
                icon.setCompleteListener(()=>{
                    status.removeFromParent();  
                });
            
            })
            ))
            break;
            case 25:
                
                iconJson = Res.GAME.DongZhuo_Json;
                iconAtlas = Res.GAME.DongZhuo_Atlas;

                self._sceneNode.runAction(cc.sequence(
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                var status = ActionMgr.createSpine(statusJson, statusAtlas, 1);
                status.setAnimation(0, "die_b", false);
                status.setPosition(cc.winSize.width/2, cc.winSize.height/2);
                status.setTimeScale(0.4);

                var statusBg = ActionMgr.createSpine(statusBgJson, statusBgAtlas, 1);
                statusBg.setAnimation(2, "die", false);
                statusBg.setPosition(status.width/2-130, status.height/2-43);
                statusBg.setTimeScale(0.4);
                status.addChild(statusBg, -1);

                var icon = ActionMgr.createSpine(iconJson,iconAtlas, 1 );
                icon.setAnimation(1, "die", false);
                icon.setTimeScale(0.4);
                icon.setPosition(status.width/2-130, status.height/2-43);
                status.addChild(icon, -1);
                
                
                self._sceneNode.addChild(status);  
                
                
                icon.setCompleteListener(()=>{
                    status.removeFromParent();  
                });
            })
            ))
            break;
        
        }
    },
    /**
     * 
     * 顯示彩金魚討取動畫
     */
    showBonusFishAnimation: function(fishModel, fishPos, msg){
        var spineJson, spineAtlas;
        spineJson = Res.GAME.status_Json;
        spineAtlas = Res.GAME.status_Atlas;

        var spineJson, spineAtlas;
        spineJson = Res.GAME.status_Json;
        spineAtlas = Res.GAME.status_Atlas;

     
        var status =  ActionMgr.createSpine(spineJson,spineAtlas,1);
        //status.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        status.setAnimation(0, "die", false);
        spDragonBall.addChild(status, cf.Zoder.BK);
        status.setCompleteListener(()=>{
            var dice = new cc.Sprite("#dice1.png");
            dice.setPosition(status.width/2, status.height/2);
            spDragonBall.addChild(status, cf.Zoder.BK);
        });

    },

    /**
     * 獲得龍珠JP金幣
     * @param money 金幣數
     */
    showDragonJPEffect: function(money){

        var self = this;
        var bgJson, bgAtlas;
        bgJson = Res.GAME.jpbg_Json;
        bgAtlas = Res.GAME.jpbg_Atlas;

        var sparkJson, sparkAtlas;
        sparkJson = Res.GAME.spark_Json;
        sparkAtlas = Res.GAME.spark_Atlas;

        var rewardJson, rewardAtlas;
        rewardJson = Res.GAME.jpreward_Json;
        rewardAtlas = Res.GAME.jpreward_Atlas;

        var fortType = cf.PlayerMgr.getLocalPlayer().getFortType();
        var fortnum = cf.Data.CANNON_LEVEL_DATA[fortType].cannonlevel;
        
        var num = money / fortnum;
        
        // var maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 220));
        // maskLayer.setName("dragon_mask");
        // maskLayer.setVisible(true);
        // self._sceneNode.addChild(maskLayer, cf.Zoder.UI + 1);
        
        
        
        var bg =  ActionMgr.createSpine(bgJson,bgAtlas,1);
        bg.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        bg.setTag("dragonBk");
        bg.setAnimation(0, "jp1", false);
        self._sceneNode.addChild(bg);

            
        bg.addAnimation(0, "jp2", false);
        
        bg.addAnimation(0, "jp3", false);
       
             bg.runAction(cc.sequence(
                cc.delayTime(3),
                cc.callFunc(()=>{
                    var spBK = new cc.Sprite("#brust_001.png");
                    spBK.setAnchorPoint(0.5, 1);
                    spBK.setScale(4);
                    spBK.setPosition(0, 300);
                    spBK.runAction(cc.sequence(
                    cc.repeat(ActionMgr.getAnimate("brust"), 1),
                    cc.callFunc(()=>{
                        spBK.removeFromParent();
                
                    })
                    
            ))
                    
                bg.addChild(spBK, 1);
                var reward = ActionMgr.createSpine(rewardJson,rewardAtlas,1);
                    reward.setAnimation(1, "5", false);
                    reward.setTag("reward");
                    bg.addChild(reward, 3);

                    var spark = ActionMgr.createSpine(sparkJson,sparkAtlas,1);
                spark.setBlendFunc( cc.SRC_ALPHA,cc.ONE);  
                spark.setAnimation(4, "jp3", false);
                bg.addChild(spark, 4);

                }),
                cc.delayTime(0.5),
                cc.callFunc(()=>{
                    self.showOpenDragonBall(money);

                    var moneya = new cc.Sprite("#jpmoney_001.png");
                    moneya.setAnchorPoint(0.5, 1);
                    moneya.setScale(2);
                    moneya.setPosition(0, 350);
                        bg.addChild(moneya, 1);
                        moneya.runAction(cc.sequence(
                        cc.repeat(ActionMgr.getAnimate("jpmoney"), 1),
                        cc.callFunc(()=>{
                            moneya.removeFromParent();
                    
                        })
                        ))

                    var jpmoney1 = new cc.Sprite("#jpmoney2_001.png");
                    jpmoney1.setAnchorPoint(0.5, 1);
                    jpmoney1.setScale(2);
                    jpmoney1.setPosition(0, 300);
                    bg.addChild(jpmoney1, 1);

                    var jpmoney = new cc.Sprite("#jpmoney2_001.png");
                    jpmoney.setAnchorPoint(0.5, 1);
                    jpmoney.setScale(2);
                    jpmoney.setPosition(0, 300);
                    bg.addChild(jpmoney, 1);

                    
                    jpmoney.runAction(cc.sequence(
                        cc.repeat(ActionMgr.getAnimate("jpmoney2"),2),
                        cc.callFunc(function(){
                             jpmoney.removeFromParent();

                        })
                       
                    ))

                    jpmoney1.runAction(cc.sequence(
                        cc.delayTime(1),
                       
                        cc.repeat(ActionMgr.getAnimate("jpmoney2"),2),
                        cc.callFunc(function(){
                             jpmoney1.removeFromParent();

                        })
                       
                    ))

                    

                    

                })

             ))
            
       

       
           
           

        

    },

    /**
     * 打开龙珠获得金币
     * @param money 金币数
     */
    showDragonEffect: function (money) {
        var self = this;

        var bgJson, bgAtlas;
        bgJson = Res.GAME.jpbg_Json;
        bgAtlas = Res.GAME.jpbg_Atlas;

        var rewardJson, rewardAtlas;
        rewardJson = Res.GAME.jpreward_Json;
        rewardAtlas = Res.GAME.jpreward_Atlas;

        var sparkJson, sparkAtlas;
        sparkJson = Res.GAME.spark_Json;
        sparkAtlas = Res.GAME.spark_Atlas;

        var fortType = cf.PlayerMgr.getLocalPlayer().getFortType();
        var fortnum = cf.Data.CANNON_LEVEL_DATA[fortType].cannonlevel;
        
      

        var num = Math.floor(money / fortnum);
        
        // var maskLayer = new cc.LayerColor(cc.color(0, 0, 0, 220));
        // maskLayer.setName("dragon_mask");
        // maskLayer.setVisible(true);
        // self._sceneNode.addChild(maskLayer, cf.Zoder.UI + 1);
      
        
        var bg =  ActionMgr.createSpine(bgJson,bgAtlas,1);
        bg.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        bg.setTag("dragonBk");
        bg.setAnimation(0, "jp1", false);
        self._sceneNode.addChild(bg);

                
        bg.addAnimation(0, "jp2", false);
        
        bg.addAnimation(0, "jp3", false);
       

        bg.runAction(cc.sequence(
                
            cc.delayTime(3),
            cc.callFunc(()=>{
               
                if(num >= 1 && num <= 30){
                    var str = "1";
                }else if(num >= 31 && num <= 60){
                    var str = "2";
                }else if(num >= 61 && num <= 80){
                    var str = "3";
                }else if(num >= 81){
                    var str = "4";
                }
                
                var reward = ActionMgr.createSpine(rewardJson,rewardAtlas,1);
                reward.setAnimation(1, str, false);
                reward.setTag("reward");
                bg.addChild(reward, 3);

                var spark = ActionMgr.createSpine(sparkJson,sparkAtlas,1);
                spark.setBlendFunc( cc.SRC_ALPHA,cc.ONE);  
                spark.setAnimation(4, "jp3", false);
                bg.addChild(spark, 4);
                
            }),
                cc.delayTime(0.5),
                cc.callFunc(()=>{
                    self.showOpenDragonBall(money);
          
                    var brust = new cc.Sprite("#jpmoney_001.png");
                    brust.setAnchorPoint(0.5, 1);
                    brust.setScale(2);
                    brust.setPosition(0, 350);
                        bg.addChild(brust, 1);
                        brust.runAction(cc.sequence(
                        cc.repeat(ActionMgr.getAnimate("jpmoney"), 1),
                        cc.callFunc(()=>{
                            brust.removeFromParent();
                    
                        })
                        ))

                    

                })
    
        ))
       
    },

    /**
     * 龙珠金币转动
     * @param money 金币数
     */
    showOpenDragonBall: function (money) {
        var self = this;
        var scrollsJson, scrollsAtlas;
        scrollsJson = Res.GAME.scrolls_Json;
        scrollsAtlas = Res.GAME.scrolls_Atlas;
       

        var scroll = ActionMgr.createSpine(scrollsJson,scrollsAtlas,1);
        scroll.setPosition(cc.winSize.width / 2, cc.winSize.height / 2+5);
        scroll.setAnimation(2, "jp3", false);
        this._sceneNode.addChild(scroll, 2);
       

        var numStr = money.toString().split('');
        var _numLen = numStr.length;
        //单数字大小72
        var totalW = (72+5)*(_numLen-1);

        self._bossRateNum = [];
        self._bossCount = [];
        for (var i = 0; i < _numLen; i++) {
            self._bossRateNum[i] = new cc.LabelBMFont("0", Res.LOBBY.font_resource_090);
            self._bossRateNum[i].setScale(2);
            self._bossRateNum[i].setTag("jpcoin"+i);
            self._bossRateNum[i].setPosition( (scroll.width-totalW-750)/2+(72+5)*i, scroll.height- 830 / 2);
            scroll.addChild(self._bossRateNum[i],4);
            self._bossCount.push(cf.randomRange(0,9));
        }

        var updateBossRate = function (dt) {
            for(var i = 0; i < _numLen; ++i){
                self._bossCount[i]++;
                if(self._bossCount[i] >= 9){
                    self._bossCount[i] = 0;
                }
                self._bossRateNum[i].setString(self._bossCount[i]);
            }
        };

        scroll.schedule(updateBossRate,0.1);

        // var maskLayer = new cc.Sprite("#resource_067_9.png");
        // maskLayer.setScaleX(100);
        // maskLayer.setScaleY(100);
        // maskLayer.setColor(cc.color.BLACK);
        // maskLayer.setOpacity(220);
        // maskLayer.setPosition(baseSprite.width / 2, baseSprite.height / 2);
        // maskLayer.setVisible(false);
        // baseSprite.addChild(maskLayer, 1);

        // var spRotateEffect01 = new cc.Sprite("#reward_back_effect.png");
        // spRotateEffect01.setPosition(baseSprite.width / 2, baseSprite.height / 2-10);
        // spRotateEffect01.runAction(cc.repeatForever(cc.rotateBy(5,360)));
        // spRotateEffect01.setScale(2);
        // baseSprite.addChild(spRotateEffect01, 3);

        // var spRotateEffect02 = new cc.Sprite("#reward_front_effect.png");
        // spRotateEffect02.setPosition(baseSprite.width / 2, baseSprite.height / 2-10);
        // spRotateEffect02.runAction(cc.repeatForever(cc.rotateBy(3,-360)));
        // spRotateEffect02.setScale(2);
        // baseSprite.addChild(spRotateEffect02, 3);

        // var spLightEffect = new cc.Sprite("#dragonLight_001.png");
        // spLightEffect.setPosition(baseSprite.width / 2, baseSprite.height / 2+80);
        // spLightEffect.setVisible(false);
        // spLightEffect.setScale(1.5);
        // baseSprite.addChild(spLightEffect, 3);

        scroll.runAction(cc.sequence(
            ActionMgr.createDelayBounce(0.1, 1.2, 0.25, 0.2, 0.1, 0),
            cc.delayTime(1),
            cc.callFunc(function () {
                //cf.SoundMgr.playEffect(45, false, 3);
                // var goldParticle01 = new cc.ParticleSystem(Res.LOBBY.gold_boom_plist);
                // goldParticle01.setPosition(scroll.width / 2, scroll.height / 2);
                // goldParticle01.setAutoRemoveOnFinish(true);
                // scroll.addChild(goldParticle01,2);

               // maskLayer.setVisible(true);
                // spLightEffect.setVisible(true);
                // spLightEffect.runAction(cc.sequence(
                //     ActionMgr.getAnimate("dragonLight"),
                //     cc.fadeOut(0)
                // ));
                self.shakeRoom(1, 2, 0.05);
                scroll.unschedule(updateBossRate,1);
                for (var i = 0; i < _numLen; i++) {
                    self._bossRateNum[i].setScale(2);
                    self._bossRateNum[i].runAction(cc.scaleTo(2,2).easing(cc.easeBackOut()));
                    self._bossRateNum[i].setString(""+numStr[i]);
                }
            }),
            // cc.callFunc(function () {
            //     self._sceneNode.getChildByName("dragon_mask").removeFromParent();
            // }),
            // cc.delayTime(1.5),
            // cc.callFunc(function () {
            //     // var goldParticle02 = new cc.ParticleSystem(Res.LOBBY.get_gold_plist);
            //     // goldParticle02.setPosition(scroll.width / 2, scroll.height / 2);
            //     // goldParticle02.setAutoRemoveOnFinish(true);
            //    //scroll.addChild(goldParticle02,5);
            // }),
            cc.delayTime(2),
            // cc.callFunc(function () {
            //     maskLayer.setVisible(false);
            // }),
           
          //  cc.scaleTo(0.2, 0),
            cc.callFunc( ()=> {
                for(var a = 0 ; a < self._bossRateNum.length; a++){
                    scroll.getChildByTag("jpcoin"+a).runAction(cc.sequence(
                        cc.spawn(cc.tintTo(3, 0, 0, 0),cc.fadeOut(3)),
                        cc.callFunc(()=>{
                            if(scroll.getChildByTag("jpcoin"+a)){
                                scroll.getChildByTag("jpcoin"+a).removeFromParent();
                            }
                            
                        })
                    ));
                }
                

                scroll.runAction(cc.sequence(
                    cc.spawn(cc.tintTo(3, 0, 0, 0),cc.fadeOut(3)),
                    cc.callFunc(()=>{
                        scroll.removeFromParent();
                    })
                ));
                
                self._sceneNode.getChildByTag("dragonBk").runAction(cc.sequence(
                    cc.spawn(cc.tintTo(3, 0, 0, 0),cc.fadeOut(3)),
                    cc.callFunc(()=>{
                        self._sceneNode.getChildByTag("dragonBk").removeFromParent();
                    })
                ));
                self._sceneNode.getChildByTag("dragonBk").getChildByTag("reward").runAction(cc.sequence(
                    cc.spawn(cc.tintTo(3, 0, 0, 0),cc.fadeOut(3)),
                    cc.callFunc(()=>{
                        self._sceneNode.getChildByTag("dragonBk").getChildByTag("reward").removeFromParent();
                    })
                ));

            })
        ))
    },

    shakeRoom: function (times, moveOffset, amplitude) {
        var action = this._sceneNode.getActionByTag(88);
        if (action != null && !action.isDone()) {
            return;
        }

        var pos = this._sceneNode.getPosition();
        var _shake = cc.sequence(
            cc.repeat(cc.sequence(
                cc.moveTo(amplitude, pos.x + moveOffset, pos.y),
                cc.moveTo(amplitude, pos.x - moveOffset, pos.y),
                cc.moveTo(amplitude, pos.x, pos.y + moveOffset),
                cc.moveTo(amplitude, pos.x, pos.y - moveOffset)
            ), times),
            cc.callFunc(function () {
                //这里的this是mapArray
                this.stopAllActions();
                this.setPosition(0, 0);
            }, this._sceneNode)
        );
        _shake.setTag(88);
        this._sceneNode.runAction(_shake);
    },

    BonusEffect:function(fishPos, fishModel, msg){
        var self = this;
        console.log(fishPos,fishModel,msg);
        var localSeatId = this._localPlayer.getSeatID();
        var _fishposX = fishPos.x, _fishposY = fishPos.y;
        var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
        // var rewardScoreNum = msg.nScore;

        var _posX = player._curPos.x , _posY = player._curPos.y;

        var seatId = cf.convertSeatId(localSeatId);
        var convert = cf.convertSeatId(msg.nSeatID);
        
        switch(seatId){
            case 0 :
                
            if(localSeatId == msg.nSeatID){
                _posX = player._curPos.x;
                _posY = player._curPos.y+100;
                _fishposX = fishPos.x;
                _fishposY = fishPos.y;

                console.log("1");

            }else{
                if(convert >= 2){

                    _posY = cc.winSize.width - player._curPos.y+100;
                    _posX = player._curPos.x;
                    _fishposX =  fishPos.x;
                    _fishposY = cc.winSize.height - _fishposY; 
                    console.log("2");

                }else{
                    
                    _posX = player._curPos.x;
                    _posY = player._curPos.y+100;
                    _fishposX = cc.winSize.width -fishPos.x;
                    _fishposY = fishPos.y;

                    console.log("3");
                }
                

            }
                   
            break;
            case 1 :
                
            if(localSeatId == msg.nSeatID){
                _posX = player._curPos.x;
                _posY = player._curPos.y+100;
                _fishposX = fishPos.x;
                _fishposY = fishPos.y;

                console.log("4");
            }else{
                if(convert >= 2){
                    _posY = cc.winSize.width - player._curPos.y+100;
                    _posX = player._curPos.x;
                    _fishposX =  fishPos.x;
                    _fishposY = cc.winSize.height - _fishposY; 

                    console.log("5");
                }else{
                    
                    _posX = player._curPos.x;
                    _posY = player._curPos.y+100;
                    _fishposX = cc.winSize.width - fishPos.x;
                    _fishposY = fishPos.y;  

                    console.log("6");
                }
                

            }
                   
            break;
        }

            var Bonuseffect = ActionMgr.createSpine(Res.GAME.bonus_Json, Res.GAME.bonus_Altas);
            Bonuseffect.setScale(1);
            Bonuseffect.setAnimation(0,"animation",true);        
            Bonuseffect.setPosition(_fishposX,_fishposY);
            Bonuseffect.setTag("diceSlotBonus");
            Bonuseffect.runAction(cc.sequence(  
               // cc.delayTime(2),
                cc.spawn(cc.moveTo(1.5,_posX ,_posY), cc.scaleTo(1.5, 0.5)),         
                cc.delayTime(1.5),
                cc.callFunc( ()=> {
                    // console.log("fishModel._fishid",fishModel._fishId)
                    if(fishModel._fishId==29){
                        this.gamingTable(fishPos, fishModel, msg);
                      }else{
                        this.larbar(fishPos, fishModel, msg);
                      }
                })
            ))

        this._sceneNode.addChild(Bonuseffect);
          
            // var ani = ActionMgr.createSpine(Res.GAME.god_wealth_Json, Res.GAME.god_wealth_Atlas);
            
            // if(fishModel.fishid == 29){
            //     ani.setAnimation(0, "dice", false);               
            // }else{
            //     ani.setAnimation(0, "larbar", false);
            // }
            // Bonuseffect.setAnimation(0, "animation", false);
             
            // ani.setScale(0.8);
            // node.addChild(ani);
            // var _scale = 0.75;
            // if (rewardScoreNum >= 1000000) {
            //     _scale = 0.55
            // }








        //     //得分
        //     var label = new cc.LabelBMFont("" + rewardScoreNum, Res.LOBBY.font_resource_088);
        //     label.setPosition(0, -90);
        //     label.setScale(_scale);
        //     node.addChild(label, 1);

        //     node.runAction(cc.sequence(
        //      cc.scaleTo(0.25, 1.4),
        //      cc.scaleTo(0.2, 1.3),
        //      cc.scaleTo(0.1, 1.4),
        //      cc.delayTime(2),
        //     //cc.scaleTo(0.2, 0),
        //     cc.spawn(cc.moveTo(1.2, player._curPos.x ,player._curPos.y), cc.scaleTo(1.2, 0)),
        //     cc.callFunc(function () {
        //         node.removeFromParent();
        //     })
        // ));

        // //数字动作
        // for (var i = 0; i < label.getChildrenCount(); ++i) {
        //     var child = label.getChildByTag(i);
        //     if (child) {
        //         child.setScale(0);
        //         child.runAction(cc.sequence(ActionMgr.createDelayBounce(0.1 * i, 1.7, 0.25, 0.2, 0.1, 0)));
        //     }
        // }

     
       // }
     
        
        // var _posX = fishPos.x, _posY = fishPos.y;
        // var localSeatId = this._localPlayer.getSeatID();
        // if (localSeatId >= 2) {
        //     _posX = cc.winSize.width - _posX;
        //     _posY = cc.winSize.height - _posY;
        // }

        // if (msg.nSeatID == -1) {
        //     this.doOthersGetItem(fishPos, msg, itemLen, _posX, _posY);
        //     return;
        // }
        // var god_wealthJson = Res.GAME.god_wealth_Json;
        // var god_wealthAltas = Res.GAME.god_wealth_Altas;
        //  console.log("got it")
        // var god_wealth = ActionMgr.createSpine(god_wealthJson,god_wealthAltas,1);
        // god_wealth.setPosition(cc.winSize.width / 2, cc.winSize.height / 2 );
        // god_wealth.setAnimation(0, type, true);
        // this._sceneNode.addChild(god_wealth); 
        // var font = Res.LOBBY.font_resource_088;
        // for(var a =0; a < 1; a++){
            
        //     var goldLabel = new cc.LabelBMFont(5000, font);
        //     goldLabel.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        //     goldLabel.setScale(2);
        //     // goldLabel.runAction(cc.sequence(
        //     //     //action,
        //     // //cc.moveBy(0.5,0,65),
        //     // //cc.delayTime(0.5),
        //     //     cc.scaleTo(0.3, 1, 1),
        //     //     cc.delayTime(1),
        //     //     cc.fadeOut(0.5),
                
        //     // ));
        //     this._sceneNode.addChild(goldLabel, 0);
        // }

    },

    gamingTable : function(fishPos, fishModel, msg){
        
        var mag = msg.mag;
        var localSeatId = this._localPlayer.getSeatID();
        var _fishposX = fishPos.x, _fishposY = fishPos.y;
        var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
  
        if(localSeatId == msg.nSeatID){
            var msakAttack = new ccui.Button();
            msakAttack.loadTextureNormal("resource_021.png", ccui.Widget.PLIST_TEXTURE);
            msakAttack.setPosition(cc.winSize.width/2 ,cc.winSize.height/2 + 100);
            msakAttack.setScaleX(20);
            msakAttack.setOpacity(1);
            msakAttack.addClickEventListener(()=>{

            })
            this._sceneNode.addChild(msakAttack);
            //创建遮罩层
            var bar = new cc.LayerColor(cc.color(0, 0, 0, 240));
            bar.setContentSize(cc.winSize.width,  100);
            bar.setPosition(0,cc.winSize.height/2 + 250);
            this._sceneNode.addChild(bar);

            


            var textLabel = new cc.LabelTTF("請搖一搖或點擊骰盅擲出骰子點數", "Arial", 42);
            textLabel.lineWidth = 1;
            textLabel.setPosition(600, 40 );
            //textLabel.setFontFillColor(cc.color.YELLOW);
            textLabel.setColor(cc.color(247,225,26));
            //textLabel.enableStroke(cc.color.BLACK, 2);
            bar.addChild(textLabel);

            var TextNumber = new cc.LabelTTF("15", "Arial", 42);
            TextNumber.lineWidth = 0.5;
            TextNumber.setPosition(975, 40 );
            TextNumber.setColor(cc.color(247,225,26));
            TextNumber.setTag("Number");
            
            var IsShake =false;
            TextNumber.runAction(
                cc.sequence(
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;         
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>
                    {
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsShake)
                            return;  
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                        dicecup.runAction(
                            cc.sequence(                   
                                cc.callFunc(()=>{
                                    dicecup.setAnimation(0,"up_move",false);  
                                    dicecupUnder.setAnimation(0,"beiow_move",false)
                                }),
                                cc.delayTime(1),
                                cc.callFunc(()=>{
                                    dicecup.setAnimation(0,"up_open",false);
                                }),
                                cc.delayTime(0.25),
                                cc.callFunc(()=>{
                                    Gamingdice.setVisible(true);
                                    dicecupUnder.setAnimation(0,"beiow_open",false);
                                }),
                                cc.delayTime(2),
                                cc.callFunc(()=>{
                                    GamingTable.removeFromParent();
                                    Gamingdice.removeFromParent();
                                    dicecupUnder.removeFromParent();
                                    ShakeBtn.removeFromParent();
                                    dice1.removeFromParent();
                                    dice2.removeFromParent();
                                    dicecup.removeFromParent();
                                    textLabelShake.removeFromParent();
                                    bar.removeFromParent();
                                    // console.log(this._sceneNode);
                                    this._sceneNode.getChildByTag("diceSlotBonus").removeFromParent();      
                                    this.BonusUI(fishModel.fishid,msg.nScore);                   
                                })
                            
                            ))  
                    })
                     
                )
            )
            bar.addChild(TextNumber);  
            var textLabe2 = new cc.LabelTTF("秒後自動開獎", "Arial", 40);
            textLabe2.setPosition(1150, 30 );
            textLabe2.setColor(cc.color(247,225,26));
            bar.addChild(textLabe2);

            var gamingTableJson, gamingTableAltas,dicecupAltas,dicecupJson;
            gamingTableJson = Res.GAME.gamingTable_Json;
            gamingTableAltas = Res.GAME.gamingTable_Altas;
        // var BackGround=
            var GamingTable = ActionMgr.createSpine(gamingTableJson,gamingTableAltas,1);
            GamingTable.setPosition(cc.winSize.width / 2, cc.winSize.height / 2 -50);
            GamingTable.setAnimation(0, "table", true);
            // GamingTable.setScale(0.8);
            this._sceneNode.addChild(GamingTable); 

            var skipBtn = new ccui.Button();
            skipBtn.loadTextureNormal("#skip.png", ccui.Widget.PLIST_TEXTURE);
            skipBtn.setPosition(330,-200);
            skipBtn.setScale(1.7);
            skipBtn.addClickEventListener(()=>{
                textLabelShake.removeFromParent();
                ShakeBtn.removeFromParent();
                IsShake = true;
                dicecup.runAction(
                    cc.sequence(                   
                        cc.callFunc(()=>{
                            this._sceneNode.getChildByTag("dice1").setSpriteFrame("#"+dice_count+".png");
                            this._sceneNode.getChildByTag("dice2").setSpriteFrame("#"+pic+".png");
                            dicecup.setAnimation(0,"up_open",false);
                        }),
                        cc.delayTime(0.25),
                        cc.callFunc(()=>{
                            Gamingdice.setVisible(true);
                            dicecupUnder.setAnimation(0,"beiow_open",false);
                        }),
                        cc.delayTime(2),
                        cc.callFunc(()=>{
                            GamingTable.removeFromParent();
                            Gamingdice.removeFromParent();
                            dicecupUnder.removeFromParent();
                            ShakeBtn.removeFromParent();
                            dice1.removeFromParent();
                            dice2.removeFromParent();
                            dicecup.removeFromParent();
                            textLabelShake.removeFromParent();
                            bar.removeFromParent();
                            // console.log(this._sceneNode);
                            this._sceneNode.getChildByTag("diceSlotBonus").removeFromParent();      
                            this.BonusUI(fishModel.fishid,msg.nScore);                   
                        })
                    
                    ))  
            })
            GamingTable.addChild(skipBtn);
            
            //textLabel.enableStroke(cc.color.BLACK, 2);
            bar.addChild(textLabel);
            // TextNumber.setString("10");
            // this._sceneNode.schedule(function() {
            //     // 這裏的 this 指向 component
            //     TextNumber.setString("10");
            //     console.log((Math.floor(Math.random() * (6 - 1+1)) + 1));
            // }, 10)
            var result,diceMax,diceMin,diceNumber;
            switch(mag){

                case 550:
                result = 0;
                diceMax=1;
                diceMin=1;
                diceNumber=2;
                break;
                case 50:
                result = 1;
                diceMax=2;
                diceMin=1;
                diceNumber=3;
                break;
                case 80:
                result = 2;
                diceMax=3;
                diceMin=1;
                diceNumber=4;
                break;
                case 100:
                result = 3;
                diceMax=4;
                diceMin=1;
                diceNumber=5;
                break;
                case 150:
                result = 4;
                diceMax=5;
                diceMin=1;
                diceNumber=6;
                break;
                case 200:
                result = 5;
                diceMax=6;
                diceMin=1;
                diceNumber=7;
                break;
                case 250:
                result = 6;
                diceMax=6;
                diceMin=2;
                diceNumber=8;
                break;
                case 300:
                result = 7;
                diceMax=6;
                diceMin=3;
                diceNumber=9;
                break;
                case 400:
                result = 8;
                diceMax=6;
                diceMin=4;
                diceNumber=10;
                break;
                case 500:
                result = 9;
                diceMax=6;
                diceMin=5;
                diceNumber=11;
                break; 
                case 600: 
                result = 10; 
                diceMax=6;
                diceMin=6;
                diceNumber=12;
                break;
                
                default:
                result = 1;

            }
            var position = 78
            var Gamingdice= ActionMgr.createSpine(gamingTableJson,gamingTableAltas,1);
            Gamingdice.setPosition(cc.winSize.height/2 -110 +result * position, cc.winSize.height / 2-125);
            Gamingdice.setAnimation(0, "effects_01", false);
            Gamingdice.setCompleteListener(()=>{
                Gamingdice.setAnimation(0, "effects_02", false);
            })
            this._sceneNode.addChild(Gamingdice); 
            Gamingdice.setVisible(false);
            

            dicecupJson =Res.GAME.dicecup_Json;
            dicecupAltas = Res.GAME.dicecup_Altas;
            var dicecupUnder = ActionMgr.createSpine(dicecupJson,dicecupAltas,1);
            dicecupUnder.setPosition(cc.winSize.width / 2, cc.winSize.height / 2 + 40);
            dicecupUnder.setAnimation(0,"beiow_loop",false);  
            this._sceneNode.addChild(dicecupUnder); 

            
            let dice_count = Math.floor(Math.random() * (diceMax - diceMin+1)) + diceMin;
            // console.log("diceNumber"+diceNumber,"dice_count",dice_count);
            var dice1 = new cc.Sprite("#"+dice_count+".png");
            dice1.setPosition(cc.winSize.width/2 -30 ,cc.winSize.height/2 + 70);
            dice1.setTag("dice1");
            this._sceneNode.addChild(dice1);
            var pic = diceNumber - dice_count;
            console.log("dice1",dice1);
            console.log("this._sceneNode.getChildByTag(dice2)",this._sceneNode.getChildByTag("dice2"))
            var dice2 = new cc.Sprite("#"+pic+".png");
            dice2.setPosition(cc.winSize.width/2 + 30 ,cc.winSize.height/2 + 40);
            dice2.setTag("dice2");
            this._sceneNode.addChild(dice2);

            var dicecup = ActionMgr.createSpine(dicecupJson,dicecupAltas,1);
            dicecup.setPosition(cc.winSize.width / 2, cc.winSize.height / 2 + 40);
            dicecup.setAnimation(0,"up_loop",false);  
            this._sceneNode.addChild(dicecup); 

            
            

            var textLabelShake = new cc.LabelTTF("搖一搖", "Arial", 42);
            textLabelShake.lineWidth = 0.7;
            textLabelShake.setPosition(cc.winSize.width/2 ,cc.winSize.height/2 + 100);
            textLabelShake.setFontFillColor(cc.color.YELLOW);
            this._sceneNode.addChild(textLabelShake);
            
            var ShakeBtn = new ccui.Button();
            ShakeBtn.loadTextureNormal("resource_021.png", ccui.Widget.PLIST_TEXTURE);
            ShakeBtn.setPosition(cc.winSize.width/2 ,cc.winSize.height/2 + 100);
            ShakeBtn.setScaleX(1.6);
            ShakeBtn.setOpacity(0);


            ShakeBtn.addClickEventListener( ()=> {
                textLabelShake.removeFromParent();
                ShakeBtn.removeFromParent();
                IsShake = true;
                dicecup.runAction(
                    cc.sequence(                   
                        cc.callFunc(()=>{
                            dicecup.setAnimation(0,"up_move",false);  
                            dicecupUnder.setAnimation(0,"beiow_move",false)
                        }),
                        cc.delayTime(1),
                        cc.callFunc(()=>{
                            dicecup.setAnimation(0,"up_open",false);
                        }),
                        cc.delayTime(0.25),
                        cc.callFunc(()=>{
                            Gamingdice.setVisible(true);
                            dicecupUnder.setAnimation(0,"beiow_open",false);
                        }),
                        cc.delayTime(2),
                        cc.callFunc(()=>{
                            GamingTable.removeFromParent();
                            Gamingdice.removeFromParent();
                            dicecupUnder.removeFromParent();
                            ShakeBtn.removeFromParent();
                            dice1.removeFromParent();
                            dice2.removeFromParent();
                            dicecup.removeFromParent();
                            textLabelShake.removeFromParent();
                            bar.removeFromParent();
                            // console.log(this._sceneNode);
                            this._sceneNode.getChildByTag("diceSlotBonus").removeFromParent();      
                            this.BonusUI(fishModel.fishid,msg.nScore);                   
                        })
                    
                    ))    
            });
            this._sceneNode.addChild(ShakeBtn);
        }
    },

    BonusUI : function(fishid,nScore){
        var ani = ActionMgr.createSpine(Res.GAME.god_wealth_Json, Res.GAME.god_wealth_Altas, 1);
        ani.setPosition(cc.winSize.width/2,cc.winSize.height/2);
        if(fishid == 29){
            ani.setAnimation(0, "dice", false);               
        }else{
            ani.setAnimation(0, "larbar", false);
        }               
        
        var label = new cc.LabelBMFont( nScore, Res.LOBBY.font_resource_088);
        label.setPosition(0, -110);
        var _scale = 0.75;
            if (nScore >= 1000000) {
                _scale = 0.55
            }
        label.setScale(_scale);
        ani.addChild(label);
        this._sceneNode.addChild(ani);
        ani.runAction(
            cc.sequence(
                cc.delayTime(2),
                cc.callFunc(function(){
                    this.removeFromParent();
                },ani)
            )
        )
    },

    spin: function(dt){
        
        
        for(var i =0;i<this.larbar.children.length;i++){
                
            console.log(i)
            // console.log("n",this.larbar.children[i].getPosition());
           // n.y -=150;
           this.larbar.children[i].setPosition( this.larbar.children[i].getPosition().x, this.larbar.children[i].getPosition().y-=50);
           
        }
    },
    
    SpinStatus: function(){
        
        return this._IsScroll;
        
    },
   

    larbar:function(fishPos, fishModel, msg){
        var mag = msg.mag
        var localSeatId = this._localPlayer.getSeatID();
        var player = cf.RoomMgr.getRoomPlayer(msg.nSeatID);
        
        if(localSeatId == msg.nSeatID){
            var BackGround = new cc.LayerColor(cc.color(0,0,0,90))
            BackGround.setContentSize(cc.winSize.width,  cc.winSize.height);
            BackGround.setPosition(0,0);
            var msakAttack = new ccui.Button();
            msakAttack.loadTextureNormal("resource_021.png", ccui.Widget.PLIST_TEXTURE);
            msakAttack.setPosition(cc.winSize.width/2 ,cc.winSize.height/2 + 100);
            msakAttack.setScaleX(20);
            msakAttack.setOpacity(1);
            msakAttack.addClickEventListener(()=>{

            })
            this._sceneNode.addChild(msakAttack);
            var bar = new cc.LayerColor(cc.color(0, 0, 0, 240));
            bar.setContentSize(cc.winSize.width,  100);
            bar.setPosition(0,cc.winSize.height/2 + 250);
            this._sceneNode.addChild(bar);
            
            var textLabel = new cc.LabelTTF("請點擊拉桿轉出獎項", "Arial", 42);
            textLabel.lineWidth = 1;
            textLabel.setPosition(600, 40 );
            //textLabel.setFontFillColor(cc.color.YELLOW);
            textLabel.setColor(cc.color(247,225,26));
            //textLabel.enableStroke(cc.color.BLACK, 2);
            bar.addChild(textLabel);
            var IsPull = false;
            var TextNumber = new cc.LabelTTF("15", "Arial", 42);
            TextNumber.lineWidth = 1;
            TextNumber.setPosition(975, 40 );
            TextNumber.setColor(cc.color(247,225,26));
            TextNumber.setTag("Number");
            TextNumber.runAction(
                cc.sequence(
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;         
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>
                    {
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;     
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                    }),
                    cc.delayTime(1),
                    cc.callFunc(()=>{
                        if(IsPull)
                            return;  
                        TextNumber.string=(parseInt(TextNumber.string) -1).toString();
                        clipper.schedule(update1,0.1,50,0.1);
                larbar.runAction(
                            cc.sequence(        
                                cc.delayTime(0.5),           
                                cc.callFunc(()=>{
                                    larbar.setAnimation(0,"start",false);  
                                    textLabelPull.setOpacity(0);
                                    ShakeBtn.schedule(update2,0.1,50,0.1);
                                    
                                }),
                                cc.delayTime(0.5),
                                cc.callFunc(()=>{
                                    textLabelPull.schedule(update3,0.1,50,0.1);
                                    larbar.setAnimation(0,"normal",false);
                                    textLabelPull.setOpacity(1);
                                    this._IsScroll = true;                    
                                }),
                                cc.delayTime(5.5),
                                cc.callFunc(()=>{
                                    if(winB_IndexX==0){
                                        win_b.setVisible(true); //獎項
                                    }else{
                                        win_s.setVisible(true);//default
                                    }

                                    if(larbar_light!=null){                        
                                        for(var i =0;i<3;i++){
                                            if(larbar_light.tag=="001"){
                                                larbar_light.setVisible(true);
                                            }
                                    }
                                    }
                                }),
                                cc.delayTime(2),
                                cc.callFunc(()=>{
                                    bar.removeFromParent();
                                    larbar.removeFromParent();
                                    clipper.removeFromParent();
                                    BackGround.removeFromParent();
                                    // console.log(this._sceneNode);
                                    this._sceneNode.getChildByTag("diceSlotBonus").removeFromParent();
                                    this.BonusUI(fishModel.fishid,msg.nScore);  
                                    
                                })
                            
                                
                            )
                )
                    })
                     
                )
            )
            bar.addChild(TextNumber);   

            var textLabe2 = new cc.LabelTTF("秒後自動開獎", "Arial", 28);
            textLabe2.lineWidth=1;
            textLabe2.setPosition(1088, 35 );
            textLabe2.setColor(cc.color(247,225,26));
            bar.addChild(textLabe2);

            var clipper = new cc.ClippingNode();
                clipper.attr({x: 0, y: 0});
                clipper.inverted = false;
                clipper.alphaThreshold = 0.5;
                clipper.setTag("clipper");
                var stencil = new cc.Sprite("#resource_012_9.png");
                stencil.setPosition(530,cc.winSize.height/2);
                stencil.setScaleY(1);
                stencil.setScaleX(5.5);
                clipper.stencil = stencil;
            


            var larbarJson, larbarAltas,dicecupAltas,dicecupJson;
            larbarJson = Res.GAME.larbar_Json;
            larbarAltas = Res.GAME.larbar_Altas;
            var larbar = ActionMgr.createSpine(larbarJson,larbarAltas,1);
            larbar.setPosition(cc.winSize.width / 2, cc.winSize.height / 2 -50);
            larbar.setAnimation(0, "normal", false);
            // GamingTable.setScale(0.8);
            this._sceneNode.addChild(larbar); 
            this._sceneNode.addChild(clipper);
            var moveby = cc.moveBy(0.5,cc.p(0,-50));
            var moveTo = cc.moveTo(1,cc.p(0,50));
            var fadeTo = cc.fadeTo(0.5, 1);
            var fadeIn = cc.fadeIn(0.5,1);

            var array1 = new cc.Node();
            var array2 = new cc.Node();
            var array3 = new cc.Node();
            
            var list = [2, 3, 1, 5, 4];
            var EndY=360
            for(var a = 1; a <=7; a++){
                var s = new cc.Sprite("#larbar_0"+a+".png");
                s.setTag("00"+a);
                var t = new cc.Sprite("#larbar_0"+a+".png");
                t.setTag("00"+a);
                var u = new cc.Sprite("#larbar_0"+a+".png");
                u.setTag("00"+a);
                t.setPosition(460 ,EndY+((a-1)*160));
                u.setPosition(640 ,EndY+((a-1)*160));
                s.setPosition(820 ,EndY+((a-1)*160));
                
                array3.addChild(s);
                array2.addChild(u);
                array1.addChild(t);
            }
            

            clipper.addChild(array1);
            clipper.addChild(array2);
            clipper.addChild(array3);
            console.log("clipper",this._sceneNode.getChildByTag("clipper"));
            let LuckySeven = Math.floor(Math.random() * (3 - 1+1)) + 1;
            var Tag1,Tag2,Tag3,winB_IndexX =0,winB_IndexY,arrayTag,check = true;
            var Num = Math.floor(Math.random() * (7 - 2 + 1)) + 2;;
            switch(mag){
                case 500:
                    Tag1=Tag2=Tag3="001";
                    arrayTag = ["001","001","001"];
                    winB_IndexX = winB_IndexY = 1;
                break;
                
                case 300:
                    winB_IndexX = 1;
                    winB_IndexY = 2;
                    switch(LuckySeven){                    
                        case 1:
                            Tag1=Tag2 ="001";
                            Tag3 = "00"+Num;
                            arrayTag=["001","001","00"+Num]
                            break;
                        case 2:
                            Tag2=Tag3="001";
                            Tag1 = "00"+Num;
                            arrayTag=["00"+Num,"001","001"]
                            break;
                        case 3:
                            Tag1=Tag3="001";
                            Tag2 = "00"+Num;
                            arrayTag=["001","00"+Num,"001"]
                            break;
                    }
                break;

                case 100:          
                winB_IndexX = 1;
                winB_IndexY = 3;
                    switch(LuckySeven){
                        case 1:
                            Tag1 ="001";
                            Tag2 = "00"+Num;
                            Tag3 = "00"+(8-Num);
                            arrayTag=["001","00"+Num,"00"+(8-Num)]
                            break;
                        case 2:
                            Tag2="001";
                            Tag3 = "00"+Num;
                            Tag1 = "00"+(8-Num);
                            arrayTag=["00"+(8-Num),"001","00"+Num]
                            break;
                        case 3:
                            Tag3="001";
                            Tag1 = "00"+Num;
                            Tag2 = "00"+(8-Num);
                            arrayTag=["00"+Num,"00"+(8-Num),"001"]
                            break;
                    }
                break;

                case 350:
                    winB_IndexX =2;
                    winB_IndexY = 1;
                    Tag1=Tag2=Tag3="002";
                    arrayTag=["002","002","002"]
                break;

                case 250:
                    winB_IndexX = winB_IndexY = 2;
                    Tag1=Tag2=Tag3="003";
                    arrayTag=["003","003","003"]
                break;

                case 200:
                    winB_IndexX = 2;
                    winB_IndexY = 3;
                    Tag1=Tag2=Tag3="004";
                    arrayTag=["004","004","004"]
                break;

                case 180:
                    winB_IndexX = 3;
                    winB_IndexY = 1;
                    Tag1=Tag2=Tag3="005";
                    arrayTag=["005","005","005"]
                break;

                case 120:
                    winB_IndexX = 3;
                    winB_IndexY = 2;
                    Tag1=Tag2=Tag3="006";
                    arrayTag=["006","006","006"]
                break;

                case 80:
                    winB_IndexX = winB_IndexY = 3;
                    Tag1=Tag2=Tag3="007";
                    arrayTag=["007","007","007"]
                break;

                default:
                    var Num2 = Math.floor(Math.random() * (7 - 2 + 1)) + 2;
                    var Num3 = Math.floor(Math.random() * (7 - 2 + 1)) + 2;

                    while(check){
                        // console.log(Num==Num2 &&Num2==Num3 && Num==Num3)
                        if(Num==Num2 &&Num2==Num3 && Num==Num3){
                            console.log("Repeat");
                            Num3 = Math.floor(Math.random() * (7 - 2 + 1)) + 2;;
                        }else{
                            arrayTag=[Num,Num2,Num3];
                            check = false;
                        }
                    }
                break;

                
            }
            // console.log(arrayTag);
            if(winB_IndexX==0){
                var win_b = new cc.Sprite("#win_b.png");
                win_b.setPosition(435 ,-175 );
                win_b.setScale(2);
                win_b.setVisible(false);
                larbar.addChild(win_b);
            }
            if(winB_IndexX!=0){
                var win_s = new cc.Sprite("#win_s.png");
                win_s.setPosition(-415+((winB_IndexX-1) * 290) , -115 - ((winB_IndexY-1) * 60)) ;
                win_s.setScale(2);
                win_s.setVisible(false);
                larbar.addChild(win_s);
            }      

            var larbar_light;
            for(var i =0;i<3;i++){
                if(arrayTag[i]=="001"){
                    larbar_light = new cc.Sprite("#larbar_light.png");
                    larbar_light.setPosition(-180 +(i) * 180 , 50);
                    larbar_light.setTag(i);
                    larbar_light.setScale(2);
                    larbar_light.setVisible(false);
                    larbar.addChild(larbar_light);
                    larbar_light.runAction(cc.repeatForever(cc.blink(2, 2)));
                }
            }
            var count=count2=count3=0 , movedelta = movedelta2 = movedelta3 = 30 , Number = Number2 = Number3 = 50 ,ischeck = ischeck2 = ischeck3 = true,
            CompleteFlag = CompleteFlag2 = CompleteFlag3 = false,CheckNode,CheckNode2,CheckNode3;
            count = count2 = count3 =0;
            var update3 =  (dt)=> {
                    if(Number3>20){              
                        if(count3==1){
                            movedelta3+=30;
                        }else if(count3 == 2){
                            movedelta3+=20;
                        }else if(count3==3){
                            movedelta3 +=30
                        }else if(count3 == 14)
                        {
                            movedelta3-=40
                        }else if(count3>=16){
                            movedelta3-=20;
                        }
                }
                for(var i =0;i<array3.children.length;i++){
                    var n = array3.children[i];  
                    
                    n.y-=movedelta3;
                }
                    if(Number3<=20 && ischeck3){
                        for(var i =0;i<array3.children.length;i++){
                            var n = array3.children[i];
                                if(n.tag==arrayTag[2]){
                                    // console.log(Number3,n,ischeck3);
                                    CheckNode3 = i;
                                    
                                    if(n.y>EndY){
                                        // console.log("n.y,EndY",Number3,n.y,EndY)
                                        movedelta3 = Math.round((n.y-EndY)/Number3) ;
                                        ischeck3 = false;
                                    }
                                    // console.log("array3",Number,array3.children[i].y,movedelta3,ischeck3);
                                    break;
                                }  
                        }
            
                    }
            
                
                for(var i =0;i<array3.children.length;i++){
                    var n = array3.children[i];
            
                    if(n.y<-200){
                    let k  = ( i + 1) >= array3.children.length ? 0 : i+1;                 
            
                        n.y = array3.children[k].y + ((array3.children.length-1) *160) ;
                        count3++;
                    }
                    
                }
                if(Number3==0){
                    // console.log("array3",array3.children[CheckNode3].y)
                }
                        Number3--;

            };
            var update2 =  (dt)=> {
                if(Number2>20){
                    if(count2==1){
                        movedelta2+=30;
                    }else if(count2 == 2){
                        movedelta2+=20;
                    }else if(count2==3){
                        movedelta2 +=30
                    }else if(count2 == 14)
                    {
                        movedelta2-=40
                    }else if(count2>=17){
                        movedelta2-=20;
                    }
            }  
            for(var i =0;i<array2.children.length;i++){
                var n = array2.children[i];  
                
                n.y-=movedelta2;
            }
                if(Number2<=20 && ischeck2){
                    for(var i =0;i<array2.children.length;i++){
                        var n = array2.children[i];
                            if(n.tag==arrayTag[1]){
                                // console.log(Number,n,ischeck);
                                CheckNode2 = i;
                                
                                if(n.y>EndY){
                                    // console.log("n.y,EndY",Number,n.y,EndY)
                                    movedelta2 = Math.round((n.y-EndY)/Number2) ;
                                    ischeck2 = false;
                                }
                                // console.log("array1",Number,array1.children[i].y,movedelta,ischeck);
                                break;
                            }  
                    }
        
                }
        
            
            for(var i =0;i<array2.children.length;i++){
                var n = array2.children[i];
        
                if(n.y<-200){
                let k  = ( i + 1) >= array1.children.length ? 0 : i+1;                 
        
                    n.y = array2.children[k].y + ((array2.children.length-1) *160) ;
                    count2++;
                }
                
            }
            if(Number2==0){
                // console.log("array2",array2.children[CheckNode2].y)
            }
                    Number2--;

                
        };
    var update1 =  (dt)=> {
        
        if(Number>20 && ischeck){
            if(count==1){
                movedelta+=30;
            }else if(count == 2){
                movedelta+=20;
            }else if(count==3){
                movedelta +=30
            }else if(count == 14){
                movedelta-=40
            }else if(count>=17){
                movedelta-=20;
            }
        }
        for(var i =0;i<array1.children.length;i++){
            var n = array1.children[i];  
            
            n.y-=movedelta;
            // if(!ischeck && i==CheckNode)
            //     console.log(Number,array1.children[i].y,movedelta)
        }
            if(Number<=20 && ischeck){
                for(var i =0;i<array1.children.length;i++){
                    var n = array1.children[i];
                        if(n.tag==arrayTag[0]){
                            // console.log(Number,n,ischeck);
                            CheckNode = i;
                            
                            if(n.y>EndY){
                                // console.log("n.y,EndY",Number,n.y,EndY)
                                movedelta = Math.round((n.y-EndY)/Number) ;
                                ischeck = false;
                            }
                            // console.log("array1",Number,array1.children[i].y,movedelta,ischeck);
                            break;
                        }  
                }

            }

        
        for(var i =0;i<array1.children.length;i++){
            var n = array1.children[i];

            if(n.y<-200){
            let k  = ( i + 1) >= array1.children.length ? 0 : i+1;                 

                n.y = array1.children[k].y + ((array1.children.length-1) *160) ;
                count++;
            }
            
        }

        if(Number==0){
            // console.log("array1",array1.children[CheckNode].y)
        }
        
        Number--;
        
        
    };

            var textLabelPull = new cc.LabelTTF("拉", "Arial", 46);
            textLabelPull.lineWidth=1;
            textLabelPull.setPosition(320, 211 );
            textLabelPull.setColor(cc.color(247,225,26));
            larbar.addChild(textLabelPull);

            var skipBtn = new ccui.Button();
            skipBtn.loadTextureNormal("#skip.png", ccui.Widget.PLIST_TEXTURE);
            skipBtn.setPosition(480,0);
            skipBtn.setScale(1.7);
        
            skipBtn.addClickEventListener(()=>{
                IsPull =  true;               
                larbar.runAction(
                            cc.sequence(        
                                cc.delayTime(0.5),           
                                cc.callFunc(()=>{
                                    // larbar.setAnimation(0,"start",false);  
                                    textLabelPull.setOpacity(0);
                                    this._sceneNode.getChildByTag("clipper").children[0].children[0].setSpriteFrame("larbar_02.png");
                                    
                                }),
                                cc.delayTime(2),
                                cc.callFunc(()=>{
                                    bar.removeFromParent();
                                    larbar.removeFromParent();
                                    clipper.removeFromParent();
                                    BackGround.removeFromParent();
                                    console.log(this._sceneNode);
                                    this._sceneNode.getChildByTag("diceSlotBonus").removeFromParent();
                                    this.BonusUI(fishModel.fishid,msg.nScore);  
                                    
                                })
                            
                                
                            )
                )
                // bar.removeFromParent();
                // larbar.removeFromParent();
                // clipper.removeFromParent();
                // BackGround.removeFromParent();
                // console.log(this._sceneNode);
                // this._sceneNode.getChildByTag("diceSlotBonus").removeFromParent();
            })
            larbar.addChild(skipBtn);

            var ShakeBtn = new ccui.Button();
            ShakeBtn.loadTextureNormal("resource_021.png", ccui.Widget.PLIST_TEXTURE);

            ShakeBtn.setPosition(320 ,215 );
            
            ShakeBtn.addClickEventListener(()=>{
                IsPull =  true;
                clipper.schedule(update1,0.1,50,0.1);
                larbar.runAction(
                            cc.sequence(        
                                cc.delayTime(0.5),           
                                cc.callFunc(()=>{
                                    larbar.setAnimation(0,"start",false);  
                                    textLabelPull.setOpacity(0);
                                    ShakeBtn.schedule(update2,0.1,50,0.1);
                                    
                                }),
                                cc.delayTime(0.5),
                                cc.callFunc(()=>{
                                    textLabelPull.schedule(update3,0.1,50,0.1);
                                    larbar.setAnimation(0,"normal",false);
                                    textLabelPull.setOpacity(1);
                                    this._IsScroll = true;                    
                                }),
                                cc.delayTime(5.5),
                                cc.callFunc(()=>{
                                    if(winB_IndexX==0){
                                        win_b.setVisible(true); //獎項
                                    }else{
                                        win_s.setVisible(true);//default
                                    }

                                    if(larbar_light!=null){                        
                                        for(var i =0;i<3;i++){
                                            if(larbar_light.tag=="001"){
                                                larbar_light.setVisible(true);
                                            }
                                    }
                                    }
                                }),
                                cc.delayTime(2),
                                cc.callFunc(()=>{
                                    bar.removeFromParent();
                                    larbar.removeFromParent();
                                    clipper.removeFromParent();
                                    BackGround.removeFromParent();
                                    console.log(this._sceneNode);
                                    this._sceneNode.getChildByTag("diceSlotBonus").removeFromParent();
                                    this.BonusUI(fishModel.fishid,msg.nScore);  
                                    
                                })
                            
                                
                            )
                )
                
            })
            ShakeBtn.setOpacity(0);          
            larbar.addChild(ShakeBtn);
    }
        
    }
});

/**
 * 特效管理器实例
 * @type {undefined}
 * @private
 */
EffectMgr._sInstance = undefined;
EffectMgr.getInstance = function () {
    if (!EffectMgr._sInstance) {
        EffectMgr._sInstance = new EffectMgr();
    }
    return EffectMgr._sInstance;
};

/**
 * 移除实例
 */
EffectMgr.purgeRelease = function () {
    if (EffectMgr._sInstance) {
        EffectMgr._sInstance.onClear();
        EffectMgr._sInstance = null;
    }
};