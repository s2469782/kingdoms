cf.PlayerMgr = {
    _localPlayer: null,
    _uid: null,
    _safeCode: null,

    /**
     * 创建本地玩家数据
     * @param resMsg
     */
    createLocalPlayer: function (resMsg) {
        var self = this;

        //初始化玩家信息
        var playerModel = PlayerModel.create();
        playerModel.initTaskCount(0);
        playerModel.setIdLogin(resMsg.nidLogin);
        playerModel.setGamePlayerId(resMsg.strGamePlayerID);
        playerModel.setNickName(resMsg.strNickname);
        playerModel.setPlayerLevel(resMsg.nPlayerLevel);
        playerModel.setVipLevel(resMsg.nVIPLevel);
        playerModel.setGoldNum(resMsg.nGoldNum);
        playerModel.setDiamondNum(resMsg.nDiamondNum);
        playerModel.setFortData(resMsg.nFortData);
        playerModel.setPhone(resMsg.strPhone);
        playerModel.setSafeCode(resMsg.strSafeCode);
        playerModel.setJackpot(resMsg.nJackpot);
        playerModel.setExp(resMsg.nExp);
        playerModel.setSex(resMsg.nSex);
        playerModel.setHeadId(resMsg.nHeadType);
        playerModel.setItemList(resMsg.strItemList);
        playerModel.setDayMission(resMsg.strDayMission);
        playerModel.setWeekMission(resMsg.strWeekMission);
        playerModel.setSeriesForeverRecord(resMsg.nSeriesForeverRecord);
        playerModel.setSeriesRecord(resMsg.strSeriesRecord);
        playerModel.setVipExp(resMsg.nVIPExp);
        playerModel.setWeekData(resMsg.strWeekData);
        playerModel.setCoupons(resMsg.nCoupons);
        playerModel.setLottery(resMsg.nLottery);
        playerModel.setVipEndTime(resMsg.strVIPEndTime);
        playerModel.setLoginData(resMsg.strLoginData);
        playerModel.setDailyData(resMsg.strDailyData);
        playerModel.setOneTimeData(resMsg.strOneTimeData);
        playerModel.setFortSkinUse(resMsg.nFortSkinUse);
        playerModel.setFortSkin(resMsg.strFortSkin);
        playerModel.setGameTime(resMsg.strNowTime);
        playerModel.setEmailData(resMsg.strEmailData);
        playerModel.setUpdateRankTime(resMsg.nUpdateRankTime);
        playerModel.setFAccPoints(resMsg.fAccPoints);
        playerModel.setLastGoldNum(resMsg.nGoldNum);
        playerModel.setSignature(resMsg.playerSignature);
        playerModel.setMultipleLotteryNum(resMsg.nSignTimes);
        playerModel.setLotteryGetSign(resMsg.nLotteryBase);
        playerModel.setIsOpenLotteryGet(resMsg.nIsNewPlayer == 0 ? true : false);
        playerModel.setHasDrawLottery(resMsg.nLotteryBase > 0 ? true : false);
        playerModel.setNoviceGuideMask(resMsg.nGuideMarkRes);
        playerModel.setPermission(resMsg.nPermission);
        playerModel.setPlayerChipNum(resMsg.dChipNum);
        self._localPlayer = playerModel;

        //这里判断下最大炮倍索引
        if(resMsg.nFortData == cf.fortMaxIndex){
            cf.unlockShow = false;
            cf.playerHasPay = true;
        }
        
        //记录第三方uid
        self._uid = resMsg.strUID;
        self._safeCode = resMsg.strSafeCode;
    },

    /**
     * 获取玩家自己
     * @returns {null}
     */
    getLocalPlayer: function () {
        return this._localPlayer;
    },
    
    getUid:function () {
        return this._uid;
    },
    
    getSafeCode:function () {
        return this._safeCode;
    }
};