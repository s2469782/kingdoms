var DataMgr = DataMgr || {};

/**
 * 读取鱼数据
 * @param jsonFile
 */
DataMgr.readFishData = function (jsonFile) {
    //这里使用预载的方式处理数据，即单个loader完成加载，如需要单独加载请使用loadJson
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var fishData = cf.Data.FISH_DATA;
        if (fishData && fishData.length > 0) {
            cc.log("已读取FishData");
            return;
        }

        cc.log("读取FishData");

        for (var i in jsonData) {
            
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.fishId = jsonData[i].fishId;
                tmpInfo.fishName = jsonData[i].fishName;
                tmpInfo.fishType = jsonData[i].fishType;
                tmpInfo.fishScore = jsonData[i].fishScore;
                tmpInfo.killRate = jsonData[i].killRate;
                tmpInfo.baseGold = jsonData[i].baseGold.split(",");
                tmpInfo.fishSpeed = jsonData[i].fishSpeed;
                tmpInfo.showGold = jsonData[i].showGold.split(",");
                tmpInfo.showBonus = jsonData[i].showBonus;
                tmpInfo.music = jsonData[i].music;
                tmpInfo.talk = jsonData[i].talk;
                tmpInfo.boneFile = jsonData[i].boneFile;
                tmpInfo.bonePng = jsonData[i].bonePng;
                tmpInfo.animateType = jsonData[i].animateType;
                tmpInfo.moveAction = jsonData[i].moveAction;
                tmpInfo.theZoom = jsonData[i].theZoom;
                tmpInfo.diedAction = jsonData[i].diedAction;
                tmpInfo.extAction1 = jsonData[i].extAction1;
                tmpInfo.extAction2 = jsonData[i].extAction2;
                tmpInfo.showPriority = jsonData[i].showPriority;
                tmpInfo.lockPriority = jsonData[i].lockPriority;
                tmpInfo.killEffect = cf.parseStringForMap(jsonData[i].killEffect);
                fishData[tmpInfo.fishId] = tmpInfo;
            }

        }
    } else {
        cc.error("读取FishData失败！！");
    }
};

/**
 * 读取动画数据
 * @param jsonFile
 */
DataMgr.readAnimateData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var animateData = cf.Data.ANIMATE_DATA;
        if (animateData && animateData.length > 0) {
            cc.log("已读取AnimateData");
            return;
        }

        cc.log("读取AnimateData");

        for (var i in jsonData) {

            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.id = jsonData[i].id;
                tmpInfo.frameName = jsonData[i].frameName;
                tmpInfo.frameNum = jsonData[i].frameNum;
                tmpInfo.frameSpeed = jsonData[i].frameSpeed;

                animateData[tmpInfo.id] = tmpInfo;
            }
        }
    } else {
        cc.error("读取AnimateData失败");
    }
};

/**
 * 读取路径数据
 * @param jsonFile
 */
DataMgr.readPathData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var pathData = cf.Data.PATH_DATA;
        if (pathData && pathData.length > 0) {
            cc.log("已读取PathData");
            return;
        }

        cc.log("读取PathData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.pathId = jsonData[i].id;
                tmpInfo.pathGroup = jsonData[i].pathGroup;
                tmpInfo.pathType = jsonData[i].pathType;
                //在内部做坐标系转换
                tmpInfo.startPoint = cf.convertCoordinates(jsonData[i].startPointX, jsonData[i].startPointY);
                tmpInfo.endPoint = cf.convertCoordinates(jsonData[i].endPointX, jsonData[i].endPointY);

                tmpInfo.wayPoint = [];
                if (jsonData[i].wayPoint.length > 0) {

                    switch (tmpInfo.pathType) {
                        case cf.PathType.CURVE:
                            tmpInfo.wayPoint.push(tmpInfo.startPoint);
                            break;
                    }

                    for (var j in jsonData[i].wayPoint) {
                        var wayPoint = cf.convertCoordinates(jsonData[i].wayPoint[j].x, jsonData[i].wayPoint[j].y);
                        tmpInfo.wayPoint.push(wayPoint);
                    }

                    //这里做个特殊处理，直接把终点放入wayPoint数组
                    switch (tmpInfo.pathType) {
                        case cf.PathType.BEZIER:
                        case cf.PathType.CURVE:
                            tmpInfo.wayPoint.push(tmpInfo.endPoint);
                            break;
                    }

                }

                tmpInfo.duration = jsonData[i].duration;
                pathData[tmpInfo.pathId] = tmpInfo;
            }
        }
    } else {
        cc.error("读取PathData失败");
    }
};

/**
 * 读取鱼群数据
 * @param jsonFile
 */
DataMgr.readFishGroupData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var groupData = cf.Data.FISH_GROUP_DATA;
        if (groupData && groupData.length > 0) {
            cc.log("已读取GroupData");
            return;
        }

        cc.log("读取GroupData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.id = jsonData[i].id;
                tmpInfo.groupName = jsonData[i].groupName;
                tmpInfo.groupType = jsonData[i].groupType;
                tmpInfo.fishId = jsonData[i].fishId;
                tmpInfo.fishNum = jsonData[i].fishNum;
                tmpInfo.distance = jsonData[i].distance;
                tmpInfo.interval = jsonData[i].interval;
                tmpInfo.pathGroup = jsonData[i].pathGroup;

                groupData[tmpInfo.id] = tmpInfo;
            }
        }

    } else {
        cc.error("读取GroupData失败");
    }
};

/**
 * 读取鱼碰撞数据
 * @param jsonFile
 */
DataMgr.readFishCollisionData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var collisionData = cf.Data.FISH_COLLISION_DATA;
        if (collisionData && collisionData.length > 0) {
            cc.log("已读取FishCollisionData");
            return;
        }

        cc.log("读取FishCollisionData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.index = jsonData[i].index;
                tmpInfo.name = jsonData[i].name;
                tmpInfo.circleArray = [];
                for (var j in jsonData[i].circleArray) {
                    if (jsonData[i].circleArray.hasOwnProperty(j)) {

                        var circle = {};
                        circle.pt = cc.p(jsonData[i].circleArray[j].x, jsonData[i].circleArray[j].y);
                        circle.r = jsonData[i].circleArray[j].r;
                        tmpInfo.circleArray.push(circle);
                    }
                }
                collisionData[tmpInfo.index] = tmpInfo;
            }
        }

    } else {
        cc.error("读取FishCollisionData失败");
    }
};

/**
 * 读取子弹碰撞数据
 * @param jsonFile
 */
DataMgr.readBulletCollisionData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var collisionData = cf.Data.BULLET_COLLISION_DATA;
        if (collisionData && collisionData.length > 0) {
            cc.log("已读取BulletCollisionData");
            return;
        }

        cc.log("读取BulletCollisionData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.name = jsonData[i].name;
                tmpInfo.index = jsonData[i].index;
                tmpInfo.circleArray = [];
                for (var j in jsonData[i].circleArray) {
                    if (jsonData[i].circleArray.hasOwnProperty(j)) {

                        var circle = {};
                        circle.pt = cc.p(jsonData[i].circleArray[j].x, jsonData[i].circleArray[j].y);
                        circle.r = jsonData[i].circleArray[j].r;
                        tmpInfo.circleArray.push(circle);
                    }
                }
                collisionData[tmpInfo.index] = tmpInfo;
            }
        }

    } else {
        cc.error("读取BulletCollisionData失败");
    }
};

/**
 * 读取房间数据
 * @param jsonFile
 */
DataMgr.readRoomData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var RoomData = cf.Data.ROOM_DATA;
        if (RoomData && RoomData.length > 0) {
            cc.log("已读取RoomData");
            return;
        }

        cc.log("读取RoomData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.roomId = jsonData[i].index;
                tmpInfo.room_name = jsonData[i].room_name;
                tmpInfo.room_type = jsonData[i].room_type;
                tmpInfo.background = jsonData[i].background;
                tmpInfo.bgm = jsonData[i].bgm;
                tmpInfo.show_condition = jsonData[i].show_condition.split(",");
                tmpInfo.cannonlevel_limit = jsonData[i].cannonlevel_limit.split(",");
                tmpInfo.gold_limit = jsonData[i].gold_limit.split(",");
                tmpInfo.fish_config_ID = jsonData[i].fish_config_ID;
                tmpInfo.fish_num_limit = jsonData[i].fish_num_limit;
                tmpInfo.playerlevel_limit = jsonData[i].playerlevel_limit;
                tmpInfo.dropItem = jsonData[i].dropItem;
                RoomData[tmpInfo.roomId] = tmpInfo;
            }
        }

    } else {
        cc.error("读取RoomData失败");
    }
};
/**
 * 讀取龍珠技能數據
 */
DataMgr.readAttrBallSkillData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var AttrBallSkillData = cf.Data.ATTRBALLSKILL_DATA;
        if (AttrBallSkillData && AttrBallSkillData.length > 0) {
            cc.log("已读取AttrBallSkillData");
            return;
        }

        cc.log("读取AttrBallSkillData");       
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.index = i;
                tmpInfo.hitRange = jsonData[i].hitRange;
                tmpInfo.fortLvUp = jsonData[i].fortLvUp;
                tmpInfo.fireNum = jsonData[i].fireNum;
                tmpInfo.free = jsonData[i].free;
                tmpInfo.killRateUp = jsonData[i].killRateUp;
                AttrBallSkillData[tmpInfo.index] = tmpInfo;
            }
        }

    } else {
        cc.error("读取AttrBallSkillData失败");
    }
};

/**
 * 读取炮数据
 * @param jsonFile
 */
DataMgr.readCannonLevelData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var cannonLevelData = cf.Data.CANNON_LEVEL_DATA;
        if (cannonLevelData && cannonLevelData.length > 0) {
            cc.log("已读取CannonLevelData");
            return;
        }

        cc.log("读取CannonLevelData");       
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.index = jsonData[i].index;
                tmpInfo.cannonlevel = jsonData[i].cannonlevel;
                tmpInfo.unlock_type = jsonData[i].unlock_type;
                tmpInfo.gold_reward = jsonData[i].gold_reward;
                var tmpType = jsonData[i].cost_item.split("|");
                var costArray = [];
                for (var j = 0; j < tmpType.length; j++) {
                    var costChildArray = [];
                    var localArray = tmpType[j].split(",");
                    var localLength = localArray.length;
                    //提取出符号后使用局部的结构体存放
                    for (var index = 0; index < localLength; index += 2) {
                        var localInfo = {};
                        localInfo.itemCostId = localArray[index];
                        localInfo.itemCostNum = localArray[index + 1];
                        costChildArray.push(localInfo);
                    }
                    costArray.push(costChildArray);
                }
                tmpInfo.cost_item = costArray;
                tmpInfo.cost_lucky_stone = jsonData[i].cost_lucky_stone;
                tmpInfo.return_lucky_stone = jsonData[i].return_lucky_stone;
                tmpInfo.success_rate = jsonData[i].success_rate;
                tmpInfo.add_exp = jsonData[i].add_exp;
                tmpInfo.skinIndex = cf.parseStringForMap(jsonData[i].skinIndex);

                var tmpIndex = parseInt(tmpInfo.index);
                if(cf.fortMaxIndex <= tmpIndex){
                    cf.fortMaxIndex = tmpIndex;
                }

                cannonLevelData[tmpInfo.index] = tmpInfo;
            }
        }

    } else {
        cc.error("读取CannonLevelData失败");
    }
};

/**
 * 读取道具数据
 * @param jsonFile
 */
DataMgr.readItemData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var backData = cf.Data.BACKPACK_DATA;
        var itemData = cf.Data.ITEM_DATA;
        if (itemData && itemData.length > 0) {
            cc.log("已读取ItemData");
            return;
        }

        cc.log("读取ItemData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.ItemId = jsonData[i].ItemId;
                tmpInfo.itemName = jsonData[i].itemName;
                tmpInfo.itemInfo = jsonData[i].itemInfo;
                tmpInfo.Type = jsonData[i].Type;
                tmpInfo.childType = jsonData[i].childType;
                tmpInfo.dropIcon = jsonData[i].dropIcon;
                tmpInfo.itemIcon = jsonData[i].itemIcon;
                tmpInfo.bigIcon = jsonData[i].bigIcon;
                tmpInfo.levelLimit = jsonData[i].levelLimit;
                tmpInfo.maxNum = jsonData[i].maxNum;
                tmpInfo.uselifeType = jsonData[i].uselifeType;
                tmpInfo.uselifeData = jsonData[i].uselifeData;
                tmpInfo.sell = jsonData[i].sell;
                tmpInfo.price = jsonData[i].price;
                tmpInfo.equip = jsonData[i].equip;
                tmpInfo.send = jsonData[i].send;
                tmpInfo.sendNum = jsonData[i].sendNum;
                tmpInfo.useType = jsonData[i].useType;
                tmpInfo.private = jsonData[i].private;
                tmpInfo.rewardId = jsonData[i].rewardId;
                tmpInfo.rank = jsonData[i].rank;
                tmpInfo.assessedValue = jsonData[i].assessedValue;

                itemData[tmpInfo.ItemId] = tmpInfo;
                if (tmpInfo.maxNum > 1 && tmpInfo.rank != 0) {
                    if(tmpInfo.ItemId != cf.ItemID.LOTTERYNUM && tmpInfo.ItemId != cf.ItemID.CHAT_CARD ){
                        backData.push(tmpInfo);
                    }
                }
            }
        }

    } else {
        cc.error("读取ItemData失败");
    }
};

/**
 * 读取商店Ui数据
 * @param jsonFile
 */
DataMgr.readShopUiData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {
        var count = 0;
        var ShopData = cf.Data.SHOPUI_DATA;
        for (var i in ShopData) {
            count++;
        }
        if (ShopData && count > 0) {
            cc.log("已读取ShopData");
            return;
        }

        cc.log("读取ShopData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.res = jsonData[i].res;
                tmpInfo.uiName = jsonData[i].uiName;
                tmpInfo.positionX = jsonData[i].positionX;
                tmpInfo.positionY = jsonData[i].positionY;
                ShopData[tmpInfo.uiName] = tmpInfo;
            }
        }

    } else {
        cc.error("读取ShopData失败");
    }
};
/**
 * 读取邮件Ui数据
 * @param jsonFile
 */
DataMgr.readEmailUiData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {
        var count = 0;
        var LobbyData = cf.Data.EMAILUI_DATA;
        for (var i in LobbyData) {
            count++;
        }
        if (LobbyData && count > 0) {
            cc.log("已读取EmailData");
            return;
        }

        cc.log("读取EmailData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.res = jsonData[i].res;
                tmpInfo.uiName = jsonData[i].uiName;
                LobbyData[tmpInfo.uiName] = tmpInfo;
            }
        }

    } else {
        cc.error("读取EmailData失败");
    }
};

/**
 * 读取角色数据
 * @param jsonFile
 */
DataMgr.readRoleLevelData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var roleLevelData = cf.Data.ROLE_LEVEL_DATA;
        if (roleLevelData && roleLevelData.length > 0) {
            cc.log("已读取RoleLevelData");
            return;
        }
       
        cc.log("读取RoleLevelData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.index = jsonData[i].index;
                tmpInfo.role_evel = jsonData[i].role_evel;
                tmpInfo.exp = jsonData[i].exp;
                tmpInfo.reward = cf.parseString(jsonData[i].reward);
                tmpInfo.show = jsonData[i].show.split(",");
                if(parseInt(tmpInfo.show[0]) != 0){
                    cf.lvRewardMax = tmpInfo.index+1;
                }
                roleLevelData[tmpInfo.index] = tmpInfo;
            }
        }
    } else {
        cc.error("读取RoleLevelData失败");
    }
};
/**
 * 读取每日领取奖励
 * @param jsonFile
 */
DataMgr.readLoginRewardData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var roleLevelData = cf.Data.LOGINREWARD_DATA;
        if (roleLevelData && roleLevelData.length > 0) {
            cc.log("readLoginRewardData");
            return;
        }
        cc.log("读取readLoginRewardData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.id = jsonData[i].id;
                tmpInfo.type = jsonData[i].type;
                tmpInfo.days = jsonData[i].days;
                tmpInfo.name = jsonData[i].name;
                tmpInfo.doubleVip = jsonData[i].doubleVip;
                tmpInfo.reward = jsonData[i].reward;
                tmpInfo.text = jsonData[i].text;

                roleLevelData.push(tmpInfo);
            }
        }
    } else {
        cc.error("读取readLoginRewardData失败");
    }
};


/**
 * 读取每日领取奖励列表
 * @param jsonFile
 */
DataMgr.readRewardData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var roleLevelData = cf.Data.REWARD_DATA;
        if (roleLevelData && roleLevelData.length > 0) {
            cc.log("readRewardData");
            return;
        }
        cc.log("读取readRewardData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.id = jsonData[i].id;
                tmpInfo.singleRandom = cf.parseString(jsonData[i].singleRandom);
                tmpInfo.weightRandom = cf.parseString(jsonData[i].weightRandom);
                tmpInfo.wheelSurfPay = cf.parseString(jsonData[i].wheelSurfPay);
                roleLevelData[tmpInfo.id] = tmpInfo;
            }
        }
    } else {
        cc.error("读取readRewardData失败");
    }
};


/**
 * 读取救济金配置信息
 * @param jsonFile
 */
DataMgr.readDoleData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var doleData = cf.Data.DOLE_DATA;
        if (doleData && doleData.length > 0) {
            cc.log("readDoleData");
            return;
        }
        cc.log("读取readDoleData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                var doleInfo = jsonData[i].onDole.split("|");
                tmpInfo.doleInfo = [];
                for(var j in doleInfo) {
                    var localInfo = doleInfo[j].split(",");
                    tmpInfo.doleInfo[j] = localInfo;
                }
                doleData[jsonData[i].vipGrade] = tmpInfo;
            }
        }
    } else {
        cc.error("读取readDoleData失败");
    }
};

/**
 * 读取在线奖励配置信息
 * @param jsonFile
 */
DataMgr.readOnlineRewordData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var onlineRewardData = cf.Data.ONLINEREWARD_DATA;
        if (onlineRewardData && onlineRewardData.length > 0) {
            cc.log("readOnlineRewardData");
            return;
        }
        cc.log("读取readOnlineRewardData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.id = jsonData[i].id;
                tmpInfo.goldNum = jsonData[i].goldNum;
                tmpInfo.onlineTime = jsonData[i].onlineTime;

                onlineRewardData[tmpInfo.id] = tmpInfo;
            }
        }
    } else {
        cc.error("读取readOnlineRewardData失败");
    }
};

/**
 * 主线任务数据
 * @param jsonFile
 */
DataMgr.readMainTaskData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var growTaskData = cf.Data.GROW_TASK_DATA;
        if (growTaskData && growTaskData.length > 0) {
            cc.log("readMainTaskData");
            return;
        }
        cc.log("读取readMainTaskData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.taskId = jsonData[i].taskId;
                tmpInfo.taskName = jsonData[i].taskName;
                tmpInfo.taskInfo = jsonData[i].taskInfo;
                tmpInfo.taskSeries = jsonData[i].taskSeries;
                tmpInfo.taskTarget = jsonData[i].taskTarget.split(",");
                tmpInfo.taskReward = cf.parseString(jsonData[i].taskReward);
                tmpInfo.taskIcon = jsonData[i].taskIcon;
                tmpInfo.lastTask = jsonData[i].lastTask;
                tmpInfo.nextTask = jsonData[i].nextTask;
                tmpInfo.guide = jsonData[i].guide;
                tmpInfo.show = jsonData[i].show;
                tmpInfo.guide_1 = jsonData[i].guide_1;

                growTaskData[tmpInfo.taskId] = tmpInfo;
            }
        }

    } else {
        cc.error("读取readMainTaskData失败");
    }
};

/**
 * 彩金抽奖数据
 * @param jsonFile
 */
DataMgr.readbonusDrawData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var data = cf.Data.BONUS_DRAW_DATA;
        if (data && data.length > 0) {
            cc.log("readbonusDrawData");
            return;
        }
        cc.log("读取readbonusDrawData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.index = jsonData[i].index;
                tmpInfo.drawName = jsonData[i].drawName;
                tmpInfo.bonusNeed = jsonData[i].bonusNeed;
                tmpInfo.rewardId = jsonData[i].rewardId;
                tmpInfo.showReward = jsonData[i].showReward;
                tmpInfo.chestIcon = jsonData[i].chestIcon;

                data.push(tmpInfo);
            }
        }

    } else {
        cc.error("读取readbonusDrawData失败");
    }
};

/**
 * 技能数据
 * @param jsonFile
 */
DataMgr.readSkillData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var data = cf.Data.SKILLDATA;
        if (data && data.length > 0) {
            cc.log("readSkillData");
            return;
        }
        cc.log("读取readSkillData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.skill_id = jsonData[i].skill_id;
                tmpInfo.skill_name = jsonData[i].skill_name;
                tmpInfo.skill_info = jsonData[i].skill_info;
                tmpInfo.vip_limit = jsonData[i].vip_limit;
                tmpInfo.item_cost = jsonData[i].item_cost;
                tmpInfo.diamond_cost = jsonData[i].diamond_cost;
                tmpInfo.skill_icon = jsonData[i].skill_icon;
                tmpInfo.skill_last = jsonData[i].skill_last;
                tmpInfo.skill_cd = jsonData[i].skill_cd;
                tmpInfo.skill_effect = jsonData[i].skill_effect;
                tmpInfo.skill_sound = jsonData[i].skill_sound;

                data[tmpInfo.skill_id] = tmpInfo;
            }
        }

    } else {
        cc.error("读取readSkillData失败");
    }
};

DataMgr.readCannonData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var data = cf.Data.CANNON_DATA;
        if (data && data.length > 0) {
            cc.log("readCannonData");
            return;
        }
        cc.log("读取readCannonData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.fortSkin = jsonData[i].fortSkin;
                tmpInfo.cannon_name = jsonData[i].cannon_name;
                tmpInfo.cannon_icon = jsonData[i].cannon_icon;
                tmpInfo.bullet_icon = jsonData[i].bullet_icon;
                tmpInfo.net_icon = jsonData[i].net_icon;
                tmpInfo.cannon_type = jsonData[i].cannon_type;
                tmpInfo.howGetItem = jsonData[i].howGetItem;
                tmpInfo.connect_item_id = jsonData[i].connect_item_id;
                tmpInfo.connect_timeItem_id = jsonData[i].connect_timeItem_id;
                tmpInfo.cannon_info = jsonData[i].cannon_info;

                data[tmpInfo.fortSkin] = tmpInfo;
            }
        }

    } else {
        cc.error("读取readCannonData失败");
    }
};
/**
 * 读取VIP数据
 * @param jsonFile
 */
DataMgr.readVipData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var data = cf.Data.VIP_DATA;
        if (data && data.length > 0) {
            cc.log("readVipData");
            return;
        }
        cc.log("读取readVipData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.id = jsonData[i].id;
                tmpInfo.vipLevel = jsonData[i].vipLevel;
                tmpInfo.totalPay = jsonData[i].totalPay;
                tmpInfo.shouItem = cf.parseString(jsonData[i].shouItem);
                tmpInfo.sent = jsonData[i].sent;
                tmpInfo.sentLimit = jsonData[i].sentLimit;
                tmpInfo.vipInfo = jsonData[i].vipInfo;
                tmpInfo.slowDown = jsonData[i].slowDown;
                tmpInfo.goldPlay_limit = jsonData[i].goldPlay_limit;
                tmpInfo.luckDrawNumber = jsonData[i].luckDrawNumber;
                data.push(tmpInfo);
            }
        }

        //获取vip最大等级
        cf.VipMaxLv = data.length - 1;

    } else {
        cc.error("读取readVipData失败");
    }
};

/**
 * 读取头像数据
 * @param jsonFile
 */
DataMgr.readHeadPortraitData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var data = cf.Data.HEADPORTRAIT_DATA;
        if (data && data.length > 0) {
            cc.log("readHeadPortraitData");
            return;
        }
        cc.log("读取readHeadPortraitData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.index = jsonData[i].index;
                tmpInfo.sex = jsonData[i].sex;
                tmpInfo.headIcon = jsonData[i].headIcon;

                data[tmpInfo.index] = tmpInfo;
            }
        }

    } else {
        cc.error("读取readHeadPortraitData失败");
    }
};

/**
 * 读取商店数据
 * @param jsonFile
 */
DataMgr.readShopData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var data = cf.Data.SHOP_DATA;
        if (data && data.length > 0) {
            cc.log("readShopData");
            return;
        }
        cc.log("读取readShopData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.id = jsonData[i].id;
                tmpInfo.storageId = jsonData[i].storageId;
                tmpInfo.goodName = jsonData[i].goodName;
                tmpInfo.goodType = jsonData[i].goodType;
                tmpInfo.itemId = jsonData[i].itemId;
                tmpInfo.itemNum = jsonData[i].itemNum;
                tmpInfo.rank = jsonData[i].rank;
                tmpInfo.costType = jsonData[i].costType;
                tmpInfo.cost = jsonData[i].cost;
                tmpInfo.firstBuy = jsonData[i].firstBuy;
                tmpInfo.salesRatio = jsonData[i].salesRatio;
                tmpInfo.base_SalesRatio = jsonData[i].base_SalesRatio;
                tmpInfo.icon = jsonData[i].icon;
                //data.push(tmpInfo);
                data[tmpInfo.id] = tmpInfo;
            }
        }
    } else {
        cc.error("读取readShopData失败");
    }
};

/**
 * 读取骨骼动画数据
 * @param jsonFile
 */
DataMgr.readSkeletonData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var data = cf.Data.SKELETON_DATA;
        if (data && data.length > 0) {
            cc.log("readSkeletonData");
            return;
        }
        cc.log("读取readSkeletonData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.id = jsonData[i].id;
                tmpInfo.action = jsonData[i].action;
                tmpInfo.json = jsonData[i].json;
                tmpInfo.arts = jsonData[i].arts;
                tmpInfo.offsetx = jsonData[i].offsetx;
                tmpInfo.offsety = jsonData[i].offsety;
                tmpInfo.scale = jsonData[i].scale;
                tmpInfo.png = jsonData[i].png;
                data[tmpInfo.id] = tmpInfo;
                
                // if (cc.sys.isNative) {
                //     var json = createVerPath("animate/" + tmpInfo.json);
                //     var arts = createVerPath("animate/" + tmpInfo.arts);
                //     var png = createVerPath("animate/" + tmpInfo.png);
                //     g_resGameScene.push(json);
                //     g_resGameScene.push(arts);
                //     g_resGameScene.push(png);
                // }
            }
        }

    } else {
        cc.error("读取readHeadSkeletonData失败");
    }
};

/**
 * 读取鱼种展示数据
 * @param jsonFile
 */
DataMgr.readFishKindData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var prizeFishData = cf.Data.PRIZE_FISH_DATA;
        if (prizeFishData && prizeFishData.length > 0) {
            cc.log("已读取FishKindData");
            return;
        }

        cc.log("读取FishKindData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.index = jsonData[i].index;
                tmpInfo.fishType = jsonData[i].fishType;
                tmpInfo.fishPng = jsonData[i].fishPng;
                tmpInfo.fishScore = jsonData[i].fishScore.split(",");
                tmpInfo.fishName = jsonData[i].fishName;

                prizeFishData.push(tmpInfo);
            }
        }

    } else {
        cc.error("读取FishKindData失败");
    }
};

/**
 * 读取音乐数据
 * @param jsonFile
 */
DataMgr.readSoundData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var soundData = cf.Data.SOUND_DATA;
        if (soundData && soundData.length > 0) {
            cc.log("已读取soundData");
            return;
        }

        cc.log("读取soundData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.id = jsonData[i].id;
                tmpInfo.name = jsonData[i].name;
                tmpInfo.preload = jsonData[i].preload;
                tmpInfo.type = jsonData[i].type;
                tmpInfo.priority = jsonData[i].priority;
                //这里直接创建路径
                var suffix = ".mp3";
                if (cc.sys.isNative) {
                    //根据版本选择音效格式
                    if (cc.sys.os === cc.sys.OS_ANDROID) {
                        //android使用ETC1压缩纹理
                        suffix = ".ogg";
                    } else if (cc.sys.os === cc.sys.OS_IOS) {
                        suffix = ".mp3";
                    }
                } else {
                    suffix = ".mp3";
                }
                tmpInfo.path = createVerPath("sound/" + tmpInfo.name + suffix);
                soundData[tmpInfo.id] = tmpInfo;

                //如果是游戏语音
                if (tmpInfo.type == 2) {
                    cf.Data.GAME_VOICE_DATA.push(tmpInfo);
                }
            }
        }

    } else {
        cc.error("读取soundData失败");
    }
};

/**
 * 消费数据
 * @param jsonFile
 */
DataMgr.readCostData = function (jsonFile) {

    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var costData = cf.Data.COST_DATA;
        if (costData && costData.length > 0) {
            cc.log("readCostData");
            return;
        }
        cc.log("读取readCostData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.id = jsonData[i].id;
                tmpInfo.name = jsonData[i].name;
                tmpInfo.costType = jsonData[i].costType;
                tmpInfo.costNum = jsonData[i].costNum;
                costData.push(tmpInfo);
            }
        }
    }
    else {
        cc.error("读取readCostData失败");
    }
};

/**
 * 读取寻宝数据
 */
DataMgr.readTreasureHuntData = function (jsonFile) {

    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var treasureHuntData = cf.Data.TREASUREHUNT_DATA;
        if (treasureHuntData && treasureHuntData.length > 0) {
            cc.log("readTreasureHuntData");
            return;
        }
        cc.log("读取readTreasureHuntData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.victoryTime = jsonData[i].victoryTime;
                tmpInfo.multipleWeight = jsonData[i].multipleWeight;
                var firstArray = jsonData[i].multipleNumber.split("|");
                var length = firstArray.length;
                var secondArray = [];
                for (var index = 0; index < length; index++) {
                    secondArray.push(firstArray[index].split(","));
                }
                tmpInfo.multipleNumber = secondArray;
                treasureHuntData.push(tmpInfo);
            }
        }
    }
    else {
        cc.error("读取readTreasureHuntData失败");
    }
};

/**
 * 读取玩法介绍Data
 */
DataMgr.readPlayMethodIntroduceData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var playMethodData = cf.Data.PLAYMETHOD_DATA;
        if (playMethodData && playMethodData.length > 0) {
            cc.log("readPlayMethodData");
            return;
        }
        cc.log("读取readPlayMethodData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.goldPlay_id = jsonData[i].goldPlay_id;
                tmpInfo.goldPlay_name = jsonData[i].goldPlay_name;
                tmpInfo.goldPlay_info = jsonData[i].goldPlay_info;
                tmpInfo.goldPlay_icon = jsonData[i].goldPlay_icon;
                playMethodData.push(tmpInfo);
            }
        }
    }
    else {
        cc.error("读取readplayMethodData失败");
    }
};

/**
 * 读取玩法介绍Data
 */
DataMgr.readPlayMethodIntroduceData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var playMethodData = cf.Data.PLAYMETHOD_DATA;
        if (playMethodData && playMethodData.length > 0) {
            cc.log("readPlayMethodData");
            return;
        }
        cc.log("读取readPlayMethodData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.goldPlay_id = jsonData[i].goldPlay_id;
                tmpInfo.goldPlay_name = jsonData[i].goldPlay_name;
                tmpInfo.goldPlay_info = jsonData[i].goldPlay_info;
                tmpInfo.goldPlay_icon = jsonData[i].goldPlay_icon;
                playMethodData.push(tmpInfo);
            }
        }
    }
    else {
        cc.error("读取readplayMethodData失败");
    }
};

/**
 * 读取扑克玩法数据
 */
DataMgr.readPokerGameIntroduceData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var pokerData = cf.Data.POKER_DATA;
        if (pokerData && pokerData.length > 0) {
            cc.log("readPokerData");
            return;
        }
        cc.log("读取readPokerData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.index = jsonData[i].index;
                tmpInfo.pokerName = jsonData[i].pokerName;
                tmpInfo.pokerMultiple = jsonData[i].pokerMultiple;
                tmpInfo.pokerWeight = jsonData[i].pokerWeight;
                var tmpType = jsonData[i].pokerType.split("|");
                var typeArray = [];
                for (var j = 0; j < tmpType.length; j++) {
                    var typeChildArray = [];
                    var localArray = tmpType[j].split(",");
                    var localLength = localArray.length;
                    //提取出符号后使用局部的结构体存放
                    for (var index = 0; index < localLength; index += 2) {
                        var localInfo = {};
                        localInfo.suit = localArray[index];
                        localInfo.num = localArray[index + 1];
                        typeChildArray.push(localInfo);
                    }
                    typeArray.push(typeChildArray);
                }
                tmpInfo.pokerType = typeArray;

                pokerData[tmpInfo.index] = tmpInfo;
            }
        }
    }
    else {
        cc.error("读取readPokerData失败");
    }
};

/**
 * 读取鱼潮数据
 */
DataMgr.readFishTideData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var fishTideData = cf.Data.FISHTIDEDATA;
        if (fishTideData && fishTideData.length > 0) {
            cc.log("readFishTideData");
            return;
        }
        cc.log("读取readFishTideData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.fishTideId = jsonData[i].fishTideId;
                tmpInfo.fishTideType = jsonData[i].fishTideType;
                tmpInfo.fishTideInterval = jsonData[i].fishTideInterval;
                tmpInfo.fishTideArray = jsonData[i].fishTideArray;
                fishTideData[tmpInfo.fishTideId] = tmpInfo;
            }
        }
    }
    else {
        cc.error("读取readFishTideData失败");
    }
};

/**
 * 读取阵列数据
 */
DataMgr.readFishArrayData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var fishArrayData = cf.Data.FISHARRAYDATA;
        if (fishArrayData && fishArrayData.length > 0) {
            cc.log("readFishArrayData");
            return;
        }
        cc.log("读取readFishArrayData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.id = jsonData[i].id;
                tmpInfo.arrayDirection = jsonData[i].arrayDirection;
                tmpInfo.fishId = jsonData[i].fishId;
                tmpInfo.pathId = jsonData[i].pathId;
                tmpInfo.arrayType = jsonData[i].arrayType;
                tmpInfo.offsetPoint = jsonData[i].offsetPoint;
                if (tmpInfo.offsetPoint) {
                    var maxOffsetX = tmpInfo.offsetPoint[0].x;
                    for (var index in tmpInfo.offsetPoint) {
                        if (tmpInfo.offsetPoint[index].x > maxOffsetX) {
                            maxOffsetX = tmpInfo.offsetPoint[index].x;
                        }
                    }
                    tmpInfo.maxOffsetX = maxOffsetX;
                }
                fishArrayData[tmpInfo.id] = tmpInfo;
            }
        }
    }
    else {
        cc.error("读取readFishArrayData失败");
    }
};

/**
 * 读取隐藏金币表
 */
DataMgr.readHideCoinData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var hideGoldData = cf.Data.HIDE_GOLD_DATA;
        if (hideGoldData && hideGoldData.length > 0) {
            cc.log("readHideCoinData");
            return;
        }
        cc.log("读取readHideCoinData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.index = jsonData[i].index;
                tmpInfo.hideCoins = jsonData[i].hideCoins.split(",");
                tmpInfo.cannonLv = jsonData[i].cannonLv.split(",");
                tmpInfo.fishScore = jsonData[i].fishScore.split(",");
                tmpInfo.killProbability = jsonData[i].killProbability;

                hideGoldData[tmpInfo.index] = tmpInfo;
            }
        }
    }
    else {
        cc.error("读取readHideCoinData失败");
    }
};

/**
 * 读取大奖赛数据表
 */
DataMgr.readMatchRewardData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var matchRewardData = cf.Data.MATCH_REWARD_DATA;
        if (matchRewardData && matchRewardData.length > 0) {
            cc.log("matchRewardData");
            return;
        }
        cc.log("读取matchRewardData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.index = jsonData[i].index;
                tmpInfo.dayRank = jsonData[i].dayRank.split(",");
                tmpInfo.dayRankScore = jsonData[i].dayRankScore;
                tmpInfo.dayRankReward = jsonData[i].dayRankReward.split(",");
                tmpInfo.weekRank = jsonData[i].weekRank.split(",");
                tmpInfo.weekRankScore = jsonData[i].weekRankScore;
                tmpInfo.weekRankReward = jsonData[i].weekRankReward.split(",");
                matchRewardData[tmpInfo.index] = tmpInfo;
            }
        }
    }
    else {
        cc.error("读取matchRewardData失败");
    }
};
/**
 * 排行榜
 * @param jsonFile
 */
DataMgr.readRankData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var rankData = cf.Data.RANK_DATA;
        if (rankData && rankData.length > 0) {
            cc.log("readRankData");
            return;
        }
        cc.log("读取readRankData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.index = jsonData[i].index;
                tmpInfo.rank_name = jsonData[i].rank_name;
                tmpInfo.rank_type = jsonData[i].rank_type;
                tmpInfo.rank_rank = jsonData[i].rank_rank;
                tmpInfo.rank_icon = jsonData[i].rank_icon;
                tmpInfo.rank_promp = jsonData[i].rank_promp;

                rankData[tmpInfo.index] = tmpInfo;
            }
        }
    }
    else {
        cc.error("读取readRankData失败");
    }
};

/**
 * 读取轮盘数据
 * @param jsonFile
 */
DataMgr.readLuckDrawData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var luckDrawData = cf.Data.LUCKDRAW_DATA;
        if (luckDrawData && luckDrawData.length > 0) {
            cc.log("luckDrawData");
            return;
        }
        cc.log("读取luckDrawData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.index = jsonData[i].index;
                tmpInfo.luckDrawNumber = jsonData[i].luckDrawNumber;
                tmpInfo.luckDrawPay = cf.parseString(jsonData[i].luckDrawPay);
                luckDrawData.push(tmpInfo);
            }
        }
    } else {
        cc.error("读取luckDrawData失败");
    }
};

/**
 * 读取serverConfig
 * @param jsonFile
 */
DataMgr.readServerConfig = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var severConfigData = cf.Data.SEVERCONFIGDATA;
        if (severConfigData && severConfigData.length > 0) {
            cc.log("serverConfigData");
            return;
        }
        cc.log("读取serverConfigData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.unlockFortLv = jsonData[i].unlockFortLv;
                severConfigData.push(tmpInfo);
            }
        }
    } else {
        cc.error("读取serverConfigData失败");
    }
};

DataMgr.readDropMissile = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);

    if (jsonData) {
        var dropMissileData = cf.Data.DROP_MISSILE_DATA;
        if (dropMissileData && dropMissileData.length > 0) {
            cc.log("dropMissileData");
            return;
        }
        cc.log("读取dropMissileData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};
                tmpInfo.id = jsonData[i].id;
                tmpInfo.num = jsonData[i].num;
                dropMissileData[tmpInfo.id] = tmpInfo;
            }
        }
    } else {
        cc.error("读取serverConfigData失败");
    }
};