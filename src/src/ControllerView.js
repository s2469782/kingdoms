var AppController = AppController || {};

AppController.init = function () {
    var sys = cc.sys;

    //获取服务器列表
    cf.HTTP_URL = cf.HTTP_URL_CONFIG[GameConfig.serverIdx];
    cf.HTTP_SERVER_STATE_URL = cf.HTTP_SERVER_STATE_URL_CONFIG[GameConfig.serverIdx];

    //当前实际分辨率
    var frameSize = cc.winSize;
    cf.WINSIZE_WIDTH = frameSize.width;
    cf.WINSIZE_HEIGHT = frameSize.height;
    cc.log("winSize.width = " + frameSize.width + ", winSize.height = " + frameSize.height);

    //游戏设计分辨率
    cc.log("virSize.width = " + cf.DESIGN_WIDTH + ", virSize.height = " + cf.DESIGN_HEIGHT);

    cc.view.enableRetina(false);

    //加载框
    if (document.getElementById("cocosLoading")) //If referenced loading.js, please remove it
        document.body.removeChild(document.getElementById("cocosLoading"));

    cc.view.enableRetina(true);
    //自动全屏
    if (sys.isMobile &&
        sys.browserType !== sys.BROWSER_TYPE_BAIDU &&
        sys.browserType !== sys.BROWSER_TYPE_WECHAT) {
        //cc.view.enableAutoFullScreen(true);
    }
    // Adjust viewport meta
    cc.view.adjustViewPort(true);
    cc.view.setOrientation(cc.ORIENTATION_LANDSCAPE);
    cc.view.setDesignResolutionSize(cf.DESIGN_WIDTH, cf.DESIGN_HEIGHT, cc.ResolutionPolicy.SHOW_ALL);
    cc.view.resizeWithBrowserSize(true);

    cf.RATIO_X = 1;
    cf.RATIO_Y = 1;

    //初始化资源
    initResConfig();

    //设置全局纹理格式
    //暂时设定为16位
    // cf.setDefaultTextureFormat(cc.Texture2D.PIXEL_FORMAT_RGBA4444);

    //注册读取proto
    cc.loader.register(["proto", "atlas", "pkm"], cc._txtLoader);

    //错误管理器
    cf.ErrorMgr = ErrorMgr.getInstance();

    //注册全局通知
    cc.director.setNotificationNode(new NotificationScene());

    SDKHelper.init();   
    cf.SaveDataMgr = DataSaveMgr.getInstance();
    cf.SoundMgr = SoundMgr.getInstance();

    //检测IE浏览器和渲染模式是否为Canvas
    if (cc.sys.browserType == cc.sys.BROWSER_TYPE_IE || cc._renderType == cc.game.RENDER_TYPE_CANVAS) {
        cf.SceneMgr.runScene(cf.SceneFlag.LOGO, null);
    } else {
        cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
    }
};
