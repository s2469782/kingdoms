var Res = null;
//预加载资源
var g_resLoginScene = [];
var g_resLobbyScene = [];
var g_resGameScene = [];

/**
 * 更具版本创建文件路径
 * @param path
 * @returns {*}
 */
var createVerPath = function (path, isMd5) {
    if (isMd5 === undefined)
        isMd5 = true;
    var _path = GameConfig.resPath + path;
    if (!cc.sys.isNative) {
        var _md5 = UpdateRes[_path];
        if (_md5) {
            return _path + "?v=" + _md5;
        }
    } else {
        if (isMd5) {
            if (cc.sys.os === cc.sys.OS_ANDROID || cc.sys.os === cc.sys.OS_IOS) {
                return mappingRes[_path];
            }
        }
    }
    return _path;
};


/**
 * 预载资源
 * @param resArray 预载数组
 * @param resData 资源数据
 */
var preLoadRes = function (resArray, resData) {
    for (var i in resData) {
        resArray.push(resData[i]);
        cc.log(resData[i]);
    }
    cc.log(resData.length + "resData" + resArray.length + "resArray")
    cc.log(g_resGameScene.length + "g_resGameScene");
};

/**
 * 初始化资源配置
 */
var initResConfig = function () {
    Res = {
        BKImage: createVerPath("picture/lobbyBg" + GameConfig.suffix_jpg),
        LoginImage: createVerPath("picture/login" + GameConfig.suffix_jpg),
        LoadingImage: createVerPath("picture/loading" + GameConfig.suffix_jpg),
        BossImage: createVerPath("picture/Background_001" + GameConfig.suffix_jpg),
        //登录场景资源列表
        LOGIN: {
            scale9Png: createVerPath("picture/scale9" + GameConfig.suffix),
            scale9Plist: createVerPath("picture/scale9.plist"),
            resourcePng: createVerPath("picture/resource" + GameConfig.suffix),
            resourcePlist: createVerPath("picture/resource.plist"),
            LanguageDataJson: createVerPath("table/LanguageData.json"),
            commonPng: createVerPath("picture/common" + GameConfig.suffix),
            commonPlist: createVerPath("picture/common.plist"),
            newPng: createVerPath("picture/new" + GameConfig.suffix),
            newPlist: createVerPath("picture/new.plist"),
            logoImage: createVerPath("picture/logo" + GameConfig.suffix),
            AnimateDataJson: createVerPath("table/AnimateData.json"),
            SoundDataJson: createVerPath("table/soundData.json"),
            loginPng: createVerPath("picture/login" + GameConfig.suffix),
            loginPlist: createVerPath("picture/login.plist"),
            CannonLevelDataJson: createVerPath("table/CannonLevelData.json"),

            fishLibPng: createVerPath("picture/fishLibrary" + GameConfig.suffix),
            fishLibPlist: createVerPath("picture/fishLibrary.plist"),

            water_behind: createVerPath("picture/water_behind" + GameConfig.suffix),

            music_start: createVerPath("sound/music_start.mp3"),

            //简体字
            gb_font: {
                type: "font",
                name: "Microsoft Yahei",
                srcs: []
            }
        },

        //大厅场景资源列表
        LOBBY: {
            music_hall: createVerPath("sound/music_hall.mp3"),

            hall_ui01_Atlas: createVerPath("animate/hall_ui01.atlas"),
            hall_ui01_Json: createVerPath("animate/hall_ui01.json"),
            hall_ui01_Png: createVerPath("animate/hall_ui01" + GameConfig.suffix),

            hallPlist: createVerPath("picture/hall.plist"),
            hallPng: createVerPath("picture/hall" + GameConfig.suffix),


            DongZhuo_Atlas: createVerPath("animate/DongZhuo.atlas"),
            DongZhuo_Json: createVerPath("animate/DongZhuo.json"),
            DongZhuo_Png: createVerPath("animate/DongZhuo" + GameConfig.suffix),

            LuBu_Atlas: createVerPath("animate/LuBu.atlas"),
            LuBu_Json: createVerPath("animate/LuBu.json"),
            LuBu_Png: createVerPath("animate/LuBu"+GameConfig.suffix),

            DiaoChan_Atlas: createVerPath("animate/DiaoChan.atlas"),
            DiaoChan_Json: createVerPath("animate/DiaoChan.json"),
            DiaoChan_Png: createVerPath("animate/DiaoChan"+GameConfig.suffix),


            bg_effect_Atlas: createVerPath("animate/bg_effects.atlas"),
            bg_effect_Json: createVerPath("animate/bg_effects.json"),
            bg_effect_Png: createVerPath("animate/bg_effects" + GameConfig.suffix),

            red_reward_Atlas: createVerPath("animate/reward.atlas"),
            red_reward_Json: createVerPath("animate/reward.json"),
            red_reward_Png: createVerPath("animate/reward" + GameConfig.suffix),
            

            bonusDrawDataJson: createVerPath("table/bonusDrawData.json"),
            
            cannonPlist: createVerPath("picture/cannon.plist"),
            cannonPng: createVerPath("picture/cannon" + GameConfig.suffix),
           
            
            font_resource_083: createVerPath("font/font-001.fnt"),
            font_resource_084: createVerPath("font/font-002.fnt"),
            font_resource_085: createVerPath("font/font-003.fnt"),
            font_resource_086: createVerPath("font/font-004.fnt"),
            font_resource_087: createVerPath("font/font-005.fnt"),
            font_resource_088: createVerPath("font/font-006.fnt"),
            font_resource_089: createVerPath("font/font-007.fnt"),
            font_resource_090: createVerPath("font/font-008.fnt"),
            font_resource_091: createVerPath("font/font-009.fnt"),
            font_resource_092: createVerPath("font/randomLotteryNum-export.fnt"),
            font_resource_093: createVerPath("font/font_text.fnt"),

            ItemDataJson: createVerPath("table/ItemData.json"),
            rewardDataJson: createVerPath("table/rewardData.json"),
            CannonDataJson: createVerPath("table/CannonData.json"),
            vipDataJson: createVerPath("table/vipData.json"),
            skeletonDataJson: createVerPath("table/skeletonAnimationData.json"),
            RoomDataJson: createVerPath("table/RoomData.json"),
            HeadPortraitDataJson: createVerPath("table/HeadPortraitData.json"),
            ShopDataJson: createVerPath("table/shopData.json"),
            MainTaskData: createVerPath("table/mainTaskData.json"),
            roleLevelDataJson: createVerPath("table/roleLevelData.json"),
            costDataJson: createVerPath("table/costData.json"),
            matchRewardDataJson: createVerPath("table/matchRewardData.json"),
            rankDataJson: createVerPath("table/rankData.json"),
            FishDataJson: createVerPath("table/FishData.json"),

            loginRewardDataJson: createVerPath("table/loginRewardData.json"),

            luckDrawDataJson: createVerPath("table/luckDrawDate.json"),

            particle_001_png: createVerPath("particle/particle_001" + GameConfig.suffix, false),
            particle_001_plist: createVerPath("particle/particle_001.plist", false),
            particle_002_png: createVerPath("particle/particle_002" + GameConfig.suffix, false),
            particle_002_plist: createVerPath("particle/particle_002.plist", false),
            particle_003_png: createVerPath("particle/particle_003" + GameConfig.suffix, false),
            particle_003_plist: createVerPath("particle/particle_003.plist", false),
            goldPng: createVerPath("particle/particle_gold" + GameConfig.suffix, false),
            gold_boom_plist: createVerPath("particle/particle_005.plist", false),
            get_gold_plist: createVerPath("particle/particle_004.plist", false),

            FishLibDataJson: createVerPath("table/fishLibrary.json"),

            helperImg: createVerPath("picture/helper" + GameConfig.suffix_jpg),

        },

        //游戏主场景资源
        GAME: {
            music_primary: createVerPath("sound/music_primary.mp3"),
            music_middle: createVerPath("sound/music_middle.mp3"),
            music_high: createVerPath("sound/music_high.mp3"),
            sprite01Plist: createVerPath("picture/sprite01.plist"),
            sprite01Png: createVerPath("picture/sprite01" + GameConfig.suffix),
            sprite02Plist: createVerPath("picture/sprite02.plist"),
            sprite02Png: createVerPath("picture/sprite02" + GameConfig.suffix),
            sprite02_1Plist: createVerPath("picture/sprite02_1.plist"),
            sprite02_1Png: createVerPath("picture/sprite02_1" + GameConfig.suffix),
            sprite02_2Plist: createVerPath("picture/sprite02_2.plist"),
            sprite02_2Png: createVerPath("picture/sprite02_2" + GameConfig.suffix),
            sprite03Plist: createVerPath("picture/sprite03.plist"),
            sprite03Png: createVerPath("picture/sprite03" + GameConfig.suffix),
            sprite03_1Plist: createVerPath("picture/sprite03_1.plist"),
            sprite03_1Png: createVerPath("picture/sprite03_1" + GameConfig.suffix),
            sprite04Plist: createVerPath("picture/sprite04.plist"),
            sprite04Png: createVerPath("picture/sprite04" + GameConfig.suffix),
            sprite04_1Plist: createVerPath("picture/sprite04_1.plist"),
            sprite04_1Png: createVerPath("picture/sprite04_1" + GameConfig.suffix),
            sprite05Plist: createVerPath("picture/sprite05.plist"),
            sprite05Png: createVerPath("picture/sprite05" + GameConfig.suffix),
            sprite05_1Plist: createVerPath("picture/sprite05_1.plist"),
            sprite05_1Png: createVerPath("picture/sprite05_1" + GameConfig.suffix),
            sprite06Plist: createVerPath("picture/sprite06.plist"),
            sprite06Png: createVerPath("picture/sprite06" + GameConfig.suffix),
            sprite07Plist: createVerPath("picture/sprite07.plist"),
            sprite07Png: createVerPath("picture/sprite07" + GameConfig.suffix),
            sprite08Plist: createVerPath("picture/sprite08.plist"),
            sprite08Png: createVerPath("picture/sprite08" + GameConfig.suffix),
            sprite09Plist: createVerPath("picture/sprite09.plist"),
            sprite09Png: createVerPath("picture/sprite09" + GameConfig.suffix),
            sprite10Plist: createVerPath("picture/sprite10.plist"),
            sprite10Png: createVerPath("picture/sprite10" + GameConfig.suffix),
            skillFishPlist: createVerPath("picture/skillFish.plist"),
            skillFishPng: createVerPath("picture/skillFish" + GameConfig.suffix),
            brustPlist: createVerPath("picture/brust.plist"),
            brustPng: createVerPath("picture/brust" + GameConfig.suffix),
            jpmoney1Plist: createVerPath("picture/jpmoney1.plist"),
            jpmoney1Png: createVerPath("picture/jpmoney1" + GameConfig.suffix),
            jpmoney1_1Plist: createVerPath("picture/jpmoney1_1.plist"),
            jpmoney1_1Png: createVerPath("picture/jpmoney1_1" + GameConfig.suffix),
            jpmoney2Plist: createVerPath("picture/jpmoney2.plist"),
            jpmoney2Png: createVerPath("picture/jpmoney2" + GameConfig.suffix),
            jpmoney2_1Plist: createVerPath("picture/jpmoney2_1.plist"),
            jpmoney2_1Png: createVerPath("picture/jpmoney2_1" + GameConfig.suffix),

            effects_Atlas: createVerPath("animate/effects.atlas"),
            effects_Json: createVerPath("animate/effects.json"),
            effects_Png: createVerPath("animate/effects" + GameConfig.suffix),

            skill_txt_Atlas: createVerPath("animate/skill_txt.atlas"),
            skill_txt_Json: createVerPath("animate/skill_txt.json"),
            skill_txt_Png: createVerPath("animate/skill_txt" + GameConfig.suffix),

            real_dynamic_Atlas: createVerPath("animate/Reel_dynamic.atlas"),
            real_dynamic_Json: createVerPath("animate/Reel_dynamic.json"),
            real_dynamic_Png: createVerPath("animate/Reel_dynamic" + GameConfig.suffix),

            bonusAni_Atlas: createVerPath("animate/huodecaijing.atlas"),
            bonusAni_Json: createVerPath("animate/huodecaijing.json"),
            bonusAni_Png: createVerPath("animate/huodecaijing" + GameConfig.suffix),

            hammer30_Atlas: createVerPath("animate/hammer30.atlas"),
            hammer30_Json: createVerPath("animate/hammer30.json"),
            hammer30_Png: createVerPath("animate/hammer30" + GameConfig.suffix),

            hammer31_Atlas: createVerPath("animate/hammer31.atlas"),
            hammer31_Json: createVerPath("animate/hammer31.json"),
            hammer31_Png: createVerPath("animate/hammer31" + GameConfig.suffix),

            bonusAni_Atlas: createVerPath("animate/huodecaijing.atlas"),
            bonusAni_Json: createVerPath("animate/huodecaijing.json"),
            bonusAni_Png: createVerPath("animate/huodecaijing" + GameConfig.suffix),

            MaChao_Atlas: createVerPath("animate/MaChao.atlas"),
            MaChao_Json: createVerPath("animate/MaChao.json"),
            MaChao_Png: createVerPath("animate/MaChao" + GameConfig.suffix),

            DongZhuo_Atlas: createVerPath("animate/DongZhuo.atlas"),
            DongZhuo_Json: createVerPath("animate/DongZhuo.json"),
            DongZhuo_Png: createVerPath("animate/DongZhuo" + GameConfig.suffix),

            skill_MaChao_Atlas: createVerPath("animate/skill_MaChao.atlas"),
            skill_MaChao_Json: createVerPath("animate/skill_MaChao.json"),
            skill_MaChao_Png: createVerPath("animate/skill_MaChao" + GameConfig.suffix),

            sunJian_Atlas: createVerPath("animate/SunJian.atlas"),
            sunJian_Json: createVerPath("animate/SunJian.json"),
            sunJian_Png: createVerPath("animate/SunJian" + GameConfig.suffix),

            skill_sunJian_Atlas: createVerPath("animate/skill_SunJian.atlas"),
            skill_sunJian_Json: createVerPath("animate/skill_SunJian.json"),
            skill_sunJian_Png: createVerPath("animate/skill_SunJian" + GameConfig.suffix),

            boss_coin_Atlas: createVerPath("animate/boss_coin.atlas"),
            boss_coin_Json: createVerPath("animate/boss_coin.json"),
            boss_coin_Png: createVerPath("animate/boss_coin" + GameConfig.suffix),

            stage_num_Atlas: createVerPath("animate/numeral.atlas"),
            stage_num_Json: createVerPath("animate/numeral.json"),
            stage_num_Png: createVerPath("animate/numeral" + GameConfig.suffix),

            skill_txt_light_Atlas: createVerPath("animate/skill_txt_light.atlas"),
            skill_txt_light_Json: createVerPath("animate/skill_txt_light.json"),
            skill_txt_light_Png: createVerPath("animate/skill_txt_light" + GameConfig.suffix),

            skill_XiahouDun_Atlas: createVerPath("animate/skill_XiahouDun.atlas"),
            skill_XiahouDun_Json: createVerPath("animate/skill_XiahouDun.json"),
            skill_XiahouDun_Png: createVerPath("animate/skill_XiahouDun" + GameConfig.suffix),

            XiahouDun_Atlas: createVerPath("animate/XiahouDun.atlas"),
            XiahouDun_Json: createVerPath("animate/XiahouDun.json"),
            XiahouDun_Png: createVerPath("animate/XiahouDun" + GameConfig.suffix),

            boss_text_Atlas: createVerPath("animate/boss_text.atlas"),
            boss_text_Json: createVerPath("animate/boss_text.json"),
            boss_text_Png: createVerPath("animate/boss_text" + GameConfig.suffix),


            statusBg_Atlas: createVerPath("animate/statusBg.atlas"),
            statusBg_Json: createVerPath("animate/statusBg.json"),
            statusBg_Png: createVerPath("animate/statusBg" + GameConfig.suffix),

            status_Atlas: createVerPath("animate/status.atlas"),
            status_Json: createVerPath("animate/status.json"),
            status_Png: createVerPath("animate/status" + GameConfig.suffix),

            skill_money1_Atlas: createVerPath("animate/skill_money1.atlas"),
            skill_money1_Json: createVerPath("animate/skill_money1.json"),
            skill_money1_Png: createVerPath("animate/skill_money1" + GameConfig.suffix),

          
            status_Atlas: createVerPath("animate/status.atlas"),
            status_Json: createVerPath("animate/status.json"),
            status_Png: createVerPath("animate/status" + GameConfig.suffix),


            spark_Atlas: createVerPath("animate/spark.atlas"),
            spark_Json: createVerPath("animate/spark.json"),
            spark_Png: createVerPath("animate/spark" + GameConfig.suffix),


            fisheryPng: createVerPath("picture/fishery" + GameConfig.suffix),
            fisheryPlist: createVerPath("picture/fishery.plist"),
            
            wave_Atlas: createVerPath("animate/bg_wave.atlas"),
            wave_Json: createVerPath("animate/bg_wave.json"),
            wave_Png: createVerPath("animate/bg_wave" + GameConfig.suffix),

            moneywave_Atlas: createVerPath("animate/money_wave.atlas"),
            moneywave_Json: createVerPath("animate/money_wave.json"),
            moneywave_Png: createVerPath("animate/money_wave" + GameConfig.suffix),

            ef_Atlas: createVerPath("animate/bg_ef.atlas"),
            ef_Json: createVerPath("animate/bg_ef.json"),
            ef_Png: createVerPath("animate/bg_ef" + GameConfig.suffix),

            dice_Atlas: createVerPath("animate/dice.atlas"),
            dice_Json: createVerPath("animate/dice.json"),
            dice_Png: createVerPath("animate/dice" + GameConfig.suffix),

            lv17_Atlas: createVerPath("animate/lv17.atlas"),
            lv17_Json: createVerPath("animate/lv17.json"),
            lv17_Png: createVerPath("animate/lv17" + GameConfig.suffix),

            lv18_Atlas: createVerPath("animate/lv18.atlas"),
            lv18_Json: createVerPath("animate/lv18.json"),
            lv18_Png: createVerPath("animate/lv18" + GameConfig.suffix),

            lv19_Atlas: createVerPath("animate/lv19.atlas"),
            lv19_Json: createVerPath("animate/lv19.json"),
            lv19_Png: createVerPath("animate/lv19" + GameConfig.suffix),

            lv20_Atlas: createVerPath("animate/lv20.atlas"),
            lv20_Json: createVerPath("animate/lv20.json"),
            lv20_Png: createVerPath("animate/lv20" + GameConfig.suffix),

            lv21_Atlas: createVerPath("animate/lv21.atlas"),
            lv21_Json: createVerPath("animate/lv21.json"),
            lv21_Png: createVerPath("animate/lv21" + GameConfig.suffix),

            lv22_Atlas: createVerPath("animate/lv22.atlas"),
            lv22_Json: createVerPath("animate/lv22.json"),
            lv22_Png: createVerPath("animate/lv22" + GameConfig.suffix),

            lv23_Atlas:createVerPath("animate/DiaoXain_2.atlas"),
            lv23_Json:createVerPath("animate/DiaoXain_2.json"),
            lv23_Png:createVerPath("animate/DiaoXain_2" + GameConfig.suffix),

            lv16_Atlas:createVerPath("animate/caiweiji_ka.atlas"),
            lv16_Json:createVerPath("animate/caiweiji_ka.json"),
            lv16_Png:createVerPath("animate/caiweiji_ka" + GameConfig.suffix),

            
            FishGroupDataJson: createVerPath("table/FishGroupData.json"),
            PathDataJson: createVerPath("table/PathData.json"),
            FishCollisionDataJson: createVerPath("table/FishCollisionData.json"),
            BulletCollisionDataJson: createVerPath("table/BulletCollisionData.json"),
            roleLevelDataJson: createVerPath("table/roleLevelData.json"),
            DoleDataJson: createVerPath("table/DoleData.json"),
            onlineRewardDataJson: createVerPath("table/onlineRewardData.json"),
            attrBallSkillDataJson: createVerPath("table/AttrBallSkillData.json"),

            bulletPlist: createVerPath("picture/bullet.plist"),
            bulletPng: createVerPath("picture/bullet" + GameConfig.suffix),

            bullet1Plist: createVerPath("picture/bullet1.plist"),
            bullet1Png: createVerPath("picture/bullet1" + GameConfig.suffix),

            goldAnimatePlist: createVerPath("picture/goldAnimate.plist"),
            goldAnimatePng: createVerPath("picture/goldAnimate" + GameConfig.suffix),

            silverAnimatePlist: createVerPath("picture/silverAnimate.plist"),
            silverAnimatePng: createVerPath("picture/silverAnimate" + GameConfig.suffix),


            skillDataJson: createVerPath("table/skillData.json"),
            playMethodDataJson: createVerPath("table/goldPlayInfoData.json"),
            fishTideDataJson: createVerPath("table/FishTideData.json"),
            fishArrayDataJson: createVerPath("table/FishArrayData.json"),
            pokerDataJson: createVerPath("table/goldPlay2Data.json"),

            jpfortress_Atlas: createVerPath("animate/jpfortress.atlas"),
            jpfortress_Json: createVerPath("animate/jpfortress.json"),
            jpfortress_Png: createVerPath("animate/jpfortress"+GameConfig.suffix),

            jpfortressall_Atlas: createVerPath("animate/jpfortressall.atlas"),
            jpfortressall_Json: createVerPath("animate/jpfortressall.json"),
            jpfortressall_Png: createVerPath("animate/jpfortressall"+GameConfig.suffix),

            jpbg_Atlas: createVerPath("animate/jpbg.atlas"),
            jpbg_Json: createVerPath("animate/jpbg.json"),
            jpbg_Png: createVerPath("animate/jpbg"+GameConfig.suffix),

            jpreward_Atlas: createVerPath("animate/jpreward.atlas"),
            jpreward_Json: createVerPath("animate/jpreward.json"),
            jpreward_Png: createVerPath("animate/jpreward"+GameConfig.suffix),

            LuBu_Atlas: createVerPath("animate/LuBu.atlas"),
            LuBu_Json: createVerPath("animate/LuBu.json"),
            LuBu_Png: createVerPath("animate/LuBu"+GameConfig.suffix),

            DiaoChan_Atlas: createVerPath("animate/DiaoChan.atlas"),
            DiaoChan_Json: createVerPath("animate/DiaoChan.json"),
            DiaoChan_Png: createVerPath("animate/DiaoChan"+GameConfig.suffix),
           
            boss_appear_Atlas: createVerPath("animate/boss_appear.atlas"),
            boss_appear_Json: createVerPath("animate/boss_appear.json"),
            boss_appear_Png: createVerPath("animate/boss_appear"+GameConfig.suffix),
            

            skill_bg_Atlas: createVerPath("animate/skill_bg.atlas"),
            skill_bg_Json: createVerPath("animate/skill_bg.json"),
            skill_bg_Png: createVerPath("animate/skill_bg"+GameConfig.suffix),

            GuanYu_Atlas: createVerPath("animate/GuanYu.atlas"),
            GuanYu_Json: createVerPath("animate/GuanYu.json"),
            GuanYu_Png: createVerPath("animate/GuanYu"+GameConfig.suffix),

            skill_ZhaoYun_Atlas: createVerPath("animate/skill_ZhaoYun.atlas"),
            skill_ZhaoYun_Json: createVerPath("animate/skill_ZhaoYun.json"),
            skill_ZhaoYun_Png: createVerPath("animate/skill_ZhaoYun"+GameConfig.suffix),

            ZhaoYun_Atlas: createVerPath("animate/ZhaoYun.atlas"),
            ZhaoYun_Json: createVerPath("animate/ZhaoYun.json"),
            ZhaoYun_Png: createVerPath("animate/ZhaoYun"+GameConfig.suffix),

            skill_GuanYu_Atlas: createVerPath("animate/skill_GuanYu.atlas"),
            skill_GuanYu_Json: createVerPath("animate/skill_GuanYu.json"),
            skill_GuanYu_Png: createVerPath("animate/skill_GuanYu"+GameConfig.suffix),

            skill_Shu_Atlas: createVerPath("animate/skill_Shu.atlas"),
            skill_Shu_Json: createVerPath("animate/skill_Shu.json"),
            skill_Shu_Png: createVerPath("animate/skill_Shu"+GameConfig.suffix),

            Shu_Atlas: createVerPath("animate/Shu.atlas"),
            Shu_Json: createVerPath("animate/Shu.json"),
            Shu_Png: createVerPath("animate/Shu"+GameConfig.suffix),

            LiuBei_Atlas: createVerPath("animate/LiuBei.atlas"),
            LiuBei_Json: createVerPath("animate/LiuBei.json"),
            LiuBei_Png: createVerPath("animate/LiuBei"+GameConfig.suffix),

            ZhangFei_Atlas: createVerPath("animate/ZhangFei.atlas"),
            ZhangFei_Json: createVerPath("animate/ZhangFei.json"),
            ZhangFei_Png: createVerPath("animate/ZhangFei"+GameConfig.suffix),


            skill_up_Atlas: createVerPath("animate/skill_up.atlas"),
            skill_up_Json: createVerPath("animate/skill_up.json"),
            skill_up_Png: createVerPath("animate/skill_up"+GameConfig.suffix),

            skill_center_Atlas: createVerPath("animate/skill_center.atlas"),
            skill_center_Json: createVerPath("animate/skill_center.json"),
            skill_center_Png: createVerPath("animate/skill_center"+GameConfig.suffix),

            scrolls_Atlas: createVerPath("animate/scrolls.atlas"),
            scrolls_Json: createVerPath("animate/scrolls.json"),
            scrolls_Png: createVerPath("animate/scrolls"+GameConfig.suffix),

            localTip_Atlas: createVerPath("animate/animate_001.atlas"),
            localTip_Json: createVerPath("animate/animate_001.json"),
            localTip_Png: createVerPath("animate/animate_001" + GameConfig.suffix),

            HideCoinDataJson: createVerPath("table/hideCoinsData.json"),
            SeverConfigDataJson: createVerPath("table/severConfigData.json"),

            DropMissileJson: createVerPath("table/DropMissile.json"),

            dragonBallPng: createVerPath("picture/dragonBall" + GameConfig.suffix),
            dragonBallPlist: createVerPath("picture/dragonBall.plist"),


            gamingTable_Altas: createVerPath("animate/gaming_table.atlas"),
            gamingTable_Json: createVerPath("animate/gaming_table.json"),
            gamingTable_Png: createVerPath("animate/gaming_table" + GameConfig.suffix),

            dicecup_Altas: createVerPath("animate/dicecup.atlas"),
            dicecup_Json: createVerPath("animate/dicecup.json"),
            dicecup_Png: createVerPath("animate/dicecup" + GameConfig.suffix),

            god_wealth_Altas: createVerPath("animate/god_wealth.atlas"),
            god_wealth_Json: createVerPath("animate/god_wealth.json"),
            god_wealth_Png: createVerPath("animate/god_wealth" + GameConfig.suffix),

            god_of_wealth_Altas: createVerPath("animate/god_of_wealth.atlas"),
            god_of_wealth_Json: createVerPath("animate/god_of_wealth.json"),
            god_of_wealth_Png: createVerPath("animate/god_of_wealth" + GameConfig.suffix),

            god_of_wealth2_Altas: createVerPath("animate/god_of_wealth2.atlas"),
            god_of_wealth2_Json: createVerPath("animate/god_of_wealth2.json"),
            god_of_wealth2_Png: createVerPath("animate/god_of_wealth2" + GameConfig.suffix),

            larbar_Altas: createVerPath("animate/larbar.atlas"),
            larbar_Json: createVerPath("animate/larbar.json"),
            larbar_Png: createVerPath("animate/larbar" + GameConfig.suffix),

            bonus_Altas: createVerPath("animate/bonus.atlas"),
            bonus_Json: createVerPath("animate/bonus.json"),
            bonus_Png: createVerPath("animate/bonus" + GameConfig.suffix),

            BonusPng: createVerPath("picture/Bonus" + GameConfig.suffix),
            BonusPlist: createVerPath("picture/Bonus.plist")
        }
    };

    preLoadRes(g_resLoginScene, Res.LOGIN);
    preLoadRes(g_resLobbyScene, Res.LOBBY);
    preLoadRes(g_resGameScene, Res.GAME);
};



