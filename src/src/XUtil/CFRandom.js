cf.Random = cc.Class.extend({
    _seed: 0,
    _count: 0,
    ctor: function (seed) {
        this._seed = seed;
        this._count = 0;
    },

    /**
     * 随机整数
     * @param min
     * @param max
     * @returns {number}
     */
    nextInt: function (min, max) {
        max = max || 1;
        min = min || 0;
        this._seed = (this._seed * 9301 + 49297) % 233280;
        var rnd = this._seed / 233280.0;

        this._count++;
        return Math.ceil(min + rnd * (max - min));
    },

    getCount:function () {
        return this._count;
    }
});