var cf = cf || {};
/**
 * 设计分辨率
 */
cf.DESIGN_WIDTH = 1280;
cf.DESIGN_HEIGHT = 720;

cf.WINSIZE_WIDTH = 0;
cf.WINSIZE_HEIGHT = 0;

//随机loading
cf.RAND_LOADER = 0;

//服务器列表
cf.HTTP_URL_CONFIG = [
    
    "https://server.suobei237.com",//正式服
    "https://server.xunihouse.com",//测试服
    "http://35.229.203.14/"//本地
    //"http://34.92.100.119:38786"
];
//服务器状态列表
cf.HTTP_SERVER_STATE_URL_CONFIG = [
    "https://h5.suobei237.com/onlineServerState/checkServerState.php",//正式服
    "https://h5.xunihouse.com/onlineServerState/checkServerState.php",//测试服
    //"http://192.168.199.90/onlineServerState/checkServerState.php"//本地
    "http://35.229.203.14/onlineServerState/checkServerState.php"
];
cf.d = "";
cf.HTTP_URL = "";
cf.HTTP_SERVER_STATE_URL = "";

/**
 * 屬性技能
 */
cf.btnDisable = false;
cf.dragon_gold = false;
cf.dragon_wood = false;
cf.goldwoodDisable = false;
cf.goldBullet = 50;
cf.woodBullet = 30;
/**
 * 坐标转换比率
 */
cf.RATIO_X = 1;
cf.RATIO_Y = 1;

/**
 * 技能魚 屬性技能參數
 */
cf.SkillfishBoolean = false;
/**
 * 登录类型
 */
cf.LoginType = {
    ACCOUNT: 1,      //< 账号登录
    GUEST: 2,        //< 游客登录
    OTHER: 3,         //< 其他登录，例如微信公众号
    GAME_ACCOUNT: 5,     //< 游戏内登录
    H5_ACCOUNT: 6
};

/**
 * 数据表
 */
cf.Data = {
    FISH_DATA: [],                      //< 鱼数据
    FISH_GROUP_DATA: [],                //< 鱼群数据
    ANIMATE_DATA: [],                   //< 动画数据
    PATH_DATA: [],                      //< 路径数据
    FISH_COLLISION_DATA: [],            //< 鱼碰撞数据
    BULLET_COLLISION_DATA: [],          //< 子弹碰撞数据
    ROOM_DATA: [],                      //< 房间数据
    ITEM_DATA: [],                      //< 道具数据
    CANNON_LEVEL_DATA: [],              //< 炮倍等级数据
    LOBBYUI_DATA: [],                   //< 大厅UI的数据
    SHOPUI_DATA: [],                    //< 商店UI数据
    ROLE_LEVEL_DATA: [],                //< 玩家等级数据
    BACKPACK_DATA: [],                  //< 背包数据
    LOGINREWARD_DATA: [],               //< 每日登录奖励数据
    EMAILUI_DATA: [],                   //< 邮件UI信息
    REWARD_DATA: [],                    //< 每日登录奖励列表
    DAY_TASK_DATA: [],                  //< 日常任务数据
    WEEK_TASK_DATA: [],                 //< 周常任务数据
    GROW_TASK_DATA: [],                 //< 成长任务数据
    TASK_GIFT_DATA: [],                 //< 任务礼包数据
    DOLE_DATA: [],                      //< 救济金信息
    ONLINEREWARD_DATA: [],              //< 在线奖励信息
    BONUS_DRAW_DATA: [],                //< 彩金抽奖信息
    SKILLDATA: [],                      //< 技能数据
    CANNON_DATA: [],                    //< 炮台数据
    VIP_DATA: [],                       //< VIP数据
    HEADPORTRAIT_DATA: [],              //< 头像数据
    SHOP_DATA: [],                      //< 商品数据
    SKELETON_DATA: [],                  //< 骨骼动画数据
    PRIZE_FISH_DATA: [],                //< 彩金鱼展示数据
    SOUND_DATA: [],                     //< 音乐数据   
    COST_DATA: [],                      //< 获取消费数据
    TREASUREHUNT_DATA: [],              //< 寻宝数据
    PLAYMETHOD_DATA: [],                //< 玩法信息数据
    POKER_DATA: [],                     //< 卡牌玩法数据
    GAME_VOICE_DATA: [],                //< 游戏语音特效
    FISHTIDEDATA: [],                   //< 鱼潮数据
    FISHARRAYDATA: [],                  //< 阵列数据
    HIDE_GOLD_DATA: [],                  //< 隐藏金币数据
    MATCH_REWARD_DATA: [],                  //< 大奖赛奖励表
    RANK_DATA: [],                        //< 排行榜奖励表
    ATTRBALLSKILL_DATA: [],
    LUCKDRAW_DATA: [],                    //< 轮盘数据
    SEVERCONFIGDATA: [],                 //< 其他数据
    DROP_MISSILE_DATA: []
};

/**
 * 层级索引
 */
cf.Zoder = {
    BK: -100,
    FISH: -90,
    BULLET: -80,
    PLAYER: -70,
    EFFECT: -60,
    UI: -50,
    MAX: 100
};

/**
 * 动画类型
 */
cf.AnimateType = {
    FRAME: 0,        //< 帧动画
    BONES: 1         //< 骨骼动画
};

/**
 * 路径类型
 */
cf.PathType = {
    LINE: 1,         //< 直线路径
    BEZIER: 2,       //< 贝塞尔路径
    CURVE: 3         //< 曲线路径
};

/**
 * 鱼类型
 */
cf.FishType = {
    COMMON: 0,          //< 普通鱼
    BONUS: 1,           //< 彩金鱼
    SPECIAL: 2,         //< 特殊鱼
    BOSS: 3,            //< BOSS鱼
    SKILL: 4,
    POKER: 5,           //< 扑克鱼    
    LOTTERY: 6,         //< 兑换券鱼
    MATCH: 7,            //< 争霸赛鱼
    DICESLOT: 8          //<財神骰子魚
};

/**
 * 鱼群类型
 */
cf.FishGroupType = {
    GROUP: 0,       //< 鱼群
    TEAM: 1         //< 队列
};

/**
 * 网络消息类型
 */
cf.NetMsgType = {
    HTTP: 0,         //< http消息
    SOCKET: 1        //< socket消息
};

/**
 * 场景标记
 */
cf.SceneFlag = {
    LOGO: 0,                //< logo场景
    UPDATE: 1,              //< 更新场景
    LOGIN: 2,               //< 登录场景
    LOBBY: 3,               //< 大厅场景
    GAME_LOADING: 4,        //< 游戏加载场景
    GAME: 5                 //< 游戏场景
};

/**
 *  弹窗类型
 */
cf.DlgType = {
    NICKNAME_DLG: 0,        //< 修改昵称
    ROLE_INFO_DLG: 1,       //< 修改玩家信息
    HEAD_DLG: 2,            //< 修改头像
    SELL_DLG: 3,            //< 出售道具
    SEND_DLG: 4,            //< 赠送道具
    SELL_GAME_DLG: 5,       //< 渔场内出售道具
    CODE_DLG: 6,            //< 兑换码    
    SUPPORT_DLG: 7,         //< 客服
    SIGNATURE: 8,           //< 签名

    MAX: 999

};
/**
 * 货物类型
 */
cf.PropsType = {
    PROPSTYPE_DIAMOND: 100001,
    PROPSTYPE_GOLD: 100002,
    PROPSTYPE_SECURITIES: 100003
};

/**
 * 聊天文本截取的文字个数
 */
cf.ChatTextSubNumber = 19;

/**
 * 聊天频道类型
 */
cf.ChatChannel = {
    INSERVERS: 2,
    INROOM: 1
};

/**
 * 邮件显示类型
 */
cf.EmailType = {
    SYSTEMEMAIL: 0,
    FRIENDEMAIL: 1,
    GIFTHISTORY: 2,
    NORMAL: 3,
    RECIEVEGIFT: 4
};

/**
 * 解析字符串的关键字
 */
cf.KeyWordOFParseString = {};

cf.SkillId = {
    lockSkill: 1,
    iceSkill: 2,
    summonSkill: 3,
    rageSkill: 4,
    gunSkill: 5,
    auto: 6,
    autolock: 7
};

cf.fireInterval = {
    initialFireInterval: 0.2,
    rageFireInterval: 0.15
};


/**
 * 道具id
 * @remark 具体参数含义查表，ItemData
 */
cf.ItemID = {
    GOLD: 1,
    DIAMOND: 2,
    POINT: 3,
    EXP: 4,
    VIP_EXP: 5,
    DAY_VAL: 6,
    WEEK_VAL: 7,
    LOTTERY: 8,
    ACCPOINTS: 12,
    LOTTERYNUM: 13,

    CHIPS: 15,

    LIMTITED_CANNON3: 203,

    SKILL_1030101: 1030101,
    SKILL_1030102: 1030102,
    SKILL_1030103: 1030103,
    SKILL_1030104: 1030104,
    SKILL_1030105: 1030105,
    SKILL_1030106: 1030106,

    SKILL_1030111: 1030111,
    SKILL_1030112: 1030112,
    SKILL_1030113: 1030113,
    SKILL_1030114: 1030114,

    RedPart: 1030201,
    YellowPart: 1030202,
    BluePart: 1030203,
    GreenPart: 1030204,
    UpdatePart: 1030205,

    MAP01: 1030301,
    MAP02: 1030302,
    MAP03: 1030303,
    MAP04: 1030304,

    CHAT_CARD: 1001,

    MAX: -1
};
/**
 * 活動訊息
 */
cf.activityInfo = null;
/**
 * 服务器时间
 */
cf.currentServerTime = 0;
/**
 * 道具类型
 * @type 具体参数含义查表，ItemData
 */
cf.ItemType = {
    ItemType01: 1,//礼包
    ItemType02: 2,//装备
    ItemType03: 3,//消耗
    ItemType04: 4,//其他
    ItemType05: 5,//货币
    ItemType06: 6//炮皮肤关联道具(不在背包内显示）

};
/**
 * 激活彩金鱼数量
 * @type {{firstBonus: number, otherBonus: number}}
 */
cf.BonusType = {
    firstBonus: 3,
    otherBonus: 5
};

/**
 * 按钮颜色
 * @type {{GREEN: ({r, g, b, a}|cc.Color), BLUE: ({r, g, b, a}|cc.Color), YELLOW: ({r, g, b, a}|cc.Color)}}
 */
cf.BtnColor = {
    GREEN: cc.color(21, 163, 44),
    BLUE: cc.color(23, 108, 175),
    YELLOW: cc.color(181, 95, 12)
};

cf.ACTTagFlag = {
    Roulette: 0,                    //< 轮盘抽奖（常态活动）
    TopUP: 1,                       //< 累计充值 (活动目前只有累充)
    Champions: 3,                   //< 争霸赛
    Discount: 4,                    //< 限时折扣
    Rank: 5,                        //< 排行活动
};

//最大子弹发射数
cf.BULLET_COUNT = 30;

/**
 *  存档标记
 */
cf.EmailNoReadNum = "EmailNoReadNum";  //< 邮件未读个数

/**
 * 任务类型
 * @type {{DAY: number, WEEK: number, GROW: number}}
 */
cf.TaskType = {
    DAY: 0,         //< 每日任务
    WEEK: 1,        //< 周常任务
    GROW: 2         //< 成长任务
};

/**
 * 将时间转换为正确显示的时间片段
 */
cf.transFormTime = function (time) {
    var minute1 = time / 60;
    var minute2 = Math.round(time / 60);
    var minute;
    if (minute1 >= minute2) {
        minute = minute2;
    }
    else {
        minute = minute2 - 1;
    }
    minute < 10 ? minute = "0" + minute : minute;
    var second = Math.round(time % 60);
    second < 10 ? second = "0" + second : second;
    return minute + ":" + second;
};

cf.isPlayGameVoice = false;
cf.nGameVoiceCount = 0;
cf.autoOpenRemouldLayer = false;
cf.openHuntLayerFortData = 0;
cf.localGuideData = undefined;

/**
 * 子弹速度
 * @type {{COMMON: number, RAGE: number}}
 */
cf.BulletSpeed = {
    COMMON: 550,        //< 普通
    RAGE: 650           //< 狂暴
};

cf.bFirstInit = false;
cf.bActInit = false;

/**
 * 排行榜标记
 * @type {{map: number, dayIntegral: number, weekIntegral: number}}
 */
cf.rankTag = {
    map: 1,                                 //< 藏宝图排行
    dayIntegral: 2,                         //< 今日积分排行
    weekIntegral: 3,                        //< 本周积分排行
};

/**
 * 排行榜本地记录标记
 * @type {string}
 */
cf.mapRank = "mapRank";
cf.dayIntegralRank = "dayIntegralRank";
cf.weekIntegralRank = "weekIntegralRank";
cf.boomMissile = "missileRank";
cf.boomMissileLast = "missileRankLast";
//当前期排行更新时间
cf.momentRankUpdateTime = "momentRankUpdateTime";
//上一期排行更新时间
cf.rankUpdateTime = "rankUpdateTime";
//本地流水报表
cf.localCostInfo = "costInfo";
//本地捕鱼报表
cf.localFishingInfo = "fishingInfo";
//本地捕鱼详情
cf.localDetaileInfo = "detaileInfo";
/**
 * 奖卷兑换奖品分类
 * @type {{props: number, MobilePhone: number, GiftCards: number, FashionDigital: number, ElectricAppliance: number, LifeSupplies: number, GameGolds: number}}
 */
cf.exchangeTag = {
    props: 1,                //< 游戏道具
    MobilePhone: 2,          //< 手机话费
    GiftCards: 3,            //< 购物卡
    FashionDigital: 4,       //< 时尚数码
    ElectricAppliance: 5,    //< 家用电器
    LifeSupplies: 6,         //< 生活用品
    GameGolds: 7            //< 游戏金币
};

cf.AnnouncementEventType = {
    fishEvent: 2,
    lotteryEvent: 3,
    pokerEvent: 4,
    scoreEvent: 5,
    rouletteEvent: 6, //转盘抽奖
    prizesLuckeyEvent: 7, //彩金抽奖
    dragonJP: 8,
    dragonSkill: 9
};

cf.TitleColor = {
    FONT_1: cc.color(252, 255, 228),
    OUT_1: cc.color(0, 143, 141),
    FONT_2: cc.color(147, 255, 255),
    OUT_2: cc.color(2, 100, 182),

    SIZE: 4
};

cf.VipMaxLv = 0;

//统计类型
cf.DCType = {
    DC_BUY: 0,               //>购买:月卡、首充礼包
    DC_EXCHANGE: 1,          //>兑换:积分兑换、兑换券兑换
    //系统赠送：任务、升级奖励、每日登录、7日轮盘、彩金抽奖、邮件赠送、活动赠送、兑换码
    DC_SYSTEM_TASK: 2,       //< 任务
    DC_SYSTEM_UPGRADE: 3,    //< 升级
    DC_SYSTEM_LOGIN: 4,      //< 每日登录
    DC_SYSTEM_ROULETTE: 5,   //< 7日轮盘
    DC_SYSTEM_BONUS: 6,      //< 彩金抽奖
    DC_SYSTEM_EMAIL: 7,      //< 邮件赠送
    DC_SYSTEM_ACT: 8,        //< 活动赠送
    DC_SYSTEM_VIP: 9,        //< VIP赠送
    DC_SYSTEM_CODE: 10,      //< 兑换码

    //消耗：出售，赠送
    DC_SELL: 11,             //< 出售
    DC_GIVE: 12,             //< 赠送


    DC_MAX: 999
};

//事件id
cf.EventMessage = {
    EventId_Login: 0,
    EventId_Logout: 1,
    EventId_Pay: 2,

    EventId_Copy: 3,
    EventId_OpenURL: 4,

    EventId_Exit: 5,
    EventId_AnalyticsInit: 6,

    EventId_UploadInfo: 7,
    EventId_BBS: 8,
    EventId_ShowAd: 9,
    EventId_Support: 10,
    EventId_Init: 11,

    EventId_LoginQQ: 12,
    EventId_LoginWX: 13,

    EventId_Max: 999
};

/**
 * 随机范围
 * @param Min
 * @param Max
 * @returns {*}
 */
cf.randomRange = function (Min, Max) {
    var Range = Max - Min;
    var Rand = Math.random();
    return (Min + Math.round(Rand * Range));
};

cf.blockMap = null;

//断线重连标记
cf.reConnect = {
    bReturn: false,
    roomID: 0
};


//智能捕魚
cf.autolock = false;
cf.autolockBulletNum = 0;
cf.autofishData = [];

//轮盘抽奖最大次数
cf.MaxRouletteNum = 10;

//轮盘抽奖消耗积分数
cf.MaxRouletteConsumePoints = 300;

//确保在引导过程中并不会出现引导覆盖其他的界面情况
cf.layerHasShowInGame = false;

//是否忽略积分获取提示
cf.isIgnoreScoreDialog = false;
//争霸赛规则UI是否显示
cf.isChampionsRuleUI = false;
//全面屏刘海高度
cf.statusBarHeight = 0;
//补单提示开关
cf.AskOrderTip = false;
//补单请求开关
cf.AskOrder = true;

//是否已经执行过兑换券飞行动画
cf.hasShowFlyLottery = false;
//等级奖励最大等级
cf.lvRewardMax = 0;

//玩家是否已经充值过
cf.playerHasPay = false;

//时候显示过1元礼包
cf.hasShowGiftBagOfOne = false;

//解锁全部炮倍UI展示开关
cf.unlockShow = true;

cf.fortMaxIndex = 0;

cf.Account = "";
cf.Password = "";
cf.CheckState = 0;

cf.NoSleep = null;

//心跳开始时间
cf.KeepAliveStartTime = 0;
//是否收到心跳
cf.bKeepAliveRes = false;
//网络延迟时间
cf.NetDelayTime = 0;

cf.RemoveUserTime = 0;
cf.bRemoveUser = false;