var ParserOfString = cc.Class.extend({
    _delegate: null,
    _textArray: null,
    ctor: function () {

    },
    /**
     * @param string        {string}         需要解析的字符串
     * @param parentNode    {ccui.richText}  显示的富文本（在解析后自动生成字文本并添加到传递的富文本对象上）
     */
    parsing: function (string,parentNode) {
        var self = this;
        self._delegate = parentNode;
        self._textArray = [];
        var str = string;
        var length = str.length;
        var strArray = [];
        var strInfo = [];
        var infoNum = 0;
        var contentStr = "";
        var start = false;
        var infostart = false;
        var readInfo = false;
        var infoKey;
        for (var i = 0; i < length; i++) {
            i = parseInt(i);
            if (str[i] == "<" && ((str[i - 1] != "\"" || str[i + 1] != "\""))) {
                start = true;
                continue
            }
            else if (str[i] == ">" && ((str[i - 1] != "\"" || str[i + 1] != "\""))) {
                start = false;
            }
            else if (str[i] == "{" && ((str[i - 1] != "\"" || str[i + 1] != "\""))) {
                infoNum++;
                readInfo = true;
                start = true;
                continue;
            }
            else if (str[i] == "}" && ((str[i - 1] != "\"" || str[i + 1] != "\""))) {
                readInfo = false;
                start = false;
                infostart = false;
            }
            if (!start && !infostart && str[i] != ">" && str[i] != "}") {
                if (str[i + 1] != "<") {
                    contentStr = contentStr + str[i];
                    if( i == length - 1 && contentStr != null){
                        infoNum++;
                        strInfo[infoNum - 1] = [];
                        strArray.push(contentStr);
                        contentStr = ""
                    }
                }
                else {
                    contentStr = contentStr + str[i];
                    infoNum++;
                    strInfo[infoNum - 1] = [];
                    strArray.push(contentStr);
                    contentStr = ""
                }
            }
            if (start) {
                if (readInfo) {
                    if (str[i] == ":") {
                        infostart = true;
                        infoKey = str[i - 1];
                        continue;
                    }
                    if (str[i] == ";") {
                        infostart = false;
                    }
                    if (infostart) {
                        contentStr = contentStr + str[i];
                    }
                    else {
                        if (!strInfo[infoNum - 1]) {
                            strInfo[infoNum - 1] = [];
                        }
                        switch (infoKey) {
                            case "f" :
                                if (contentStr != "") {
                                    strInfo[infoNum - 1].font = contentStr;
                                }
                                contentStr = "";
                                break;
                            case "c" :
                                if (contentStr != "") {
                                    strInfo[infoNum - 1].color = contentStr;
                                }
                                contentStr = "";
                                break;
                            case "s" :
                                if (contentStr != "") {
                                    strInfo[infoNum - 1].stroke = contentStr;
                                }
                                contentStr = "";
                                break;
                        }
                    }
                }
                else {
                    if (str[i] != "\"") {
                        contentStr = contentStr + str[i];
                    }
                }
            }
            else {
                if (!readInfo && str[i] == ">") {
                    if (contentStr != "") {
                        strArray.push(contentStr);
                        contentStr = ""
                    }
                }
            }
        }
        length = strArray.length;
        for (var i = 0; i < length; i++) {
            self._textArray[i] = [];
            self._textArray[i].content = strArray[i];
            if (strInfo[i].font) {
                self._textArray[i].font = strInfo[i].font;
                var colorArray = strInfo[i].color.split(",");
                self._textArray[i].color = cc.color(parseInt(colorArray[0]), parseInt(colorArray[1]), parseInt(colorArray[2]));
                self._textArray[i].stroke = strInfo[i].stroke;
            }
            else {
                self._textArray[i].font = 20;
                self._textArray[i].color = cc.color.WHITE;
                self._textArray[i].stroke = 0;
            }
        }
        self.initChildOfRichText();
    },
    initChildOfRichText: function () {
        var self = this;
        for (var i in self._textArray) {
            var richChild = new ccui.RichElementText(i, self._textArray[i].color, 255, self._textArray[i].content, cf.Language.FontName, self._textArray[i].font);
            self._delegate.pushBackElement(richChild);
        }
    },
    /**
     * 释放资源
     */
    onClear: function () {
        var self = this;
        self._delegate = null;
    }
});

/**
 * 字符串解析器实例
 * @type {undefined}
 * @private
 */
ParserOfString._sInstance = undefined;
ParserOfString.getInstance = function () {
    if (!ParserOfString._sInstance) {
        ParserOfString._sInstance = new ParserOfString();
    }
    return ParserOfString._sInstance;
};

/**
 * 移除实例
 */
ParserOfString.purgeRelease = function () {
    if (ParserOfString._sInstance) {
        ParserOfString._sInstance.onClear();
        ParserOfString._sInstance = null;
    }
};