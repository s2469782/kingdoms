var AnalyticsHelper = AnalyticsHelper || {};

/**
 * 初始化
 */
AnalyticsHelper.DCInit = function (version) {
    if(cc.sys.isNative){
        cf.JsbHelper.DCInit(version);
    }
};

/**
 * 登录
 * @param idLogin
 */
AnalyticsHelper.DCLogin = function (idLogin) {
    if(cc.sys.isNative){
        cf.JsbHelper.DCLogin(idLogin+"");
    }
};

/**
 * 登出
 */
AnalyticsHelper.DCLogout = function () {
    if(cc.sys.isNative){
        cf.JsbHelper.DCLogout();
    }
};

/**
 * 设置等级
 * @param level
 */
AnalyticsHelper.DCSetLevel = function (level) {
    if(cc.sys.isNative){
        cf.JsbHelper.DCSetLevel(parseInt(level));
    }
};

/**
 * 道具购买
 * @param itemId 道具id
 * @param itemType 道具类型
 * @param itemCnt 道具数量
 * @param amount 道具虚拟币价值
 * @param currency 道具虚拟币类型，例如：金币
 */
AnalyticsHelper.DCItemBuy = function (itemId, itemType, itemCnt, amount, currency) {
    /*if(cc.sys.isNative){
        cf.JsbHelper.DCItemBuy(itemId+"", itemType+"", itemCnt, amount, currency);
    }*/
};

/**
 * 获得道具（系统赠送）
 * @param itemId 道具id
 * @param itemType 道具类型
 * @param itemCnt 道具数量
 * @param reason 获得原因
 */
AnalyticsHelper.DCItemGet = function (itemId, itemType, itemCnt, reason) {
    /*if(cc.sys.isNative){
        cf.JsbHelper.DCItemGet(itemId+"", itemType+"", itemCnt, reason);
    }*/
};

/**
 * 道具消耗: 使用消耗、赠送消耗、出售消耗
 * @param itemId 道具id
 * @param itemType 道具类型
 * @param itemCnt 道具数量
 * @param reason 消耗原因
 */
AnalyticsHelper.DCItemConsume = function (itemId, itemType, itemCnt, reason) {
    /*if(cc.sys.isNative){
        cf.JsbHelper.DCItemConsume(itemId+"", itemType+"", itemCnt, reason);
    }*/
};

/**
 * 虚拟币留存，记录玩家虚拟币留存总量
 * @param coinNum 虚拟币数量
 * @param coinType 虚拟币类型，例如：金币
 */
AnalyticsHelper.DCSetCoinNum = function (coinNum,coinType) {
    /*if(cc.sys.isNative){
        cf.JsbHelper.DCSetCoinNum(coinNum+"",coinType);
    }*/
};

/**
 * 记录玩家虚拟币获取并同时记录虚拟币留存总量
 * @param reason 获取虚拟币原因
 * @param coinType 虚拟币类型
 * @param gain 虚拟币获取数量
 * @param left 虚拟币总量
 */
AnalyticsHelper.DCGain = function (reason, coinType, gain, left) {
    /*if(cc.sys.isNative){
        cf.JsbHelper.DCGain(reason,coinType,gain+"",left+"");
    }*/
};

/**
 * 记录玩家虚拟币消耗并同时记录虚拟币留存总量
 * @param reason 消耗虚拟币原因
 * @param coinType 虚拟币类型
 * @param lost 虚拟币消耗数量
 * @param left 虚拟币总量
 */
AnalyticsHelper.DCLost = function (reason, coinType, lost, left) {
    /*if(cc.sys.isNative){
        cf.JsbHelper.DCLost(reason,coinType,lost+"",left+"");
    }*/
};

/**
 * 事件
 * @param eventId
 * @param eventName
 */
AnalyticsHelper.DCOnEvent = function (eventId, eventName) {
    if(cc.sys.isNative){

    }
};

/**
 * 事件
 * @param eventId
 */
AnalyticsHelper.DCOnEvent = function (eventId) {
    if(cc.sys.isNative){

    }
};

/**
 * 上报错误
 * @param title
 * @param error
 */
AnalyticsHelper.DCReportError = function (title, error) {
    /*if(cc.sys.isNative){
        cf.JsbHelper.DCReportError(title+"",error+"");
    }*/
};

/**
 * 上传角色数据
 * @param type
 * @constructor
 */
AnalyticsHelper.UploadInfo = function (type) {
    if(cc.sys.isNative){
        var playerModel = cf.PlayerMgr.getLocalPlayer();
        if(playerModel){
            var jsonInfo = {};
            jsonInfo.type = type;
            jsonInfo.gameId = playerModel.getIdLogin();
            jsonInfo.nickName = playerModel.getNickName();
            jsonInfo.lv = playerModel.getPlayerLevel();
            jsonInfo.vip = playerModel.getVipLevel();
            jsonInfo.gold = playerModel.getGoldNum();

            var json = JSON.stringify(jsonInfo);
            cf.JsbHelper.doEvent(cf.EventMessage.EventId_UploadInfo,json);    
        }
    }
};