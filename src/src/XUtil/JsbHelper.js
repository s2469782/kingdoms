cf.JsbHelper = {

    PACKAGE_NAME: "com/jsb/helper/JsbHelper",

    getChannelInfo: function () {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "getChannelInfo");
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME,//类的路径。最后一个hello是java文件名，但是不需要加入文件名后缀
            "getChannelInfo",//方法名
            "()Ljava/lang/String;");//括号里的是参数，后面的是返回值
    },

    /**
     * 统一事件回调函数
     * @param id
     * @param str
     * @returns {*}
     */
    doEvent: function (id,str) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "doEvent::",id, str);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "doEvent", "(ILjava/lang/String;)V", id,str);
    },
    //////////////////////////////////////////////////////
    //统计相关
    //////////////////////////////////////////////////////
    DCInit: function (version) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCInit:", version);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCInit", "(Ljava/lang/String;)V", version);
    },

    DCLogin: function (account) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCLogin:", account);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCLogin", "(Ljava/lang/String;)V", account);
    },

    DCLogout: function () {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCLogout");
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCLogout", "()V");
    },

    DCSetLevel: function (level) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCSetLevel:", level);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCSetLevel", "(I)V", level);
    },

    DCItemBuy: function (itemId, itemType, itemCnt, amount, currency) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCItemBuy:::::", itemId, itemType, itemCnt, amount, currency);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCItemBuy", "(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V", itemId, itemType, itemCnt, amount, currency);
    },

    DCItemGet: function (itemId, itemType, itemCnt, reason) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCItemGet::::", itemId, itemType, itemCnt, reason);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCItemGet", "(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V", itemId, itemType, itemCnt, reason);
    },

    DCItemConsume: function (itemId, itemType, itemCnt, reason) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCItemConsume::::", itemId, itemType, itemCnt, reason);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCItemConsume", "(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V", itemId, itemType, itemCnt, reason);
    },

    DCSetCoinNum: function (coinNum, coinType) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCSetCoinNum::", coinNum, coinType);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCSetCoinNum", "(Ljava/lang/String;Ljava/lang/String;)V", coinNum, coinType);
    },

    DCGain: function (reason, coinType, gain, left) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCGain::::", reason, coinType, gain, left);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCGain", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", reason, coinType, gain, left);
    },

    DCLost: function (reason, coinType, lost, left) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCLost::::", reason, coinType, lost, left);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCLost", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", reason, coinType, lost, left);
    },

    DCReportError: function (title, error) {
        if (cc.sys.os === cc.sys.OS_IOS) {
            return jsb.reflection.callStaticMethod("JsbHelper", "DCReportError::", title, error);
        }
        return jsb.reflection.callStaticMethod(this.PACKAGE_NAME, "DCReportError", "(Ljava/lang/String;Ljava/lang/String;)V", title, error);
    }

};
