cf.Language = {};

/**
 * 语言配置
 * @type {{ZH: number, ZH_TW: number, EN: number}}
 */
cf.Language.TYPE = {
    ZH: 0,          //< 简体中文
    ZH_TW: 1,       //< 繁体中文
    EN: 2,          //< 英文
    JP: 3           //< 日文
};

cf.Language._type = cf.Language.TYPE.ZH;
cf.Language.FontName = "SimSun";
cf.Language.Text = [];

/**
 * 初始化多语言配置
 * @param jsonFile
 */
cf.Language.init = function (jsonFile) {
    cf.Language._type = GameConfig.language;
    if (cc.sys.isNative) {
        cf.Language.FontName = Res.LOGIN.gb_font.srcs[1];
    }else{
        cf.Language.FontName = Res.LOGIN.gb_font.name;
    }
    cf.Language.readData(jsonFile);
};

/**
 * 获取语言text
 * @param key
 * @returns {string|string}
 */
cf.Language.getText = function (key) {
    return cf.Language.Text[key].text;
};

/**
 * 读取多语言配置文件
 * @param jsonFile
 */
cf.Language.readData = function (jsonFile) {
    var jsonData = cc.loader.getRes(jsonFile);
    if (jsonData) {

        var languageData = cf.Language.Text;
        if (languageData && languageData.length > 0) {
            cc.log("已读取LanguageData");
            return;
        }

        cc.log("读取LanguageData");
        for (var i in jsonData) {
            if (jsonData.hasOwnProperty(i)) {
                var tmpInfo = {};

                tmpInfo.key = jsonData[i].key;
                tmpInfo.text = jsonData[i].text;

                languageData[tmpInfo.key] = tmpInfo;
            }
        }

    } else {
        cc.log("读取LanguageData失败");
    }
};




