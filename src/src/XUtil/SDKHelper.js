var SDKHelper = SDKHelper || {};

SDKHelper._target = null;
SDKHelper._loginCallBack = null;
SDKHelper._payCallBack = null;
SDKHelper._uid = null;
SDKHelper._session = null;
SDKHelper._orderId = null;               //< 断线重连记录订单号
SDKHelper._resultState = -1;              //< 记录支付订单返回状态
SDKHelper._isInBack = false;            //< 切入后台标记
SDKHelper._pollingNum = 0;               //< 轮询次数
SDKHelper._GoodsId = -1;              //< 获取本次支付商品ID
SDKHelper._nActID = -1;               //< 记录折扣活动ID
SDKHelper._bReConnect = false;      //< 断线重连标记
SDKHelper._pollingTime = 2500;          //< 轮询间隔毫秒
SDKHelper._bRegister = false;
SDKHelper._keepAliveIntervalFunc = null;

SDKHelper.init = function () {
    cf.addNetListener(MessageCode.CH_AskGateMsg_Res, SDKHelper.loginCallBack.bind(this));
    cf.addNetListener(MessageCode.CS_AskPaymentMsg_Res, SDKHelper.payCallBack.bind(this));
    cf.addNetListener(MessageCode.CH_InHallReceiveOrderItemMsg_Res, SDKHelper.payCallBack.bind(this));
    cf.addNetListener(MessageCode.CS_InRoomReceiveOrderItemMsg_Res, SDKHelper.payCallBack.bind(this));
    cf.addNetListener(MessageCode.CS_AskAllNotReceiveOrderMsg_Res, SDKHelper.payCallBack.bind(this));
    cf.addNetListener(MessageCode.KeepAlive_Res, SDKHelper.keepAliveCallBack.bind(this));

    if (!cc.sys.isNative) {
        //H5初始化sdk
        ThirdSdk.init();
    }
    this._pollingTime = 2500;
};

/**
 * 请求门服务器
 * @param bReConnect
 */
SDKHelper.askGate = function (bReConnect) {
    this._bReConnect = bReConnect;

    if (!bReConnect) {
        cf.UITools.showLoadingToast(cf.Language.getText("text_1000"), 25, true, this._target, function () {
            //超时
            cf.dispatchEvent(NotificationScene.eventName, NotificationScene.Error.ERROR_SOCKET_TIMEOUT);
        });
    }
    //请求门服务器
    /*var msg = new $root.CH_AskGateMsg();
    msg.nidLogin = 100;
    msg.strSafeCode = "100";
    msg.strVer = GameConfig.verName;
    if (cc.sys.isNative) {
        //暂时先使用更新版本号
        msg.strVer = GameConfig.verNameForce;
    }
    var wr = $root.CH_AskGateMsg.encode(msg).finish();
    cf.NetMgr.sendHttpMsg(MessageCode.CH_AskGateMsg, wr);*/

    //使用明文发送
    var msg = "nMsgID="+MessageCode.CH_AskGateMsg+"&strVer="+GameConfig.verName;
    cf.NetMgr.sendHttpMsg(MessageCode.CH_AskGateMsg,msg,HttpNet.Type.text);
};

/**
 * 登录
 */
SDKHelper.login = function (target, type) {
    var self = this;
    self._target = target;
    self._bRegister = false;
    if (cc.sys.isNative) {
        if (cc.sys.os === cc.sys.OS_WINDOWS) {
            self.askGate(false);
        } else {

            switch (type) {
                case 1:
                    cf.JsbHelper.doEvent(cf.EventMessage.EventId_LoginQQ, "");
                    break;
                case 2:
                    cf.JsbHelper.doEvent(cf.EventMessage.EventId_LoginWX, "");
                    break;
                default:
                    cf.JsbHelper.doEvent(cf.EventMessage.EventId_Login, "");
                    break;
            }
        }
    } else {
        self.askGate(false);
    }
};

SDKHelper.register = function (target) {
    var self = this;
    self._target = target;
    self._bRegister = true;
    self.askGate(false);
};

/**
 * 登出
 */
SDKHelper.logout = function () {
};

/**
 * 账号登录
 * @param loginType
 * @private
 */
SDKHelper._loginAccount = function () {
    //获取登录信息
    var strTreeUID = 0, strSession = "";
    if (cc.sys.isNative) {
        strTreeUID = this._uid;
        strSession = this._session;
    } else {
        //H5登录
        var loginInfo = ThirdSdk.login();
        strTreeUID = loginInfo.user_id;
        strSession = loginInfo.json;
    }
    cc.log("strTreeUID = " + strTreeUID);
    cc.log("strSession = " + strSession);

    //发送登录消息
    var msg = new $root.CH_LoginMsg();
    msg.nLoginType = parseInt(GameConfig.loginType);
    msg.strTreeUID = 0;
    msg.strSession = strSession;
    msg.strIdentity = "";
    msg.strMac = "";
    msg.nBigChannelID = parseInt(GameConfig.channelId);
    msg.strChannelName = GameConfig.channelName;
    msg.nidLogin = parseInt(strTreeUID);
    msg.strPassword = "";
    msg.strAccount = "";
    //断线重连
    /*if (this._bReConnect) {
     msg.nLoginType = 4;
     msg.strTreeUID = cf.PlayerMgr.getUid();
     msg.strSafeCode = cf.PlayerMgr.getSafeCode();
     }*/

    var wr = $root.CH_LoginMsg.encode(msg).finish();
    cf.NetMgr.sendSocketMsg(MessageCode.CH_LoginMsg, wr);
};

/**
 * 游客登录
 * @private
 */
SDKHelper._loginGuest = function () {
    //游客登录
    var identity = "";              //唯一识别码
    var mac = "";                   //mac地址
    var idLogin = 0;                //用户id
    var password = "";              //用户密码

    //判断是否保存过密码
    idLogin = parseInt(cf.SaveDataMgr.getNativeData("idLogin"));
    password = cf.SaveDataMgr.getNativeData("password");
    if (idLogin != null && password != null) {
        //如果有就走idLogin登录
    } else {
        //没有则游客登录
        idLogin = 0;
        password = "";
    }

    if (cc.sys.isNative) {
        //根本不同系统做不同的逻辑处理，获取identity和mac
        if (cc.sys.os === cc.sys.OS_ANDROID || cc.sys.os === cc.sys.OS_IOS) {
            //获取唯一标识
            identity = cf.generateUUID();//GameConfig.identity;
            //获取mac地址
            mac = GameConfig.mac;

            cc.log("identity:" + identity + ",mac:" + mac);
        } else {
            //临时用
            identity = cf.generateUUID();
            cc.log("identity:" + identity);
        }
    } else {
        //临时用
        identity = cf.generateUUID();
        cc.log("identity:" + identity);
    }

    //发送登录消息
    var msg = new $root.CH_LoginMsg();
    msg.nLoginType = cf.LoginType.GUEST;
    msg.strTreeUID = 0;
    msg.strSession = "";
    msg.strIdentity = identity;
    msg.strMac = mac;
    msg.nBigChannelID = parseInt(GameConfig.channelId);
    msg.strChannelName = GameConfig.channelName;
    msg.nLoginChannelID = parseInt(GameConfig.loginId);
    msg.nidLogin = idLogin;
    msg.strPassword = password;
   
    var wr = $root.CH_LoginMsg.encode(msg).finish();
    cf.NetMgr.sendSocketMsg(MessageCode.CH_LoginMsg, wr);
};

SDKHelper.loginGameAccount = function (account, password, checkState) {
    //发送登录消息
    var msg = new $root.CH_LoginMsg();
    msg.nLoginType = cf.LoginType.GAME_ACCOUNT;
    msg.strTreeUID = 0;
    msg.strSession = "";
    msg.strIdentity = "";
    msg.strMac = "";
    msg.nBigChannelID = parseInt(GameConfig.channelId);
    msg.strChannelName = GameConfig.channelName;
    msg.nidLogin = 0;
    msg.strPassword = password;
    msg.strAccount = account;
    
    //保存账密和状态
    cf.SaveDataMgr.setNativeData("game_account", account);
    var _strPassword = password.split("").reverse().join("");
    cf.SaveDataMgr.setNativeData("game_password", _strPassword);
    cf.SaveDataMgr.setNativeData("game_checkState", checkState);

    var wr = $root.CH_LoginMsg.encode(msg).finish();
    cf.NetMgr.sendSocketMsg(MessageCode.CH_LoginMsg, wr);
};

SDKHelper.reConnectLogin = function () {
    cc.log("服务器拒绝登录，正在重试...");
    var self = this;
    //登录相关逻辑
    var loginType = GameConfig.loginType;
    switch (parseInt(loginType)) {
        case cf.LoginType.GUEST:
            self._loginGuest();
            break;
        case cf.LoginType.ACCOUNT:
        case cf.LoginType.OTHER:
            self._loginAccount();
            break;
        case cf.LoginType.GAME_ACCOUNT:
            self.loginGameAccount(cf.Account, cf.Password, cf.CheckState);
            break;
        case cf.LoginType.H5_ACCOUNT:
            self._loginAccount();
            break;
    }
};

/**
 * 登录回调
 * @param msg
 * @param msgId
 */
SDKHelper.loginCallBack = function (msg, msgId) {
    var self = this;
    var resMsg = null;
    switch (msgId) {
        case MessageCode.CH_AskGateMsg_Res://获取服务器门信息
            resMsg = $root.CH_AskGateMsg_Res.decode(msg);

            if (resMsg) {
                cc.log("gateId = " + resMsg.nGateID);
                cc.log("ip = " + resMsg.strIP);
                cc.log("port = " + resMsg.nPort);

                //连接socket
                cf.NetMgr.connectSocket(resMsg.strIP, resMsg.nPort, function () {
                    //登录相关逻辑
                    var loginType = GameConfig.loginType;
                    switch (parseInt(loginType)) {
                        case cf.LoginType.GUEST:
                            self._loginGuest();
                            break;
                        case cf.LoginType.ACCOUNT:
                        case cf.LoginType.OTHER:
                            self._loginAccount();
                            break;
                        case cf.LoginType.GAME_ACCOUNT:
                            if (self._bRegister) {
                                /*cf.UITools.hideLoadingToast();
                                var layer = new UIRegisteredLayer(self._target);
                                self._target.addChild(layer, 100);*/
                            } else {
                                self.loginGameAccount(cf.Account, cf.Password, cf.CheckState);
                            }
                            break;
                        case cf.LoginType.H5_ACCOUNT:
                            self._loginAccount();
                            break;
                    }
                });
            } else {
                cf.UITools.hideLoadingToast();
                cc.error("error:CH_LoginMsg");
            }
            break;
    }
};

/**
 * 支付接口
 * @param goodsId
 * @param payCallBack
 * @param target
 * @param nActID 活动支付专用
 */
SDKHelper.pay = function (goodsId, payCallBack, target, nActID) {
    cf.UITools.showLoadingToast(cf.Language.getText("text_1205"), 25, true, target);
    var locPlayer = cf.PlayerMgr.getLocalPlayer();
    var msg = new $root.CS_AskPaymentMsg();
    msg.nIdLogin = locPlayer.getIdLogin();
    msg.strSafeCode = locPlayer.getSafeCode();
    msg.nGoodsId = goodsId;

    //扩展参数
    if (cc.sys.isNative) {

    } else {
        var json = ThirdSdk.getPayInfo();
        if (json) {
            msg.strOtherPar = json;
        }
    }
    if (GameConfig.channelName == "AppStore") {
        //获取服务器相关参数，组装成json，传递给sdk并拉起
        var json = SDKHelper._createPayAppStoreJson(goodsId);
        cc.log(goodsId);
        cf.JsbHelper.doEvent(cf.EventMessage.EventId_Pay, json);
    }
    else {
        var wr = $root.CS_AskPaymentMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_AskPaymentMsg, wr);
    }

    this._payCallBack = payCallBack;
    this._target = target;
    if (nActID) {
        this._nActID = nActID;
    }
    else {
        this._nActID = -1;
    }
    console.log("发起支付请求...");
};

/**
 * 查询订单
 * @param orderId
 * @param bTip
 * @param callBack
 */
SDKHelper.orderQuery = function (orderId, bTip, callBack) {
    if (callBack)
        this._payCallBack = callBack;

    if (bTip) {
        cf.UITools.showLoadingToast(cf.Language.getText("text_1227"), 20, true, this._target);
    }

    var locPlayer = cf.PlayerMgr.getLocalPlayer();
    var msg = null, wr = null;
    if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY) {
        //获取商品ID
        msg = new $root.CH_InHallReceiveOrderItemMsg();
        msg.nIdLogin = locPlayer.getIdLogin();
        msg.strSafeCode = locPlayer.getSafeCode();
        msg.strOrderID = orderId;

        //判断是否限时活动支付
        if (this._GoodsId != -1) {
            var shopData = cf.Data.SHOP_DATA[parseInt(this._GoodsId)];
            if (shopData.goodType == 5) {
                msg.nActID = this._nActID;
                msg.nIndex = this._GoodsId - 500;
            }
        }
        wr = $root.CH_InHallReceiveOrderItemMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_InHallReceiveOrderItemMsg, wr);
    }
    else {
        msg = new $root.CS_InRoomReceiveOrderItemMsg();
        msg.nIdLogin = locPlayer.getIdLogin();
        msg.strSafeCode = locPlayer.getSafeCode();
        msg.strOrderID = orderId;
        wr = $root.CS_InRoomReceiveOrderItemMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_InRoomReceiveOrderItemMsg, wr);
    }
};

/**
 * 轮询订单
 * @returns {boolean}
 */
SDKHelper.pollingOrder = function () {
    var self = this;
    if (self._pollingNum >= 16) {
        self._pollingNum = 0;
        return false;
    }
    cf.UITools.showLoadingToast(cf.Language.getText("text_1227"), 80, true, this._target);
    setTimeout(function () {
        self.orderQuery(self._orderId, false);
        self._pollingNum++;

        //大于4次之后，开始递增轮询时间
        if (self._pollingNum > 4) {
            self._pollingTime = 3500;
        } else if (self._pollingNum > 8) {
            self._pollingTime = 5000;
        }
        console.log("开始轮询订单，次数 = " + self._pollingNum + ", 订单号 = " + self._orderId);
    }, self._pollingTime);
    return true;
};

/**
 * 获取补单列表
 * @param target
 */
SDKHelper.askOrderList = function (target) {
    this._target = target;
    cf.UITools.showLoadingToast(cf.Language.getText("text_1226"), 2, false, this._target);
    var locPlayer = cf.PlayerMgr.getLocalPlayer();
    var msg = new $root.CS_AskAllNotReceiveOrderMsg();
    msg.nIdLogin = locPlayer.getIdLogin();
    msg.strSafeCode = locPlayer.getSafeCode();
    var wr = $root.CS_AskAllNotReceiveOrderMsg.encode(msg).finish();
    cf.NetMgr.sendSocketMsg(MessageCode.CS_AskAllNotReceiveOrderMsg, wr);
    cc.log("索要订单列表 = askOrderList");
};

SDKHelper._createPayJson = function (msg) {
    var playerModel = cf.PlayerMgr.getLocalPlayer();
    var shopData = cf.Data.SHOP_DATA[msg.nGoodsId];

    var payJson = {};
    payJson.gameId = playerModel.getIdLogin();
    payJson.nickName = playerModel.getNickName();
    payJson.lv = playerModel.getPlayerLevel();
    payJson.vip = playerModel.getVipLevel();
    payJson.channelId = GameConfig.channelId;
    payJson.orderId = msg.strOrderID;
    payJson.itemName = shopData.goodName;
    payJson.gold = playerModel.getGoldNum();
    payJson.price = shopData.cost;
    payJson.sign = msg.strSignPar;
    payJson.goodsId = msg.nGoodsId;
    //这是个json
    payJson.extJson = JSON.parse(msg.strOtherPar);

    return JSON.stringify(payJson);
};

/**
 * 支付回调
 * @param msg
 * @param msgId
 * @remark callPaySdk回调函数res返回值说明：0 = 成功，-1 = 失败， 101 = 主动查询订单结果
 */
SDKHelper.payCallBack = function (msg, msgId) {
    var resMsg = null;
    switch (msgId) {
        case MessageCode.CS_AskPaymentMsg_Res://获取订单号
            cf.UITools.hideLoadingToast();
            resMsg = $root.CS_AskPaymentMsg_Res.decode(msg);

            cc.log("获取订单号成功，orderId = " + resMsg.strOrderID);
            SDKHelper._orderId = resMsg.strOrderID;
            SDKHelper._GoodsId = resMsg.nGoodsId;
            SDKHelper._resultState = 1;
            //调用SDK接口拉起支付,支付成功后向服务器发起查询
            if (cc.sys.isNative) {
                if (GameConfig.channelName == "1034") {
                    SDKHelper.orderQuery(resMsg.strOrderID, true);
                } else {
                    //获取服务器相关参数，组装成json，传递给sdk并拉起
                    var json = SDKHelper._createPayJson(resMsg);
                    cf.JsbHelper.doEvent(cf.EventMessage.EventId_Pay, json);
                }
            } else {
                //H5支付
                ThirdSdk.callPaySdk(resMsg, function (resultCode) {
                    if (resultCode == 0) {
                        SDKHelper.orderQuery(resMsg.strOrderID, true);
                    }
                });
            }
            console.log("拉起支付sdk...");
            break;
        case MessageCode.CH_InHallReceiveOrderItemMsg_Res://购买成功领取道具(大厅)
            cf.UITools.hideLoadingToast();
            resMsg = $root.CH_InHallReceiveOrderItemMsg_Res.decode(msg);
            this._paySuccess(resMsg);
            if (this._payCallBack)
                this._payCallBack.call(this._target, resMsg);

            //清除支付状态
            this.clearPayState();
            break;
        case MessageCode.CS_InRoomReceiveOrderItemMsg_Res://购买成功领取道具(房间)
            cf.UITools.hideLoadingToast();
            resMsg = $root.CS_InRoomReceiveOrderItemMsg_Res.decode(msg);
            this._paySuccess(resMsg);
            if (this._payCallBack)
                this._payCallBack.call(this._target, resMsg);

            //清除支付状态
            this.clearPayState();
            break;
        case MessageCode.CS_AskAllNotReceiveOrderMsg_Res:
            cf.UITools.hideLoadingToast();
            resMsg = $root.CS_AskAllNotReceiveOrderMsg_Res.decode(msg);
            var len = resMsg.orderInfor.length;
            switch (len) {
                case 0:
                    //清除支付状态
                    if (cf.AskOrderTip) {
                        cf.AskOrderTip = false;
                        cf.UITools.showHintToast(cf.Language.getText("text_1462"));
                    }
                    this.clearPayState();
                    return;
                default:
                    cf.AskOrderTip = false;
                    var orderList = new UIOrderList(resMsg.orderInfor);
                    this._target.addChild(orderList, 100);
                    break;
            }
            break;
    }
};

SDKHelper._paySuccess = function (resMsg) {
    var self = this;
    var localPlayerModel = cf.PlayerMgr.getLocalPlayer();
    //特殊处理一些道具，经验值，月卡炮台
    var length = resMsg.itemReward.length;
    var tempRewardList = [];
    var firstPayMark = localPlayerModel.getFirstPayMark();
    if (firstPayMark == "0") {
        if (self._GoodsId != 1 && self._GoodsId != -1) {
            localPlayerModel.setFirstPayMark("1");
        }
    }
    if (!cf.playerHasPay) {
        cf.playerHasPay = true;
    }

    for (var i = 0; i < length; i++) {
        var itemInfo = resMsg.itemReward[i];
        if (itemInfo.nError == 0) {
            //道具信息获取正常
            switch (itemInfo.nItemID) {
                case cf.ItemID.VIP_EXP:
                    var exp = itemInfo.nItemNum;
                    localPlayerModel.setVipExp(exp);

                    //计算vip等级
                    var vipLevel = localPlayerModel.getVipLevel();
                    if (vipLevel >= cf.VipMaxLv) {
                        break;
                    }
                    var levelCost = cf.Data.VIP_DATA[vipLevel].totalPay;
                    while (exp >= levelCost) {
                        localPlayerModel.setVipLevel(vipLevel + 1);
                        //isUpgrade = true;
                        vipLevel = localPlayerModel.getVipLevel();
                        levelCost = cf.Data.VIP_DATA[vipLevel].totalPay;
                        if (vipLevel >= cf.VipMaxLv) {
                            break;
                        }
                    }
                    break;
                case cf.ItemID.LIMTITED_CANNON3:
                    localPlayerModel.addFortSkin(cf.ItemID.LIMTITED_CANNON3, null, itemInfo.strEndTime);
                    //tempRewardList.push(itemInfo);
                    break;
                default:
                    tempRewardList.push(itemInfo);
                    break;
            }
        }
    }
    localPlayerModel.setVipEndTime(resMsg.strVIPEndTime);
    localPlayerModel.setFirstPayRewardInfo(resMsg.nDoubleBilling);
    localPlayerModel.setItemDailyGetMark(resMsg.nDayFirstBilling);

    if (self._target && tempRewardList.length > 0) {
        self._target.addChild(UIAwardToast.create(tempRewardList, function () {

        }, self._target, false), 110);
    }

    switch (cf.SceneMgr.getCurSceneFlag()) {
        case cf.SceneFlag.LOBBY:
            cf.dispatchEvent(LobbyLayer.DoEvent, LobbyLayer.EventFlag.UI_UPDATE);
            break;
        case cf.SceneFlag.GAME:
            cf.dispatchEvent(BaseRoom.DoEvent, BaseRoom.EventFlag.UPDATE_ROOM_UI);

            //月卡
            if (self._GoodsId == 1) {
            }
            break;
    }
};

SDKHelper.clearPayState = function () {
    //清除订单号
    this._orderId = null;
    this._pollingNum = 0;
    this._pollingTime = 2500;
    //清除支付状态
    this._resultState = -1;
    //清除商品ID
    this._GoodsId = -1;
    //清除活动ID
    this._nActID = -1;
    //清空回调函数
    this._payCallBack = null;
};

/**
 * 工具函数
 */

/**
 *
 * @param key
 * @returns {*}
 * @private
 */
SDKHelper._getParamString = function (key) {
    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)return decodeURI(r[2]);
    return null;
};

/**
 * native调用JS 登录逻辑
 */
SDKHelper.loginNativeCallBack = function (jsonData) {
    cc.log("jsonData = " + jsonData);
    //这里jsonData是返回的一个对象，所以需要转换为json
    if (jsonData && jsonData != "") {
        this._uid = jsonData.uid + "";
        this._session = JSON.stringify(jsonData);
    }
    this.askGate(false);
};

SDKHelper.logoutNativeCallBack = function () {
    cf.NetMgr.closeSocket();
    if (cf.SceneMgr.getCurSceneFlag() != cf.SceneFlag.LOGIN) {
        cf.SceneMgr.runScene(cf.SceneFlag.LOGIN, null);
    }
    SDKHelper.logout();
};

/**
 * 退出逻辑
 */
SDKHelper.exitNativeCallBack = function (type) {
    cf.dispatchEvent(LoginScene.EventName, {flag: LoginScene.Event.QUIT, data: type});
};

/**
 * 支付状态
 * @param code
 */
SDKHelper.payNativeCallBack = function (code) {
    var res = parseInt(code);
    //这里状态只要不等于-1，就表示是在支付进行中
    this._resultState = res;
    cc.log("_resultState = " + this._resultState);
    //如果回调时已切入后台则不向下进行
    if (this._isInBack) {
        cc.log("切入后台，保存支付状态");
        cf.dispatchEvent(GameScene.Reload, {bReturn: true});
        return;
    }

    switch (res) {
        case SDKHelper.CODE.SUCCESS:
            SDKHelper.orderQuery(this._orderId, true);
            break;
        case SDKHelper.CODE.CANCEL:
            //cf.UITools.showHintToast("取消支付");
            this.clearPayState();
            break;
        case SDKHelper.CODE.FAILED:
            //cf.UITools.showHintToast("支付失败");
            this.clearPayState();
            break;
    }
};


SDKHelper.setInBack = function (bInBack) {
    this._isInBack = bInBack;
};

SDKHelper.isInBack = function () {
    return this._isInBack;
};

SDKHelper.setTarget = function (target) {
    this._target = target;
};

SDKHelper.setPayCallBack = function (callBack) {
    this._payCallBack = callBack;
};

SDKHelper.setGoodsId = function (goodID) {
    this._GoodsId = goodID;
};
/**
 * AppStore 所需参数
 * @param goodsId
 * @private
 */
SDKHelper._createPayAppStoreJson = function (goodsId) {
    var playerModel = cf.PlayerMgr.getLocalPlayer();
    var shopData = cf.Data.SHOP_DATA[goodsId];

    var payJson = {};
    payJson.gameId = playerModel.getIdLogin();
    payJson.nickName = playerModel.getNickName();
    payJson.lv = playerModel.getPlayerLevel();
    payJson.vip = playerModel.getVipLevel();
    payJson.channelId = GameConfig.channelId;
    payJson.itemName = shopData.goodName;
    payJson.gold = playerModel.getGoldNum();
    payJson.price = shopData.cost;
    payJson.goodsId = goodsId;

    return JSON.stringify(payJson);
};

/**
 * 苹果支付 native调用JS
 * 在大厅请求校验订单并领取订单
 * @param msgData
 */
SDKHelper.payNativeAppStoreCallBack = function (msgData) {
    cf.UITools.showLoadingToast(cf.Language.getText("text_1226"), 60, true, this._target);
    var locPlayer = cf.PlayerMgr.getLocalPlayer();
    if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.LOBBY) {
        var msg = new $root.CH_InHallCheckAndReceiveOrderMsg();
        msg.nIdLogin = locPlayer.getIdLogin();
        msg.strSafeCode = locPlayer.getSafeCode();
        msg.strOtherPar = msgData;
        var wr = $root.CH_InHallCheckAndReceiveOrderMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_InHallCheckAndReceiveOrderMsg, wr);
    }
    else if (cf.SceneMgr.getCurSceneFlag() == cf.SceneFlag.GAME) {
        var msg = new $root.CH_InRoomCheckAndReceiveOrderMsg();
        msg.nIdLogin = locPlayer.getIdLogin();
        msg.strSafeCode = locPlayer.getSafeCode();
        msg.strOtherPar = msgData;
        var wr = $root.CH_InRoomCheckAndReceiveOrderMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CH_InRoomCheckAndReceiveOrderMsg, wr);
    }
};

SDKHelper.payNativeHideLoadingToast = function () {
    cf.UITools.hideLoadingToast();
};

/**
 * 发送心跳检测
 */
SDKHelper.sendKeepAliveMsg = function () {
    if(SDKHelper._keepAliveIntervalFunc){
        clearInterval(SDKHelper._keepAliveIntervalFunc);
    }
    //每间隔5秒向服务器发送一次心跳检测
    SDKHelper._keepAliveIntervalFunc = setInterval(function () {
        //这里的消息不具备任何意义，只需发送一个int即可
        var msg = new $root.CS_DoleGet();
        msg.nSeatID = 1;
        var wr = $root.CS_DoleGet.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.KeepAlive,wr);

        cf.KeepAliveStartTime = Math.floor(new Date().getTime());
        if(!cf.bKeepAliveRes){
            cf.NetDelayTime = Math.floor(new Date().getTime()) - cf.KeepAliveStartTime;
            switch (cf.SceneMgr.getCurSceneFlag()){
                case cf.SceneFlag.LOBBY:
                    cf.dispatchEvent(LobbyLayer.DoEvent,{flag:LobbyLayer.EventFlag.UPDATE_NET_DELAY});
                    break;
                case cf.SceneFlag.GAME:
                    cf.dispatchEvent(BaseRoom.DoEvent,{flag:BaseRoom.EventFlag.UPDATE_NET_DELAY});
                    break;
            }
            //延迟过高处理
        }
        cf.bKeepAliveRes = false;
    },5500);
};

/**
 * 心跳检测
 * @param msg
 */
SDKHelper.keepAliveCallBack = function (msg) {
    //不用做消息的解析，只确认收到即可
    cf.bKeepAliveRes = true;
    cf.NetDelayTime = Math.floor(new Date().getTime()) - cf.KeepAliveStartTime;
    switch (cf.SceneMgr.getCurSceneFlag()){
        case cf.SceneFlag.LOBBY:
            cf.dispatchEvent(LobbyLayer.DoEvent,{flag:LobbyLayer.EventFlag.UPDATE_NET_DELAY});
            break;
        case cf.SceneFlag.GAME:
            cf.dispatchEvent(BaseRoom.DoEvent,{flag:BaseRoom.EventFlag.UPDATE_NET_DELAY});
            break;
    }
    //延迟过高处理
};

//和native同步code值
SDKHelper.CODE = {
    SUCCESS: 500,
    FAILED: 501,
    CANCEL: 502
};
