cf.generateUUID = function () {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
};

/**
 * safe retain
 * @param obj
 * @constructor
 */
cf.SAFE_RETAIN = function (obj) {
    if (obj && obj.retain) {
        obj.retain();
    }
};

/**
 * safe release
 * @param obj
 * @constructor
 */
cf.SAFE_RELEASE = function (obj) {
    if (obj && obj.release) {
        obj.release();
    }
};

/**
 * 设置全局纹理格式
 * @param format 纹理格式
 * @value
 *      cc.Texture2D.PIXEL_FORMAT_RGBA8888
 *      cc.Texture2D.PIXEL_FORMAT_RGB888
 *      cc.Texture2D.PIXEL_FORMAT_RGB565
 *      cc.Texture2D.PIXEL_FORMAT_A8
 *      cc.Texture2D.PIXEL_FORMAT_I8
 *      cc.Texture2D.PIXEL_FORMAT_AI88
 *      cc.Texture2D.PIXEL_FORMAT_RGBA4444
 *      cc.Texture2D.PIXEL_FORMAT_RGB5A1
 *      cc.Texture2D.PIXEL_FORMAT_PVRTC4
 *      cc.Texture2D.PIXEL_FORMAT_PVRTC2
 *      cc.Texture2D.PIXEL_FORMAT_DEFAULT
 */
cf.setDefaultTextureFormat = function (format) {
    if (cc.sys.isNative) {
        cc.Texture2D.defaultPixelFormat = format;
    }
};

/**
 * 移除纹理
 * @param key 纹理
 */
cf.removeTextureForKey = function (key) {
    //H5不确定移除纹理是否适当，暂时只在native版本下使用
    if (!key) {
        cc.warn("removeTextureForKey: " + key + " is null");
    }
    if (cc.sys.isNative) {
        cc.textureCache.removeTextureForKey(key);
        cc.loader.release(key);
    }
};

/**
 * 移除Plist
 * @param plist
 */
cf.removeSpritePlist = function (plist) {
    if (!plist) {
        cc.warn("removeSpritePlist: " + plist + " is null");
    }
    if (cc.sys.isNative) {
        cc.spriteFrameCache.removeSpriteFramesFromFile(plist);
        cc.loader.release(plist);
    }
};

/**
 * 坐标转换
 * @remark 路径坐标转换
 */
cf.convertCoordinates = function (pointX, pointY) {
    return cc.p(pointX * cf.RATIO_X, pointY * cf.RATIO_Y);
};

/**
 * 添加网络监听
 * @param msgId 消息id
 * @param callFunc 回调方法
 * @returns {*}
 */
cf.addNetListener = function (msgId, callFunc) {
    //获取消息配置
    var config = MessageConfig.Message[msgId];
    //创建监听
    var listener = null;
    if (config) {
        listener = cc.EventListener.create({
            event: cc.EventListener.CUSTOM,
            eventName: config.eventName,
            callback: function (event) {
                callFunc(event.getUserData(), msgId);
            }
        });
        cc.eventManager.addListener(listener, 1);

        cc.log("添加网络监听: name = " + config.eventName);
    }
    return listener;
};

/**
 * 添加监听
 * @param eventName
 * @param callFunc
 * @returns {cc.EventListener}
 */
cf.addListener = function (eventName, callFunc) {
    var listener = cc.EventListener.create({
        event: cc.EventListener.CUSTOM,
        eventName: eventName,
        callback: function (event) {
            callFunc(event.getUserData());
        }
    });
    cc.eventManager.addListener(listener, 1);
    return listener;
};

/**
 * 移除监听
 * @param listener
 */
cf.removeListener = function (listener) {
    if (listener) {
        cc.eventManager.removeListener(listener);
    }
};

/**
 * 分发消息
 * @param eventName
 * @param msg
 */
cf.dispatchEvent = function (eventName, msg) {
    var event = new cc.EventCustom(eventName);
    event.setUserData(msg);
    cc.eventManager.dispatchEvent(event);
};

/**
 * 克隆对象数据
 * @param obj
 * @returns {*}
 */
cf.clone = function (obj) {
    var newObj = (obj.constructor) ? new obj.constructor : {};
    for (var key in obj) {
        var copy = obj[key];
        // Beware that typeof null == "object" !
        if (cc.sys.isNative) {
            if (((typeof copy) === "object") && copy && !(copy instanceof cc.Node)) {
                newObj[key] = cf.clone(copy);
            } else {
                newObj[key] = copy;
            }
        } else {
            if (((typeof copy) === "object") && copy && !(copy instanceof cc.Node) && !(copy instanceof HTMLElement)) {
                newObj[key] = cf.clone(copy);
            } else {
                newObj[key] = copy;
            }
        }

    }
    return newObj;
};

/**
 * 随机范围
 * @param Min
 * @param Max
 * @returns {*}
 */
cf.randomRange = function (Min, Max) {
    var Range = Max - Min;
    var Rand = Math.random();
    return (Min + Math.round(Rand * Range));
};

/**
 * 随机布尔值
 * @returns {boolean}
 */
cf.randomBool = function () {
    var val = cf.randomRange(0, 1);
    return (val === 0);
};

/**
 * 转换座位id
 * @param seatId
 * @returns {*}
 */
cf.convertSeatId = function (seatId) {
    var mySeatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
    var maxSeatsNum = cf.RoomMgr.getMaxRoomNum();
    if (mySeatId >= maxSeatsNum / 2) {
        seatId = (seatId + maxSeatsNum / 2) % maxSeatsNum;
    }
    return seatId;
};

/**
 * 根据座位id转换坐标
 * @param pos
 */
cf.convertPosWithSeatId = function (pos) {
    var seatId = cf.PlayerMgr.getLocalPlayer().getSeatID();
    if (seatId >= 2) {
        pos.x = cc.winSize.width - pos.x;
        pos.y = cc.winSize.height - pos.y;
    }
};

/**
 * 根据座位id转换角度
 * @param seatId
 * @param angle
 * @returns {*}
 */
cf.convertAngleWithSeatId = function (seatId, angle) {
    if (seatId >= 2) {
        angle = -180 + angle;
    }
    return angle;
};

cf.pointRotateByAngle = function (inPoint, centerX, centerY, angle) {
    var cosa = Math.cos(angle), sina = Math.sin(angle);

    var subPointX = inPoint.x - centerX;
    var subPointY = inPoint.y - centerY;

    inPoint.x = subPointX * cosa + subPointY * sina + centerX;
    inPoint.y = -subPointX * sina + subPointY * cosa + centerY;
    return inPoint;
};

cf.collisionPointAndCircle = function (point, circle) {
    var dis = cc.pDistanceSQ(point, circle.pt);
    var r = circle.r * circle.r;
    return (dis < r);
};

cf.collisionPointAndCircleEx = function (pointX, pointY, circle) {
    var subx = pointX - circle.pt.x;
    var suby = pointY - circle.pt.y;
    var dis = subx * subx + suby * suby;
    var r = circle.r * circle.r;
    return (dis < r);
};

cf.collisionCircleAndCircle = function (circleA, circleB) {
    var dis = cc.pDistanceSQ(circleA.pt, circleB.pt);
    var r = circleA.r + circleB.r;
    return (dis < r * r);
};

cf.collisionCircleAndCircleEx = function (circleA, circleB) {
    var subx = circleA.pt.x - circleB.pt.x;
    var suby = circleA.pt.y - circleB.pt.y;
    var dis = subx * subx + suby * suby;
    var r = circleA.r + circleB.r;
    return (dis < r * r);
};

cf.checkCanLock = function (node) {
    /*var w, h, offset;
     if (cf.IS_IPAD) {
     w = cc.winSize.width + cf.RATIO_X;
     h = cc.winSize.height;
     offset = cf.RATIO_X;
     } else {
     w = cc.winSize.width;
     h = cc.winSize.height;
     offset = 0;
     }
     return (node.x >= -offset && node.x <= w && node.y >= 0 && node.y <= h);*/
    if (node) {
        var spWidth = 0.25 * node._sprite.width;
        var spheight = 0.25 * node._sprite.height;
        return (node.x >= 0 - spWidth && node.x <= cc.winSize.width + spWidth && node.y >= 0 - spheight && node.y <= cc.winSize.height + spheight);
    }
    return false;
};

/**
 * 检测屏幕内
 */
cf.checkInScreen = function (node) {
    return ( node.x >= 0 && node.x <= cc.winSize.width && node.y >= 0 && node.y <= cc.winSize.height );
};

/**
 * 生成屏蔽字map
 */
cf.buildMap = function (wordList) {
    var result = {};
    var count = wordList.length;
    for (var i = 0; i < count; ++i) {
        var map = result;
        var word = wordList[i];
        for (var j = 0; j < word.length; ++j) {
            var ch = word.charAt(j);
            if (typeof(map[ch]) != "undefined") {
                map = map[ch];
                if (map["empty"]) {
                    break;
                }
            }
            else {
                if (map["empty"]) {
                    delete map["empty"];
                }
                map[ch] = {"empty": true};
                map = map[ch];
            }
        }
    }
    return result;
};

/**
 * 查看包含的屏蔽字
 * return被屏蔽的数组
 */
cf.checkWarningWord = function check(map, content) {
    var result = [];
    var count = content.length;
    var stack = [];
    var point = map;
    for (var i = 0; i < count; ++i) {
        var ch = content.charAt(i);
        var item = point[ch];
        if (typeof(item) == "undefined") {
            i = i - stack.length;
            stack = [];
            point = map;
        }
        else if (item["empty"]) {
            stack.push(ch);
            result.push(stack.join(""));
            stack = [];
            point = map;
        }
        else {
            stack.push(ch);
            point = item;
        }
    }
    return result;
};

/**
 * 检测字符串中是否包含违禁词汇如果包含则使用*替换
 */
cf.checkStrIsIllicit = function (str) {
    var map = cf.blockMap;
    var reg = /[\u4e00-\u9fa5]/g;
    if (str.match(reg) != null) {
        var localStr = str.match(reg).join("");
        var checkArray = cf.checkWarningWord(map, localStr);
        for (var index in checkArray) {
            var length = checkArray[index].length;
            if (length > 0) {
                for (var i = 0; i < length; i++) {
                    var warnWordLength = checkArray[index][i].length;
                    for (var j = 0; j < warnWordLength; j++) {
                        str = str.replace(checkArray[index][i][j], "*");
                    }
                }
            }
        }
    }
    return str;
};

/**
 * str = str1, str2 | str3, str4 | ...
 * @param str
 */
cf.parseString = function (str) {
    if (str == "") {
        return null;
    }

    if (!cc.isString(str)) {
        cc.warn("cf.parseString:not string!!!");
        return null;
    }
    var map = [];
    //容错处理
    str = str.replace(/｜/g, "|");
    var arr1 = str.split("|");
    var len = arr1.length;
    for (var i = 0; i < len; i++) {
        var arr2 = arr1[i].split(",");
        map.push(arr2);
    }
    return map;
};

/**
 * 以key val的形式将字符串解析并存储
 * str = str1, str2 | str3, str4 | ...
 * @param str
 */
cf.parseStringForMap = function (str) {
    if (str == "") {
        return null;
    }
    cc.log(str);
    if (!cc.isString(str)) {
        cc.warn("cf.parseString:not string!!!");
        return null;
    }
    var map = cf.Map.create();

    //容错处理
    str = str.replace(/｜/g, "|");

    var arr1 = str.split("|");
    var len = arr1.length;
    for (var i = 0; i < len; i++) {
        var arr2 = arr1[i].split(",");
        var tmpLength = arr2.length;
        var arr3 = [];
        for (var j = 1; j < tmpLength; j++) {
            arr3.push(arr2[j]);
        }
        map.put(arr2[0], arr3);
    }
    return map;
};

/**
 * 替换所有匹配exp的字符串为指定字符串
 * @param exp 被替换部分的正则
 * @param newStr 替换成的字符串
 */
String.prototype.replaceAll = function (exp, newStr) {
    return this.replace(new RegExp(exp, "gm"), newStr);
};

/**
 * 原型：字符串格式化
 * @param args 格式化参数值
 */
String.prototype.format = function (args) {
    var result = this;
    if (arguments.length < 1) {
        return result;
    }

    var data = arguments; // 如果模板参数是数组
    if (arguments.length == 1 && typeof (args) == "object") {
        // 如果模板参数是对象
        data = args;
    }
    for (var key in data) {
        var value = data[key];
        if (undefined != value) {
            result = result.replaceAll("\\{" + key + "\\}", value);
        }
    }
    return result;
};

/**
 * 原型：字符串格式化
 * @param args 格式化参数值
 */
String.prototype.formatEx = function (args) {
    var result = this;
    if (arguments.length < 1) {
        return result;
    }

    var data = arguments; // 如果模板参数是数组
    if (arguments.length == 1 && typeof (args) == "object") {
        // 如果模板参数是对象
        data = args;
    }
    for (var key in data) {
        var value = data[key];
        if (undefined != value) {
            result = result.replaceAll("\\{" + key + "\\}", "|" + value + "|");
        }
    }
    return result;
};

/**
 * 格式化时间: 10小时20分20秒
 * @param time
 * @returns {string}
 */
cf.timeFormat = function (time, isNumber) {
    var str = "0秒";
    if (time > 0) {
        //换算成秒
        var _time = time / 1000;
        var hours = Math.floor(_time / 3600);
        var minutes = Math.floor((_time - hours * 3600) / 60);

        if (isNumber) {
            str = hours + "时" + minutes + "分";
            if (hours <= 0) {
                str = minutes + "分";
            }
            else if (hours >= 24) {
                var day = Math.floor(_time / 3600 / 24);
                str = day + "天";
            }
        }
        else {
            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            str = hours + ":" + minutes;
            if (hours >= 96) {
                var day = Math.floor(_time / 3600 / 24);
                str = day + "天";
            }
        }
    }
    return str;
};

String.prototype.lenB = function () {
    return this.replace(/[^\x00-\xff]/g, "**").length;
};

cf.dumpTextureInfo = function () {
    if (cc.sys.isNative) {
        cc.log(cc.textureCache.getCachedTextureInfo());
    }
};

/**
 * 检查非数字字幕
 * @param str
 * @returns {string}
 */
cf.checkCHWord = function (str) {
    return str.replace(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g, "");
};

/**
 * 日期转时间戳
 * @param strDate
 * @returns {number}
 */
cf.dateToTime = function (strDate) {
    var str = strDate.replace(/-/g, '/');
    var date = new Date(str);
    var time = date.getTime();
    return time;
};

/**
 * 数量单位转换，向下取整
 * @param num 数字
 * @param digits 保留位数
 * @returns {string}
 */
cf.numberToW = function (num, digits) {
    var _num = num;
    var _unit = "";
    if (_num > 10000) {
        _num = _num / 10000;
        //单位万
        _unit = "万";
        if (_num >= 10000) {
            _num = _num / 10000;
            //单位亿
            _unit = "亿";
        }
    }

    if (digits == undefined)
        digits = 0;

    if (digits == 0) {
        return _num + _unit;
    }
    return Math.floor(_num * Math.pow(10, digits)) / Math.pow(10, digits) + _unit;
};

cf.numberToY = function (num, digits) {
    var _num = num;
    var _unit = "";
    if (_num >= 100000000) {
        _num = _num / 100000000;
        //单位亿
        _unit = "亿";
    }

    if (digits == undefined)
        digits = 0;

    if (digits == 0) {
        return _num + _unit;
    }
    return Math.floor(_num * Math.pow(10, digits)) / Math.pow(10, digits) + _unit;
};

cf.getPicName = function (url) {
    var length = url.length;
    var picName = [];
    for (var i = length - 1; i >= 0; i--) {
        if (url[i] != "/") {
            picName.push(url[i]);
        }
        else {
            break;
        }
    }
    cc.log("/");
    cc.log(picName.reverse().join(""));
    return picName.reverse().join("");
};

/**
 * 时间戳转日期
 * @param str
 * @returns {string}
 * @constructor
 */
cf.TimeToDate = function (str) {
    var date = new Date();
    date.setTime(parseInt(str));
    var sTime = date.getFullYear() + "-";
    // 月
    var sMonth = date.getMonth() + 1;
    // 日
    var sDay = date.getDate();
    // 时
    var sHour = date.getHours();
    // 分
    var sMinut = date.getMinutes();
    // 秒
    var sSecond = date.getSeconds();

    sTime += sMonth + "-" + sDay + " " + sHour + ":" + sMinut + ":" + sSecond;

    return sTime;
};
/**
 * "|"文本换行
 * @param str
 * @returns {string}
 * @private
 */
cf.changeText = function (str) {

    var _arrtext = str.split("|");
    var len = _arrtext.length;
    var _text = "";
    for (var i = 0; i < len; i++) {
        _text += _arrtext[i];
        if (i < len) {
            _text += "\n\n";
        }
    }
    return _text;
};

cf.checkEnterRoomId = function () {
    var gold = cf.PlayerMgr.getLocalPlayer().getGoldNum();
    var roomId = 1002;
    for (var i = 0; i < 3; ++i) {
        var roomData = cf.Data.ROOM_DATA[1002 + i];
        var min = parseInt(roomData.gold_limit[0]);
        var max = parseInt(roomData.gold_limit[1]);
        if (gold > min && gold < max) {
            roomId = 1002 + i;
        }
        if (gold > min && max == 0) {
            roomId = 1004;
        }
    }
    return roomId;
};
/**
 * 转换显示时间
 * @param str 格式（年/月/日）
 * @returns {string}
 */
cf.getStrTime = function (str) {
    var date = new Date();
    date.setTime(parseInt(str));
    return date.getFullYear() + cf.Language.getText("text_1126") +
        (date.getMonth() + 1) + cf.Language.getText("text_1127") +
        date.getDate() + cf.Language.getText("text_1128");
};
/**
 * 转换显示时间
 * @param str 格式（时分）
 * @returns {string}
 */
cf.getStrHourTime = function (str) {
    var date = new Date();
    date.setTime(parseInt(str));
    return date.getHours() + ":" + date.getMinutes();
};
/**
 * 获取本次活动在本地记录的位置
 * @param jsonData
 */
cf.getJSONIndex = function (jsonData, type) {
    var index = -1;
    var playerModel = cf.PlayerMgr.getLocalPlayer();
    for (var i in jsonData) {
        if (jsonData[i].nType == type) {
            if (jsonData[i].strStartShowTime < playerModel.getGameTime() ||
                jsonData[i].strEndShowTime > playerModel.getGameTime()) {
                index = i;
                break;
            }
        }
    }
    return index;
};

/**
 * 版本号转换为数字
 * @param ver
 * @returns {string}
 */
cf.versionToNum = function (ver) {
    var a = ver.toString();
    var c = a.split('.');
    var num_place = ["", "0", "00", "000"], r = num_place.reverse();
    for (var i = 1; i < c.length; i++) {
        var len = c[i].length;
        c[i] = r[len] + c[i];
    }
    return c.join("");
};

cf.indexToCannonLevel = function (index) {
    var data = cf.Data.CANNON_LEVEL_DATA;
    return data[index].cannonlevel;
};

cf.enableNoSleep = function () {
    if (cf.NoSleep == null) {
        cf.NoSleep = new NoSleep();
    }

    if (cf.NoSleep) {
        cf.NoSleep.enable();
    }
};

cf.disableNoSleep = function () {
    if (cf.NoSleep) {
        cf.NoSleep.disable();
    }
};

