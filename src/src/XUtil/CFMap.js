cf.Map = cc.Class.extend({
    _elements: null
});

/**
 * 创建Map
 * @returns {*}
 */
cf.Map.create = function () {
    var res = new cf.Map();
    res.clear();
    return res;
};

/**
 * Map元素的长度
 * @returns {*}
 */
cf.Map.prototype.size = function () {
    return this._elements.length;
};

/**
 * 判断Map是否为空
 * @returns {boolean}
 */
cf.Map.prototype.isEmpty = function () {
    return (this._elements.length < 1);
};

cf.Map.prototype.clear = function () {
    this._elements = new Array();
};

/**
 * 向Map中增加元素
 * @param key
 * @param value
 */
cf.Map.prototype.put = function (key, value) {
    if (this.containKey(key)) {
        this.remove(key);
    }
    this._elements.push({
        key: key,
        value: value
    });
};

/**
 * 向Map中插入元素
 * @param index
 * @param key
 * @param value
 */
cf.Map.prototype.insert = function (index, key, value) {
    if (this.containKey(key)) {
        this.remove(key);
    }
    this._elements.splice(index, 0, {
        key: key,
        value: value
    });
};

/**
 * 删除指定Key的元素
 * @param key
 * @returns {boolean}
 */
cf.Map.prototype.remove = function (key) {
    var ret = false;
    try {
        var length = this._elements.length;
        for (var i = 0; i < length; i++) {
            if (this._elements[i].key == key) {
                this._elements.splice(i, 1);
                return true;
            }
        }
    } catch (e) {
        ret = false;
    }
    return ret;
};

/**
 * 获取指定Key的元素值Value，失败返回NULL
 * @param key
 * @returns {*}
 */
cf.Map.prototype.get = function (key) {
    try {
        var length = this._elements.length;
        for (var i = 0; i < length; i++) {
            if (this._elements[i].key == key) {
                return this._elements[i].value;
            }
        }
    } catch (e) {
        return null;
    }
};

/**
 * 设置指定Key的元素值Value，失败返回FALSE
 * @param key
 * @param value
 * @returns {boolean}
 */
cf.Map.prototype.set = function (key, value) {
    try {
        var length = this._elements.length;
        for (var i = 0; i < length; i++) {
            if (this._elements[i].key == key) {
                this._elements[i].value = value;
                return true;
            }
        }
    } catch (e) {
        return false;
    }
};

/**
 * 通过Index获取对应的Value
 * @param index
 * @returns {*}
 */
cf.Map.prototype.getValueByIndex = function (index) {
    return this._elements[index].value;
};

/**
 * 通过Key获取对应的Index
 * @param key
 * @returns {number}
 */
cf.Map.prototype.getIndexByKey = function (key) {
    try {
        var length = this._elements.length;
        for (var i = 0; i < length; i++) {
            if (this._elements[i].key == key) {
                return i;
            }
        }
    } catch (e) {
        return -1;
    }
};

/**
 * 通过Index获得Key
 * @param index
 * @returns {*}
 */
cf.Map.prototype.getKeyByIndex = function (index) {
    return this._elements[index].key;
};

cf.Map.prototype.removeByIndex = function (index) {
    this.remove(this._elements[index].key);
};

/**
 * 获取指定索引的元素
 * @param index
 * @returns {*}
 */
cf.Map.prototype.element = function (index) {
    if (index < 0 || index >= this._elements.length) {
        return null;
    }
    return this._elements[index];
};

/**
 * 判断Map中是否含有指定KEY的元素
 * @param key
 * @returns {boolean}
 */
cf.Map.prototype.containKey = function (key) {
    var ret = false;
    try {
        var length = this._elements.length;
        for (var i = 0; i < length; i++) {
            if (this._elements[i].key == key) {
                ret = true;
            }
        }
    } catch (e) {
        ret = false;
    }
    return ret;
};

/**
 * 判断Map中是否含有指定VALUE的元素
 * @param value
 * @returns {boolean}
 */
cf.Map.prototype.containValue = function (value) {
    var ret = false;
    try {
        var length = this._elements.length;
        for (var i = 0; i < length; i++) {
            if (this._elements[i].value == value) {
                ret = true;
            }
        }
    } catch (e) {
        ret = false;
    }
    return ret;
};

/**
 * 获取Map中所有VALUE的数组
 * @returns {Array}
 */
cf.Map.prototype.values = function () {
    var arr = [];
    var length = this._elements.length;
    for (var i = 0; i < length; i++) {
        arr.push(this._elements[i].value);
    }
    return arr;
};

/**
 * 获取Map中所有KEY的数组
 * @returns {Array}
 */
cf.Map.prototype.keys = function () {
    var arr = [];
    var length = this._elements.length;
    for (var i = 0; i < length; i++) {
        arr.push(this._elements[i].key);
    }
    return arr;
};
/**
 * 增加Map排序（根据Key值）
 * @constructor
 */
cf.Map.prototype.MapSort = function(){
    this._elements.sort(function(a,b){return a.key-b.key});
};