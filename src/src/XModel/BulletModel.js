var BulletModel = cc.Class.extend({
    _nSeatID: null,                     //< 座位id
    _strBulletIndex: null,              //< 子弹索引
    _nBulletLevel: null,                //< 和炮倍关联
    _nBulletValue: null,                //< 子弹分数值
    _fAngle: null,                      //< 飞行角度
    _strBulletTime: null,               //< 发射时间
    _strServerTime: null,               //< 服务器发射时间
    _bulletPos: null,                   //< 发射位置
    _bulletType: null,                  //< 子弹类型
    _lockedElement: null,               //< 被锁定的精灵
    _moveAction: null,                  //< 移动动画
    _diedAction: null,                  //< 死亡动画
    _hitTotalCount: null,               //< 反弹次数
    _nSpeed: null,                      //< 子弹速度

    setSeatID: function (val) {
        this._nSeatID = val;
    },

    getSeatID: function () {
        return this._nSeatID;
    },

    setServerIndex: function (val) {
        this._strBulletIndex = val;
    },

    getServerIndex: function () {
        return this._strBulletIndex;
    },

    setBulletLevel: function (val) {
        this._nBulletLevel = val;
    },

    getBulletLevel: function () {
        return this._nBulletLevel;
    },

    setBulletValue: function (val) {
        this._nBulletValue = val;
    },

    getBulletValue: function () {
        return this._nBulletValue;
    },

    setAngle: function (val) {
        this._fAngle = val;
    },

    getAngle: function () {
        return this._fAngle;
    },

    setBulletTime: function (val) {
        this._strBulletTime = val;
    },

    getBulletTime: function () {
        return this._strBulletTime;
    },

    setServerTime: function (val) {
        this._strServerTime = val;
    },

    getServerTime: function () {
        return this._strServerTime;
    },

    setBulletPos: function (val) {
        this._bulletPos = val;
    },

    getBulletPos: function () {
        return this._bulletPos;
    },

    setBulletType: function (val) {
        this._bulletType = val;
    },

    getBulletType: function () {
        return this._bulletType;
    },

    setLockedElement: function (val) {
        this._lockedElement = val;
    },

    getLockedElement: function () {
        return this._lockedElement;
    },

    /**
     * 设置移动动画
     * @param val
     */
    setMoveAction: function (val) {
        this._moveAction = val;
    },

    /**
     * 获得移动动画
     * @returns {null}
     */
    getMoveAction: function () {
        return this._moveAction;
    },

    /**
     * 设置死亡动画
     * @param val
     */
    setDiedAction: function (val) {
        this._diedAction = val;
    },

    /**
     * 获得死亡动画
     * @returns {*}
     */
    getDiedAction: function () {
        return this._diedAction;
    },

    /**
     * 设置子弹反弹次数
     * @param val
     */
    setBulletHitCount: function (val) {
        this._hitTotalCount = val;
    },

    /**
     * 获得子弹反弹次数
     * @returns {null}
     */
    getBulletHitCount: function () {
        return this._hitTotalCount;
    },

    /**
     * 设置子弹速度
     * @param val 速度
     */
    setBulletSpeed:function (val) {
        this._nSpeed = val;
    },

    /**
     * 获得子弹速度
     * @returns {null}
     */
    getBulletSpeed:function () {
        return this._nSpeed;
    }
});

BulletModel.create = function () {
    return new BulletModel();
};