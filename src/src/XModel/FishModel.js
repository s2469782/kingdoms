var FishModel = cc.Class.extend({
    _serverIndex: null,     //< 服务器索引
    _fishId: null,          //< 鱼种id
    _fishType: null,        //< 鱼类型id

    _killRate: 0,           //< 击杀概率
    _baseGold: 0,           //< 金币基础值
    _fishScore: 0,          //< 鱼分数
    _fishSpeed: 1.0,        //< 鱼速度

    _animateType: null,     //< 动画类型
    _moveAction: null,      //< 移动动作名称
    _diedAction: null,      //< 死亡动作名称

    _showGold: 0,           //< 显示金币特效
    _showBonus: 0,          //< 显示彩金特效

    _collisionId: 0,        //< 碰撞检测id

    _theZoom: 1,            //< 缩放

    _killEffect: null,      //< 彩金击杀特效

    _bonusNum: 0,
    _randomScore: false,    //<随机分数开启
    _music: null,           //< 音效
    _bFeatsFish: false,      //是否为功绩鱼

    /**
     * 初始化数据
     * @param id    鱼种类id
     * @param serverIndex 服务器索引
     * @returns {boolean}
     */
    initData: function (id, serverIndex) {

        //从数据表获取数据
        var fishData = cf.Data.FISH_DATA[id];
        if (!fishData) {
            cc.log("数据获取错误：fishInfo = " + id);
            return false;
        }

        this._serverIndex = serverIndex;
        this._fishId = id;

        this._fishType = fishData.fishType;
        this._killRate = fishData.killRate;
        this._baseGold = fishData.baseGold;
        if(fishData.baseGold[1] > fishData.baseGold[0]){
            this._randomScore = true;
        }else{
            this._randomScore = false;
        }
        this._fishScore = fishData.fishScore;
        this._fishSpeed = fishData.fishSpeed;
        this._animateType = fishData.animateType;
        if(fishData.animateType == cf.AnimateType.FRAME)
            this._moveAction = cf.Data.ANIMATE_DATA[fishData.moveAction].frameName;
        this._diedAction = fishData.diedAction;
        this._showGold = fishData.showGold;
        this._showBonus = fishData.showBonus;
        this._collisionId = fishData.moveAction;
        this._theZoom = fishData.theZoom;
        this._killEffect = fishData.killEffect;
        this._music = fishData.music;
        this._bFeatsFish = false;

        return true;
    },

    /**
     * 获取服务器索引
     * @returns {null}
     */
    getServerIndex: function () {
        return this._serverIndex;
    },

    /**
     * 获取鱼种id
     * @returns {null}
     */
    getId: function () {
        return this._fishId;
    },
    /**
     * 获取鱼类型
     * @returns {null}
     */
    getType: function () {
        return this._fishType;
    },

    /**
     * 获取索引id
     * @returns {null}
     */
    getIndex: function () {
        return this._nIndex;
    },

    /**
     * 设置分数
     * @returns {number}
     */
    setScore: function (num) {
        this._fishScore = num;
    },

    /**
     * 获取分数
     * @returns {number}
     */
    getScore: function () {
        return this._fishScore;
    },

    /**
     * 获取动画类型
     * @returns {null}
     */
    getAnimateType: function () {
        return this._animateType;
    },

    /**
     * 获取Move动作名称
     * @returns {null}
     */
    getMoveAction: function () {
        return this._moveAction;
    },

    /**
     * 获取Died动作名称
     * @returns {null}
     */
    getDiedAction: function () {
        return this._diedAction;
    },

    /**
     * 获得击杀概率
     * @returns {number}
     */
    getKillRate: function () {
        return this._killRate;
    },

    /**
     * 获得缩放大小
     * @returns {number}
     */
    getZoom: function () {
        return this._theZoom;
    },

    /**
     * 获得击杀特效
     * @param roomId 房间id
     * @returns {*}
     */
    getKillEffect: function (roomId) {
        if (this._killEffect != null) {
            return this._killEffect.get(roomId);
        }
        return null;
    },

    /**
     * 获得音效
     * @returns {null}
     */
    getMusic: function () {
        return this._music;
    },

    /**
     * 鱼是否为彩金鱼
     */
    getBFeatsFish: function () {
        return this._bFeatsFish;
    }
});

/**
 * 创建鱼数据模型
 * @param id 鱼种类id
 * @param index 服务器索引
 * @returns {null}
 */
FishModel.create = function (id, index) {
    var model = new FishModel();
    if (model && model.initData(id, index)) {
        return model;
    }
    return null;
};

