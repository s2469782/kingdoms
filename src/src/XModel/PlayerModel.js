var PlayerModel = cc.Class.extend({
    _nIdLogin: null,                //< 服务器玩家id
    _strGamePlayerID: null,         //< 玩家id，客户端显示用
    _strNickName: null,             //< 玩家昵称
    _nPlayerLevel: null,            //< 玩家等级
    _nVIPLevel: null,               //< VIP等级
    _nGoldNum: null,                //< 金币数
    _nDiamondNum: null,             //< 钻石数
    _nFortData: null,               //< 最大炮倍数
    _strPhone: null,                //< 玩家手机号
    _strSafeCode: null,             //< 安全码
    _nSeatID: null,                 //< 座位id
    _nHeadID: null,                 //< 头像id
    _nJackpot: null,                //< 个人奖池
    _nExp: null,                    //< 玩家经验值
    _nSex: null,                    //< 玩家性别选择
    _strItemList: null,             //< 背包道具，格式：道具id,道具数量|道具id,道具数量
    _strDayMission: null,           //< 日常任务记录
    _strWeekMission: null,          //< 周常任务记录
    _nSeriesForeverRecord: null,    //< 永久系列任务完成记录
    _strSeriesRecord: null,         //< 正在进行的系列任务记录
    _nFortSkinUse: null,            //< 炮台皮肤
    _nVIPExp: null,                 //< VIP经验
    _strWeekData: null,             //< 周记录
    _nCoupons: null,                //< 点券
    _nLottery: null,                //< 玩家奖券
    _strVIPEndTime: null,           //< vip月卡到期时间
    _strLoginData: null,            //< 每日登陆

    _bulletList: null,              //< 子弹列表
    _nFortMode: null,               //< 玩家单/双炮模式，1单炮，2双炮
    _nFortType: null,               //< 当前炮倍(炮倍索引)
    _random: null,                  //< 随机数对象
    _nDoleCoolDownTime: null,       //< 救济金倒计时
    _strDailyData: null,             //< 日记录
    _strOneTimeData: null,           //< 一次性记录

    _strFortSkin: null,             //< 已拥有的皮肤

    _nUpdateRankTime: 0,              //< 排行榜生成时间

    _fAccPoints: 0.0,                //< 玩家积分

    _exchangeInfo: null,             //< 兑换整体数据
    _nUpdateExchangeTime: -1,      //< 兑换整体刷新时间

    //客户端自己定义
    _nDayActiveVal: 0,               //< 日活跃值
    _nWeekActiveVal: 0,              //< 周活跃值
    _dayActiveFlag: null,           //< 日活跃领取标记
    _weekActiveFlag: null,          //< 周活跃领取标记
    _nBonusFishNum: 0,               //< 玩家打到彩金鱼数量
    _FirstBonus: 0,                  //<  个人首次奖池抽奖标记
    _VIPGift: 0,                     // vip领奖记录，用位数表示
    _gameTime: 0,                    //< 全局游戏时间

    _strEmailData: null,            //< 邮件标记

    _ModifyNickname: 0,              //< 修改昵称次数
    _LoginDayDate: null,             //< 累计登录记录
    _LoginWeekDate: null,            //< 周记录
    _LoginDayNumDate: 0,              //< 连续登录天数记录
    _nNewLoginCount: 0,               //< 累计登录的次数
    _leaveRoomInfo: null,            //<跳转信息
    _actRecordList: null,            //< 活动记录
    _bulletCount: 0,                  //< 子弹发射次数
    _firstPayRewardInfo: null,       //< 商店道具的首充奖励标示
    _itemDailyGetMark: null,         //< 月卡道具的每日领取
    _vipBulletHitCount: null,        //< vip子弹反弹次数
    _consumeSkillByDiamondNeedPrompt: true, //< 通过钻石使用技能是否给出提示
    _taskFinishCount: null,            //< 任务完成数量
    _stopSendMsg: null,                //玩家执行离开房间的操作后停止发送消息
    _totalTreasureNum: 0,               //玩家寻宝总次数
    _treasureNum: 0,                    //玩家当前的剩余寻宝次数
    _nBillingWelfare: 0,                 //福利金

    _nSinglePairCannon: 0,              //单双炮  -1:永久开启单双 0:没开启单双炮  时间戳代表使用的结束时间
    _noviceGuideMask: null,            //新手引导的标记
    _isBUpdateGrandRank: false,          //临时记录大奖赛标记位
    _firstPayMark: null,               //首充礼包标记
    _fTempAccPoints: -1,                //< 进渔场零时记录玩家积分
    _lastGoldNum: 0,                     //< 记录进入渔场时的金币数
    _rankPlayerScore:0,                  //< 玩家争霸赛功绩值
    _pokerNum: 0,                        //< 扑克牌数量
    _rankDayPlayerState:0,                  //< 争霸赛日排行榜状态值
    _rankWeekPlayerState:0,                  //< 争霸赛周排行榜状态值
    _RouletteRecord:null,                   //< 抽奖记录
    _newPlayerLotteryGetSign:"",         //< 新手获取奖券标示
    _isOpenNewPlayerLotteryGet: false,  //< 是否开始新玩家的点劵摇奖获取
    _hasDrawLottery: false,             //< 已经签到摇奖完成
    _playerSignature: "",                //< 玩家签到
    _multipleLotteryNum: "",                    //< 兑换券奖励倍数
    _nPermission: 0,                     //<权限
    _playerChipNum:0,                    //<玩家筹码数量
    

    //龙珠相关
    _dragonBallRoomData:[],  //<房间龙珠数据
    _dragonString: null,
    _dragonSkill: null,
    

    setIdLogin: function (val) {
        this._nIdLogin = val;
    },

    getIdLogin: function () {
        return this._nIdLogin;
    },

    setGamePlayerId: function (val) {
        this._strGamePlayerID = val;
    },

    getGamePlayerId: function () {
        return this._strGamePlayerID;
    },

    setNickName: function (val) {
        this._strNickName = val;
    },

    getNickName: function () {
        return this._strNickName;
    },

    setPlayerLevel: function (val) {
        this._nPlayerLevel = val;
    },

    getPlayerLevel: function () {
        return this._nPlayerLevel;
    },

    setVipLevel: function (val) {
        this._nVIPLevel = val;
    },

    getVipLevel: function () {
        return this._nVIPLevel;
    },

    setGoldNum: function (val) {
        this._nGoldNum = val;
    },

    getGoldNum: function () {
        return this._nGoldNum;
    },

    setDiamondNum: function (val) {
        this._nDiamondNum = val;
    },

    getDiamondNum: function () {
        return this._nDiamondNum;
    },

    /**
     * 设置最大炮倍（索引）
     * @param val
     */
    setFortData: function (val) {
        this._nFortData = val;
    },

    /**
     * 获取最大炮倍（索引）
     * @returns {null}
     */
    getFortData: function () {
        return this._nFortData;
    },

    setPhone: function (val) {
        this._strPhone = val;
    },

    getPhone: function () {
        return this._strPhone;
    },

    setSafeCode: function (val) {
        this._strSafeCode = val;
    },

    getSafeCode: function () {
        return this._strSafeCode;
    },

    setSeatID: function (val) {
        this._nSeatID = val;
    },

    getSeatID: function () {
        return this._nSeatID;
    },

    setHeadId: function (val) {
        this._nHeadID = val;
    },

    getHeadId: function () {
        return this._nHeadID;
    },

    setBulletList: function (val) {
        this._bulletList = val;
    },

    getBulletList: function () {
        return this._bulletList;
    },

    /**
     * 设置当前炮倍（索引）
     * @param val
     */
    setFortType: function (val) {
        this._nFortType = val;
    },

    /**
     * 获取当前炮倍（索引）
     * @returns {null}
     */
    getFortType: function () {
        return this._nFortType;
    },

    setFortMode: function (val) {
        this._nFortMode = val;
    },

    getFortMode: function () {
        return this._nFortMode;
    },

    setRandom: function (seed) {
        this._random = new cf.Random(seed);
    },

    getRandom: function () {
        return this._random;
    },

    setExp: function (val) {
        this._nExp = val;
    },

    getExp: function () {
        return this._nExp;
    },

    setJackpot: function (val) {
        this._nJackpot = val;
    },

    getJackpot: function () {
        return this._nJackpot;
    },

    setSex: function (val) {
        this._nSex = val;
    },

    getSex: function () {
        return this._nSex;
    },

    setItemList: function (val) {
        this._strItemList = cf.parseStringForMap(val);
    },

    getItemList: function () {
        return this._strItemList;
    },

    /**
     * 日常任务
     * @remark 日常任务记录，每日重置，格式：
     * 任务id,目标完成数量,领奖情况|
     * 领奖情况为：0不能领奖，1可以领奖，2已经领奖
     * @param val
     */
    setDayMission: function (val) {
        this.setTaskCount(cf.TaskType.DAY, 0);
        this._strDayMission = this._parseTaskString(val, cf.TaskType.DAY);
    },

    getDayMission: function () {
        return this._strDayMission;
    },


    setDragonBonusString: function(val){
        this._dragonString = val;

    },

    getDragonBonusString: function(){
        return this._dragonString;
    },

    setDragonSkill: function(val){
        this._dragonSkill = val;

    },

    getDragonSkill: function(){
        
        return this._dragonSkill;

    },

   
    /**
     * 周常任务
     * @remark
     * 周常任务记录，每周重置，格式：
     * 任务id,目标完成数量,领奖情况|
     * 领奖情况为：0不能领奖，1可以领奖，2已经领奖
     * @param val
     */
    setWeekMission: function (val) {
        this.setTaskCount(cf.TaskType.WEEK, 0);
        this._strWeekMission = this._parseTaskString(val, cf.TaskType.WEEK);
    },

    getWeekMission: function () {
        return this._strWeekMission;
    },

    setSeriesForeverRecord: function (val) {
        this._nSeriesForeverRecord = val;
    },

    getSeriesForeverRecord: function () {
        return this._nSeriesForeverRecord;
    },

    /**
     * 成长任务
     * @remark
     * 正在进行的永久系列任务记录，格式为
     * 任务id,系列id,任务目标达成数量,领奖情况|
     * 领奖情况为：0不能领奖，1可以领奖，2已经领奖
     * @param val
     */
    setSeriesRecord: function (val) {
        this.setTaskCount(cf.TaskType.GROW, 0);
        this._strSeriesRecord = this._parseTaskString(val, cf.TaskType.GROW);
    },

    getSeriesRecord: function () {
        return this._strSeriesRecord;
    },

    setVipExp: function (val) {
        this._nVIPExp = val;
    },

    getVipExp: function () {
        return this._nVIPExp;
    },

    setCoupons: function (val) {
        this._nCoupons = val;
    },

    getCoupons: function () {
        return this._nCoupons;
    },

    setLottery: function (val) {
        this._nLottery = val;
    },

    getLottery: function () {
        return this._nLottery;
    },

    setVipEndTime: function (val) {
        this._strVIPEndTime = val;
    },

    getVipEndTime: function () {
        return this._strVIPEndTime;
    },
    /**
     * 获取每日登录记录
     * 1.LoginWeekDate 周记录（周一到周日签到领取记录）
     * 2.LoginDayNumDate 连续登录天数
     * 3.LoginDayDat 日记录 累计天数（3，5，7）
     * @param val
     */
    setLoginData: function (val) {
        this._strLoginData = val;
        cc.log("this._strLoginData = " + this._strLoginData);
        var tempInfo = this._strLoginData.split(",");
        this.setLoginWeekDate(tempInfo[0]);
        this.setLoginDayNumDate(tempInfo[1]);
        this.setLoginDayDate(tempInfo[2]);
        this.setLoginCount(tempInfo[3]);
    },

    getLoginData: function () {
        return this._strLoginData;
    },

    setDoleCoolDownTime: function (val) {
        this._nDoleCoolDownTime = val;
    },

    getDoleCoolDownTime: function () {
        return this._nDoleCoolDownTime;
    },

    /**
     * 格式为：0,0,0,0.....
     字段1，救济金领取次数（房间）
     字段2，救济金领取倒计时，0为无倒计时（房间）
     字段3，在线奖励领取档位（房间）
     字段4，在线奖励领取倒计时（房间）
     字段5，当日打鱼掉钻石数量（房间）
     字段6，日常任务活跃值（大厅）
     字段7，日常活跃值领取箱子记录，用数字位数记录（大厅）
     字段8，个人首次奖池抽奖标记（房间）
     字段9，玩家给好友贺喜次数（大厅）
     字段10，玩家打到彩金鱼的数量（房间）
     字段11，道具每日领取的标示位


     字段16，抽奖，'|'分割表示不同含义，字段说明见处理函数（'|'分割，代表含义：免费抽奖次数|今天已抽奖次数|今天充值总额（分））
     字段17，龙珠数据


     */
    setDailyData: function (val) {
        this._strDailyData = val.split(",");
        //this.setDayActiveVal(this._strDailyData[5]);
        //this.setDayActiveFlag(this._strDailyData[6],true);
        this.setBonusFishNum(this._strDailyData[9]);

        this.setFirstBonus(this._strDailyData[7]);
        this.setItemDailyGetMark(this._strDailyData[10]);

        this.setRouletteRecord(this._strDailyData[16]);
        //解析龙珠数据
        this.parseDragonBallData(this._strDailyData[17]);

        //this.parseDragonSkillData(this._strDailyData[20]);
    },

    getDailyData: function () {
        return this._strDailyData;
    },

    /**
     * 格式为：0,0,0,0.....
     字段1，周活跃点数（大厅）
     字段2，周活跃宝箱领取记录，用数字位数记录（大厅）
     */
    setWeekData: function (val) {
        this._strWeekData = val.split(",");
        //this.setWeekActiveVal(this._strWeekData[0]);
        //this.setWeekActiveFlag(this._strWeekData[1],true);
    },

    getWeekData: function () {
        return this._strWeekData;
    },

    /**
     *
     */
    setItemDailyGetMark: function (info) {
        this._itemDailyGetMark = info;
    },

    /**
     *
     */
    getItemDailyGetMark: function (shoratageId) {
        var dailyInfo = this._itemDailyGetMark;
        var dailyGet;
        if (dailyInfo == 0) {
            dailyGet = 0;
        }
        else {
            dailyGet = (dailyInfo & (1 << (shoratageId - 1))) >> (shoratageId - 1);
        }
        return dailyGet;
    },

    /**
     * 设置日活跃值
     * @param val
     */
    setDayActiveVal: function (val) {
        this._nDayActiveVal = val;
    },

    /**
     * 获取日活跃值
     * @returns {number}
     */
    getDayActiveVal: function () {
        return this._nDayActiveVal;
    },

    /**
     * 设置周活跃值
     * @param val
     */
    setWeekActiveVal: function (val) {
        this._nWeekActiveVal = val;
    },

    /**
     * 获取周活跃值
     * @returns {number}
     */
    getWeekActiveVal: function () {
        return this._nWeekActiveVal;
    },

    /**
     * 设置日活跃领取标记
     * @param val
     * @param bCount 默认不开启任务计数
     */
    setDayActiveFlag: function (val, bCount) {
        if (bCount === undefined)
            bCount = false;
        this._dayActiveFlag = [];
        for (var i = 0; i < 5; ++i) {
            this._dayActiveFlag[i] = ((val >> i) & 1);

            if (bCount) {
                var _taskGift = cf.Data.TASK_GIFT_DATA[i + 11];
                if (this._nDayActiveVal >= _taskGift.activeValue && this._dayActiveFlag[i] == 0) {
                    this._taskFinishCount[cf.TaskType.DAY]++;
                }
            }
        }
    },

    /**
     * 获得日活跃领取标记
     * @returns {null}
     */
    getDayActiveFlag: function () {
        return this._dayActiveFlag;
    },

    /**
     * 设置周活跃领取标记
     * @param val
     * @param bCount 默认不开启任务计数
     */
    setWeekActiveFlag: function (val, bCount) {
        if (bCount === undefined)
            bCount = false;
        this._weekActiveFlag = [];
        for (var i = 0; i < 5; ++i) {
            this._weekActiveFlag[i] = ((val >> i) & 1);

            if (bCount) {
                var _taskGift = cf.Data.TASK_GIFT_DATA[i + 21];
                if (this._nWeekActiveVal >= _taskGift.activeValue && this._weekActiveFlag[i] == 0) {
                    this._taskFinishCount[cf.TaskType.WEEK]++;
                }
            }
        }
    },

    /**
     * 获取周活跃领取标记
     * @returns {null}
     */
    getWeekActiveFlag: function () {
        return this._weekActiveFlag;
    },

    /** 玩家一次性记录
     * 格式为：0,0,0,0,0
     字段1，新手引导标记
     字段2，首充礼包标记
     字段3，各种充值翻倍标记，用位数表示
     字段4，个人首次奖池抽奖标记（暂时不用）
     字段5，vip领奖记录，用位数表示
     字段6，修改昵称次数

     */

    setOneTimeData: function (val) {
        this._strOneTimeData = val.split(",");
        this.setFirstPayMark(this._strOneTimeData[1]);
        this.setFirstPayRewardInfo(this._strOneTimeData[2]);
        this.setVIPGift(this._strOneTimeData[4]);
        this.setModifyNickname(this._strOneTimeData[5]);
    },
    /**
     * 获取玩家一次性记录
     * @returns {null}
     */
    getOneTimeData: function () {
        return this._strOneTimeData;
    },
    /**
     * 设置彩金鱼的数量
     * @param val
     */
    setBonusFishNum: function (val) {
        cc.log("setBonusFishNum");
        this._nBonusFishNum = val;
        cc.log(val);
        cc.log(this._nBonusFishNum);
    },
    /**
     * 获取彩金鱼的数量
     * @returns {number}
     */
    getBonusFishNum: function () {
        return this._nBonusFishNum;
    },
    /**
     * 设置个人首次奖池抽奖标记
     * @param val
     */
    setFirstBonus: function (val) {
        this._FirstBonus = val;
    },

    /**
     * 获取个人首次奖池抽奖标记
     * @returns {number}
     */
    getFirstBonus: function () {
        return this._FirstBonus;
    },

    /**
     * 设置首次充值双倍的标记信息
     */
    setFirstPayRewardInfo: function (info) {
        this._firstPayRewardInfo = parseInt(info);
        cc.log(this._firstPayRewardInfo);
    },

    /**
     * 获取首次充值的标记信息
     */
    getFirstPayRewardInfo: function (shoratageId) {
        var getInfo = this._firstPayRewardInfo;
        var doubleGet;
        if (getInfo == 0) {
            doubleGet = 0;
        }
        else {
            doubleGet = (getInfo & (1 << (shoratageId - 1))) >> (shoratageId - 1);
        }
        return doubleGet;
    },

    /**
     * 设置已拥有的皮肤
     * 格式：id,time|id,time|..., time = 0为永久
     * @param val
     */
    setFortSkin: function (val) {
        this._strFortSkin = cf.parseStringForMap(val);
    },

    /**
     * 获取已拥有的皮肤
     * @returns {null}
     */
    getFortSkin: function () {
        return this._strFortSkin;
    },
    /**
     * 设置正在使用的皮肤
     * @param val
     */

    setFortSkinUse: function (val) {
        this._nFortSkinUse = val;
    },
    /**
     * 获取正在使用的皮肤
     * @returns {null}
     */
    getFortSkinUse: function () {
        return this._nFortSkinUse;
    },

    /**
     * 检测玩家是否存在该道具 如果存在则增加数量 不存在则添加新道具
     * @param   itemId          { number } 道具Id
     * @param   itemNum         { number } 道具数量
     * @param   itemEndTime     { number } 道具限时
     */
    setPlayerItem: function (itemId, itemNum, itemEndTime, nError) {
        if (nError != 0)
            return;
        if (itemId == null && itemNum == null && itemEndTime == null) {
            //cc.warn("修改道具信息传递的数据出错");
        }
        //获取包含道具信息的数组
        if (!this._strItemList) {
            //如果不存在则新建
            //cc.warn("获取道具列表出错");
            this._strItemList = cf.Map.create();
        }
        //cc.log("修改道具");
        if (itemNum != null) {
            if (itemNum <= 0) {
                this._strItemList.remove(itemId);
                //cc.log("道具数量为0，或者已超出使用时间，移除道具");
            }
            else {
                if (!itemEndTime || itemEndTime == "") {
                    this._strItemList.put(itemId, [itemNum]);
                    cc.log("添加新道具");
                }
                else {
                    this._strItemList.put(itemId, [itemNum, itemEndTime]);
                    cc.log("添加新道具并添加道具的使用时间");
                }
            }
        }
    },

    /**
     * 根据道具id获取玩家的道具数量
     */
    getPlayerItemNum: function (itemId) {
        var self = this;
        var itemMap = self.getItemList();
        if (itemMap) {
            var valueArr = itemMap.get(itemId);
            if (valueArr) {
                if (valueArr[0]) {
                    return valueArr[0];
                }
                else {
                    return 0;
                }
            }
            else {
                return 0;
            }
        }
        else {
            return 0;
        }
    },

    /**
     * 根据给出的道具id获取玩家的道具结束时间
     */
    getPlayerItemEndTime: function (itemId) {
        var self = this;
        var itemMap = self.getItemList();
        if (itemMap) {
            var valueArr = itemMap.get(itemId);
            if (valueArr) {
                if (!valueArr[1]) {
                    cc.warn("该道具没有使用时间");
                    return 0;
                }
                else {
                    return valueArr[1];
                }
            }
        }
        else {
            cc.warn("获取道具列表出错");
        }
    },

    /**
     * 设置VIP领奖记录
     * @param val
     */
    setVIPGift: function (val) {
        this._VIPGift = val;
        cc.log(this._VIPGift);
    },

    /**
     * 获得VIP领奖记录
     * @returns {number}
     */
    getVIPGift: function () {
        return this._VIPGift;
    },

    /**
     * 设置全局游戏时间
     * @param val
     */
    setGameTime: function (val) {
        this._gameTime = Math.floor(val);
    },

    /**
     * 设置全局游戏时间偏移
     * @param val
     */
    setOffsetGameTime: function (val) {
        this._gameTime += Math.floor(val);
    },

    /**
     * 获取全局游戏时间
     * @returns {number}
     */
    getGameTime: function () {
        return this._gameTime;
    },

    /**
     * 移除炮台皮肤
     * @remark 只需传入其中一个参数即可
     * @param itemId 道具id，见道具表
     * @param skinId 皮肤id，见皮肤表
     */
    removeFortSkin: function (itemId, skinId) {
        if (this._strFortSkin) {
            if (itemId) {
                var len = cf.Data.CANNON_DATA.length;
                for (var i = 0; i < len; ++i) {
                    var skinData = cf.Data.CANNON_DATA[i];
                    if (itemId == skinData.connect_item_id || itemId == skinData.connect_timeItem_id) {
                        this._strFortSkin.remove(i);
                        break;
                    }
                }
            }

            if (skinId) {
                this._strFortSkin.remove(skinId);
            }
        }
    },

    /**
     * 添加炮台皮肤
     * @remark 只需传入其中一个id参数即可
     * @param itemId 道具id，见道具表
     * @param skinId 皮肤id，见皮肤表
     * @param time 到期时间，0表示永久
     */
    addFortSkin: function (itemId, skinId, time) {
        if (this._strFortSkin == null) {
            //为null则创建一个map
            this._strFortSkin = cf.Map.create();
        }
        if (itemId) {
            var len = cf.Data.CANNON_DATA.length;
            for (var i = 0; i < len; ++i) {
                var skinData = cf.Data.CANNON_DATA[i];
                if (itemId == skinData.connect_item_id || itemId == skinData.connect_timeItem_id) {
                    this._strFortSkin.put(i, [time]);
                    break;
                }
            }
            return;
        }

        if (skinId) {
            this._strFortSkin.put(skinId, [time]);
        }
    },

    getFortSkinSize: function () {
        if (this._strFortSkin) {
            return this._strFortSkin.size();
        }
        return 0;
    },

    /**
     * 邮件标记
     *@param val
     */
    setEmailData: function (val) {
        this._strEmailData = val;
    },

    getEmailData: function () {
        return this._strEmailData;
    },

    /**
     * 设置修改昵称次数
     * @param val
     */
    setModifyNickname: function (val) {
        this._ModifyNickname = val;
        cc.log("this._ModifyNickname = " + this._ModifyNickname);
    },

    /**
     * 获取昵称修改次数
     * @returns {number}
     */
    getModifyNickname: function () {
        return this._ModifyNickname;
    },

    /**
     * 设置累计天数登录记录
     * @param val
     */
    setLoginDayDate: function (val) {
        this._LoginDayDate = val;
        cc.log("this._LoginDayDate = " + this._LoginDayDate);
    },

    /**
     * 获取累计天数登录记录
     * @returns {null}
     */
    getLoginDayDate: function () {
        return this._LoginDayDate;
    },

    /**
     * 设置签到周记录
     * @param val
     */
    setLoginWeekDate: function (val) {
        this._LoginWeekDate = val;
        cc.log("this._weekDate = " + this._LoginWeekDate);
    },

    /**
     * 设置签到次数
     */
    setLoginCount: function (val) {
        this._nNewLoginCount = val;
    },

    /**
     * 获得签到次数
     */
    getLoginCount: function () {
        return this._nNewLoginCount;
    },

    /**
     * 获取签到周记录
     * @returns {null}
     */
    getLoginWeekDate: function () {
        return this._LoginWeekDate;
    },

    /**
     * 设置连续登录天数记录
     * @param val
     */
    setLoginDayNumDate: function (val) {
        this._LoginDayNumDate = val;
        cc.log("this._LoginDayNumDate = " + this._LoginDayNumDate);
    },

    /**
     * 获取连续登录天数记录
     * @returns {number}
     */
    getLoginDayNumDate: function () {
        return this._LoginDayNumDate;
    },

    /**
     * 离开房间
     * @param info
     */
    leaveRoom: function (info) {
        this._leaveRoomInfo = info;
        var msg = new $root.CS_LeaveRoomMsg();
        msg.nidLogin = this._nIdLogin;
        msg.nSeatID = this._nSeatID;
        var wr = $root.CS_LeaveRoomMsg.encode(msg).finish();
        cf.NetMgr.sendSocketMsg(MessageCode.CS_LeaveRoomMsg, wr);
    },

    /**
     * 获取离开房间信息
     * @returns {null}
     */
    getLeaveRoomInfo: function () {
        return this._leaveRoomInfo;
    },

    /**
     * 设置活动记录
     * @param val
     */
    setActRecordList: function (val) {
        this._actRecordList = cf.Map.create();
        for (var i = 0; i < val.length; i++) {
            var info = {};
            info.nActID = val[i].nActID;                                //活动id
            info.nScore = val[i].nScore;                                //活动类型
            info.strOneTimeData = val[i].strOneTimeData;     //活动记录(0可领取1已领取)

            this._actRecordList.put(info.nActID, info);
        }
        cc.log("this._actRecordList");
        cc.log(this._actRecordList);
        
    },

    /**
     * 获取活动记录
     * @returns {number}
     */
    getActRecordList: function () {
        return this._actRecordList;
    },

    /**
     * 设置子弹发射数量
     * @param val
     */
    setBulletCount: function (val) {
        this._bulletCount = val;
    },

    /**
     * 获取子弹数量
     * @returns {number}
     */
    getBulletCount: function () {
        return this._bulletCount;
    },

    /**
     * 设置子弹发射增量
     * @param val
     */
    setBulletCountOffset: function (val) {
        this._bulletCount += val;
    },

    /**
     * 设置子弹反弹次数
     * @param val
     */
    setBulletHitCount: function (val) {
        this._vipBulletHitCount = val;
    },

    /**
     * 获取子弹反弹次数
     * @returns {null}
     */
    getBulletHitCount: function () {
        return this._vipBulletHitCount;
    },

    /**
     * 是否显示每日登陆奖励
     * @returns {boolean}
     */
    isLoginShow: function () {
        var date = new Date();
        date.setTime(this._gameTime);
        //弹每日登录界面
        //cc.log(date.getDay());

        var tempDay = date.getDay();
        if (tempDay == 0) {
            tempDay = 7;
        }

        var nSaveWeek = parseInt(this._LoginWeekDate);
        var nSaveCon = parseInt(this._LoginDayDate);

        var arrnWeekGet = [];
        var arrnConGet = [];
        cc.log(this._LoginDayNumDate);
        //获取每日登录标记
        cc.log("arrnWeekGet**************");
        for (var i = 0; i < 7; i++) {
            arrnWeekGet[i] = (nSaveWeek & (3 << (i * 2))) >> (i * 2);
            cc.log(arrnWeekGet[i]);
        }

        //获取连续登录标记
        cc.log("arrnConGet**************");
        for (var i = 0; i < 4; i++) {
            arrnConGet[i] = (nSaveCon & (1 << i)) >> i;
            cc.log(arrnConGet[i]);
        }

        if (this._strLoginData != "" && arrnWeekGet[tempDay - 1]) {
            if (arrnWeekGet[tempDay - 1] != 3) {
                return true;
            }
        }
        return false;
    },

    setConsumeSkillNeedPrompt: function (state) {
        this._consumeSkillByDiamondNeedPrompt = state;
    },

    getConsumeSkillNeedPrompt: function () {
        return this._consumeSkillByDiamondNeedPrompt;
    },

    /**
     * 初始化任务计数
     * @param val
     */
    initTaskCount: function (val) {
        this._taskFinishCount = [];
        for (var i = 0; i < 3; ++i) {
            this._taskFinishCount[i] = val;
        }
    },

    /**
     * 增加任务计数
     * @param type
     * @param val
     */
    addTaskCount: function (type, val) {
        this._taskFinishCount[type] += val;
        //cc.error("任务类型 type = "+type+", 任务数量 val = "+this._taskFinishCount[type]);
    },

    /**
     * 减少任务计数
     * @param type
     * @param val
     */
    subTaskCount: function (type, val) {
        this._taskFinishCount[type] -= val;
        //cc.error("任务类型 type = "+type+", 任务数量 val = "+this._taskFinishCount[type]);
    },

    /**
     * 设置任务计数
     * @param type
     * @param val
     */
    setTaskCount: function (type, val) {
        this._taskFinishCount[type] = val;
    },

    /**
     * _stopSendMsg
     */
    setStopSendMsg: function (state) {
        this._stopSendMsg = state;
    },

    getStopSendMsg: function () {
        return this._stopSendMsg;
    },

    /**
     * 获得任务计数
     * @param type
     * @returns {*}
     */
    getTaskCount: function (type) {
        if (type == undefined) {
            var count = 0;
            for (var i = 0; i < 3; ++i) {
                count += this._taskFinishCount[i];
            }
            return count;
        }
        return this._taskFinishCount[type];
    },

    /**
     * 解析任务，并添加任务计数
     * @param str
     * @param taskType
     * @returns {*}
     * @private
     */
    _parseTaskString: function (str, taskType) {
        if (str == "") {
            return null;
        }

        if (!cc.isString(str)) {
            cc.warn("cf.parseString:not string!!!");
            return null;
        }
        var map = [];
        //容错处理
        str = str.replace(/｜/g, "|");
        var arr1 = str.split("|");
        var len = arr1.length;
        for (var i = 0; i < len; i++) {
            var arr2 = arr1[i].split(",");

            //统计任务完成数量
            if (taskType == cf.TaskType.GROW) {
                if (parseInt(arr2[3]) == 1) {
                    this._taskFinishCount[taskType]++;
                }
            } else {
                if (parseInt(arr2[2]) == 1) {
                    this._taskFinishCount[taskType]++;
                }
            }
            map.push(arr2);
        }
        return map;
    },

    getPopUpMonthCardLayer: function () {
        var presentTime = (new Date()).getTime();
        if (!(this.getVipEndTime() == "" || this.getVipEndTime() == null || this.getVipEndTime() == 0)) {
            if (this.getVipEndTime() > presentTime) {
                var monthCardGet = this.getItemDailyGetMark();
                if (monthCardGet != null) {
                    if (monthCardGet == 0) {
                        return true
                    }
                }
            }
        }
        return false;
    },

    //初始化寻宝次数
    setTotalTreasureNum: function (num) {
        this._totalTreasureNum = num;
    },

    //获取寻宝次数
    getTotalTreasureNum: function () {
        return this._totalTreasureNum;
    },

    //设置剩余的寻宝次数
    setTreasureNum: function (num) {
        this._treasureNum = num;
    },

    //获取剩余的寻宝次数
    getTreasureNum: function () {
        return this._treasureNum;
    },

    setUpdateRankTime: function (val) {
        this._nUpdateRankTime = val;
    },

    getUpdateRankTime: function () {
        return this._nUpdateRankTime;
    },

    setBillingWelfare: function (val) {
        this._nBillingWelfare = val;
    },

    getBillingWelfare: function () {
        return this._nBillingWelfare;
    },
    setSinglePairCannon: function (val) {
        this._nSinglePairCannon = val;
    },

    getSinglePairCannon: function () {
        return this._nSinglePairCannon;
    },
    setIsBUpdateGrandRank: function (val) {
        this._isBUpdateGrandRank = val;
    },

    getIsBUpdateGrandRank: function () {
        return this._isBUpdateGrandRank;
    },

    setNoviceGuideMask: function (guideMask) {
        this._noviceGuideMask = guideMask;
    },

    getNoviceGuideMask: function (guideId) {
        var getInfo = this._noviceGuideMask;
        var maskFinish;
        if (getInfo == 0) {
            maskFinish = 0;
        }
        else {
            maskFinish = (getInfo & (1 << guideId)) >> guideId;
        }
        return maskFinish;
    },

    /**
     * 完成新手引导步骤设置标记位
     * @param guideId
     * @returns {Array}
     */
    changeNoviceGuideMask: function (guideId) {
        var getInfo = this._noviceGuideMask;
        var maskFinish = [];
        for (var i = 0; i < GuideLayer.PhaseEnum.poolReward + 1; i++) {
            maskFinish[i] = (getInfo & (1 << i)) >> i;
        }
        maskFinish[guideId] = 1;
        return maskFinish;
    },

    /**
     * 结束新手引导
     * @returns {Number}
     */
    endNoviceGuideMask: function () {
        var mask = "";
        for (var j = 0; j < GuideLayer.PhaseEnum.taskHas + 1; j++) {
            mask = mask + (1 + "");
        }
        return parseInt(mask, 2);
    },

    /**
     * 更新VIP标记位
     * @param VIPLevel
     */
    updateVIPGift: function (VIPLevel) {
        var arrVIPGift = [];
        var VIPGift = this.getVIPGift();

        //VIP领取标记
        for (var i = 0; i < 8; i++) {
            arrVIPGift[i] = (VIPGift & (1 << i)) >> i;
        }
        arrVIPGift[VIPLevel - 1] = 1;
        var temp = "";
        for (var i = 7; i >= 0; i--) {
            temp += arrVIPGift[i];
        }
        this.setVIPGift(parseInt(temp, 2));
    },
    setFAccPoints: function (val) {
        this._fAccPoints = val;
        cc.log("this._fAccPoints = " + this._fAccPoints);
    },

    getFAccPoints: function () {
        return this._fAccPoints;
    },

    setFirstPayMark: function (value) {
        this._firstPayMark = value;
    },

    getFirstPayMark: function () {
        return this._firstPayMark;
    },
    setUpdateExchangeTime: function (value) {
        this._nUpdateExchangeTime = value;
    },

    getUpdateExchangeTime: function () {
        return this._nUpdateExchangeTime;
    },
    setExchangeInfo: function (value) {
        if (this._exchangeInfo == null) {
            this._exchangeInfo = [];
        }
        this._exchangeInfo = value;
        cc.log(this._exchangeInfo);
    },

    getExchangeInfo: function () {
        return this._exchangeInfo;
    },

    setFTempAccPoints: function (val) {
        this._fTempAccPoints = val;
    },

    getFTempAccPoints: function () {
        return this._fTempAccPoints;
    },

    /**
     * 记录进入渔场时的金币
     * @param val
     */
    setLastGoldNum: function (val) {
        this._lastGoldNum = val;
    },

    /**
     * 得到进入渔场时的金币
     * @returns {number}
     */
    getLastGoldNum: function () {
        return this._lastGoldNum;
    },

    /**
     * 设置扑克牌数量
     * @param val
     */
    setPokerNum:function (val) {
        this._pokerNum = val;
    },
    
    getPokerNum:function () {
        return this._pokerNum;
    },

    /**
     * 记录玩家争霸赛积分
     */
    setRankScore: function (val) {
        this._rankPlayerScore = val;
    },

    /**
     * 获取玩家争霸赛积分
     */
    getRankScore: function () {
        return this._rankPlayerScore;
    },
    /**
     * 记录争霸赛日排行榜更新状态
     * @param val
     */
    setDayRankPlayerState: function (val) {
        this._rankDayPlayerState = val;
    },
    /**
     * 记录争霸赛日排行榜更新状态
     * @returns {number}
     */
    getDayRankPlayerState: function () {
        return this._rankDayPlayerState;
    },
    /**
     * 记录争霸赛周排行榜更新状态
     * @param val
     */
    setWeekRankPlayerState: function (val) {
        this._rankWeekPlayerState = val;
    },
    /**
     * 记录争霸赛周排行榜更新状态
     * @returns {number}
     */
    getWeekRankPlayerState: function () {
        return this._rankWeekPlayerState;
    },
    /**
     * 设置轮盘抽奖记录
     * @param val
     */
    setRouletteRecord: function (val) {
        this._RouletteRecord = cf.parseString(val);
        cc.log(this._RouletteRecord);
    },
    /**
     * 获取轮盘抽奖记录
     * @returns {number}
     */
    getRouletteRecord: function () {
        return this._RouletteRecord;
    },
    /**
     * 设置新手的摇奖标记
     */
    setLotteryGetSign: function (val) {
        this._newPlayerLotteryGetSign = val;
    },
    /**
     * 获取新手的摇奖标记
     */
    getLotteryGetSign: function () {
        return this._newPlayerLotteryGetSign;
    },
    /**
     * 设置时候开始新手三日兑换券摇奖
     */
    setIsOpenLotteryGet: function (val) {
        this._isOpenNewPlayerLotteryGet = val;
    },
    /**
     * 返回时候可以开启新手三日兑换券抽奖
     */
    getIsOpenLotteryGet: function () {
        return this._isOpenNewPlayerLotteryGet;
    },
    /**
     * 签到摇奖完成变量的设置
     */
    setHasDrawLottery: function (val) {
        this._hasDrawLottery = val;
    },
    /**
     * 获取摇奖是否已经完成
     */
    getHasDrawLottery: function () {
        return this._hasDrawLottery;
    },

    /**
     * 设置签到奖励倍数
     */
    setMultipleLotteryNum: function (multipleNum) {
        this._multipleLotteryNum = multipleNum;
    },

    /**
     * 获取签到奖励倍数
     */
    getMultipleLotteryNum: function () {
        return this._multipleLotteryNum;
    },

    /**
     * 设置签名信息
     */
    setSignature: function (signature) {
        this._playerSignature = signature;
    },

    /**
     * 获取签名信息
     */
    getSignature: function () {
        return this._playerSignature;
    },

    /**
     * 设置权限
     * @param nPermission
     */
    setPermission:function (nPermission) {
        this._nPermission = nPermission;        
    },

    /**
     * 获取权限
     * @returns {number}
     */
    getPermission:function () {
        return this._nPermission;
    },

    //设置玩家筹码数量
    setPlayerChipNum: function (chipNum) {
        this._playerChipNum = chipNum;
    },

    //获取玩家筹码数量
    getPlayerChipNum: function () {
        return this._playerChipNum;
    },

    /**
     * 设置龙珠数据
     * 格式：房间|炮倍|数量，例如：1001#1000000#1|1002#2000000#2
     *
     */
    parseDragonBallData:function (val) {
        var _dbData = val.split("|");
        for(var i = 0; i < _dbData.length; ++i){
            var _roomBallData = _dbData[i].split("#");
            if(_roomBallData.length > 1){
                this._dragonBallRoomData[_roomBallData[0]] = _roomBallData;
            }
        }
    },

    setDragonBallData:function (roomId,attrBall, attrBullet, collection) {
        var _roomBallData = this._dragonBallRoomData[roomId];
       
        if(_roomBallData){
            this._dragonBallRoomData[roomId][2] = attrBall;
            this._dragonBallRoomData[roomId][3] = attrBullet;
            this._dragonBallRoomData[roomId][4] = collection;
            
        }
        else{
            this._dragonBallRoomData[roomId]=[]
            this._dragonBallRoomData[roomId][2] = attrBall;
            this._dragonBallRoomData[roomId][3] = attrBullet;
            this._dragonBallRoomData[roomId][4] = collection;
        }
    },
  
    getDragonBallData:function (roomId) {
        if(this._dragonBallRoomData[roomId]){
            return this._dragonBallRoomData[roomId];
        }
        return null;
    }
});



/**
 * 创建玩家数据模型
 * @returns {null}
 */

PlayerModel.create = function () {
    return new PlayerModel();
};