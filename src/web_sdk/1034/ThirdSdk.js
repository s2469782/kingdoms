/**
 * Created by beiping on 2017/5/4.
 */

var ThirdSdk = ThirdSdk || {};

ThirdSdk.init = function () {    
};

ThirdSdk.login = function () {
    //获取登录信息
    var obj = {};
    obj.user_id = this.getParamString("idlogin");
    obj.json = this.getParamString("token");

    return obj;
};

ThirdSdk.logout = function () {
};

ThirdSdk.getPayInfo = function () {    
    return null;
};

ThirdSdk.callPaySdk = function (msg,callback) {
    callback(0);
};

ThirdSdk.getParamString = function (key) {
    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)return decodeURI(r[2]);
    return null;
};
