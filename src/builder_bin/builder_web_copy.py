# coding=gbk

import os
import shutil
import time
#import tinify

#tinify.key = "pk3t5RhpvTbnmwzK7NDlvJj_EDwuwSsa"

# 获取当前路径
curPath = os.path.abspath(os.curdir);

# 删除.*后缀文件
def remove_file_with_ext(work_dir, ext, exceptpath):
	file_list = os.listdir(work_dir)
	for f in file_list:		
		full_path = os.path.join(work_dir, f)
		if os.path.isdir(full_path):
			remove_file_with_ext(full_path, ext,exceptpath)
		elif os.path.isfile(full_path):
			isDel = True;			
			for ex_path in exceptpath:
				if work_dir == ex_path:
					isDel = False;
			name, cur_ext = os.path.splitext(f)
			if cur_ext == ext and isDel:
				os.remove(full_path)

# 拷贝文件
def copy_files(src, dst):

	for item in os.listdir(src):
		path = os.path.join(src, item)
		# Android can not package the file that ends with ".gz"
		if not item.startswith('.') and not item.endswith('.gz') and os.path.isfile(path):
			shutil.copy(path, dst)
		if os.path.isdir(path):
			new_dst = os.path.join(dst, item)
			os.mkdir(new_dst)
			copy_files(path, new_dst)
			
def compressPng(filepath,exceptFile):		
	for root,dirs,files in os.walk(filepath):
		for file in files:
			fileFullPath = os.path.join(root,file)
			
			isBreak = False;
			for ex_file in exceptFile:
				if file == ex_file:
					isBreak = True;
			
			#根据条件压缩png
			ext = os.path.splitext(fileFullPath)[1]
			if ext == ".png" and isBreak == False:
				compile_cmd = "builder_bin\\bin\\pngquant.exe --verbose --force --speed 3 256 "+fileFullPath+" --ext .png"
				print(compile_cmd)
				os.system(compile_cmd)

def compressJs(filepath,exceptpath):		
	for root,dirs,files in os.walk(filepath):
		for file in files:
			fileFullPath = os.path.join(root,file)
			
			#检测不压缩的目录
			isBreak = False;
			for ex_path in exceptpath:
				if root == ex_path:
					isBreak = True;
			#根据条件压缩png
			ext = os.path.splitext(fileFullPath)[1]
			if ext == ".js" and isBreak == False:
				print "开始压缩"+fileFullPath
				compile_cmd = "uglifyjs "+fileFullPath+" -m -c -o "+fileFullPath;
				os.system(compile_cmd)
				print "压缩完成"

# 拷贝assets资源
def copy_resources(app_web_root):
	web_res_dir = os.path.join(app_web_root,"publish\\html5\\res")
	web_sdk_dir = os.path.join(app_web_root,"publish\\html5\\web_sdk")
	if os.path.isdir(web_sdk_dir):		
		shutil.rmtree(web_sdk_dir)
	# 创建目录	
	os.mkdir(web_sdk_dir)
	# 复制文件
	print "复制渠道SDK..."
	resources_dir = os.path.join(app_web_root, "web_sdk")	
	copy_files(resources_dir, web_sdk_dir)
	
	#例外目录
	exceptpath = [];
	#压缩	
	#compressDir(web_res_dir,exceptpath);
	
	#删除没有文件
	remove_file_with_ext(web_res_dir,".ogg", exceptpath);
	remove_file_with_ext(web_res_dir,".manifest", exceptpath);
	remove_file_with_ext(web_res_dir,".ttf", exceptpath)
	#remove_file_with_ext(web_res_dir,".png", exceptpath);
	print "删除无用文件..."
	
	# 例外文件
	exceptFile = [
		"scale9.png",
		"font_text_0.png",
		"font-001_0.png",
		"font-002_0.png"
	];
	#压缩	
	compressPng(web_res_dir,exceptFile);
	
	# 重名名alpha文件
	#os.system("builder_bin\\builder_rename_png.bat "+web_res_dir);
	#生成update.js
	compile_cmd = "builder_bin\\bin\\createUpadtejs.exe -i "+web_res_dir+" -o "+web_res_dir+" -r res";
	os.system(compile_cmd)
	#压缩update.js
	compile_cmd = "uglifyjs "+web_res_dir+"\update.js -m -c -o "+web_res_dir+"\update.js";
	os.system(compile_cmd)
	#压缩sdk	
	compressJs(web_sdk_dir,exceptpath)
	print "压缩js..."

#复制资源
copy_resources(curPath)