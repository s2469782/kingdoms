# coding=gbk

import os
import shutil

# 获取当前路径
curPath = os.path.abspath(os.curdir)
outPath = curPath+"\\assets";

# 删除文件
def del_file_and_dir(deldir):
	filelist=[]
	filelist=os.listdir(deldir)
	for f in filelist:
	  filepath = os.path.join( deldir, f )
	  if os.path.isfile(filepath):
		os.remove(filepath)
		print filepath+" removed!"
	  elif os.path.isdir(filepath):
		shutil.rmtree(filepath,True)
		print "dir "+filepath+" removed!"

# 删除.*后缀文件
def remove_file_with_ext(work_dir, ext):
	file_list = os.listdir(work_dir)
	for f in file_list:
		full_path = os.path.join(work_dir, f)
		if os.path.isdir(full_path):
			remove_file_with_ext(full_path, ext)
		elif os.path.isfile(full_path):
			name, cur_ext = os.path.splitext(f)
			if cur_ext == ext:
				os.remove(full_path)
		
# 编辑js为字节码		
def jscompile(outdir):
	rm_ext = ".js"
	compile_cmd = "cocos jscompile -s "+outdir+" -d "+outdir
	os.system(compile_cmd)
	remove_file_with_ext(outdir,rm_ext)
	
	#os.system("call builder_encode_res.bat")
	
# 拷贝文件
def copy_files(src, dst):

	for item in os.listdir(src):
		path = os.path.join(src, item)
		# Android can not package the file that ends with ".gz"
		if not item.startswith('.') and not item.endswith('.gz') and os.path.isfile(path):
			shutil.copy(path, dst)
		if os.path.isdir(path):
			new_dst = os.path.join(dst, item)
			os.mkdir(new_dst)
			copy_files(path, new_dst)

# 拷贝assets资源
def copy_resources(app_android_root):
	
	# remove app_android_root/assets if it exists
	assets_dir = os.path.join(app_android_root, "assets")
	print "dir "+assets_dir
	if os.path.isdir(assets_dir):
		shutil.rmtree(assets_dir)

	# copy resources
	os.mkdir(assets_dir)

	assets_res_dir = assets_dir + "/res";
	assets_scripts_dir = assets_dir + "/src";
	assets_jsb_dir = assets_dir + "/script";
	os.mkdir(assets_res_dir);
	os.mkdir(assets_scripts_dir);
	os.mkdir(assets_jsb_dir);


	shutil.copy(os.path.join(app_android_root, "../../../main.js"), assets_dir)
	shutil.copy(os.path.join(app_android_root, "../../../project.json"), assets_dir)

	resources_dir = os.path.join(app_android_root, "../../../res")
	copy_files(resources_dir, assets_res_dir)

	resources_dir = os.path.join(app_android_root, "../../../src")
	copy_files(resources_dir, assets_scripts_dir)

	resources_dir = os.path.join(app_android_root, "../../../frameworks/cocos2d-x/cocos/scripting/js-bindings/script")
	copy_files(resources_dir, assets_jsb_dir)
	
	#编译js文件
	jscompile(assets_dir)
	

#复制资源
copy_resources(curPath)


	
	
	
	
	
	
	
	