
const { src, dest, parallel } = require('gulp');
const uglify = require('gulp-uglify-es').default;
var strip = require('gulp-strip-comments');

exports.default = function() {
  return src('output/bundle.js')
    // gulp-uglify 插件并不改变文件名
    .pipe(strip())
    .pipe(uglify())
    .pipe(dest('dist/'));
}

