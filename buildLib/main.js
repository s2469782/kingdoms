var projectConfig = projectConfig || null;
var BUILDProduction = BUILDProduction ||null;
var GameConfig = {
    verName: "",
    verNameForce: "1.0.0.0",
    channelId: "",
    channelName: "",
    gameId: "",
    loginId: "",
    loginType: "",
    resPath: "",
    language: 0,
    suffix: ".png",
    suffix_jpg: ".jpg",
    server_url: "",
    state_url: "",
    download_url: "",
    identity: "",
    mac: "",
    QQ: "",
    WeChat: "",
    Phone: "",
    buy: 0,
    serverIdx: 0
};

var mappingRes = {};

function compare(curV, reqV) {
    if (curV && reqV) {
        //将两个版本号拆成数字
        var arr1 = curV.split('.'),
            arr2 = reqV.split('.');
        var minLength = Math.min(arr1.length, arr2.length),
            position = 0,
            diff = 0;
        //依次比较版本号每一位大小，当对比得出结果后跳出循环
        while (position < minLength && ((diff = parseInt(arr1[position]) - parseInt(arr2[position])) == 0)) {
            position++;
        }
        diff = (diff != 0) ? diff : (arr1.length - arr2.length);
        //若curV大于reqV，则返回true
        return diff > 0;
    } else {
        //输入为空
        return false;
    }
}

var _getParamString = function (key) {
    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)return decodeURI(r[2]);
    return null;
};

var _getNativeVer = function () {
    if (cc.sys.os === cc.sys.OS_IOS) {
        return jsb.reflection.callStaticMethod("JsbHelper", "getVersionName");
    }
    return jsb.reflection.callStaticMethod("com/jsb/helper/JsbHelper", "getVersionName", "()Ljava/lang/String;");
};

if (cc.sys.isNative) {
    //对比版本号
    var ls = cc.sys.localStorage;
    var lastGameVer = ls.getItem("lastGameVer");
    var hotUpdateSearchPaths = ls.getItem('HotUpdateSearchPaths');

    //获取当前版本号
    if (cc.sys.os === cc.sys.OS_WINDOWS)
        GameConfig.verNameForce = "1.0.0.0";
    else
        GameConfig.verNameForce = _getNativeVer();

    if (lastGameVer) {
        var isUpdate = compare(GameConfig.verNameForce, lastGameVer);
        if (isUpdate) {
            var storagePath = (jsb.fileUtils ? jsb.fileUtils.getWritablePath() : "/") + "/update_res";
            jsb.fileUtils.removeDirectory(storagePath, function () {

            });
            ls.setItem("lastGameVer", GameConfig.verNameForce);
            ls.setItem("HotUpdateSearchPaths", null);
        } else {
            //热更新
            if (hotUpdateSearchPaths) {
                jsb.fileUtils.setSearchPaths(JSON.parse(hotUpdateSearchPaths));
            }
        }
    } else {
        ls.setItem("lastGameVer", GameConfig.verNameForce);
    }

    //读取配置文件
    cc.loader.loadJson("gameset.json", function (err, json) {
        if (err) {

        } else {
            GameConfig.verName = json.verName;
            GameConfig.resPath = json.resPath;
            GameConfig.language = json.language;
            GameConfig.server_url = json.server_url;
            GameConfig.state_url = json.state_url;
            GameConfig.download_url = json.download_url;
            GameConfig.QQ = json.QQ;
            GameConfig.WeChat = json.WeChat;
            GameConfig.Phone = json.Phone;
        }
    });

    if (cc.sys.os === cc.sys.OS_ANDROID) {
        //android使用ETC1压缩纹理
        GameConfig.suffix = ".png";
        GameConfig.suffix_jpg = ".jpg";
    } else if (cc.sys.os === cc.sys.OS_IOS) {
        GameConfig.suffix = ".png";
        GameConfig.suffix_jpg = ".jpg";
    }
    
    //读取映射文件
    cc.loader.loadJson("mapping.json",function (err,json) {
        mappingRes = json;
    })
} else {
    GameConfig.verName = g_gameVer;
    GameConfig.channelId = _getParamString("xj_cid");
    GameConfig.channelName = _getParamString("xj_cidName");
    GameConfig.loginType = _getParamString("xj_lt");
    GameConfig.loginId = _getParamString("xj_loginId");
    GameConfig.d = _getParamString("d");
    GameConfig.resPath = "res/";
    GameConfig.suffix = ".png";
    GameConfig.suffix_jpg = ".jpg";
    GameConfig.QQ = "xxxxxx";
    GameConfig.WeChat = "xxxxxx";
    GameConfig.Phone = "xxxxxxx";
    GameConfig.gameId = "1005";    
    
    //获取服务器索引
    GameConfig.serverIdx = _getParamString("xj_serverIdx");
    if(GameConfig.serverIdx == null)
        GameConfig.serverIdx = 0;

    if (GameConfig.channelId == null)
        GameConfig.channelId = "1034";
    if (GameConfig.channelName == null)
        GameConfig.channelName = "1034";
    if (GameConfig.loginType == null)
        GameConfig.loginType = 6;
    if (GameConfig.loginId == null)
        GameConfig.loginId = "1034";
}

/**
 * 游戏启动入口类
 */
cc.game.onStart = function () {
    //程序入口
    AppController.init();
};
if(projectConfig){
    cc.game.run(projectConfig);
}
else{
    cc.game.run();
}
